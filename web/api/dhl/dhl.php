<?php
$data = '<?xml version="1.0" encoding="UTF-8"?>
<req:BookPURequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.dhl.com book-pickup-global-req.xsd" schemaVersion="1.0">
    <Request>
        <ServiceHeader>
            <MessageTime>'.date('c').'</MessageTime>
            <MessageReference>1234567890123456789012345678901</MessageReference>
            <SiteID>clscls</SiteID>
            <Password>PzfZ8UiJma</Password>
        </ServiceHeader>
   </Request>
   <RegionCode>AP</RegionCode>
    <Requestor>
        <AccountType>D</AccountType>
        <AccountNumber>550000055</AccountNumber>
        <RequestorContact>
            <PersonName>Rikhil</PersonName>
            <Phone>23162</Phone>
        </RequestorContact>
    </Requestor>
    <Place>
        <LocationType>B</LocationType>
        <CompanyName>String</CompanyName>
        <Address1>String</Address1>
        <Address2>norfolk st</Address2>
        <PackageLocation>Infosys</PackageLocation>
        <City>Kuala Lumpur</City>
        <StateCode>MH</StateCode>
        <DivisionName>California</DivisionName>
        <CountryCode>MY</CountryCode>
	<PostalCode>2516251</PostalCode>
    </Place>
    <Pickup>
        <PickupDate>2015-05-19</PickupDate>
        <ReadyByTime>10:20</ReadyByTime>
        <CloseTime>14:20</CloseTime>
        <Pieces>1</Pieces>
    </Pickup>
    <PickupContact>
        <PersonName>Subhayu</PersonName>
        <Phone>4801313131</Phone>
        <PhoneExtension>5768</PhoneExtension>
    </PickupContact>
     <ShipmentDetails>
        <AccountType>D</AccountType>
        <AccountNumber>851624480</AccountNumber>
        <BillToAccountNumber>100000000</BillToAccountNumber>
        <AWBNumber>7520067111</AWBNumber>
        <NumberOfPieces>1</NumberOfPieces>
        <Weight>99.9</Weight>
        <WeightUnit>K</WeightUnit>
        <GlobalProductCode>D</GlobalProductCode>
        <DoorTo>DD</DoorTo>
        <DimensionUnit>C</DimensionUnit>
        <InsuredAmount>999999.99</InsuredAmount>
        <InsuredCurrencyCode>USD</InsuredCurrencyCode>
        <Pieces>
            <Weight>9.9</Weight>
            <Width>2</Width>
            <Height>2</Height>
            <Depth>2</Depth>
        </Pieces>
        <SpecialService>S</SpecialService>
        <SpecialService>T</SpecialService>
    </ShipmentDetails>
</req:BookPURequest>
';

$tuCurl = curl_init();
curl_setopt($tuCurl, CURLOPT_URL, "https://xmlpitest-ea.dhl.com/XMLShippingServlet");
curl_setopt($tuCurl, CURLOPT_PORT , 443);
curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
curl_setopt($tuCurl, CURLOPT_HEADER, 0);
curl_setopt($tuCurl, CURLOPT_POST, 1);
curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $data);
curl_setopt($tuCurl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml","SOAPAction: \"/soap/action/query\"", "Content-length: ".strlen($data)));

$tuData = curl_exec($tuCurl);
curl_close($tuCurl);
$simple = $tuData;
$xml = simplexml_load_string($tuData);

print_r($xml);
echo 'xml'; exit();
?>
