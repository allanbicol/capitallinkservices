<?php
require_once( 'EwayPayment.php' );

$eway = new EwayPayment( '91281879', 'https://www.eway.com.au/gateway_cvn/xmlpayment.asp' );

//Substitute 'FirstName', 'Lastname' etc for $_POST["FieldName"] where FieldName is the name of your INPUT field on your webpage
$eway->setCustomerFirstname( 'Leo karl' ); 
$eway->setCustomerLastname( 'Ladeno' );
$eway->setCustomerEmail( 'leokarl2108@gmail.com' );
$eway->setCustomerAddress( '123 Someplace Street, Somewhere ACT' );
$eway->setCustomerPostcode( '2609' );
$eway->setCustomerInvoiceDescription( 'Testing' );
$eway->setCustomerInvoiceRef( 'INV120394' );
$eway->setCardHoldersName( 'TestUser' );
$eway->setCardNumber( '4444333322221111' );
$eway->setCardExpiryMonth( '04' );
$eway->setCardExpiryYear( '14' );
$eway->setTrxnNumber( '4230' );
$eway->setTotalAmount( 100 );
$eway->setCVN( 123 );

if( $eway->doPayment() == EWAY_TRANSACTION_OK ) {
    echo "Transaction successful. Auth Code: " . $eway->getAuthCode();
} else {
    echo "Error occurred (".$eway->getError()."): " . $eway->getErrorMessage();
}
?>
