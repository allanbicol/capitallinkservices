<?php
require_once( 'EwayPayment.php' );

$eway = new EwayPayment( '91281879', 'https://www.eway.com.au/gateway_cvn/xmltest/testpage.asp' );

//Substitute 'FirstName', 'Lastname' etc for $_POST["FieldName"] where FieldName is the name of your INPUT field on your webpage
$eway->setCustomerFirstname( 'Leo karl' ); 
$eway->setCustomerLastname( 'Ladeno' );
$eway->setCustomerEmail( 'leokarl2108@gmail.com' );
$eway->setCustomerAddress( '123 Someplace Street, Somewhere ACT' );
$eway->setCustomerPostcode( '2609' );
$eway->setCustomerInvoiceDescription( 'Testing' );
$eway->setCustomerInvoiceRef( 'INV120394' );
$eway->setCardHoldersName( 'TestUser' );
$eway->setCardNumber( '4444333322221111' );
$eway->setCardExpiryMonth( '04' );
$eway->setCardExpiryYear( '14' );
$eway->setTrxnNumber( '4230' );
$eway->setTotalAmount( 100 );
$eway->setCVN( 123 );

$result = '';
if( $eway->doPayment() == EWAY_TRANSACTION_OK ) {
    $result = "Transaction successful. Auth Code: " . $eway->getAuthCode();
    $data =  array('success' => $result);
} else {
    $result = "Error occurred (".$eway->getError()."): " . $eway->getErrorMessage();
    $data =  array('error' => $result);
}

echo json_encode($data);
?>
