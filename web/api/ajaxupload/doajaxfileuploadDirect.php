<?php
	$error = "";
	$msg = "";
	$fileElementName = 'fileToUpload';
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;

			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}else 
	{
            $path_parts = pathinfo($_FILES["fileToUpload"]["name"]);
            $extension = $path_parts['extension'];
            $allowedExts = array(
                "pdf", 
                "doc", 
                "docx",
                "jpg",
                "jpeg",
                "png",
                "gif"
              ); 
            if(!in_array($extension, $allowedExts)){
                $error = 'Invalid file type..';
            }else{
		$path = '../../dev/users/'.$_POST['uid'].'/'.$_POST['dir'].'/'; 
                $location = $path . $_FILES['fileToUpload']['name']; 
                $newfilename = $_POST['tm'];
                move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $location); 
                $msg =$location;
                //for security reason, we force to remove all uploaded file
                @unlink($_FILES['fileToUpload']);	
                
                if($_SERVER['REMOTE_ADDR'] == '127.0.0.1'){
                    $con=mysqli_connect("localhost","root","root","apha");
                }else{
                    $con=mysqli_connect("internal-db.s179771.gridserver.com","db179771","Voodoo!750","db179771_apha");
                }
                list($sid, $fid)= explode('/',$_POST['dir']);
                mysqli_query($con,"INSERT INTO tbl_submission_facility_files(facility_id,filename) VALUES(".$fid.",'".$_FILES['fileToUpload']['name']."')");
                mysqli_close($con);
                
                
                /* Send Email */
//                $yourWebsite = "APHA"; // the name of your website
//        
//                if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
//                    $headers = "From: $from";
//                    $headers.= "Reply-To: <$recipients>";
//                    $headers .= "MIME-Version: 1.0\r\n";
//                    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//                } else {
//                    $headers = "From: $yourWebsite $from\n";
//                    $headers.= "Reply-To: <$recipients>";
//                    $headers .= "MIME-Version: 1.0\r\n";
//                    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//                }
//
//
//                mail($recipients,$subject,$body,$headers);
            }
	}		
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
?>