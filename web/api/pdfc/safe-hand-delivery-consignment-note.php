<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();
function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}
function ausDateFormat($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($y, $m , $d) = explode("-", $stringdate);
        return $d.'/'.$m.'/'.$y;
    }
}

if(isset($_SESSION['_sf2_attributes']['user_type']) && ($_SESSION['_sf2_attributes']['user_type'] == 'admin-user' || $_SESSION['_sf2_attributes']['user_type'] == 'public' || $_SESSION['_sf2_attributes']['user_type'] == 'corporate' || $_SESSION['_sf2_attributes']['user_type'] == 'government')){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
        $img_url = siteURL().'/capitallinkservices.com.au/web/img';
        $asset_url = siteURL().'/capitallinkservices.com.au/web';
    }else{
        if(php_uname("n") == 'cls-testing'){
            $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdevtest","dreNca8gW4","capitallinkservices_test");
            $img_url = siteURL().'/web/img';
            $asset_url = siteURL().'/web';
        }else{
            $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
            $img_url = siteURL().'/web/img';
            $asset_url = siteURL().'/web';
        }
//
//        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
//        $img_url = siteURL().'/web/img';
//        $asset_url = siteURL().'/web';
    }

    $_GET['order'] = filter_var($_GET['order'], FILTER_SANITIZE_STRING);
    $_GET['order'] = intval($_GET['order']);
    
    if($_SESSION['_sf2_attributes']['user_type'] == 'admin-user'){ 
        $query = mysqli_query($con,"SELECT o.*, CONCAT(p.fname, ' ', p.lname) as sender_contact_name, p.phone, p.address, p.city, p.postcode, p.state, p.mba_organisation_name,
            o.doc_delivery_contact_no, dd.type as courier_type
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no=o.order_no
            LEFT JOIN tbl_settings_document_delivery dd ON dd.id = o.doc_delivery_type
            WHERE order_type=7 AND o.order_no=".$_GET['order']);
    }else{
        $query = mysqli_query($con,"SELECT o.*, CONCAT(p.fname, ' ', p.lname) as sender_contact_name, p.phone, p.address, p.city, p.postcode, p.state, p.mba_organisation_name,
            o.doc_delivery_contact_no, dd.type as courier_type
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no=o.order_no
            LEFT JOIN tbl_settings_document_delivery dd ON dd.id = o.doc_delivery_type
            WHERE order_type=7 AND o.order_no=".$_GET['order']." AND o.client_id=".$_SESSION['_sf2_attributes']['client_id']);
    }
    $order = mysqli_fetch_array($query);
    
    //Select * from tbl_orders where order_type=7 and order_no=10003908

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page>';
    
    
    
    $html .= '<div style="background-image: url('.$asset_url.'/img/CLS-Watermark-transparent.png); background-repeat: no-repeat; background-position: center; font-size: 11px;">';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="font-weight: bold;"><tr>';
    $html .= '<td style="width: 200px; border: 1px solid black; text-align: center; padding-top: 10px; padding-bottom: 10px;" >';
    $html .= '<img src="'.$asset_url.'/img/cls-logo.png" style="width: 140px;">';
    $html .= '<br>CLS COPY';
    $html .= '</td>';
    
    $html .= '<td style="vertical-align: top; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; padding: 10px; " >';
    $html .= '<table>';
    $html .= '<tr>';
    $html .= '<td style="padding-bottom: 12px; width: 130px;">CONSIGNMENT NOTE</td>';
    $html .= '<td style="padding-bottom: 12px;">'.$_GET['order'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="padding-bottom: 12px; width: 130px;">COURIER TYPE</td>';
    $html .= '<td style="padding-bottom: 12px; width: 139px;">'. $order['courier_type'] .'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 130px;">DATE</td>';
    $html .= '<td >'.ausDateFormat($order['doc_package_pickup_date']).'</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<div style="width: 490px; font-weight: bold; background-color: #e6e7e8; padding: 10px 10px 10px 10px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= 'SENDER DETAILS';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-top: 20px; padding-left: 10px; font-weight: bold;">COMPANY</td>';
    $html .= '<td style="width: 270px; padding-top: 20px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_company'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">CONTACT AREA</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">CONTACT NAME</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_name'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">ADDRESS</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= $order['doc_pickup_address']. '<br>';
    $html .= $order['doc_pickup_city'] .' '.$order['doc_pickup_postcode'];
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">PHONE NUMBER</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_no'].'</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    
   
    $html .= '<div style="width: 490px; font-weight: bold; background-color: #e6e7e8; padding: 10px 10px 10px 10px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= 'RECEIVER DETAILS';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">COMPANY</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_delivery_company'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px;">ADDRESS</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= $order['doc_delivery_address']. '<br>';
    $html .= $order['doc_delivery_city'] .' '.$order['doc_delivery_postcode'];
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">PRIMARY CONTACT AREA</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_delivery_primary_receipient_contact_area'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_primary_receipient_contact_name'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_primary_receipient_contact_no'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">ALTERNATE CONTACT AREA (1)</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_pickup_contact_area_alt1'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_recipient_name_alt1'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_contact_no_alt1'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">ALTERNATE CONTACT AREA (2)</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_pickup_contact_area_alt2'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_recipient_name_alt2'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_contact_no_alt2'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    
    
    $html .= '<div style="width: 490px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    $html .= '<tr>';
    $html .= '<td style="width: 201px; padding-top: 45px; padding-bottom: 45px; border-right: 1px solid black; text-align: center;">';;
    $html .= '<img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 80px;">';
    $html .= '</td>';
    $html .= '<td>';
    
    $html .= '<div style="width: 281px; background-color: #e6e7e8; padding: 10px; border-bottom: 1px solid black; ">';
    $html .= 'PACKAGE CONDITION';
    $html .= '</div>';
    
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '</div>';
    
    
    
    
    
    
    
    
    
    
    
    
    
    $html .= '<div style="background-image: url('.$asset_url.'/img/CLS-Watermark-transparent.png); background-repeat: no-repeat; background-position: center; font-size: 11px;">';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="font-weight: bold;"><tr>';
    $html .= '<td style="width: 200px; border: 1px solid black; text-align: center; padding-top: 10px; padding-bottom: 10px;" >';
    $html .= '<img src="'.$asset_url.'/img/cls-logo.png" style="width: 140px;">';
    $html .= '<br>RECEIVER COPY';
    $html .= '</td>';
    
    $html .= '<td style="vertical-align: top; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; padding: 10px; " >';
    $html .= '<table>';
    $html .= '<tr>';
    $html .= '<td style="padding-bottom: 12px; width: 130px;">CONSIGNMENT NOTE</td>';
    $html .= '<td style="padding-bottom: 12px;">'.$_GET['order'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="padding-bottom: 12px; width: 130px;">COURIER TYPE</td>';
    $html .= '<td style="padding-bottom: 12px; width: 139px;">'. $order['courier_type'] .'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 130px;">DATE</td>';
    $html .= '<td >'.ausDateFormat($order['doc_package_pickup_date']).'</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<div style="width: 490px; font-weight: bold; background-color: #e6e7e8; padding: 10px 10px 10px 10px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= 'SENDER DETAILS';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-top: 20px; padding-left: 10px; font-weight: bold;">COMPANY</td>';
    $html .= '<td style="width: 270px; padding-top: 20px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_company'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">CONTACT AREA</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">CONTACT NAME</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_name'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">ADDRESS</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= $order['doc_pickup_address']. '<br>';
    $html .= $order['doc_pickup_city'] .' '.$order['doc_pickup_postcode'];
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">PHONE NUMBER</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_no'].'</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    
   
    $html .= '<div style="width: 490px; font-weight: bold; background-color: #e6e7e8; padding: 10px 10px 10px 10px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= 'RECEIVER DETAILS';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">COMPANY</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_delivery_company'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px;">ADDRESS</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= $order['doc_delivery_address']. '<br>';
    $html .= $order['doc_delivery_city'] .' '.$order['doc_delivery_postcode'];
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">PRIMARY CONTACT AREA</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_delivery_primary_receipient_contact_area'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_primary_receipient_contact_name'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_primary_receipient_contact_no'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">ALTERNATE CONTACT AREA (1)</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_pickup_contact_area_alt1'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_recipient_name_alt1'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_contact_no_alt1'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">ALTERNATE CONTACT AREA (2)</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_pickup_contact_area_alt2'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_recipient_name_alt2'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_contact_no_alt2'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    
    
    $html .= '<div style="width: 490px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    $html .= '<tr>';
    $html .= '<td style="width: 201px; padding-top: 45px; padding-bottom: 45px; border-right: 1px solid black; text-align: center;">';;
    $html .= '<img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 80px;">';
    $html .= '</td>';
    $html .= '<td>';
    
    $html .= '<div style="width: 281px; background-color: #e6e7e8; padding: 10px; border-bottom: 1px solid black; ">';
    $html .= 'PACKAGE CONDITION';
    $html .= '</div>';
    
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '</div>';
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    $html .= '<div style="background-image: url('.$asset_url.'/img/CLS-Watermark-transparent.png); background-repeat: no-repeat; background-position: center; font-size: 11px;">';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="font-weight: bold;"><tr>';
    $html .= '<td style="width: 200px; border: 1px solid black; text-align: center; padding-top: 10px; padding-bottom: 10px;" >';
    $html .= '<img src="'.$asset_url.'/img/cls-logo.png" style="width: 140px;">';
    $html .= '<br>SENDER COPY';
    $html .= '</td>';
    
    $html .= '<td style="vertical-align: top; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; padding: 10px; " >';
    $html .= '<table>';
    $html .= '<tr>';
    $html .= '<td style="padding-bottom: 12px; width: 130px;">CONSIGNMENT NOTE</td>';
    $html .= '<td style="padding-bottom: 12px;">'.$_GET['order'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="padding-bottom: 12px; width: 130px;">COURIER TYPE</td>';
    $html .= '<td style="padding-bottom: 12px; width: 139px;">'. $order['courier_type'] .'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 130px;">DATE</td>';
    $html .= '<td >'.ausDateFormat($order['doc_package_pickup_date']).'</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<div style="width: 490px; font-weight: bold; background-color: #e6e7e8; padding: 10px 10px 10px 10px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= 'SENDER DETAILS';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-top: 20px; padding-left: 10px; font-weight: bold;">COMPANY</td>';
    $html .= '<td style="width: 270px; padding-top: 20px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_company'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">CONTACT AREA</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">CONTACT NAME</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_name'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">ADDRESS</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= $order['doc_pickup_address']. '<br>';
    $html .= $order['doc_pickup_city'] .' '.$order['doc_pickup_postcode'];
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px; font-weight: bold;">PHONE NUMBER</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_pickup_contact_no'].'</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    
   
    $html .= '<div style="width: 490px; font-weight: bold; background-color: #e6e7e8; padding: 10px 10px 10px 10px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= 'RECEIVER DETAILS';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">COMPANY</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">'.$order['doc_delivery_company'].'</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-left: 10px;">ADDRESS</td>';
    $html .= '<td style="width: 270px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= $order['doc_delivery_address']. '<br>';
    $html .= $order['doc_delivery_city'] .' '.$order['doc_delivery_postcode'];
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">PRIMARY CONTACT AREA</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_delivery_primary_receipient_contact_area'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_primary_receipient_contact_name'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_primary_receipient_contact_no'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">ALTERNATE CONTACT AREA (1)</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_pickup_contact_area_alt1'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_recipient_name_alt1'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_contact_no_alt1'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '<div style="width: 510px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    
    $html .= '<tr>';
    $html .= '<td style="width: 183px; font-weight: bold; border-right: 1px solid black; padding-bottom: 7px; padding-top: 7px; padding-left: 10px;">ALTERNATE CONTACT AREA (2)</td>';
    $html .= '<td style="width: 270px; padding-top: 7px; padding-bottom: 7px;  padding-left: 10px;">';
    $html .= '<strong>Area:</strong> '. $order['doc_pickup_contact_area_alt2'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Name:</strong> '. $order['doc_delivery_recipient_name_alt2'] .'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    $html .= '<strong>Phone No.:</strong> '. $order['doc_delivery_contact_no_alt2'] .'';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '</div>';
    
    
    
    
    $html .= '<div style="width: 490px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">';
    $html .= '<table border="0" cellpading="0" cellspacing="0">';
    $html .= '<tr>';
    $html .= '<td style="width: 201px; padding-top: 45px; padding-bottom: 45px; border-right: 1px solid black; text-align: center;">';;
    $html .= '<img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 80px;">';
    $html .= '</td>';
    $html .= '<td>';
    
    $html .= '<div style="width: 281px; background-color: #e6e7e8; padding: 10px; border-bottom: 1px solid black; ">';
    $html .= 'PACKAGE CONDITION';
    $html .= '</div>';
    
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';
    
    $html .= '</div>';
    
    
    
    $html .='</page>';
    
    
    require_once(dirname(__FILE__).'/html2pdf.class.php');
    //$html2pdf = new HTML2PDF('L', array(127.35, 73.5), 'en', true, 'UTF-8', array(5, 3, 5, 3));
    $html2pdf = new HTML2PDF('P', 'A5', 'en', true, 'UTF-8', array(6, 6, 6, 6));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    $html2pdf->WriteHTML($html);
    $html2pdf->Output(); //output to file
}else{
    header('Location: '.siteURL().'/user-login');
}

