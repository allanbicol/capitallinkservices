<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();
function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}

if(isset($_SESSION['_sf2_attributes']['user_type']) && $_SESSION['_sf2_attributes']['user_type'] == 'admin-user'){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","root","cls");
        $img_url = $_SERVER['DOCUMENT_ROOT'].'/cls/web/img';
    }else{
//        $con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
        $img_url = $_SERVER['DOCUMENT_ROOT'].'/web/img';
    }

    $_GET['order'] = filter_var($_GET['order'], FILTER_SANITIZE_STRING);
    $_GET['order'] = intval($_GET['order']);
    
    $query = mysqli_query($con,"SELECT o.order_type, o.visa_mdd_company, o.visa_mdd_fname, o.visa_mdd_lname, o.visa_mdd_contact, o.visa_mdd_address, 
            o.visa_mdd_city, o.visa_mdd_state, o.visa_mdd_postcode, p.fname, p.lname, p.phone, p.address, p.city, p.postcode
        FROM tbl_orders o
        LEFT JOIN tbl_payment p ON p.order_no = o.order_no
        WHERE o.order_no=".$_GET['order']);
    $order = mysqli_fetch_array($query);
    

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page>';
    $html .= '<br/>';
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 100%; vertical-align: top;" >';
    
    $html .= '<span style="font-size: 31px"><b>';
    
    if(trim($order['visa_mdd_company']) != ''){
        $html .= $order['visa_mdd_company'] . '<br/>';
    }
    $html .= 'Attn: ' . $order['visa_mdd_fname'] . ' ' . $order['visa_mdd_lname'] . '<br/>';
    $html .= 'PH: ' . $order['visa_mdd_contact'] . '<br/>';

    if(trim($order['visa_mdd_address']) != ''){
        $html .= $order['visa_mdd_address'] . ', ';
    }

    if(trim($order['visa_mdd_city']) != ''){
        $html .= $order['visa_mdd_city'] . '&nbsp;&nbsp;&nbsp;';
    }

    if(trim($order['visa_mdd_state']) != ''){
        $html .= $order['visa_mdd_state'] . '&nbsp;&nbsp;&nbsp;';
    }

    $html .= $order['visa_mdd_postcode'];
        
        
    $html .= '</b></span>';
    
    
    $html .= '</td>';
    
    
    $html .= '</tr></table>';
    $html .='</page>';
    require_once(dirname(__FILE__).'/html2pdf.class.php');
    $html2pdf = new HTML2PDF('L', array(127.35, 73.5), 'en', true, 'UTF-8', array(5, 3, 5, 3));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    $html2pdf->WriteHTML($html);
    $html2pdf->Output(); //output to file
}

