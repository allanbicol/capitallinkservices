<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();
function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}

if(isset($_SESSION['_sf2_attributes']['user_type']) && $_SESSION['_sf2_attributes']['user_type'] == 'admin-user'){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","root","cls");
        $asset_url = $_SERVER['DOCUMENT_ROOT'].'/cls/web';
    }else{
//        $con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
        $asset_url = $_SERVER['DOCUMENT_ROOT'].'/web';
    }

    $_GET['order'] = filter_var($_GET['order'], FILTER_SANITIZE_STRING);
    $_GET['order'] = intval($_GET['order']);
    $_GET['uid'] = filter_var($_GET['uid'], FILTER_SANITIZE_STRING);
    $_GET['uid'] = intval($_GET['uid']);
    
    $query = mysqli_query($con,"SELECT a.fname, a.mname, a.lname, c.country_name,
            a.passport_no, a.departure_date
        FROM tbl_order_police_clearance_applicants a 
        LEFT JOIN tbl_countries c ON c.id = a.country_id
        WHERE a.order_no=".$_GET['order']." AND a.id=".$_GET['uid']);
    $applicant = mysqli_fetch_array($query);

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page>';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 80%">';
    $html .= '<span style="font-size: 20px">www.capitallinkservices.com.au</span><br/>';
    $html .= "FOR PICKUP PLEASE CALL 6282 7155<br/>";
    $html .= 'Date: '. date('d/m/Y') .'<br/>';
    $html .= '<br/>';
    $html .= '<table border="0" cellpadding="0" cellspacing="0" style="font-weight: bold;">';
    $html .= '<tr><td>PAX</td><td>: '. $applicant['fname'] .' '. $applicant['mname'] .' '. $applicant['lname'] .'</td></tr>';
    $html .= '<tr><td>Nationality</td><td>: '. $applicant['country_name'] .'</td></tr>';
    $html .= '<tr><td>Passport No</td><td>: '. $applicant['passport_no'] .'</td></tr>';
    //$html .= '<tr><td>Departure</td><td>: '. date('d/m/Y', strtotime($applicant['departure_date'])) .'</td></tr>';
    $html .= '</table>';
    $html .= '<p><img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 50px;"></p>';
    $html .= '</td>';
    
    $html .= '<td style="width: 20%;">';
    $html .= '<img src="'.$asset_url.'/img/label-logo.png" style="float: right;>';
    $html .= '</td>';
    $html .= '</tr></table>';
    $html .='</page>';
    require_once(dirname(__FILE__).'/html2pdf.class.php');
    $html2pdf = new HTML2PDF('L', array(127.35, 63.5), 'en', true, 'UTF-8', array(5, 3, 5, 3));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    $html2pdf->WriteHTML($html);
    $html2pdf->Output(); //output to file
}

