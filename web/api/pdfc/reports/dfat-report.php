<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();
    
    $site_web = ($_SERVER['SERVER_NAME'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] ."/cls/web" : $_SERVER['DOCUMENT_ROOT'] ."/web";

    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
    }else{
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices")
        or die('not connected');
        
    }

    
    $condition = '';

        // date from filter
        if(isset($_GET['dtpn_submitted_from'])){
            if(trim($_GET['dtpn_submitted_from']) != ''){
                $_GET['dtpn_submitted_from'] = filter_var($_GET['dtpn_submitted_from'], FILTER_SANITIZE_STRING);
                $_GET['dtpn_submitted_from'] = trim($_GET['dtpn_submitted_from']);
                $condition .= ' AND q.visa_date_submitted_for_processing >= "'. changeFormatToOriginal($_GET['dtpn_submitted_from']).' 00:00:59"';
            }
        }

        if(isset($_GET['dtpn_submitted_to'])){

            if(trim($_GET['dtpn_submitted_to']) != ''){
                $_GET['dtpn_submitted_to'] = filter_var($_GET['dtpn_submitted_to'], FILTER_SANITIZE_STRING);
                $_GET['dtpn_submitted_to'] = trim($_GET['dtpn_submitted_to']);
                $condition .= ' AND q.visa_date_submitted_for_processing <= "'.changeFormatToOriginal($_GET['dtpn_submitted_to']).' 23:59:59"';
            }
        }
        
        // date from filter
        if(isset($_GET['dsubmitted_by_applicant_from'])){
            if(trim($_GET['dsubmitted_by_applicant_from']) != ''){
                $_GET['dsubmitted_by_applicant_from'] = filter_var($_GET['dsubmitted_by_applicant_from'], FILTER_SANITIZE_STRING);
                $_GET['dsubmitted_by_applicant_from'] = trim($_GET['dsubmitted_by_applicant_from']);
                $condition .= ' AND q.date_submitted_by_applicant >= "'. changeFormatToOriginal($_GET['dsubmitted_by_applicant_from']).' 00:00:59"';
            }
        }

        if(isset($_GET['dsubmitted_by_applicant_to'])){

            if(trim($_GET['dsubmitted_by_applicant_to']) != ''){
                $_GET['dsubmitted_by_applicant_to'] = filter_var($_GET['dsubmitted_by_applicant_to'], FILTER_SANITIZE_STRING);
                $_GET['dsubmitted_by_applicant_to'] = trim($_GET['dsubmitted_by_applicant_to']);
                $condition .= ' AND q.date_submitted_by_applicant <= "'.changeFormatToOriginal($_GET['dsubmitted_by_applicant_to']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['dtpn_autorised_from'])){
            if(trim($_GET['dtpn_autorised_from']) != ''){
                $_GET['dtpn_autorised_from'] = filter_var($_GET['dtpn_autorised_from'], FILTER_SANITIZE_STRING);
                $_GET['dtpn_autorised_from'] = trim($_GET['dtpn_autorised_from']);
                $condition .= ' AND q.date_authorised >= "'. changeFormatToOriginal($_GET['dtpn_autorised_from']).'"';
            }
        }

        if(isset($_GET['dtpn_autorised_to'])){

            if(trim($_GET['dtpn_autorised_to']) != ''){
                $_GET['dtpn_autorised_to'] = filter_var($_GET['dtpn_autorised_to'], FILTER_SANITIZE_STRING);
                $_GET['dtpn_autorised_to'] = trim($_GET['dtpn_autorised_to']);
                $condition .= ' AND q.date_authorised <= "'.changeFormatToOriginal($_GET['dtpn_autorised_to']).'"';
            }
        }
        
        if(isset($_GET['ddfrom'])){
            if(trim($_GET['ddfrom']) != ''){
                $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
                $_GET['ddfrom'] = trim($_GET['ddfrom']);
                $condition .= ' AND q.departure_date >= "'. changeFormatToOriginal($_GET['ddfrom']).'"';
            }
        }

        if(isset($_GET['ddto'])){

            if(trim($_GET['ddto']) != ''){
                $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
                $_GET['ddto'] = trim($_GET['ddto']);
                $condition .= ' AND q.departure_date <= "'.changeFormatToOriginal($_GET['ddto']).'"';
            }
        }
        
//        if(isset($_GET['dsubmitted_to_embassy_from'])){
//            if(trim($_GET['dsubmitted_to_embassy_from']) != ''){
//                $_GET['dsubmitted_to_embassy_from'] = filter_var($_GET['dsubmitted_to_embassy_from'], FILTER_SANITIZE_STRING);
//                $_GET['dsubmitted_to_embassy_from'] = trim($_GET['dsubmitted_to_embassy_from']);
//                $condition .= ' AND t.departure_date >= "'. changeFormatToOriginal($_GET['dsubmitted_to_embassy_from']).'"';
//            }
//        }
//
//        if(isset($_GET['dsubmitted_to_embassy_to'])){
//
//            if(trim($_GET['dsubmitted_to_embassy_to']) != ''){
//                $_GET['dsubmitted_to_embassy_to'] = filter_var($_GET['dsubmitted_to_embassy_to'], FILTER_SANITIZE_STRING);
//                $_GET['dsubmitted_to_embassy_to'] = trim($_GET['dsubmitted_to_embassy_to']);
//                $condition .= ' AND t.departure_date <= "'.changeFormatToOriginal($_GET['dsubmitted_to_embassy_to']).'"';
//            }
//        }
        
        if(isset($_GET['dpassport_returned_to_applicant_from'])){
            if(trim($_GET['dpassport_returned_to_applicant_from']) != ''){
                $_GET['dpassport_returned_to_applicant_from'] = filter_var($_GET['dpassport_returned_to_applicant_from'], FILTER_SANITIZE_STRING);
                $_GET['dpassport_returned_to_applicant_from'] = trim($_GET['dpassport_returned_to_applicant_from']);
                $condition .= ' AND q.visa_date_order_on_route_and_closed >= "'. changeFormatToOriginal($_GET['dpassport_returned_to_applicant_from']).' 00:00:59"';
            }
        }

        if(isset($_GET['dpassport_returned_to_applicant_to'])){

            if(trim($_GET['dpassport_returned_to_applicant_to']) != ''){
                $_GET['dpassport_returned_to_applicant_to'] = filter_var($_GET['dpassport_returned_to_applicant_to'], FILTER_SANITIZE_STRING);
                $_GET['dpassport_returned_to_applicant_to'] = trim($_GET['dpassport_returned_to_applicant_to']);
                $condition .= ' AND q.visa_date_order_on_route_and_closed <= "'.changeFormatToOriginal($_GET['dpassport_returned_to_applicant_to']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND q.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        if(isset($_GET['destination'])){

            if(trim($_GET['destination']) != ''){
                $_GET['destination'] = filter_var($_GET['destination'], FILTER_SANITIZE_STRING);
                $_GET['destination'] = trim($_GET['destination']);
                $_GET['destination'] = intval($_GET['destination']);
                $condition .= ' AND q.destination_id = '.$_GET['destination'];
            }
        }
        
        if(isset($_GET['traveller_lname'])){

            if(trim($_GET['traveller_lname']) != ''){
                $_GET['traveller_lname'] = filter_var($_GET['traveller_lname'], FILTER_SANITIZE_STRING);
                $_GET['traveller_lname'] = trim($_GET['traveller_lname']);
                $condition .= " AND FIND_IN_SET('".$_GET['traveller_lname']."',q.traveller_lnames )";
            }
        }
        
        if(isset($_GET['tpn_number'])){

            if(trim($_GET['tpn_number']) != ''){
                $_GET['tpn_number'] = filter_var($_GET['tpn_number'], FILTER_SANITIZE_STRING);
                $_GET['tpn_number'] = trim($_GET['tpn_number']);
                $condition .= " AND q.tpn_no = '".$_GET['tpn_number']."'";
            }
        }
        
        if(isset($_GET['stat'])){

            if(trim($_GET['stat']) != ''){
                $_GET['stat'] = filter_var($_GET['stat'], FILTER_SANITIZE_STRING);
                $_GET['stat'] = trim($_GET['stat']);
                $_GET['stat'] = intval($_GET['stat']);
                $condition .= ' AND q.status = '.$_GET['stat'];
            }
        }

        if(isset($_GET['tpn_application_stat'])){

            if(trim($_GET['tpn_application_stat']) != ''){
                $_GET['tpn_application_stat'] = filter_var($_GET['tpn_application_stat'], FILTER_SANITIZE_STRING);
                $_GET['tpn_application_stat'] = trim($_GET['tpn_application_stat']);
                $_GET['tpn_application_stat'] = intval($_GET['tpn_application_stat']);
                $order_type = ($_GET['tpn_application_stat'] == 0) ? 2 : 3;
                $condition .= ' AND q.order_type = '.$order_type;
            }
        }
        
        if(isset($_GET['passport_type'])){

            if(trim($_GET['passport_type']) != ''){
                $_GET['passport_type'] = filter_var($_GET['passport_type'], FILTER_SANITIZE_STRING);
                $_GET['passport_type'] = trim($_GET['passport_type']);
                $_GET['passport_type'] = intval($_GET['passport_type']);
                $condition .= " AND FIND_IN_SET('".$_GET['passport_type']."',q.passport_types )";
            }
        }
        
        if(isset($_GET['tracking_number'])){

            if(trim($_GET['tracking_number']) != ''){
                $_GET['tracking_number'] = filter_var($_GET['tracking_number'], FILTER_SANITIZE_STRING);
                $_GET['tracking_number'] = trim($_GET['tracking_number']);
                $_GET['tracking_number'] = intval($_GET['tracking_number']);
                $condition .= ' AND q.order_no = '.$_GET['tracking_number'];
            }
        }


    $result = mysqli_query($con,"SELECT * FROM 
                (SELECT o.date_submitted as date_submitted_by_applicant, o.pri_dept_contact_department_id, d.name AS department, p.account_no, o.order_no, t.tpn_no, 
                    o.primary_traveller_name, t.destination as destination_id, c.country_name AS destination, t.date_last_updated, t.departure_date AS departure_date, 
                    t.status,
                    t.`date_issued` as date_authorised,
                    od.visa_date_submitted_for_processing,
                    (SELECT GROUP_CONCAT(ot.lname)  FROM tbl_order_travellers ot WHERE ot.order_no=t.order_no) AS traveller_lnames,
                    (SELECT GROUP_CONCAT(ot.passport_type)  FROM tbl_order_travellers ot WHERE ot.order_no=t.order_no) AS passport_types,
                    (SELECT GROUP_CONCAT(pt.type)  FROM tbl_order_travellers ot LEFT JOIN tbl_passport_types pt ON pt.id=ot.passport_type WHERE ot.order_no=t.order_no) AS passport_types_names,
                    o.status as order_status,
                    o.order_type,
                    od.visa_date_order_on_route_and_closed
                FROM tbl_tpn t
                LEFT JOIN tbl_order_destinations od ON (od.order_no = t.order_no AND od.country_id = t.destination)
                LEFT JOIN tbl_orders o ON o.order_no = t.order_no
                LEFT JOIN tbl_payment p ON p.order_no = o.order_no
                LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
                LEFT JOIN tbl_countries c ON c.id = t.destination
                WHERE o.status >= 10) q
            WHERE q.order_status >=10
            " . $condition);

    $row = mysqli_fetch_array($result);
    
    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    
    
    // header
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 70%">';
    $html .= '<p><b>CLS - Your Gateway to the World (Reports)</b></p>';
    $html .= '<p><span style="font-size: 13px">Date: ' . date('l j F Y') . '</span></p>';
    $html .= '</td>';
    $html .= '<td style="width:30%; text-align: right">';
    $html .= '<img src="'.$site_web.'/img/email-logo.png">';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<br/>';
    $html .= '<br/>';
    // data
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="width: 100%">';
    $html .= '<tr style="font-size: 13px;">';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Date TPN submitted</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 80px;">Date TPN authorised</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Departure date of applicant</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Date application submitted by applicant</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Date passport is returned to applicant</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 90px;">Department or Agency</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 80px;">Country destination</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 90px;">TPN number</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">TPN status</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">TPN application status</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Travellers</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 60px;">Passport type</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 60px;">Tracking number</th>';
    $html .= '</tr>';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $tpn_status = $row['order_type']==2 ? 'TPN':'TPN+Visa';
        $status = '';
        if($row['status']==0){
            $status = 'Pending';
        }elseif($row['status']==1){
            $status = 'Approved';
        }else{
            $status = 'Rejected';
        }
        
        $travellers = str_replace(',','<br/>',$row['traveller_lnames']);
        $pass_type = str_replace(',','<br/>',$row['passport_types_names']);
        $html .= '<tr style="font-size: 13px;">';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 50px;">'. $row['date_submitted_by_applicant'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 50px;">'. $row['date_authorised'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 50px;" >'. $row['departure_date'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 50px;">'. $row['date_submitted_by_applicant'].'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 50px;">'. $row['visa_date_order_on_route_and_closed'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 60px;">'. $row['department'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 60px;">'. $row['destination'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 90px;" >'. $row['tpn_no'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 60px;">'. $status.'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 60px;">'. $tpn_status .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 60px;">'. $travellers .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 60px;">'. $pass_type .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 60px;" >'. $row['order_no'] .'</td>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    $html .='</page>';
    
//    print_r($_SERVER['DOCUMENT_ROOT'].'web/img/email-logo.png');
//    exit();
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));

    $html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array(13, 18, 13, 18));
    $html2pdf->pdf->SetTitle('Embassy Delivery');
    $html2pdf->pdf->SetAuthor('Voodoo Creative');

    $html2pdf->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $html2pdf->WriteHTML($html);
    $html2pdf->Output('CLS - Your Gateway to the World (Reports)'.".pdf"); //output to file
    
    

function changeFormatToOriginal($stringdate){
        $arrdate = explode(" ",$stringdate);
        if(isset($arrdate[1])){
            if(trim($arrdate[0]) != '' && substr_count($arrdate[0], "-") > 1){
                list($d, $m , $y) = explode("-", $arrdate[0]);
                $time = (isset($arrdate[1])) ? $arrdate[1] : '';
                return $y.'-'.$m.'-'.$d . ' ' . $time;
            }
        }else{
            if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
                list($d, $m , $y) = explode("-", $stringdate);
                return $y.'-'.$m.'-'.$d;
            }
        }
    }
    
    
    
function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}