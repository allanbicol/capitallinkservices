<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();
    
    $site_web = ($_SERVER['SERVER_NAME'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] ."/cls/web" : $_SERVER['DOCUMENT_ROOT'] ."/web";

    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
    }else{
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices")
        or die('not connected');
        
    }

    
     $condition = '';

    // date from filter
    if(isset($_GET['from'])){
        if(trim($_GET['from']) != ''){
            $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
            $_GET['from'] = trim($_GET['from']);
            $condition .= ' AND od.visa_date_submitted_for_processing >= "'. changeFormatToOriginal($_GET['from']).'"';
        }
    }

    if(isset($_GET['to'])){

        if(trim($_GET['to']) != ''){
            $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
            $_GET['to'] = trim($_GET['to']);
            $condition .= ' AND od.visa_date_submitted_for_processing <= "'.changeFormatToOriginal($_GET['to']).'"';
        }
    }


    $result = mysqli_query($con,"
        SELECT 
            COUNT(od.id) as items,
            od.country_id,
            c.country_name, 
            od.visa_date_submitted_for_processing ,
            '' as receiver_name,
            '' as receiver_signature,
            '' as notes
        FROM tbl_order_destinations od
        LEFT JOIN tbl_countries c ON c.id = od.country_id
        WHERE od.visa_date_submitted_for_processing != ''
        " . $condition . "
        GROUP BY od.country_id
        ");

    $row = mysqli_fetch_array($result);
    
    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    
    
    // header
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 70%">';
    $html .= '<p><b>CLS - Your Gateway to the World (Embassy Delivery Orders Report)</b></p>';
    $html .= '<p><span style="font-size: 13px">Date: ' . date('l j F Y') . '</span></p>';
    $html .= '</td>';
    $html .= '<td style="width:30%; text-align: right">';
    $html .= '<img src="'.$site_web.'/img/email-logo.png">';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<br/>';
    $html .= '<br/>';
    // data
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="width: 100%">';
    $html .= '<tr style="font-size: 13px;">';
    $html .= '<th style="border-bottom: 1px solid black; width: 120px;">Items</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 150px;">Embassy</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 140px;">The Receiver Name</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 150px;">Receivers Signature</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 130px;">Notes</th>';
    $html .= '</tr>';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $html .= '<tr style="font-size: 13px;">';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 120px;">'. $row['items'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 220px;">'. $row['country_name'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 200px;" >'. $row['receiver_name'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 230px;">'. $row['receiver_signature'].'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 220px;">'. $row['notes'] .'</td>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    $html .='</page>';
    
//    print_r($_SERVER['DOCUMENT_ROOT'].'web/img/email-logo.png');
//    exit();
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));

    $html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array(13, 18, 13, 18));
    $html2pdf->pdf->SetTitle('Embassy Delivery');
    $html2pdf->pdf->SetAuthor('Voodoo Creative');

    $html2pdf->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $html2pdf->WriteHTML($html);
    $html2pdf->Output('CLS - Your Gateway to the World (Embassy Delivery Orders Report)'.".pdf"); //output to file
    
    

function changeFormatToOriginal($stringdate){
        $arrdate = explode(" ",$stringdate);
        if(isset($arrdate[1])){
            if(trim($arrdate[0]) != '' && substr_count($arrdate[0], "-") > 1){
                list($d, $m , $y) = explode("-", $arrdate[0]);
                $time = (isset($arrdate[1])) ? $arrdate[1] : '';
                return $y.'-'.$m.'-'.$d . ' ' . $time;
            }
        }else{
            if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
                list($d, $m , $y) = explode("-", $stringdate);
                return $y.'-'.$m.'-'.$d;
            }
        }
    }
    
    
    
function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}