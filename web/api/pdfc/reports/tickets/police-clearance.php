<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();

$site_web = ($_SERVER['SERVER_NAME'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] ."/capitallinkservices.com.au/web" : $_SERVER['DOCUMENT_ROOT'] ."/web";
if(isset($_SESSION['_sf2_attributes']['user_type'])){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
    }else{
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    }
    $condition = '';
    $voptions = '';
    // date from filter
    if(isset($_GET['from'])){
        if(trim($_GET['from']) != ''){
            $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
            $_GET['from'] = trim($_GET['from']);
            $condition .= ' AND o.date_submitted >= "'. changeFormatToOriginal($_GET['from']).' 00:00:59"';
        }
    }

    if(isset($_GET['to'])){

        if(trim($_GET['to']) != ''){
            $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
            $_GET['to'] = trim($_GET['to']);
            $condition .= ' AND o.date_submitted <= "'.changeFormatToOriginal($_GET['to']).' 23:59:59"';
        }
    }


    if(isset($_GET['paymentstat'])){

        if(trim($_GET['paymentstat']) != ''){
            $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
            $_GET['paymentstat'] = trim($_GET['paymentstat']);
            $_GET['paymentstat'] = intval($_GET['paymentstat']);
            $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
        }
    }

    if(isset($_GET['onaccount'])){

        if(trim($_GET['onaccount']) != ''){
            $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
            $_GET['onaccount'] = trim($_GET['onaccount']);
            $_GET['onaccount'] = intval($_GET['onaccount']);
            $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
        }
    }

    if(isset($_GET['accountno'])){

        if(trim($_GET['accountno']) != ''){
            $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
            $_GET['accountno'] = trim($_GET['accountno']);
            $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
        }
    }

    if(isset($_GET['clsteammem'])){

        if(trim($_GET['clsteammem']) != ''){
            $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
            $_GET['clsteammem'] = trim($_GET['clsteammem']);
            $_GET['clsteammem'] = intval($_GET['clsteammem']);
            $condition .= ' AND au.id = '.$_GET['clsteammem'];
        }
    }
        
    $result = mysqli_query($con,"SELECT DATE_FORMAT(o.date_submitted,'%d-%m-%Y %H:%i:%s') as date_submitted, p.account_no, o.order_no, o.order_type,
                (Select group_concat(concat(concat(opa.fname,' '), opa.lname))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicants,
                (Select group_concat(opa.passport_no)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as passports,
                (Select group_concat(DATE_FORMAT(opa.departure_date,'%d-%m-%Y'))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as departure_dates,
                 o.status, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 5
            " . $condition);
    
    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    
    
    // header
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 70%">';
    $html .= '<p><b>Tickets Report - Police Clearancde</b></p>';
    //$html .= '<p><span style="font-size: 12px">Department: '.$department.'</span><br/><span style="font-size: 12px">Date: ' . date('l j F Y') . '</span></p>';
    $html .= '</td>';
    $html .= '<td style="width:30%; text-align: right">';
    $html .= '<img src="'.$site_web.'/img/email-logo.png">';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<br/>';
    $html .= '<br/>';
    // data
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="width: 100%">';
    $html .= '<tr style="font-size: 12px;">';
    $html .= '<th style="border-bottom: 1px solid black; width: 130px;">Date Submitted</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 80px;">Account No.</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 70px;">Order No.</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 100px;">Applicant Name</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Passport No.</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Departure Date</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Order Status</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">CLS Team Member</th>';
    
    $html .= '</tr>';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $order_status = '';
        if($row['status'] == 10){
            $order_status = 'Pending';
        }elseif($row['status'] == 11){
            $order_status = 'Paid';
        }elseif($row['status'] == 12){
            $order_status = 'Completed';
        }
        $html .= '<tr style="font-size: 12px;">';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 130px;">'. $row['date_submitted'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;">'. $row['account_no'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 70px;">'. $row['order_no'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 100px;" >'. str_replace(",", "<br/>", $row['applicants'])  .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;" >'. str_replace(",", "<br/>", $row['passports'])  .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;">'. str_replace(",", "<br/>", $row['departure_dates']) .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;">'. $order_status .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;" >'. $row['cls_team_mem'] .'</td>';
            
        $html .= '</tr>';
    }
    $html .= '</table>';
    $html .='</page>';
    require_once(dirname(__FILE__).'/../../html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    //$html2pdf->addFont('Tahoma');
    
    
    $html2pdf->WriteHTML($html);
    $html2pdf->Output("police-clearance-tickets-report.pdf"); //output to file
}


function changeFormatToOriginal($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($d, $m , $y) = explode("-", $stringdate);
        return $y.'-'.$m.'-'.$d;
    }
}

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}

function validateDate($date)
{
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
        return true;
    }else{
        return false;
    }
}