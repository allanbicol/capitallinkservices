<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();

$site_web = ($_SERVER['SERVER_NAME'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] ."/capitallinkservices.com.au/web" : $_SERVER['DOCUMENT_ROOT'] ."/web";
if(isset($_SESSION['_sf2_attributes']['user_type'])){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
    }else{
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    }
    $condition = '';
    $voptions = '';
    // date from filter
    if(isset($_GET['from'])){
        if(trim($_GET['from']) != ''){
            $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
            $_GET['from'] = trim($_GET['from']);
            $condition .= ' AND o.date_submitted >= "'. changeFormatToOriginal($_GET['from']).' 00:00:59"';
        }
    }

    if(isset($_GET['to'])){

        if(trim($_GET['to']) != ''){
            $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
            $_GET['to'] = trim($_GET['to']);
            $condition .= ' AND o.date_submitted <= "'.changeFormatToOriginal($_GET['to']).' 23:59:59"';
        }
    }

    if(isset($_GET['ddfrom'])){
        if(trim($_GET['ddfrom']) != ''){
            $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
            $_GET['ddfrom'] = trim($_GET['ddfrom']);
            $condition .= ' AND od.departure_date >= "'. changeFormatToOriginal($_GET['ddfrom']).' 00:00:59"';
        }
    }

    if(isset($_GET['ddto'])){

        if(trim($_GET['ddto']) != ''){
            $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
            $_GET['ddto'] = trim($_GET['ddto']);
            $condition .= ' AND od.departure_date <= "'.changeFormatToOriginal($_GET['ddto']).' 23:59:59"';
        }
    }

    if(isset($_GET['department'])){

        if(trim($_GET['department']) != ''){
            $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
            $_GET['department'] = trim($_GET['department']);
            $_GET['department'] = intval($_GET['department']);
            $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
        }
    }

    if(isset($_GET['destination'])){

        if(trim($_GET['destination']) != ''){
            $_GET['destination'] = filter_var($_GET['destination'], FILTER_SANITIZE_STRING);
            $_GET['destination'] = trim($_GET['destination']);
            $_GET['destination'] = intval($_GET['destination']);
            $condition .= ' AND t.destination = '.$_GET['destination'];
        }
    }

    if(isset($_GET['paymentstat'])){

        if(trim($_GET['paymentstat']) != ''){
            $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
            $_GET['paymentstat'] = trim($_GET['paymentstat']);
            $_GET['paymentstat'] = intval($_GET['paymentstat']);
            $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
        }
    }

    if(isset($_GET['onaccount'])){

        if(trim($_GET['onaccount']) != ''){
            $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
            $_GET['onaccount'] = trim($_GET['onaccount']);
            $_GET['onaccount'] = intval($_GET['onaccount']);
            $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
        }
    }

    if(isset($_GET['accountno'])){

        if(trim($_GET['accountno']) != ''){
            $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
            $_GET['accountno'] = trim($_GET['accountno']);
            $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
        }
    }
        
    $result = mysqli_query($con,"SELECT o.date_submitted, d.name as department, p.account_no, o.order_no, t.tpn_no, 
                o.primary_traveller_name, c.country_name, DATE_FORMAT(t.departure_date,'%d-%m-%Y') as departure_date, t.status,
                t.tpn_no, p.s_paid
            FROM tbl_tpn t
            LEFT JOIN tbl_orders o ON o.order_no = t.order_no
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = t.destination
            WHERE o.status >= 10 AND (o.order_type = 2 OR o.order_type = 3)
            " . $condition . " GROUP BY t.tpn_no");
    
    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    
    
    // header
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 70%">';
    $html .= '<p><b>Tickets Report - TPN</b></p>';
    //$html .= '<p><span style="font-size: 12px">Department: '.$department.'</span><br/><span style="font-size: 12px">Date: ' . date('l j F Y') . '</span></p>';
    $html .= '</td>';
    $html .= '<td style="width:30%; text-align: right">';
    $html .= '<img src="'.$site_web.'/img/email-logo.png">';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<br/>';
    $html .= '<br/>';
    // data
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="width: 100%">';
    $html .= '<tr style="font-size: 12px;">';
    $html .= '<th style="border-bottom: 1px solid black; width: 130px;">Date Submitted</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 150px;">Organisation</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 80px;">Account No.</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 70px;">Order No.</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 90px;">TPN No.</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 150px;">Traveller Name</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 90px;">Destination</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Departure Date</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Status</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 80px;">Payment Status</th>';
    $html .= '</tr>';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $order_status = $row['status'];
        if($row['status'] == 0){
            $order_status = 'Pending';
        }elseif($row['status'] == 1){
            $order_status = 'Accepted';
        }elseif($row['status'] == 2){
            $order_status = 'Rejected';
        }
        $payment_status = '';
        if($row['s_paid'] == 1){
            $payment_status = 'Paid online';
        }else{
            $payment_status = 'Paid By account';
        }
        $html .= '<tr style="font-size: 12px;">';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 130px;">'. $row['date_submitted'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 150px;">'. $row['department'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;">'. $row['account_no'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 70px;">'. $row['order_no'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 90px;">'. $row['tpn_no'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 150px;" >'. str_replace(",", "<br/>", $row['primary_traveller_name'])  .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 90px;">'. $row['country_name'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;" >'. $row['departure_date'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;">'. $order_status .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;">'. $payment_status .'</td>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    $html .='</page>';
    require_once(dirname(__FILE__).'/../../html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));
    $html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    //$html2pdf->addFont('Tahoma');
    
    
    $html2pdf->WriteHTML($html);
    $html2pdf->Output("tpn-tickets-report.pdf"); //output to file
}


function changeFormatToOriginal($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($d, $m , $y) = explode("-", $stringdate);
        return $y.'-'.$m.'-'.$d;
    }
}

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}

function validateDate($date)
{
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
        return true;
    }else{
        return false;
    }
}