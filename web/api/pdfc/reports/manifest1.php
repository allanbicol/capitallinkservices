<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();

$site_web = ($_SERVER['SERVER_NAME'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] ."/capitallinkservices.com.au/web" : $_SERVER['DOCUMENT_ROOT'] ."/web";
if(isset($_SESSION['_sf2_attributes']['user_type'])){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
    }else{
        //$con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    }
    $condition = '';
    $voptions = '';
    // date from filter
    // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND od.visa_date_completed_and_received_at_cls >= "'. changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND od.visa_date_completed_and_received_at_cls <= "'.changeFormatToOriginal($_GET['to']).' 53:23:59"';
            }
        }
        
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }
        
        if(isset($_GET['vvisa']) && $_GET['vvisa'] == 1){
            if(trim($voptions) != ''){
                $voptions .= ' OR ';
            }
            $voptions .= ' o.order_type = 1 OR o.order_type = 3 ';
        }
        
        if(isset($_GET['vtpn']) && $_GET['vtpn'] == 1){
            if(trim($voptions) != ''){
                $voptions .= ' OR ';
            }
            $voptions .= ' o.order_type = 2 OR o.order_type = 3 ';
        }
        
        if(isset($_GET['vpvisa']) && $_GET['vpvisa'] == 1){
            if(trim($voptions) != ''){
                $voptions .= ' OR ';
            }
            $voptions .= ' o.order_type = 6 ';
        }
        
        if(trim($voptions) == ''){
            $voptions = 'o.order_type = 1 OR o.order_type = 2 OR o.order_type = 3 OR o.order_type = 6';
        }
        
    $result = mysqli_query($con,"SELECT CONCAT(CONCAT(uc.fname, ' '), uc.lname) as client_name, 
            DATE_FORMAT(o.date_submitted,'%d-%m-%Y') as date_submitted, 
            d.name as department, 
            p.account_no, 
            o.order_no, 
            o.status, 
            c.country_name,
            DATE_FORMAT(o.date_completed,'%d-%m-%Y') as date_completed, 
            (SELECT GROUP_CONCAT(CONCAT(opa.fname,' ',opa.lname))  FROM tbl_order_travellers opa WHERE opa.order_no=o.order_no) AS primary_traveller_name,
            (SELECT GROUP_CONCAT(opa.passport_number) FROM tbl_order_travellers opa WHERE opa.order_no=o.order_no) AS primary_traveller_passport_no,
            vt.type as visa_type, od.departure_date, p.s_paid, CONCAT(CONCAT(au.fname, ' '), au.lname) as team_mem,
            od.id, DATE_FORMAT(od.visa_date_completed_and_received_at_cls,'%d-%m-%Y') as visa_date_completed_and_received_at_cls
        FROM tbl_order_destinations od
        LEFT JOIN tbl_orders o ON o.order_no = od.order_no
        LEFT JOIN tbl_payment p ON p.order_no = o.order_no
        LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
        LEFT JOIN tbl_countries c ON c.id = od.country_id
        LEFT JOIN tbl_visa_types vt ON vt.id = od.selected_visa_type
        LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
        LEFT JOIN tbl_user_client uc ON uc.id = o.client_id
        WHERE o.status >= 10 AND (". $voptions .")
            AND od.visa_date_completed_and_received_at_cls != ''
        " . $condition);
    
    //$row = mysqli_fetch_array($result);

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    
    
    // header
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 70%">';
    $html .= '<p><b>Manifest Report:</b></p>';
    $html .= '<p><span style="font-size: 12px">Date: ' . date('l j F Y') . '</span></p>';
    $html .= '</td>';
    $html .= '<td style="width:30%; text-align: right">';
    $html .= '<img src="'.$site_web.'/img/email-logo.png">';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<br/>';
    $html .= '<br/>';
    // data
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="width: 100%">';
    $html .= '<tr style="font-size: 12px;">';
    $html .= '<th style="border-bottom: 1px solid black; width: 120px;">Client Name</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 100px;">Date Submitted</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 100px;">Date Completed & Received at CLS</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 130px;">Traveller Name</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 120px;">Passport No.</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 120px;">Destination</th>';
    $html .= '</tr>';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $html .= '<tr style="font-size: 12px;">';
            $date_completed = (validateDate($row['visa_date_completed_and_received_at_cls'])) ? $row['visa_date_completed_and_received_at_cls'] : "";
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 120px;">'. $row['client_name'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 100px;">'. $row['date_submitted'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 100px;" >'. $row['visa_date_completed_and_received_at_cls'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 130px;">'. str_replace(",", "<br/>", $row['primary_traveller_name']) .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 120px;">'. str_replace(",", "<br/>", $row['primary_traveller_passport_no']) .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 120px;" >'. $row['country_name'] .'</td>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    $html .='</page>';
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    //$html2pdf->addFont('Tahoma');
    
    
    $html2pdf->WriteHTML($html);
    $html2pdf->Output("manifest_report.pdf"); //output to file
}


function changeFormatToOriginal($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($d, $m , $y) = explode("-", $stringdate);
        return $y.'-'.$m.'-'.$d;
    }
}

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}

function validateDate($date)
{
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
        return true;
    }else{
        return false;
    }
}