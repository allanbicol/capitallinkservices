<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();

$site_web = ($_SERVER['SERVER_NAME'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] ."/capitallinkservices.com.au/web" : $_SERVER['DOCUMENT_ROOT'] ."/web";
if(isset($_SESSION['_sf2_attributes']['user_type'])){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
    }else{
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    }
    $condition = '';
    $voptions = '';
    // date from filter
    if(isset($_GET['from'])){
        if(trim($_GET['from']) != ''){
            $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
            $_GET['from'] = trim($_GET['from']);
            $condition .= ' AND o.date_submitted >= "'. changeFormatToOriginal($_GET['from']).' 00:00:59"';
        }
    }

    if(isset($_GET['to'])){

        if(trim($_GET['to']) != ''){
            $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
            $_GET['to'] = trim($_GET['to']);
            $condition .= ' AND o.date_submitted <= "'.changeFormatToOriginal($_GET['to']).' 23:59:59"';
        }
    }


    if(isset($_GET['paymentstat'])){

        if(trim($_GET['paymentstat']) != ''){
            $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
            $_GET['paymentstat'] = trim($_GET['paymentstat']);
            $_GET['paymentstat'] = intval($_GET['paymentstat']);
            $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
        }
    }

    if(isset($_GET['orderstat'])){

        if(trim($_GET['orderstat']) != ''){
            $_GET['orderstat'] = filter_var($_GET['orderstat'], FILTER_SANITIZE_STRING);
            $_GET['orderstat'] = trim($_GET['orderstat']);
            $_GET['orderstat'] = intval($_GET['orderstat']);
            $condition .= ' AND o.status = '.$_GET['orderstat'];
        }
    }
        
    $result = mysqli_query($con,"SELECT DATE_FORMAT(o.date_submitted,'%d-%m-%Y %H:%i:%s') as date_submitted, o.order_no, CONCAT(CONCAT(uc.fname, ' '), uc.lname) as applicant, o.status,
                p.s_paid, c.country_name
            FROM tbl_orders o
            LEFT JOIN tbl_countries c ON c.id = o.dl_embassy
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_client uc ON uc.id = o.client_id
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 9
            " . $condition);
    
    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    
    
    // header
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 70%">';
    $html .= '<p><b>Order Report - Document Legalisation</b></p>';
    //$html .= '<p><span style="font-size: 12px">Department: '.$department.'</span><br/><span style="font-size: 12px">Date: ' . date('l j F Y') . '</span></p>';
    $html .= '</td>';
    $html .= '<td style="width:30%; text-align: right">';
    $html .= '<img src="'.$site_web.'/img/email-logo.png">';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<br/>';
    $html .= '<br/>';
    // data
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="width: 100%">';
    $html .= '<tr style="font-size: 12px;">';
    $html .= '<th style="border-bottom: 1px solid black; width: 130px;">Date Submitted</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 80px;">Order No.</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 120px;">Customer Name</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 110px;">Embassy</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 140px;">Payment Status</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 100px;">Order Status</th>';
    $html .= '</tr>';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $order_status = '';
        if($row['status'] == 10){
            $order_status = 'Pending';
        }elseif($row['status'] == 11){
            $order_status = 'Paid';
        }elseif($row['status'] == 12){
            $order_status = 'Completed';
        }
        $payment_status = '';
        if($row['s_paid'] == 1){
            $payment_status = 'Paid online';
        }else{
            $payment_status = 'Paid By account';
        }
        $html .= '<tr style="font-size: 12px;">';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 130px;">'. $row['date_submitted'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 80px;">'. $row['order_no'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 120px;">'. $row['applicant'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 110px;">'. $row['country_name'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 140px;">'. $payment_status.'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 100px;">'. $order_status .'</td>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    $html .='</page>';
    require_once(dirname(__FILE__).'/../../html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    //$html2pdf->addFont('Tahoma');
    
    
    $html2pdf->WriteHTML($html);
    $html2pdf->Output("document-legalisation-order-report.pdf"); //output to file
}


function changeFormatToOriginal($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($d, $m , $y) = explode("-", $stringdate);
        return $y.'-'.$m.'-'.$d;
    }
}

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}

function validateDate($date)
{
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
        return true;
    }else{
        return false;
    }
}