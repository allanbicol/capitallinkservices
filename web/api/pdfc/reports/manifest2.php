<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();

$site_web = ($_SERVER['SERVER_NAME'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] ."/cls/web" : $_SERVER['DOCUMENT_ROOT'] ."/web";
if(isset($_SESSION['_sf2_attributes']['user_type'])){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","root","cls");
    }else{
//        $con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    }
    $condition = '';
    
    // date from filter
    if(isset($_GET['from'])){
        if(trim($_GET['from']) != ''){
            $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
            $_GET['from'] = trim($_GET['from']);
            $condition .= ' AND o.date_doc_sent >= "'. changeFormatToOriginal($_GET['from']).' 00:00:59"';
        }
    }

    if(isset($_GET['to'])){

        if(trim($_GET['to']) != ''){
            $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
            $_GET['to'] = trim($_GET['to']);
            $condition .= ' AND o.date_doc_sent <= "'.changeFormatToOriginal($_GET['to']).' 23:59:59"';
        }
    }


    if(isset($_GET['department'])){

        if(trim($_GET['department']) != ''){
            $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
            $_GET['department'] = trim($_GET['department']);
            $_GET['department'] = intval($_GET['department']);
            $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
        }
    }


    if(isset($_GET['accountno'])){

        if(trim($_GET['accountno']) != ''){
            $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
            $_GET['accountno'] = trim($_GET['accountno']);
            $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
        }
    }
        
    $result = mysqli_query($con,"SELECT CONCAT(CONCAT(uc.fname, ' '), uc.lname) as client_name, 
                DATE_FORMAT(o.date_submitted,'%d-%m-%Y') as date_submitted, 
                DATE_FORMAT(o.date_doc_sent,'%d-%m-%Y') as date_doc_sent,
                DATE_FORMAT(o.date_completed,'%d-%m-%Y') as date_completed,
                CAST((SELECT IF(GROUP_CONCAT(opa.fullname) IS NULL,'',GROUP_CONCAT(opa.fullname SEPARATOR ',') ) AS applicants FROM tbl_order_passport_applicants opa WHERE opa.order_no=o.order_no) AS CHAR(10000) CHARACTER SET utf8) AS applicants,
                (Select 
                    group_concat(opa.personal_passport,opa.diplomatic_official_passport,opa.birth_certificate, opa.marriage_certificate, opa.certificate_of_australian_citizenship)
                    from tbl_order_passport_applicants opa 
                    where opa.order_no=o.order_no
                ) as doc_types,
                p.account_no, o.order_no
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            LEFT JOIN tbl_user_client uc ON uc.id = o.client_id
            WHERE o.status >= 10 AND o.order_type = 4
                AND o.s_doc_sent = 1 
            " . $condition);
    
    //$row = mysqli_fetch_array($result);

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    
    
    // header
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 70%">';
    $html .= '<p><b>Manifest Report:</b></p>';
    $html .= '<p><span style="font-size: 13px">Date: ' . date('l j F Y') . '</span></p>';
    $html .= '</td>';
    $html .= '<td style="width:30%; text-align: right">';
    $html .= '<img src="'.$site_web.'/img/email-logo.png">';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<br/>';
    $html .= '<br/>';
    // data
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="width: 100%">';
    $html .= '<tr style="font-size: 13px;">';
    $html .= '<th style="border-bottom: 1px solid black; width: 120px;">Client Name</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 100px;">Date Submitted</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 100px;">Date Sent</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 130px;">Applicant(s)</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 120px;">Document Types</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 120px;">Account No.</th>';
    $html .= '</tr>';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $html .= '<tr style="font-size: 13px;">';
            $date_doc_sent = (validateDate($row['date_doc_sent'])) ? $row['date_doc_sent'] : "";
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 120px;">'. $row['client_name'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 100px;">'. $row['date_submitted'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 100px;" >'. $date_doc_sent .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 130px;">'. str_replace(",",",<br/>",$row['applicants']) .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 120px;">'. str_replace(",",",<br/>",$row['doc_types']) .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 120px;" >'. $row['account_no'] .'</td>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    $html .='</page>';
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    //$html2pdf->addFont('Tahoma');
    
    
    $html2pdf->WriteHTML($html);
    $html2pdf->Output("manifest_report.pdf"); //output to file
}


function changeFormatToOriginal($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($d, $m , $y) = explode("-", $stringdate);
        return $y.'-'.$m.'-'.$d;
    }
}

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}

function validateDate($date)
{
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
        return true;
    }else{
        return false;
    }
}