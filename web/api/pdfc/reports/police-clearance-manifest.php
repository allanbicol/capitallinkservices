<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();

$site_web = ($_SERVER['SERVER_NAME'] == 'localhost') ? $_SERVER['DOCUMENT_ROOT'] ."/cls/web" : $_SERVER['DOCUMENT_ROOT'] ."/web";
if(isset($_SESSION['_sf2_attributes']['user_type'])){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","root","cls");
    }else{
//        $con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    }
    $condition = '';
    // date from filter
    if(isset($_GET['from'])){
        if(trim($_GET['from']) != ''){
            $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
            $_GET['from'] = trim($_GET['from']);
            $condition .= ' AND o.date_submitted >= "'. changeFormatToOriginal($_GET['from']).'"';
        }
    }

    if(isset($_GET['to'])){

        if(trim($_GET['to']) != ''){
            $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
            $_GET['to'] = trim($_GET['to']);
            $condition .= ' AND o.date_submitted <= "'.changeFormatToOriginal($_GET['to']).'"';
        }
    }


    if(isset($_GET['paymentstat'])){

        if(trim($_GET['paymentstat']) != ''){
            $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
            $_GET['paymentstat'] = trim($_GET['paymentstat']);
            $_GET['paymentstat'] = intval($_GET['paymentstat']);
            $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
        }
    }

    if(isset($_GET['onaccount'])){

        if(trim($_GET['onaccount']) != ''){
            $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
            $_GET['onaccount'] = trim($_GET['onaccount']);
            $_GET['onaccount'] = intval($_GET['onaccount']);
            $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
        }
    }

    if(isset($_GET['accountno'])){

        if(trim($_GET['accountno']) != ''){
            $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
            $_GET['accountno'] = trim($_GET['accountno']);
            $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
        }
    }

    if(isset($_GET['clsteammem'])){

        if(trim($_GET['clsteammem']) != ''){
            $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
            $_GET['clsteammem'] = trim($_GET['clsteammem']);
            $_GET['clsteammem'] = intval($_GET['clsteammem']);
            $condition .= ' AND au.id = '.$_GET['clsteammem'];
        }
    }

    if(isset($_GET['clearance'])){

        if(trim($_GET['clearance']) != ''){
            $_GET['clearance'] = filter_var($_GET['clearance'], FILTER_SANITIZE_STRING);
            $_GET['clearance'] = trim($_GET['clearance']);
            $_GET['clearance'] = intval($_GET['clearance']);
            $condition .= ' AND o.police_clearance_id = '.$_GET['clearance'];
            
            $police_clearance = mysqli_query($con,"SELECT name FROM tbl_police_clearances WHERE id = ". $_GET['clearance']);
            while ($pc = mysqli_fetch_array($police_clearance, MYSQLI_ASSOC)) {
                $police_clearance_name = $pc['name'];
            }
            
        }
    }
    
        
    $result = mysqli_query($con," SELECT o.date_submitted, p.account_no, o.order_no, o.order_type,
                (Select group_concat(opa.fname)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicant_fname,
                (Select group_concat(opa.lname)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicant_lname,
                (Select group_concat(opa.passport_no)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as passports,
                (Select group_concat(DATE_FORMAT(opa.departure_date,'%d-%m-%Y'))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as departure_dates,
                 o.status, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem,
                 '460717338' as reference_out_to_sa, '8702120054088' as clearance_no, pc.name as clerance_name
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            LEFT JOIN tbl_police_clearances pc ON pc.id = o.police_clearance_id
            WHERE o.status >= 10 AND o.order_type = 5 
                AND (o.police_clearance_date_cls_received_all_items != '0000-00-00' AND o.police_clearance_date_cls_received_all_items != '' AND o.police_clearance_date_cls_received_all_items IS NOT NULL )
        " . $condition);
    
    //$row = mysqli_fetch_array($result);

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    
    
    // header
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 70%">';
    if(isset($police_clearance_name)){
        $html .= '<h2>Police Clearance Manifest Report<br>'.$police_clearance_name.'</h2>';
    }else{
        $html .= '<h2>Police Clearance Manifest Report</h2>';
    }
    //$html .= '<p><span style="font-size: 12px">Date: ' . date('l j F Y') . '</span></p>';
    $html .= '</td>';
    $html .= '<td style="width:30%; text-align: right">';
    $html .= '<img src="'.$site_web.'/img/email-logo.png">';
    $html .= '</td>';
    $html .= '</tr></table>';
    
    $html .= '<br/>';
    $html .= '<br/>';
    // data
    $html .= '<table border="0" cellpading="0" cellspacing="0" style="width: 100%">';
    $html .= '<tr style="font-size: 12px;">';
    $html .= '<th style="border-bottom: 1px solid black; width: 130px;">Order No.</th>';
    $html .= '<th style="border-bottom: 1px solid black ;width: 140px;">First Name</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 140px;">Surname</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 140px;">Reference out to SA</th>';
    $html .= '<th style="border-bottom: 1px solid black; width: 140px;">Clearance No.</th>';
    $html .= '</tr>';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $html .= '<tr style="font-size: 12px;">';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 130px;">'. $row['order_no'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 140px;">'. $row['applicant_fname'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 140px;" >'. $row['applicant_lname'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 140px;">'. $row['reference_out_to_sa'] .'</td>';
            $html .= '<td style="padding-top: 5px; padding-bottom: 5px; border-bottom: 1px solid #a4a3a3; width: 140px;">'. $row['clearance_no'] .'</td>';
        $html .= '</tr>';
    }
    $html .= '</table>';
    $html .='</page>';
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    //$html2pdf->addFont('Tahoma');
    
    
    $html2pdf->WriteHTML($html);
    $html2pdf->Output("police_clearance_manifest_report.pdf"); //output to file
}


function changeFormatToOriginal($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($d, $m , $y) = explode("-", $stringdate);
        return $y.'-'.$m.'-'.$d;
    }
}

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}

function validateDate($date)
{
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
    {
        return true;
    }else{
        return false;
    }
}