<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();
function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}
function ausDateFormat($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($y, $m , $d) = explode("-", $stringdate);
        return $d.'/'.$m.'/'.$y;
    }
}

if(isset($_SESSION['_sf2_attributes']['user_type']) && ($_SESSION['_sf2_attributes']['user_type'] == 'admin-user' || $_SESSION['_sf2_attributes']['user_type'] == 'public' || $_SESSION['_sf2_attributes']['user_type'] == 'corporate')){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
        $img_url = siteURL().'/capitallinkservices.com.au/web/img';
        $asset_url = siteURL().'/capitallinkservices.com.au/web';
    }else{
        if(php_uname("n") == 'cls-testing'){
            $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdevtest","dreNca8gW4","capitallinkservices_test");
            $img_url = siteURL().'/web/img';
            $asset_url = siteURL().'/web';
        }else{
            $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
            $img_url = siteURL().'/web/img';
            $asset_url = siteURL().'/web';
        }
    }

    $_GET['order'] = filter_var($_GET['order'], FILTER_SANITIZE_STRING);
    $_GET['order'] = intval($_GET['order']);
    
    
    $query = mysqli_query($con,"SELECT o.order_type, o.visa_mdd_company, o.visa_mdd_fname, o.visa_mdd_lname, o.visa_mdd_contact, o.visa_mdd_address, 
            o.visa_mdd_city, o.visa_mdd_state, o.visa_mdd_postcode, o.visa_mdd_contact, p.fname, p.lname, p.phone, p.address, p.city, p.postcode,
            o.destination, c.embassy_address_line1, c.embassy_address_line2, c.embassy_street, c.embassy_city, c.embassy_state, 
            c.embassy_postcode, c.embassy_phone, o.`primary_traveller_name`, c.country_name, c.rep_name, o.dl_contact_name,
            o.dl_company, o.dl_address, o.dl_city, o.dl_state, o.dl_postcode
        FROM tbl_orders o
        LEFT JOIN tbl_payment p ON p.order_no = o.order_no
        LEFT JOIN tbl_countries c ON (o.order_type != 9 AND c.id = o.destination) OR (o.order_type = 9 AND c.id = o.dl_embassy)
        WHERE o.order_no=".$_GET['order']);
    $order = mysqli_fetch_array($query);
    
    //Select * from tbl_orders where order_type=7 and order_no=10003908

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page>';
    
    
    
    $html .= '<div style="font-size: 11px; color: #2e2a2b">';
    $html .= '<table border="0" cellpadding="0" cellspacing="0">';
    $html .= '<tr>';
    
    // COLUMN 1
    $html .= '<td style="width: 46%; ">';
        $html .= '<table border="0" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td style="width: 35%;">';
            $html .= '<img src="'.$asset_url.'/img/cls-logo1.png" style="width: 100%;">';
        $html .= '</td>';
        $html .= '<td style="width: 65%; padding-left: 8px; padding-top: 5px;">';
            $html .= '<span style="padding: 0px; font-size: 14px;">HIGH SECURITY DOCUMENT <br> EXPRESS COURIER SERVICE</span>';
            $html .= '<img src="'.$asset_url.'/img/icon-contact-black.jpg" style="height: 28px; margin-top: 5px;"> 
                    <span style="font-size: 27px; margin-left: 4px; margin-top: -2px;">1300 909 488</span>';
            
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        
        $html .= '<table border="0" cellpadding="0" cellspacing="0" style="margin-top: 30px;">';
        $html .= '<tr>';
            $html .= '<td style="width: 25%; font-size: 15px;">TO:</td>';
            $html .= '<td style="width: 65%; font-size: 15px;">' ;
            if( $order['order_type'] != 9 ){
                // IF VISA ORDER
                $html .= $order['primary_traveller_name'] . '<br> ';
                if(trim($order['visa_mdd_address']) != ''){
                    $html .= $order['visa_mdd_address'] . ', ';
                }

                if(trim($order['visa_mdd_city']) != ''){
                    $html .= $order['visa_mdd_city'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['visa_mdd_state']) != ''){
                    $html .= $order['visa_mdd_state'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['visa_mdd_postcode']) != ''){
                    $html .= $order['visa_mdd_postcode'] ;
                }
                $html .= '<br>' . $order['visa_mdd_contact'];
            }else{
                // IF DOCUMENT LEGALISATION
                if(trim($order['embassy_address_line1']) != ''){
                    $html .= $order['embassy_address_line1'] . '<br> ';
                }
                if(trim($order['embassy_address_line2']) != ''){
                    $html .= $order['embassy_address_line2'] . '<br> ';
                }
                if(trim($order['embassy_address_line1']) == '' && trim($order['embassy_address_line2']) == ''){
                    if(trim($order['rep_name']) != ''){
                        $html .= $order['rep_name'] . '<br> ';
                    }else{
                        $html .= $order['country_name'] . '<br> ';
                    }
                }

                if(trim($order['embassy_street']) != ''){
                    $html .= $order['embassy_street'] . ', ';
                }
                
                if(trim($order['embassy_city']) != ''){
                    $html .= $order['embassy_city'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['embassy_state']) != ''){
                    $html .= $order['embassy_state'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['embassy_postcode']) != ''){
                    $html .= $order['embassy_postcode'] . '&nbsp;&nbsp;&nbsp;';
                }
                
                $html .= '<br>'. $order['embassy_phone'];
            }
            $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        
        $html .= '<table border="0" cellpadding="0" cellspacing="0" style="margin-top: 40px;">';
        $html .= '<tr>';
            $html .= '<td style="width: 25%; font-size: 15px;">FROM:</td>';
            $html .= '<td style="width: 65%; font-size: 15px;">';
            if( $order['order_type'] != 9 ){
                // IF VISA
                if(trim($order['embassy_address_line1']) != ''){
                    $html .= $order['embassy_address_line1'] . '<br> ';
                }
                if(trim($order['embassy_address_line2']) != ''){
                    $html .= $order['embassy_address_line2'] . '<br> ';
                }
                
                if(trim($order['embassy_address_line1']) == '' && trim($order['embassy_address_line2']) == ''){
                    if(trim($order['rep_name']) != ''){
                        $html .= $order['rep_name'] . '<br> ';
                    }else{
                        $html .= $order['country_name'] . '<br> ';
                    }
                }

                if(trim($order['embassy_street']) != ''){
                    $html .= $order['embassy_street'] . ', ';
                }
                
                if(trim($order['embassy_city']) != ''){
                    $html .= $order['embassy_city'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['embassy_state']) != ''){
                    $html .= $order['embassy_state'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['embassy_postcode']) != ''){
                    $html .= $order['embassy_postcode'] . '&nbsp;&nbsp;&nbsp;';
                }
                
                $html .= '<br>'. $order['embassy_phone'];
            }else{
                // IF DOCUMENT LEGALISATION
                $html .= $order['dl_company'] . '<br> ';
                if(trim($order['dl_address']) != ''){
                    $html .= $order['dl_address'] . ', ';
                }

                if(trim($order['dl_city']) != ''){
                    $html .= $order['dl_city'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['dl_state']) != ''){
                    $html .= $order['dl_state'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['dl_postcode']) != ''){
                    $html .= $order['dl_postcode'] ;
                }
                $html .= '<br>' . $order['dl_contact_name'];
            }
            $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        
        $html .= '<div style="width: 95%; margin-top: 20px; padding-top: 15px; padding-bottom: 15px; text-align: center; border: 1px solid #2e2a2b;">';
            $html .= '<img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 60px;">';
        $html .= '</div>';
        
        $html .= '<div style="width: 95%; margin-top: 15px; text-align: center;">';
            $html .= '<span style="font-size: 15px;">ORDER NUMBER: '. $_GET['order'] .'</span>';
        $html .= '</div>';
        
        
    $html .= '</td>';
    
    
    
    
    
    
    $html .= '<td style="width: 9%; ">';
    $html .= '</td>';
    
    
    
    
    
    
    
    
    // COLUMN 2
    $html .= '<td style="width: 46%; ">';
        $html .= '<table border="0" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td style="width: 35%;">';
            $html .= '<img src="'.$asset_url.'/img/cls-logo1.png" style="width: 100%;">';
        $html .= '</td>';
        $html .= '<td style="width: 65%; padding-left: 8px; padding-top: 5px;">';
            $html .= '<span style="padding: 0px; font-size: 14px;">HIGH SECURITY DOCUMENT <br> EXPRESS COURIER SERVICE</span>';
            $html .= '<img src="'.$asset_url.'/img/icon-contact-black.jpg" style="height: 28px; margin-top: 5px;"> 
                    <span style="font-size: 27px; margin-left: 4px; margin-top: -2px;">1300 909 488</span>';
            
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        
        $html .= '<table border="0" cellpadding="0" cellspacing="0" style="margin-top: 30px;">';
        $html .= '<tr>';
            $html .= '<td style="width: 25%; font-size: 15px;">TO:</td>';
            $html .= '<td style="width: 65%; font-size: 15px;">';
            if( $order['order_type'] != 9 ){
                // IF VISA
                if(trim($order['embassy_address_line1']) != ''){
                    $html .= $order['embassy_address_line1'] . '<br> ';
                }
                if(trim($order['embassy_address_line2']) != ''){
                    $html .= $order['embassy_address_line2'] . '<br> ';
                }

                if(trim($order['embassy_address_line1']) == '' && trim($order['embassy_address_line2']) == ''){
                    if(trim($order['rep_name']) != ''){
                        $html .= $order['rep_name'] . '<br> ';
                    }else{
                        $html .= $order['country_name'] . '<br> ';
                    }
                }
                
                if(trim($order['embassy_street']) != ''){
                    $html .= $order['embassy_street'] . ', ';
                }
                
                if(trim($order['embassy_city']) != ''){
                    $html .= $order['embassy_city'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['embassy_state']) != ''){
                    $html .= $order['embassy_state'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['embassy_postcode']) != ''){
                    $html .= $order['embassy_postcode'] . '&nbsp;&nbsp;&nbsp;';
                }
                
                $html .= '<br>'. $order['embassy_phone'];
            }else{
                // IF DOCUMENT LEGALISATION
                $html .= $order['dl_company'] . '<br> ';
                if(trim($order['dl_address']) != ''){
                    $html .= $order['dl_address'] . ', ';
                }

                if(trim($order['dl_city']) != ''){
                    $html .= $order['dl_city'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['dl_state']) != ''){
                    $html .= $order['dl_state'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['dl_postcode']) != ''){
                    $html .= $order['dl_postcode'] ;
                }
                $html .= '<br>' . $order['dl_contact_name'];
            }
            $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        
        $html .= '<table border="0" cellpadding="0" cellspacing="0" style="margin-top: 40px;">';
        $html .= '<tr>';
            $html .= '<td style="width: 25%; font-size: 15px;">FROM:</td>';
            $html .= '<td style="width: 65%; font-size: 15px;">';
            if( $order['order_type'] != 9 ){
                // IF VISA
                $html .= $order['primary_traveller_name'] . '<br> ';
                if(trim($order['visa_mdd_address']) != ''){
                    $html .= $order['visa_mdd_address'] . ', ';
                }

                if(trim($order['visa_mdd_city']) != ''){
                    $html .= $order['visa_mdd_city'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['visa_mdd_state']) != ''){
                    $html .= $order['visa_mdd_state'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['visa_mdd_postcode']) != ''){
                    $html .= $order['visa_mdd_postcode'] ;
                }
                $html .= '<br>' . $order['visa_mdd_contact'];
            }else{
                // IF DOCUMENT LEGALISATION
                if(trim($order['embassy_address_line1']) != ''){
                    $html .= $order['embassy_address_line1'] . '<br> ';
                }
                if(trim($order['embassy_address_line2']) != ''){
                    $html .= $order['embassy_address_line2'] . '<br> ';
                }
                
                if(trim($order['embassy_address_line1']) == '' && trim($order['embassy_address_line2']) == ''){
                    if(trim($order['rep_name']) != ''){
                        $html .= $order['rep_name'] . '<br> ';
                    }else{
                        $html .= $order['country_name'] . '<br> ';
                    }
                }

                if(trim($order['embassy_street']) != ''){
                    $html .= $order['embassy_street'] . ', ';
                }
                
                if(trim($order['embassy_city']) != ''){
                    $html .= $order['embassy_city'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['embassy_state']) != ''){
                    $html .= $order['embassy_state'] . '&nbsp;&nbsp;&nbsp;';
                }

                if(trim($order['embassy_postcode']) != ''){
                    $html .= $order['embassy_postcode'] . '&nbsp;&nbsp;&nbsp;';
                }
                
                $html .= '<br>'. $order['embassy_phone'];
            }
            $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        
        $html .= '<div style="width: 95%; margin-top: 20px; padding-top: 15px; padding-bottom: 15px; text-align: center; border: 1px solid #2e2a2b;">';
            $html .= '<img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 60px;">';
        $html .= '</div>';
        
        $html .= '<div style="width: 95%; margin-top: 15px; text-align: center;">';
            $html .= '<span style="font-size: 15px;">ORDER NUMBER: '. $_GET['order'] .'</span>';
        $html .= '</div>';
        
        
    $html .= '</td>';
    
    
    
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';
    
    $html .='</page>';
    
    
    require_once(dirname(__FILE__).'/html2pdf.class.php');
    //$html2pdf = new HTML2PDF('L', array(127.35, 73.5), 'en', true, 'UTF-8', array(5, 3, 5, 3));
    $html2pdf = new HTML2PDF('L', 'A5', 'en', true, 'UTF-8', array(8, 8, 8, 8));
//    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(8, 8, 8, 8));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    $html2pdf->WriteHTML($html);
    $html2pdf->Output(); //output to file
}else{
    header('Location: '.siteURL().'/user-login');
}

