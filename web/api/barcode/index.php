<?php

if( !isset($_GET['number']) || $_GET['number']=='' ){
	exit;
}

// include Barcode39 class 
include "Barcode39.php"; 

// set Barcode39 object 
$bc = new Barcode39($_GET['number']); 


// save barcode GIF file 
$bc->draw();

?>