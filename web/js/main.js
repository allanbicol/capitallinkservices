$(document).ready(function(){
    if($(".datetime-picker").length > 0){
        $(".datetime-picker").datetimepicker({
            //language:  'fr',
            format : "dd-mm-yyyy HH:ii:ss",
            weekStart: 1,
            todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
            showMeridian: 1
        });
    }
    
    if($('.date-picker').length > 0){
        $('.date-picker').datepicker({
             format : "dd-mm-yyyy"
         });
     }
    
});

$("body").on("focus",".datetime-picker",function(){
    $self = $(this);
    var position = $self.offset();
    var top = position.top + 35;
   $("div.datetimepicker").css("top", top + "px"); 
});

$("body").on("focus",".date-picker",function(){
    $self = $(this);
    var position = $self.offset();
    var top = position.top + 35;
   $("div.datepicker").css("top", top + "px"); 
});

$(".clear-selections").click(function(){
   $self = $(this);
   $self.closest("form.filter").find("input").val("");
   $self.closest("form.filter").find("select").val("");
});

$("input.rvv-visa-applied-other").keyup(function(){
   $("input[name=rvv_visa_applied_at]").val($(this).val());
});

/*==========================================
 * Global scripts
 *==========================================*/

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}



