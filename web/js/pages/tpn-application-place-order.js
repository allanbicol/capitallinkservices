

/**
 * run scripts
 * on document ready
 */
$(document).ready(function(){
    //$(".payment-option.default").click();
    paymentOption($('.payment-option'));
    
});


/**
 * use default address
 * get from user's details
 */
$("#chkUseDefaultAddress").click(function(){
    if(this.checked == true){
        $.ajax({
            url: url,
            method: 'POST',
            data: {'client':client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("#txtFname").val(data["fname"]);
                $("#txtLname").val(data["lname"]);
                $("#txtEmail").val(data["email"]);
                $("#txtPhone").val(data["phone"]);
                $("#txtMobile").val(data["mobile"]);
                $("#txtAddress").val(data["address"]);
                $("#txtCity").val(data["city"]);
                $("#txtState").val(data["state"]);
                $("#txtPostCode").val(data["postcode"]);
                $("#sltCountry").val(data["country_id"]);
            },
            error:function(){
            }
        });
    }else{
        
        // clear fields
        $("#txtFname").val("");
        $("#txtLname").val("");
        $("#txtEmail").val("");
        $("#txtPhone").val("");
        $("#txtMobile").val("");
        $("#txtAddress").val("");
        $("#txtCity").val("");
        $("#txtState").val("");
        $("#txtPostCode").val("");
        $("#sltCountry").val("");
    }
});





/**
 * use default billing address
 * get from user's details
 */
$("#chkUseDefaultBillingAddress").click(function(){
    if(this.checked == true){
        $.ajax({
            url: url,
            method: 'POST',
            data: {'client':client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("#txtCompany").val(data["company"]);
                $("#sltDepartment").val(data["department_id"]);
                $("#txtBillingAddress").val(data["mba_address"]);
                $("#txtBillingCity").val(data["mba_city"]);
                $("#txtBillingState").val(data["mba_state"]);
                $("#txtBillingPostCode").val(data["mba_postcode"]);
                $("#sltBillingCountry").val(data["mba_country_id"]);
            },
            error:function(){
            }
        });
    }else{
        
        // clear fields
        $("#txtCompany").val("");
        $("#sltDepartment").val("");
        $("#txtBillingAddress").val("");
        $("#txtBillingCity").val("");
        $("#txtBillingState").val("");
        $("#txtBillingPostCode").val("");
        $("#sltBillingCountry").val("");
    }
});






/**
 * payment option
 * hide in Card fee ($-.--) text
 * if payment option is
 * pay on account
 */
$(".payment-option").change(function(){
    var elem = $(this);
    paymentOption(elem);
});


function paymentOption(elem){
    //if(elem.val() == 0){
    var grand_total = 0;
    if($('input[name=paymentType]:radio:checked').val() == 0){
        $("#incCardFee").hide();
        $("#totalOrderPrice").html(orig_price.toFixed(2));
        $(".pay-required-one").attr("required", "required");
        $(".pay-required").removeAttr("required");
        $(".payment-option-fields.account").show();
        $(".payment-option-fields.credit-card").hide();
    }else if($('input[name=paymentType]:radio:checked').val() == 1){
        var inc_card_fee = parseFloat(orig_price) * (parseFloat(ccp_fee) / 100);
        var total_price = parseFloat( orig_price) + inc_card_fee;
        
        $("#incCardFee").find("span").html(inc_card_fee.toFixed(2));
        $("#incCardFee").show();
        
        
        $("#totalOrderPrice").html(parseFloat(total_price).toFixed(2));
        
        $(".pay-required").attr("required", "required");
        $(".pay-required-one").removeAttr("required");
        $(".payment-option-fields.account").hide();
        $(".payment-option-fields.credit-card").show();
    }else{
        $("#totalOrderPrice").html(orig_price.toFixed(2));
        $("#incCardFee").hide();
        $(".pay-required").removeAttr("required");
        $(".pay-required-one").removeAttr("required");
        $(".payment-option-fields.account").hide();
        $(".payment-option-fields.credit-card").hide();
        
    }
}