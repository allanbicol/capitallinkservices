/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    // show/hide traveller buttons
    showHideTravellerButtons();
    showHideTravelButtons();
});

// entry option select
$(".entry-option").click(function(){
    if($(this).val() == 1){
        $("#lbl-entry-date-country").html("Entry Date to Country");
        $("#lbl-depart-date-country").html("Departure Date from Country");
    }else{
        $("#lbl-entry-date-country").html("First Entry Date to Country");
        $("#lbl-depart-date-country").html("Last Departure Date from Country");
    }
});

// add travel button event
$("#travel-details .add").click(function(e){
   var elem = $(this);
   
   if($(".travel-details-group .group").length <= 4){
       $(".travel-details-group").append('\n\
        <div class="group">\n\
            <hr/>\n\
            <div class="form-group">\n\
                <label>Choose Destination</label>\n\
                <select class="form-control destination" name="destination[]" required>\n\
                </select>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label>Departure Date from Australia</label>\n\
                <input type="date" class="form-control" name="departureDate" required/>\n\
            </div>\n\
            \n\
             <div class="form-group">\n\
                <ul>\n\
                    <li><label><input type="radio" name="entryOption[]" value="1" class="entry-option"> Single Entry</label></li>\n\
                    <li><label><input type="radio" name="entryOption[]" value="2" class="entry-option"> Double Entry</label></li>\n\
                    <li><label><input type="radio" name="entryOption[]" value="3" class="entry-option"> Multiple Entry</label></li>\n\
                </ul>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label id="lbl-entry-date-country">First Entry Date to Country</label>\n\
                <input type="date" class="form-control" name="entryDateCounty[]" required/>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label id="lbl-depart-date-country">Last Departure Date from Country</label>\n\
                <input type="date" class="form-control" name="departureDateCountry[]" required/>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label>Purpose of Travel</label>\n\
                <textarea class="form-control purpose-text" name="travelPurpose[]" required placeholder="[NOTE] - Many diplomatic missions will not accept generic \'Government Business\' as a purpose of travel. Please clearly state the reason for your travel including some level of detail"></textarea>\n\
                <p class="note">PLEASE write your purpose for travel in a format that will work in a sentence that starts with the words \'applicant is travelling to \'country name’ for the purpose of… \' For example: \'…for the purpose of attending meetings with staff at the Australian Embassy in Moscow\'</p>\n\
            </div>\n\
        </div>\n\
        ');
        
        
        // copy travel options from the main travel to additional traveller
        $(".travel-details-group .group:last-child").find("select.destination").append($("select.main-destination").html());
        
        // show/hide travel buttons
        showHideTravelButtons()
        
        // smooth scroll
        $('html, body').animate({
            scrollTop: $(".travel-details-group .group:last-child").offset().top
        }, "slow");
   }
});



// add traveller button event
$("#traveller-details .add").click(function(e){
    var elem = $(this);
    
    if($(".traveller-details-group .group").length <= 6){ 
        $(".traveller-details-group").append('\n\
        <div class="group">\n\
            <hr />\n\
            <div class="form-group">\n\
                <label>Title</label>\n\
                <select class="form-control title" name="title[]" required>\n\
                </select>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>First name</label>\n\
                <input type="text" class="form-control" name="fname[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Middle name</label>\n\
                <input type="text" class="form-control" name="mname[]"/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Last name</label>\n\
                <input type="text" class="form-control" name="lname[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Email</label>\n\
                <input type="email" class="form-control" name="email[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Occupation</label>\n\
                <input type="text" class="form-control" name="occupation[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Phone</label>\n\
                <input type="phone" class="form-control" name="phone[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Date of Birth</label>\n\
                <input type="date" class="form-control" name="birthDate[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Gender</label><br/>\n\
                <label><input type="radio" name="travellerGender[]" value="male"> Male</label>\n\
                <label><input type="radio" name="travellerGender[]" value="female"> Female</label>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Nearest Capital City</label>\n\
                <select name="travellerNearestCapitalCity[]" class="form-control nearest-capital-city">\n\
                </select>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Password type</label>\n\
                <select class="form-control password-type" name="passwordType[]" required>\n\
                </select>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Nationality</label>\n\
                <select class="form-control nationality" name="nationality[]" required>\n\
                </select>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Passport Number</label>\n\
                <input type="text" class="form-control" name="passportNumber[]" required/>\n\
            </div>\n\
        </div>');
        
        
        // copy traveler options from the main traveler to additional traveller
        $(".traveller-details-group .group:last-child").find("select.title").append($("select.main-title").html());
        $(".traveller-details-group .group:last-child").find("select.password-type").append($("select.main-password-type").html());
        $(".traveller-details-group .group:last-child").find("select.nationality").append($("select.main-nationality").html());
        $(".traveller-details-group .group:last-child").find("select.nearest-capital-city").append($("select.main-nearest-capital-city").html());
        
        // show/hide traveller buttons
        showHideTravellerButtons();

        // smooth scroll
        $('html, body').animate({
            scrollTop: $(".traveller-details-group .group:last-child").offset().top
        }, "slow");
    }
    
    
});






/*======================================================
 * functions
 ======================================================*/


// show/hide travel buttons
function showHideTravelButtons(){
    // show/hide add button
    if($(".travel-details-group .group").length >= 6){
        $("#travel-details .add").hide();
    }else{
        $("#travel-details .add").show();
    }
    
    // show/hide remove button
    if($(".travel-details-group .group").length > 1){
        $("#travel-details .remove").show();
    }else{
        $("#travel-details .remove").hide();
    }
}

// show/hide traveller buttons
function showHideTravellerButtons(){
    // show/hide add button
    if($(".traveller-details-group .group").length >= 6){
        $("#traveller-details .add").hide();
    }else{
        $("#traveller-details .add").show();
    }
    
    // show/hide remove button
    if($(".traveller-details-group .group").length > 1){
        $("#traveller-details .remove").show();
    }else{
        $("#traveller-details .remove").hide();
    }
}