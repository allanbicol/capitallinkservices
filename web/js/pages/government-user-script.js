var total_cost = 0
var inc_card_fee = 0;
var selected = [];
var visa_types = [];
var visa_type_fees = [];

$(document).ready(function(){
    if($('#passport-listings').length > 0){
            $('#passport-listings').dataTable({
                "bProcessing": false,

                "sAjaxSource": list_url1,
                "aoColumns": [ 
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true }
                ],
                 "aoColumnDefs": [
                    {
                        "aTargets": [2],
                        "mData": null,
                        "mRender": function (data, type, full){
                            var output = '';
                            var string = full[2];
                            string = string.split(",");

                            for(i=0; i<string.length; i++){
                                output = output  + string[i] + '<br/>';
                            }
                            return output;
                        }
                    },
                    {
                        "aTargets": [3],
                        "mData": null,
                        "mRender": function (data, type, full){
                            var output = '';
                            var string = full[3];
                            string = string.split(",");

                            for(i=0; i<string.length; i++){
                                output = output  + string[i] + '<br/>';
                            }
                            return output;
                        }
                    },
                  ],
                  "sDom": 'T<"clear">lfrtip',
                  "oTableTools": {
                        "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                        "aButtons": [
                            {
                                "sExtends": "copy",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            },
                            {
                                "sExtends": "csv",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            },
                            {
                                "sExtends": "pdf",
                                "mColumns": [0, 1, 2, 3, 4, 5],
                                //"sPdfOrientation": "landscape",
                                "sTitle": "Manifest Report:", 
                                //"sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                                "fnClick":  function( nButton, oConfig, flash ) {
                                    window.open(api_url+"/pdfc/reports/gov-manifest.php"+ params);
                                    return false;
                                }
                            },
                            {
                                "sExtends": "print",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            }
                        ]
                  },
                  "fnInitComplete": function(oSettings, json) {
                    $("div.dataTable-buttons-2").html( $('#passport-listings_wrapper div.DTTT_container').html() );
                 }
            });
        }
        
    if($("body#order-history").length > 0){

        $('#pending-tpn-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": pending_tpn_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                 {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                         }else{
                             return '';
                         }
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+open_tpn_order_url +"?order_no="+ full[5]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> <a role="button" class="btn btn-danger btn-xs" onclick="javascript: deleteTpnOrder('+ full[5] +');"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#pendingTpn #pending-tpn-listings_info").find(".totalEntries").html();
                    $("#totalPendingTPN").html(total_entries);
                 }
        });
        
        $('#past-tpn-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": past_tpn_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                         }else{
                             return '';
                         }
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+open_tpn_order_url +"?order_no="+ full[5]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> <a role="button" class="btn btn-danger btn-xs" onclick="javascript: deleteTpnOrder('+ full[5] +');"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#pastTpn #past-tpn-listings_info").find(".totalEntries").html();
                    $("#totalPastTPN").html(total_entries);
                 }
        });
        
        
        $('#courier-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": courier_service_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                         }else{
                             return '';
                         }
                     }
                 },
                 {
                    "aTargets": [2],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return (full[2] == 0) ? 'Yes' : 'No';
                     }
                 },
                {
                    "aTargets": [3],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        var price = parseFloat(full[3]);
                        return '$'+ price.toFixed(2);
                     }
                 },
                {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        if(full[4] == 10 && full[5] == 0){
                            return 'Pending';
                        }else if(full[4] == 10 && full[5] == 1){
                            return 'Paid online';
                        }else if(full[4] == 10 && full[5] == 2){
                            return 'Paid by account';
                        }else if(full[4] == 12 && full[5] == 0){
                            return 'Completed';
                        }else if(full[4] == 12 && full[5] == 1){
                            return 'Completed';
                        }
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+open_courier_service_url +"?order_no="+ full[0]+'"><span class="glyphicon glyphicon-th-list"></span> View</a>';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#courierService #courier-listings_info").find(".totalEntries").html();
                    $("#totalCouriers").html(total_entries);
                 }
        });
        
        
        
        $('#pending-visa-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": pending_visa_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                         }else{
                             return '';
                         }
                     }
                 },
                {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[4] != ''){
                            var $date = full[4].split("-");
                            var $day = $date[2];
                            var $month = $date[1];
                            var $year = $date[0];
                            return $day + '-' + $month + '-' + $year;
                         }else{
                             return '';
                         }
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+open_visa_url +"?order_no="+ full[5]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> <a role="button" class="btn btn-danger btn-xs" onclick="javascript: deleteTpnOrder('+ full[5] +');"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#pendingVisa #pending-visa-listings_info").find(".totalEntries").html();
                    $("#totalPendingVisa").html(total_entries);
                 }
        });
        
        
        $('#past-visa-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": past_visa_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                         }else{
                             return '';
                         }
                     }
                 },
                {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[4] != ''){
                            var $date = full[4].split("-");
                            var $day = $date[2];
                            var $month = $date[1];
                            var $year = $date[0];
                            return $day + '-' + $month + '-' + $year;
                         }else{
                             return '';
                         }
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+open_visa_url +"?order_no="+ full[5]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#pastVisa #past-visa-listings_info").find(".totalEntries").html();
                    $("#totalPastVisa").html(total_entries);
                 }
        });
        
        
        
        $('#police-clearance-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": police_clearance_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                         }else{
                             return '';
                         }
                     }
                 },
                 {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return (full[4] == 0) ? 'Yes' : 'No';
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        var price = parseFloat(full[5]);
                        return '$'+ price.toFixed(2);
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        if(full[6] == 10){
                            return 'Pending';
                        }else if(full[6] == 11){
                            return 'Paid';
                        }else if(full[6] == 12){
                            return 'Completed';
                        }
                     }
                 },
                {
                    "aTargets": [7],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+open_police_clearance_url +"?order_no="+ full[0]+'"><span class="glyphicon glyphicon-th-list"></span> View</a>';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#policeClearance #police-clearance-listings_info").find(".totalEntries").html();
                    $("#totalPoliceClearance").html(total_entries);
                 }
        });
        
        
        $('#document-delivery-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": document_delivery_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                         }else{
                             return '';
                         }
                     }
                 },
                 {
                    "aTargets": [3],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return (full[3] == 0) ? 'Yes' : 'No';
                     }
                 },
                {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        var price = parseFloat(full[4]);
                        return '$'+ price.toFixed(2);
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        if(full[5] == 10){
                            return 'Pending';
                        }else if(full[5] == 11){
                            return 'Paid';
                        }else if(full[5] == 12){
                            return 'Completed';
                        }
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+open_doc_delivery_url +"?order_no="+ full[0]+'"><span class="glyphicon glyphicon-th-list"></span> View</a>';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#documentDelivery #document-delivery-listings_info").find(".totalEntries").html();
                    $("#totalDocDelivery").html(total_entries);
                 }
        });
        
        
        
        $('#russian-visa-voucher-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": russian_visa_voucher_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [0],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         var $date = full[0].split("-");
                         var $day = $date[2];
                         var $month = $date[1];
                         var $year = $date[0];
                         return $day + '-' + $month + '-' + $year;
                     }
                 },
                {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        var price = parseFloat(full[4]);
                        return '$'+ price.toFixed(2);
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[5] == 10){
                               return "Applied";
                          }else if(full[5] == 11){
                               return "Paid";
                           }else if(full[5] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_russian_visa_voucher_url +"?order_no="+ full[6]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                var total_entries = $("#russianVisaVoucher #russian-visa-voucher-listings_info").find(".totalEntries").html();
                $("#totalRussianVisaVoucher").html(total_entries);
             }
        });
        
        $('#doc-legalisation-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": document_legalisation_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [0],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         var $datetime = full[0].split(" ");
                            var $date = $datetime[0].split("-");
                            var $day = $date[2];
                            var $month = $date[1];
                            var $year = $date[0];
                            return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                
                {
                    "aTargets": [2],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[2] == 10){
                               return "Applied";
                          }else if(full[2] == 11){
                               return "Paid";
                           }else if(full[2] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [3],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_doc_legalisation_url +"?order_no="+ full[3]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                var total_entries = $("#documentLegalisation #doc-legalisation-listings_info").find(".totalEntries").html();
                $("#totalDocLegalisation").html(total_entries);
             }
        });
        
    }else if($("body#tpn-resubmission").length > 0){
        
         bkLib.onDomLoaded(function() {
            new nicEditor({
                iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
                buttonList : ['bold'],
            }).panelInstance('tpnSrc');
        });
        
        
        
    }else if($("body#application-visa-options").length > 0){
        $("select#courier").change();
        $("body#application-visa-options select.visa-type").change();
         
    }else if($("body#application-public-visa").length > 0){
        
        applicationPubVisaShowHideTravelButtons();
        applicationPubVisaShowHideTravellerButtons();
        
    }else if($("body#application-public-visa-options").length > 0){
        $("select#courier").change();
        $("body#application-public-visa-options select.visa-type").change();
        
    }else if($("body#passport-office-pickup-delivery").length > 0){
        
        
        
        total_cost = cost + (($("#applicants > div.form-group").length - 1) * additional_cost);
        
        paymentOption($('.payment-option'));
        
        addRemoveApplicant();
        
        
    }else if($("body#application-police-clearance").length > 0){
        
        
        $("#policeClearance").change();
        paymentOption($('.payment-option'));
        
       if($('.payment-option:checked').val() == 0){
            $("#incCardFee").hide();
            $(".pay-required-one").attr("required", "required");
            $(".pay-required").removeAttr("required");
        }else{


            $("#incCardFee").show();
            $(".pay-required").attr("required", "required");
            $(".pay-required-one").removeAttr("required");
        }
        
        showHideTravelButtons();
        
        
    }else if($("body#application-document-delivery").length > 0){
        $("#paymentDocType").change();
        paymentOption($('.payment-option'));
        
        
        
    }else if($("body#report").length > 0){
        
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": false, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [7],
                    "mData": null,
                    "mRender": function (data, type, full) {
                        var total = parseFloat(full[7]);
                        return '$' + total.toFixed(2);
                    }
                },
                {
                    "aTargets": [8],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        if(full[2] == 1){
                            var view_url = view_order_visa;
                        }else if(full[2] == 2){
                            var view_url = view_order_tpn;
                        }else if(full[2] == 3){
                            var view_url = view_order_tpn;
                        }else if(full[2] == 4){
                            var view_url = view_order_courier;
                        }else if(full[2] == 5){
                            var view_url = view_order_police_clearance;
                        }else if(full[2] == 6){
                            var view_url = view_order_pulic_visa;
                        }else if(full[2] == 7){
                            var view_url = view_order_document_delivery;
                        }else if(full[2] == 8){
                            var view_url = russian_visa_voucher_list_url;
                        }else if(full[2] == 9){
                            var view_url = document_legalisation_list_url;
                        }
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ view_url +"?order_no="+ full[8]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [0, 1, 3, 4, 5, 6, 7]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [0, 1, 3, 4, 5, 6, 7]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [0, 1, 3, 4, 5, 6, 7]
                        },
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#application-bulk-public-visa").length > 0){
        initializeEditables();
        
        
         
         $("#selectAll").prop({checked: false});
         $(".delete-items").attr("disabled","disabled");
         
         
         $('.editable-item.destination').each(function(){
             if($.trim($(this).attr("data-value")) != ''){
                getPublicVisaTypes($(this).attr("data-value"), this);
             }
         });
         
         $('.editable-item-visa-types').each(function(){
             if($.trim($(this).attr("data-value")) != ''){
                getPublicVisaTypeFees($(this).attr("data-value"), this);
             }
         });
         //console.log(visa_types);
         
    }
   
 });
 
// $('body').on('click', '.bulk-select', function() {
//    //var id = this.id;
//    var $tr = $(this).closest("tr");
//    var id = $tr.attr("id");
//    var index = $.inArray(id, selected);
//
//    if ( index === -1 ) {
//        selected.push( id );
//    } else {
//        selected.splice( index, 1 );
//    }
//
//    $tr.toggleClass('selected');
//    var isChecked = $tr.hasClass('selected');
//    $tr.find("input.bulk-select").prop({checked: isChecked});
//
//    if($('#datatable-listings tbody tr.selected, #datatable-listings tfoot tr.selected').length > 0){
//        $("#deleteItems").removeAttr("disabled");
//    }else{
//        $("#deleteItems").attr("disabled","disabled");
//    }
//});

$('body#application-bulk-public-visa').on('click', '.bulk-select', function() {
    //var id = this.id;
    var $tr = $(this).closest("tr");
    var id = $tr.attr("id");
    var index = $.inArray(id, selected);

    if ( index === -1 ) {
        selected.push( id );
    } else {
        selected.splice( index, 1 );
    }

    $tr.toggleClass('selected');
    var isChecked = $tr.hasClass('selected');
    //$tr.find("input.bulk-select").prop({checked: isChecked});

    if($(this).closest("div.order").find('table.traveller tbody tr.selected, table.traveller tfoot tr.selected').length > 0){
        $(this).closest("div.order").find(".delete-items").removeAttr("disabled");
    }else{
        $(this).closest("div.order").find(".delete-items").attr("disabled","disabled");
    }
});


 
 $("body#application-bulk-public-visa").on("click",".btn-new-traveller",function(){
    var $order_count = $(this).attr("data-order");
    var $html = '<tr>\n\
        <td><input type="checkbox" name="bulk_select[]" class="bulk-select"></td>\n\
        <td><span class="editable-item editable-empty required" data-type="select"  data-showbuttons="true" data-mode="popup"  data-original-title="Select title" data-source="'+ $json_titles +'" required></span><input class="value" type="hidden" name="travellerTitle['+ $order_count +'][]"></td>\n\
        <td><span class="editable-item editable-empty required" data-type="text" data-showbuttons="true" data-mode="popup" data-original-title="First name" required></span><input class="value" type="hidden" name="travellerFname['+ $order_count +'][]"></td>\n\
        <td><span class="editable-item editable-empty" data-type="text" data-showbuttons="true" data-mode="popup" data-original-title="Middle name"></span><input class="value" type="hidden" name="travellerMname['+ $order_count +'][]"></td>\n\
        <td><span class="editable-item editable-empty required" data-type="text" data-showbuttons="true" data-mode="popup" data-original-title="Last name" required></span><input class="value" type="hidden" name="travellerLname['+ $order_count +'][]"></td>\n\
        <td><span class="editable-item editable-empty required" data-type="text" data-showbuttons="true" data-mode="popup" data-original-title="Email" required>'+ client_email +'</span><input class="value" type="hidden" name="travellerEmail['+ $order_count +'][]" value="'+ client_email +'"></td>\n\
        <td><span class="editable-item editable-empty required" data-type="text" data-showbuttons="true" data-mode="popup" data-original-title="Phone" required>'+ client_phone +'</span><input class="value" type="hidden" name="travellerPhone['+ $order_count +'][]" value="'+ client_phone +'"></td>\n\
        <td><span class="editable-item editable-empty required" data-type="select"  data-showbuttons="true" data-mode="popup"  data-original-title="Select type" data-source="'+ $json_passport_types +'" required></span><input class="value" type="hidden" name="travellerPassportType['+ $order_count +'][]"></td>\n\
        <td><span class="editable-item editable-empty required" data-type="select"  data-showbuttons="true" data-mode="popup"  data-original-title="Select nationality" data-source="'+ $json_natinalities +'" required></span><input class="value" type="hidden" name="travellerNationality['+ $order_count +'][]"></td>\n\
        <td><span class="editable-item editable-empty required" data-type="text" data-showbuttons="true" data-mode="popup" data-original-title="Passport number" required></span><input class="value" type="hidden" name="travellerPassportNumber['+ $order_count +'][]"></td>\n\
    </tr>';
     
     
    $(this).closest("div.order").find("#traveller-details").find("table tfoot").append($html);
    
    initializeEditables();
    
 });
 
 
 $("body#application-bulk-public-visa").find("#newOrder").click(function(){
    $(".spinner-container").html("<center><img src='"+ img_url +"/spinner.gif'/></center>");
    $.ajax({
        url: new_order_form_url,
        method: 'POST',
        data: {'order_count': $("div.order").length + 1},
        success: function(form) {
            $("body#application-bulk-public-visa").find(".order-bundle").append(form);
            initializeEditables();
            $('.date-picker').datepicker({
                format : "dd-mm-yyyy"
            });
            
            $(".spinner-container").html("");
        },
        error:function(){
        }
    }); 
 });
 
 $("body#application-bulk-public-visa").on("click",".delete-order",function(){
     var $self = $(this);
     $self.closest("div.order").hide('slow', function(){ $self.closest("div.order").remove(); });
 });
 
 function initializeEditables(){
     var $vt_count = 0;
     var $tf_count = 0;
     $('.editable-item').editable({
         validate: function(value) {
            if($.trim(value) == '') {
                return 'This field is required';
            }
        }
     });
     $('.editable-item-visa-types').each(function(){
         
         
            var $self = $(this);
            $vt_count = parseInt($vt_count);
            $self.attr("tag",$vt_count);
            $self.attr("data-param",$vt_count);
            // first search the row

            $('.editable-item-visa-types').editable({
                source: function() {
                    var $num = $(this).editable().data('param');
                    return visa_types[$num];
                }
            });
            
        
         
        $vt_count = $vt_count + 1;
     });
     
     
     $('.editable-item-visa-type-fees').each(function(){
         var $self = $(this);
         $self.attr("tag",$tf_count);
         $self.attr("data-param",$tf_count);
         
         $('.editable-item-visa-type-fees').editable({
           source: function() {
               var $num = $(this).editable().data('param');
               return visa_type_fees[$num];
           }
        });
        $tf_count = $tf_count + 1;
     });
     
        
    // this is to automatically make the next item in the table editable
    $('.editable-item').on('save', function(e, params){
        var that = this;
        // persist the old value in the element to be restored when clicking reset
        var oldItemValue = $(that)[0].innerHTML;
        var item = $(that).closest('td').next().find('.editable-item');
        var itemValue = "";
        if($(that).closest('td').find(".editable-input").find("input").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("input").val();
        }else if($(that).closest('td').find(".editable-input").find("select").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("select").val();
        }
        if (!$(that).attr('oldValue')) {
            console.log('persisting original value: ' + oldItemValue);
            $(that).attr('oldValue', oldItemValue);
        }
        setTimeout(function() {
            // first search the row

            if(itemValue == ""){
                itemValue = $(that).html();
            }
            itemValue = (itemValue != "Empty") ? itemValue : "";
            $(that).closest('td').find('.value').val(itemValue);
            
            if(itemValue != ""){
                $(that).closest('td').removeClass("field-error");
            }
            console.log(item);
            if (item.length == 0) {
                // check the next row
                item = $(that).closest('tr').next().find('.editable-item');
            }
            item.editable('show');
        }, 200);
    });
    
    
    // destination on change
    // get visa types
    $('.editable-item.destination').on('save', function(e, params){
        var that = this;
        var itemValue = "";
        if($(that).closest('td').find(".editable-input").find("input").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("input").val();
        }else if($(that).closest('td').find(".editable-input").find("select").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("select").val();
        }
        
        setTimeout(function() {
            // first search the row

            if(itemValue == ""){
                itemValue = $(that).html();
            }
            itemValue = (itemValue != "Empty") ? itemValue : "";
            if(itemValue != ""){
                $(that).closest('td').removeClass("field-error");
            }
            getPublicVisaTypes(itemValue, that);
            
            
        }, 200);
    });
    
    
    $('.editable-item-visa-types').on('save', function(e, params){
        var that = this;
        var itemValue = "";
        if($(that).closest('td').find(".editable-input").find("input").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("input").val();
        }else if($(that).closest('td').find(".editable-input").find("select").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("select").val();
        }
        setTimeout(function() {
            // first search the row

            if(itemValue == ""){
                itemValue = $(that).html();
            }
            itemValue = (itemValue != "Empty") ? itemValue : "";
            $(that).closest('td').find('input.value').val(itemValue);
            
            if(itemValue != ""){
                $(that).closest('td').removeClass("field-error");
            }
            
            getPublicVisaTypeFees(itemValue, that);
        }, 200);
    });
    
    $('.editable-item-visa-type-fees').on('save', function(e, params){
        var that = this;
        var itemValue = "";
        if($(that).closest('td').find(".editable-input").find("input").length > 0){
            //itemValue = $(that).closest('td').find(".editable-input").find("input").val();
            var itemValue = '';
            $(that).closest('td').find(".editable-input").find("input").each(function(){
                var $self = $(this);
                if($self.is(":checked")){
                    var price = $self.parent().find("span").html();
                    price = price.split(" - $");
                    
                    itemValue += $self.val() + "," + price[1]  + ";";
                }
            });
            
        }else if($(that).closest('td').find(".editable-input").find("select").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("select").val();
        }
        setTimeout(function() {
            // first search the row

            if(itemValue == ""){
                itemValue = $(that).html();
            }
            itemValue = (itemValue != "Empty") ? itemValue : "";
            $(that).closest('td').find('.value').val(itemValue);
            
            if(itemValue != ""){
                $(that).closest('td').removeClass("field-error");
            }
            getPublicVisaTypeFees(itemValue, that);
        }, 200);
    });
 }
 
 
 function getPublicVisaTypes($id, self){
     
     $.ajax({
        url: get_visa_types_url,
        method: 'POST',
        data: {'country_id': $id},
        success: function(data) {
            //$(self).closest("tr").find(".editable-item.visa-types").attr("data-source",data);
            var $count = parseInt($(self).closest("tr").find(".editable-item-visa-types").attr("tag"));
            //$count = $count + 1;
            visa_types[$count] = data; 
            //visa_types[2] = data; 
        },
        error:function(){
        }
    });
 }
 
 function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }

    alert(out);

    // or, if you wanted to avoid alerts...

    var pre = document.createElement('pre');
    pre.innerHTML = out;
    document.body.appendChild(pre)
}
 
 function getPublicVisaTypeFees($id, self){
     
     $.ajax({
        url: get_visa_type_fees_url,
        method: 'POST',
        data: {'visa_id': $id},
        success: function(data) {
            //$(self).closest("tr").find(".editable-item.visa-types").attr("data-source",data);
            var $count = parseInt($(self).closest("tr").find(".editable-item-visa-type-fees").attr("tag"));
            visa_type_fees[$count] = data; 
        },
        error:function(){
        }
    });
 }
 
 
 $("body#application-bulk-public-visa").on("click",".delete-items",function(){
    bootbox.confirm("Are you sure?\n Selecting \"Yes\" will remove selected item(s) temporarily until \"Save Progress\" button or \"Next\" is clicked.", function(result) {
      if(result == true){
          $(".bulk-select").each(function(){
              var $self = $(this);
              if($self.is(":checked")){
                $self.closest("tr").remove();
              }
          });
      }
  }); 
});

/**
 * author: leokarl
 * description: datatables selection column
 */
$("body#application-bulk-public-visa").on("change",".bulk-select-all", function(){
    if($(this).is(':checked')){
        $(this).closest("div.order").find('table.traveller tbody tr, tfoot tr').not('.selected').find("input.bulk-select").click(); // click all rows to add "selected" class
    }else{
        $(this).closest("div.order").find('table.traveller tbody tr.selected, table.traveller tfoot tr.selected').find("input.bulk-select").prop({checked: false}); // unchecked row checkboxes
        $(this).closest("div.order").find('table.traveller tbody tr.selected, table.traveller tfoot tr.selected').removeClass('selected'); // remove selected classes on rows
        $(this).closest("div.order").find(".delete-items").attr("disabled","disabled"); // disable bulk delete button
    }
   
});


// net page event
$("body#application-bulk-public-visa").on('click', '.delete-items', function(){
//$("#nextPage").click(function(){
   $("#hidSubmit").val(2);
   $(".required").removeAttr("required");
   
});

$("body#application-bulk-public-visa").find(".next-page").click(function(){
    var $error = 0;
    $("body#application-bulk-public-visa").find("span.required").each(function(){
        var $self = $(this);
        if($self.html() == '' || $self.html() == 'Empty'){
            $self.closest("td").addClass("field-error");
            $error += 1;
        }
    });
    if($error > 0){
        alert("Highlighted fields are required fields.");
        $("#error-notes").html("<b>Error: </b> Highlighted fields are required.");
        return false;
    }else{
        $("#error-notes").html("");
        return true;
    }
});



 /**
  * Start: Visa Application - Visa Options Page
  */
 
 $("body#application-visa-options select.visa-type").change(function(){ 
    var elem = $(this);
    elem.parents().eq(1).find("div.visa-type-requirements").html('<img src="'+ asset +'/spinner.gif"/>');
    $.ajax({
        url: url,
        method: 'POST',
        data: {'type_id': elem.val(), 'destination_id' : elem.attr('id')},
        success: function(data) {
            if(elem.val() != ""){
                data = JSON.parse(data);
                if(data.length > 0){
                    var html = '<ul>';
                    for(i=0; i<data.length; i++){
                        var isChecked = (data[i]['s_checked'] == true) ? "checked" : "";
                        var isRequired = (data[i]['s_required'] == 1) ? "required" : "";
                        var requiredSign = (data[i]['s_required'] == 1) ? "<b><i><font color='red'>*</font></i></b>" : "";
                        var orig_cost = data[i]['cost'];
                        //data[i]['cost'] = data[i]['cost'] + (data[i]['cost'] * (special_price/100));
                        if(data[i]['s_checked'] == true){
                            
                            visa_opt_total_amt += parseFloat(data[i]['cost']);
                            html += '<li><label><input type="checkbox" name="addReq['+ data[i]['visa_id'] +'][]" class="visa-type-requirement '+ isRequired +'" tag="'+data[i]['cost']+'" value="'+ data[i]['id']+ ',' + orig_cost +'" checked '+ isRequired +'> '+ requiredSign +' '+ data[i]['requirement'] +' ($'+ data[i]['cost'] +')</label></li>';
                            elem.parents().eq(2).find("select.visa-type").attr("tag", parseFloat(elem.parents().eq(2).find("select.visa-type").attr("tag")) + parseFloat(data[i]['cost']));
                            $(".panel-info").html(visa_opt_total_amt.toFixed(2));
                        }else{
                            html += '<li><label><input type="checkbox" name="addReq['+ data[i]['visa_id'] +'][]" class="visa-type-requirement '+ isRequired +'" tag="0" value="'+ data[i]['id']+ ',' + orig_cost +'" '+ isRequired +'> '+ requiredSign +' '+ data[i]['requirement'] +' ($'+ data[i]['cost'] +')</label></li>';
                        }
                    }
                    html += '</ul>';
                    
                }else{
                    var html="<i>not available</i>";
                }
                elem.parents().eq(1).find("div.visa-type-requirements").html(html);
                
            }else{
                elem.parents().eq(1).find("div.visa-type-requirements").html('select visa type');
            }
                
        },
        error:function(){
        }
    });
    
 });
 
 $("body#application-public-visa-options select.visa-type, body#cls-visa-information select.visa-type").change(function(){
    var elem = $(this);
    elem.parents().eq(1).find("div.visa-type-requirements").html('<img src="'+ asset +'/spinner.gif"/>');
    elem.parents().eq(2).find("div.visa-information").html('<img src="'+ asset +'/spinner.gif"/>');
    if(typeof selected_reqs === 'undefined'){
    }
    $.ajax({
        url: url,
        method: 'POST',
        data: {'type_id': elem.val(), 
            'destination_id' : elem.attr('id'),
            'selected_reqs' : ''
        },
        success: function(data) {
            if(elem.val() != ""){
                data = JSON.parse(data);
                var visa_information = data['visa_information'];
                if(data['additional_requirements'].length > 0){
                    var reqs = '<ul>';
                    for(i=0; i<data['additional_requirements'].length; i++){
                        var isChecked = (data['additional_requirements'][i]['s_checked'] == true) ? "checked" : "";
                        var isRequired = (data['additional_requirements'][i]['s_required'] == 1) ? "required" : "";
                        var requiredSign = (data['additional_requirements'][i]['s_required'] == 1) ? "<b><i><font color='red'>*</font></i></b>" : "";
                        
                        if(data['additional_requirements'][i]['s_checked'] == true){
                            visa_opt_total_amt += parseFloat(data['additional_requirements'][i]['cost']);
                            reqs += '<li><label><input type="checkbox" name="addReq['+ data['additional_requirements'][i]['visa_id'] +'][]" class="visa-type-requirement '+ isRequired +'" tag="'+data['additional_requirements'][i]['cost']+'" value="'+ data['additional_requirements'][i]['id']+ ',' + data['additional_requirements'][i]['cost'] +'" checked '+ isRequired +'> '+ requiredSign +' '+ data['additional_requirements'][i]['requirement'] +' ($'+ data['additional_requirements'][i]['cost'] +')</label></li>';
                            elem.parents().eq(2).find("select.visa-type").attr("tag", parseFloat(elem.parents().eq(2).find("select.visa-type").attr("tag")) + parseFloat(data['additional_requirements'][i]['cost']));
                            $("#visaOptTotalAmnt").html(visa_opt_total_amt.toFixed(2));
                        }else{
                            reqs += '<li><label><input type="checkbox" name="addReq['+ data['additional_requirements'][i]['visa_id'] +'][]" class="visa-type-requirement '+ isRequired +'" tag="0" value="'+ data['additional_requirements'][i]['id']+ ',' + data['additional_requirements'][i]['cost'] +'" '+ isRequired +'> '+ requiredSign +' '+ data['additional_requirements'][i]['requirement'] +' ($'+ data['additional_requirements'][i]['cost'] +')</label></li>';
                        }
                    }
                    reqs += '</ul>';
                    
                    
                }else{
                    var reqs="<i>not available</i>";
                }
                elem.parents().eq(1).find("div.visa-type-requirements").html(reqs);
                elem.parents().eq(2).find("div.visa-information").html(visa_information);
                
            }else{
                elem.parents().eq(1).find("div.visa-type-requirements").html('select visa type');
                elem.parents().eq(2).find("div.visa-information").html('');
            }
                
        },
        error:function(){
        }
    });
    
 });
 
 
 $("body#application-visa-options #chkUseDefaultAddress, body#application-public-visa-options #chkUseDefaultAddress, body#application-document-legalisation #chkUseDefaultAddress, body#order-dl-quote-place-order #chkUseDefaultAddress, body#application-bulk-public-visa #chkUseDefaultAddress").click(function(){
    if(this.checked === true){
         $.ajax({
            url: udetails_url,
            method: 'POST',
            data: {'client': client},
            success: function(data) {
                data = JSON.parse(data);
                $("#docReturnCompany").val(data['company']);
                $("#docReturnAddress").val(data['mdda_address']);
                $("#docReturnCity").val(data['mdda_city']);
                $("#docReturnState").val(data['mba_state']);
                $("#docReturnPostCode").val(data['mdda_postcode']);
                $("#docReturnFname").val(data['fname']);
                $("#docReturnLname").val(data['lname']);
                $("#docReturnContactNo").val(data['phone']);

            },
            error:function(){
            }
        });
    }else{
    }
 });
 
 
 
 // save progress event
$("#saveProgress").click(function(){
   $("#hidSubmit").val(0);
   $(".required").removeAttr("required");
});


// net page event
$("body").on('click', '#nextPage', function(){
//$("#nextPage").click(function(){
   $("#hidSubmit").val(1);
   $(".required").attr("required","required");
   $("select.visa-type.required").each(function(){
       if($(this).val() == ''){
           $('body#application-visa-options ul.nav-tabs li a[href="#'+ $(this).parents().eq(2).attr('id') +'"]').click();
           $('body#application-public-visa-options ul.nav-tabs li a[href="#'+ $(this).parents().eq(2).attr('id') +'"]').click();
           return false;
       }
   });
   
});

$("body#cls-visa-information").on('click', '#nextPage', function(){
   var $count=0;
   $(".visa-type-requirement").each(function(){
      if(this.checked){
          $count +=1;
      }
   });
   
   if($count == 0){
       $(".add-reqs-selection").css("display","inline");
       return false;
   }
});

 var visa_opt_total_amt = 0;
 $("select#courier").change(function(){
     visa_opt_total_amt -= parseFloat($(this).attr("tag"));
     visa_opt_total_amt += parseFloat($(this).find("option:selected").attr("tag"));
     //visa_opt_total_amt = visa_opt_total_amt + (visa_opt_total_amt * (special_price/100));
     $(this).attr("tag",parseFloat($(this).find("option:selected").attr("tag")));
     $("#visaOptTotalAmnt").html(visa_opt_total_amt.toFixed(2));
     $("#visaCourierAmnt").html(parseFloat($(this).attr("tag")).toFixed(2));
 });
 
 $("select.visa-type").change(function(){
     visa_opt_total_amt -= parseFloat($(this).attr("tag"));
     visa_opt_total_amt += parseFloat($(this).find("option:selected").attr("tag"));
     $(this).attr("tag",parseFloat($(this).find("option:selected").attr("tag")));
     $("#visaOptTotalAmnt").html(visa_opt_total_amt.toFixed(2));
     $("#visaServiceChargeAmnt").html(parseFloat($(this).attr("tag")).toFixed(2));
 });
 
 $("body#application-visa-options, body#application-public-visa-options, body#cls-visa-information").on('click', '.visa-type-requirement', function(){
     
     if(this.checked === true){
        var cost = $(this).val();
        cost = cost.split(",");
        cost = parseFloat(cost[1]);
        //cost = (typeof special_price === 'undefined') ? cost : cost + (cost * (special_price/100));
        visa_opt_total_amt += parseFloat(cost);
        $(this).attr("tag",parseFloat(cost));
        $(this).parents().eq(5).find("select.visa-type").attr("tag", parseFloat($(this).parents().eq(5).find("select.visa-type").attr("tag")) + parseFloat(cost));
         var newVal = parseFloat($("#visaApplicationAmnt").html()) + parseFloat(cost);
         $("#visaApplicationAmnt").html(newVal.toFixed(2));
    }else{
        
        var newVal = parseFloat($("#visaApplicationAmnt").html()) - parseFloat($(this).attr("tag")).toFixed(2);
        visa_opt_total_amt -= parseFloat($(this).attr("tag"));
        $("#visaApplicationAmnt").html(newVal.toFixed(2));

    }
    
    $("#visaOptTotalAmnt").html(visa_opt_total_amt.toFixed(2));
    
 });
 
 
 /**
  * End: Visa Application - Visa Options Page
  */
 
 
 
 
  $("body#passport-office-pickup-delivery #addApplicant").click(function(){
    var applicants = $("#applicants > div.form-group").length;
    if(applicants >= 6){ 
        return false;
    }
    
    $("#applicants").append('\n\
        <div class="form-group">\n\
            <div class="row">\n\
                <div class="col-lg-12">\n\
                    <label>Passport Applicant Full Name (<a class="link remove remove-passport-applicant">remove applicant</a>)</label>\n\
                    <input type="text" name="passportApplicantFullname['+ applicants +']" class="form-control" placeholder="Passport applicant full name"  required>\n\
                    <ul style="margin-left: 10px; margin-top: 10px;">\n\
                        <li><label><input type="checkbox" name="personalPassport['+ applicants +']"> Personal Passport </label></li>\n\
                        <li><label><input type="checkbox" name="doPassport['+ applicants +']"> Diplomatic/Official Passport </label></li>\n\
                        <li><label><input type="checkbox" name="birthCertificate['+ applicants +']"> Birth Certificate </label></li>\n\
                        <li><label><input type="checkbox" name="marriageCertificate['+ applicants +']"> Marriage Certificate </label></li>\n\
                        <li><label><input type="checkbox" name="certOfAustralianCitizenship['+ applicants +']"> Certificate of Australian Citizenship </label></li>\n\
                    </ul>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    ');
     
     // smooth scroll
    $('html, body').animate({
        scrollTop: $("#addApplicant").offset().top
    }, "slow");
    
    addRemoveApplicant();
    courierPrice();
    
 });
 
 
 $("body#passport-office-pickup-delivery").on('click','.remove-passport-applicant', function(){
    var elem = $(this);
    bootbox.confirm("Are you sure?", function(result) {
        if(result == true){
            elem.parents().eq(3).animate(
                {height: "0px"},
                {complete: function(){
                    elem.parents().eq(3).remove();
                    if($("#remApplicants").length > 0){
                        $("#remApplicants").val($("#remApplicants").val() + elem.parent().next('.passport-applicant-id').val() + ';');
                    }
                    addRemoveApplicant();
                }}
            ); 
        }
    }); 
    
 });
 
 $("#chkUseDefaultAddress").click(function(){
    if(this.checked == true){
        $.ajax({
            url: url,
            method: 'POST',
            data: {'client': client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("#txtFname").val(data["fname"]);
                $("#txtLname").val(data["lname"]);
                $("#txtEmail").val(data["email"]);
                $("#txtPhone").val(data["phone"]);
                $("#txtMobile").val(data["mobile"]);
                $("#txtAddress").val(data["address"]);
                $("#txtCity").val(data["city"]);
                $("#txtState").val(data["state"]);
                $("#txtPostCode").val(data["postcode"]);
                $("#sltCountry").val(data["country_id"]);
            },
            error:function(){
            }
        });
    }else{
        
        // clear fields
//        $("#txtFname").val("");
//        $("#txtLname").val("");
//        $("#txtEmail").val("");
//        $("#txtPhone").val("");
//        $("#txtMobile").val("");
//        $("#txtAddress").val("");
//        $("#txtCity").val("");
//        $("#txtState").val("");
//        $("#txtPostCode").val("");
//        $("#sltCountry").val("");
    }
});
 

/**
 * payment option
 * hide in Card fee ($-.--) text
 * if payment option is
 * pay on account
 */
$(".payment-option").change(function(){
    var elem = $(this);
    paymentOption(elem);
});



$("#policeClearance").change(function(){
   var elem = $(this);
   $("#genInfo").html('<center><img src="' + img_asset + '/spinner.gif' + '"/></center>');
   $.ajax({
        url: get_clearance_info_url,
        method: 'POST',
        data: {'clearance_id': elem.val()},
        success: function(data) {
            if(elem.val() != ''){
                data = JSON.parse(data);
                clearance_price = data[0]['price'];
                clearance_price_additional = data[0]['price_additional'];
                $("#genInfo").html(data[0]['gen_info']);
                clearancePrice();
            }else{
                $("#genInfo").html("");
                $("#totalOrderPrice").html("0.00");
            }
        },
        error:function(){
        }
    });
});


$("#application-police-clearance #chkUseDefaultDocReturnAddress").change(function(){
    if(this.checked == true){
        $.ajax({
            url: url,
            method: 'POST',
            data: {'client': client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("#drAddress").val(data["mdda_address"]);
                $("#drCity").val(data["mdda_city"]);
                $("#drPostcode").val(data["mdda_postcode"]);
                $("#drFname").val(data["fname"]);
                $("#drLname").val(data["lname"]);
                $("#drContactNo").val(data["phone"]);
                $("#drEmail").val(data["email"]);
                
            },
            error:function(){
            }
        });
    }else{
        
        // clear fields
        $("#drAddress").val("");
        $("#drCity").val("");
        $("#drPostcode").val("");
        $("#drFname").val("");
        $("#drLname").val("");
        $("#drContactNo").val("");
        $("#drEmail").val("");
    }
});


$("#application-police-clearance #useMyDetails").change(function(){
   if(this.checked == true){
        $.ajax({
            url: url,
            method: 'POST',
            data: {'client': client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("#fname").val(data["fname"]);
                $("#mname").val(data["mname"]);
                $("#lname").val(data["lname"]);
                $("#email").val(data["email"]);
                $("#phone").val(data["phone"]);
                $("#mobile").val(data["mobile"]);
                $("#address").val(data["address"]);
                $("#city").val(data["city"]);
                $("#postcode").val(data["postcode"]);
                $("#state").val(data["state"]);
                $("#country").val(data["country_id"]);
                
                
                $("#mainNationality").val(data["country_id"]);
            },
            error:function(){
            }
        });
    }else{
        
        // clear fields
//        $("#fname").val("");
//        $("#mname").val("");
//        $("#lname").val("");
//        $("#email").val("");
//        $("#phone").val("");
//        $("#mobile").val("");
//        $("#address").val("");
//        $("#city").val("");
//        $("#postcode").val("");
//        $("#state").val("");
//        $("#country").val("");
    }
});



$("#application-russian-visa-voucher #useMyDetails").change(function(){
   if(this.checked == true){
        $.ajax({
            url: mydetails_url,
            method: 'POST',
            data: {'client': client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("select[name=rvv_applicant_title]").val(data["title"]);
                $("input[name=rvv_applicant_fname]").val(data["fname"]);
                $("input[name=rvv_applicant_mname]").val(data["mname"]);
                $("input[name=rvv_applicant_lname]").val(data["lname"]);
                $("input[name=rvv_applicant_email]").val(data["email"]);
                $("input[name=rvv_applicant_phone]").val(data["phone"]);
                $("input[name=rvv_city]").val(data["city"]);
                $("input[name=rvv_postcode]").val(data["postcode"]);
                $("input[name=rvv_state]").val(data["state"]);
                $("select[name=rvv_country]").val(data["country_id"]);
                
            },
            error:function(){
            }
        });
    }
});


// use my details event
$("body#application-public-visa #useMyDetails").click(function(){
    if(this.checked == true){
        $.ajax({
            url: url,
            method: 'POST',
            data: {'client': client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("#mainTitle").val(data["title"]);
                $("#mainFname").val(data["fname"]);
                $("#mainLname").val(data["lname"]);
                $("#mainEmail").val(data["email"]);
                $("#mainPhone").val(data["phone"]);
                $("#mainNationality").val(data["country_id"]);
            },
            error:function(){
            }
        });
    }else{
        
        // clear fields
        $("#mainTitle").val("");
        $("#mainFname").val("");
        $("#mainLname").val("");
        $("#mainEmail").val("");
        $("#mainPhone").val("");
        $("#mainNationality").val("");
    }
});

$("body").on("click",".date-picker-button", function(){
    $(this).prev("input").focus();
});


$("body#application-police-clearance #applicant-details").find(".remove").click(function(){
    $(".applicant-details-group .group:last-child").remove();
    
    // show/hide travel buttons
    showHideTravelButtons();
    
    clearancePrice();
    
    // smooth scroll
    $('html, body').animate({
        scrollTop: $(".applicant-details-group .group:last-child").offset().top
    }, "slow");
});


$("body#application-police-clearance #applicant-details").find(".add").click(function(){
   $(".applicant-details-group").append('\n\
    <div class="group">\n\
<hr/>\n\
        <div class="form-group">\n\
            <label>First name</label>\n\
            <input type="text" class="form-control required" name="fname[]"  required/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>Middle name</label>\n\
            <input type="text" class="form-control" name="mname[]"/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>Last name</label>\n\
            <input type="text" class="form-control required" name="lname[]" required/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>Email</label>\n\
            <input type="email" class="form-control required" name="email[]" required/>\n\
        </div>\n\
\n\
\n\
        <div class="form-group">\n\
            <label>Contact Phone</label>\n\
            <input type="phone" class="form-control required" name="phone[]" required/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>Mobile Phone</label>\n\
            <input type="phone" class="form-control required" name="mobile[]" required/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>Address</label>\n\
            <input type="text" class="form-control required" name="address[]" required/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>City</label>\n\
            <input type="text" class="form-control required" name="city[]" required/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>Postcode</label>\n\
            <input type="text" class="form-control required" name="postcode[]" required/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>State</label>\n\
            <input type="text" class="form-control required" name="state[]" required/>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>Country</label>\n\
            <select class="form-control country required" name="country[]" required>\n\
                <option value="">Select country</option>\n\
            </select>\n\
        </div>\n\
\n\
        <div class="form-group">\n\
            <label>Passport Number</label>\n\
            <input type="text" class="form-control required" name="passportNumber[]" required/>\n\
        </div>\n\
    </div>\n\
   ');
    
    
    $("select.country").html($("#country").html());
    // show/hide travel buttons
    showHideTravelButtons();

    clearancePrice();
    
    // datepicker 
    // http://www.eyecon.ro/bootstrap-datepicker/
    $('.date-picker').datepicker({
        format : "dd-mm-yyyy"
    });

    // date-picker icon button event
    $(".date-picker-button").click(function(){
       $(this).parent().find(".date-picker").focus();
    });
        
    // smooth scroll
    $('html, body').animate({
        scrollTop: $(".applicant-details-group .group:last-child").offset().top
    }, "slow");
});




// add travel button event
$("body#application-public-visa #travel-details .add").click(function(e){
   var elem = $(this);
   var travel_group_length = $(".travel-details-group .group").length;
   if(travel_group_length <= 4){
       $(".travel-details-group").append('\n\
        <div class="group">\n\
            <hr/>\n\
            <div class="form-group">\n\
                <label>Choose Destination</label>\n\
                <select class="form-control destination required" name="destination[]" required>\n\
                </select>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label>Departure Date from Australia</label>\n\
                <!--<input type="date" class="form-control required" name="departureDate[]" required/>-->\n\
                <div class="input-group date">\n\
                    <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="departureDate[]" placeholder="DD-MM-YYYY" required></input>\n\
                    <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                </div>\n\
            </div>\n\
            <div class="form-group">\n\
                <label id="lbl-entry-date-country">First Entry Date to Country</label>\n\
                <!--<input type="date" class="form-control required" name="entryDateCounty[]" required/>-->\n\
                <div class="input-group date">\n\
                    <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="entryDateCounty[]" placeholder="DD-MM-YYYY" required></input>\n\
                    <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                </div>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label id="lbl-depart-date-country">Last Departure Date from Country</label>\n\
                <!--<input type="date" class="form-control required" name="departureDateCountry[]" required/>-->\n\
                <div class="input-group date">\n\
                    <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="departureDateCountry[]" placeholder="DD-MM-YYYY" required></input>\n\
                    <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                </div>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label>Purpose of Travel</label>\n\
                <textarea class="form-control purpose-text required" name="travelPurpose[]" required placeholder="[NOTE] - Many diplomatic missions will not accept generic \'Government Business\' as a purpose of travel. Please clearly state the reason for your travel including some level of detail"></textarea>\n\
            </div>\n\
        </div>\n\
        ');
        
        
        // copy travel options from the main travel to additional traveller
        $(".travel-details-group .group:last-child").find("select.destination").append($("select.main-destination").html());
        
        // datepicker 
        // http://www.eyecon.ro/bootstrap-datepicker/
        $('.date-picker').datepicker({
            format : "dd-mm-yyyy"
        });
        
        // date-picker icon button event
        $(".date-picker-button").click(function(){
           $(this).parent().find(".date-picker").focus();
        });
    
        // entry option select
        $(".entry-option").click(function(){
            $(this).parent().parent().parent().parent().find(".entry-option-value").val($(this).val());
            if($(this).val() == 1){
                $(this).parent().parent().parent().parent().parent().find("#lbl-entry-date-country").html("Entry Date to Country");
                $(this).parent().parent().parent().parent().parent().find("#lbl-depart-date-country").html("Departure Date from Country");
            }else{
                $(this).parent().parent().parent().parent().parent().find("#lbl-entry-date-country").html("First Entry Date to Country");
                $(this).parent().parent().parent().parent().parent().find("#lbl-depart-date-country").html("Last Departure Date from Country");
            }
        });


        // show/hide travel buttons
        applicationPubVisaShowHideTravelButtons();
        
        // smooth scroll
        $('html, body').animate({
            scrollTop: $(".travel-details-group .group:last-child").offset().top
        }, "slow");
   }
});



// remove travel button event
$("body#application-public-visa #travel-details .remove").click(function(e){
    
    // remove last added travel details
    $(".travel-details-group .group:last-child").remove();
    
    // show/hide travel buttons
    applicationPubVisaShowHideTravelButtons();
    
    // smooth scroll
    $('html, body').animate({
        scrollTop: $(".travel-details-group .group:last-child").offset().top
    }, "slow");
});


// add traveller button event
$("body#application-public-visa #traveller-details .add").click(function(e){
    var elem = $(this);
    
    if($(".traveller-details-group .group").length <= 6){
        $(".traveller-details-group").append('\n\
        <div class="group">\n\
            <hr />\n\
            <div class="form-group">\n\
                <label>Title</label>\n\
                <select class="form-control title required" name="travellerTitle[]" required>\n\
                </select>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>First name</label>\n\
                <input type="text" class="form-control required" name="travellerFname[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Middle name</label>\n\
                <input type="text" class="form-control" name="travellerMname[]"/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Last name</label>\n\
                <input type="text" class="form-control required" name="travellerLname[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Email</label>\n\
                <input type="email" class="form-control required" name="travellerEmail[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Organisation</label>\n\
                <input type="text" class="form-control required" name="travellerOrganisation[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Occupation</label>\n\
                <input type="text" class="form-control required occupation" name="travellerOccupation[]" autocomplete="off" required/>\n\
            </div>\n\
            <div class="form-group rp-info" style="display:none">\n\
                <p><strong>Please enter related person\'s information:</strong></p>\n\
                <div class="row">\n\
                    <div class="col-lg-6">\n\
                        Fullname\n\
                    </div>\n\
                    <div class="col-lg-6">\n\
                        <input type="text" name="rpinfoFullname[]" class="form-control"/>\n\
                    </div>\n\
                </div>\n\
\n\
                <div class="row">\n\
                    <div class="col-lg-6">\n\
                        Position at post\n\
                    </div>\n\
                    <div class="col-lg-6">\n\
                        <input type="text" name="rpinfoPositionAtPost[]" class="form-control"/>\n\
                    </div>\n\
                </div>\n\
\n\
                <div class="row">\n\
                    <div class="col-lg-6">\n\
                        Name of post\n\
                    </div>\n\
                    <div class="col-lg-6">\n\
                        <input type="text" name="rpinfoNameOfPost[]" class="form-control"/>\n\
                    </div>\n\
                </div>\n\
\n\
                <div class="row">\n\
                    <div class="col-lg-6">\n\
                        City\n\
                    </div>\n\
                    <div class="col-lg-6">\n\
                        <input type="text" name="rpinfoCity[]" class="form-control"/>\n\
                    </div>\n\
                </div>\n\
\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Phone</label>\n\
                <input type="phone" class="form-control required" name="travellerPhone[]" required/>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Date of Birth</label>\n\
                <!--<input type="date" class="form-control required" name="travellerBirthDate[]" required/>-->\n\
                <div class="input-group date">\n\
                    <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="travellerBirthDate[]" placeholder="DD-MM-YYYY" required></input>\n\
                    <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                </div>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Passport type</label>\n\
                <select class="form-control password-type required" name="travellerPassportType[]" required>\n\
                </select>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Nationality</label>\n\
                <select class="form-control nationality required" name="travellerNationality[]" required>\n\
                </select>\n\
            </div>\n\
    \n\
            <div class="form-group">\n\
                <label>Passport Number</label>\n\
                <input type="text" class="form-control required" name="travellerPassportNumber[]" required/>\n\
            </div>\n\
        </div>');
        
        
        // copy traveler options from the main traveler to additional traveller
        $(".traveller-details-group .group:last-child").find("select.title").append($("select.main-title").html());
        $(".traveller-details-group .group:last-child").find("select.password-type").append($("select.main-password-type").html());
        $(".traveller-details-group .group:last-child").find("select.nationality").append($("select.main-nationality").html());
        
        
        // datepicker 
        // http://www.eyecon.ro/bootstrap-datepicker/
        $('.date-picker').datepicker({
            format : "dd-mm-yyyy"
        });
        
        // date-picker icon button event
        $(".date-picker-button").click(function(){
           $(this).parent().find(".date-picker").focus();
        });
        
        // show/hide traveller buttons
        applicationPubVisaShowHideTravellerButtons();

        // auto complete 
//        $.get(jsonpath, function(data){
//            $("input.occupation").typeahead({ source:data });
//        },'json');
        /**
            * start: occupation options
            */
           $("input.occupation").focus(function(){
               var position = $(this).offset();
               var height = $(this).height();
               $(this).addClass("on");
               $('.occupation-options').css("left",position.left + "px");
               $('.occupation-options').css("top", (position.top + height + 12) + "px");
               $('.occupation-options').css("display","block");
           });


           $("input.occupation").focusout(function(){
               $('.occupation-options').css("display","none");
               $(this).removeClass("on");
           });

           $('.occupation-options').find("li").mousedown(function(){
               var val = $(this).attr("tag");
               $("input.occupation.on").val(val);
           });
           /**
            * end: occupation options
            */
        
        // smooth scroll
        $('html, body').animate({
            scrollTop: $(".traveller-details-group .group:last-child").offset().top
        }, "slow");
    }
    
    
});



// remove traveller button event
$("body#application-public-visa #traveller-details .remove").click(function(e){
    
    // remove last added traveller details
    $(".traveller-details-group .group:last-child").remove();
    
    // show/hide traveller buttons
    applicationPubVisaShowHideTravellerButtons();
    
    // smooth scroll
    $('html, body').animate({
        scrollTop: $(".traveller-details-group .group:last-child").offset().top
    }, "slow");
});



/**
 * start: occupation options
 */
$("body#application-public-visa input.occupation").focus(function(){
    var position = $(this).offset();
    var height = $(this).height();
    $(this).addClass("on");
    $('.occupation-options').css("left",position.left + "px");
    $('.occupation-options').css("top", (position.top + height + 12) + "px");
    $('.occupation-options').css("display","block");
    
});


$("body#application-public-visa input.occupation").focusout(function(){
    $('.occupation-options').css("display","none");
    $(this).removeClass("on");
});

$('body#application-public-visa .occupation-options').find("li").mousedown(function(){
    var val = $(this).attr("tag");
    $("input.occupation.on").val(val);
    
    val = val.trim().toLowerCase();
    if(val == 'a dependant' || val == 'a spouse'){
        $("input.occupation.on").parent().next('.rp-info').css('display','block');
        $("input.occupation.on").parent().next('.rp-info').find('input').attr('required','required');
    }else{
        $("input.occupation.on").parent().next('.rp-info').css('display','none');
        $("input.occupation.on").parent().next('.rp-info').find('input').removeAttr('required');
    }
});

$("body#application-public-visa  input.occupation").keyup(function(){
   if($(this).val().trim().toLowerCase() == 'a dependant' || $(this).val().trim().toLowerCase() == 'a spouse'){
        $(this).parent().next('.rp-info').css('display','block');
        $(this).parent().next('.rp-info').find('input').attr('required','required');
   }else{
        $(this).parent().next('.rp-info').css('display','none');
        $(this).parent().next('.rp-info').find('input').removeAttr('required');
   }
});
/**
 * end: occupation options
 */



/*
 * Document Delivery Application
 * Position: Start
 */
var doc_delivery_price = 0;
$("body#application-document-delivery").on('change', '#paymentDocType', function(){
   doc_delivery_price = parseFloat($(this).find("option:selected").attr("tag"));
   
   docDeliveryPrice();
});


$("body#application-document-legalisation #addNewDocType").click(function(){
    $(".dls-group").append('\n\
        <div class="form-group group">\n\
            <div class="row">\n\
                <div class="col-lg-4">\n\
                    <label>Type of document enclosed</label>\n\
                    <input type="text" class="form-control required" name="dl_type[]" placeholder="Type" required>\n\
                </div>\n\
\n\
                <div class="col-lg-2">\n\
                    <label>Number</label>\n\
                    <input type="number" class="form-control required" name="dl_number[]" placeholder="0" required>\n\
                </div>\n\
\n\
                <div class="col-lg-4">\n\
                    <label>Note</label>\n\
                    <input type="text" class="form-control" name="dl_note[]" placeholder="note">\n\
                </div>\n\
\n\
                <div class="col-lg-2" style="padding-top: 27px;">\n\
                    <button type="button" class="btn btn-danger btn-sm remove-doc-type"><span class="glyphicon glyphicon-remove"></span> remove</button>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    ');
});


$("body#cls-services-doc-legalisation #addNewDocType").click(function(){
    $(".dls-group").append('\n\
        <div class="form-group group">\n\
            <div class="row">\n\
                <div class="col-lg-4">\n\
                    <span>Type of document enclosed</span>\n\
                    <input type="text" class="form-control required" name="dl_type[]" placeholder="Type" required>\n\
                </div>\n\
\n\
                <div class="col-lg-2">\n\
                    <span>Number</span>\n\
                    <input type="number" class="form-control required" name="dl_number[]" placeholder="0" required>\n\
                </div>\n\
\n\
                <div class="col-lg-4">\n\
                    <span>Note</span>\n\
                    <input type="text" class="form-control" name="dl_note[]" placeholder="note">\n\
                </div>\n\
\n\
                <div class="col-lg-2" style="padding-top: 23px;">\n\
                    <button type="button" class="btn btn-danger btn-sm remove-doc-type"><span class="glyphicon glyphicon-remove"></span> remove</button>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    ');
});

$("body#application-document-legalisation").on('click','.remove-doc-type',function(){
   var $self = $(this);
   bootbox.confirm("Are you sure?", function(result) {
        if(result == true){
            $self.parents().eq(2).remove();
        }
   });
});


$("body#cls-services-doc-legalisation").on('click','.remove-doc-type',function(){
   var $self = $(this);
    $self.parents().eq(2).remove();
});


$("body").on('change','#chkSameAsAbove',function(){
    if(this.checked){
        $("#docReturnCompany").val($("input[name=dl_company]").val());
        $("#docReturnAddress").val($("input[name=dl_address]").val());
        $("#docReturnCity").val($("input[name=dl_city]").val());
        $("#docReturnState").val($("input[name=dl_state]").val());
        $("#docReturnPostCode").val($("input[name=dl_postcode]").val());
        $("#docReturnFname").val($("input[name=dl_contact_name]").val());
        $("#docReturnContactNo").val($("input[name=dl_mobile]").val());
    }
});


$("input[name=useUserInfo]").change(function(){
   if($(this).is(":checked")){
       $.ajax({
            url: udetails_url,
            method: 'POST',
            data: {'client': client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("input[name=dl_company]").val(data["company"]);
                $("select[name=dl_nationality]").val(data["country_id"]);
                $("input[name=dl_address]").val(data["address"]);
                $("input[name=dl_city]").val(data["city"]);
                $("input[name=dl_state]").val(data["state"]);
                $("input[name=dl_postcode]").val(data["postcode"]);
                $("input[name=dl_contact_name]").val(data["fname"]);
                $("input[name=dl_mobile]").val(data["mobile"]);
                $("input[name=dl_email]").val(data["email"]);
            },
            error:function(){
            }
        });
   }
});



/*
 * Document Delivery Application
 * Position: End
 */
 /*======================================================
 * functions
 ======================================================*/
 
 // refresh datatables
function RefreshTable(tableId, urlData)
{
  $.getJSON(urlData, null, function( json )
  {
    table = $(tableId).dataTable();
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<json.aaData.length; i++)
    {
      table.oApi._fnAddData(oSettings, json.aaData[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
  });
  //clearconsole();
}

 
 
 function deleteTpnOrder(order_no){
     bootbox.confirm("Do you really want to delete this pending order?", function(result) {
        if(result == true){
            $.ajax({
                url: del_order_url,
                method: 'POST',
                data: {'order_no': order_no},
                success: function(data) {
                    if(data == 'success'){
                        location.reload();
                    }
                    
                },
                error:function(){
                }
            });
        }
     });
 }
 
 
 
 function getVisaInformation(country_id, country_name){
     $('#visaInformation').find('#myModalLabel').html(country_name);
     $('#visaInformation').find('#myModalBody').html('<center><img src="' + asset + '/spinner.gif' + '"/></center>');
     $('#visaInformation').modal('show');
     $.ajax({
        url: open_url,
        method: 'POST',
        data: {'country_id': country_id},
        success: function(data) {
            $('#visaInformation').find('#myModalBody').html(data);
        },
        error:function(){
        }
    });
     
 }
 
 function getClearanceInformation(clearance_id, clearance_name){
     $('#clearanceInformation').find('#myModalLabel').html(clearance_name);
     $('#clearanceInformation').find('#myModalBody').html('<center><img src="' + asset + '/spinner.gif' + '"/></center>');
     $('#clearanceInformation').modal('show');
     $.ajax({
        url: open_url,
        method: 'POST',
        data: {'clearance_id': clearance_id},
        success: function(data) {
            data = JSON.parse(data);
            $('#clearanceInformation').find('#myModalBody').html(data[0]['gen_info']);
        },
        error:function(){
        }
    });
     
 }
 
 
  function addRemoveApplicant(){
     if($("#applicants > div.form-group").length > 1){ 
        $("#removeLastApplicant").css('display','inline');
    }else{
        $("#removeLastApplicant").css('display','none');
    }
    
    if($("#applicants > div.form-group").length >= 6){
        $("#addApplicant").css('display','none');
    }else{
        $("#addApplicant").css('display','inline');
    }
    
    courierPrice();
 }
 
 
 
 // show/hide travel buttons
function applicationPubVisaShowHideTravelButtons(){
    // show/hide add button
    if($(".travel-details-group .group").length >= 5){
        $("#travel-details .add").hide();
    }else{
        $("#travel-details .add").show();
    }
    
    // show/hide remove button
    if($(".travel-details-group .group").length > 1){
        $("#travel-details .remove").show();
    }else{
        $("#travel-details .remove").hide();
    }
}


// show/hide traveller buttons
function applicationPubVisaShowHideTravellerButtons(){
    // show/hide add button
    if($(".traveller-details-group .group").length >= 6){
        $("#traveller-details .add").hide();
    }else{
        $("#traveller-details .add").show();
    }
    
    // show/hide remove button
    if($(".traveller-details-group .group").length > 1){
        $("#traveller-details .remove").show();
    }else{
        $("#traveller-details .remove").hide();
    }
}

// show/hide travel buttons
function showHideTravelButtons(){
    // show/hide add button
    if($(".applicant-details-group .group").length >= 5){
        $("#btn-container .add").hide();
    }else{
        $("#btn-container .add").show();
    }
    
    // show/hide remove button
    if($(".applicant-details-group .group").length > 1){
        $("#btn-container .remove").show();
    }else{
        $("#btn-container .remove").hide();
    }
}

function clearancePrice(){
    var additional = $(".applicant-details-group .group").length - 1;
    var total = clearance_price + (additional * clearance_price_additional);
    //total = total + (total * (10/100));
    total = total + (total * (special_price/100));
    var inc_card_fee = 0;
    if($(".payment-option:checked").val() == 1){
        inc_card_fee = total * (ccp_fee/100);
        total = total + inc_card_fee;
    }
    $("#incCardFee").find("span").html(inc_card_fee.toFixed(2));
    $("#totalOrderPrice").html(total.toFixed(2));
}


    

function courierPrice(){
    var additional = $("#applicants > div.form-group").length - 1;
    var total = cost + (additional * additional_cost);
    
    total = total + (total * (special_price/100));
    total = total + (total * (10/100));
    var inc_card_fee = 0;
    if($(".payment-option:checked").val() == 1){
        inc_card_fee = total * (ccp_fee/100);
        total = total + inc_card_fee;
    }
    $("#incCardFee").find("span").html(inc_card_fee.toFixed(2));
    $("#totalOrderPrice").html(total.toFixed(2));
    
}



function docDeliveryPrice(){
    var total = doc_delivery_price;
    //total = total + (total * (special_price/100));
    total = total + (total * (10/100));
    var inc_card_fee = 0;
    if($(".payment-option:checked").val() == 1){
        inc_card_fee = total * (ccp_fee/100);
        total = total + inc_card_fee;
    }
    $("#incCardFee").find("span").html(inc_card_fee.toFixed(2));
    $("#totalOrderPrice").html(total.toFixed(2));
}


function paymentOption(elem){
    
    //if(elem.val() == 0){
    if($('input[name=paymentType]:radio:checked').val() == 0){  
        $("#incCardFee").hide();
        $(".pay-required-one").attr("required", "required");
        $(".pay-required").removeAttr("required");
        $(".payment-option-fields.account").show();
        $(".payment-option-fields.credit-card").hide();
    }else if($('input[name=paymentType]:radio:checked').val() == 1){
        $("#incCardFee").show();
        $(".pay-required").attr("required", "required");
        $(".pay-required-one").removeAttr("required");
        $(".payment-option-fields.account").hide();
        $(".payment-option-fields.credit-card").show();
    }else{
        $("#incCardFee").hide();
        $(".pay-required").removeAttr("required");
        $(".pay-required-one").removeAttr("required");
        $(".payment-option-fields.account").hide();
        $(".payment-option-fields.credit-card").hide();
    }
    
    if($("body#passport-office-pickup-delivery").length > 0){
        courierPrice();
    }else if($("body#application-police-clearance").length > 0){
        clearancePrice();
    }else if($("body#application-document-delivery").length > 0){
        docDeliveryPrice();
    }
    
    $(document).ready(function(){
    
    });
        $("body").on("click",".dataTable-buttons-2 > .DTTT_button.DTTT_button_print",function(){
            $("body").find("#passport-listings_wrapper").find("div.DTTT_container").find(".DTTT_button.DTTT_button_print").click();
        });
}

// added by leokarl 4-27-2015
$(".copy-details-from-primary").change(function(){
   var $self = $(this);
   var $altno = $self.closest("div.alternate").attr("data-alt");
   if($self.is(":checked")){
       $("input[name=deliveryRecipientName"+$altno+"]").val( $("input[name=deliveryRecipientName]").val() );
       $("input[name=deliveryCompany"+$altno+"]").val( $("input[name=deliveryCompany]").val() );
       $("input[name=deliveryAddress"+$altno+"]").val( $("input[name=deliveryAddress]").val() );
       $("input[name=deliveryCity"+$altno+"]").val( $("input[name=deliveryCity]").val() );
       $("input[name=deliveryPostcode"+$altno+"]").val( $("input[name=deliveryPostcode]").val() );
       $("input[name=deliveryContactNo"+$altno+"]").val( $("input[name=deliveryContactNo]").val() );
   }
});

// added by leokarl 4-27-2015
$(".home-copy-details-from-primary").change(function(){
   var $self = $(this);
   var $altno = $self.closest("div.alternate").attr("data-alt");
   if($self.is(":checked")){
       $("input[name=doc_delivery_recipient_name"+$altno+"]").val( $("input[name=doc_delivery_recipient_name]").val() );
       $("input[name=doc_delivery_company"+$altno+"]").val( $("input[name=doc_delivery_company]").val() );
       $("input[name=doc_delivery_address"+$altno+"]").val( $("input[name=doc_delivery_address]").val() );
       $("input[name=doc_delivery_city"+$altno+"]").val( $("input[name=doc_delivery_city]").val() );
       $("input[name=doc_delivery_postcode"+$altno+"]").val( $("input[name=doc_delivery_postcode]").val() );
       $("input[name=doc_delivery_contact_no"+$altno+"]").val( $("input[name=doc_delivery_contact_no]").val() );
   }
});


// added by leokarl 5-11-2015
$("select[name=courier]").change(function(){
   var $self = $(this);
   var $element = $self.find('option:selected'); 
   var $s_courier_enabled = $element.attr("data-dhl"); 
   var s_act = $("input[name=docReturnCity]").val().toLowerCase().indexOf("canberra");
   
   if($s_courier_enabled == 1 && ($("select[name=docReturnState]").val() != 'ACT')){
       $("div.courier-pickup").show();
       $(".courier-required").attr("required","required");
   }else{
       $("div.courier-pickup").hide();
       $(".courier-required").removeAttr("required");
   }
});

$("#chkUseDefaultAddress").change(function(){
    $("select[name=courier]").change();
});

$("select[name=docReturnState]").change(function(){
    $("select[name=courier]").change();
});
/*
$(document).ready(function(){
    if($("body").attr("id") == 'application-visa-options'){
        // Set our timer global and give a timeout for stop typing.
        var timer, timeout = 750;


        // Watch for the user to type in the text field.
        $('input[name=docReturnCity]').keyup(function()
        {
            // Clear timer if it's set.
            if (typeof timer != undefined)
            clearTimeout(timer);

            // Set status to show we're typing.
        //        $("#status").html("Typing ...").css("color", "#009900");

            // Set status to show we're done typing on a delay.
            timer = setTimeout(function()
            {
                $("select[name=courier]").change();
            }, timeout);
        });
    }
});

*/


$("body#application-document-delivery #chkUseDefaultAddress").click(function(){
    if(this.checked === true){
         $.ajax({
            url: udetails_url,
            method: 'POST',
            data: {'client': client},
            success: function(data) { 
                data = JSON.parse(data);
                $("input[name=doc_pickup_company]").val(data['company']);
                $("input[name=pickupAddress]").val(data['address']);
                $("input[name=pickupCity]").val(data['city']);
                $("input[name=pickupPostcode]").val(data['postcode']);
                $("input[name=pickupContactNo]").val(data['phone']);
                $("input[name=doc_pickup_email]").val(data['email']);

            },
            error:function(){
            }
        });
    }else{
    }
 });