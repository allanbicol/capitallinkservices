/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    $('#gov-visa-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": list_url_visa,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null,
                {"bSearchable": false, "bVisible": false },
            ],
             "aoColumnDefs": [
                 {
                    "aTargets": [0],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        if(full[0] != ''){
                            var $datetime = full[0].split(" ");
                            var $date = $datetime[0].split("-");
                            var $day = $date[2];
                            var $month = $date[1];
                            var $year = $date[0];
                            return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return full[1];
                     }
                },
                {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" tag="'+full[5]+'" title="Destination: '+full[3]+'" onclick="trackVisaOrder($(this))"><span class="glyphicon glyphicon-play-circle"></span> Track Order</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                        var total_entries = $("#gov-visa #gov-visa-listings_info").find(".totalEntries").html();
                        $("#visaTicketsCount").html(total_entries);
                     }
    });
    
    
    $('#police-clearance-listings').dataTable( {
            "bProcessing": false,
            "sAjaxSource": list_url_police_clearance,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null,
                {"bSearchable": false, "bVisible": false },
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [0],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        if(full[0] != ''){
                            var $datetime = full[0].split(" ");
                            var $date = $datetime[0].split("-");
                            var $day = $date[2];
                            var $month = $date[1];
                            var $year = $date[0];
                            return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [2],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        var output = '';
                        var string = full[2];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                     }
                },
                {
                    "aTargets": [3],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        var output = '';
                        var string = full[3];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                     }
                },
                {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" tag="'+full[5]+'" title="Order No.: '+full[5]+'" onclick="trackPoliceClearanceOrder($(this))"><span class="glyphicon glyphicon-play-circle"></span> Track Order</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#police-clearance #police-clearance-listings_info").find(".totalEntries").html();
                    $("#policeClearanceOrderCount").html(total_entries);
                 }
    });
     
     
     $('#doc-legalisation-listings').dataTable( {
            "bProcessing": false,
            "sAjaxSource": list_url_doc_legalisation,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [0],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        if(full[0] != ''){
                           var $datetime = full[0].split(" ");
                            var $date = $datetime[0].split("-");
                            var $day = $date[2];
                            var $month = $date[1];
                            var $year = $date[0];
                            return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [2],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" tag="'+full[2]+'" title="Order No.: '+full[2]+'" onclick="trackPoliceClearanceOrder($(this))"><span class="glyphicon glyphicon-play-circle"></span> Track Order</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                    var total_entries = $("#doc-legalisation #doc-legalisation-listings_info").find(".totalEntries").html();
                    $("#docLegalisationOrderCount").html(total_entries);
                 }
    });
    
    
    $('#doc-delivery-listings').dataTable({
        "bProcessing": false,

        "sAjaxSource": doc_delivery_list_url,
        "aaSorting": [[0, 'desc']],
        "aoColumns": [ 
            {"bSearchable": true, "bVisible": false },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": false, "bVisible": false },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": false, "bVisible": false },
            {"bSearchable": true, "bVisible": false },
            null
        ],
         "aoColumnDefs": [
            {
                "aTargets": [1],
                 "mData": null,
                 "iDataSort": 0,
                 "mRender": function (data, type, full) {
                     var $datetime = full[1].split(" ");
                    var $date = $datetime[0].split("-");
                    var $day = $date[2];
                    var $month = $date[1];
                    var $year = $date[0];
                    return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                 }
             },
            {
                "aTargets": [11],
                 "mData": null,
                 "mRender": function (data, type, full) {
                       if(full[11] == 10){
                           return "Applied";
                      }else if(full[11] == 11){
                           return "Paid";
                       }else if(full[11] == 12){
                           return "Completed";
                       }else{
                           return "Undefined";
                       }
                 }
             },
            {
                "aTargets": [12],
                 "mData": null,
                 "mRender": function (data, type, full) {
                    return '<a role="button" class="btn btn-primary btn-xs" tag="'+full[12]+'" title="Order No.: '+full[12]+'" onclick="trackDocDeliveryOrder($(this))"><span class="glyphicon glyphicon-play-circle"></span> Track Order</a> ';
                    //return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[12]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                 }
             }
          ],
          "fnInitComplete": function(oSettings, json) {
            var total_entries = $("#doc-delivery #doc-delivery-listings_info").find(".totalEntries").html();
            $("#docDeliveryOrderCount").html(total_entries);
         }
    });
   
 });
 
 
 

$("#btnSaveChanges").click(function(){
    var $self = $(this);
    if($('#btnSaveChanges').attr('href') == '0'){
        $.ajax({
             url: add_note_url,
             method: 'POST',
             data: {'destination_id': $self.attr("tag"), 'note': $("#note").val()},
             async: false,
             success: function(data) {
                 data = JSON.parse(data);
                 $("#orderNotes").append('<li><span class="note-text">'+$("#note").val()+'</span><br/><span class="noted-by"><i>- by '+ data.note_by_name +' ('+ data.user_type +') '+ data.date_added +'</i></span></li>');
                 $("#note").val("");
             },
             error:function(){
             }
         });
    }else if($('#btnSaveChanges').attr('href') == '1'){
        $.ajax({
             url: add_police_clearance_note_url,
             method: 'POST',
             data: {'order_no': $self.attr("tag"), 'note': $("#note").val()},
             async: false,
             success: function(data) {
                 data = JSON.parse(data);
                 $("#orderNotes").append('<li><span class="note-text">'+$("#note").val()+'</span><br/><span class="noted-by"><i>- by '+ data.note_by_name +' ('+ data.user_type +') '+ data.date_added +'</i></span></li>');
                 $("#note").val("");
             },
             error:function(){
             }
         });
    }
});









/*======================================================
 * functions
 ======================================================*/


// refresh datatables
function RefreshTable(tableId, urlData)
{
  $.getJSON(urlData, null, function( json )
  {
    table = $(tableId).dataTable();
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<json.aaData.length; i++)
    {
      table.oApi._fnAddData(oSettings, json.aaData[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
  });
  clearconsole();
}


function trackVisaOrder(elem){
    $('#btnSaveChanges').attr('href', '0');
    $('#trackOrderBody').html('<center><img src="'+asset+'/spinner.gif"/></center>');
    $('#trackOrderModal').modal('show');
    $('#trackOrderModal').find('#title').html(elem.attr("title"));
    $('#trackOrderModal').find('#btnSaveChanges').attr("tag",elem.attr("tag"));
    $('#trackOrderModal').find('#btnSaveChanges').show();
    $.ajax({
        url: tracking_url,
        method: 'POST',
        data: {'destination_id': elem.attr("tag")},
        success: function(data) {
            $("#trackOrderBody").html(data);
        },
        error:function(){
        }
    });
}

function trackPoliceClearanceOrder(elem){
    $('#btnSaveChanges').attr('href', '1');
    $('#trackOrderBody').html('<center><img src="'+asset+'/spinner.gif"/></center>');
    $('#trackOrderModal').modal('show');
    $('#trackOrderModal').find('#title').html(elem.attr("title"));
    $('#trackOrderModal').find('#btnSaveChanges').attr("tag",elem.attr("tag"));
    $('#trackOrderModal').find('#btnSaveChanges').show();
    $.ajax({
        url: tracking_police_clearance_url,
        method: 'POST',
        data: {'order_no': elem.attr("tag")},
        success: function(data) {
            $("#trackOrderBody").html(data);
        },
        error:function(){
        }
    });
}

function trackDocDeliveryOrder(elem){
    $('#trackOrderBody').html('<center><img src="'+asset+'/spinner.gif"/></center>');
    $('#trackOrderModal').modal('show');
    $('#trackOrderModal').find('#title').html(elem.attr("title"));
    $('#trackOrderModal').find('#btnSaveChanges').hide();
    $.ajax({
        url: tracking_doc_delivery_url,
        method: 'POST',
        data: {'order_no': elem.attr("tag")},
        success: function(data) {
            $("#trackOrderBody").html(data);
        },
        error:function(){
        }
    });
}