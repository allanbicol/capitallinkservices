/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */




$(document).ready(function(){
    
    // show/hide traveller buttons
    showHideTravellerButtons();
    showHideTravelButtons();
    
    // datepicker 
    // http://www.eyecon.ro/bootstrap-datepicker/
    
    
    // typeahead
    
//   $.get(jsonpath, function(data){
//        $("input.occupation").typeahead({ source:data });
//    },'json');

    
    
});

/**
 * start: occupation options
 */
$("input.occupation").focus(function(){
    var position = $(this).offset();
    var height = $(this).height();
    $(this).addClass("on");
    $('.occupation-options').css("left",position.left + "px");
    $('.occupation-options').css("top", (position.top + height + 12) + "px");
    $('.occupation-options').css("display","block");
    
});


$("input.occupation").focusout(function(){
    $('.occupation-options').css("display","none");
    $(this).removeClass("on");
});

$('.occupation-options').find("li").mousedown(function(){
    var val = $(this).attr("tag");
    $("input.occupation.on").val(val);
    
    val = val.trim().toLowerCase();
    if(val == 'a dependant' || val == 'a spouse'){
        $("input.occupation.on").parent().next('.rp-info').css('display','block');
        $("input.occupation.on").parent().next('.rp-info').find('input').attr('required','required');
    }else{
        $("input.occupation.on").parent().next('.rp-info').css('display','none');
        $("input.occupation.on").parent().next('.rp-info').find('input').removeAttr('required');
    }
});

$("input.occupation").keyup(function(){
   if($(this).val().trim().toLowerCase() == 'a dependant' || $(this).val().trim().toLowerCase() == 'a spouse'){
        $(this).parent().next('.rp-info').css('display','block');
        $(this).parent().next('.rp-info').find('input').attr('required','required');
   }else{
        $(this).parent().next('.rp-info').css('display','none');
        $(this).parent().next('.rp-info').find('input').removeAttr('required');
   }
});
/**
 * end: occupation options
 */



// date-picker icon button event
$(".date-picker-button").click(function(){
   $(this).parent().find(".date-picker").focus();
});



// entry option select
$(".entry-option").click(function(){
    $(this).parent().parent().parent().parent().find(".entry-option-value").val($(this).val());
    if($(this).val() == 1){
        $(this).parent().parent().parent().parent().parent().find("#lbl-entry-date-country").html("Entry Date to Country");
        $(this).parent().parent().parent().parent().parent().find("#lbl-depart-date-country").html("Departure Date from Country");
    }else{
        $(this).parent().parent().parent().parent().parent().find("#lbl-entry-date-country").html("First Entry Date to Country");
        $(this).parent().parent().parent().parent().parent().find("#lbl-depart-date-country").html("Last Departure Date from Country");
    }
});





// add travel button event
$("#travel-details .add").click(function(e){
   var elem = $(this);
   var travel_group_length = $(".travel-details-group .group").length;
   if(travel_group_length <= 4){
       $(".travel-details-group").append('\n\
        <div class="group">\n\
            <hr/>\n\
            <div class="form-group">\n\
                <label>Choose Destination</label>\n\
                <select class="form-control destination required" name="destination[]" required>\n\
                </select>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label>Departure Date from Australia</label>\n\
                <!--<input type="date" class="form-control required" name="departureDate[]" required/>-->\n\
                <div class="input-group date">\n\
                    <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="departureDate[]" placeholder="DD-MM-YYYY" required></input>\n\
                    <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                </div>\n\
            </div>\n\
            \n\
             <div class="form-group">\n\
                <ul>\n\
                    <li><label><input type="radio" name="entryOptionSelect'+travel_group_length+'[]" value="1" class="entry-option"> Single Entry</label></li>\n\
                    <li><label><input type="radio" name="entryOptionSelect'+travel_group_length+'[]" value="2" class="entry-option"> Double Entry</label></li>\n\
                    <li><label><input type="radio" name="entryOptionSelect'+travel_group_length+'[]" value="3" class="entry-option"> Multiple Entry</label></li>\n\
                </ul>\n\
                <input type="hidden" class="entry-option-value" name="entryOption[]" value="0">\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label id="lbl-entry-date-country">First Entry Date to Country</label>\n\
                <!--<input type="date" class="form-control required" name="entryDateCounty[]" required/>-->\n\
                <div class="input-group date">\n\
                    <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="entryDateCounty[]" placeholder="DD-MM-YYYY" required></input>\n\
                    <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                </div>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label id="lbl-depart-date-country">Last Departure Date from Country</label>\n\
                <!--<input type="date" class="form-control required" name="departureDateCountry[]" required/>-->\n\
                <div class="input-group date">\n\
                    <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="departureDateCountry[]" placeholder="DD-MM-YYYY" required></input>\n\
                    <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                </div>\n\
            </div>\n\
            <div class="form-group">\n\
                <label id="lbl-followup-date-country">Follow Up Date <span class="glyphicon glyphicon-question-sign tips" data-toggle="tooltip" data-placement="right" title="This is an optional date field to help your internal process. Our system will email you on this date so you can check up on the order manually."></span></label>\n\
                <!--<input type="date" class="form-control required" name="followUpDate[]" required/>-->\n\
                <div class="input-group date">\n\
                    <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="followUpDate[]" placeholder="DD-MM-YYYY"></input>\n\
                    <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                </div>\n\
            </div>\n\
            \n\
            <div class="form-group">\n\
                <label>Purpose of Travel</label>\n\
                <textarea class="form-control purpose-text required" name="travelPurpose[]" required placeholder="[NOTE] - Many diplomatic missions will not accept generic \'Government Business\' as a purpose of travel. Please clearly state the reason for your travel including some level of detail"></textarea>\n\
                <p class="note">PLEASE write your purpose for travel in a format that will work in a sentence that starts with the words \'applicant is travelling to \'country name’ for the purpose of… \' For example: \'…for the purpose of attending meetings with staff at the Australian Embassy in Moscow\'</p>\n\
            </div>\n\
        </div>\n\
        ');
        
        
        // copy travel options from the main travel to additional traveller
        $(".travel-details-group .group:last-child").find("select.destination").append($("select.main-destination").html());
        
        // datepicker 
        // http://www.eyecon.ro/bootstrap-datepicker/
        $('.date-picker').datepicker({
            format : "dd-mm-yyyy"
        });
        
        // date-picker icon button event
        $(".date-picker-button").click(function(){
           $(this).parent().find(".date-picker").focus();
        });
    
        // entry option select
        $(".entry-option").click(function(){
            $(this).parent().parent().parent().parent().find(".entry-option-value").val($(this).val());
            if($(this).val() == 1){
                $(this).parent().parent().parent().parent().parent().find("#lbl-entry-date-country").html("Entry Date to Country");
                $(this).parent().parent().parent().parent().parent().find("#lbl-depart-date-country").html("Departure Date from Country");
            }else{
                $(this).parent().parent().parent().parent().parent().find("#lbl-entry-date-country").html("First Entry Date to Country");
                $(this).parent().parent().parent().parent().parent().find("#lbl-depart-date-country").html("Last Departure Date from Country");
            }
        });


        // show/hide travel buttons
        showHideTravelButtons()
        
        // smooth scroll
        $('html, body').animate({
            scrollTop: $(".travel-details-group .group:last-child").offset().top
        }, "slow");
   }
});





// remove travel button event
$("#travel-details .remove").click(function(e){
    
    // remove last added travel details
    $(".travel-details-group .group:last-child").remove();
    
    // show/hide travel buttons
    showHideTravelButtons();
    
    // smooth scroll
    $('html, body').animate({
        scrollTop: $(".travel-details-group .group:last-child").offset().top
    }, "slow");
});







// add traveller button event
$("#traveller-details .add").click(function(e){
    var elem = $(this);
    
    if($(".traveller-details-group .group").length <= 6){
        if($(".traveller-details-group .group:first-child").find("select.nearest-capital-city").length > 0){
            $(".traveller-details-group").append('\n\
            <div class="group">\n\
                <hr />\n\
                <div class="form-group">\n\
                    <label>Title</label>\n\
                    <select class="form-control title required" name="travellerTitle[]" required>\n\
                    </select>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>First name</label>\n\
                    <input type="text" class="form-control required" name="travellerFname[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Middle name</label>\n\
                    <input type="text" class="form-control" name="travellerMname[]"/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Last name</label>\n\
                    <input type="text" class="form-control required" name="travellerLname[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Email</label>\n\
                    <input type="email" class="form-control required" name="travellerEmail[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Organisation</label>\n\
                    <input type="text" class="form-control required" name="travellerOrganisation[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Occupation</label>\n\
                    <input type="text" class="form-control required occupation" name="travellerOccupation[]" autocomplete="off" required/>\n\
                </div>\n\
                <div class="form-group rp-info" style="display:none">\n\
                    <p><strong>Please enter related person\'s information:</strong></p>\n\
                    <div class="row">\n\
                        <div class="col-lg-6">\n\
                            Fullname\n\
                        </div>\n\
                        <div class="col-lg-6">\n\
                            <input type="text" name="rpinfoFullname[]" class="form-control"/>\n\
                        </div>\n\
                    </div>\n\
    \n\
                    <div class="row">\n\
                        <div class="col-lg-6">\n\
                            Position at post\n\
                        </div>\n\
                        <div class="col-lg-6">\n\
                            <input type="text" name="rpinfoPositionAtPost[]" class="form-control"/>\n\
                        </div>\n\
                    </div>\n\
    \n\
                    <div class="row">\n\
                        <div class="col-lg-6">\n\
                            Name of post\n\
                        </div>\n\
                        <div class="col-lg-6">\n\
                            <input type="text" name="rpinfoNameOfPost[]" class="form-control"/>\n\
                        </div>\n\
                    </div>\n\
    \n\
                    <div class="row">\n\
                        <div class="col-lg-6">\n\
                            City\n\
                        </div>\n\
                        <div class="col-lg-6">\n\
                            <input type="text" name="rpinfoCity[]" class="form-control"/>\n\
                        </div>\n\
                    </div>\n\
    \n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Phone</label>\n\
                    <input type="phone" class="form-control required" name="travellerPhone[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Date of Birth</label>\n\
                    <!--<input type="date" class="form-control required" name="travellerBirthDate[]" required/>-->\n\
                    <div class="input-group date">\n\
                        <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="travellerBirthDate[]" placeholder="DD-MM-YYYY" required></input>\n\
                        <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                    </div>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Gender</label>\n\
                    <select name="travellerGender[]" class="form-control gender">\n\
                        <option value="male"> Male </option>\n\
                        <option value="female"> Female </option>\n\
                    </select>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Nearest Capital City</label>\n\
                    <select name="travellerNearestCapitalCity[]" class="form-control nearest-capital-city">\n\
                    </select>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Passport type</label>\n\
                    <select class="form-control password-type required" name="travellerPassportType[]" required>\n\
                    </select>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Nationality</label>\n\
                    <select class="form-control nationality required" name="travellerNationality[]" required>\n\
                    </select>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Passport Number</label>\n\
                    <input type="text" class="form-control required" name="travellerPassportNumber[]" required/>\n\
                </div>\n\
            </div>');

        }else{
            
            $(".traveller-details-group").append('\n\
            <div class="group">\n\
                <hr />\n\
                <div class="form-group">\n\
                    <label>Title</label>\n\
                    <select class="form-control title required" name="travellerTitle[]" required>\n\
                    </select>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>First name</label>\n\
                    <input type="text" class="form-control required" name="travellerFname[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Middle name</label>\n\
                    <input type="text" class="form-control" name="travellerMname[]"/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Last name</label>\n\
                    <input type="text" class="form-control required" name="travellerLname[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Email</label>\n\
                    <input type="email" class="form-control required" name="travellerEmail[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Organisation</label>\n\
                    <input type="text" class="form-control required" name="travellerOrganisation[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Occupation</label>\n\
                    <input type="text" class="form-control required occupation" name="travellerOccupation[]" autocomplete="off" required/>\n\
                </div>\n\
                <div class="form-group rp-info" style="display:none">\n\
                    <p><strong>Please enter related person\'s information:</strong></p>\n\
                    <div class="row">\n\
                        <div class="col-lg-6">\n\
                            Fullname\n\
                        </div>\n\
                        <div class="col-lg-6">\n\
                            <input type="text" name="rpinfoFullname[]" class="form-control"/>\n\
                        </div>\n\
                    </div>\n\
    \n\
                    <div class="row">\n\
                        <div class="col-lg-6">\n\
                            Position at post\n\
                        </div>\n\
                        <div class="col-lg-6">\n\
                            <input type="text" name="rpinfoPositionAtPost[]" class="form-control"/>\n\
                        </div>\n\
                    </div>\n\
    \n\
                    <div class="row">\n\
                        <div class="col-lg-6">\n\
                            Name of post\n\
                        </div>\n\
                        <div class="col-lg-6">\n\
                            <input type="text" name="rpinfoNameOfPost[]" class="form-control"/>\n\
                        </div>\n\
                    </div>\n\
    \n\
                    <div class="row">\n\
                        <div class="col-lg-6">\n\
                            City\n\
                        </div>\n\
                        <div class="col-lg-6">\n\
                            <input type="text" name="rpinfoCity[]" class="form-control"/>\n\
                        </div>\n\
                    </div>\n\
    \n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Phone</label>\n\
                    <input type="phone" class="form-control required" name="travellerPhone[]" required/>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Date of Birth</label>\n\
                    <!--<input type="date" class="form-control required" name="travellerBirthDate[]" required/>-->\n\
                    <div class="input-group date">\n\
                        <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="travellerBirthDate[]" placeholder="DD-MM-YYYY" required></input>\n\
                        <span class="input-group-addon date-picker-button"><span class="glyphicon glyphicon-calendar"></span></span>\n\
                    </div>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Passport type</label>\n\
                    <select class="form-control password-type required" name="travellerPassportType[]" required>\n\
                    </select>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Nationality</label>\n\
                    <select class="form-control nationality required" name="travellerNationality[]" required>\n\
                    </select>\n\
                </div>\n\
        \n\
                <div class="form-group">\n\
                    <label>Passport Number</label>\n\
                    <input type="text" class="form-control required" name="travellerPassportNumber[]" required/>\n\
                </div>\n\
            </div>');
            
        }
        // copy traveler options from the main traveler to additional traveller
        $(".traveller-details-group .group:last-child").find("select.title").append($("select.main-title").html());
        $(".traveller-details-group .group:last-child").find("select.password-type").append($("select.main-password-type").html());
        $(".traveller-details-group .group:last-child").find("select.nationality").append($("select.main-nationality").html());
        if($(".traveller-details-group .group:first-child").find("select.nearest-capital-city").length > 0){
            $(".traveller-details-group .group:last-child").find("select.nearest-capital-city").append( $(".traveller-details-group .group:first-child").find("select.nearest-capital-city").html());
        }

        // datepicker 
        // http://www.eyecon.ro/bootstrap-datepicker/
        $('.date-picker').datepicker({
            format : "dd-mm-yyyy"
        });

        // date-picker icon button event
        $(".date-picker-button").click(function(){
           $(this).parent().find(".date-picker").focus();
        });

        // show/hide traveller buttons
        showHideTravellerButtons();

        // auto complete 
//        $.get(jsonpath, function(data){
//            $("input.occupation").typeahead({ source:data });
//        },'json');
        /**
            * start: occupation options
            */
           $("input.occupation").focus(function(){
               var position = $(this).offset();
               var height = $(this).height();
               $(this).addClass("on");
               $('.occupation-options').css("left",position.left + "px");
               $('.occupation-options').css("top", (position.top + height + 12) + "px");
               $('.occupation-options').css("display","block");
           });


           $("input.occupation").focusout(function(){
               $('.occupation-options').css("display","none");
               $(this).removeClass("on");
           });

           $('.occupation-options').find("li").mousedown(function(){
               var val = $(this).attr("tag");
               $("input.occupation.on").val(val);
           });
           /**
            * end: occupation options
            */

        // smooth scroll
        $('html, body').animate({
            scrollTop: $(".traveller-details-group .group:last-child").offset().top
        }, "slow");

    }
    
    
});






// remove traveller button event
$("#traveller-details .remove").click(function(e){
    
    // remove last added traveller details
    $(".traveller-details-group .group:last-child").remove();
    
    // show/hide traveller buttons
    showHideTravellerButtons();
    
    // smooth scroll
    $('html, body').animate({
        scrollTop: $(".traveller-details-group .group:last-child").offset().top
    }, "slow");
});






// use my details event
$("#useMyDetails").click(function(){
    if(this.checked == true){
        $.ajax({
            url: url,
            method: 'POST',
            data: {'client': client},
            success: function(data) {
                // parse the jason data property 
                // into javascript array
                var data = $.parseJSON(data);
                
                // fill out fields
                $("#mainTitle").val(data["title"]);
                $("#mainFname").val(data["fname"]);
                $("#mainLname").val(data["lname"]);
                $("#mainEmail").val(data["email"]);
                $("#mainPhone").val(data["phone"]);
                $("#mainNationality").val(data["country_id"]);
            },
            error:function(){
            }
        });
    }else{
        
        // clear fields
        $("#mainTitle").val("");
        $("#mainFname").val("");
        $("#mainLname").val("");
        $("#mainEmail").val("");
        $("#mainPhone").val("");
        $("#mainNationality").val("");
    }
});




// save progress event
$("#saveProgress").click(function(){
   $(".required").removeAttr("required");
   $("#hidSubmit").val(0);
});





// net page event
$("#nextPage").click(function(){
   $(".required").attr("required", "required");
   $("#hidSubmit").val(1);
});








/*======================================================
 * functions
 ======================================================*/


// show/hide travel buttons
function showHideTravelButtons(){
    // show/hide add button
    if($(".travel-details-group .group").length >= 5){
        $("#travel-details .add").hide();
    }else{
        $("#travel-details .add").show();
    }
    
    // show/hide remove button
    if($(".travel-details-group .group").length > 1){
        $("#travel-details .remove").show();
    }else{
        $("#travel-details .remove").hide();
    }
}

// show/hide traveller buttons
function showHideTravellerButtons(){
    // show/hide add button
    if($(".traveller-details-group .group").length >= 6){
        $("#traveller-details .add").hide();
    }else{
        $("#traveller-details .add").show();
    }
    
    // show/hide remove button
    if($(".traveller-details-group .group").length > 1){
        $("#traveller-details .remove").show();
    }else{
        $("#traveller-details .remove").hide();
    }
}