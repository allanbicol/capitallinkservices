/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $("select.country").change();
    
    if (typeof new_pass_req !== 'undefined') {
    checkpassword($('input[name="password"]').val());
        if(new_pass_req==1){
            $('#modalNew').modal('show');
        }
    }

});

$("body").on('change','select.country',function(){
   var $self = $(this);
   $.ajax({
        url: visa_types_url,
        method: 'POST',
        data: {'country_id': $self.val()},
        success: function(data) {
            $self.parents().eq(3).find('select.visa-type').html(data);
        },
        error:function(){
        }
    });
});


$("button.add-country").click(function(){
    
    $("#destinations").append('\n\
        <div class="panel panel-default group">\n\
            <div class="panel-body">\n\
                <div class="row">\n\
                    <div class="col-lg-4">\n\
                        <div class="form-group">\n\
                            <label>Country</label>\n\
                            <select class="form-control country" name="country[]"  required>\n\
                            </select>\n\
                        </div>\n\
                    </div>\n\
\n\
                    <div class="col-lg-4">\n\
                        <div class="form-group">\n\
                            <label>Entry Date</label>\n\
                            <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="entry_date[]" placeholder="DD-MM-YYYY" required></input>\n\
                        </div>\n\
                    </div>\n\
\n\
                    <div class="col-lg-4">\n\
                        <div class="form-group">\n\
                            <label>Exit Date</label>\n\
                            <input class="form-control date-picker" data-format="dd-MM-yyyy" type="text" name="exit_date[]" placeholder="DD-MM-YYYY" required></input>\n\
                        </div>\n\
                    </div>\n\
\n\
                </div>\n\
\n\
                <div class="row">\n\
\n\
\n\
                    <div class="col-lg-4">\n\
                        <div class="form-group">\n\
                            <label>Visa type</label>\n\
                            <select class="form-control visa-type" name="visa_type[]" required>\n\
                                <option>Select type</option>\n\
                            </select>\n\
                        </div>\n\
                    </div>\n\
\n\
                    <div class="col-lg-4">\n\
                        <div class="form-group">\n\
                            <label>No. of Entries</label>\n\
                            <input class="form-control" type="number" name="no_of_entries[]" required></input>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    ');
    
   $("#destinations .group:last-child").find("select.country").append($("select.main-country").html());
   
   $('.date-picker').datepicker({
        format : "dd-mm-yyyy"
    });
});


$('input[name="password"]').keyup(function(){
    var $self = $(this);
    checkpassword($self.val());
});

$('#btnUpdatePassword').click(function(){
    var pass = $('input[name="password"]').val();
    var con_pass = $('input[name="passwordConfirm"]').val();
    if(pass !='' ){
        if(con_pass!=pass){
            alert('Password should be match!');
            return false;
        }
        $.ajax({
            url: update_pass_url,
            method: 'POST',
            data: {'password': pass,'passwordConfirm':con_pass},
            success: function(data) {
                if(data==0){
                    
                    $('#msg_div').show();
                    $('#msg').html('Your password has been successfully updated.');
                    $('#btnDone').show();
                    $('#btnUpdatePassword').hide();
                }
            }, 
            error:function(){
            }
        });
    }else{
        alert('Password field is required!');
    }
});

function checkpassword(pass){
    $.ajax({
        url: check_pass_url,
        method: 'POST',
        data: {'pass': pass},
        success: function(data) {
            var res = data.split('#');
            if(res[0]==1){
                $('#let').addClass('glyphicon-ok-circle').addClass('green');
                $('#let').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#let').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#let').addClass('glyphicon-remove-circle').addClass('red');
            }
            
            if(res[1]==1){
                $('#num').addClass('glyphicon-ok-circle').addClass('green');
                $('#num').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#num').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#num').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[2]==1){
                $('#spe').addClass('glyphicon-ok-circle').addClass('green');
                $('#spe').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#spe').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#spe').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[3]==1){
                $('#len').addClass('glyphicon-ok-circle').addClass('green');
                $('#len').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#len').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#len').addClass('glyphicon-remove-circle').addClass('red');
            }
        },
        error:function(){
        }
    });
}
