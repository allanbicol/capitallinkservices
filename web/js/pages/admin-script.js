$(document).ready(function(){
    
    // Home Page
    if($("body#admin-home").length > 0){
        $("ul#ticketTabs li:first-child").click(); 

        var oTable;

        /* Init the table */
          oTable =  $('#gov-visa-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": gov_visa_list_url,
                  "aaSorting": [[0, 'desc']],
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": false },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": false },
                      null,
                      {"bSearchable": false, "bVisible": false }
                  ],
                   "aoColumnDefs": [
                       {
                          "aTargets": [1],
                           "mData": null,
                           "iDataSort": 0,
                           "mRender": function (data, type, full) {
                               var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                           }
                       },
                       {
                          "aTargets": [3],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return full[3];
                           }
                       },
                       {
                          "aTargets": [8],
                           "mData": null,
                           "mRender": function (data, type, full) {
                                 if(full[8] == 10){
                                     return "Applied";
                                 }else if(full[8] == 11){
                                     return "Paid";
                                 }else if(full[8] == 12){
                                     return "Completed";
                                 }else{
                                     return "Rejected";
                                 }
                           }
                       },
                       {
                          "aTargets": [10],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+open_order_url+'?order_no='+ full[11] +'"><span class="glyphicon glyphicon-th-list"></span> View</a>';
                           }
                       }
                    ],
                    
                    "fnInitComplete": function(oSettings, json) {
                        var total_entries = $("#gov-visa #gov-visa-listings_info").find(".totalEntries").html();
                        $("#visaTicketsCount").html(total_entries);
                     }
          } );

var oTable;

         /* Init the table */
          oTable =  $('#tpn-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": tpn_list_url,
                  "aaSorting": [[0, 'desc']],
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": false },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": false },
                      null,
                      {"bSearchable": false, "bVisible": false }
                  ],
                   "aoColumnDefs": [
                        {
                          "aTargets": [1],
                           "mData": null,
                           "iDataSort": 0,
                           "mRender": function (data, type, full) {
                               var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                           }
                       },
                       {
                          "aTargets": [3],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return full[3];
                           }
                       },
                       {
                          "aTargets": [8],
                           "mData": null,
                           "mRender": function (data, type, full) {
                                if(full[8] == 10){
                                     return "Applied";
                                }else if(full[8] == 11){
                                     return "Paid";
                                 }else if(full[8] == 12){
                                     return "Completed";
                                 }else{
                                     return "Rejected";
                                 }
                           }
                       },
                       {
                          "aTargets": [10],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+open_order_url+'?order_no='+ full[11] +'"><span class="glyphicon glyphicon-th-list"></span> View</a>';
                           }
                       }
                    ],
                    "fnInitComplete": function(oSettings, json) {
                        var total_entries = $("#tpn #tpn-listings_info").find(".totalEntries").html();
                        $("#tpnTicketsCount").html(total_entries);
                     }
          } );

          
          var oTable;

         /* Init the table */
          oTable =  $('#passport-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": passport_list_url,
                  "aaSorting": [[0, 'desc']],
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": false },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      null
                  ],
                   "aoColumnDefs": [
                        {
                          "aTargets": [1],
                           "mData": null,
                           "iDataSort": 0,
                           "mRender": function (data, type, full) {
                               var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                           }
                       },
                               
                        {
                          "aTargets": [2],
                           "mData": null,
                           "iDataSort": 0,
                           "mRender": function (data, type, full) {
                               
                               return full[2].replace(/,/gi,"<br/>");
                           }
                       },
                       {
                          "aTargets": [7],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              if(full[7] == 10){
                                  return 'Ordered';
                              }else if(full[7] == 11){
                                     return "Paid";
                              }else if(full[7] == 12){
                                  return 'Completed';
                              }else{
                                  return 'Undefined';
                              }
                           }
                       },
                       {
                          "aTargets": [8],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+open_order_url+'?order_no='+ full[8] +'"><span class="glyphicon glyphicon-th-list"></span> View</a> <!--<a class="btn btn-info btn-xs" href="'+edit_passport_order_url+'?order_no='+ full[8] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a>-->';
                           }
                       },
                    ],
                    "fnInitComplete": function(oSettings, json) {
                        var total_entries = $("#passport #passport-listings_info").find(".totalEntries").html();
                        $("#passportOrderCount").html(total_entries);
                     }
          } );
          
          
          
          
          
          var oTable;

         /* Init the table */
          oTable =  $('#police-clearance-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": public_police_clearance_list,
                  "aaSorting": [[0, 'desc']],
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": false },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": false, "bVisible": true }
                  ],
                   "aoColumnDefs": [
                        {
                          "aTargets": [1],
                           "mData": null,
                           "iDataSort": 0,
                           "mRender": function (data, type, full) {
                               var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                           }
                       },
                        {
                             "aTargets": [4],
                              "mData": null,
                              "mRender": function (data, type, full) {
                                 var output = '';
                                 var string = full[4];
                                 string = string.split(",");

                                 for(i=0; i<string.length; i++){
                                     output = output  + string[i] + '<br/>';
                                 }
                                 return output;
                              }
                         },
                       {
                          "aTargets": [6],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              if(full[6] == 10){
                                  return 'Ordered';
                              }else if(full[6] == 11){
                                     return "Paid";
                              }else if(full[6] == 12){
                                  return 'Completed';
                              }else{
                                  return 'Undefined';
                              }
                           }
                       },
                       {
                          "aTargets": [7],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+open_order_url+'?order_no='+ full[7] +'"><span class="glyphicon glyphicon-th-list"></span> View</a>';
                           }
                       },
                    ],
                    "fnInitComplete": function(oSettings, json) {
                        var total_entries = $("#police-clearance #police-clearance-listings_info").find(".totalEntries").html();
                        $("#policeClearanceOrderCount").html(total_entries);
                     }
          } );
          
          
          
          
          var oTable;

        /* Init the table */
          oTable =  $('#pub-visa-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": public_visa_list_url,
                  "aaSorting": [[0, 'desc']],
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": false },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": false },
                      null,
                      {"bSearchable": false, "bVisible": false }
                  ],
                   "aoColumnDefs": [
                        {
                          "aTargets": [1],
                           "mData": null,
                           "iDataSort": 0,
                           "mRender": function (data, type, full) {
                               var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                           }
                       },
                       {
                          "aTargets": [3],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return full[3];
                           }
                       },
                       {
                          "aTargets": [8],
                           "mData": null,
                           "mRender": function (data, type, full) {
                                 if(full[8] == 10){
                                     return "Applied";
                                }else if(full[8] == 11){
                                     return "Paid";
                                 }else if(full[8] == 12){
                                     return "Completed";
                                 }else{
                                     return "Rejected";
                                 }
                           }
                       },
                       {
                          "aTargets": [10],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+open_order_url+'?order_no='+ full[11] +'"><span class="glyphicon glyphicon-th-list"></span> View</a>';
                           }
                       }
                    ],
                    "fnInitComplete": function(oSettings, json) {
                        var total_entries = $("#pub-visa #pub-visa-listings_info").find(".totalEntries").html();
                        $("#publicVisaOrderCount").html(total_entries);
                     }
          } );
          
          
          
          
          $('#doc-delivery-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": doc_delivery_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": false, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": false, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                        var $date = $datetime[0].split("-");
                        var $day = $date[2];
                        var $month = $date[1];
                        var $year = $date[0];
                        return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                {
                    "aTargets": [11],
                     "mData": null,
                     "mRender": function (data, type, full) {
                           if(full[11] == 10){
                               return "Applied";
                          }else if(full[11] == 11){
                               return "Paid";
                           }else if(full[11] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [12],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[12]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                var total_entries = $("#doc-delivery #doc-delivery-listings_info").find(".totalEntries").html();
                $("#docDeliveryOrderCount").html(total_entries);
             }
        });
        
        
        
        
        $('#russian-visa-voucher-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": russian_visa_voucher_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                        var $date = $datetime[0].split("-");
                        var $day = $date[2];
                        var $month = $date[1];
                        var $year = $date[0];
                        return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[6] == 10){
                               return "Applied";
                          }else if(full[6] == 11){
                               return "Paid";
                           }else if(full[6] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [7],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[7]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                var total_entries = $("#russian-visa-voucher #russian-visa-voucher-listings_info").find(".totalEntries").html();
                $("#russianVisaVoucherOrderCount").html(total_entries);
             }
        });
        
        
        $('#doc-legalisation-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": document_legalisation_list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                            var $date = $datetime[0].split("-");
                            var $day = $date[2];
                            var $month = $date[1];
                            var $year = $date[0];
                            return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[5] == 10){
                               return "Applied";
                          }else if(full[5] == 11){
                               return "Paid";
                           }else if(full[5] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [3],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return full[3] + " " + full[4];
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[6]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "fnInitComplete": function(oSettings, json) {
                var total_entries = $("#document-legalisation #doc-legalisation-listings_info").find(".totalEntries").html();
                $("#docLegalisationCount").html(total_entries);
             }
        });
          
    // Clients Page
    }else if($("body#admin-clients").length > 0){
        
        if(typeof client_type !== 'undefined' && client_type == 'government'){
            $("#govClient").attr("checked", "checked");
            $("#privateClient").removeAttr("checked");
            $("#corporateClient").removeAttr("checked");
            $("#bulkVisaClient").removeAttr("checked");
            
            $("body#admin-clients").find("div#department").show();
            $("body#admin-clients").find("div#company").hide();

            $("body#admin-clients").find("div#department > select").attr("required","required");
            $("body#admin-clients").find("div#company > input").removeAttr("required");
        }else{
            $("#privateClient").attr("checked", "checked");
            $("#govClient").removeAttr("checked");
            
            $("body#admin-clients").find("div#department").hide();
            $("body#admin-clients").find("div#company").show();

            $("body#admin-clients").find("div#department > select").removeAttr("required");
            $("body#admin-clients").find("div#company > input").attr("required","required");
           
        }
        
        var oTable;

         /* Init the table */
          oTable =  $('#client-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": list_url,
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      null,
                  ],
                   "aoColumnDefs": [
                       {
                           "aTargets": [5],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return toTitleCase(full[5]) + ' ' + 'Client';
                           }
                       },
                       {
                          "aTargets": [7],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?user='+ full[7] +'"><span class="glyphicon glyphicon-log-in"></span> Open</a> ' +
                                      '<a class="btn btn-danger btn-xs" onclick="javascript: deleteClient('+ full[7] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                           }
                       }
                    ],
                    "fnInitComplete": function(oSettings, json) {
                        
                     }
          } );
          
          
          
    }else if($("body#admin-staff").length > 0){

        var oTable;

         /* Init the table */
          oTable =  $('#staff-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": list_url,
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": false },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": false, "bVisible": false },
                      null,
                  ],
                   "aoColumnDefs": [
                       {
                           "aTargets": [0],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return full[0] + " " + full[1];
                           }
                       },
                        {
                            "aTargets": [3],
                             "mData": null,
                             "mRender": function (data, type, full) {
                                if(full[3] != ''){
                                    var $fulldate = full[3].split(" ");
                                    var $date = $fulldate[0].split("-");
                                    var $day = $date[2];
                                    var $month = $date[1];
                                    var $year = $date[0];
                                    return $day + '-' + $month + '-' + $year + ' ' + $fulldate[1];
                                }else{
                                    return '';
                                }
                             }
                         },
                       {
                           "aTargets": [4],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return (full[4] == 1) ? 'Enabled' : 'Disabled';
                           }
                       },
                       {
                          "aTargets": [5],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?id='+ full[5] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a> ' +
                                      '<a class="btn btn-danger btn-xs" onclick="javascript: deleteStaff('+ full[5] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                           }
                       }
                    ],
                    "fnInitComplete": function(oSettings, json) {
                        
                     }
          } );

          
          
     
    }else if($("body#admin-embassy").length > 0){

        var oTable;

         /* Init the table */
          oTable =  $('#embassy-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": list_url,
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": false },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": false, "bVisible": false },
                      null,
                  ],
                   "aoColumnDefs": [
                       {
                           "aTargets": [0],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return full[0] + " " + full[1];
                           }
                       },
                       {
                           "aTargets": [3],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return (full[3] == 1) ? 'Enabled' : 'Disabled';
                           }
                       },
                       {
                          "aTargets": [4],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?id='+ full[4] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a> '+
                                      '<a class="btn btn-danger btn-xs" onclick="javascript: deleteEmbassy('+ full[4] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                           }
                       }
                    ],
                    "fnInitComplete": function(oSettings, json) {
                        
                     }
          } );   
          
          
    // Manage Departments Page
    }else if($("body#admin-manage-departments").length > 0){
        var oTable;

         /* Init the table */
          oTable =  $('#deparment-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": list_url,
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      null,
                  ],
                   "aoColumnDefs": [
                       {
                          "aTargets": [2],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?id='+ full[2] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a> ' +
                                      '<a class="btn btn-danger btn-xs" onclick="javascript: deleteRecord('+ full[2] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                           }
                       }
                    ],
                  "fnInitComplete": function(oSettings, json) {
                        
                  }
          } );
          
          
          
          
          
          
    // Manage Discount Page
    }else if($("body#admin-manage-discount").length > 0){
        var oTable;

         /* Init the table */
          oTable =  $('#discount-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": list_url,
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      null,
                  ],
                   "aoColumnDefs": [
                       {
                          "aTargets": [2],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return full[2] + '%';
                           }
                       },
                       {
                          "aTargets": [3],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?id='+ full[3] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a> ' +
                                      '<a class="btn btn-danger btn-xs" onclick="javascript: deleteRecord('+ full[3] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                           }
                       }
                  ],
                  "fnInitComplete": function(oSettings, json) {
                        
                  }
          } );
          
          
    }else if($("body#admin-manage-travel-alerts").length > 0){
        var oTable;

         /* Init the table */
          oTable =  $('#discount-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": list_url,
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      null,
                  ],
                   "aoColumnDefs": [
                        {
                          "aTargets": [0],
                           "mData": null,
                           //"iDataSort": 0,
                           "mRender": function (data, type, full) {
                               var $date = full[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year;
                           }
                       },
                       {
                          "aTargets": [3],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?id='+ full[3] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a> ' +
                                      '<a class="btn btn-danger btn-xs" onclick="javascript: deleteRecord('+ full[3] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                           }
                       }
                  ],
                  "fnInitComplete": function(oSettings, json) {
                        
                  }
          } );
          
          
    // Manage Visa Courier Options Page
    }else if($("body#admin-manage-visa-courier-options").length > 0){
        var oTable;

         /* Init the table */
          oTable =  $('#visa-courier-options-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": list_url,
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      null,
                  ],
                   "aoColumnDefs": [
                       {
                          "aTargets": [2],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return (full[2] == 1) ? 'Active' : 'Inactive';
                           }
                       },
                       {
                          "aTargets": [3],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?id='+ full[3] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a> ' +
                                      '<a class="btn btn-danger btn-xs" onclick="javascript: deleteRecord('+ full[3] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                           }
                       }
                  ],
                  "fnInitComplete": function(oSettings, json) {
                        
                  }
          } );
    }else if($("body#admin-manage-visas").length > 0){
        
        $('div.additional-requirements .content').sortable({placeholder: "ui-state-highlight",helper:'clone'});
        
        bkLib.onDomLoaded(function() {
            new nicEditor({
                iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
                fullPanel : true,
                //buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'left', 'center', 'right', 'justify', 'removeformat','xhtml']
            }).panelInstance('visaInformation');
        });
        
       
    }else if($("body#admin-manage-public-visas").length > 0){
        
        $('div.additional-requirements .content').sortable({placeholder: "ui-state-highlight",helper:'clone'});
        
        
        for(i=0; i<$('div.visa-types > .type-group').length; i++){
            bkLib.onDomLoaded(function() {
                new nicEditor({
                    iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
                    fullPanel : true,
                    //buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'left', 'center', 'right', 'justify', 'removeformat','xhtml']
                }).panelInstance('visaInformation['+ i +']');
            });
        }
        
        
        
        
        
        
    }else if($("body#admin-view-order").length > 0 ){
        
        
        
        
        
    }else if($("body#passport-office-pickup-delivery").length > 0){
        
        
        
        
        
        
    }else if($("body#admin-manage-police-clearances").length > 0){
        if($('#police-clearance-listings').length > 0){
            var oTable;

             /* Init the table */
              oTable =  $('#police-clearance-listings').dataTable({
                      "bProcessing": false,
                      "sAjaxSource": list_url,
                      "aoColumns": [ 
                          {"bSearchable": true, "bVisible": true },
                          {"bSearchable": true, "bVisible": true },
                          {"bSearchable": true, "bVisible": true },
                          null,
                      ],
                       "aoColumnDefs": [
                           {
                              "aTargets": [1],
                               "mData": null,
                               "mRender": function (data, type, full) {
                                   var price = parseFloat(full[1]);
                                  return '$' + price.toFixed(2);
                               }
                           },
                           {
                              "aTargets": [2],
                               "mData": null,
                               "mRender": function (data, type, full) {
                                  return (full[2] == 1) ? 'Active' : 'Inactive';
                               }
                           },
                           {
                              "aTargets": [3],
                               "mData": null,
                               "mRender": function (data, type, full) {
                                  return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?id='+ full[3] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a> ' +
                                          '<a class="btn btn-danger btn-xs" onclick="javascript: deleteRecord('+ full[3] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                               }
                           }
                      ],
                      "fnInitComplete": function(oSettings, json) {

                      }
              } );

            }
            
          bkLib.onDomLoaded(function() {
            new nicEditor({
                iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
                //fullPanel : true,
                buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'left', 'center', 'right', 'justify', 'removeformat','xhtml']
            }).panelInstance('genInfo');
        });
        
        
        
        
        
    // Manage Departments Page
    }else if($("body#admin-manage-document-delivery").length > 0){
        var oTable;

         /* Init the table */
          oTable =  $('#doc-delivery-listings').dataTable({
                  "bProcessing": false,
                  "sAjaxSource": list_url,
                  "aoColumns": [ 
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      {"bSearchable": true, "bVisible": true },
                      null,
                  ],
                   "aoColumnDefs": [
                       {
                          "aTargets": [1],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              var cost = parseFloat(full[1]);
                              return '$' + cost.toFixed(2);
                           }
                       },
                       {
                          "aTargets": [2],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return (full[2] == 1) ? 'Active' : 'Inactive';
                           }
                       },
                       {
                          "aTargets": [3],
                           "mData": null,
                           "mRender": function (data, type, full) {
                              return '<a class="btn btn-primary btn-xs" href="'+ open_url +'?id='+ full[3] +'"><span class="glyphicon glyphicon-edit"></span> Edit</a> ' +
                                      '<a class="btn btn-danger btn-xs" onclick="javascript: deleteRecord('+ full[3] +')"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                           }
                       }
                    ],
                  "fnInitComplete": function(oSettings, json) {
                        
                  }
          } );
          
          
          
          
    }else if($("body#tickets-report-visa").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[5] == 10){
                            return 'Pending';
                        }else if(full[5] == 11){
                            return 'Paid';
                        }else if(full[5] == 12){
                            return 'Completed';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [11],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[11] == 1){
                            return 'Paid';
                        }else if(full[11] == 0){
                            return 'Pending';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [13],
                     "mData": null,
                     "mRender": function (data, type, full) {
                 
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[4]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                            //
                            "sTitle": "Visa Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/tickets/visa.php"+ params);
                                return false;
                            }
                        },
                                
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#tickets-report-public-visa").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": false, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[5] == 10){
                            return 'Pending';
                        }else if(full[5] == 11){
                            return 'Paid';
                        }else if(full[5] == 12){
                            return 'Completed';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [11],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[11] == 1){
                            return 'Paid';
                        }else if(full[11] == 0){
                            return 'Pending';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [13],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[4]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                            //
                            "sTitle": "Public Visa Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/tickets/public-visa.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
        
        
        
        
    }else if($("body#tickets-report-tpn").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [9],
                    "mData":null,
                    "mRender": function (data, type, full) {
                        if(full[9] == 0){
                            return 'Pending';
                        }else if(full[9] == 1){
                            return 'Accepted';
                        }else if(full[9] == 2){
                            return 'Rejected';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [10],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[10] == 1){
                            return 'Paid';
                        }else if(full[10] == 0){
                            return 'Pending';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [11],
                     "mData": null,
                     "mRender": function (data, type, full) {
                 
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[4]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                            //
                            "sTitle": "TPN Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/tickets/tpn.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#tickets-report-passport").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [6],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[6] == 1){
                            return 'Paid';
                        }else if(full[6] == 0){
                            return 'Pending';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [8],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[3]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7],
                            //
                            "sTitle": "Passport Office Pickup or Delivery Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/tickets/passport.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#tickets-report-police-clearance").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [4],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[4];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[5];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                },
                {
                    "aTargets": [6],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[6];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                },
                {
                    "aTargets": [7],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[7] == 10){
                            return 'Pending';
                        }else if(full[7] == 11){
                            return 'Paid';
                        }else if(full[7] == 12){
                            return 'Completed';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [9],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[3]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8],
                            //
                            "sTitle": "Police Clearance Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/tickets/police-clearance.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
        
        
        
        
        
    }else if($("body#tickets-report-doc-delivery").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [12],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[12]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                            //
                            "sTitle": "Document Delivery Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/tickets/document-delivery.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
        
    }else if($("body#tickets-report-russian-visa-voucher").length > 0){
        
        $('#russian-visa-voucher-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[6] == 10){
                               return "Applied";
                          }else if(full[6] == 11){
                               return "Paid";
                           }else if(full[6] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [7],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[7]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5],
                            //
                            "sTitle": "Russian Visa Voucher:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/tickets/russian-visa-voucher.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#tickets-report-document-legalisation").length > 0){
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[5] == 1){
                               return "Completed";
                            }else if(full[5] == 0 || full[5] == ""){
                                return "Pending";
                            }else{
                               return "Pending";
                            }
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[6] == 10){
                               return "Applied";
                          }else if(full[6] == 11){
                               return "Paid";
                           }else if(full[6] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [7],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[7]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6],
                            //
                            "sTitle": "Document Legalisation:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/tickets/document-legalisation.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
        
        
        
        
    }else if($("body#orders-report-visa").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[5] == 10){
                            return 'Pending';
                        }else if(full[5] == 11){
                            return 'Paid';
                        }else if(full[5] == 12){
                            return 'Completed';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [8],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[8] == 1){
                            return 'Paid';
                        }else if(full[8] == 0){
                            return 'Pending';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [10],
                     "mData": null,
                     "mRender": function (data, type, full) {
                 
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[4]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            //
                            "sTitle": "Visa Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/orders/visa.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#orders-report-public-visa").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": false, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[5] == 10){
                            return 'Pending';
                        }else if(full[5] == 11){
                            return 'Paid';
                        }else if(full[5] == 12){
                            return 'Completed';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [8],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[8] == 1){
                            return 'Paid';
                        }else if(full[8] == 0){
                            return 'Pending';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [10],
                     "mData": null,
                     "mRender": function (data, type, full) {
                 
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[4]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 3, 4, 5, 6, 7, 8, 9]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 3, 4, 5, 6, 7, 8, 9]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 3, 4, 5, 6, 7, 8, 9],
                            //
                            "sTitle": "Public Visa Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/orders/public-visa.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 3, 4, 5, 6, 7, 8, 9]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#orders-report-tpn").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": false, "bVisible": false },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[5] == 10){
                            return 'Pending';
                        }else if(full[5] == 11){
                            return 'Paid';
                        }else if(full[5] == 12){
                            return 'Completed';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [8],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var $cost = parseFloat(full[8]);
                        return '$'+ $cost.toFixed(2);
                        
                    }
                },
                {
                    "aTargets": [10],
                     "mData": null,
                     "mRender": function (data, type, full) {
                 
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[4]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8],
                            //
                            "sTitle": "TPN Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/orders/tpn.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#orders-report-passport").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [7],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[7] == 1){
                            return 'Paid';
                        }else if(full[7] == 0){
                            return 'Pending';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [9],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[3]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8],
                            //
                            "sTitle": "Passport Office Pickup or Delivery Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/orders/passport.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#orders-report-police-clearance").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [4],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[4];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[5];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                },
                {
                    "aTargets": [6],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[6];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                },
                {
                    "aTargets": [7],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[7] == 10){
                            return 'Pending';
                        }else if(full[7] == 11){
                            return 'Paid';
                        }else if(full[7] == 12){
                            return 'Completed';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [9],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[3]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8],
                            //
                            "sTitle": "Police Clearance Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/orders/police-clearance.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
        
        
        
        
        
    }else if($("body#orders-report-doc-delivery").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [12],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[12]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                            //
                            "sTitle": "Document Delivery Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy')
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#orders-report-all-visa").length > 0){
        
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": false, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[5] == 10){
                            return 'Pending';
                        }else if(full[5] == 11){
                            return 'Paid';
                        }else if(full[5] == 12){
                            return 'Completed';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [11],
                    "mData": null,
                    "mRender": function (data, type, full){
                        if(full[11] == 1){
                            return 'Paid';
                        }else if(full[11] == 0){
                            return 'Pending';
                        }else{
                            return 'Undefined';
                        }
                    }
                },
                {
                    "aTargets": [13],
                     "mData": null,
                     "mRender": function (data, type, full) {
                 
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[4]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12],
                            //"sPdfOrientation": "landscape",
                            "sTitle": "Visa Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy')
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#orders-report-manifest").length > 0){
        if($("input[name=from]").val() == ""){
            $("input[name=from]").val($.format.date(new Date(), 'dd-MM-yyyy'));
        }
        
        if($("input[name=to]").val() == ""){
            $("input[name=to]").val($.format.date(new Date(), 'dd-MM-yyyy'));
        }
        
        if($('#visa-listings').length > 0){
            $('#visa-listings').dataTable({
                "bProcessing": false,

                "sAjaxSource": list_url1,
                "aaSorting": [[0, 'desc']],
                "aoColumns": [ 
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true }
                ],
                 "aoColumnDefs": [
                    {
                        "aTargets": [3],
                        "mData": null,
                        "mRender": function (data, type, full){
                            var output = '';
                            var string = full[3];
                            string = string.split(",");

                            for(i=0; i<string.length; i++){
                                output = output  + '' + string[i] + '<br/>';
                            }
                            return output;
                        }
                    },
                    {
                        "aTargets": [4],
                        "mData": null,
                        "mRender": function (data, type, full){
                            var output = '';
                            var string = full[4];
                            string = string.split(",");

                            for(i=0; i<string.length; i++){
                                output = output  + '' + string[i] + '<br/>';
                            }
                            return output;
                        }
                    }
                  ],
                  "sDom": 'T<"clear">lfrtip',
                  "oTableTools": {
                        "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                        "aButtons": [
                            {
                                "sExtends": "copy",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            },
                            {
                                "sExtends": "csv",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            },
                            {
                                "sExtends": "pdf",
                                "mColumns": [0, 1, 2, 3, 4, 5],
                                //"sPdfOrientation": "landscape",
                                "sTitle": "Manifest Report:", 
                                "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                                "fnClick":  function( nButton, oConfig, flash ) {
                                    window.open(api_url+"/pdfc/reports/manifest1.php"+ params);
                                    return false;
                                }
                            },
                            {
                                "sExtends": "print",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            }
                        ]
                  },
                  "fnInitComplete": function(oSettings, json) {
                    $("div.dataTable-buttons-1").html( $('#visa-listings_wrapper div.DTTT_container').html() );
                    
                 }
            });
        }
        
        
        
        if($('#passport-listings').length > 0){
            $('#passport-listings').dataTable({
                "bProcessing": false,

                "sAjaxSource": list_url2,
                "aoColumns": [ 
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true }
                ],
                 "aoColumnDefs": [
                    {
                        "aTargets": [4],
                        "mData": null,
                        "mRender": function (data, type, full){
                            var output = '';
                            var string = full[4];
                            string = string.split(",");

                            for(i=0; i<string.length; i++){
                                output = output  + '- ' + string[i] + '<br/>';
                            }
                            return output;
                        }
                    },
                    {
                        "aTargets": [5],
                        "mData": null,
                        "mRender": function (data, type, full){
                            var output = '';
                            var string = full[5];
                            string = string.split(",");

                            for(i=0; i<string.length; i++){

                                var docs = string[i].split('');
                                var docs_merge = '- ';
                                for(e=0; e<docs.length; e++){
                                    if(e==0 && docs[e] == '1'){
                                        docs_merge = docs_merge + 'Personal Passport,';
                                    }

                                    if(e==1 && docs[e] == '1'){
                                        docs_merge = docs_merge + 'Diplomatic/Official Passport,';
                                    }

                                    if(e==2 && docs[e] == '1'){
                                        docs_merge = docs_merge + 'Birth Certificate,';
                                    }

                                    if(e==3 && docs[e] == '1'){
                                        docs_merge = docs_merge + 'Marriage Certificate,';
                                    }

                                    if(e==4 && docs[e] == '1'){
                                        docs_merge = docs_merge + 'Certificate of Australian Citizenship';
                                    }
                                }

                                output = output  + docs_merge + '<br/>';
                            }
                            return output;
                        }
                    },
                  ],
                  "sDom": 'T<"clear">lfrtip',
                  "oTableTools": {
                        "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                        "aButtons": [
                            {
                                "sExtends": "copy",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            },
                            {
                                "sExtends": "csv",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            },
                            {
                                "sExtends": "pdf",
                                "mColumns": [0, 1, 2, 3, 4, 5],
                                //"sPdfOrientation": "landscape",
                                "sTitle": "Manifest Report:", 
                                "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                                "fnClick":  function( nButton, oConfig, flash ) {
                                    window.open(api_url+"/pdfc/reports/manifest2.php"+ params);
                                    return false;
                                }
                            },
                            {
                                "sExtends": "print",
                                "mColumns": [0, 1, 2, 3, 4, 5]
                            }
                        ]
                  },
                  "fnInitComplete": function(oSettings, json) {
                    $("div.dataTable-buttons-2").html( $('#passport-listings_wrapper div.DTTT_container').html() );
                 }
            });
        }
    }else if($("body#orders-report-police-clearance-manifest").length > 0){
        if($("input[name=from]").val() == ""){
            $("input[name=from]").val($.format.date(new Date(), 'dd-MM-yyyy'));
        }
        
        if($("input[name=to]").val() == ""){
            $("input[name=to]").val($.format.date(new Date(), 'dd-MM-yyyy'));
        }
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true }
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                        if(full[1] != ''){
                            var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                        }else{
                            return '';
                        }
                     }
                 },
                {
                    "aTargets": [3],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[3];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                },
                {
                    "aTargets": [4],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[4];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                },
                {
                    "aTargets": [5],
                    "mData": null,
                    "mRender": function (data, type, full){
                        var output = '';
                        var string = full[5];
                        string = string.split(",");
                        
                        for(i=0; i<string.length; i++){
                            output = output  + string[i] + '<br/>';
                        }
                        return output;
                    }
                }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [2, 3, 4, 6, 7]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [2, 3, 4, 6, 7]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [2, 3, 4, 6, 7],
                            //
                            "sTitle": "Police Clearance Manifest Report:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) {
                                window.open(api_url+"/pdfc/reports/police-clearance-manifest.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [2, 3, 4, 6, 7]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
        
        
        
    }else if($("body#orders-report-embassy-delivery").length > 0){
        
        
        $('#datatables-listings').dataTable({
            "bProcessing": false,
            
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true }
            ],
             "aoColumnDefs": [
                
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [0, 1, 2, 3, 4]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [0, 1, 2, 3, 4]
                        },
                        {
                            "sExtends": "pdf",
                            "sTitle": "Embassy Delivery:", 
                            "sPdfMessage": "Date: " + $.format.date(new Date(), 'ddd d MMMM yyyy'),
                            "fnClick":  function( nButton, oConfig, flash ) {
                                window.open(api_url+"/pdfc/reports/embassyDelivery.php"+ params_);
                                return false;
                            },
                            "mColumns": [0, 1, 2, 3, 4]
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [0, 1, 2, 3, 4]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
        
    }else if($("body#orders-report-russian-visa-voucher").length > 0){
        
        $('#russian-visa-voucher-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[6] == 10){
                               return "Applied";
                          }else if(full[6] == 11){
                               return "Paid";
                           }else if(full[6] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [7],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[7]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5],
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/orders/russian-visa-voucher.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#orders-report-document-legalisation").length > 0){;
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                {
                    "aTargets": [5],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[5] == 1){
                               return "Completed";
                            }else if(full[5] == 0 || full[5] == ""){
                                return "Pending";
                            }else{
                               return "Pending";
                            }
                     }
                 },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                            if(full[6] == 10){
                               return "Applied";
                          }else if(full[6] == 11){
                               return "Paid";
                           }else if(full[6] == 12){
                               return "Completed";
                           }else{
                               return "Undefined";
                           }
                     }
                 },
                {
                    "aTargets": [7],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+ open_order_url +"?order_no="+ full[7]+'"><span class="glyphicon glyphicon-th-list"></span> View</a> ';
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5, 6]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5, 6]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5, 6],
                            "fnClick":  function( nButton, oConfig, flash ) { 
                                window.open(api_url+"/pdfc/reports/orders/document-legalisation.php"+ params);
                                return false;
                            }
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
    }else if($("body#admin-manage-russian-visa-voucher").length > 0){
        $('div.invitation .type-group').sortable({placeholder: "ui-state-highlight",helper:'clone'});
        $('div.business .type-group').sortable({placeholder: "ui-state-highlight",helper:'clone'});
        
        
    }else if($("body#process-payment").length > 0){
        initializeEditables();
        
        
        $("input[name=order_no]").keyup(function(e){
            var $self = $(this);
            $("input[name=order]").val($self.val());
        });
        
        $("#btnGetDetails").click(function(){
           $("#frmGetOrderDetails").submit(); 
        });
        
        $(".payment-option").change();
        
        
        
        
    }else if($("body#admin-activities").length > 0){
        
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "iDisplayLength": 100,
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true }
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                {
                    "aTargets": [4],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         if(full[4] == 'dfat'){
                            return full[4].toUpperCase();
                         }else{
                            return toTitleCase(full[4]);
                         }
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
        
        
        
    }else{
        
        
        var bodyId = $("body").attr("id");
        
        if(bodyId.indexOf('admin-content-pages') > -1){
            bkLib.onDomLoaded(function() {
                new nicEditor({
                    iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
                    fullPanel : true,
                    //buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'left', 'center', 'right', 'justify', 'removeformat','xhtml']
                }).panelInstance('manageContent');
            });
        }else if(bodyId.indexOf('admin-manage-saudi-visa-popup-content') > -1){
            bkLib.onDomLoaded(function() {
                new nicEditor({
                    iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
                    fullPanel : true,
                    //buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'left', 'center', 'right', 'justify', 'removeformat','xhtml']
                }).panelInstance('popUpContent');
            });
        }
    }
    
    
    
    
    
    
//Create Update password modal
if (typeof new_pass_req !== 'undefined') {
    checkpassword($('input[name="password"]').val());
    if(new_pass_req==1){
        $('#modalNew').modal('show');
    }
}
    
    
    
});


$('input[name="password"]').keyup(function(){
    var $self = $(this);
    checkpassword($self.val());
});

$('#btnUpdatePassword').click(function(){
    var pass = $('input[name="password"]').val();
    var con_pass = $('input[name="passwordConfirm"]').val();
    if(pass !='' ){
        if(con_pass!=pass){
            alert('Password should be match!');
            return false;
        }
        $.ajax({
            url: update_pass_url,
            method: 'POST',
            data: {'password': pass,'passwordConfirm':con_pass},
            success: function(data) {
                if(data==0){
                    $('#msg_div').show();
                    $('#msg').html('Your password has been successfully updated.');
                    $('#btnDone').show();
                    $('#btnUpdatePassword').hide();
                }
            }, 
            error:function(){
            }
        });
    }else{
        alert('Password field is required!');
    }
});

function checkpassword(pass){
    $.ajax({
        url: check_pass_url,
        method: 'POST',
        data: {'pass': pass},
        success: function(data) {
            var res = data.split('#');
            if(res[0]==1){
                $('#let').addClass('glyphicon-ok-circle').addClass('green');
                $('#let').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#let').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#let').addClass('glyphicon-remove-circle').addClass('red');
            }
            
            if(res[1]==1){
                $('#num').addClass('glyphicon-ok-circle').addClass('green');
                $('#num').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#num').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#num').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[2]==1){
                $('#spe').addClass('glyphicon-ok-circle').addClass('green');
                $('#spe').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#spe').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#spe').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[3]==1){
                $('#len').addClass('glyphicon-ok-circle').addClass('green');
                $('#len').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#len').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#len').addClass('glyphicon-remove-circle').addClass('red');
            }
        },
        error:function(){
        }
    });
}


/*
 * Clients page
 * Position: Start
 */


// client type selection
$('body#admin-clients input[name=clientType]').change(function(){
    if($(this).val() == 'public' || $(this).val() == 'corporate' || $(this).val() == 'bulk-visa'){
        $("body#admin-clients").find("div#department").hide();
        $("body#admin-clients").find("div#company").show();
        
        $("body#admin-clients").find("div#department > select").removeAttr("required");
        $("body#admin-clients").find("div#company > input").attr("required","required");
    }else{
        $("body#admin-clients").find("div#department").show();
        $("body#admin-clients").find("div#company").hide();
        
        $("body#admin-clients").find("div#department > select").attr("required","required");
        $("body#admin-clients").find("div#company > input").removeAttr("required");
    }
});



// copy address details to main document delivery address if checked
$("body#admin-clients #copyMainDoc").click(function(){
    if(this.checked == true){
        $("#mddaAddress").val($("#address").val());
        $("#mddaCity").val($("#city").val());
        $("#mddaState").val($("#state").val());
        $("#mddaPostcode").val($("#postcode").val());
        $("#mddaCountry").val($("#country").val());
    }else{
        $("#mddaAddress").val("");
        $("#mddaCity").val("");
        $("#mddaState").val("");
        $("#mddaPostcode").val("");
        $("#mddaCountry").val("");
    }
});

// copy address details to main billing address if checked
$("body#admin-clients #copyMainBilling").click(function(){
    if(this.checked == true){
        $("#mbaAddress").val($("#address").val());
        $("#mbaCity").val($("#city").val());
        $("#mbaState").val($("#state").val());
        $("#mbaPostcode").val($("#postcode").val());
        $("#mbaCountry").val($("#country").val());
    }else{
        $("#mbaAddress").val("");
        $("#mbaCity").val("");
        $("#mbaState").val("");
        $("#mbaPostcode").val("");
        $("#mbaCountry").val("");
    }
});

/*
 * Clients page
 * Position: End
 */




/*
 * Manage - Visas Page
 * Position: Start
 */


// add additional requirement button
$("body#admin-manage-visas, body#admin-manage-public-visas").on('click','.btn-add-requirement',function(){
    if($("body#admin-manage-public-visas").length > 0){
        var visa_no = $(this).parents().eq(4).attr('id');
    }else{
        var visa_no = $(this).parents().eq(3).attr('id');
    }
    $(this).parents().eq(1).find(".content").append('\n\
    <div class="row requirement">\n\
        <div class="col-lg-4">\n\
            <div class="form-group">\n\
                <textarea name="additionalRequirement['+visa_no+'][]" class="form-control" placeholder="Ex. Single Entry Visa" required></textarea>\n\
            </div>\n\
        </div>\n\
\n\
        <div class="col-lg-2">\n\
            <div class="form-group">\n\
                <input type="text" name="additionalRequirementCost['+visa_no+'][]" class="form-control" placeholder="Cost $(Ex GST)" required>\n\
            </div>\n\
        </div>\n\
        <div class="col-lg-3" style="padding-top: 5px;">\n\
            <div class="form-group">\n\
                <label style="padding-right: 10px;"><input type="checkbox" class="additional-requirement-required"> Required <input type="hidden"  name="additionalRequirementIsRequired['+visa_no+'][]" value="0"></label>\n\
                <label><input type="checkbox" class="additional-requirement-active"> Active <input type="hidden"  name="additionalRequirementIsActive['+visa_no+'][]" value="0"></label>\n\
            </div>\n\
        </div>\n\
        <div class="col-lg-2">\n\
            <div class="form-group" style="padding-top: 1px;">\n\
                <button type="button" class="btn btn-danger btn-sm btn-remove-requirement"><span class="glyphicon glyphicon-remove-circle"></span> Remove</button>\n\
            </div>\n\
        </div>\n\
    </div>\n\
    ');
});

// remove additional requirement button
$("body#admin-manage-visas, body#admin-manage-public-visas").on('click', '.btn-remove-requirement',function(){
    var elem = $(this);
    bootbox.confirm("Are you sure?", function(result) {
      if(result == true){
          if(elem.parents().eq(3).find(".additionalRequirementRemove").length > 0){
            var remove_reqs = elem.parents().eq(3).find(".additionalRequirementRemove").val();
            var remove_req_id = elem.parents().eq(2).find("input.requirement-id").val();
            var separator = (remove_reqs.trim() != '') ? ',' : '';
            if(typeof remove_req_id != 'undefined'){
                elem.parents().eq(3).find(".additionalRequirementRemove").val(remove_reqs + separator + remove_req_id);
            }
          }
          elem.parents().eq(2).fadeOut(500, function() { elem.parents().eq(2).remove(); });
      }
    }); 
});

// add new visa type
$("body#admin-manage-visas #addNewVisaType").click(function(){
   var visa_no = $("body#admin-manage-visas .visa-types .type-group").length;
   $("body#admin-manage-visas .visa-types").append('\n\
        <div class="type-group panel panel-default" id="'+ visa_no +'">\n\
            <div class="panel-body">\n\
                <div class="col-lg-4">\n\
                    <h4>Type of Visa</h4>\n\
                    <div class="form-group">\n\
                        <input type="text" name="visaType['+ visa_no +']" class="form-control required" placeholder="Ex. Diplomatic/Official" required>\n\
                    </div>\n\
\n\
                    <div class="form-group">\n\
                        <label>Cost $(Ex GST)</label>\n\
                        <input type="text" name="visaCost['+ visa_no +']" class="form-control required" placeholder="0.00" required>\n\
                    </div>\n\
\n\
                    <div class="form-group">\n\
                        <label>File attachment (sent to client on purchase)</label>\n\
                        <input type="file" name="visaFile['+ visa_no +']" class="form-control">\n\
                    </div>\n\
\n\
                    <div class="form-group">\n\
                        <label><input type="checkbox" class="is-active"> is Active</label>\n\
                        <input type="hidden" name="visaStatus['+ visa_no +']" value="0">\n\
                    </div>\n\
                </div>\n\
                <div class="col-lg-8 additional-requirements">\n\
                    <h4>Additional Requirements</h4>\n\
                    <div class="content">\n\
                    </div>\n\
                    <div>\n\
                        <a class="btn-add-requirement"><span class="glyphicon glyphicon-plus-sign"></span> Add additional requirement</a>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
            <div class="panel-footer">\n\
                <button type="button" class="btn btn-danger btn-sm btn-delete-visa-type"><span class="glyphicon glyphicon-remove-circle"></span> Delete Visa Type</button>\n\
                <button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span> Update Visa Type</button>\n\
            </div>\n\
        </div>\n\
  ');
    
    // smooth scroll
    $('html, body').animate({
        scrollTop: $(".type-group:last-child").offset().top
    }, "slow");
    
    $('div.additional-requirements .content').sortable({placeholder: "ui-state-highlight",helper:'clone'});
});

// add new visa type
$("body#admin-manage-public-visas #addNewVisaType").click(function(){
   var visa_no = $("body#admin-manage-public-visas .visa-types .type-group").length;
   $("body#admin-manage-public-visas .visa-types").append('\n\
        <div class="type-group panel panel-default" id="'+ visa_no +'">\n\
            <div class="panel-body">\n\
                <div class="row">\n\
                    <div class="col-lg-4">\n\
                        <h4>Type of Visa</h4>\n\
                        <div class="form-group">\n\
                            <input type="text" name="visaType['+ visa_no +']" class="form-control required" placeholder="Ex. Diplomatic/Official" required>\n\
                        </div>\n\
    \n\
                        <div class="form-group">\n\
                            <label>Cost $(Ex GST)</label>\n\
                            <input type="text" name="visaCost['+ visa_no +']" class="form-control required" placeholder="0.00" required>\n\
                        </div>\n\
    \n\
                        <div class="form-group">\n\
                            <label>File attachment (sent to client on purchase)</label>\n\
                            <input type="file" name="visaFile['+ visa_no +']" class="form-control">\n\
                        </div>\n\
    \n\
                        <div class="form-group">\n\
                            <label><input type="checkbox" class="is-active"> is Active</label>\n\
                            <input type="hidden" name="visaStatus['+ visa_no +']" value="0">\n\
                        </div>\n\
                    </div>\n\
                    <div class="col-lg-8 additional-requirements">\n\
                        <h4>Additional Requirements</h4>\n\
                        <div class="content">\n\
                        </div>\n\
                        <div>\n\
                            <a class="btn-add-requirement"><span class="glyphicon glyphicon-plus-sign"></span> Add additional requirement</a>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
                <div class="row">\n\
                    <div class="col-lg-12">\n\
                        <h4>Visa Information</h4>\n\
                        <textarea id="visaInformation['+ visa_no +']" name="visaInformation['+ visa_no +']"></textarea>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
            <div class="panel-footer">\n\
                <button type="button" class="btn btn-danger btn-sm btn-delete-visa-type"><span class="glyphicon glyphicon-remove-circle"></span> Delete Visa Type</button>\n\
                <button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span> Update Visa Type</button>\n\
            </div>\n\
        </div>\n\
  ');
    
    
    // smooth scroll
    $('html, body').animate({
        scrollTop: $(".type-group:last-child").offset().top
    }, "slow");
    
    $('div.additional-requirements .content').sortable({placeholder: "ui-state-highlight",helper:'clone'});
        
       wsiwyg =  new nicEditor({
            iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
            fullPanel : true,
            //buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'left', 'center', 'right', 'justify', 'removeformat','xhtml']
        }).panelInstance('visaInformation['+ visa_no +']');
});


// remove visa type
$("body#admin-manage-visas, body#admin-manage-public-visas").on('click','.btn-delete-visa-type', function(){
   var elem = $(this);
   bootbox.confirm("Are you sure?", function(result) {
      if(result == true){
        var remove_types = $("#visaTypeRemove").val();
        var remove_visa_type_id = elem.parents().eq(1).find('input.visa-type-id').val();
        var separator = (remove_types.trim() != '') ? ',' : '';
        if(typeof remove_visa_type_id !== 'undefined'){
            $("#visaTypeRemove").val(remove_types + separator + remove_visa_type_id);
        }
        elem.parents().eq(1).fadeOut(500, function() { elem.parents().eq(1).remove(); });
      }
   }); 
});



$("body#admin-manage-visas, body#admin-manage-public-visas").on('change','.is-active',function(){
  var isChecked = this.checked ? 1 : 0;
  $(this).parent().next('input').val(isChecked);
});


$("body#admin-manage-visas, body#admin-manage-public-visas").on('change','.additional-requirement-required',function(){
  var isChecked = this.checked ? 1 : 0;
  $(this).next('input').val(isChecked);
});

$("body#admin-manage-visas, body#admin-manage-public-visas").on('change','.additional-requirement-active',function(){
  var isChecked = this.checked ? 1 : 0;
  $(this).next('input').val(isChecked);
});

/*
 * Manage - Visas Page
 * Position: End
 */



/*
 * Passport Office Courier Service Booking
 * Position: Start
 */

$("body#passport-office-pickup-delivery .get-date").change(function(){
    var elem = $(this);
    var today = new Date();
    var dd = today.getDate();
    dd = ("0" + dd).slice(-2);
    var mm = today.getMonth()+1; //January is 0!
    mm = ("0" + mm).slice(-2);
    var yyyy = today.getFullYear();
    
    if(this.checked == true){
        elem.parent().next("input").val(yyyy + '-' + mm + '-' + dd);
    }else{
        elem.parent().next("input").val('');
    }
});

/*
 * Passport Office Courier Service Booking
 * Position: End
 */




/**
 * Manifest Report
 * Position: Start
 */

$("body#orders-report-manifest, body#orders-report-embassy-delivery").on('click','.chkDateToday',function(){
   var $self = this;
   if($self.checked == true){
        var today = new Date();
        var dd = today.getDate();
        dd = ("0" + dd).slice(-2);
        var mm = today.getMonth()+1; //January is 0!
        mm = ("0" + mm).slice(-2);
        var yyyy = today.getFullYear();
        
        $("input[name=from]").val(dd + '-' + mm + '-' + yyyy);
        $("input[name=to]").val(dd + '-' + mm + '-' + yyyy);
   }else{
        $("input[name=from]").val("");
        $("input[name=to]").val("");
        $("input[name=from]").focus();
   }
});

/**
 * Manifest Report
 * Position: End
 */

$("#addCountry").click(function(){
    $("input[name=hidSubmit]").val(1);
    $("#myModalLabel").html("New Country");
    $("input[name=countryCode]").val('');
    $("input[name=countryName]").val('');
    $("input[name=embassy]").val('');
    $("#deleteCountry").hide();
    $("#myModal").modal("show");
    
});

$("a.country").click(function(){
    var $self = $(this);
    var $data = $self.attr("tag");
    $("input[name=hidSubmit]").val(0);
    $("#myModalLabel").html("Edit Country: " + $self.html());
    
    $data = $data.split(':');
    
    $("input[name=id]").val($data[0]);
    $("input[name=countryCode]").val($data[1]);
    $("input[name=countryName]").val($self.html());
    $("input[name=embassy]").val($data[2]);
    $("input[name=countryNameDisplay]").val($data[3]);
    $("#deleteCountry").show();
    $("#myModal").modal("show");
});

$("a.embassy").click(function(){
    var $self = $(this);
    var $data = $self.attr("tag");
    $("input[name=hidSubmit]").val(0);
    $("#myModalLabel").html("Contact Details: " + $self.html());
    
    $data = $data.split(':');
    
    $("input[name=id]").val($data[0]);
    $("input[name=embassy_address_line1]").val($data[1]);
    $("input[name=embassy_address_line2]").val($data[2]);
    $("input[name=embassy_street]").val($data[3]);
    $("input[name=embassy_city]").val($data[4]);
    $("input[name=embassy_state]").val($data[5]);
    $("input[name=embassy_postcode]").val($data[6]);
    $("input[name=embassy_phone]").val($data[7]);
    $("#deleteCountry").show();
    $("#myModal").modal("show");
});

$("#deleteCountry").click(function(){
    $("#countrySave").val(0);
   var r = confirm("Are you sure you want to delete this country?\n\
Please note that deleting countries might cause of losing destinations of some applications and/or countries of some clients/applicants! \n\
So be sure that this country isn't being used yet before deciding to continue this deletion.");
    if (r == true) {
        $("#countryForm").submit();
    }
});


$("#saveCountry").click(function(){
    $("#countrySave").val(1);
});




$(".invitation .btn-add-new-type").click(function(){
    var number = $(".invitation .type-group > .form-group").length;
    
   $('.invitation .type-group').append('\n\
        <div class="form-group">\n\
            <div class="row">\n\
                <div class="col-lg-3">\n\
                    <label>Description</label>\n\
                    <input type="text" name="inv_description[new-'+ number +']" class="form-control" placeholder="Ex. Tourist single entry" required/>\n\
                </div>\n\
\n\
                <div class="col-lg-2">\n\
                    <label>3 days processing</label>\n\
                    <input type="number" name="inv_three_days_fee[new-'+ number +']" class="form-control" placeholder="Cost $(Ex GST)"/>\n\
                </div>\n\
\n\
                <div class="col-lg-2">\n\
                    <label>1-2 days processing</label>\n\
                    <input type="number" name="inv_one_two_days_fee[new-'+ number +']" class="form-control" placeholder="Cost $(Ex GST)"/>\n\
                </div>\n\
\n\
                <div class="col-lg-2">\n\
                    <label>12 hours processing</label>\n\
                    <input type="number" name="inv_twelve_hrs_fee[new-'+ number +']" class="form-control" placeholder="Cost $(Ex GST)"/>\n\
                </div>\n\
\n\
                <div class="col-lg-1" style="padding-top: 25px;">\n\
                    <label><input type="checkbox" name="inv_s_active[new-'+ number +']"> Active</label>\n\
                </div>\n\
\n\
                <div class="col-lg-2" style="padding-top: 25px;">\n\
                    <button type="button" class="btn btn-danger btn-sm btn-remove-type"><span class="glyphicon glyphicon-remove-circle"></span> Remove</button>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    ');
    
    $('div.invitation .type-group').sortable({placeholder: "ui-state-highlight",helper:'clone'});
});


$("body .invitation").on('click','.btn-remove-type', function(){
   var $self = $(this);
   bootbox.confirm("Are you sure? Please note that changes won't be made until you hit 'Save Changes' button.", function(result) {
      if(result == true){
        var remove_types = $(".invitation").find("input.remove-types").val();
        var remove_type_id = $self.attr("tag");
        var separator = (remove_types.trim() != '') ? ',' : '';
        if(typeof remove_type_id !== 'undefined'){
            $(".invitation").find("input.remove-types").val(remove_types + separator + remove_type_id);
        }
        $self.parents().eq(2).fadeOut(500, function() { $self.parents().eq(2).remove(); });
      }
   }); 
   
});



$(".business .btn-add-new-type").click(function(){
    var number = $(".business .type-group > .form-group").length;
    
    $('.business .type-group').append('\n\
        <div class="form-group">\n\
            <div class="row">\n\
                <div class="col-lg-3">\n\
                    <label>Description</label>\n\
                    <input type="text" name="bus_description[new-'+ number +']" class="form-control" placeholder="Ex. Business 1 month single entry" required/>\n\
                </div>\n\
\n\
                <div class="col-lg-2">\n\
                    <label>13 days </label>\n\
                    <input type="number" name="bus_thirteen_days_fee[new-'+ number +']" class="form-control" placeholder="Cost $(Ex GST)"/>\n\
                </div>\n\
\n\
                <div class="col-lg-2">\n\
                    <label>4 days</label>\n\
                    <input type="number" name="bus_four_days_fee[new-'+ number +']" class="form-control" placeholder="Cost $(Ex GST)"/>\n\
                </div>\n\
\n\
                <div class="col-lg-1" style="padding-top: 25px;">\n\
                    <label><input type="checkbox" name="bus_s_active[new-'+ number +']"> Active</label>\n\
                </div>\n\
\n\
                <div class="col-lg-2" style="padding-top: 25px;">\n\
                    <button type="button" class="btn btn-danger btn-sm btn-remove-type"><span class="glyphicon glyphicon-remove-circle"></span> Remove</button>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    ');
    
    $('div.business .type-group').sortable({placeholder: "ui-state-highlight",helper:'clone'});
});


$("body .business").on('click','.btn-remove-type', function(){ 
   var $self = $(this);
   bootbox.confirm("Are you sure? Please note that changes won't be made until you hit 'Save Changes' button.", function(result) {
      if(result == true){
        var remove_types = $(".business").find("input.remove-types").val();
        var remove_type_id = $self.attr("tag");
        var separator = (remove_types.trim() != '') ? ',' : '';
        if(typeof remove_type_id !== 'undefined'){
            $(".business").find("input.remove-types").val(remove_types + separator + remove_type_id);
        }
        $self.parents().eq(2).fadeOut(500, function() { $self.parents().eq(2).remove(); });
      }
   }); 
   
});



$("#sNoVisaRequired").change(function(){
    var $self = this;
    if($self.checked == true){
        $("div.visa-types-container").hide();
        $("div.no-visa-required-container").show();
        $(".required").removeAttr("required");
        
    }else{
        $("div.visa-types-container").show();
        $("div.no-visa-required-container").hide();
        $(".required").attr("required","required");
    }
});


$(document).ready(function(){
    if($("body#admin-manage-public-visas, body#admin-manage-visas").length > 0){
        $("#sNoVisaRequired").change();
        bkLib.onDomLoaded(function() {
            new nicEditor({
                iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
                fullPanel : true,
                //buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'left', 'center', 'right', 'justify', 'removeformat','xhtml']
            }).panelInstance('noVisaRequiredInformation');
        }); 
    }
});



$("body#admin-view-order #addNewDocType").click(function(){
    $(".dls-group").append('\n\
        <div class="form-group group">\n\
            <div class="row">\n\
                <div class="col-lg-4">\n\
                    <label>Type of document enclosed</label>\n\
                    <input type="text" class="form-control required" name="dl_type[]" placeholder="Type" required>\n\
                </div>\n\
\n\
                <div class="col-lg-2">\n\
                    <label>Number</label>\n\
                    <input type="number" class="form-control required" name="dl_number[]" placeholder="0" required>\n\
                </div>\n\
\n\
                <div class="col-lg-4">\n\
                    <label>Note</label>\n\
                    <input type="text" class="form-control" name="dl_note[]" placeholder="note">\n\
                </div>\n\
\n\
                <div class="col-lg-2" style="padding-top: 27px;">\n\
                    <button type="button" class="btn btn-danger btn-sm remove-doc-type"><span class="glyphicon glyphicon-remove"></span> remove</button>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    ');
});

$("body#admin-view-order").on('click','.remove-doc-type',function(){
   var $self = $(this);
   bootbox.confirm("Are you sure?", function(result) {
        if(result == true){
            $self.parents().eq(2).remove();
        }
   });
});

$("body#admin-view-order .btn-add-line").click(function(){
   var $item = '<tr class="item">\n\
        <td class="remove-item-col">\n\
            <a class="btn-remove-item"><span class="glyphicon glyphicon-remove-circle"></span> </a>\n\
        </td>\n\
        <td style="width: 220px;">\n\
            <textarea class="form-control description" style="max-width: 220px;" name="qdescription[]" placeholder="Type description here..." required></textarea>\n\
        </td>\n\
        <td>\n\
            <input type="number" class="form-control quantity" name="quantity[]" placeholder="#" required>\n\
        </td>\n\
        <td>\n\
            <input type="text" class="form-control price" name="qprice[]" placeholder="0.00" required>\n\
        </td>\n\
        <td>\n\
            <label><input type="checkbox" class="gst"> 10% <input type="hidden" name="gst[]" class="val"></label>\n\
        </td>\n\
        <td>\n\
            <span class="total"></span>\n\
        </td>\n\
    </tr>';
    
    $("#items").find("tbody").append($item);
});


$("body#admin-view-order").on("click",".btn-remove-item", function(){
    var $self = $(this);
    $self.parents().eq(1).remove();
    getDlQuotesTotal()
});

$("body#admin-view-order").on("click", ".btn-create-quote",function(){
   $("#modCreateQuote").modal("show");
});

$("body#admin-view-order").on("keyup", "#items .quantity",function(){
    getDlQuotesTotal();
});

$("body#admin-view-order").on("keyup", "#items .price",function(){
    getDlQuotesTotal();
});

$("body#admin-view-order").on("change", "#items .gst",function(){
    getDlQuotesTotal();
    if($(this).is(":checked")){
        $(this).parent().find("input.val").val(1);
    }else{
        $(this).parent().find("input.val").val(0);
    }
});

$("body#admin-view-order").on("click", ".delete-dl-quote", function(){
    var $self = $(this);
    bootbox.confirm("Are you sure?", function(result) {
      if(result == true){
        $.ajax({
            url: delete_dl_quote_url,
            type: 'POST',
            data: {'order_no': $self.attr("data-order-no"), 
                'group': $self.attr("data-group")
            },
            success: function(data) {
                location.reload();
            },
            error:function(){
            }
        });
      }
  }); 
});

 $("body#process-payment").on("click",".new-line",function(){
     
    var $html = '<tr>\n\
                <td><span class="glyphicon glyphicon-trash remove-line"></span></td>\n\
                <td><span class="editable-item editable-empty required" data-type="text"  data-showbuttons="true" data-mode="popup"  data-original-title="Description" required></span><input class="value" type="hidden" name="description[]"></td>\n\
                <td><span class="editable-item editable-empty required price" data-type="number" data-step="any"  data-showbuttons="true" data-mode="popup"  data-original-title="Unit Price" required></span><input class="value tprice" type="hidden" name="price[]"></td>\n\
                <td><span class="editable-item editable-empty required quantity" data-type="number" data-step="any"  data-showbuttons="true" data-mode="popup"  data-original-title="Quantity" required></span><input class="value tquantity" type="hidden" name="quantity[]"></td>\n\
                <td><span class="editable-item editable-empty required gst" data-type="number" data-step="any"  data-showbuttons="true" data-mode="popup"  data-original-title="GST" required></span><input class="value tgst" type="hidden" name="gst[]"></td>\n\
                <td class="text-right">$<span class="total">0</span></td>\n\
            </tr>';
     
     
    $("#paymentItems").find("tbody").append($html);
    
    initializeEditables();
    
 });
 
 
 $("body#process-payment").on("click",".remove-line",function(){
    $(this).closest("tr").remove(); 
    computeTotal(this);
 });
 
 $("body#process-payment").on("click","#chkCopyPaymentDetails",function(){
    if($(this).is(":checked")){
        $("#txtBillingAddress").val($("#txtAddress").val());
        $("#txtBillingCity").val($("#txtCity").val());
        $("#txtBillingState").val($("#txtState").val());
        $("#txtBillingPostCode").val($("#txtPostCode").val());
        $("#sltBillingCountry").val($("#sltCountry").val());
    }
 });

$("body#process-payment #chkUseDefaultAddress").click(function(){
    if(this.checked === true){ 
         $.ajax({
            url: udetails_url,
            type: 'POST',
            data: {'client': client},
            success: function(data) {
                data = JSON.parse(data);
                $("#txtFname").val(data['fname']);
                $("#txtLname").val(data['lname']);
                $("#txtEmail").val(data['email']);
                $("#txtPhone").val(data['phone']);
                $("#txtMobile").val(data['mobile']);
                $("#txtAddress").val(data['address']);
                $("#txtCity").val(data['city']);
                $("#txtState").val(data['state']);
                $("#txtPostCode").val(data['postcode']);
                $("#sltCountry").val(data['country_id']);

            },
            error:function(){
            }
        });
    }else{
    }
 });

$(".payment-option").change(function(){
    var elem = $(this);
    paymentOption(elem);
});


function paymentOption(elem){ 
    var $orig_grand_total =  $("table#paymentItems tfoot").find(".grand-total").html();
    $orig_grand_total = parseFloat($orig_grand_total);
    //if(elem.val() == 0){
    if($('input[name=paymentType]:radio:checked').val() == 0){  
        $("#incCardFee").hide();
        $(".pay-required-one").attr("required", "required");
        $(".pay-required").removeAttr("required");
        $(".payment-option-fields.account").show();
        $(".payment-option-fields.credit-card").hide();
        
        var $grand_total = $("#totalOrderPrice").html();
        $grand_total = parseFloat($grand_total);
        $("#totalOrderPrice").html($orig_grand_total.toFixed(2));
        $("input[name=grand_total]").val($orig_grand_total.toFixed(2));
    }else if($('input[name=paymentType]:radio:checked').val() == 1){
        $("#incCardFee").show();
        $(".pay-required").attr("required", "required");
        $(".pay-required-one").removeAttr("required");
        $(".payment-option-fields.account").hide();
        $(".payment-option-fields.credit-card").show();
        
        var $grand_total = $("#totalOrderPrice").html();
        $grand_total = parseFloat($grand_total);
        var $ccp = $grand_total * (ccp_fee/100);
        $grand_total = $grand_total + $ccp;
        $("#totalOrderPrice").html($grand_total.toFixed(2));
        $("input[name=grand_total]").val($grand_total.toFixed(2));
    }else{
        $("#incCardFee").hide();
        $(".pay-required").removeAttr("required");
        $(".pay-required-one").removeAttr("required");
        $(".payment-option-fields.account").hide();
        $(".payment-option-fields.credit-card").hide();
        
        var $grand_total = $("#totalOrderPrice").html();
        $grand_total = parseFloat($grand_total);
        $("#totalOrderPrice").html($orig_grand_total.toFixed(2));
        $("input[name=grand_total]").val($orig_grand_total.toFixed(2));
    }
    
    
    
}

function deleteAdminComment($note_id, self){
    var elem = $(self);
    bootbox.confirm("Are you sure you want to delete this comment?", function(result) {
          if(result == true){
              elem.closest("li").fadeOut(500, function() { 
                  $.ajax({
                        url: delete_comment_url,
                        type: 'POST',
                        data: {'id': $note_id},
                        success: function(data) {
                            elem.closest("li").remove(); 
                        },
                        error:function(){
                        }
                    });
                  
              });
          }
    }); 
}

function initializeEditables(){
     var $vt_count = 0;
     var $tf_count = 0;
     $('.editable-item').editable({
         type : 'number',
         validate: function(value) {
            if($.trim(value) == '') {
                return 'This field is required';
            }
        }
     });
     
        
    // this is to automatically make the next item in the table editable
    $('.editable-item').on('save', function(e, params){
        var that = this;
        // persist the old value in the element to be restored when clicking reset
        var oldItemValue = $(that)[0].innerHTML;
        var item = $(that).closest('td').next().find('.editable-item');
        var itemValue = "";
        if($(that).closest('td').find(".editable-input").find("input").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("input").val();
        }else if($(that).closest('td').find(".editable-input").find("select").length > 0){
            itemValue = $(that).closest('td').find(".editable-input").find("select").val();
        }
        if (!$(that).attr('oldValue')) {
            console.log('persisting original value: ' + oldItemValue);
            $(that).attr('oldValue', oldItemValue);
        }
        setTimeout(function() {
            // first search the row

            if(itemValue == ""){
                itemValue = $(that).html();
            }
            itemValue = (itemValue != "Empty") ? itemValue : "";
            $(that).closest('td').find('.value').val(itemValue);
            
            if(itemValue != ""){
                $(that).closest('td').removeClass("field-error");
            }
            console.log(item);
            computeTotal(that);
            if (item.length == 0) {
                // check the next row
                item = $(that).closest('tr').next().find('.editable-item');
            }
            item.editable('show');
        }, 200);
    });
    
    
    
 }
 
 
 function computeTotal(obj){ 
     var $line = $(obj).closest("tr");
     var $price = parseFloat($line.find(".tprice").val());
     var $quantity = parseFloat($line.find(".tquantity").val());
     var $gst_percent = parseFloat($line.find(".tgst").val());
     var $line_total = 0;
     var $sub_total = 0;
     var $gst_total = 0;
     var $credit_card_total = 0;
     var $grand_total = 0;
     var $discount = 0;
     
     $line_total = ($price * $quantity) + (($price * $quantity) * ($gst_percent/100));
     $line.find("span.total").html($line_total.toFixed(2));
     
     $("table#paymentItems tbody tr").each(function(){
         //$sub_total += parseFloat($(this).find(".tprice").val());
         $sub_total += (parseFloat($(this).find(".tprice").val()) * parseFloat($(this).find(".tquantity").val()));
         $gst_total += ((parseFloat($(this).find(".tprice").val()) * parseFloat($(this).find(".tquantity").val())) * (parseFloat($(this).find(".tgst").val())/100));
         
     });
     
     if(!isNaN($sub_total)){
        var $sub_total_final = $sub_total;
        var $gst_total_final = $gst_total;
        if(discount_rate != 0){
            $sub_total_final = $sub_total - ($sub_total * (discount_rate/100));
            
            $gst_total_final = $gst_total - ($gst_total * (discount_rate/100));
            
            $discount = ($sub_total + $gst_total) * (discount_rate/100);
        }
        $("table#paymentItems tfoot").find(".discount").html($discount.toFixed(2));
        $("table#paymentItems tfoot").find(".sub-total").html($sub_total_final.toFixed(2));
        $("table#paymentItems tfoot").find(".gst").html($gst_total_final.toFixed(2));
        
        $credit_card_total = (($sub_total_final + $gst_total_final) * (ccp_fee/100));
        
        $("table#paymentItems tfoot").find(".credit-card").html($credit_card_total.toFixed(2));
        
        $grand_total = $sub_total_final + $gst_total_final;
        if($('input[name=paymentType]:radio:checked').val() == 1){
            $grand_total_to_pay = $grand_total + $credit_card_total;
        }else{
            $grand_total_to_pay = $grand_total;
        }
        
        $("#incCardFee").find("span").html($credit_card_total.toFixed(2));
        $("table#paymentItems tfoot").find(".grand-total").html($grand_total.toFixed(2));
        $("#totalOrderPrice").html($grand_total_to_pay.toFixed(2));
        $("input[name=grand_total]").val($grand_total_to_pay.toFixed(2));
     }
     
 }
/*======================================================
 * functions
 ======================================================*/
function checkFloat(val){
    return (isNaN(val)) ? false : true;
}
function getDlQuotesTotal(){
   var $grand_total = 0;
   $("#items .price").each(function(){
       var $self = $(this);
       var $quantity = $self.closest("tr").find(".quantity").val();
       var $price = $self.val();
       var $total = $price * $quantity;
       var $gst = ($self.closest("tr").find(".gst").is(":checked")) ? $total * 0.10 : 0;
       $total = ($price * $quantity) + $gst;
       $grand_total += parseFloat($total);
       $total = $total.toFixed(2);
       if(!isNaN($total)){
            $self.closest("tr").find("span.total").html("$"+ $total);
       }else{
           $self.closest("tr").find("span.total").html("$0.00");
       }
//       if($.trim($self.val()) != ''){
//        if(checkFloat($self.val())){
//             $price = parseFloat($self.val());
//        }else{
//            alert("Numbers only please!");
//        }
//       }
   });
   $grand_total = $grand_total.toFixed(2);
   
   if(!isNaN($grand_total)){
        $("#items .grand-total span").html($grand_total);
   }
   
}

// refresh datatables
function RefreshTable(tableId, urlData)
{
  $.getJSON(urlData, null, function( json )
  {
    table = $(tableId).dataTable();
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<json.aaData.length; i++)
    {
      table.oApi._fnAddData(oSettings, json.aaData[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
  });
  //clearconsole();
}



function deleteClient(client_id){
  bootbox.confirm("Are you sure? All client records will be deleted and cannot be undone!", function(result) {
      if(result == true){
        $.ajax({
            url: delete_url,
            type: 'POST',
            data: {'client_id': client_id},
            success: function(data) {
                location.reload();
            },
            error:function(){
            }
        });
      }
  }); 
}

function deleteStaff(id){
    bootbox.confirm("Are you sure?!", function(result) {
        if(result == true){
          $.ajax({
              url: delete_url,
              type: 'POST',
              data: {'id': id},
              success: function(data) {
                  location.reload();
              },
              error:function(){
              }
          });
        }
    }); 
}


function deleteEmbassy(client_id){
  bootbox.confirm("Are you sure?!", function(result) {
      if(result == true){
        $.ajax({
            url: delete_url,
            type: 'POST',
            data: {'client_id': client_id},
            success: function(data) {
                location.reload();
            },
            error:function(){
            }
        });
      }
  }); 
}


function deleteRecord(id){
    bootbox.confirm("Are you sure?", function(result) {
      if(result == true){
        $.ajax({
            url: delete_url,
            type: 'POST',
            data: {'id': id},
            success: function(data) {
                location.reload();
            },
            error:function(){
            }
        });
      }
  }); 
}


function editAdminComment(id, self){
    $("#txtEditCommentId").val(id);
    $("#txtEditComment").val($(self).attr("tag"));
    $('#modEditComment').modal('show');
}

function saveCommentChanges(){
    $.ajax({
        url: edit_comment_url,
        type: 'POST',
        data: {'id': $("#txtEditCommentId").val(), 
            'comment': $("#txtEditComment").val()
        },
        success: function(data) {
            location.reload();
        },
        error:function(){
        }
    });
}

// added by leokarl 2-23-2015
$("#sendManualInvoice").click(function(){
    var $self = $(this);
    $self.html("Sending ...");
    $.ajax({
         url: send_invoice_url,
         type: 'POST',
         data: {
             'type': $self.attr("data-type"),
             'client_id': $self.attr("data-client"),
             'orderNumber': $self.attr("data-order")
         },
         success: function(data) {
             $self.html("Send Invoice");
             location.reload();
         },
         error:function(){
         }
     });
});

// added by leokarl 2-26-2015
$("body").on("click",".dataTable-buttons-1 > .DTTT_button.DTTT_button_print",function(){
    $("body").find("#visa-listings_wrapper").find("div.DTTT_container").find(".DTTT_button.DTTT_button_print").click();
});

$("body").on("click",".dataTable-buttons-2 > .DTTT_button.DTTT_button_print",function(){
    $("body").find("#passport-listings_wrapper").find("div.DTTT_container").find(".DTTT_button.DTTT_button_print").click();
});

$("body").on("click",".dataTable-buttons > .DTTT_button.DTTT_button_print",function(){
    $("body").find("#datatable-listings_wrapper").find("div.DTTT_container").find(".DTTT_button.DTTT_button_print").click();
});
$("body").on("click",".dataTable-buttons > .DTTT_button.DTTT_button_print",function(){
    $("body").find("#datatables-listings_wrapper").find("div.DTTT_container").find(".DTTT_button.DTTT_button_print").click();
});

$("body").on("click",".dataTable-buttons > .DTTT_button.DTTT_button_print",function(){
    $("body").find("#russian-visa-voucher-listings_wrapper").find("div.DTTT_container").find(".DTTT_button.DTTT_button_print").click();
});

//added by allan 2-16-1015
$('#reprintInvoice').click(function(){
    $('#post_type').attr('name','reprint');
    $('#frmPost').attr('target','_blank');
});
$('#update').click(function(){
    $('#post_type').attr('name','updateStatus');
    
});