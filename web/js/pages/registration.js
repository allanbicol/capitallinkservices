/*==========================================
 * Registration scripts
 *==========================================*/
// copy address details to main document delivery address if checked
$("#copyMainDoc").click(function(){
    if(this.checked == true){
        $("#mddaAddress").val($("#address").val());
        $("#mddaCity").val($("#city").val());
        $("#mddaState").val($("#state").val());
        $("#mddaPostcode").val($("#postcode").val());
        $("#mddaCountry").val($("#country").val());
    }else{
//        $("#mddaAddress").val("");
//        $("#mddaCity").val("");
//        $("#mddaState").val("");
//        $("#mddaPostcode").val("");
//        $("#mddaCountry").val("");
    }
});

// copy address details to main billing address if checked
$("#copyMainBilling").click(function(){
    if(this.checked == true){
        $("#mbaAddress").val($("#address").val());
        $("#mbaCity").val($("#city").val());
        $("#mbaState").val($("#state").val());
        $("#mbaPostcode").val($("#postcode").val());
        $("#mbaCountry").val($("#country").val());
    }else{
//        $("#mbaAddress").val("");
//        $("#mbaCity").val("");
//        $("#mbaState").val("");
//        $("#mbaPostcode").val("");
//        $("#mbaCountry").val("");
    }
});
$(document).ready(function(){
    checkpassword($('input[name="password"]').val());
});

$('input[name="password"]').keyup(function(){
    var $self = $(this);
    checkpassword($self.val());
});

function checkpassword(pass){
    $.ajax({
        url: check_pass_url,
        method: 'POST',
        data: {'pass': pass},
        success: function(data) {
            var res = data.split('#');
            if(res[0]==1){
                $('#let').addClass('glyphicon-ok-circle').addClass('green');
                $('#let').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#let').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#let').addClass('glyphicon-remove-circle').addClass('red');
            }
            
            if(res[1]==1){
                $('#num').addClass('glyphicon-ok-circle').addClass('green');
                $('#num').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#num').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#num').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[2]==1){
                $('#spe').addClass('glyphicon-ok-circle').addClass('green');
                $('#spe').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#spe').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#spe').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[3]==1){
                $('#len').addClass('glyphicon-ok-circle').addClass('green');
                $('#len').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#len').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#len').addClass('glyphicon-remove-circle').addClass('red');
            }
        },
        error:function(){
        }
    });
}