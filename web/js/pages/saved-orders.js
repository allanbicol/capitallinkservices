/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    var oTable;

   /* Init the table */
    oTable =  $('#datatable-listings').dataTable( {
            "bProcessing": false,
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true, "bWidth": 100},
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": false, "bVisible": false },
                null
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return full[6] + "<br />" + full[1];
                     }
                },
                {
                    "aTargets": [6],
                     "mData": null,
                     "mRender": function (data, type, full) {
                        return '<a role="button" class="btn btn-primary btn-xs" href="'+open_order_url +'?order='+ full[6]+'&type='+ full[5]+'"><span class="glyphicon glyphicon-floppy-open"></span> Continue</a> <a role="button" class="btn btn-danger btn-xs" tag="'+full[5]+'" onclick="javascript: deleteSavedOrder('+full[6]+',this)"><span class="glyphicon glyphicon-remove"></span> Delete</a>';
                     }
                 }
              ],
    } );
     
   
 });
 
 
 











/*======================================================
 * functions
 ======================================================*/


// refresh datatables
function RefreshTable(tableId, urlData)
{
  $.getJSON(urlData, null, function( json )
  {
    table = $(tableId).dataTable();
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<json.aaData.length; i++)
    {
      table.oApi._fnAddData(oSettings, json.aaData[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
  });
  clearconsole();
}

 function deleteSavedOrder(order_no, that){
     bootbox.confirm("Do you really want to delete this saved order?", function(result) {
        if(result == true){
            $.ajax({
                url: del_order_url,
                method: 'POST',
                data: {'order_no': order_no, 'type' : $(that).attr('tag')},
                success: function(data) {
                    if(data == 'success'){
                        location.reload();
                    }
                    
                },
                error:function(){
                }
            });
        }
     });
 }