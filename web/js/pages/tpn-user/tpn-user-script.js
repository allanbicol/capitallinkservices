/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var editor1;
$(document).ready(function(){
    // tpn home
    if($("body#tpn-home").length > 0){
        var oTable;
        
       /* Init the table */
        oTable =  $('#datatable-listings').dataTable( {
                "bProcessing": false,
                "sAjaxSource": list_url,
                "aaSorting": [[0, 'asc']],
                "aoColumns": [ 
                    {"bSearchable": true, "bVisible": false },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    null
                ],
                 "aoColumnDefs": [
                     {
                            "aTargets": [1],
                             "mData": null,
                             "iDataSort": 0,
                             "mRender": function (data, type, full) {
                                if(full[1] != ''){
                                    var $fulldate = full[1].split(" ");
                                    var $date = $fulldate[0].split("-");
                                    var $day = $date[2];
                                    var $month = $date[1];
                                    var $year = $date[0];
                                    return $day + '-' + $month + '-' + $year + ' ' + $fulldate[1];
                                }else{
                                    return '';
                                }
                             }
                         },
                    {
                            "aTargets": [2],
                             "mData": null,
                             "mRender": function (data, type, full) {
                                if(full[2] != ''){
                                    var $fulldate = full[2].split(" ");
                                    var $date = $fulldate[0].split("-");
                                    var $day = $date[2];
                                    var $month = $date[1];
                                    var $year = $date[0];
                                    return $day + '-' + $month + '-' + $year + ' ' + $fulldate[1];
                                }else{
                                    return '';
                                }
                             }
                         },
                    {
                            "aTargets": [7],
                             "mData": null,
                             "mRender": function (data, type, full) {
                                if(full[7] != ''){
                                    var $date = full[7].split("-");
                                    var $day = $date[2];
                                    var $month = $date[1];
                                    var $year = $date[0];
                                    return $day + '-' + $month + '-' + $year;
                                }else{
                                    return '';
                                }
                             }
                         },
                     {
                        "aTargets": [8],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            if(full[8] == 0){
                                return "Applied";
                            }else if(full[8] == 1){
                                return "Accepted";
                            }else if(full[8] == 2){
                                return "Declined";
                            }
                         }
                     },
                    {
                        "aTargets": [9],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            return '<a role="button" class="btn btn-primary btn-xs" href="'+open_order_url +"?tpn="+ full[4]+'"><span class="glyphicon glyphicon-edit"></span> Edit </a>';
                         }
                     }
                  ],
        } );
     
     
     
    // tpn review
    }else if($("body#tpn-review").length > 0){
        
        
         bkLib.onDomLoaded(function() {
            editor1 = new nicEditor({
                iconsPath : plugins_path + '/nicEdit/nicEditorIcons.gif',
                buttonList : ['bold'],
            }).panelInstance('tpnSrc');
            
            
            
        });
        
        
        
    // tpn staff list
    }else if($("body#tpn-staff").length > 0){
        var oTable;

       /* Init the table */
        oTable =  $('#datatable-listings').dataTable( {
                "bProcessing": false,
                "sAjaxSource": list_url,
                "aoColumns": [ 
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": false, "bVisible": false },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": false, "bVisible": false },
                    null
                ],
                 "aoColumnDefs": [
                     {
                        "aTargets": [0],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            return full[0] + ' ' + full[1];
                         }
                     },
                    {
                            "aTargets": [3],
                             "mData": null,
                             "mRender": function (data, type, full) {
                                if(full[3] != ''){
                                    var $fulldate = full[3].split(" ");
                                    var $date = $fulldate[0].split("-");
                                    var $day = $date[2];
                                    var $month = $date[1];
                                    var $year = $date[0];
                                    return $day + '-' + $month + '-' + $year + ' ' + $fulldate[1];
                                }else{
                                    return '';
                                }
                             }
                         },
                     {
                        "aTargets": [4],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            return (full[4] == 0) ? 'No' : 'Yes';
                         }
                     },
                    {
                        "aTargets": [5],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            return '<a role="button" class="btn btn-primary btn-xs" href="'+open_url +"?id="+ full[5]+'"><span class="glyphicon glyphicon-edit"></span> Edit </a> <a class="btn btn-danger btn-xs" onclick="deleteUser('+full[5]+')"><span class="glyphicon glyphicon-trash"></span> Delete</a>';
                         }
                     }
                  ]
        } );
        
        
    }else if($("body#tpn-reports").length > 0){
        
        
       /* Init the table */
        $('#datatable-listings').dataTable( {
                "bProcessing": false,
                "sAjaxSource": list_url,
                "aaSorting": [[0, 'desc']],
                "scroll": true,
                "aoColumns": [ 
                    {"bSearchable": true, "bVisible": false },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": false },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": false },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": false },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": false },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    {"bSearchable": true, "bVisible": true },
                    null
                ],
                 "aoColumnDefs": [
                     {
                        "aTargets": [1],
                         "mData": null,
                         "iDataSort": 0,
                         "mRender": function (data, type, full) {
                            if(full[1] != ''){
                                var $datetime = full[1].split(" ");
                                var $date = $datetime[0].split("-");
                                var $day = $date[2];
                                var $month = $date[1];
                                var $year = $date[0];
                                return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                            }else{
                                return '';
                            }
                         }
                     },
                     {
                        "aTargets": [3],
                         "mData": null,
                         "iDataSort": 2,
                         "mRender": function (data, type, full) {
                            if(full[3] != ''){
                                var $datetime = full[3].split(" ");
                                var $date = $datetime[0].split("-");
                                var $day = $date[2];
                                var $month = $date[1];
                                var $year = $date[0];
                                return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                            }else{
                                return '';
                            }
                         }
                     },
                     {
                        "aTargets": [5],
                         "mData": null,
                         "iDataSort": 4,
                         "mRender": function (data, type, full) {
                            if(full[5] != ''){
                                var $date = full[5].split("-");
                                var $day = $date[2];
                                var $month = $date[1];
                                var $year = $date[0];
                                return $day + '-' + $month + '-' + $year;
                            }else{
                                return '';
                            }
                         }
                     },
                     {
                        "aTargets": [7],
                         "mData": null,
                         "iDataSort": 6,
                         "mRender": function (data, type, full) {
                            if(full[7] != ''){
                                var $datetime = full[7].split(" ");
                                var $date = $datetime[0].split("-");
                                var $day = $date[2];
                                var $month = $date[1];
                                var $year = $date[0];
                                return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                            }else{
                                return '';
                            }
                         }
                     },
                     {
                        "aTargets": [9],
                         "mData": null,
                         "iDataSort": 8,
                         "mRender": function (data, type, full) {
                            if(full[9] != ''){
                                var $datetime = full[9].split(" ");
                                var $date = $datetime[0].split("-");
                                var $day = $date[2];
                                var $month = $date[1];
                                var $year = $date[0];
                                return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                            }else{
                                return '';
                            }
                         }
                     },
                     {
                        "aTargets": [13],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            if(full[13] == 0){
                                return 'Pending';
                            }else if(full[13] == 1){
                                return 'Approved';
                            }else if(full[13] == 2){
                                return 'Rejected';
                            }
                         }
                     },
                     {
                        "aTargets": [14],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            if(full[14] == 2){
                                return 'TPN';
                            }else if(full[14] == 3){
                                return 'TPN+Visa';
                            }
                         }
                     },
                    {
                        "aTargets": [15],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            var string = full[15].replace(",",",<br>");
                            return string;
                         }
                     },
                    {
                        "aTargets": [16],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            var string = full[16].replace(",",",<br>");
                            return string;
                         }
                     },
                    {
                        "aTargets": [18],
                         "mData": null,
                         "mRender": function (data, type, full) {
                            return '<a role="button" class="btn btn-primary btn-xs" href="'+open_order_url +"?tpn="+ full[18]+'"><span class="glyphicon glyphicon-floppy-open"></span> View </a>';
                         }
                     }
                  ],
                  "sDom": 'T<"clear">lfrtip',
                    "oTableTools": {
                          "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                          "aButtons": [
                                {
                                    "sExtends": "copy",
                                    "mColumns": [1, 3, 4, 5, 6, 7, 8]
                                },
                                {
                                    "sExtends": "csv",
                                    "mColumns": [1, 3, 4, 5, 6, 7, 8]
                                },
                                {
                                    "sExtends": "pdf",
                                    "mColumns": [1, 3, 4, 5, 6, 7, 8],
                                    "fnClick":  function( nButton, oConfig, flash ) {
                                            window.open(api_url+"/pdfc/reports/dfat-report.php"+ params_);
                                            return false;
                                        }
                                },
                                {
                                    "sExtends": "print",
                                    "mColumns": [1, 3, 4, 5, 6, 7, 8]
                                }
                            ]
                    },
                    "fnInitComplete": function(oSettings, json) {
                        $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
                     }
        } );
//       $('#datatable-listings1').dataTable( {
//                "bProcessing": false,
//                "sAjaxSource": list_url,
//                "aaSorting": [[0, 'desc']],
//                "aoColumns": [ 
//                    {"bSearchable": true, "bVisible": false },
//                    {"bSearchable": true, "bVisible": true },
//                    {"bSearchable": false, "bVisible": false },
//                    {"bSearchable": true, "bVisible": true },
//                    {"bSearchable": true, "bVisible": true },
//                    {"bSearchable": true, "bVisible": true },
//                    {"bSearchable": true, "bVisible": true },
//                    {"bSearchable": true, "bVisible": true },
//                    {"bSearchable": true, "bVisible": true },
//                    null
//                ],
//                 "aoColumnDefs": [
//                     {
//                        "aTargets": [1],
//                         "mData": null,
//                         "iDataSort": 0,
//                         "mRender": function (data, type, full) {
//                            // edited by leokarl 2-27-2015
//                            if(full[1] != ''){
//                                var $fulldate = full[1].split(" ");
//                                var $date = $fulldate[0].split("-");
//                                var $day = $date[2];
//                                var $month = $date[1];
//                                var $year = $date[0];
//                                return $day + '-' + $month + '-' + $year + ' ' + $fulldate[1];
//                            }else{
//                                return '';
//                            }
//                         }
//                     },
//                     {
//                        "aTargets": [8],
//                         "mData": null,
//                         "mRender": function (data, type, full) {
//                            if(full[8] == 0){
//                                return "Applied";
//                            }else if(full[8] == 1){
//                                return "Accepted";
//                            }else if(full[8] == 2){
//                                return "Declined";
//                            }
//                         }
//                     },
//                    {
//                        "aTargets": [9],
//                         "mData": null,
//                         "mRender": function (data, type, full) {
//                            return '<a role="button" class="btn btn-primary btn-xs" href="'+open_order_url +"?tpn="+ full[4]+'"><span class="glyphicon glyphicon-floppy-open"></span> View </a>';
//                         }
//                     }
//                  ],
//                  "sDom": 'T<"clear">lfrtip',
//                    "oTableTools": {
//                          "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
//                          "aButtons": [
//                                {
//                                    "sExtends": "copy",
//                                    "mColumns": [1, 3, 4, 5, 6, 7, 8]
//                                },
//                                {
//                                    "sExtends": "csv",
//                                    "mColumns": [1, 3, 4, 5, 6, 7, 8]
//                                },
//                                {
//                                    "sExtends": "pdf",
//                                    "mColumns": [1, 3, 4, 5, 6, 7, 8]
//                                },
//                            ]
//                    },
//                    "fnInitComplete": function(oSettings, json) {
//                        $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
//                     }
//        } );
     
     
     
    }else if($("body#tpn-activities").length > 0){
        $('#datatable-listings').dataTable({
            "bProcessing": false,
            "sAjaxSource": list_url,
            "aaSorting": [[0, 'desc']],
            "iDisplayLength": 100,
            "aoColumns": [ 
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": false },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true },
                {"bSearchable": true, "bVisible": true }
            ],
             "aoColumnDefs": [
                {
                    "aTargets": [1],
                     "mData": null,
                     "iDataSort": 0,
                     "mRender": function (data, type, full) {
                         var $datetime = full[1].split(" ");
                               var $date = $datetime[0].split("-");
                               var $day = $date[2];
                               var $month = $date[1];
                               var $year = $date[0];
                               return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                     }
                 },
                {
                    "aTargets": [4],
                     "mData": null,
                     "mRender": function (data, type, full) {
                         return full[4].toUpperCase();
                     }
                 }
              ],
              "sDom": 'T<"clear">lfrtip',
              "oTableTools": {
                    "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [
                        {
                            "sExtends": "copy",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "csv",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "pdf",
                            "mColumns": [1, 2, 3, 4, 5]
                        },
                        {
                            "sExtends": "print",
                            "mColumns": [1, 2, 3, 4, 5]
                        }
                    ]
              },
              "fnInitComplete": function(oSettings, json) {
                $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
             }
        });
    }
    
    if (typeof new_pass_req !== 'undefined') {
    checkpassword($('input[name="password"]').val());
        if(new_pass_req==1){
            $('#modalNew').modal('show');
        }
    }
 });

$('input[name="password"]').keyup(function(){
    var $self = $(this);
    checkpassword($self.val());
});

$('#btnUpdatePassword').click(function(){
    var pass = $('input[name="password"]').val();
    var con_pass = $('input[name="passwordConfirm"]').val();
    if(pass !='' ){
        if(con_pass!=pass){
            alert('Password should be match!');
            return false;
        }
        $.ajax({
            url: update_pass_url,
            method: 'POST',
            data: {'password': pass,'passwordConfirm':con_pass},
            success: function(data) {
                if(data==0){
                    
                    $('#msg_div').show();
                    $('#msg').html('Your password has been successfully updated.');
                    $('#btnDone').show();
                    $('#btnUpdatePassword').hide();
                }
            }, 
            error:function(){
            }
        });
    }else{
        alert('Password field is required!');
    }
});

function checkpassword(pass){
    $.ajax({
        url: check_pass_url,
        method: 'POST',
        data: {'pass': pass},
        success: function(data) {
            var res = data.split('#');
            if(res[0]==1){
                $('#let').addClass('glyphicon-ok-circle').addClass('green');
                $('#let').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#let').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#let').addClass('glyphicon-remove-circle').addClass('red');
            }
            
            if(res[1]==1){
                $('#num').addClass('glyphicon-ok-circle').addClass('green');
                $('#num').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#num').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#num').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[2]==1){
                $('#spe').addClass('glyphicon-ok-circle').addClass('green');
                $('#spe').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#spe').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#spe').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[3]==1){
                $('#len').addClass('glyphicon-ok-circle').addClass('green');
                $('#len').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#len').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#len').addClass('glyphicon-remove-circle').addClass('red');
            }
        },
        error:function(){
        }
    });
}
 
 
$("body#tpn-review #btnDownload").click(function(){
    $("#submitType").val("download"); 
});
 
$("body#tpn-review #btnSave").click(function(){
    $("#submitType").val("save"); 
 });
 
$("body#tpn-review #btnReprint").click(function(){
    $("#submitType").val("reprint"); 
    $("#frmTpn").attr("target", '_blank');
 });

 $("body#tpn-review #btnDecline").click(function(){
    $("#submitType").val("decline"); 
    // appy changes to textarea
    for(var i=0;i<editor1.nicInstances.length;i++){editor1.nicInstances[i].saveContent();}
    
    bootbox.dialog({
        message: '<label>Reason:</label><textarea class="form-control" name="tpnNote" id="tpnNote"></textarea>',
        title: "Are you sure you want to reject this TPN?",
        buttons: {
          success: {
            label: "Cancel",
            className: "btn-default",
            callback: function() {
              //Example.show("great success");
            }
          },
          main: {
            label: "Yes",
            className: "btn-primary",
            callback: function(result) {
                if($("#tpnNote").val().trim() != ""){
                    $("#tpnRejectNote").val($("#tpnNote").val());
                    $("#frmTpn").submit();
                }else{
                    $("#tpnNote").focus();
                    return false;
                }
            }
          }
        }
      });
      
 });


 $("body#tpn-review #btnAccept").click(function(){
    $("#submitType").val("accept"); 
    // appy changes to textarea
    for(var i=0;i<editor1.nicInstances.length;i++){editor1.nicInstances[i].saveContent();}
    
    bootbox.confirm("Are you sure you want to accept and issue this TPN?", function(result) {
      if(result == true){
          $("#frmTpn").submit();
      }
    }); 
    return false;
 });


$("#tpn-review #addNote").click(function(){
   $.ajax({
        url: add_note_url,
        method: 'POST',
        data: {'note': $("#tpn-review #note").val(), 'tpn_no': tpn_no},
        success: function(data) {
            var obj = $.parseJSON(data);
            $("#tpn-review ul#tpnNotes").append('<li><span class="note-text">'+ obj["note"] + '</span><br/><span class="noted-by"><i>- by '+ obj["added_by"] +' '+ obj["date_added"] +'</i></span></li>');
        },
        error:function(){
        }
    });
});





$(document).ready(function(){
  $("body").find("table.table").wrap( "<div class='table_scroll_wrap'></div>" );
});


$("body").on("click",".dataTable-buttons > .DTTT_button.DTTT_button_print",function(){
    $("body").find("#datatable-listings_wrapper").find("div.DTTT_container").find(".DTTT_button.DTTT_button_print").click();
});


//added by Allan 03/02/2015
$("body#tpn-review #btnReissue").click(function(){
    $("#submitType").val("reissue"); 
});
$("body#tpn-review #btnPreviousTpn").click(function(){
    $("#submitType").val("previousTpnLetter"); 
});

$("body#tpn-review #btnCurrentTpn").click(function(){
    $("#submitType").val("currentTpnLetter"); 
});
/*======================================================
 * functions
 ======================================================*/


// refresh datatables
function RefreshTable(tableId, urlData)
{
  $.getJSON(urlData, null, function( json )
  {
    table = $(tableId).dataTable();
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<json.aaData.length; i++)
    {
      table.oApi._fnAddData(oSettings, json.aaData[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
  });
  //clearconsole();
}



function deleteUser(user_id){
  bootbox.confirm("Are you sure?", function(result) {
    if(result == true){
        $.ajax({
            url: del_url,
            method: 'POST',
            data: {'tpn_user_id': user_id},
            success: function(data) {
//                RefreshTable('datatable-listings', list_url);
                location.reload();
            },
            error:function(){
            }
        });
    }
  }); 
}