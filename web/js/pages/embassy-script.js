/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function(){
    
    
   $('#datatable-listings').dataTable({
        "bProcessing": false,
        "sAjaxSource": list_url,
        "aaSorting": [[0, 'desc']],
        "aoColumns": [ 
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true },
            {"bSearchable": true, "bVisible": true }
        ],
         "aoColumnDefs": [
             {
                "aTargets": [1],
                 "mData": null,
                 "mRender": function (data, type, full) {
                     if(full[1] != ''){
                        var $date = full[1].split("-");
                        var $day = $date[2];
                        var $month = $date[1];
                        var $year = $date[0];
                        return $day + '-' + $month + '-' + $year;
                     }else{
                         return full[1];
                     }
                 }
             },
             {
                "aTargets": [2],
                 "mData": null,
                 "mRender": function (data, type, full) {
                    if(full[2] != ''){
                        var $datetime = full[2].split(" ");
                        var $date = $datetime[0].split("-");
                        var $day = $date[2];
                        var $month = $date[1];
                        var $year = $date[0];
                        return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                    }else{
                        return full[2];
                    }
                 }
             },
             {
                "aTargets": [3],
                 "mData": null,
                 "mRender": function (data, type, full) {
                    if(full[3] != ''){
                        var $datetime = full[3].split(" ");
                        var $date = $datetime[0].split("-");
                        var $day = $date[2];
                        var $month = $date[1];
                        var $year = $date[0];
                        return $day + '-' + $month + '-' + $year + ' ' + $datetime[1];
                    }else{
                        return full[3];
                    }
                 }
             },
             {
                "aTargets": [4],
                 "mData": null,
                 "mRender": function (data, type, full) {
                    var $applicants = full[4].split(',');
                    var $return = '';
                    if($applicants.length > 1 ){
                        for(i=0; i<$applicants.length; i++){
                            $return = $return + '-' + $applicants[i] + '<br/>';
                        }
                    }else{
                        $return = $return + '-'+ $applicants[0];
                    }
                    return $return;
                 }
             }
             
          ],
            "sDom": 'T<"clear">lfrtip',
            "oTableTools": {
                  "sSwfPath": api_url + "/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                  "aButtons": [
                        {"sExtends": "copy"},
                        {"sExtends": "csv"},
                        {"sExtends": "pdf"}
                    ]
            },
            "fnInitComplete": function(oSettings, json) {
              $("div.dataTable-buttons").html( $('div.DTTT_container').html() );
           }
    }); 
    
    if (typeof new_pass_req !== 'undefined') {
    checkpassword($('input[name="password"]').val());
        if(new_pass_req==1){
            setTimeout(function(){
                $('#modalNew').modal('show');
            },3000);
        }
    }
    
});

$('input[name="password"]').keyup(function(){
    var $self = $(this);
    checkpassword($self.val());
});

$('#btnUpdatePassword').click(function(){
    var pass = $('input[name="password"]').val();
    var con_pass = $('input[name="passwordConfirm"]').val();
    if(pass !='' ){
        if(con_pass!=pass){
            alert('Password should be match!');
            return false;
        }
        $.ajax({
            url: update_pass_url,
            method: 'POST',
            data: {'password': pass,'passwordConfirm':con_pass},
            success: function(data) {
                if(data==0){
                    
                    $('#msg_div').show();
                    $('#msg').html('Your password has been successfully updated.');
                    $('#btnDone').show();
                    $('#btnUpdatePassword').hide();
                }
            }, 
            error:function(){
            }
        });
    }else{
        alert('Password field is required!');
    }
});

function checkpassword(pass){
    $.ajax({
        url: check_pass_url,
        method: 'POST',
        data: {'pass': pass},
        success: function(data) {
            var res = data.split('#');
            if(res[0]==1){
                $('#let').addClass('glyphicon-ok-circle').addClass('green');
                $('#let').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#let').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#let').addClass('glyphicon-remove-circle').addClass('red');
            }
            
            if(res[1]==1){
                $('#num').addClass('glyphicon-ok-circle').addClass('green');
                $('#num').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#num').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#num').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[2]==1){
                $('#spe').addClass('glyphicon-ok-circle').addClass('green');
                $('#spe').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#spe').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#spe').addClass('glyphicon-remove-circle').addClass('red');
            }
            if(res[3]==1){
                $('#len').addClass('glyphicon-ok-circle').addClass('green');
                $('#len').removeClass('glyphicon-remove-circle').removeClass('red');
            }else{
                $('#len').removeClass('glyphicon-ok-circle').removeClass('green');
                $('#len').addClass('glyphicon-remove-circle').addClass('red');
            }
        },
        error:function(){
        }
    });
}