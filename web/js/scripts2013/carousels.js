$(document).ready(function(){
	scrollCarousel();
	fadeCarousel();
	//toggleNav();
});


//function toggleNav() {
	//var SideNav = $('.sideNav');
	//SideNav.find('li ul').show().hide();
	
	//SideNav.find('li').hover(function(){
		//var UL = $(this).children('ul');
		//if( !UL.length ) return;
		
		//if( UL.is(':visible') ) {
			//UL.fadeOut();
			//SideNav.css({ top:'' });
			//var default_top = SideNav.css('top');
			//SideNav.css({ top:0 }).animate({ top:default_top });
		//} else {
			//UL.fadeIn();
			//SideNav.animate({ top:0 });
		//}
		//return;
	//});;
//}




// Scroll Carousel  (MODIFIED)
//==================================================
function scrollCarousel(){
	$('.scrollCarousel').each(function(){
		
		// EDITABLE VARS
		var slides_change = 5.0;		// seconds (how long slides stay still)
		var slides_transition = 1.0;	// seconds (duration of transition between slides)
		var multiple_increments = false;	// true or false
		var autoplay = true;			// true or false
		var update_buttons = false;		// true or false
		var keyboard_navigation = false;// true or false
		
		// Select Objects
		var Block = $(this);
		Block.wrapInner('<div>');
		var Container = Block.children();
		var Slides = Container.find('.slide');
		// LOOP DUPLICATE
		Container.append( Slides.clone() );
		
		// Setup Vars
		slides_change *= 1000;
		slides_transition *= 1000;
		var total = Slides.length;
		var width = Slides.outerWidth(true);
		var slides_visible = Math.ceil( Block.width()/width );
		var increments = multiple_increments ? slides_visible : 1;
		var limit = multiple_increments ?  Math.floor((total-1)/increments)*increments : total-slides_visible;
		var current = -1;
		var moving = false;
		var timer = false;
		
		// Fade Details
		Slides.mouseenter(function(){
			$(this).find('.showInfo').hide().fadeIn();
		});
		
		// Exit if carousel not needed
		if( limit<1 )
			return false;
		
		// FUNCTION: showSlide
		function showSlide(number,speed){
			
			// Defaults
			if(speed===undefined) speed=slides_transition;
			
			// Validate
			if( number>limit+1 ) number=0;
			if( number<0 ) number=limit+1;
			if( number==current || moving )
				return false;
			
			// Update Vars and Links
			moving = true;
			current = number;
			clearTimeout(timer);
			
			// Move Container
			var left = number*-width;
			Container.animate({left:left},{ duration:speed, complete:function(){
				moving = false;
				if( autoplay ) {
					timer = setTimeout(showNext,slides_change );
				}
				
				
				/* LOOP! */
				var First = Container.find('.slide').first();
				var first_offset = First.offset();
				var first_width = First.outerWidth();
				
				if( first_offset.left + first_width <= 0 ) {
					Container.css({ left:left+first_width }).append(First);
					current -= 1;
				}
				
			} });
			
			// Update Buttons
			if( update_buttons ) {
				Prev.show();
				Next.show();
				if( number<1 )
					Prev.hide();
				if( number>= limit )
					Next.hide();
			}
		}
		
		// FUNCTION: showPrevious
		function showPrevious() {
			showSlide(current-increments);
		}
		
		// FUNCTION: showNext
		function showNext() {
			showSlide(current+increments);
		}
		
		// Create Navigation
		var Prev = $('<a>',{href:'#',text:'Previous','class':'navButton prev'});
		var Next = $('<a>',{href:'#',text:'Next','class':'navButton next'});
		Prev.click( function(){showPrevious();return false;} );
		Next.click( function(){showNext();return false;} );
		Block.prepend( Prev, Next );
		
		// Keyboard Navigation
		if( keyboard_navigation ) {
			$(document).keydown(function(event){
				var key = event.which;
				if( key==37 )      showPrevious();
				else if( key==39 ) showNext();
			});
		}
		
		// Show First
		showSlide(2,0);
	});
};


// Fade Carousel
//==================================================
function fadeCarousel() {
	$('.fadeCarousel').each(function(){
		
		// EDITABLE PARAMS
		var slides_change = 4.0;	// seconds (how long slides stay still)
		var slides_transition = 1.0;// seconds (duration of transition between slides)
		
		// Select Objects
		var Container = $(this)
		var Slides = Container.find('.slide');
		if( Slides.length<2 )
			return false;
		
		// Setup Vars
		slides_change *= 1000;
		slides_transition *= 1000;
		var total = Slides.length;
		var current = -1;
		var changing = false;
		var timer = false;
		
		// FUNCTION: showSlide
		function showSlide(number,speed) {
			
			// Defaults
			if(speed===undefined) speed=slides_transition;
			
			// Validate
			if( number>total-1 ) number=0;
			if( number<0 ) number=total-1;
			if( number==current || changing )
				return false;
			
			// Select objects
			var Current = Slides.eq(current);
			var Selected = Slides.eq(number);
			var Others = Slides.not(Current).not(Selected);
			
			// Update Vars
			changing = true;
			current = number;
			NavLinks.removeClass('selected').eq(number).addClass('selected');
			clearTimeout(timer);
			
			// Change Slides
			Current.css({ zIndex:5 });
			Selected.hide().css({ zIndex:10 }).fadeIn(speed,function(){
				Current.css({ zIndex:0 });
				changing = false;
				timer = setTimeout( showNext, slides_change );
			});
		}
		
		// FUNCTION: showNext
		function showNext() {
			showSlide( current+1 );
		}
		
		// Create Navigation
		var NavContainer = $('<div>',{'class':'fadeCarouselNav'});
		for( var i=0; i<Slides.length; i++ ) {
			NavContainer.append('<a href="#">'+'</a>');
		}
		var NavLinks = NavContainer.children('a');
		NavLinks.click(function(){
			var number = $(this).index();
			showSlide(number);
			return false;
		});
		Slides.parent().prepend(NavContainer);
		
		// Start!
		showSlide(0,0);
	});
}