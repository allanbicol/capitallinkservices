<?php

$webdir = dirname(__FILE__);
$webdir = str_replace("\web\crons","",$webdir);
$webdir = str_replace("/web/crons","",$webdir);
$webdir = str_replace("\\","/",$webdir);

require_once $webdir . '/vendor/swiftmailer/swiftmailer/lib/swift_required.php';

$transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 25)
  ->setUsername('info@voodoocreative.com.au')
  ->setPassword('rMqbOuJ1RFPqakul3It_8w')
  ;
// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

if(php_uname("n") != 'cls-production'){
    $con=mysqli_connect("localhost","root","","cls");
    $siteURL = 'http://localhost/capitallinkservices.com.au';
    $asset_url='http://localhost/capitallinkservices.com.au/web';
}else{
    //$con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
    $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    $siteURL = 'https://www.capitallinkservices.com.au';
    $asset_url='https://www.capitallinkservices.com.au/web';
}

$result = mysqli_query($con,"SELECT ofud.*, b.order_type, admin.fname, admin.lname, admin.email
    FROM tbl_order_follow_up_date ofud
    LEFT JOIN tbl_orders b ON b.order_no = ofud.order_id
    LEFT JOIN tbl_user_admin admin ON admin.id = ofud.admin_id
    WHERE DATE(ofud.follow_up_date) = CURDATE() AND b.`status` >= 10");


while($row = mysqli_fetch_array($result)){
    //1=visa; 2=tpn; 3=tpn+visa; 4=passport delivery; 5=police clearance; 6=public visa; 7=document delivery; 8=russian visa voucher; 9=DL
    $order_type = '';
    if($row['order_type']== 1){
        $order_type = 'Visa';
    }elseif($row['order_type']== 2){
        $order_type = 'TPN';
    }elseif($row['order_type']== 3){
        $order_type = 'TPN + Visa';
    }elseif($row['order_type']== 4){
        $order_type = 'Passport Delivery';
    }elseif($row['order_type']== 5){
        $order_type = 'Police Clearance';
    }elseif($row['order_type']== 6){
        $order_type = 'Public Visa';
    }elseif($row['order_type']== 7){
        $order_type = 'Document Delivery';
    }elseif($row['order_type']== 8){
        $order_type = 'Russian Visa Voucher';
    }elseif($row['order_type']== 9){
        $order_type = 'Document Legalisation';
    }
    
    $yourWebsite = "CLS [".$order_type." - Follow Up]"; // the name of your website
    $recipients = $row['email'];
    $from = "help@capitallinkservices.com.au";
    
//    if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
//        $headers = "From: $from";
//        $headers.= "Reply-To: <$recipients>";
//        $headers .= "MIME-Version: 1.0\r\n";
//        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//    }else{
//        $headers = "From: $yourWebsite $from\n";
//        $headers.= "Reply-To: <$recipients>";
//        $headers .= "MIME-Version: 1.0\r\n";
//        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//    }

    $body = '<html>';
    $body .= '<head>';
    $body .= '</head>';
    $body .= '<body>';
    $body .= '<p>Hi '.$row['fname'].', </p>';
    $body .= '<p>You\'ve set a follow up reminder for an order in the CLS. Please click <a href="'.$siteURL.'/admin/view-order?order_no='.$row['order_id'].'">here</a> to see the order details.</p>';
    $body .= '<p>Kind regards,<br/>Capital Link Services<br/><img src="'.$asset_url.'/img/email-logo.png"></p>';
    $body .= '<p><b>A.C.N</b> 095720531 &nbsp;&nbsp;/&nbsp;&nbsp; <b>A.B.N</b> 31986341198</p>';
    $body .= '</body>';
    $body .= '</html>';
    //mail($recipients,"Visa Order - Follow Up",$body,$headers);
    
    
    // Create a message
    $message = Swift_Message::newInstance('Visa Order - Follow Up')
      ->setFrom(array('help@capitallinkservices.com.au' => 'CLS [Visa Order - Follow Up]'))
      ->setTo(array($recipients))
      ->setBody($body, 'text/html')
      ;
    // Send the message
    $result1 = $mailer->send($message);
}

//$headers = "From: CLS [Visa Order - Follow Up] help@capitallinkservices.com.au\n";
//$headers.= "Reply-To: <leo@voodoocreative.com.au>";
//$headers .= "MIME-Version: 1.0\r\n";
//$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//
//mail('leo@voodoocreative.com.au',"Visa Order - Follow Up admin (Cronjob)",'test',$headers);


$message = Swift_Message::newInstance('Visa Order - Follow Up admin (Cronjob)')
      ->setFrom(array('help@capitallinkservices.com.au' => 'CLS [Visa Order - Follow Up]'))
      ->setTo(array('leo@voodoocreative.com.au'))
      ->setBody('test')
      ;
// Send the message
$result1 = $mailer->send($message);