<?php


$webdir = dirname(__FILE__);
$webdir = str_replace("\web\crons","",$webdir);
$webdir = str_replace("/web/crons","",$webdir);
$webdir = str_replace("\\","/",$webdir);

require_once $webdir . '/vendor/swiftmailer/swiftmailer/lib/swift_required.php';

$transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 25)
  ->setUsername('info@voodoocreative.com.au')
  ->setPassword('rMqbOuJ1RFPqakul3It_8w')
  ;
// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

if(php_uname("n") != 'cls-production'){
    $con=mysqli_connect("localhost","root","","cls");
    $siteURL = 'http://localhost/capitallinkservices.com.au';
    $asset_url='http://localhost/capitallinkservices.com.au/web';
}else{
    //$con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
    $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    $siteURL = 'https://www.capitallinkservices.com.au';
    $asset_url='https://www.capitallinkservices.com.au/web';
}

$result = mysqli_query($con,"SELECT a.*, c.fname, c.lname, c.email
    FROM tbl_order_destinations a 
    LEFT JOIN tbl_orders b ON b.order_no = a.order_no
    LEFT JOIN tbl_user_client c ON c.id=b.client_id
    WHERE DATE(a.visa_follow_up_date) = CURDATE() AND b.`status` >= 10");

while($row = mysqli_fetch_array($result)){
    
    $yourWebsite = "CLS [Visa Order - Follow Up]"; // the name of your website
    $recipients = $row['email'];
    $from = "help@capitallinkservices.com.au";
    
    if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
        $headers = "From: $from";
        $headers.= "Reply-To: <$recipients>";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    }else{
        $headers = "From: $yourWebsite $from\n";
        $headers.= "Reply-To: <$recipients>";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    }

    $body = '<html>';
    $body .= '<head>';
    $body .= '</head>';
    $body .= '<body>';
    $body .= '<p>Hi '.$row['fname'].', </p>';
    $body .= '<p>You set a follow up reminder for an order in the CLS client centre. Please click <a href="'.$siteURL.'/client/view-visa-order?order_no='.$row['order_no'].'">here</a> to see the order details.</p>';
    $body .= '<p>Kind regards,<br/>Capital Link Services<br/><img src="'.$asset_url.'/img/email-logo.png"></p>';
    $body .= '<p><b>A.C.N</b> 095720531 &nbsp;&nbsp;/&nbsp;&nbsp; <b>A.B.N</b> 31986341198</p>';
    $body .= '</body>';
    $body .= '</html>';
    //mail($recipients,"Visa Order - Follow Up",$body,$headers);
    
    // Create a message
    $message = Swift_Message::newInstance('Visa Order - Follow Up')
      ->setFrom(array('help@capitallinkservices.com.au' => 'CLS [Visa Order - Follow Up]'))
      ->setTo(array($recipients))
      ->setBody($body, 'text/html')
      ;
    // Send the message
    $result1 = $mailer->send($message);
}
//$headers = "From: CLS [Visa Order - Follow Up] help@capitallinkservices.com.au\n";
//$headers.= "Reply-To: <leo@voodoocreative.com.au>";
//$headers .= "MIME-Version: 1.0\r\n";
//$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//mail('leo@voodoocreative.com.au',"Visa Order - Follow Up (Cronjob)",'test',$headers);
$message = Swift_Message::newInstance('Visa Order - Follow Up (Cronjob)')
      ->setFrom(array('help@capitallinkservices.com.au' => 'CLS [Visa Order - Follow Up]'))
      ->setTo(array('leo@voodoocreative.com.au'))
      ->setBody('test')
      ;
// Send the message
$result1 = $mailer->send($message);