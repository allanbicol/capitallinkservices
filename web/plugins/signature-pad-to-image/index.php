<?php

require_once 'signature-to-image.php';

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}
function ausDateFormat($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($y, $m , $d) = explode("-", $stringdate);
        return $d.'/'.$m.'/'.$y;
    }
}


if($_SERVER['SERVER_NAME'] == 'localhost'){
    $con=mysqli_connect("localhost","root","","cls");
    $img_url = siteURL().'/capitallinkservices.com.au/web/img';
    $asset_url = siteURL().'/capitallinkservices.com.au/web';
}else{
//        $con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
    $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    $img_url = siteURL().'/web/img';
    $asset_url = siteURL().'/web';
}

if(!isset($_GET['order_no']) || !isset($_GET['type'])){
    exit();
}

$_GET['order_no'] = intval($_GET['order_no']);

$query = mysqli_query($con,"SELECT o.*
            FROM tbl_orders o
            WHERE order_type=7 AND o.order_no=".$_GET['order_no']);

$order = mysqli_fetch_array($query);

if( $_GET['type'] == 'sender'  ){
    $img = sigJsonToImage( $order['sender_signature'] );
}else{
    $img = sigJsonToImage( $order['signature'] );
}
// Save to file
//imagepng($img, 'signature.png');

// Output to browser
header('Content-Type: image/png');
imagepng($img);

// Destroy the image in memory when complete
imagedestroy($img);
