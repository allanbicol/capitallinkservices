<?php
require_once( 'EwayPayment.php' );

//$eway = new EwayPayment( '91281879', 'https://www.eway.com.au/gateway_cvn/xmltest/testpage.asp' );
$eway = new EwayPayment( '18983792', 'https://www.eway.com.au/gateway_cvn/xmlpayment.asp' );

//Substitute 'FirstName', 'Lastname' etc for $_POST["FieldName"] where FieldName is the name of your INPUT field on your webpage
$eway->setCustomerFirstname( urldecode($_POST['customerFirstname']) ); 
$eway->setCustomerLastname( urldecode($_POST['customerLastname']) );
$eway->setCustomerEmail( urldecode($_POST['customerEmail']) );
$eway->setCustomerAddress( urldecode($_POST['customerAddress']) );
$eway->setCustomerPostcode( urldecode($_POST['customerPostcode']) );
$eway->setCustomerInvoiceDescription( urldecode($_POST['customerInvoiceDescription']) );
$eway->setCustomerInvoiceRef( urldecode($_POST['customerInvoiceRef']) );
$eway->setCardHoldersName( urldecode($_POST['cardHoldersName']) );
$eway->setCardNumber( urldecode($_POST['cardNumber']) );
$eway->setCardExpiryMonth( urldecode($_POST['cardExpiryMonth']) );
$eway->setCardExpiryYear( urldecode($_POST['cardExpiryYear']) );
$eway->setTrxnNumber( urldecode($_POST['trxnNumber']) );
$eway->setTotalAmount( urldecode($_POST['totalAmount']) );
$eway->setCVN( urldecode($_POST['cvn']) );


$result = '';
if( $eway->doPayment() == EWAY_TRANSACTION_OK ) {
    $result = "Transaction successful. Auth Code: " . $eway->getAuthCode();
    $data =  array('success' => $result);
} else {
    $result = "Transaction unsuccessful.! There was an error with your credit card processing:   (".$eway->getError()."): " . $eway->getErrorMessage();
    $data =  array('error' => $result);
}

echo json_encode($data);
?>
