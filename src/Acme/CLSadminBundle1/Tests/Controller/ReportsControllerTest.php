<?php

namespace Acme\CLSadminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ReportsControllerTest extends WebTestCase
{
    public function testOpentickets()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/reports/open-tickets');
    }

    public function testClosedtickets()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/reports/closed-tickets');
    }

}
