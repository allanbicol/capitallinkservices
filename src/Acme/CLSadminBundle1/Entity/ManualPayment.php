<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ManualPayment
 *
 * @ORM\Table(name="tbl_manual_payment")
 * @ORM\Entity
 */
class ManualPayment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="order_no", type="string", length=100)
     */
    private $order_no;

    /**
     * @var string
     *
     * @ORM\Column(name="cust_name", type="string", length=200)
     */
    private $cust_name;

    /**
     * @var string
     *
     * @ORM\Column(name="cust_email", type="string", length=100)
     */
    private $cust_email;

    /**
     * @var string
     *
     * @ORM\Column(name="items", type="string")
     */
    private $items;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_details", type="string")
     */
    private $payment_details;

    /**
     * @var string
     *
     * @ORM\Column(name="billing_details", type="string")
     */
    private $billing_details;

    /**
     * @var string
     *
     * @ORM\Column(name="card_details", type="string")
     */
    private $card_details;
    
    /**
     * @var float
     *
     * @ORM\Column(name="grand_total", type="float")
     */
    private $grand_total;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order_no
     *
     * @param string $orderNo
     * @return ManualPayment
     */
    public function setOrderNo($orderNo)
    {
        $this->order_no = $orderNo;
    
        return $this;
    }

    /**
     * Get order_no
     *
     * @return string 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set cust_name
     *
     * @param string $custName
     * @return ManualPayment
     */
    public function setCustName($custName)
    {
        $this->cust_name = $custName;
    
        return $this;
    }

    /**
     * Get cust_name
     *
     * @return string 
     */
    public function getCustName()
    {
        return $this->cust_name;
    }

    /**
     * Set cust_email
     *
     * @param string $custEmail
     * @return ManualPayment
     */
    public function setCustEmail($custEmail)
    {
        $this->cust_email = $custEmail;
    
        return $this;
    }

    /**
     * Get cust_email
     *
     * @return string 
     */
    public function getCustEmail()
    {
        return $this->cust_email;
    }

    /**
     * Set items
     *
     * @param string $items
     * @return ManualPayment
     */
    public function setItems($items)
    {
        $this->items = $items;
    
        return $this;
    }

    /**
     * Get items
     *
     * @return string 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set payment_details
     *
     * @param string $paymentDetails
     * @return ManualPayment
     */
    public function setPaymentDetails($paymentDetails)
    {
        $this->payment_details = $paymentDetails;
    
        return $this;
    }

    /**
     * Get payment_details
     *
     * @return string 
     */
    public function getPaymentDetails()
    {
        return $this->payment_details;
    }

    /**
     * Set billing_details
     *
     * @param string $billingDetails
     * @return ManualPayment
     */
    public function setBillingDetails($billingDetails)
    {
        $this->billing_details = $billingDetails;
    
        return $this;
    }

    /**
     * Get billing_details
     *
     * @return string 
     */
    public function getBillingDetails()
    {
        return $this->billing_details;
    }

    /**
     * Set card_details
     *
     * @param string $cardDetails
     * @return ManualPayment
     */
    public function setCardDetails($cardDetails)
    {
        $this->card_details = $cardDetails;
    
        return $this;
    }

    /**
     * Get card_details
     *
     * @return string 
     */
    public function getCardDetails()
    {
        return $this->card_details;
    }

    /**
     * Set grand_total
     *
     * @param float $grandTotal
     * @return ManualPayment
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grand_total = $grandTotal;
    
        return $this;
    }

    /**
     * Get grand_total
     *
     * @return float 
     */
    public function getGrandTotal()
    {
        return $this->grand_total;
    }
}