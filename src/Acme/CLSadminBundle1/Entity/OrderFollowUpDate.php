<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderFollowUpDate
 *
 * @ORM\Table(name="tbl_order_follow_up_date")
 * @ORM\Entity
 */
class OrderFollowUpDate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="admin_id", type="integer")
     */
    private $admin_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer")
     */
    private $order_id;

    /**
     * @var string
     *
     * @ORM\Column(name="follow_up_date", type="string", length=20)
     */
    private $follow_up_date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set admin_id
     *
     * @param integer $adminId
     * @return OrderFollowUpDate
     */
    public function setAdminId($adminId)
    {
        $this->admin_id = $adminId;
    
        return $this;
    }

    /**
     * Get admin_id
     *
     * @return integer 
     */
    public function getAdminId()
    {
        return $this->admin_id;
    }

    /**
     * Set order_id
     *
     * @param integer $orderId
     * @return OrderFollowUpDate
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;
    
        return $this;
    }

    /**
     * Get order_id
     *
     * @return integer 
     */
    public function getOrderId()
    {
        return $this->order_id;
    }


    /**
     * Set follow_up_date
     *
     * @param string $followUpDate
     * @return OrderFollowUpDate
     */
    public function setFollowUpDate($followUpDate)
    {
        $this->follow_up_date = $followUpDate;
    
        return $this;
    }

    /**
     * Get follow_up_date
     *
     * @return string 
     */
    public function getFollowUpDate()
    {
        return $this->follow_up_date;
    }
}