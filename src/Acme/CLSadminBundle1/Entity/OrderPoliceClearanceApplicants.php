<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderPoliceClearanceApplicants
 *
 * @ORM\Table(name="tbl_order_police_clearance_applicants")
 * @ORM\Entity
 */
class OrderPoliceClearanceApplicants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_no", type="integer")
     */
    private $order_no;

    /**
     * @var string
     *
     * @ORM\Column(name="fname", type="string", length=100)
     */
    private $fname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="mname", type="string", length=100)
     */
    private $mname;

    /**
     * @var string
     *
     * @ORM\Column(name="lname", type="string", length=100)
     */
    private $lname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=20)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=500)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=100)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=100)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="country_id", type="integer")
     */
    private $country_id;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_no", type="string", length=200)
     */
    private $passport_no;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_date", type="string", length=20)
     */
    private $departure_date;


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order_no
     *
     * @param integer $orderNo
     * @return OrderPoliceClearanceApplicants
     */
    public function setOrderNo($orderNo)
    {
        $this->order_no = $orderNo;
    
        return $this;
    }

    /**
     * Get order_no
     *
     * @return integer 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return OrderPoliceClearanceApplicants
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return OrderPoliceClearanceApplicants
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return OrderPoliceClearanceApplicants
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return OrderPoliceClearanceApplicants
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return OrderPoliceClearanceApplicants
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    
        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return OrderPoliceClearanceApplicants
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return OrderPoliceClearanceApplicants
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return OrderPoliceClearanceApplicants
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return OrderPoliceClearanceApplicants
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return OrderPoliceClearanceApplicants
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;
    
        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set passport_no
     *
     * @param string $passportNo
     * @return OrderPoliceClearanceApplicants
     */
    public function setPassportNo($passportNo)
    {
        $this->passport_no = $passportNo;
    
        return $this;
    }

    /**
     * Get passport_no
     *
     * @return string 
     */
    public function getPassportNo()
    {
        return $this->passport_no;
    }

    /**
     * Set departure_date
     *
     * @param string $departureDate
     * @return OrderPoliceClearanceApplicants
     */
    public function setDepartureDate($departureDate)
    {
        $this->departure_date = $departureDate;
    
        return $this;
    }

    /**
     * Get departure_date
     *
     * @return string 
     */
    public function getDepartureDate()
    {
        return $this->departure_date;
    }

    /**
     * Set mname
     *
     * @param string $mname
     * @return OrderPoliceClearanceApplicants
     */
    public function setMname($mname)
    {
        $this->mname = $mname;
    
        return $this;
    }

    /**
     * Get mname
     *
     * @return string 
     */
    public function getMname()
    {
        return $this->mname;
    }
}