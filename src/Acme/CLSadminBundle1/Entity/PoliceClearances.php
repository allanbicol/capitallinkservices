<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PoliceClearances
 *
 * @ORM\Table(name="tbl_police_clearances")
 * @ORM\Entity
 */
class PoliceClearances
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=500)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name_additional", type="string", length=500)
     */
    private $name_additional;

    /**
     * @var float
     *
     * @ORM\Column(name="price_additional", type="float")
     */
    private $price_additional;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=1000)
     */
    private $file_path;

    /**
     * @var string
     *
     * @ORM\Column(name="gen_info", type="string")
     */
    private $gen_info;


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PoliceClearances
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return PoliceClearances
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PoliceClearances
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set file_path
     *
     * @param string $filePath
     * @return PoliceClearances
     */
    public function setFilePath($filePath)
    {
        $this->file_path = $filePath;
    
        return $this;
    }

    /**
     * Get file_path
     *
     * @return string 
     */
    public function getFilePath()
    {
        return $this->file_path;
    }

    /**
     * Set gen_info
     *
     * @param string $genInfo
     * @return PoliceClearances
     */
    public function setGenInfo($genInfo)
    {
        $this->gen_info = $genInfo;
    
        return $this;
    }

    /**
     * Get gen_info
     *
     * @return string 
     */
    public function getGenInfo()
    {
        return $this->gen_info;
    }

    /**
     * Set name_additional
     *
     * @param string $nameAdditional
     * @return PoliceClearances
     */
    public function setNameAdditional($nameAdditional)
    {
        $this->name_additional = $nameAdditional;
    
        return $this;
    }

    /**
     * Get name_additional
     *
     * @return string 
     */
    public function getNameAdditional()
    {
        return $this->name_additional;
    }

    /**
     * Set price_additional
     *
     * @param float $priceAdditional
     * @return PoliceClearances
     */
    public function setPriceAdditional($priceAdditional)
    {
        $this->price_additional = $priceAdditional;
    
        return $this;
    }

    /**
     * Get price_additional
     *
     * @return float 
     */
    public function getPriceAdditional()
    {
        return $this->price_additional;
    }
}