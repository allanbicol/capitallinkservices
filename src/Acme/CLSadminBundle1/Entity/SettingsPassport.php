<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SettingsTPN
 *
 * @ORM\Table(name="tbl_settings_passport")
 * @ORM\Entity
 */
class SettingsPassport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     * @Assert\NotBlank(message="Passport office pickup or delivery cost should not be blank.")
     * @Assert\Type(type="numeric", message="Passport office pickup or delivery cost should be of type numeric.")
     * @ORM\Column(name="cost", type="float")
     */
    private $cost;

    /**
     * @var float
     * @Assert\NotBlank(message="Passport office pickup or delivery cost Additional Applicant should not be blank.")
     * @Assert\Type(type="numeric", message="Passport office pickup or delivery cost  Additional Applicant should be of type numeric.")
     * @ORM\Column(name="additional_cost", type="float")
     */
    private $additional_cost;


   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return SettingsPassport
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set additional_cost
     *
     * @param float $additionalCost
     * @return SettingsPassport
     */
    public function setAdditionalCost($additionalCost)
    {
        $this->additional_cost = $additionalCost;
    
        return $this;
    }

    /**
     * Get additional_cost
     *
     * @return float 
     */
    public function getAdditionalCost()
    {
        return $this->additional_cost;
    }
}