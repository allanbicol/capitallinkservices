<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderNotes
 *
 * @ORM\Table(name="tbl_order_destination_notes")
 * @ORM\Entity
 */
class OrderDestinationNotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="destination_id", type="integer")
     */
    private $destination_id;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string")
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="date_added", type="string", length=50)
     */
    private $date_added;

    /**
     * @var integer
     *
     * @ORM\Column(name="note_by", type="integer")
     */
    private $note_by;
    
    /**
     * @var string
     *
     * @ORM\Column(name="note_by_name", type="string")
     */
    private $note_by_name;

    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=10)
     */
    private $user_type;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_admin", type="integer")
     */
    private $is_admin;
    


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set destination_id
     *
     * @param integer $destinationId
     * @return OrderDestinationNotes
     */
    public function setDestinationId($destinationId)
    {
        $this->destination_id = $destinationId;
    
        return $this;
    }

    /**
     * Get destination_id
     *
     * @return integer 
     */
    public function getDestinationId()
    {
        return $this->destination_id;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return OrderDestinationNotes
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set date_added
     *
     * @param string $dateAdded
     * @return OrderDestinationNotes
     */
    public function setDateAdded($dateAdded)
    {
        $this->date_added = $dateAdded;
    
        return $this;
    }

    /**
     * Get date_added
     *
     * @return string 
     */
    public function getDateAdded()
    {
        return $this->date_added;
    }

    /**
     * Set note_by
     *
     * @param integer $noteBy
     * @return OrderDestinationNotes
     */
    public function setNoteBy($noteBy)
    {
        $this->note_by = $noteBy;
    
        return $this;
    }

    /**
     * Get note_by
     *
     * @return integer 
     */
    public function getNoteBy()
    {
        return $this->note_by;
    }

    /**
     * Set note_by_name
     *
     * @param string $noteByName
     * @return OrderDestinationNotes
     */
    public function setNoteByName($noteByName)
    {
        $this->note_by_name = $noteByName;
    
        return $this;
    }

    /**
     * Get note_by_name
     *
     * @return string 
     */
    public function getNoteByName()
    {
        return $this->note_by_name;
    }

    /**
     * Set user_type
     *
     * @param string $userType
     * @return OrderDestinationNotes
     */
    public function setUserType($userType)
    {
        $this->user_type = $userType;
    
        return $this;
    }

    /**
     * Get user_type
     *
     * @return string 
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * Set is_admin
     *
     * @param integer $isAdmin
     * @return OrderDestinationNotes
     */
    public function setIsAdmin($isAdmin)
    {
        $this->is_admin = $isAdmin;
    
        return $this;
    }

    /**
     * Get is_admin
     *
     * @return integer 
     */
    public function getIsAdmin()
    {
        return $this->is_admin;
    }
}