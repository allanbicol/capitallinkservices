<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderPassportApplicants
 *
 * @ORM\Table(name="tbl_order_passport_applicants")
 * @ORM\Entity
 */
class OrderPassportApplicants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(name="order_no", type="integer")
     */
    private $order_no;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=1000)
     */
    private $fullname;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="personal_passport", type="integer")
     */
    private $personal_passport;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="diplomatic_official_passport", type="integer")
     */
    private $diplomatic_official_passport;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="birth_certificate", type="integer")
     */
    private $birth_certificate;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="marriage_certificate", type="integer")
     */
    private $marriage_certificate;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="certificate_of_australian_citizenship", type="integer")
     */
    private $certificate_of_australian_citizenship;
    

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order_no
     *
     * @param integer $orderNo
     * @return OrderPassportApplicants
     */
    public function setOrderNo($orderNo)
    {
        $this->order_no = $orderNo;
    
        return $this;
    }

    /**
     * Get order_no
     *
     * @return integer 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     * @return OrderPassportApplicants
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    
        return $this;
    }

    /**
     * Get fullname
     *
     * @return string 
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set personal_passport
     *
     * @param integer $personalPassport
     * @return OrderPassportApplicants
     */
    public function setPersonalPassport($personalPassport)
    {
        $this->personal_passport = $personalPassport;
    
        return $this;
    }

    /**
     * Get personal_passport
     *
     * @return integer 
     */
    public function getPersonalPassport()
    {
        return $this->personal_passport;
    }

    /**
     * Set diplomatic_official_passport
     *
     * @param integer $diplomaticOfficialPassport
     * @return OrderPassportApplicants
     */
    public function setDiplomaticOfficialPassport($diplomaticOfficialPassport)
    {
        $this->diplomatic_official_passport = $diplomaticOfficialPassport;
    
        return $this;
    }

    /**
     * Get diplomatic_official_passport
     *
     * @return integer 
     */
    public function getDiplomaticOfficialPassport()
    {
        return $this->diplomatic_official_passport;
    }

    /**
     * Set birth_certificate
     *
     * @param integer $birthCertificate
     * @return OrderPassportApplicants
     */
    public function setBirthCertificate($birthCertificate)
    {
        $this->birth_certificate = $birthCertificate;
    
        return $this;
    }

    /**
     * Get birth_certificate
     *
     * @return integer 
     */
    public function getBirthCertificate()
    {
        return $this->birth_certificate;
    }

    /**
     * Set marriage_certificate
     *
     * @param integer $marriageCertificate
     * @return OrderPassportApplicants
     */
    public function setMarriageCertificate($marriageCertificate)
    {
        $this->marriage_certificate = $marriageCertificate;
    
        return $this;
    }

    /**
     * Get marriage_certificate
     *
     * @return integer 
     */
    public function getMarriageCertificate()
    {
        return $this->marriage_certificate;
    }

    /**
     * Set certificate_of_australian_citizenship
     *
     * @param integer $certificateOfAustralianCitizenship
     * @return OrderPassportApplicants
     */
    public function setCertificateOfAustralianCitizenship($certificateOfAustralianCitizenship)
    {
        $this->certificate_of_australian_citizenship = $certificateOfAustralianCitizenship;
    
        return $this;
    }

    /**
     * Get certificate_of_australian_citizenship
     *
     * @return integer 
     */
    public function getCertificateOfAustralianCitizenship()
    {
        return $this->certificate_of_australian_citizenship;
    }
}