<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSadminBundle\Entity\SettingsPassport;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManagePassportOfficePickupDeliveryController extends Controller
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-passport-office-pickup-delivery');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            $model = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            $model->setCost($_POST['passportOfficePickupDelivery']);
            $model->setAdditionalCost($_POST['passportOfficePickupDeliveryAdditional']);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'cost'=>$_POST['passportOfficePickupDelivery'],
                'additionalCost'=>$_POST['passportOfficePickupDeliveryAdditional']
            );
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Passport Office Pickup or Delivery settings updated!'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_passport_office_pickup_delivery'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManagePassportOfficePickupDelivery:index.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
            
        }else{
            $em = $this->getDoctrine()->getManager();
            $model = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            
            return $this->render('AcmeCLSadminBundle:ManagePassportOfficePickupDelivery:index.html.twig',
                    array('post'=>$model)
                    );
        }
    }

}
