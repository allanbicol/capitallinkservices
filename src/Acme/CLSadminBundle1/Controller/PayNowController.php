<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class PayNowController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function payNowAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            
            $em->getConnection()->beginTransaction();

            
            
            $payment_details = $this->getPaymentDetailsByOrderNo($_POST["orderNumber"]);
            if(count($payment_details) > 0){
                $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            }else{
                $model = new Payment();
            }
            
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($client_id);
            $model->setOrderNo($_POST["orderNumber"]);
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption($_POST["paymentType"]);
            if(isset($_POST["accountNumber"])){
                $model->setAccountNo($_POST["accountNumber"]);
            }
//            $model->setNameOnCard($_POST["nameOnCard"]);
//            $model->setCardNumber($_POST["cardNumber"]);
//            $model->setCardExpiryMonth($_POST["cardExpiryMonth"]);
//            $model->setCardExpiryYear($_POST["cardExpiryYear"]);
//            //$model->setCardType($_POST["cardType"]);
//            $model->setCcvNumber($_POST["ccvNumber"]);
            
            if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                $total_order_price = $order->getGrandTotal();
            }else{
                $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
            }
            
            //$total_order_price = round($total_order_price);
            
            $total_order_price = number_format($total_order_price, 2,'.','');
            $model->setTotalOrderPrice($total_order_price);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
            if($_POST["paymentType"] == 1){
                $model->setSPaid(1);
            }else{
                $model->setSPaid(2);
            }
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'department'=>$_POST['department'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'paymentType'=>$_POST["paymentType"],
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            if($error_count == 0){
                if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors = array();
                    $special_error=1;
                }
                
                if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                    $error_count += 1;
                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors[] = array('message'=>'Account number is not correct!');
                    $error_count += 1;
                }
            }
            
            
            if($error_count == 0){
                
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $data['mba_country']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS Visa Application'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                   $root_dir = dirname($this->get('kernel')->getRootDir());
                   
                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
                   
                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               
               /**
                * Credit card payment
                * Position: End
                */

                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();

                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
            
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSadminBundle:PayNow:payNow.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'departments'=> $this->getDepartments(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details,
                                'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                        'special_price'=> $user->getSpecialPrice()
                                    ),
                                ));
                }else{
                    return $this->render('AcmeCLSadminBundle:PayNow:payNow.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'departments'=> $this->getDepartments(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details,
                                'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                        'special_price'=> $user->getSpecialPrice()
                                    ),
                                ));
                }

            }else{
                
                //$order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                if($order->getStatus() <= 10){
                    $order->setStatus(10); // 10= Order placed
                }
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $order->setGrandTotal($total_order_price);
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                $em->persist($order);
                $em->flush();
                
                
                
                

                // commit changes
                $em->getConnection()->commit(); 

                
                $em = $this->getDoctrine()->getManager();
                $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
                    
                //$host = $request->getScheme().'://'.$request->getHost();
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                
                if($order->getOrderType() == 1){
                    // email invoice
                    $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Visa Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings
                                        )
                                    ),
                            $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                            );

                    // manual order email to admin
                    if($session->get('user_type') == 'admin-user'){
                        // email invoice
                        $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Visa Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings,
                                            'manual_order'=>1
                                            )
                                        ),
                                $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                                );
                    }

                    if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                        // email invoice (copy to admins)
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Visa Order Reciept (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings
                                            )
                                        ),
                                $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                                );
                    }
                    
                    
                    
                    
                    
                    
                }else if($order->getOrderType() == 2){
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    // email invoice
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings
                                        )
                                    )
                            );


                    // email submission summary
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Submission", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:order_details_email_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'data'=> $this->getOrderDetailsByOrderNo($_POST["orderNumber"])
                                        )
                                    )
                            );

                    // manual order email to admin
                    if($session->get('user_type') == 'admin-user'){
                        // email invoice
                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings,
                                            'manual_order'=>1
                                            )
                                        )
                                );


                        // email submission summary
                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Submission", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:order_details_email_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'data'=> $this->getOrderDetailsByOrderNo($_POST["orderNumber"]),
                                            'manual_order'=>1
                                            )
                                        )
                                );
                    }

                    if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                        // email invoice (copy to admins)
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmail($email_copies, "help@capitallinkservices.com.au", "TPN Order Reciept (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings
                                            )
                                        )
                                );
                    }
                    
                    
                    
                    
                }else if($order->getOrderType() == 3){
                    
                    
                    
                    
                    
                    
                    
                    
                    // email invoice
                    $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "TPN+Visa Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings
                                        )
                                    ),
                            $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                            );


                    // email submission summary
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Submission", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:order_details_email_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'data'=> $data
                                        )
                                    )
                            );

                    // manual order email to admin
                    if($session->get('user_type') == 'admin-user'){
                        // email invoice
                        $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN+Visa Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings,
                                            'manual_order'=>1
                                            )
                                        ),
                                $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                                );


                        // email submission summary
                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Submission", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:order_details_email_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'data'=> $data,
                                            'manual_order'=>1
                                            )
                                        )
                                );
                    }

                    if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                        // email invoice (copy to admins)
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "TPN+Visa Order Reciept (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings
                                            )
                                        ),
                                $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                                );
                    }
                    
                 
                    
                    
                    
                    
                }else if($order->getOrderType() == 5){
                    
                    
                    
                    
                    
                    
                        $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Police Clearance Application Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data
                                    )
                                ),
                                //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                array('0'=>
                                    array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/pclearancefiles/' . $data['police_clearance_id'] . '/'. $data['police_clearance_file'])
                                )
                        );

                        // manual order email to admin
                        if($session->get('user_type') == 'admin-user'){
                            $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Police Clearance Application Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'manual_order'=>1
                                        )
                                    ),
                                    //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                    array('0'=>
                                        array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/pclearancefiles/' . $data['police_clearance_id'] . '/'. $data['police_clearance_file'])
                                    )
                            );
                        }
                        if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                            // email invoice (copy to admins)
                            $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                            $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Police Clearance Application Receipt (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    ),
                                    //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                    array('0'=>
                                        array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/pclearancefiles/' . $data['police_clearance_id'] . '/'. $data['police_clearance_file'])
                                    )
                            );
                        }
                    
                    
                }else if($order->getOrderType() == 6){
                    
                    
                    
                    
                    
                    
                    
                    
                    // email invoice
                    $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Visa Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationPublicVisa:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings
                                        )
                                    ),
                            $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                            );

                    // manual order email to admin
                    if($session->get('user_type') == 'admin-user'){ 
                        // email invoice
                        $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Visa Manual Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationPublicVisa:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings,
                                        'manual_order'=>1
                                        )
                                    ),
                            $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                            );
                    }
                    if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                        // email invoice (copy to admins)
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Visa Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationPublicVisa:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings
                                        )
                                    ),
                            $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                        );
                    }
                    
                    
                    
                }else if($order->getOrderType() == 7){
                    
                    
                    
                    
                    // email invoice
                        $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Document Delivery Order Receipt", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    )
                            );


                        $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Courier Booking Submission",
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:booking_submission.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    )
                            );

                        // manual order email to admin
                        if($session->get('user_type') == 'admin-user'){ 

                            $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "Document Delivery Manual Order Receipt", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$order->getOrderNo(),
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'manual_order'=>1
                                            )
                                        )
                                );


                            $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "Courier Booking Submission - Manual Order",
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:booking_submission.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$order->getOrderNo(),
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'manual_order'=>1
                                            )
                                        )
                                );

                        }


                        if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                            // email invoice (copy to admins)
                            $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                            $this->sendEmail($email_copies, "help@capitallinkservices.com.au", "Document Delivery Order Receipt (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$order->getOrderNo(),
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data
                                            )
                                        )
                                );
                        }
                    
                }else if($order->getOrderType() == 8){
                    
                    
                    
                    
                    
                    
                        if(trim($data['rvv_file']) != ''){
                            $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data
                                            )
                                        ),
                                array('0'=>
                                    array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                    )
                                );

                            // manual order email to admin
                            if($session->get('user_type') == 'admin-user'){ 
                                $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Russian Visa Voucher Manual Order Reciept", 
                                    $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                            array('domain'=>$mod->siteURL(),
                                                'order_no'=>$_POST["orderNumber"],
                                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                                'data'=>$data,
                                                'manual_order'=>1
                                                )
                                            ),
                                    array('0'=>
                                        array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                        )
                                    );
                            }
                            if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                                // email invoice (copy to admins)
                                $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                                $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                                    $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                            array('domain'=>$mod->siteURL(),
                                                'order_no'=>$_POST["orderNumber"],
                                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                                'data'=>$data
                                                )
                                            ),
                                    array('0'=>
                                        array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                        )
                                    );
                            }

                            $admins = $this->getAdminUsers();
                            for($i=0; $i<count($admins); $i++){
                                $this->sendEmailWithAttachment($admins[$i]['email'], "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                                    $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                            array('domain'=>$mod->siteURL(),
                                                'order_no'=>$_POST["orderNumber"],
                                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                                'data'=>$data
                                                )
                                            ),
                                    array('0'=>
                                        array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                        )
                                    );
                            }

                        }else{
                            $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data
                                            )
                                        )
                                );

                            $admins = $this->getAdminUsers();
                            for($i=0; $i<count($admins); $i++){
                                $this->sendEmail($admins[$i]['email'], "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                                    $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                            array('domain'=>$mod->siteURL(),
                                                'order_no'=>$_POST["orderNumber"],
                                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                                'data'=>$data
                                                )
                                            )
                                    );
                            }
                        }

                    
                }
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );

                
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $em = $this->getDoctrine()->getManager();
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            if(count($data) > 0){
                // if order status done with the review order page
                // if not, send to review order page
                if( $data["status"] >= 2 ){


                    return $this->render('AcmeCLSadminBundle:PayNow:payNow.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'user_details'=>$user_details,
                            'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                        'special_price'=> $user->getSpecialPrice()
                                    ),
                            'tpn_settings' => $tpn_settings,
                            ));

                }

            }else{
                return $this->render('AcmeCLSclientGovBundle:PayNow:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }

}
