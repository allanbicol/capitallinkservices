<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\SettingsTPN;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;
use Acme\CLSadminBundle\Entity\OrderDlQuotes;

class OrderDlQuotesController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function createQuoteAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(!isset($_POST['order_no'])){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_POST['order_no'] = intval($_POST['order_no']);
        
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        
        $items = array();
        $group = $this->generateOrderDlQuotesGroupByOrderNo($_POST['order_no']);
        $grand_total = 0;
        for($i=0; $i<count($_POST['qdescription']); $i++){
            $_POST['qdescription'][$i] = filter_var($_POST['qdescription'][$i], FILTER_SANITIZE_STRING);
            $_POST['qprice'][$i] = floatval($_POST['qprice'][$i]);
            $_POST['quantity'][$i] = intval($_POST['quantity'][$i]);
            $_POST['gst'][$i] = intval($_POST['gst'][$i]);
            $_POST['gst'][$i] = ($_POST['gst'][$i] == 1) ? 10 : 0;
            
            $total =  ($_POST['qprice'][$i] * $_POST['quantity'][$i]);
            $gst = 0;
            if($_POST['gst'][$i] == 10){
                $gst = $total * 0.10;
            }
            $total = $total + $gst;
                    
            $model = new OrderDlQuotes();
            $model->setAdminId($session->get('admin_id'));
            $model->setOrderNo($_POST['order_no']);
            $model->setDescription($_POST['qdescription'][$i]);
            $model->setQuantity($_POST['quantity'][$i]);
            $model->setPrice($_POST['qprice'][$i]);
            $model->setGst($_POST['gst'][$i]);
            $model->setTotal($total);
            $model->setSentGroup($group);
            $model->setSentDate($datetime->format("Y-m-d H:i:s"));
            $em->persist($model);
            $em->flush();
            
            $items[] = array('description'=>$_POST['qdescription'][$i], 
                'price'=>$_POST['qprice'][$i], 
                'quantity'=>$_POST['quantity'][$i], 
                'gst'=>$_POST['gst'][$i]);
            
            
            $grand_total += $total;
        }
        
        
        $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["order_no"]));
        $order->setGrandTotal($grand_total);
        $em->persist($order);
        $em->flush();
            
        $em->getConnection()->commit(); 
        
        $od = $this->getOrderDetailsByOrderNo($_POST['order_no']);
        
        $this->sendEmail($od['user_email'], 'help@capitallinkservices.com.au', 'Document Legalisation - Quotation', 
            $this->renderView('AcmeCLSadminBundle:OrderDlQuotes:email_quote.html.twig',
                array('domain'=>$mod->siteURL(),
                    'order_no'=>$_POST['order_no'],
                    'data'=>$od,
                    'dl_quotes'=>$items,
                    'group'=>$group
                    )
                )
        );
        
        $this->get('session')->getFlashBag()->add(
                'success',
                'Your quotation(s) have been sent to customer!'
            );
        
        
        
        return $this->redirect($this->generateUrl('acme_cls_admin_view_order').'?order_no='.$_POST['order_no']); 
    }
    
    public function deleteDlQuoteAction(){
        $_POST['order_no'] = intval($_POST['order_no']);
        $_POST['group'] = intval($_POST['group']);
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('DELETE AcmeCLSadminBundle:OrderDlQuotes d WHERE d.order_no = '.$_POST['order_no'].' AND d.sent_group='.$_POST['group']);
        $query->execute(); 
        return new Response("success");
    }

}
