<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\SettingsTPN;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageTPNController extends Controller
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-tpn');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            $model->setTpn($_POST['tpn']);
            $model->setTpnAdditional($_POST['additionalTpn']);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'tpn'=>$_POST['tpn'],
                'tpnAdditional'=>$_POST['additionalTpn']
            );
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'TPN settings updated!'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_tpn'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageTPN:index.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
            
        }else{
            $em = $this->getDoctrine()->getManager();
            $model = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            
            return $this->render('AcmeCLSadminBundle:ManageTPN:index.html.twig',
                    array('post'=>$model)
                    );
        }
    }

}
