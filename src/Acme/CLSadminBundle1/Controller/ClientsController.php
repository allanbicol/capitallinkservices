<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ClientsController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-clients');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:Clients:index.html.twig');
    }

    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('u')
                ->select("u.id, u.type, u.title, u.fname, u.lname, d.name as organisation, u.email,
                    u.phone, c.countryName as country")
                ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = u.department_id')
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = u.country_id')
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("organisation","fname","lname","email","phone","type","country","id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function newClientAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-clients');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        
        if(isset($_POST['hid_submit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = new User();
            $model->setType($_POST['clientType']);
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPassword($mod->passGenerator($_POST['password']));
            $model->setEmail($_POST['email']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            if($_POST['clientType'] == 'government'){
                $model->setDepartmentId($_POST['department']);
            }else{
                $model->setCompany($_POST["company"]);
            }
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountryId($_POST['country']);
            
            $model->setMddaAddress($_POST['mddaAddress']);
            $model->setMddaCity($_POST['mddaCity']);
            $model->setMddaState($_POST['mddaState']);
            $model->setMddaPostcode($_POST['mddaPostcode']);
            $model->setMddaCountryId($_POST['mddaCountry']);
            
            $model->setMbaAddress($_POST['mbaAddress']);
            $model->setMbaCity($_POST['mbaCity']);
            $model->setMbaState($_POST['mbaState']);
            $model->setMbaPostcode($_POST['mbaPostcode']);
            $model->setMbaCountryId($_POST['mbaCountry']);
            
            $canChargeCostToAccount = (isset($_POST["canChargeCostToAccount"])) ? 1 : 0;
            $canGetSpecialPrice = (isset($_POST["canGetSpecialPrice"])) ? 1 : 0;
            
            $model->setCanChargeCostToAccount($canChargeCostToAccount);
            $model->setAccountNo($_POST["accountNumber"]);
            
            $model->setCanGetSpecialPrice($canGetSpecialPrice);
            $model->setSpecialPrice($_POST["specialPrice"]);
            
            $model->setSEnabled(1);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'password'=>$_POST['password'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'department'=>$_POST['department'],
                'company'=>$_POST['company'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'mddaAddress'=>$_POST['mddaAddress'],
                'mddaCity'=>$_POST['mddaCity'],
                'mddaState'=>$_POST['mddaState'],
                'mddaPostcode'=>$_POST['mddaPostcode'],
                'mddaCountry'=>$_POST['mddaCountry'],
                'mbaAddress'=>$_POST['mbaAddress'],
                'mbaCity'=>$_POST['mbaCity'],
                'mbaState'=>$_POST['mbaState'],
                'mbaPostcode'=>$_POST['mbaPostcode'],
                'mbaCountry'=>$_POST['mbaCountry'],
                'type'=> $_POST['clientType'],
                'canChargeCostToAccount' => $canChargeCostToAccount,
                'accountNumber' => $_POST["accountNumber"],
                'canGetSpecialPrice' => $canGetSpecialPrice,
                'specialPrice' => $_POST["specialPrice"]
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['passwordConfirm']) || $this->sEmailAvailable($_POST['email']) == false){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['passwordConfirm']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
                
                if($this->sEmailAvailable($_POST['email']) == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
            }
            
            
                
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                 $this->get('session')->getFlashBag()->add(
                        'success',
                        'New Client has been added'
                    );
                 
                return $this->redirect($this->generateUrl('acme_cls_admin_clients')); 
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSadminBundle:Clients:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                            'name_titles'=>$this->getNameTitles(),
                            'countries'=> $this->getCountries('priority'),
                            'departments'=> $this->getDepartments()
                        ));
            }
        }else{
            return $this->render('AcmeCLSadminBundle:Clients:new.html.twig',
                    array('name_titles'=>$this->getNameTitles(),
                        'countries'=> $this->getCountries('priority'),
                        'departments'=> $this->getDepartments()
                        )
                    );
        }
    }
    
    
    public function editClientAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-clients');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        
        if(isset($_POST['hid_submit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$_POST['clientId']));
            //$model->setType($_POST['clientType']);
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setEmail($_POST['email']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            if($model->getType() == 'government'){
                $model->setDepartmentId($_POST['department']);
            }else{
                $model->setCompany($_POST["company"]);
            }
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountryId($_POST['country']);
            
            $model->setMddaAddress($_POST['mddaAddress']);
            $model->setMddaCity($_POST['mddaCity']);
            $model->setMddaState($_POST['mddaState']);
            $model->setMddaPostcode($_POST['mddaPostcode']);
            $model->setMddaCountryId($_POST['mddaCountry']);
            
            $model->setMbaAddress($_POST['mbaAddress']);
            $model->setMbaCity($_POST['mbaCity']);
            $model->setMbaState($_POST['mbaState']);
            $model->setMbaPostcode($_POST['mbaPostcode']);
            $model->setMbaCountryId($_POST['mbaCountry']);
            
            $canChargeCostToAccount = (isset($_POST["canChargeCostToAccount"])) ? 1 : 0;
            $canGetSpecialPrice = (isset($_POST["canGetSpecialPrice"])) ? 1 : 0;
            
            $model->setCanChargeCostToAccount($canChargeCostToAccount);
            $model->setAccountNo($_POST["accountNumber"]);
            
            $model->setCanGetSpecialPrice($canGetSpecialPrice);
            $model->setSpecialPrice($_POST["specialPrice"]);
            
            //$model->setSEnabled(1);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'id'=>$_POST['clientId'],
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'password'=>$_POST['password'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'department'=> isset($_POST['department']) ? $_POST['department'] : "",
                'company'=>isset($_POST['company']) ? $_POST['company'] : "",
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country_id'=>$_POST['country'],
                'mdda_address'=>$_POST['mddaAddress'],
                'mdda_city'=>$_POST['mddaCity'],
                'mdda_state'=>$_POST['mddaState'],
                'mdda_postcode'=>$_POST['mddaPostcode'],
                'mdda_country_id'=>$_POST['mddaCountry'],
                'mba_address'=>$_POST['mbaAddress'],
                'mba_city'=>$_POST['mbaCity'],
                'mba_state'=>$_POST['mbaState'],
                'mba_postcode'=>$_POST['mbaPostcode'],
                'mba_country_id'=>$_POST['mbaCountry'],
                'type'=> $model->getType(),
                'can_charge_cost_to_account' => $canChargeCostToAccount,
                'account_no' => $_POST["accountNumber"],
                'can_get_special_price' => $canGetSpecialPrice,
                'special_price' => $_POST["specialPrice"]
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || (trim($_POST['password']) != '' && ($_POST['password'] != $_POST['passwordConfirm'])) || $this->sEmailAvailableInDetailUpdate($_POST['email'], $_POST["clientId"], 'client-user') == false){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }
                
                
                if(trim($_POST['password']) != '' && ($_POST['password'] != $_POST['passwordConfirm'])){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
                
                if($this->sEmailAvailableInDetailUpdate($_POST['email'], $_POST["clientId"], 'client-user') == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
            }
            
            
                
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                 $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['fname']. ' details have been updated'
                    );
                 
                return $this->redirect($this->generateUrl('acme_cls_admin_clients')); 
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSadminBundle:Clients:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                            'name_titles'=>$this->getNameTitles(),
                            'countries'=> $this->getCountries('priority'),
                            'departments'=> $this->getDepartments(),
                            'tickets'=> $this->getVisaTicketsByClientId($_POST['clientId'])
                        ));
            }
        }else{
            return $this->render('AcmeCLSadminBundle:Clients:edit.html.twig',
                    array('name_titles'=>$this->getNameTitles(),
                        'countries'=> $this->getCountries('priority'),
                        'departments'=> $this->getDepartments(),
                        'post'=>$this->getClientDetailsById($_GET["user"]),
                        'tickets'=> $this->getVisaTicketsByClientId($_GET["user"])
                        )
                    );
        }
    }
    
    
    
    
    
    
    public function deleteClientAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['client_id'])){
            return new Response("invalid id");
        }
        
        $_POST['client_id'] = filter_var($_POST['client_id'], FILTER_SANITIZE_STRING);
        $_POST['client_id'] = intval($_POST['client_id']);
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $statement = $connection->prepare("DELETE u, o, od FROM tbl_user_client u 
            LEFT JOIN tbl_orders o ON o.client_id = u.id
            LEFT JOIN tbl_order_destinations od ON od.order_no = o.order_no
            WHERE u.id=".$_POST['client_id']);
        $statement->execute();
        
        
        return new Response("success");
        
    }
    
    
    
    public function deleteOrderAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_GET['order_no'])){
            return new Response("invalid order number");
        }
        
        $_GET['order_no'] = filter_var($_GET['order_no'], FILTER_SANITIZE_STRING);
        $_GET['order_no'] = intval($_GET['order_no']);
        $_GET['client'] = filter_var($_GET['client'], FILTER_SANITIZE_STRING);
        $_GET['client'] = intval($_GET['client']);
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $statement = $connection->prepare("DELETE FROM tbl_order_police_clearance_applicants WHERE order_no=".$_GET['order_no']);
        $statement->execute();
        
        $statement = $connection->prepare("DELETE FROM tbl_order_passport_applicants WHERE order_no=".$_GET['order_no']);
        $statement->execute();
        
        $statement = $connection->prepare("DELETE FROM tbl_order_notes WHERE order_no=".$_GET['order_no']);
        $statement->execute();
        
        $statement = $connection->prepare("DELETE FROM tbl_order_travellers WHERE order_no=".$_GET['order_no']);
        $statement->execute();
        
        $statement = $connection->prepare("DELETE od, odn 
            FROM tbl_order_destinations od 
            LEFT JOIN tbl_order_destination_notes odn ON odn.destination_id = od.id
            WHERE od.order_no=".$_GET['order_no']);
        $statement->execute();
        
        $statement = $connection->prepare("DELETE FROM tbl_orders WHERE order_no=".$_GET['order_no']);
        $statement->execute();
        
        
        $this->get('session')->getFlashBag()->add(
            'success',
            'Order ' . $_GET['order_no'] . ' has been deleted!'
        );
        
        return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$_GET['client']);
    }
}
