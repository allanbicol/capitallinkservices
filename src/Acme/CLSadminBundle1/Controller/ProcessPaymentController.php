<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSadminBundle\Entity\ManualPayment;

class ProcessPaymentController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'process-payment');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(isset($_POST['hidSubmit'])){
            $em = $this->getDoctrine()->getManager();
            
            $em->getConnection()->beginTransaction();
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            
            $_POST["nameOnCard"] = filter_var($_POST['nameOnCard'], FILTER_SANITIZE_STRING);
            $_POST["cardNumber"] = filter_var($_POST['cardNumber'], FILTER_SANITIZE_STRING);
            $_POST["cardExpiryMonth"] = filter_var($_POST['cardExpiryMonth'], FILTER_SANITIZE_STRING);
            $_POST["cardExpiryYear"] = filter_var($_POST['cardExpiryYear'], FILTER_SANITIZE_STRING);
            $_POST["ccvNumber"] = filter_var($_POST['ccvNumber'], FILTER_SANITIZE_STRING);
            
            
            $items = array();
            for($i=0; $i<count($_POST['description']); $i++){
                if(trim($_POST['description'][$i]) != ""){
                    $items[] = array(
                        'description'=> $_POST['description'][$i],
                        'price'=> $_POST['price'][$i],
                        'quantity'=> $_POST['quantity'][$i],
                        'gst'=> $_POST['gst'][$i]
                    );
                }
            }
            
            $post = array(
                'order_no'=>$_POST['order_no'],
                'items'=> $items,
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'company'=>$_POST['company'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            $paymentDetails = array(
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails']
            );
            
            $billingDetails = array(
                'company'=>$_POST['company'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry']
            );
            $cardDetails = array(
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            $mp = new ManualPayment();
            $mp->setOrderNo($_POST['order_no']);
            $mp->setCustName($_POST['fname'] . " " . $_POST['lname']);
            $mp->setCustEmail($_POST['email']);
            $mp->setPaymentDetails(json_encode($paymentDetails));
            $mp->setBillingDetails(json_encode($billingDetails));
            $mp->setCardDetails(json_encode($cardDetails));
            $mp->setItems(json_encode($items));
            $mp->setGrandTotal($_POST['grand_total']);
            $em->persist($mp);
            $em->flush();
            
            if($this->sOrderNoExists($_POST['order_no'])){
                $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST["order_no"]));
                $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
                $model->setFname($_POST["fname"]);
                $model->setLname($_POST["lname"]);
                $model->setEmail($_POST["email"]);
                $model->setPhone($_POST["phone"]);
                $model->setMobile($_POST["mobile"]);
                $model->setAddress($_POST["address"]);
                $model->setCity($_POST["city"]);
                $model->setState($_POST["state"]);
                $model->setPostcode($_POST["postcode"]);
                $model->setCountryId($_POST["country"]);
                $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
                $model->setMbaAddress($_POST["billingAddress"]);
                $model->setMbaCity($_POST["billingCity"]);
                $model->setMbaState($_POST["billingState"]);
                $model->setMbaPostcode($_POST["billingPostcode"]);
                $model->setMbaCountryId($_POST["billingCountry"]);
                $model->setPaymentOption(1);
                $model->setTotalOrderPrice($_POST['grand_total']);
                $model->setSPaid(1);
                $em->persist($model);
                $em->flush();
            }
            
            
            /**
            * Credit card payment
            * Position: Start
            */
           $credit_card_error = 0;
               
            $totalAmount = $_POST['grand_total'] * 100;

            $fields = array(
                'customerFirstName' => urlencode($_POST['fname']),
                'customerLastName' => urlencode($_POST['lname']),
                'customerEmail' => urlencode($_POST['email']),
                'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $this->getCountryNameById($_POST["country"])),
                'customerPostcode' => urlencode($_POST['postcode']),
                'customerInvoiceDescription' => urlencode('CLS Manual Payment'),
                'customerInvoiceRef' => urlencode($_POST['order_no']),
                'cardHoldersName' => urlencode($_POST['nameOnCard']),
                'cardNumber' => urlencode($_POST['cardNumber']),
                'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                'trxnNumber' => urlencode('4230'),
                'totalAmount' => urlencode($totalAmount),
                'cvn' => urlencode($_POST['ccvNumber'])
            );



            //url-ify the data for the POST
            $fields_string = '';
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string, '&');


           $root_dir = dirname($this->get('kernel')->getRootDir());
            //open connection
            $ch = curl_init();
            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
            //execute post
            $credit_card_reponse = curl_exec($ch);

            $cr_result = json_decode($credit_card_reponse, true);


            if(isset($cr_result['error'])){
                $credit_card_error += 1;
            }


            //close connection
            curl_close($ch);
            
            /**
            * Credit card payment
            * Position: End
            */

            if($credit_card_error > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                $errors = array();

                $errors[] = array('message'=>$cr_result['error']);
                
                $this->get('session')->getFlashBag()->add(
                            'error',
                            $errors
                        );


                return $this->render('AcmeCLSadminBundle:ProcessPayment:index.html.twig',
                        array(
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                            'post'=> $post
                            )
                    );
            }else{
                
                
                
                // email invoice
                $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                $recipients = array(
                  'to'=> $_POST['email'],
                  'cc'=> $email_copies
                );
                
                $this->sendEmailWithCc($recipients, "help@capitallinkservices.com.au", "Payment Receipt", 
                        $this->renderView('AcmeCLSadminBundle:ProcessPayment:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'post'=>$post,
                                    'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                                    )
                                )
                        );
                
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Payment has been processed.'
                        );

                // commit changes
                $em->getConnection()->commit();
                
                return $this->redirect($this->generateUrl('acme_cls_admin_process_payment'));
                
                    
            }
        }else{
            return $this->render('AcmeCLSadminBundle:ProcessPayment:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'departments'=> $this->getDepartments(),
                        'cardtypes'=> $this->getCardTypes(),
                        'credit_card_processing_fee' => $this->getCreditCardProcessingFee()
                    )
                );
        }
    }

}
