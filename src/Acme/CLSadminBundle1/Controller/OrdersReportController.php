<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\SettingsTPN;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class OrdersReportController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function visaAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-visa');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:visa.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    public function visaListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, d.name as department, p.account_no, o.order_no, o.status, c.country_name,
                o.date_completed, o.primary_traveller_name,  DATE_FORMAT(o.departure_date,'%d-%m-%Y') as departure_date, p.s_paid, CONCAT(CONCAT(au.fname, ' '), au.lname) as team_mem,
                o.order_no
            FROM tbl_orders o 
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = o.destination
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND (o.order_type = 1 OR o.order_type = 3)
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","status","primary_traveller_name", "departure_date", "s_paid", "team_mem", "order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    public function publicVisaAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-public-visa');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:publicVisa.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    public function publicVisaListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, d.name as department, p.account_no, o.order_no, o.status, c.country_name,
                o.date_completed, o.primary_traveller_name,  DATE_FORMAT(o.departure_date,'%d-%m-%Y') as departure_date, p.s_paid, CONCAT(CONCAT(au.fname, ' '), au.lname) as team_mem,
                o.order_no
            FROM tbl_orders o 
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = o.destination
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 6
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","status","primary_traveller_name", "departure_date", "s_paid", "team_mem", "order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    
    
    public function tpnAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-tpn');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:tpn.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    public function tpnListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, d.name as department, p.account_no, o.order_no, o.status, c.country_name,
                o.date_completed, o.primary_traveller_name,  o.departure_date, p.s_paid, CONCAT(CONCAT(au.fname, ' '), au.lname) as team_mem,
                o.order_no, (select count(od.id) from tbl_order_destinations od where od.order_no = o.order_no) as tpn_count,
                (select count(ot.id) from tbl_order_travellers ot where ot.order_no = o.order_no) as traveller_count,
                o.grand_total
            FROM tbl_orders o 
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = o.destination
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND (o.order_type = 2 OR o.order_type = 3)
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","status","tpn_count", "traveller_count", "grand_total", "team_mem", "order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    public function passportAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-passport');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:passport.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    public function passportListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        if(isset($_GET['ddfrom'])){
            if(trim($_GET['ddfrom']) != ''){
                $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
                $_GET['ddfrom'] = trim($_GET['ddfrom']);
                $condition .= ' AND o.passport_office_booking_time >= "'. $mod->changeFormatToOriginal($_GET['ddfrom']).'"';
            }
        }

        if(isset($_GET['ddto'])){

            if(trim($_GET['ddto']) != ''){
                $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
                $_GET['ddto'] = trim($_GET['ddto']);
                $condition .= ' AND o.passport_office_booking_time <= "'.$mod->changeFormatToOriginal($_GET['ddto']).'"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no,  
                (Select group_concat(opa.fullname) as applicants from tbl_order_passport_applicants opa where opa.order_no=o.order_no) as applicants,
                o.passport_office_booking_no, p.s_paid, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem,
                CONCAT(CONCAT(CONCAT(CONCAT(DATE_FORMAT(o.passport_office_booking_time,'%d-%m-%Y'), ' '), o.passport_office_booking_time_hr), ':'), o.passport_office_booking_time_min) as booking_date
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 4
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","account_no","order_no","booking_date","applicants","passport_office_booking_no","s_paid","cls_team_mem","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    public function policeClearanceAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-police-clearance');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:policeClearance.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function policeClearanceListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no, o.order_type,
                (Select group_concat(concat(concat(opa.fname,' '), opa.lname))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicants,
                (Select group_concat(opa.passport_no)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as passports,
                (Select group_concat(DATE_FORMAT(opa.departure_date,'%d-%m-%Y'))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as departure_dates,
                 o.status, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 5
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","account_no","order_no","applicants","passports","departure_dates","status","cls_team_mem","order_type");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    public function docDeliveryAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-doc-delivery');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:docDelivery.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    
    public function docDeliveryListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no, CONCAT(CONCAT(p.fname, ' '), p.lname) as customer,
                o.doc_package_total_pieces, DATE_FORMAT(o.doc_package_pickup_date,'%d-%m-%Y') as doc_package_pickup_date, CONCAT(CONCAT(o.doc_package_ready_hr,':'), o.doc_package_ready_min) as pickup_time,
                CONCAT(CONCAT(o.doc_package_office_close_hr,':'), o.doc_package_office_close_min) as office_close_time, 
                dd.type as courier_type, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_settings_document_delivery dd ON dd.id = o.doc_delivery_type
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 7
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","account_no","order_no","customer","doc_package_total_pieces","doc_package_pickup_date",
            "pickup_time","office_close_time","courier_type","cls_team_mem","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }

    
    
    
    public function allVisaAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-all-visa');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:allVisa.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function allVisaListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        if(isset($_GET['ddfrom'])){
            if(trim($_GET['ddfrom']) != ''){
                $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
                $_GET['ddfrom'] = trim($_GET['ddfrom']);
                $condition .= ' AND od.departure_date >= "'. $mod->changeFormatToOriginal($_GET['ddfrom']).'"';
            }
        }

        if(isset($_GET['ddto'])){

            if(trim($_GET['ddto']) != ''){
                $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
                $_GET['ddto'] = trim($_GET['ddto']);
                $condition .= ' AND od.departure_date <= "'.$mod->changeFormatToOriginal($_GET['ddto']).'"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        if(isset($_GET['destination'])){

            if(trim($_GET['destination']) != ''){
                $_GET['destination'] = filter_var($_GET['destination'], FILTER_SANITIZE_STRING);
                $_GET['destination'] = trim($_GET['destination']);
                $_GET['destination'] = intval($_GET['destination']);
                $condition .= ' AND od.country_id = '.$_GET['destination'];
            }
        }
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted as date_submitted, d.name as department, p.account_no, o.order_no, o.status, c.country_name,
                o.date_completed, o.primary_traveller_name, vt.type as visa_type, DATE_FORMAT(od.departure_date,'%d-%m-%Y') as departure_date, p.s_paid, CONCAT(CONCAT(au.fname, ' '), au.lname) as team_mem,
                od.id
            FROM tbl_order_destinations od
            LEFT JOIN tbl_orders o ON o.order_no = od.order_no
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = od.country_id
            LEFT JOIN tbl_visa_types vt ON vt.id = od.selected_visa_type
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND (o.order_type = 1 OR o.order_type = 3 OR o.order_type = 6)
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","status","country_name","date_completed","primary_traveller_name","visa_type", "departure_date", "s_paid", "team_mem", "id");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    public function manifestAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-manifest');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:manifest.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function manifestList1Action()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND od.visa_date_completed_and_received_at_cls >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND od.visa_date_completed_and_received_at_cls <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        // CONCAT(CONCAT(o.pri_dept_contact_fname, ' '), pri_dept_contact_lname) as client_name, 
        $statement = $connection->prepare("
            SELECT CONCAT(CONCAT(uc.fname, ' '), uc.lname) as client_name, 
                DATE_FORMAT(o.date_submitted,'%d-%m-%Y') as date_submitted, 
                d.name as department, 
                p.account_no, 
                o.order_no, 
                o.status, 
                c.country_name,
                DATE_FORMAT(o.date_completed,'%d-%m-%Y') as date_completed, 
                (SELECT GROUP_CONCAT(CONCAT(opa.fname,' ',opa.lname))  FROM tbl_order_travellers opa WHERE opa.order_no=o.order_no) AS primary_traveller_name,
                (SELECT GROUP_CONCAT(opa.passport_number) FROM tbl_order_travellers opa WHERE opa.order_no=o.order_no) AS primary_traveller_passport_no,
                vt.type as visa_type, od.departure_date, p.s_paid, CONCAT(CONCAT(au.fname, ' '), au.lname) as team_mem,
                od.id, DATE_FORMAT(od.visa_date_completed_and_received_at_cls,'%d-%m-%Y') as visa_date_completed_and_received_at_cls
            FROM tbl_order_destinations od
            LEFT JOIN tbl_orders o ON o.order_no = od.order_no
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = od.country_id
            LEFT JOIN tbl_visa_types vt ON vt.id = od.selected_visa_type
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            LEFT JOIN tbl_user_client uc ON uc.id = o.client_id
            WHERE o.status >= 10 AND (o.order_type = 1 OR o.order_type = 3 OR o.order_type = 6)
                AND od.visa_date_completed_and_received_at_cls != ''
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("client_name","date_submitted","visa_date_completed_and_received_at_cls","primary_traveller_name",
            "primary_traveller_passport_no","country_name","account_no","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    
    public function manifestList2Action()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_doc_sent >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_doc_sent <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        // CONCAT(CONCAT(p.fname, ' '), p.lname) as client_name, 
        $statement = $connection->prepare("
           SELECT CONCAT(CONCAT(uc.fname, ' '), uc.lname) as client_name, 
                DATE_FORMAT(o.date_submitted,'%d-%m-%Y') as date_submitted, 
                DATE_FORMAT(o.date_doc_sent,'%d-%m-%Y') as date_doc_sent,
                DATE_FORMAT(o.date_completed,'%d-%m-%Y') as date_completed,
                CAST((SELECT IF(GROUP_CONCAT(opa.fullname) IS NULL,'',GROUP_CONCAT(opa.fullname SEPARATOR ',') ) AS applicants FROM tbl_order_passport_applicants opa WHERE opa.order_no=o.order_no) AS CHAR(10000) CHARACTER SET utf8) AS applicants,
                (Select 
                    group_concat(opa.personal_passport,opa.diplomatic_official_passport,opa.birth_certificate, opa.marriage_certificate, opa.certificate_of_australian_citizenship)
                    from tbl_order_passport_applicants opa 
                    where opa.order_no=o.order_no
                ) as doc_types,
                p.account_no, o.order_no
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            LEFT JOIN tbl_user_client uc ON uc.id = o.client_id
            WHERE o.status >= 10 AND o.order_type = 4
                AND o.s_doc_sent = 1 
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("client_name","date_submitted","date_doc_sent","date_completed",
            "applicants","doc_types","account_no","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    public function policeClearanceManifestAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-police-clearance-manifest');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:police_clearance_manifest.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers(),
                    'clearances' => $this->getPoliceClearances()
                )
            );
    }
    
    public function policeClearanceManifestListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        if(isset($_GET['clearance'])){
            
            if(trim($_GET['clearance']) != ''){
                $_GET['clearance'] = filter_var($_GET['clearance'], FILTER_SANITIZE_STRING);
                $_GET['clearance'] = trim($_GET['clearance']);
                $_GET['clearance'] = intval($_GET['clearance']);
                $condition .= ' AND o.police_clearance_id = '.$_GET['clearance'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no, o.order_type,
                (Select group_concat(opa.fname)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicant_fname,
                (Select group_concat(opa.lname)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicant_lname,
                (Select group_concat(opa.passport_no)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as passports,
                (Select group_concat(DATE_FORMAT(opa.departure_date,'%d-%m-%Y'))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as departure_dates,
                 o.status, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem,
                 '460717338' as reference_out_to_sa, '8702120054088' as clearance_no
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 5 
                AND (o.police_clearance_date_cls_received_all_items != '0000-00-00' AND o.police_clearance_date_cls_received_all_items != '' AND o.police_clearance_date_cls_received_all_items IS NOT NULL )
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","order_no","applicant_fname","applicant_lname","passports","reference_out_to_sa","clearance_no","cls_team_mem","order_type");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    
    
    public function embassyDeliveryAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-embassy-delivery');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:embassyDelivery.html.twig',
                array('get'=> $_GET
                )
            );
    }
    
    public function embassyDeliveryListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND od.visa_date_submitted_for_processing >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND od.visa_date_submitted_for_processing <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT 
                COUNT(od.id) as items,
                od.country_id,
                c.country_name, 
                od.visa_date_submitted_for_processing ,
                '' as receiver_name,
                '' as receiver_signature,
                '' as notes
            FROM tbl_order_destinations od
            LEFT JOIN tbl_countries c ON c.id = od.country_id
            WHERE od.visa_date_submitted_for_processing != ''
            " . $condition . "
            GROUP BY od.country_id
            ");
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("items","country_name","receiver_name","receiver_signature","notes");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    
    public function russianVisaVoucherAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-russian-visa-voucher');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:russianVisaVoucher.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function russianVisaVoucherListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no, o.primary_traveller_name as applicant,
                rvv.name as russian_visa_voucher_name, o.grand_total, o.status
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_russian_visa_voucher_types rvv ON rvv.id = o.russian_visa_voucher_id
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 8
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","account_no","order_no","applicant","russian_visa_voucher_name","status","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    public function documentLegalisationAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'orders-report-document-legalisation');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:OrdersReport:docLegalisation.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    public function documentLegalisationListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['orderstat'])){

            if(trim($_GET['orderstat']) != ''){
                $_GET['orderstat'] = filter_var($_GET['orderstat'], FILTER_SANITIZE_STRING);
                $_GET['orderstat'] = trim($_GET['orderstat']);
                $_GET['orderstat'] = intval($_GET['orderstat']);
                $condition .= ' AND o.status = '.$_GET['orderstat'];
            }
        }
        
//        if(isset($_GET['onaccount'])){
//
//            if(trim($_GET['onaccount']) != ''){
//                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
//                $_GET['onaccount'] = trim($_GET['onaccount']);
//                $_GET['onaccount'] = intval($_GET['onaccount']);
//                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
//            }
//        }
//        
//        if(isset($_GET['accountno'])){
//
//            if(trim($_GET['accountno']) != ''){
//                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
//                $_GET['accountno'] = trim($_GET['accountno']);
//                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
//            }
//        }

//        if(isset($_GET['clsteammem'])){
//
//            if(trim($_GET['clsteammem']) != ''){
//                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
//                $_GET['clsteammem'] = trim($_GET['clsteammem']);
//                $_GET['clsteammem'] = intval($_GET['clsteammem']);
//                $condition .= ' AND au.id = '.$_GET['clsteammem'];
//            }
//        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, o.order_no, CONCAT(CONCAT(uc.fname, ' '), uc.lname) as applicant, o.status,
                p.s_paid
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_client uc ON uc.id = o.client_id
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 9
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","order_no","applicant","s_paid","status","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
}
