<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSadminBundle\Entity\EmbassyUser;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class EmbassyController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-embassy');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:Embassy:index.html.twig');
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:EmbassyUser');
            $query = $cust->createQueryBuilder('u')
                ->select("u.id, u.fname, u.lname, u.email, u.status, u.id")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("fname","lname","email","status","id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    
    /**
     * new staff
     * @return type
     */
    public function newAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-embassy');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = new EmbassyUser();
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPassword($mod->passGenerator($_POST['password']));
            $model->setEmail($_POST['email']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            $model->setCountry($_POST['country']);
            $model->setNotes($_POST['notes']);
            $model->setStatus(1);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'country'=>$_POST['country'],
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'password'=>$_POST['password']
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['passwordConfirm']) || $this->sEmailAvailable($_POST['email']) == false){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['passwordConfirm']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
                
                if($this->sEmailAvailable($_POST['email']) == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['fname'] . ' ' . $_POST['lname'] . ' has been added.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_embassy'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:Embassy:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles()
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSadminBundle:Embassy:new.html.twig',
                array(
                    'countries'=> $this->getCountries(),
                    'name_titles'=> $this->getNameTitles()
                    )
                );
        }
    }
    
    /**
     * edit staff
     * @return type
     */
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-embassy');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
            
            
            $model = $em->getRepository('AcmeCLSadminBundle:EmbassyUser')->findOneBy(array('id'=>$_POST["id"]));
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setEmail($_POST['email']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            $model->setCountry($_POST['country']);
            $model->setNotes($_POST['notes']);
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'country'=>$_POST['country'],
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile']
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || $this->sEmailAvailableInDetailUpdate($_POST['email'], $_POST["id"], 'embassy-user') == false ||
                        (($_POST['password'] != '') && ($_POST['password'] != $_POST['passwordConfirm']))){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }
                
                if($this->sEmailAvailableInDetailUpdate($_POST['email'], $_POST["id"], 'embassy-user') == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
                
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $errors[] = array('message'=>'Password must be repeated exactly.');
                        $error_count += 1;
                    }
                }
                
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['fname'] . ' ' . $_POST['lname'] . ' details have been updated.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_embassy'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:Embassy:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles()
                        ));
            }
        }else{
            
            $em = $this->getDoctrine()->getManager(); 
            
            $_GET['id'] = filter_var($_GET['id'], FILTER_SANITIZE_STRING);

            $model = $em->getRepository('AcmeCLSadminBundle:EmbassyUser')->findOneBy(array('id'=>$_GET["id"]));
            
            return $this->render('AcmeCLSadminBundle:Embassy:edit.html.twig',
                array('post' => $model,
                    'countries'=> $this->getCountries(),
                    'name_titles'=> $this->getNameTitles()
                    )
                );
        }
    }
    
    
    public function deleteAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['client_id'])){
            return new Response("invalid id");
        }
        
        $_POST['client_id'] = filter_var($_POST['client_id'], FILTER_SANITIZE_STRING);
        $_POST['client_id'] = intval($_POST['client_id']);
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $statement = $connection->prepare("DELETE FROM tbl_user_embassy
            WHERE id=".$_POST['client_id']);
        $statement->execute();
        
        
        return new Response("success");
        
    }

}
