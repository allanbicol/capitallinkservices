<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSadminBundle\Entity\RussianVisaVoucher;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageRussianVisaVoucherController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-russian-visa-voucher');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:ManageRussianVisaVoucher:index.html.twig',
                array('voucher_types'=>$this->getRussianVisaVoucherTypes())
                );
    }
    
    
    public function saveInvitationTypeAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        
        if(isset($_POST)){
            $em->getConnection()->beginTransaction();
            
            if(isset($_POST['removeTypes']) && trim($_POST['removeTypes']) != ''){
                $_POST['removeTypes'] = filter_var($_POST['removeTypes'], FILTER_SANITIZE_STRING);
                $remove_type_ids = explode(",", $_POST['removeTypes']);
                for($e=0; $e<count($remove_type_ids); $e++){
                    $statement = $connection->prepare("DELETE FROM tbl_russian_visa_voucher_types WHERE type='invitation' AND id=".$remove_type_ids[$e]);
                    $statement->execute();
                }
            }
            
            $order = 0;
            foreach($_POST['inv_description'] as $key=>$value){
                if(is_numeric($key)){
                    $model = $em->getRepository('AcmeCLSadminBundle:RussianVisaVoucher')->findOneBy(array('id'=>$key, 'type'=>'invitation'));
                }else{
                    $model = new RussianVisaVoucher();
                }
                $model->setType('invitation');
                $model->setName($_POST['inv_description'][$key]);
                $model->setThreeDaysProcessFee($_POST['inv_three_days_fee'][$key]);
                $model->setOneTwoDaysProcessFee($_POST['inv_one_two_days_fee'][$key]);
                $model->setTwelveHrsProcessFee($_POST['inv_twelve_hrs_fee'][$key]);
                $model->setTypeOrder('a'.$order);
                if(isset($_POST['inv_s_active'][$key])){
                    $model->setSActive(1);
                }else{
                    $model->setSActive(0);
                }
                
                $em->persist($model);
                $em->flush();
                $order += 1;
            }
            // commit changes
            $em->getConnection()->commit(); 
        }
        
        $this->get('session')->getFlashBag()->add(
            'success',
            'Russian Visa Voucher has been updated'
        );
        
        return $this->redirect($this->generateUrl('acme_cls_admin_manage_russian_visa_voucher')); 
    }
    
    
    
    public function saveBusinessTypeAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        
        if(isset($_POST)){
            $em->getConnection()->beginTransaction();
            
            if(isset($_POST['removeTypes']) && trim($_POST['removeTypes']) != ''){
                $_POST['removeTypes'] = filter_var($_POST['removeTypes'], FILTER_SANITIZE_STRING);
                $remove_type_ids = explode(",", $_POST['removeTypes']);
                for($e=0; $e<count($remove_type_ids); $e++){
                    $statement = $connection->prepare("DELETE FROM tbl_russian_visa_voucher_types WHERE type='business' AND id=".$remove_type_ids[$e]);
                    $statement->execute();
                }
            }
            
            $order = 0;
            foreach($_POST['bus_description'] as $key=>$value){
                if(is_numeric($key)){
                    $model = $em->getRepository('AcmeCLSadminBundle:RussianVisaVoucher')->findOneBy(array('id'=>$key, 'type'=> 'business'));
                }else{
                    $model = new RussianVisaVoucher();
                }
                $model->setType('business');
                $model->setName($_POST['bus_description'][$key]);
                $model->setThirteenDays($_POST['bus_thirteen_days_fee'][$key]);
                $model->setFourDays($_POST['bus_four_days_fee'][$key]);
                if(isset($_POST['bus_s_active'][$key])){
                    $model->setSActive(1);
                }else{
                    $model->setSActive(0);
                }
                $model->setTypeOrder('b'.$order);
                $em->persist($model);
                $em->flush();
                $order += 1;
            }
            // commit changes
            $em->getConnection()->commit(); 
        }
        
        $this->get('session')->getFlashBag()->add(
            'success',
            'Russian Visa Voucher has been updated'
        );
        
        return $this->redirect($this->generateUrl('acme_cls_admin_manage_russian_visa_voucher')); 
    }

}
