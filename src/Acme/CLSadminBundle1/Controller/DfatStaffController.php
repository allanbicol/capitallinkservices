<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLStpnBundle\Entity\TpnUser;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class DfatStaffController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-staff');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:DfatStaff:index.html.twig');
    }
    
    
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
            $query = $cust->createQueryBuilder('p')
                //->where('p.id = :id')
                //->setParameter('id', $id)
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("fname","lname","email","date_last_login","s_enabled","id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    
    
    /**
     * new staff
     * @return type
     */
    public function newAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-staff');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = new TpnUser();
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPassword($mod->passGenerator($_POST['password']));
            $model->setEmail($_POST['email']);
            $model->setSEnabled(1);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'password'=>$_POST['password']
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['passwordConfirm']) || $this->sEmailAvailable($_POST['email']) == false){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['passwordConfirm']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
                if($this->sEmailAvailable($_POST['email']) == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                
                $this->sendEmail($_POST['email'], "help@capitallinkservices.com.au", "CLS DFAT - Registration", 
                        $this->renderView('AcmeCLSadminBundle:DfatStaff:new_staff_email_template.html.twig',
                                array('post'=>$_POST,
                                    'site_url'=>$mod->siteURL()
                                )
                            )
                        );
                        
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['fname'] . ' ' . $_POST['lname'] . ' has been added.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_tpn_staff'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:DfatStaff:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSadminBundle:DfatStaff:new.html.twig',
                array(
                    )
                );
        }
    }
    
    
    
    /**
     * edit staff
     * @return type
     */
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-staff');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
            
            $model = $em->getRepository('AcmeCLStpnBundle:TpnUser')->findOneBy(array('id'=>$_POST["id"]));
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setEmail($_POST['email']);
            $model->setSEnabled(1);
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'id'=>$_POST['id'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email']
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || $this->sEmailAvailableInDetailUpdate($_POST['email'], $_POST["id"], 'tpn-user') == false ||
                        (($_POST['password'] != '') && ($_POST['password'] != $_POST['passwordConfirm']))){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($this->sEmailAvailableInDetailUpdate($_POST['email'], $_POST["id"], 'tpn-user') == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
                
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $errors[] = array('message'=>'Password must be repeated exactly.');
                        $error_count += 1;
                    }
                }
                
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['fname'] . ' ' . $_POST['lname'] . ' details have been updated.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_tpn_staff'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:DfatStaff:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            $em = $this->getDoctrine()->getManager(); 
            
            $_GET['id'] = filter_var($_GET['id'], FILTER_SANITIZE_STRING);

            $model = $em->getRepository('AcmeCLStpnBundle:TpnUser')->findOneBy(array('id'=>$_GET["id"]));
            
            return $this->render('AcmeCLSadminBundle:DfatStaff:edit.html.twig',
                array('post' => $model
                    )
                );
        }
    }
    
    
    
    /**
     * delete user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired!");
        }
        
        $em = $this->getDoctrine()->getEntityManager();

        $_POST['tpn_user_id'] = filter_var($_POST['tpn_user_id'], FILTER_SANITIZE_STRING);
        
        $model = $em->getRepository('AcmeCLStpnBundle:TpnUser')->findOneBy(array('id'=>$_POST["tpn_user_id"]));
            
        $this->get('session')->getFlashBag()->add(
                'success',
                $model->getFname() . ' '. $model->getLname() . ' has been deleted.'
            );
        
        $connection = $em->getConnection();
        $statement = $connection->prepare("DELETE FROM tbl_user_tpn WHERE id=" .$_POST["tpn_user_id"]);
        $statement->execute();
        
        return new Response("success");
    }
}
