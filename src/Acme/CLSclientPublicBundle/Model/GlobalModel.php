<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GlobalModel
 *
 * @author leokarl
 */
namespace Acme\CLSclientPublicBundle\Model;

class GlobalModel {
    //put your code here
    public function setActivity($message, $supplier_name, $quote_packet_name, $price, $date_time){
        // replace [supplier_name]
        $message = str_replace('[supplier_name]', $supplier_name, $message);
        // replace [quote_packet_name]
        $message = str_replace('[quote_packet_name]', $quote_packet_name, $message);
        // replace [item_price]
        $message = str_replace('[item_price]', '$'.$price, $message);
        // replace [item_price]
        $date_time = new \DateTime($date_time);
        $message = str_replace('[activity_date_time]', date_format($date_time, 'g:ia \-\- F m, Y'), $message);
        return $message;
    }
    
    /**
     * @todo send email via php mail
     * @author leokarl
     * @param type $recipient
     * @param type $subject
     * @param type $body
     */
    public function sendEmail($recipients, $from, $subject, $body){
        $yourWebsite = "APHA"; // the name of your website
        
        if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
            $headers = "From: $from";
            $headers.= "Reply-To: <$recipients>";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        } else {
            $headers = "From: $yourWebsite $from\n";
            $headers.= "Reply-To: <$recipients>";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        }


        mail($recipients,$subject,$body,$headers);
    }
    
    /**
     * @todo check request number
     * @author leokarl
     * @param type $request_no
     * @return boolean
     */
    public function isRequestNoValid($request_no){
        if (strpos($request_no,' ') !== false) {
            return FALSE;
        }else{
            return TRUE;
        }
    }
    
    /**
     * @todo create directory
     * @author leokarl
     * @param type $container_path
     * @param type $propose_dir_name
     */
    public function createDirectory($container_path, $propose_dir_name){
        $dir = $container_path.$propose_dir_name;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
    
    /**
     * @todo generateJsonFile
     * @author leokarl
     * @param type $file
     * @param type $data
     */
    public function generateJsonFile($file, $data){
        $fp = fopen($file, 'w');
        fwrite($fp, $data);
        fclose($fp);
    }
    /**
    * @todo password generator
    * @author leokarl
    * @param string $password
    */
    
   public function passGenerator($password){
           return md5(md5($password));
   }
   
   /**
    * @todo disable special characters
    * @param unknown_type $mystring
    */
   public function isSpecialCharPresent($mystring){
           if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '#') === false
                           && strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false
                           && strpos($mystring, '*') === false && strpos($mystring, '_') === false
                           && strpos($mystring, '=') === false && strpos($mystring, '`') === false
                           && strpos($mystring, '"') === false
                           && strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
                           && strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){

                   return FALSE; // special character not present
           }else{
                   return TRUE; // special character present
           }
   }
   
   public function isPhoneNumber($number){
       if( !preg_match('/^(NA|[ 0-9+-]+)$/', $number) ) { 
            return FALSE;
        }else{
            return TRUE;
        }
   }
   /**
    * @todo email validator
    * @author leokarl
    * @param string $email
    * @return boolean
    */
   public function isEmailValid($email){
           if(!preg_match('/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i', strtolower($email))){
                   return FALSE;
           }else{
                   return TRUE;
           }
   }
   public function checkDate($mydate) {
        if(substr_count($mydate, '-') == 2){
                list($yy,$mm,$dd)=explode("-",$mydate);
                if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
                        return checkdate($mm,$dd,$yy);
                }else{
                        return false;
                }
        }else{
                return false;
        }
    }
   public function checkDateTime($mydatetime) {
        if(substr_count($mydatetime, '-') == 2){
                list($date, $time) = explode(" ",$mydatetime);
                list($yy,$mm,$dd)=explode("-",$date);
                list($hh,$mn,$sc)=explode(":",$time);

                if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
                    return checkdate($mm,$dd,$yy);
                }else{
                    return false;
                }

                if (!is_numeric($hh) && !is_numeric($mn) && !is_numeric($sc)){
                    return false;
                }
        }else{
            return false;
        }
    }
    
    public function isPetTypeValid($type){
        if($type == 'dog' || $type == 'cat' || $type== 'other'){
            return true;
        }else{
            return false;
        }
    }
    
    public function isReportTypeValid($type){
        if(is_numeric($type)){
            if($type == 1 || $type== 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public function isPetGenderValid($gender){
        if($gender == 'male' || $gender == 'female' || $gender == 'unknown'){
            return true;
        }else{
            return false;
        }
    }
    
    public function createImageThumb($filename,$modheight,$orig_dir,$copy_dir){
        $file= $orig_dir."/".$filename;
        $path_parts = pathinfo($file);
        
        
        // This sets it to a .jpg, but you can change this to png or gif 
        if(strtolower($path_parts['extension'])== 'jpeg' || strtolower($path_parts['extension'])== 'jpg'){
            header('Content-type: image/jpeg'); 
        }else if(strtolower($path_parts['extension'])== 'png'){
            header('Content-type: image/png'); 
        }else if(strtolower($path_parts['extension'])== 'gif'){
            header('Content-type: image/gif'); 
        }
        // Setting the resize parameters
        list($width, $height) = getimagesize($file); 
        //$modwidth = 146; 
        $modwidth = $modheight / ($height / $width);
        
        // Creating the Canvas 
        $tn= imagecreatetruecolor($modwidth, $modheight); 
        if(strtolower($path_parts['extension'])== 'jpeg' || strtolower($path_parts['extension'])== 'jpg'){
            $source = imagecreatefromjpeg($file); 
        }else if(strtolower($path_parts['extension'])== 'png'){
            $source = imagecreatefrompng($file); 
        }else if(strtolower($path_parts['extension'])== 'gif'){
            $source = imagecreatefromgif($file);
        }

        // Resizing our image to fit the canvas 
        //imagecopyresized($tn, $source, 0, 0, 0, 0, $modwidth, $modheight, $width, $height); 
        imagecopyresampled($tn, $source, 0, 0, 0, 0, $modwidth, $modheight, $width, $height); 


        // Outputs a jpg image, you could change this to gif or png if needed 
        if($path_parts['extension']== 'jpeg'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename'].".jpeg");
        }else if($path_parts['extension']== 'JPEG'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename'].".JPEG");
        }else if($path_parts['extension']== 'jpg'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename'].".jpg");
        }else if($path_parts['extension']== 'JPG'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename'].".JPG");
        }else if($path_parts['extension']== 'png'){
            imagepng($tn,$copy_dir."/".$path_parts['filename'].".png");
        }else if($path_parts['extension']== 'PNG'){
            imagepng($tn,$copy_dir."/".$path_parts['filename'].".PNG");
        }else if($path_parts['extension']== 'gif'){
            imagegif($tn,$copy_dir."/".$path_parts['filename'].".gif");
        }else if($path_parts['extension']== 'GIF'){
            imagegif($tn,$copy_dir."/".$path_parts['filename'].".GIF");
        }
    }
    
    public function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            return false;
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
    
    
}

?>
