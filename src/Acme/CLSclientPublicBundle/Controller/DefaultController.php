<?php

namespace Acme\CLSclientPublicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientPublicBundle\Model;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        echo $mod->passGenerator("leokarl");
        exit();
        return $this->render('AcmeCLSclientPublicBundle:Default:index.html.twig');
    }
}
