<?php

namespace Acme\CLStpnBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TpnUser
 *
 * @ORM\Table(name="tbl_user_tpn")
 * @ORM\Entity
 * @UniqueEntity(
 *      fields={"email"},
 *      message="Email is already used"
 * )
 */
class TpnUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fname", type="string", length=225)
     */
    private $fname;

    /**
     * @var string
     *
     * @ORM\Column(name="lname", type="string", length=225)
     */
    private $lname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="reset_pin", type="string", length=10)
     */
    private $reset_pin;


    /**
     * @ORM\Column(name="s_enabled", type="integer", length=1)
     */
    private $s_enabled;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_last_login", type="string", length=50)
     */
    private $date_last_login;
   

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return TpnUser
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return TpnUser
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return TpnUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return TpnUser
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return TpnUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set reset_pin
     *
     * @param string $resetPin
     * @return TpnUser
     */
    public function setResetPin($resetPin)
    {
        $this->reset_pin = $resetPin;
    
        return $this;
    }

    /**
     * Get reset_pin
     *
     * @return string 
     */
    public function getResetPin()
    {
        return $this->reset_pin;
    }

    /**
     * Set s_enabled
     *
     * @param integer $sEnabled
     * @return TpnUser
     */
    public function setSEnabled($sEnabled)
    {
        $this->s_enabled = $sEnabled;
    
        return $this;
    }

    /**
     * Get s_enabled
     *
     * @return integer 
     */
    public function getSEnabled()
    {
        return $this->s_enabled;
    }

    /**
     * Set date_last_login
     *
     * @param string $dateLastLogin
     * @return TpnUser
     */
    public function setDateLastLogin($dateLastLogin)
    {
        $this->date_last_login = $dateLastLogin;
    
        return $this;
    }

    /**
     * Get date_last_login
     *
     * @return string 
     */
    public function getDateLastLogin()
    {
        return $this->date_last_login;
    }
}