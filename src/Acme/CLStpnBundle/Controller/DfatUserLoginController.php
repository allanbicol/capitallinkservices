<?php

namespace Acme\CLStpnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class DfatUserLoginController extends Controller
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-user-login');
        
        $mod = new Model\GlobalModel();
        $error = 0;
        $message = array();
        
        if($session->get('tpn_user_email') != ''){
            return $this->redirect($this->generateUrl('acme_cls_tpn_home'));
        }
        
        if(isset($_POST['hid_submit'])){
            $error =0;
            // Check username and password
            if(!isset($_POST['email']) && !isset($_POST['password'])){
                return $this->redirect($this->generateUrl('acme_cls_tpn_login'));
            }
            
            if(trim($_POST['email']) == ''){
                $error += 1;
                $message = "Email address should not be blank.";
            }
            
            // check email validity
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $error == 0){
                $error += 1;
                $message = "Email ".$_POST['email']." is not a valid email address.";
            }
            
            if(trim($_POST['password']) == '' && $error == 0){
                $error += 1;
                $message = "Password should not be blank.";
            }
            
            // check user in database
            $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.password = :password AND p.s_enabled = 1')
                ->setParameter('email', $_POST['email'])
                ->setParameter('password', $mod->passGenerator($_POST['password']))
                ->getQuery();
            $rows = $query->getArrayResult();
        
            if(count($rows) < 1 && $error == 0){
                $error += 1;
                $message = "The login details you've entered don't match any in our system.";
            }
            
            if($error == 0){

                // set session
                $session->set('tpn_user_id', $rows[0]['id']); 
                $session->set('tpn_user_email', $rows[0]['email']); 
                $session->set('tpn_user_fname', $rows[0]['fname']); 
                $session->set('tpn_user_lname', $rows[0]['lname']);
                
                
                
                return $this->redirect($this->generateUrl('acme_cls_tpn_home')); 
            }else{
                
                return $this->render('AcmeCLStpnBundle:DfaUserLogin:index.html.twig',
                        array('error'=>$message,
                            'email'=>$_POST['email']
                            )
                        );
            }
        
        }else{
            
            return $this->render('AcmeCLStpnBundle:DfatUserLogin:index.html.twig');
        }
        
        
    }
    
    
    /**
     * 
     * @todo logout and clear all session
     * @author leokarl
     */
    public function logoutAction(){
        $session = $this->getRequest()->getSession();
        $session->set('tpn_user_id',''); 
        $session->set('tpn_user_email',''); 
        $session->set('tpn_user_fname', ''); 
        $session->set('tpn_user_lname', ''); 
        //$this->get('session')->clear(); // clear all sessions
        
        return $this->redirect($this->generateUrl('acme_cls_tpn_login')); // redirect to login page
        
    }
}
