<?php

namespace Acme\CLStpnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ReportsController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-reports');
        
        if($session->get('tpn_user_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLStpnBundle:Reports:index.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'passport_types'=>$this->getPassportTypes()
                )
            );
    }

    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('tpn_user_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['dtpn_submitted_from'])){
            if(trim($_GET['dtpn_submitted_from']) != ''){
                $_GET['dtpn_submitted_from'] = filter_var($_GET['dtpn_submitted_from'], FILTER_SANITIZE_STRING);
                $_GET['dtpn_submitted_from'] = trim($_GET['dtpn_submitted_from']);
                $condition .= ' AND q.visa_date_submitted_for_processing >= "'. $mod->changeFormatToOriginal($_GET['dtpn_submitted_from']).' 00:00:59"';
            }
        }

        if(isset($_GET['dtpn_submitted_to'])){

            if(trim($_GET['dtpn_submitted_to']) != ''){
                $_GET['dtpn_submitted_to'] = filter_var($_GET['dtpn_submitted_to'], FILTER_SANITIZE_STRING);
                $_GET['dtpn_submitted_to'] = trim($_GET['dtpn_submitted_to']);
                $condition .= ' AND q.visa_date_submitted_for_processing <= "'.$mod->changeFormatToOriginal($_GET['dtpn_submitted_to']).' 23:59:59"';
            }
        }
        
        // date from filter
        if(isset($_GET['dsubmitted_by_applicant_from'])){
            if(trim($_GET['dsubmitted_by_applicant_from']) != ''){
                $_GET['dsubmitted_by_applicant_from'] = filter_var($_GET['dsubmitted_by_applicant_from'], FILTER_SANITIZE_STRING);
                $_GET['dsubmitted_by_applicant_from'] = trim($_GET['dsubmitted_by_applicant_from']);
                $condition .= ' AND q.date_submitted_by_applicant >= "'. $mod->changeFormatToOriginal($_GET['dsubmitted_by_applicant_from']).' 00:00:59"';
            }
        }

        if(isset($_GET['dsubmitted_by_applicant_to'])){

            if(trim($_GET['dsubmitted_by_applicant_to']) != ''){
                $_GET['dsubmitted_by_applicant_to'] = filter_var($_GET['dsubmitted_by_applicant_to'], FILTER_SANITIZE_STRING);
                $_GET['dsubmitted_by_applicant_to'] = trim($_GET['dsubmitted_by_applicant_to']);
                $condition .= ' AND q.date_submitted_by_applicant <= "'.$mod->changeFormatToOriginal($_GET['dsubmitted_by_applicant_to']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['dtpn_autorised_from'])){
            if(trim($_GET['dtpn_autorised_from']) != ''){
                $_GET['dtpn_autorised_from'] = filter_var($_GET['dtpn_autorised_from'], FILTER_SANITIZE_STRING);
                $_GET['dtpn_autorised_from'] = trim($_GET['dtpn_autorised_from']);
                $condition .= ' AND q.date_authorised >= "'. $mod->changeFormatToOriginal($_GET['dtpn_autorised_from']).'"';
            }
        }

        if(isset($_GET['dtpn_autorised_to'])){

            if(trim($_GET['dtpn_autorised_to']) != ''){
                $_GET['dtpn_autorised_to'] = filter_var($_GET['dtpn_autorised_to'], FILTER_SANITIZE_STRING);
                $_GET['dtpn_autorised_to'] = trim($_GET['dtpn_autorised_to']);
                $condition .= ' AND q.date_authorised <= "'.$mod->changeFormatToOriginal($_GET['dtpn_autorised_to']).'"';
            }
        }
        
        if(isset($_GET['ddfrom'])){
            if(trim($_GET['ddfrom']) != ''){
                $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
                $_GET['ddfrom'] = trim($_GET['ddfrom']);
                $condition .= ' AND q.departure_date >= "'. $mod->changeFormatToOriginal($_GET['ddfrom']).'"';
            }
        }

        if(isset($_GET['ddto'])){

            if(trim($_GET['ddto']) != ''){
                $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
                $_GET['ddto'] = trim($_GET['ddto']);
                $condition .= ' AND q.departure_date <= "'.$mod->changeFormatToOriginal($_GET['ddto']).'"';
            }
        }
        
//        if(isset($_GET['dsubmitted_to_embassy_from'])){
//            if(trim($_GET['dsubmitted_to_embassy_from']) != ''){
//                $_GET['dsubmitted_to_embassy_from'] = filter_var($_GET['dsubmitted_to_embassy_from'], FILTER_SANITIZE_STRING);
//                $_GET['dsubmitted_to_embassy_from'] = trim($_GET['dsubmitted_to_embassy_from']);
//                $condition .= ' AND t.departure_date >= "'. $mod->changeFormatToOriginal($_GET['dsubmitted_to_embassy_from']).'"';
//            }
//        }
//
//        if(isset($_GET['dsubmitted_to_embassy_to'])){
//
//            if(trim($_GET['dsubmitted_to_embassy_to']) != ''){
//                $_GET['dsubmitted_to_embassy_to'] = filter_var($_GET['dsubmitted_to_embassy_to'], FILTER_SANITIZE_STRING);
//                $_GET['dsubmitted_to_embassy_to'] = trim($_GET['dsubmitted_to_embassy_to']);
//                $condition .= ' AND t.departure_date <= "'.$mod->changeFormatToOriginal($_GET['dsubmitted_to_embassy_to']).'"';
//            }
//        }
        
        if(isset($_GET['dpassport_returned_to_applicant_from'])){
            if(trim($_GET['dpassport_returned_to_applicant_from']) != ''){
                $_GET['dpassport_returned_to_applicant_from'] = filter_var($_GET['dpassport_returned_to_applicant_from'], FILTER_SANITIZE_STRING);
                $_GET['dpassport_returned_to_applicant_from'] = trim($_GET['dpassport_returned_to_applicant_from']);
                $condition .= ' AND q.visa_date_order_on_route_and_closed >= "'. $mod->changeFormatToOriginal($_GET['dpassport_returned_to_applicant_from']).' 00:00:59"';
            }
        }

        if(isset($_GET['dpassport_returned_to_applicant_to'])){

            if(trim($_GET['dpassport_returned_to_applicant_to']) != ''){
                $_GET['dpassport_returned_to_applicant_to'] = filter_var($_GET['dpassport_returned_to_applicant_to'], FILTER_SANITIZE_STRING);
                $_GET['dpassport_returned_to_applicant_to'] = trim($_GET['dpassport_returned_to_applicant_to']);
                $condition .= ' AND q.visa_date_order_on_route_and_closed <= "'.$mod->changeFormatToOriginal($_GET['dpassport_returned_to_applicant_to']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND q.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        if(isset($_GET['destination'])){

            if(trim($_GET['destination']) != ''){
                $_GET['destination'] = filter_var($_GET['destination'], FILTER_SANITIZE_STRING);
                $_GET['destination'] = trim($_GET['destination']);
                $_GET['destination'] = intval($_GET['destination']);
                $condition .= ' AND q.destination_id = '.$_GET['destination'];
            }
        }
        
        if(isset($_GET['traveller_lname'])){

            if(trim($_GET['traveller_lname']) != ''){
                $_GET['traveller_lname'] = filter_var($_GET['traveller_lname'], FILTER_SANITIZE_STRING);
                $_GET['traveller_lname'] = trim($_GET['traveller_lname']);
                $condition .= " AND FIND_IN_SET('".$_GET['traveller_lname']."',q.traveller_lnames )";
            }
        }
        
        if(isset($_GET['tpn_number'])){

            if(trim($_GET['tpn_number']) != ''){
                $_GET['tpn_number'] = filter_var($_GET['tpn_number'], FILTER_SANITIZE_STRING);
                $_GET['tpn_number'] = trim($_GET['tpn_number']);
                $condition .= " AND q.tpn_no = '".$_GET['tpn_number']."'";
            }
        }
        
        if(isset($_GET['stat'])){

            if(trim($_GET['stat']) != ''){
                $_GET['stat'] = filter_var($_GET['stat'], FILTER_SANITIZE_STRING);
                $_GET['stat'] = trim($_GET['stat']);
                $_GET['stat'] = intval($_GET['stat']);
                $condition .= ' AND q.status = '.$_GET['stat'];
            }
        }

        if(isset($_GET['tpn_application_stat'])){

            if(trim($_GET['tpn_application_stat']) != ''){
                $_GET['tpn_application_stat'] = filter_var($_GET['tpn_application_stat'], FILTER_SANITIZE_STRING);
                $_GET['tpn_application_stat'] = trim($_GET['tpn_application_stat']);
                $_GET['tpn_application_stat'] = intval($_GET['tpn_application_stat']);
                $order_type = ($_GET['tpn_application_stat'] == 0) ? 2 : 3;
                $condition .= ' AND q.order_type = '.$order_type;
            }
        }
        
        if(isset($_GET['passport_type'])){

            if(trim($_GET['passport_type']) != ''){
                $_GET['passport_type'] = filter_var($_GET['passport_type'], FILTER_SANITIZE_STRING);
                $_GET['passport_type'] = trim($_GET['passport_type']);
                $_GET['passport_type'] = intval($_GET['passport_type']);
                $condition .= " AND FIND_IN_SET('".$_GET['passport_type']."',q.passport_types )";
            }
        }
        
        if(isset($_GET['tracking_number'])){

            if(trim($_GET['tracking_number']) != ''){
                $_GET['tracking_number'] = filter_var($_GET['tracking_number'], FILTER_SANITIZE_STRING);
                $_GET['tracking_number'] = trim($_GET['tracking_number']);
                $_GET['tracking_number'] = intval($_GET['tracking_number']);
                $condition .= ' AND q.order_no = '.$_GET['tracking_number'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
//        $statement = $connection->prepare("
//            SELECT o.date_submitted, d.name as department, p.account_no, o.order_no, t.tpn_no, 
//                o.primary_traveller_name, c.country_name as destination, t.date_last_updated, DATE_FORMAT(t.departure_date, '%d-%m-%Y') as departure_date, t.status,
//                t.tpn_no, t.status
//            FROM tbl_tpn t
//            LEFT JOIN tbl_orders o ON o.order_no = t.order_no
//            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
//            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
//            LEFT JOIN tbl_countries c ON c.id = t.destination
//            WHERE o.status >= 10
//            " . $condition);
        
        $statement = $connection->prepare("
            SELECT * FROM 
                (SELECT o.date_submitted as date_submitted_by_applicant, o.pri_dept_contact_department_id, d.name AS department, p.account_no, o.order_no, t.tpn_no, 
                    o.primary_traveller_name, t.destination as destination_id, c.country_name AS destination, t.date_last_updated, t.departure_date AS departure_date, 
                    t.status,
                    t.`date_issued` as date_authorised,
                    od.visa_date_submitted_for_processing,
                    (SELECT GROUP_CONCAT(ot.lname)  FROM tbl_order_travellers ot WHERE ot.order_no=t.order_no) AS traveller_lnames,
                    (SELECT GROUP_CONCAT(ot.passport_type)  FROM tbl_order_travellers ot WHERE ot.order_no=t.order_no) AS passport_types,
                    (SELECT GROUP_CONCAT(pt.type)  FROM tbl_order_travellers ot LEFT JOIN tbl_passport_types pt ON pt.id=ot.passport_type WHERE ot.order_no=t.order_no) AS passport_types_names,
                    o.status as order_status,
                    o.order_type,
                    od.visa_date_order_on_route_and_closed
                FROM tbl_tpn t
                LEFT JOIN tbl_order_destinations od ON (od.order_no = t.order_no AND od.country_id = t.destination)
                LEFT JOIN tbl_orders o ON o.order_no = t.order_no
                LEFT JOIN tbl_payment p ON p.order_no = o.order_no
                LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
                LEFT JOIN tbl_countries c ON c.id = t.destination
                WHERE o.status >= 10) q
            WHERE q.order_status >=10
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        
        //$fieldNames = array("date_submitted","date_submitted","date_last_updated","department","tpn_no","primary_traveller_name","destination","departure_date","status","order_no");
        $fieldNames = array(
            "visa_date_submitted_for_processing", 
            "visa_date_submitted_for_processing", 
            "date_authorised",
            "date_authorised",
            "departure_date",
            "departure_date",
            "date_submitted_by_applicant",
            "date_submitted_by_applicant",
            "visa_date_order_on_route_and_closed",
            "visa_date_order_on_route_and_closed",
            "department",
            "destination",
            "tpn_no",
            "status",
            "order_type",
            "traveller_lnames",
            "passport_types_names",
            "order_no",
            "tpn_no"
            );

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
}
