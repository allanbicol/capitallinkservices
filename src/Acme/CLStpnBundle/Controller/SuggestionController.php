<?php

namespace Acme\CLStpnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class SuggestionController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel;
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-suggestion');
        if($session->get('tpn_user_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        if(isset($_POST['suggest'])){
            if($_POST['fieldname']!=''){
                //$em = $this->getDoctrine()->getManager(); 
                $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
                $query = $cust->createQueryBuilder('p')
                   // ->select("d.alert_date, d.subject, d.body, d.status, d.id")
                    ->where('p.id = :id')
                    ->setParameter('id', $session->get('tpn_user_id'))
                    ->getQuery();
                $tpn_info = $query->getArrayResult();

//                $cust1 = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
//                $query1 = $cust1->createQueryBuilder('p')
//                   // ->select("d.alert_date, d.subject, d.body, d.status, d.id")
//                    ->where('p.s_enabled = :enabled')
//                    ->setParameter('enabled', 1)
//                    ->getQuery();
//                $admin = $query1->getArrayResult();

                $body='';
                $body .= 'Hi Admin,<br/><br/>';
                $body .= '<b>DFAT User Info</b>:<br/>';
                $body .= 'Name: '.$tpn_info[0]['fname'].' '. $tpn_info[0]['lname'].'<br/>';
                $body .= 'Email: '.$tpn_info[0]['email'].'<br/>';
                $body .= 'Phone: '.$tpn_info[0]['phone'].'<br/><br/>';

                $body .= '<b>Field to change help info:</b> '.$_POST['fieldname'].'<br/>';
                $body .= '<b>New information suggested:</b> '.$_POST['info'].'<br/><br/>';

                $body .= 'Best Regards,<br/><br/>';
                $body .= 'Capital Link Services';
                //send this to naim@capitallinkservices.com.au
                $send = $this->sendEmail("naim@capitallinkservices.com.au", "help@capitallinkservices.com.au", "Suggested Field Information", 
                                $body
                            );
                
                
            }
            return $this->render('AcmeCLStpnBundle:Suggestion:thankyou.html.twig');
        }else{
            return $this->render('AcmeCLStpnBundle:Suggestion:index.html.twig');
        }
        
        
        
        
    }
}
