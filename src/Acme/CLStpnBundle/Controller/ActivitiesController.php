<?php

namespace Acme\CLStpnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\TpnNotes;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ActivitiesController extends GlobalController
{
    public function activitiesAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-activities');
        
        if($session->get('tpn_user_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLStpnBundle:Activities:activities.html.twig',
                array('get'=> $_GET)
                );
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('tpn_user_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND p.log_datetime >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:01"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND p.log_datetime <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        
        if(isset($_GET['user'])){
            if(trim($_GET['user']) != ''){
                $_GET['user'] = filter_var($_GET['user'], FILTER_SANITIZE_STRING);
                $_GET['user'] = trim($_GET['user']);
                $condition .= ' AND p.username LIKE "%'.$_GET['user'].'%"';
            }
        }
        
        if(isset($_GET['reference_no'])){
            if(trim($_GET['reference_no']) != ''){
                $_GET['reference_no'] = filter_var($_GET['reference_no'], FILTER_SANITIZE_STRING);
                $_GET['reference_no'] = trim($_GET['reference_no']);
                $condition .= ' AND p.log_details LIKE "%'.$_GET['reference_no'].'%"';
            }
        }
        
        
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT * FROM
                (SELECT p.log_id, 
                p.area, 
                p.user_id, 
                (CASE WHEN p.area = 'admin' THEN CONCAT(au.fname, ' ', au.lname) WHEN p.area = 'client' THEN CONCAT(cu.fname, ' ', cu.lname) WHEN p.area = 'dfat' THEN CONCAT(tu.fname, ' ', tu.lname) WHEN p.area = 'embassy' THEN CONCAT(eu.fname, ' ', eu.lname) ELSE '' END) AS username, 
                p.user_type, 
                p.log_datetime, 
                REPLACE(p.log_details,'admin/dfat-edit-staff','dfat-client/edit-staff') as log_details
                FROM tbl_logs p
                LEFT JOIN tbl_user_admin au ON au.id = p.user_id
                LEFT JOIN tbl_user_client cu ON cu.id = p.user_id
                LEFT JOIN tbl_user_tpn tu ON tu.id = p.user_id
                LEFT JOIN tbl_user_embassy eu ON eu.id = p.user_id
                WHERE p.area = 'dfat'
                ) p
            WHERE p.log_id IS NOT NULL " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("log_datetime","log_datetime","area","username","user_type","log_details");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
    }

}
