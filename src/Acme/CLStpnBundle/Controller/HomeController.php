<?php

namespace Acme\CLStpnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class HomeController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel;
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-home');
        $new_pass =0;
        if ($session->get('pass')!=''){
            if($mod->passwordHardening($session->get('pass')) != '1#1#1#1'){
                $new_pass = 1;
                $session->set('pass', '');
            }
        }
        
        if($session->get('tpn_user_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLStpnBundle:Home:index.html.twig',array('new_pass'=>$new_pass));
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('tpn_user_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
            $query = $cust->createQueryBuilder('p')
                ->select("p.tpn_no, p.date_submitted, p.date_last_updated, p.order_no, p.client_id, o.primary_traveller_name, p.departure_date,
                    d.name as department, c.countryName as destination, p.status")
                ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = p.order_no')
                ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.destination')
                ->where('o.status = 10 AND p.status = 0')
                //->setParameter('status', "")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","date_last_updated","department","tpn_no","primary_traveller_name","destination","departure_date","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function generateTPNFromArchiveAction(){
        $mod = new Model\GlobalModel;
            
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT * FROM tbl_orders WHERE (order_type=2 OR order_type=3) AND s_archive=1 AND `status`>= 10
            LIMIT 1000,1000
            ");
        $statement->execute();
        $rows = $statement->fetchAll();
        
        for($i=0; $i<count($rows); $i++){
            $this->generateTPNletterArchive($rows[$i]['order_no'], $rows[$i]['client_id']);
        }
    }
    
    public function changePasswordAction(){
        
        if(isset($_POST['password'])){
            
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = $em->getRepository('AcmeCLStpnBundle:TpnUser')->findOneBy(array('id'=>$session->get('tpn_user_id')));
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }        
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            $error_code = 0;
            
                
            if($error_count == 0){
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $error_code =1;
                        $error_count += 1;
                    }
                }
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $error_code = 2;//'You must Follow the password required standard.';
                $error_count += 1;
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
               
                $this->sendEmail($session->get('tpn_user_email'), "help@capitallinkservices.com.au", "Change Password",
                                        $this->renderView('AcmeCLSadminBundle:Home:changePassword.html.twig',
                                        array('fname'=>$session->get('tpn_user_fname'),
                                            'password'=> $_POST['password'],
                                            'domain'=>$mod->siteURL()
                                            )
                                        ));
    
            }else{
                $em->getConnection()->rollback();
                $em->close();
                $error_code = 3;
            }
            
            return new Response($error_code);
        }
    }

}
