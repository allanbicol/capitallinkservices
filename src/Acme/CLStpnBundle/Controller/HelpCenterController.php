<?php

namespace Acme\CLStpnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class HelpCenterController extends GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-help-center');
        
        if($session->get('tpn_user_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLStpnBundle:HelpCenter:index.html.twig',
                array('items'=> $this->getVideoTutorials('tpn-user'))
                );
    }
    
    public function tutorialAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-help-center');
        
        if($session->get('tpn_user_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(!isset($_GET['vid'])){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        $_GET['vid'] = intval($_GET['vid']);
        
        return $this->render('AcmeCLStpnBundle:HelpCenter:tutorial.html.twig',
                array('item'=> $this->getVideoTutorials('tpn-user', $_GET['vid']))
                );
    }
    
}
