<?php

namespace Acme\CLStpnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\TpnNotes;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ReviewTpnController extends GlobalController
{
    public function indexAction()
    {
        $request = $this->getRequest();
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-review');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $host = $request->getScheme().'://'.$request->getHost();
        $baseurl = ($request->getHost() == 'localhost') ? $request->getScheme().'://'.$request->getHost().'/cls/web' : $request->getScheme().'://'.$request->getHost().'/web';
        $rootdir = dirname($this->get('kernel')->getRootDir());
        
        if($session->get('tpn_user_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $_POST['tpnNo'] = filter_var($_POST['tpnNo'], FILTER_SANITIZE_STRING);
            $_POST['tpnRejectNote'] = filter_var($_POST['tpnRejectNote'], FILTER_SANITIZE_STRING);
            $_POST["tpnSrc"] = strip_tags($_POST["tpnSrc"], '<p><b><strong><br><br/><div><img><span>');
            
            $tpndetails = $this->getTpnDetailsById($_POST["tpnNo"]);
            
            if($_POST["submitType"] == "save"){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();

                $model = $em->getRepository('AcmeCLSclientGovBundle:Tpn')->findOneBy(array('tpn_no'=>$_POST["tpnNo"]));
                $model->setTpnSrc($_POST["tpnSrc"]);
                $model->setDateLastUpdated($datetime->format("Y-m-d H:i:s"));
                $em->persist($model);
                $em->flush();    

                /* + create log */
                $logDetails = "TPN no. ". $_POST["tpnNo"] ." has been updated by <a href='".$this->generateUrl('acme_cls_admin_tpn_staff_edit')."?id=".$session->get('tpn_user_id')."' target='_blank'>" . $session->get('tpn_user_fname') . " " . $session->get('tpn_user_lname') . "</a>";
                $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
                /* - create log */
                

                // commit changes
                $em->getConnection()->commit(); 

                $this->get('session')->getFlashBag()->add(
                        'success',
                        'TPN No. '. $_POST["tpnNo"] .' has been updated'
                    );
                
                return $this->redirect($this->generateUrl('acme_cls_tpn_review')."?tpn=".$_POST["tpnNo"]);
            }elseif($_POST["submitType"] == "download"){
                
                $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
                $query = $cust->createQueryBuilder('p')
                    ->select("p.tpn_src")
                    ->where('p.tpn_no = :tpn_no')
                    ->setParameter('tpn_no', $_POST["tpnNo"])
                    ->getQuery();
                $data = $query->getArrayResult();
                $previous_tpn =  $data[0]['tpn_src'];
                $previous_html_header='<div style="background-image: url(\''.$baseurl.'/img/dfat-archive.png\');background-repeat: no-repeat;background-position: center;">';
                $previous_html_footer='</div>';
                
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                $src = str_replace('<div style="background-image: url(\''.$baseurl.'/img/dfat-archive.png\');background-repeat: no-repeat;background-position: center;">', 
                        '', $_POST["tpnSrc"]);
                $model = $em->getRepository('AcmeCLSclientGovBundle:Tpn')->findOneBy(array('tpn_no'=>$_POST["tpnNo"]));
                $model->setTpnSrc($src);
                $model->setTpnSrcPrevious($previous_html_header.$previous_tpn.$previous_html_footer);
                $model->setDateLastUpdated($datetime->format("Y-m-d H:i:s"));
                $em->persist($model);
                $em->flush();    
                
                /* + create log */
                $logDetails = "TPN no. ". $_POST["tpnNo"] ." downloaded by <a href='".$this->generateUrl('acme_cls_admin_tpn_staff_edit')."?id=".$session->get('tpn_user_id')."' target='_blank'>" . $session->get('tpn_user_fname') . " " . $session->get('tpn_user_lname') . "</a>";
                $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
                /* - create log */

                // commit changes
                $em->getConnection()->commit(); 

                $this->get('session')->getFlashBag()->add(
                        'download',
                        'TPN No. '. $_POST["tpnNo"] .' has been updated'
                    );
                
                return $this->redirect($this->generateUrl('acme_cls_tpn_review')."?tpn=".$_POST["tpnNo"]);
            
            }elseif($_POST["submitType"] == "previousTpnLetter"){
                return $this->redirect($this->generateUrl('acme_cls_tpn_review')."?tpn=".$_POST["tpnNo"]."&p_letter=1");
            
                
            }elseif($_POST["submitType"] == "currentTpnLetter"){
                return $this->redirect($this->generateUrl('acme_cls_tpn_review')."?tpn=".$_POST["tpnNo"]);
  
            }elseif($_POST["submitType"] == "reissue"){
                
                $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
                $query = $cust->createQueryBuilder('p')
                    ->select("p.tpn_src")
                    ->where('p.tpn_no = :tpn_no')
                    ->setParameter('tpn_no', $_POST["tpnNo"])
                    ->getQuery();
                $data = $query->getArrayResult();
                $previous_tpn =  $data[0]['tpn_src'];
                
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();

                $model = $em->getRepository('AcmeCLSclientGovBundle:Tpn')->findOneBy(array('tpn_no'=>$_POST["tpnNo"]));
                
                $previous_html_header='<div style="background-image: url(\''.$baseurl.'/img/dfat-archive.png\');background-repeat: no-repeat;background-position: center;">';
                $previous_html_footer='</div>';
                // fill logo 
                $_POST["tpnSrc"] = str_replace('<div id="dfat-header" style="height: 116px; width: 116px; background-color: #c9c9c9; margin: auto;"></div>', 
                        '<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-footer" style="height: 116px; width: 116px; background-color: #c9c9c9;"></div>', 
                        '<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-header" style="height: 90px; width: 90px; background-color: #c9c9c9; margin: auto;"></div>', 
                        '<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #c9c9c9;"></div>', 
                        '<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div style="margin: auto; width: 90px; height: 90px; background-color: rgb(201, 201, 201);" id="dfat-header"></div>', 
                        '<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div style="width: 140px; height: 140px; background-color: rgb(201, 201, 201);" id="dfat-footer"></div>', 
                        '<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-header" style="margin: auto; width: 90px; height: 90px; background-color: rgb(201, 201, 201);"></div>', 
                        '<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-footer" style="width: 140px; height: 140px; background-color: rgb(201, 201, 201);"></div>', 
                        '<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('[DateOfIssue]', date('d F Y', strtotime($datetime->format("Y-m-d H:i:s"))) , $_POST["tpnSrc"]);
                
                $model->setTpnSrc($_POST["tpnSrc"]);
                $model->setTpnSrcPrevious($previous_html_header.$previous_tpn.$previous_html_footer);
                $model->setDateLastUpdated($datetime->format("Y-m-d H:i:s"));
                $model->setDateIssued($datetime->format("Y-m-d H:i:s"));
                $em->persist($model);
                $em->flush();

                /* + create log */
                $logDetails = "TPN no. ". $_POST["tpnNo"] ." has been reissued by <a href='".$this->generateUrl('acme_cls_admin_tpn_staff_edit')."?id=".$session->get('tpn_user_id')."' target='_blank'>" . $session->get('tpn_user_fname') . " " . $session->get('tpn_user_lname') . "</a>";
                $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
                /* - create log */
                
                $em->getConnection()->commit(); 
                
                if(count($tpndetails) > 0){
                    $this->sendEmail($tpndetails["pri_dept_contact_email"], "help@capitallinkservices.com.au", "Approved TPN", 
                            $this->renderView('AcmeCLStpnBundle:ReviewTpn:approve_email_template.html.twig',
                                array('site_url'=>$mod->siteURL(),
                                    'data'=>$tpndetails,
                                    'type'=>'reissue'
                                    )
                                )
                        );
                }

                $this->get('session')->getFlashBag()->add(
                        'success',
                        'TPN No. '. $_POST["tpnNo"] .' has been reissued.'
                    );
                
                return $this->redirect($this->generateUrl('acme_cls_tpn_review')."?tpn=".$_POST["tpnNo"]);
            }elseif($_POST["submitType"] == "decline"){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();

//                $_POST["tpnSrc"] = str_replace('<span class="tpnno">'. trim($_POST["tpnNo"]) .'</span>', '<span class="tpnno">xx xxx xxxxx</span>', $_POST["tpnSrc"]);
//                $_POST["tpnSrc"] = str_replace('<span class="tpnno"><b>'. trim($_POST["tpnNo"]) .'</b></span>', '<span class="tpnno"><b>xx xxx xxxxx</b></span>', $_POST["tpnSrc"]);
                
                $model = $em->getRepository('AcmeCLSclientGovBundle:Tpn')->findOneBy(array('tpn_no'=>$_POST["tpnNo"]));
                $model->setStatus(2);
                $model->setDateLastUpdated($datetime->format("Y-m-d H:i:s"));
                $model->setTpnSrc($_POST["tpnSrc"]);
                $em->persist($model);
                $em->flush();    
                
                
                
                if($this->sTpnOrderCompleted($model->getOrderNo())){
                    $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$model->getOrderNo()));
                    $order->setStatus(12);
                    $em->persist($model);
                    $em->flush();
                }
                        
                $tpnn = new TpnNotes();
                $tpnn->setTpnNo($_POST["tpnNo"]);
                $tpnn->setNoteBy($session->get("tpn_user_id"));
                $tpnn->setUserType('DFAT');
                $tpnn->setNote($_POST["tpnRejectNote"]);
                $tpnn->setDateAdded($datetime->format("Y-m-d H:i:s"));
                $em->persist($tpnn);
                $em->flush();

                /* + create log */
                $logDetails = "TPN no. ". $_POST["tpnNo"] ." has been rejected by <a href='".$this->generateUrl('acme_cls_admin_tpn_staff_edit')."?id=".$session->get('tpn_user_id')."' target='_blank'>" . $session->get('tpn_user_fname') . " " . $session->get('tpn_user_lname') . "</a>";
                $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
                /* - create log */
                
                // commit changes
                $em->getConnection()->commit(); 
                
                
                $this->sendEmail($tpndetails["pri_dept_contact_email"], "help@capitallinkservices.com.au", "TPN Rejected", 
                        $this->renderView('AcmeCLStpnBundle:ReviewTpn:reject_email_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'data'=>$tpndetails,
                                'note'=> $_POST["tpnRejectNote"],
                                'host' => $host
                                )
                            )
                    );

                $this->get('session')->getFlashBag()->add(
                        'success',
                        'TPN No. '. $_POST["tpnNo"] .' has been rejected'
                    );
                
                
                
            }elseif($_POST["submitType"] == "accept"){
                
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();

                $model = $em->getRepository('AcmeCLSclientGovBundle:Tpn')->findOneBy(array('tpn_no'=>$_POST["tpnNo"]));
                
                // fill logo 
                $_POST["tpnSrc"] = str_replace('<div id="dfat-header" style="height: 116px; width: 116px; background-color: #c9c9c9; margin: auto;"></div>', 
                        '<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-footer" style="height: 116px; width: 116px; background-color: #c9c9c9;"></div>', 
                        '<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-header" style="height: 90px; width: 90px; background-color: #c9c9c9; margin: auto;"></div>', 
                        '<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #c9c9c9;"></div>', 
                        '<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div style="margin: auto; width: 90px; height: 90px; background-color: rgb(201, 201, 201);" id="dfat-header"></div>', 
                        '<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div style="width: 140px; height: 140px; background-color: rgb(201, 201, 201);" id="dfat-footer"></div>', 
                        '<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-header" style="margin: auto; width: 90px; height: 90px; background-color: rgb(201, 201, 201);"></div>', 
                        '<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('<div id="dfat-footer" style="width: 140px; height: 140px; background-color: rgb(201, 201, 201);"></div>', 
                        '<div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div>', $_POST["tpnSrc"]);
                
                $_POST["tpnSrc"] = str_replace('[DateOfIssue]', date('d F Y', strtotime($datetime->format("Y-m-d H:i:s"))) , $_POST["tpnSrc"]);
                
                $model->setTpnSrc($_POST["tpnSrc"]);
                $model->setTpnSrcOriginalApproved($_POST["tpnSrc"]);
                $model->setStatus(1);
                $model->setDateLastUpdated($datetime->format("Y-m-d H:i:s"));
                $model->setDateIssued($datetime->format("Y-m-d H:i:s"));
                $em->persist($model);
                $em->flush();

//                if($this->sTpnOrderCompleted($model->getOrderNo())){
//                    $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$model->getOrderNo()));
//                    $order->setStatus(12);
//                    $em->persist($model);
//                    $em->flush();
//                }
                
                /* + create log */
                $logDetails = "TPN no. ". $_POST["tpnNo"] ." has been approved by <a href='".$this->generateUrl('acme_cls_admin_tpn_staff_edit')."?id=".$session->get('tpn_user_id')."' target='_blank'>" . $session->get('tpn_user_fname') . " " . $session->get('tpn_user_lname') . "</a>";
                $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
                /* - create log */
                
                // commit changes
                $em->getConnection()->commit(); 
                
                
                
                // create a pdf file
                //$mod->executeUrl($baseurl."/api/pdfc/save.php?tpn=".$_POST["tpnNo"]);
                
                // created pdf file
                //$document = $rootdir."/web/api/pdfc/dev/".$_POST["tpnNo"].".pdf";
                
                
                
                if(count($tpndetails) > 0){
                    $this->sendEmail($tpndetails["pri_dept_contact_email"], "help@capitallinkservices.com.au", "Approved TPN", 
                            $this->renderView('AcmeCLStpnBundle:ReviewTpn:approve_email_template.html.twig',
                                array('site_url'=>$mod->siteURL(),
                                    'data'=>$tpndetails,
                                    'type'=>'approved'
                                    )
                                )
                        );
                }
                        
                
                
                
                $this->get('session')->getFlashBag()->add(
                        'success',
                        'TPN No. '. $_POST["tpnNo"] .' has been approved and sent to user'
                    );
                
                
            }elseif($_POST["submitType"] == "reprint"){
                /* + create log */
                $logDetails = "TPN no. ". $_POST["tpnNo"] ." - reprint invoice action by <a href='".$this->generateUrl('acme_cls_admin_tpn_staff_edit')."?id=".$session->get('tpn_user_id')."' target='_blank'>" . $session->get('tpn_user_fname') . " " . $session->get('tpn_user_lname') . "</a>";
                $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
                /* - create log */
                
                $this->reprint();
            }
            
            //
            return $this->redirect($this->generateUrl('acme_cls_tpn_home'));
            
        }else{
        
            $_GET['tpn'] = filter_var($_GET['tpn'], FILTER_SANITIZE_STRING);
            
            if(!isset($_GET["p_letter"])){
                $data = $this->getTpnDetailsById($_GET["tpn"]);
            }else{
                $data = $this->getPreviousTpnDetailsById($_GET["tpn"]);
            }
            
            if(count($data) > 0){
                return $this->render('AcmeCLStpnBundle:ReviewTpn:index.html.twig',
                    array('data'=> $data,
                        'site_url'=> $mod->siteURL(),
                        'tpn_notes' => $this->getTpnNotesByTpnNo($_GET["tpn"]),
                        'order_no' => $this->sTpnOrderNo($_GET["tpn"]))
                    );
            }else{
                return $this->render('AcmeCLStpnBundle:ReviewTpn:error.html.twig',
                    array('title'=> 'Error',
                        'message' => 'TPN number not found!')
                    );
            }
        }
    }

    
    
    public function addNoteAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get("tpn_user_email") != ""){
            if(trim($_POST["tpn_no"]) != ''){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();

                $_POST['tpn_no'] = filter_var($_POST['tpn_no'], FILTER_SANITIZE_STRING);

                $model = new TpnNotes();
                $model->setTpnNo($_POST["tpn_no"]);
                $model->setNoteBy($session->get("tpn_user_id"));
                $model->setUserType('DFAT');
                $model->setNote($_POST["note"]);
                $model->setDateAdded($datetime->format("Y-m-d H:i:s"));
                $em->persist($model);
                $em->flush();    
                
                /* + create log */
                $logDetails = "A note <i>'".$_POST["note"]."'</i> has been added to TPN no. ". $_POST["tpnNo"] ." by <a href='".$this->generateUrl('acme_cls_admin_tpn_staff_edit')."?id=".$session->get('tpn_user_id')."' target='_blank'>" . $session->get('tpn_user_fname') . " " . $session->get('tpn_user_lname') . "</a>";
                $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
                /* - create log */

                // commit changes
                $em->getConnection()->commit(); 

                $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
                $query = $cust->createQueryBuilder('p')
                    ->where("p.id = :id")
                    ->setParameter("id", $session->get("tpn_user_id"))
                    ->getQuery();
                $user = $query->getArrayResult();

                $data = array('note'=>$_POST["note"], 'date_added'=>$datetime->format("Y-m-d H:i:s"), 'added_by'=>$user[0]["fname"]. " (DFAT)");
                return new Response(json_encode($data));
            }
        }else{
            return new Response("session expired");
        }
    }
    
    
    
    /**
     * =========================================================================
     * Functions
     * =========================================================================
     */
    
    /**
     * get tpn details
     * @param type $tpn_no
     * @return type
     */
    public function getTpnDetailsById($tpn_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
        $query = $cust->createQueryBuilder('p')
            ->select("p.tpn_no, p.date_submitted, p.date_last_updated, p.order_no, p.client_id, o.primary_traveller_name, p.departure_date,
                d.code as department, c.countryName as destination, p.tpn_src, u.email, u.fname, o.pri_dept_contact_email, p.status")
            ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = p.order_no')
            ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.destination')
            ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = p.client_id')
            ->where('p.tpn_no = :tpn_no AND p.status != 2')
            ->setParameter('tpn_no', $tpn_no)
            ->getQuery();
        $results = $query->getArrayResult();
        
        if(count($results) > 0){
            return $results[0];
        }else{
            return array();
        }
    }
    
        /**
     * get tpn details
     * @param type $tpn_no
     * @return type
     */
    public function getPreviousTpnDetailsById($tpn_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
        $query = $cust->createQueryBuilder('p')
            ->select("p.tpn_no, p.date_submitted, p.date_last_updated, p.order_no, p.client_id, o.primary_traveller_name, p.departure_date,
                d.code as department, c.countryName as destination, p.tpn_src_previous as tpn_src, u.email, u.fname, o.pri_dept_contact_email, p.status")
            ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = p.order_no')
            ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.destination')
            ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = p.client_id')
            ->where('p.tpn_no = :tpn_no AND p.status != 2')
            ->setParameter('tpn_no', $tpn_no)
            ->getQuery();
        $results = $query->getArrayResult();
        
        if(count($results) > 0){
            return $results[0];
        }else{
            return array();
        }
    }
    /**
     * get tpn notes
     * @param type $tpn_no
     * @return type
     */
    public function getTpnNotesByTpnNo($tpn_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:TpnNotes');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.note, p.date_added, p.note_by, p.user_type")
            ->leftJoin('AcmeCLStpnBundle:TpnUser', 'u', 'WITH', 'u.id = p.tpn_no')
            ->where('p.tpn_no = :tpn_no')
            ->setParameter('tpn_no', $tpn_no)
            ->getQuery();
        $results = $query->getArrayResult();
        
        $data = array();
        for($i=0; $i<count($results); $i++){
            $data[] = array('note'=> $results[$i]["note"],
                    'date_added'=> $results[$i]["date_added"],
                    'note_by'=> $this->getNoteOwnerByIdAndUserType($results[$i]["note_by"], $results[$i]["user_type"]),
                    'user_type' => $results[$i]["user_type"]);
        }
        
        return $data;
    }
    
    
    /**
     * get noted by details
     * @param type $user_id
     * @param type $user_type
     * @return type
     */
    public function getNoteOwnerByIdAndUserType($user_id, $user_type){
        if($user_type == 'DFAT'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUSer');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :user_id')
                ->setParameter('user_id', $user_id)
                ->getQuery();
            $data = $query->getArrayResult();
            
        }elseif($user_type == 'ADMIN'){
            
        }
        
        
        return (count($data) > 0) ? $data[0] : array();
    }
    
    
    
    
    
    /**
     * return false if order tpns are still pending
     * return true if order tpns are all completed
     * @param type $order_no
     * @param type $tpn_status
     * @return type
     */
    public function sTpnOrderCompleted($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
        $query = $cust->createQueryBuilder('p')
            ->where('p.order_no = :order_no AND p.status = 0')
            ->setParameter('order_no', $order_no)
            ->getQuery();
        $results = $query->getArrayResult();
        
        return (count($results) > 0) ? false : true;
    }
    
}
