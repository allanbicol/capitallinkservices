<?php

namespace Acme\CLStpnBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class GlobalController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    /**
     * send email
     * @param type $recipients
     * @param type $from
     * @param type $subject
     * @param type $body
     */
    public function sendEmail($recipients, $from, $subject, $body, $attachment = ""){
        if($attachment != ""){
            $message = \Swift_Message::newInstance()
                ->setEncoder(\Swift_Encoding::get8BitEncoding())
                ->setSubject($subject)
                ->setFrom($from,'Capital Link Services')
                ->setTo($recipients)
                ->setBody($body)
                ->setContentType("text/html")
                ->attach(\Swift_Attachment::fromPath($attachment))
            ;
        }else{
            $message = \Swift_Message::newInstance()
                ->setEncoder(\Swift_Encoding::get8BitEncoding())
                ->setSubject($subject)
                ->setFrom($from,'Capital Link Services')
                ->setTo($recipients)
                ->setBody($body)
                ->setContentType("text/html")
            ;
        }
        $this->get('mailer')->send($message);
    }
    
    function printPDF($html){

    include (dirname($this->container->get('kernel')->getRootdir()).'/web/api/pdfc/html2pdf.class.php');
    $html2pdf = new \HTML2PDF('L', array(200, 200), 'en', true, 'UTF-8', array(5, 3, 5, 3));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    $html2pdf->WriteHTML($html);
    $html2pdf->Output(); //
    }
    
    public function reprint(){
        
        if(isset($_POST['hidSubmit'])){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $app = '';
 
        $client_id = $_POST['client_id'];
        $client_id = intval($client_id);

        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
        $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
        
        //print_r($session->get('client_id'));

        $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));

        $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));

        $this->printPDF(
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNumber"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings
                            )
                        )
                );

        // manual order email to admin
        if($session->get('user_type') == 'admin-user'){
                $this->printPDF(
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'tpn_settings'=>$tpn_settings,
                                    'manual_order'=>1
                                    )
                                )
                        );
        }

            if(trim($_SERVER['HTTP_HOST']) != 'localhost'){

                     $this->printPDF(
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNumber"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings
                            )
                        )
                );
            }


            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_home'));
            }
        }else{
            die();
        }
        
        
        
    }
    
    public function sTpnOrderNo($tpn_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
        $query = $cust->createQueryBuilder('p')
            ->where('p.tpn_no = :tpn_no')
            ->setParameter('tpn_no', $tpn_no)
            ->getQuery();
        $results = $query->getArrayResult();
        
        return $results[0];
    }
    
    public function getVideoTutorials( $type, $id = NULL){
        if($id == NULL){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:VideoTutorials');
            $query = $cust->createQueryBuilder('p')
                ->where('p.type = :type')
                ->setParameter('type', $type)
                ->getQuery();
            return $query->getArrayResult();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:VideoTutorials');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id',$id)
                ->getQuery();
            $data = $query->getArrayResult();
            return (count($data) > 0) ? $data[0] : array();
        }
        
    }
    
}
