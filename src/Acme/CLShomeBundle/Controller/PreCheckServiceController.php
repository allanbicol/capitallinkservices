<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;

class PreCheckServiceController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-pre-check-service');
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        include(dirname($this->get('kernel')->getRootDir()).'/web/api/simple-php-captcha-master/simple-php-captcha.php');
        
        if(isset($_POST['submit'])){
            $errors = array();
            
            if(!$mod->isEmailValid($_POST['email'])){
                $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
            }
            
            
            $resp = (strtolower($_POST['captcha']) == strtolower($session->get('captcha_code'))) ? true : false;
            if ($resp == false) {
                $errors[] = array('message'=>"The captcha wasn't entered correctly");
            }
            
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);
                
            $destinations = array();
            for($i=0; $i<count($_POST['country']); $i++){
                $destinations[] = array('country'=> $this->getCountryNameById($_POST['country'][$i]),
                        'entry_date'=>$_POST['entry_date'][$i],
                        'exit_date'=>$_POST['exit_date'][$i],
                        'visa_type'=>$_POST['visa_type'][$i],
                        'no_of_entries'=>$_POST['no_of_entries'][$i],

                    ); 
            }
                
            $post = array(
                        'fullname'=> $_POST['fullname'],
                        'email'=> $_POST['email'],
                        'phone'=> $_POST['phone'],
                        'traveller_name'=> $_POST['traveller_name'],
                        'passport_no'=> $_POST['passport_no'],
                        'passport_exp'=> $_POST['passport_exp'],
                        'birth_date'=> $_POST['birth_date'],
                        'nationality'=> $_POST['nationality'],
                        'destinations'=> $destinations,
                        'passport_info'=> $_FILES["passport_info"]["name"],
                        'visa_app_form'=> $_FILES["visa_app_form"]["name"],
                        'supporting_doc'=> $_FILES["supporting_doc"]["name"]
                        );
            
            if(count($errors) > 0){
                
                return $this->render('AcmeCLShomeBundle:PreCheckService:index.html.twig',
                    array(
                        'name_titles'=> $this->getNameTitles(),
                        'countries'=> $this->getCountries(),
                        'voucher_types'=>$this->getRussianVisaVoucherTypes(),
                        'captcha'=>$captcha['image_src'],
                        'post'=> $post,
                        'errors'=>$errors
                        )
                    );
            }else{
            
                $receipients = array();
                $receipients[] = 'help@capitallinkservices.com.au';
                $receipients[] = 'info@capitallinkservices.com.au';
                
                $admins = $this->getAdminUsers();
                
                for($i=0; $i<count($admins); $i++){
                    $receipients[] = $admins[$i]['email'];
                }
                /**
                 * Start: Send Email to admins
                 */
                $message = \Swift_Message::newInstance()
                    ->setEncoder(\Swift_Encoding::get8BitEncoding())
                    ->setSubject('Free Visa Pre-Check Service')
                    ->setFrom($_POST['email'],'Capital Link Services')
                    ->setTo($receipients)
                    ->setBody(
                            $this->renderView('AcmeCLShomeBundle:PreCheckService:email_to_admins.html.twig',
                                array(
                                    'post'=> $post
                                    )
                                )
                            )
                    ->setContentType("text/html")
                ;

                if(trim($_FILES["passport_info"]["name"]) != ''){
                    if(move_uploaded_file($_FILES["passport_info"]["tmp_name"], $root_dir ."/dev/prechecktemps/". $_FILES["passport_info"]["name"])){
                        $message->attach(\Swift_Attachment::fromPath($root_dir ."/dev/prechecktemps/". $_FILES["passport_info"]["name"]));
                        //unlink($root_dir ."/dev/prechecktemps/". $_FILES["passport_info"]["name"]);
                        
                    }
                }

                if(trim($_FILES["visa_app_form"]["name"]) != ''){
                    if(move_uploaded_file($_FILES["visa_app_form"]["tmp_name"], $root_dir ."/dev/prechecktemps/". $_FILES["visa_app_form"]["name"])){
                        $message->attach(\Swift_Attachment::fromPath($root_dir ."/dev/prechecktemps/". $_FILES["visa_app_form"]["name"]));
                    }
                }

                if(trim($_FILES["supporting_doc"]["name"]) != ''){
                    if(move_uploaded_file($_FILES["supporting_doc"]["tmp_name"], $root_dir ."/dev/prechecktemps/". $_FILES["supporting_doc"]["name"])){
                        $message->attach(\Swift_Attachment::fromPath($root_dir ."/dev/prechecktemps/". $_FILES["supporting_doc"]["name"]));
                    }
                }

                $this->get('mailer')->send($message);
                
                /**
                 * End: Send Email to admins
                 */
                
                
                /**
                 * Start: Send Email copy to user
                 */
                $message = \Swift_Message::newInstance()
                    ->setEncoder(\Swift_Encoding::get8BitEncoding())
                    ->setSubject('Free Visa Pre-Check Service')
                    ->setFrom('info@capitallinkservices.com.au','Capital Link Services')
                    ->setTo($_POST['email'])
                    ->setBody(
                            $this->renderView('AcmeCLShomeBundle:PreCheckService:email_to_user.html.twig',
                                array(
                                    'post'=> $post
                                    )
                                )
                            )
                    ->setContentType("text/html")
                ;

                if(trim($_FILES["passport_info"]["name"]) != ''){
                    $message->attach(\Swift_Attachment::fromPath($root_dir ."/dev/prechecktemps/". $_FILES["passport_info"]["name"]));
                }

                if(trim($_FILES["visa_app_form"]["name"]) != ''){
                    $message->attach(\Swift_Attachment::fromPath($root_dir ."/dev/prechecktemps/". $_FILES["visa_app_form"]["name"]));
                }

                if(trim($_FILES["supporting_doc"]["name"]) != ''){
                    $message->attach(\Swift_Attachment::fromPath($root_dir ."/dev/prechecktemps/". $_FILES["supporting_doc"]["name"]));
                }

                $this->get('mailer')->send($message);
                
                /**
                 * End: Send Email copy to user
                 */
                
                
                
                

                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Form has been submitted.'
                );

                return $this->redirect($this->generateUrl('acme_cls_pre_check_service')); 
            }
        }else{
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);

            return $this->render('AcmeCLShomeBundle:PreCheckService:index.html.twig',
                array(
                    'name_titles'=> $this->getNameTitles(),
                    'countries'=> $this->getCountries(),
                    'voucher_types'=>$this->getRussianVisaVoucherTypes(),
                    'captcha'=>$captcha['image_src']
                    )
                );
        }
    }
    
    public function visaTypeOptionsAction()
    {
        $_POST['country_id'] = intval($_POST['country_id']);
        $types = $this->getPublicVisaTypesByCountryId($_POST['country_id']);
        
        $options = '<option>Select type</option>';
        for($i=0; $i<count($types); $i++){
            if(trim($types[$i]['type']) != ''){
                $options .= '<option value="'.$types[$i]['type'].'">'. $types[$i]['type'] .'</option>';
            }
        }
        
        return new Response($options);
    }

}
