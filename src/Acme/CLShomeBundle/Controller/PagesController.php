<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class PagesController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function governmentAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-government');
        return $this->render('AcmeCLShomeBundle:Pages:government.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(3),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages()
                    )
                );
    }

    public function governmentVisasAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-government-visas');
        return $this->render('AcmeCLShomeBundle:Pages:governmentVisas.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(14),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages()
                    )
                );
    }
    public function travelVisasAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-travel-visas');
        
        return $this->render('AcmeCLShomeBundle:Pages:travelVisas.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(2),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages()
                )
                );
    }

    public function travelAgentsAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-travel-agents');
        return $this->render('AcmeCLShomeBundle:Pages:travelAgents.html.twig',
                array(
                'clearances'=> $this->getPoliceClearances(),
                'countries'=> $this->getCountries(),
                'content'=> $this->getPageContentByNo(4),
                'popular_destinations'=> $this->getPopularDestinations(),
                'ads' => $this->getHomeAdsImages()
                )
                );
    }

    public function corporateAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-corporate');
        return $this->render('AcmeCLShomeBundle:Pages:corporate.html.twig',
                array(
                'clearances'=> $this->getPoliceClearances(),
                'countries'=> $this->getCountries(),
                'content'=> $this->getPageContentByNo(5),
                'popular_destinations'=> $this->getPopularDestinations(),
                'ads' => $this->getHomeAdsImages()
                )
                );
    }

    public function allServicesAction()
    {
        return $this->render('AcmeCLShomeBundle:Pages:allServices.html.twig',
                array('countries'=> $this->getCountries(),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages()
                )
                );
    }

    public function aboutCLSAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-about');
        return $this->render('AcmeCLShomeBundle:Pages:aboutCLS.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(6),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages()
                )
                );
    }
    
    public function visaProcessingAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-services-visa-processing');
        return $this->render('AcmeCLShomeBundle:Pages:visaProcessing.html.twig',
                array(
                'clearances'=> $this->getPoliceClearances(),
                'countries'=> $this->getCountries(),
                'content'=> $this->getPageContentByNo(7),
                'popular_destinations'=> $this->getPopularDestinations(),
                'ads' => $this->getHomeAdsImages()
                )
                );
    }
    
    
    
    
    
    
    public function policeClearancesAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-services-police-clearances');
        return $this->render('AcmeCLShomeBundle:Pages:policeClearances.html.twig',
            array('clearances'=> $this->getPoliceClearances(),
                'countries'=> $this->getCountries(),
                'content'=> $this->getPageContentByNo(8),
                'popular_destinations'=> $this->getPopularDestinations(),
                'ads' => $this->getHomeAdsImages()
                    )
                );
    }

    
    
    
    public function documentLegalisationAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-services-doc-legalisation');
        return $this->render('AcmeCLShomeBundle:Pages:documentLegalisation.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(10),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages()
                )
                );
    }
    
    
    public function translationServicesAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-services-translation-services');
        return $this->render('AcmeCLShomeBundle:Pages:translationServices.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(11),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages()
                    )
                );
    }
    
    
    public function helpfulLinksAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-helpful-links');
        return $this->render('AcmeCLShomeBundle:Pages:helpfulLinks.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(12),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages()
                    )
                );
    }
    
    public function travelAlertsAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-travel-alerts');
        return $this->render('AcmeCLShomeBundle:Pages:travelAlerts.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(13),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages(),
                    'travel_alerts'=>$this->getTravelAlerts()
                    )
                );
    }
    
    public function sitemapAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-sitemap');
        return $this->render('AcmeCLShomeBundle:Pages:sitemap.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'content'=> $this->getPageContentByNo(13),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'ads' => $this->getHomeAdsImages(),
                    'travel_alerts'=>$this->getTravelAlerts()
                    )
                );
    }
    
    
    public function contactAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-inquiries');
        include(dirname($this->get('kernel')->getRootDir()).'/web/api/simple-php-captcha-master/simple-php-captcha.php');
        
        
        
        if(isset($_POST['hid_submit'])){
            
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'company'=>$_POST['company'],
                'workAdress'=>$_POST['workAdress'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'serviceRequired'=>$_POST['serviceRequired'],
                'countryOfRequiredService'=>$_POST['countryOfRequiredService'],
                'itemDescription'=>$_POST['itemDescription']
            );
            
            
            
            $resp = (strtolower($_POST['captcha']) == strtolower($session->get('captcha_code'))) ? true : false;
            if ($resp == false) {
                $captcha = simple_php_captcha();
                $session->set('captcha_code', $captcha['code']);
                return $this->render('AcmeCLShomeBundle:Pages:contact.html.twig',
                    array('post'=>$post,
                        'error'=>"The captcha wasn't entered correctly.",
                        'captcha'=>$captcha['image_src'],
                        'titles'=> $this->getNameTitles(),
                        'countries'=> $this->getCountries()
                        )
                    );
            }
            
            if(!$mod->isEmailValid($_POST['email'])){
                $email_error = 'Email ' . $_POST['email'] . ' is not a valid email address.';
                
                $captcha = simple_php_captcha();
                $session->set('captcha_code', $captcha['code']);
            
                return $this->render('AcmeCLShomeBundle:Pages:contact.html.twig',
                    array('post'=>$post,
                        'error'=>$email_error,
                        'captcha'=>$captcha['image_src'],
                        'titles'=> $this->getNameTitles(),
                        'countries'=> $this->getCountries()
                        )
                    );
            }
            
            $admins = $this->getAdminUsers();
            
            for($i=0; $i<count($admins); $i++){
                $this->sendEmail($admins[$i]["email"],$_POST['email'], 'CLS Inquiries', 
                    $this->renderView('AcmeCLShomeBundle:Pages:contact_email_template.html.twig',
                        array('fullname'=>$admins[$i]["fname"],
                            'data'=>$post,
                            'domain'=>$mod->siteURL()
                            )
                        )
                );
            }
             
            
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);
                
            $this->get('session')->getFlashBag()->add(
                        'success',
                        'Thanks for your enquiry, we\'ll get back to you shortly.'
                    );
            
            
            return $this->redirect($this->generateUrl('acme_cls_contact')); 
            
//            return $this->render('AcmeCLShomeBundle:Pages:contact.html.twig',
//                    array('captcha'=>$captcha['image_src'],
//                        'titles'=> $this->getNameTitles(),
//                        'countries'=> $this->getCountries(),
//                        'post'=>$post,
//                        'success'=>1,
//                        ));
            
        }else{
            
            
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);

            return $this->render('AcmeCLShomeBundle:Pages:contact.html.twig',
                    array('captcha'=>$captcha['image_src'],
                        'titles'=> $this->getNameTitles(),
                        'countries'=> $this->getCountries()
                        ));
            
        }
        
        
    }

}
