<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class UserLoginController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'user-login');
        
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $error = 0;
        $message = array();
        
        if($session->get('user_type') != ''){
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('acme_cls_admin_home'));
            }else if($session->get('user_type') == 'tpn-user'){
                return $this->redirect($this->generateUrl('acme_cls_tpn_home'));
            }else if($session->get('user_type') == 'embassy-user'){
                return $this->redirect($this->generateUrl('acme_cls_embassy_homepage'));
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_home'));
            }
        }
        
        if(isset($_POST['hid_submit'])){
            $error =0;
            // Check username and password
            if(!isset($_POST['email']) && !isset($_POST['password'])){
                return $this->redirect($this->generateUrl('acme_cls_tpn_login'));
            }
            
            if(trim($_POST['email']) == ''){
                $error += 1;
                $message = "Email address should not be blank.";
            }
            
            // check email validity
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $error == 0){
                $error += 1;
                $message = "Email ".$_POST['email']." is not a valid email address.";
            }
            
            if(trim($_POST['password']) == '' && $error == 0){
                $error += 1;
                $message = "Password should not be blank.";
            }
            $user_type = '';
            $is_driver = 0;
            
            // check government user
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.password = :password AND p.s_enabled = 1')
                ->setParameter('email', $_POST['email'])
                ->setParameter('password', $mod->passGenerator($_POST['password']))
                ->getQuery();
            $rows = $query->getArrayResult();
            
            if(count($rows) > 0 ){
                $user_type = $rows[0]['type'];
            }else{
                
                // check tpn user in database
                $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.email = :email AND p.password = :password AND p.s_enabled = 1')
                    ->setParameter('email', $_POST['email'])
                    ->setParameter('password', $mod->passGenerator($_POST['password']))
                    ->getQuery();
                $rows = $query->getArrayResult();
                
                if(count($rows) > 0 ){
                    $user_type = "tpn-user";
                }else{
                    
                    // check tpn user in database
                    $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
                    $query = $cust->createQueryBuilder('p')
                        ->where('p.email = :email AND p.password = :password AND p.s_enabled = 1')
                        ->setParameter('email', $_POST['email'])
                        ->setParameter('password', $mod->passGenerator($_POST['password']))
                        ->getQuery();
                    $rows = $query->getArrayResult();
                    if(count($rows) > 0){
                        $user_type = "admin-user";
                        $is_driver = $rows[0]['s_driver'];
                    }else{
                        // check embassy user in database
                        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:EmbassyUser');
                        $query = $cust->createQueryBuilder('p')
                            ->select('p.email, p.id, p.status, p.fname, p.lname, p.country, c.countryName')
                            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.country')
                            ->where('p.email = :email AND p.password = :password AND p.status = 1')
                            ->setParameter('email', $_POST['email'])
                            ->setParameter('password', $mod->passGenerator($_POST['password']))
                            ->getQuery();
                        $rows = $query->getArrayResult();
                        
                        if(count($rows) > 0){
                            $user_type = "embassy-user";
                        }
                    }
                }
                
            }
            
            if(count($rows) < 1 && $error == 0){
                $error += 1;
                $message = "The login details you've entered don't match any in our system.";
            }
            
            if($error == 0){
                
                // set session
                $session->set('pass', $_POST['password']);
                if($user_type == 'tpn-user'){
                    
                    $session->set('tpn_user_id', $rows[0]['id']); 
                    $session->set('user_type', "tpn-user");
                    $session->set('tpn_user_email', $rows[0]['email']); 
                    $session->set('tpn_user_fname', $rows[0]['fname']); 
                    $session->set('tpn_user_lname', $rows[0]['lname']);
                    
                    // last login
                    $em = $this->getDoctrine()->getManager();
                    $model = $em->getRepository('AcmeCLStpnBundle:TpnUser')->findOneBy(array('id'=>$rows[0]['id']));
                    
                    $model->setDateLastLogin($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                    
                    /* + create log */
                    $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_tpn_staff_edit') ."?id=". $model->getId() ."' target='_blank'>". $model->getFname() . " ". $model->getLname() . "</a> has successfully logged on the DFAT Client Centre.";
                    $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
                    /* - create log */
                    if($session->get("redirect_url") != ''){
                        return $this->redirect($session->get("redirect_url")); 
                    }else{
                        return $this->redirect($this->generateUrl('acme_cls_tpn_home')); 
                    }
                    
                }elseif($user_type == 'admin-user'){
                    $session->set('admin_id', $rows[0]['id']); 
                    $session->set('user_type', "admin-user");
                    $session->set('admin_email', $rows[0]['email']); 
                    $session->set('admin_fname', $rows[0]['fname']); 
                    $session->set('admin_lname', $rows[0]['lname']);
                    if($is_driver==0){
                        $session->set('is_driver', 0);
                    }else{
                        $session->set('is_driver', 1);
                    }
                    // last login
                    $em = $this->getDoctrine()->getManager();
                    $em->getConnection()->beginTransaction(); 
                    $model = $em->getRepository('AcmeCLSadminBundle:AdminUser')->findOneBy(array('id'=>$rows[0]['id']));
                    $model->setLastLogin($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                    $em->getConnection()->commit(); 
                    
                    /* + create log */
                    $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_staff_edit') ."?id=". $model->getId() ."' target='_blank'>". $model->getFname() . " ". $model->getLname() . "</a> has successfully logged on the CLS Admin Centre.";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                    
                    if($session->get("redirect_url") != ''){
                        return $this->redirect($session->get("redirect_url")); 
                    }else{
                        if($is_driver==0){
                            return $this->redirect($this->generateUrl('acme_cls_admin_home')); 
                        }else{
                            return $this->redirect($this->generateUrl('acme_cls_driver_home'));
                        }
                    }
                    
                }elseif($user_type == 'embassy-user'){
                    $session->set('embassy_id', $rows[0]['id']); 
                    $session->set('user_type', "embassy-user");
                    $session->set('country', $rows[0]['country']); 
                    $session->set('embassy_email', $rows[0]['email']); 
                    $session->set('embassy_fname', $rows[0]['fname']); 
                    $session->set('embassy_lname', $rows[0]['lname']);
                    $session->set('embassy_country_name', $rows[0]['countryName']);
                    
                    // last login
//                    $em = $this->getDoctrine()->getManager();
//                    $em->getConnection()->beginTransaction(); 
//                    $model = $em->getRepository('AcmeCLSadminBundle:EmbassyUser')->findOneBy(array('id'=>$rows[0]['id']));
//                    $model->setLastLogin($datetime->format('Y-m-d H:i:s'));
//                    $em->persist($model);
//                    $em->flush();
//                    $em->getConnection()->commit(); 
                    
                    /* + create log */
                    $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_embassy_edit') ."?id=". $rows[0]['id'] ."' target='_blank'>". $rows[0]['fname'] . " ". $rows[0]['lname'] . "</a> has successfully logged on the ". $rows[0]['countryName'] ." - Embassy Client Centre.";
                    $this->createLog('embassy', $session->get('embassy_id'), 'embassy', $logDetails);
                    /* - create log */
                    
                    if($session->get("redirect_url") != ''){
                        return $this->redirect($session->get("redirect_url")); 
                    }else{
                        return $this->redirect($this->generateUrl('acme_cls_embassy_homepage')); 
                    }
                    
                }else{
                    $session->set('client_id', $rows[0]['id']); 
                    $session->set('user_type', $rows[0]['type']);
                    $session->set('email', $rows[0]['email']); 
                    $session->set('fname', $rows[0]['fname']); 
                    $session->set('lname', $rows[0]['lname']);
                    $session->set('phone', $rows[0]['phone']);
                    
                    /* + create log */
                    $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_clients_edit') ."?id=". $rows[0]['id'] ."' target='_blank'>". $rows[0]['fname'] . " ". $rows[0]['lname'] . "</a> has successfully logged on the ". ucfirst($rows[0]['type']) ." Client Centre.";
                    $this->createLog('client', $session->get('client_id'), $rows[0]['type'], $logDetails);
                    /* - create log */
                    
                    if($session->get("redirect_url") != ''){
                        return $this->redirect($session->get("redirect_url")); 
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_home')); 
                    }
                    
                    
                }
                
                
            }else{
                
                return $this->render('AcmeCLShomeBundle:UserLogin:index.html.twig',
                        array('error'=>$message,
                            'email'=>$_POST['email'],
                            'countries'=> $this->getCountries()
                            )
                        );
            }
        
        }else{
            
            return $this->render('AcmeCLShomeBundle:UserLogin:index.html.twig',
                    array('countries'=> $this->getCountries())
                    );
        }
        
        
    }
    
    
    /**
     * 
     * @todo logout and clear all session
     * @author leokarl
     */
    public function logoutAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('client_id') != ''){
            /* + create log */
            $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_clients_edit') ."?id=". $session->get('client_id') ."' target='_blank'>". $session->get('fname') . " ". $session->get('lname') . "</a> has logged out from the ". ucfirst($session->get('user_type')) ." Client Centre.";
            $this->createLog('client', $session->get('client_id'), $session->get('user_type'), $logDetails);
            /* - create log */
        }elseif($session->get('embassy_id') != ''){
            /* + create log */
            $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_embassy_edit') ."?id=". $session->get('client_id') ."' target='_blank'>". $session->get('embassy_fname') . " ". $session->get('embassy_lname') . "</a> has logged out from the ". ucfirst($session->get('embassy_country_name')) ." - Embassy Client Centre.";
            $this->createLog('embassy', $session->get('embassy_id'), 'embassy', $logDetails);
            /* - create log */
        }elseif($session->get('admin_id') != ''){
            /* + create log */
            $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_staff_edit') ."?id=". $session->get('admin_id') ."' target='_blank'>". $session->get('admin_fname') . " ". $session->get('admin_lname') . "</a> has logged out from the CLS Admin Centre.";
            $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
            /* - create log */
        }elseif($session->get('tpn_user_id') != ''){
            /* + create log */
            $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_tpn_staff_edit') ."?id=". $session->get('tpn_user_id') ."' target='_blank'>". $session->get('tpn_user_fname') . " ". $session->get('tpn_user_lname') . "</a> has logged out from the DFAT Client Centre.";
            $this->createLog('dfat', $session->get('tpn_user_id'), 'dfat', $logDetails);
            /* - create log */
        }
        
        $is_driver = $session->get('is_driver')=='1' ? 1 : 0;
        
        $this->get('session')->clear(); // clear all sessions
       if($is_driver==1){
           return $this->redirect($this->generateUrl('acme_cls_driver_login'));
            
       }else{
            return $this->redirect($this->generateUrl('acme_cls_homepage')); // redirect to login page
       }
    }
}
