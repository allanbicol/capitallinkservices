<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'homepage');
        
        return $this->render('AcmeCLShomeBundle:Default:index.html.twig',
                array(
                    'clearances'=> $this->getPoliceClearances(),
                    'countries'=> $this->getCountries(),
                    'popular_destinations'=> $this->getPopularDestinations(),
                    'unpopular_destinations'=> $this->getNotPopularDestinations(),
                    'content'=> $this->getPageContentByNo(1),
                    'ads' => $this->getHomeAdsImages()
                    )
                );
    }
}
