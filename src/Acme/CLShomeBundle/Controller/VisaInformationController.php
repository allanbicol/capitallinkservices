<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class VisaInformationController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-visa-information');
        
        $condition = '';
        // is paid on account filter
        if(isset($_GET['search'])){

            if(trim($_GET['search']) != ''){
                $_GET['search'] = filter_var($_GET['search'], FILTER_SANITIZE_STRING);
                $_GET['search'] = trim($_GET['search']);
                $condition .= ' WHERE country_code like "%'.$_GET['search'].'%" OR country_name like "%'.$_GET['search'].'%"';
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT * FROM tbl_countries ".$condition. " ORDER BY country_name ASC");
        $statement->execute();
        $results = $statement->fetchAll();
        
        return $this->render('AcmeCLShomeBundle:VisaInformation:index.html.twig',
                array('visas'=>$results,
                    'get'=>$_GET,
                    'countries'=> $this->getCountries()
                    )
                );
    }
    
    public function getInfoAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-visa-information');
        
        $_GET['id'] = filter_var($_GET['id'], FILTER_SANITIZE_STRING);
        $_GET['id'] = intval($_GET['id']);
        
        $data = $this->getPublicVisaDetailsByCountryId($_GET['id']);
        
        
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:VisaPopupContent');
        $query = $cust->createQueryBuilder('p')
                ->where('p.id =:id')
                ->setParameter("id", 1)
                ->getQuery();
        $content = $query->getArrayResult();

        return $this->render('AcmeCLShomeBundle:VisaInformation:visaInformation.html.twig',
                array('data'=>$data,
                    'visa_courier_options' => $this->getCourierOptions(),
                    'countries'=> $this->getCountries(),
                    'saudi_popup_content'=> (isset($content[0]['content'])) ? $content[0]['content'] : '' 
                ));
    }

}
