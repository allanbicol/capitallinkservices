<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class DocumentDeliveryController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-services-doc-delivery');
        return $this->render('AcmeCLShomeBundle:DocumentDelivery:index.html.twig',
            array(
                'clearances'=> $this->getPoliceClearances(),
                'countries'=> $this->getCountries(),
                'content'=> $this->getPageContentByNo(9),
                'popular_destinations'=> $this->getPopularDestinations(),
                'ads' => $this->getHomeAdsImages(),
                'doc_types'=>$this->getDocumentDeliveryTypes()
                )
            );
        
        
    }
    
    public function redirectToLoginAction(){
        $session = $this->getRequest()->getSession();
        
        $session->set("redirect_url", $this->generateUrl('cls_client_aplication_document_delivery'));
        
        return $this->redirect($this->generateUrl('acme_cls_client_login'));
    }

    
    
    
}
