<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class PoliceClearanceController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $_GET['id'] = intval($_GET['id']);
        $data = $this->getPoliceClearanceInfoById($_GET['id']);
        
        if(count($data) > 0){
            return $this->render('AcmeCLShomeBundle:PoliceClearance:index.html.twig',
                array('data'=> $data,
                    'countries'=> $this->getCountries()
                    )
                );
        }else{
            return $this->render('AcmeCLShomeBundle:PoliceClearance:error.html.twig',
                array(
                    'title'=> 'Not Found',
                    'message'=> 'Police Clearance ID '.$_GET["id"].' not found!' 
                ));
        }
        
        
    }
    
    public function redirectToLoginAction(){
        $session = $this->getRequest()->getSession();
        $_GET['id'] = intval($_GET['id']);
        
        
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            
            $session->set("redirect_url", $this->generateUrl('cls_client_gov_application_police_clearance'));
            $session->set("selected_police_clearance_id", $_GET['id']);
            $session->set("public_reg", "signup");
        }else{
            $session->set("redirect_url", $this->generateUrl('cls_client_gov_application_police_clearance'));
            $session->set("selected_police_clearance_id", $_GET['id']);
        }
        
        return $this->redirect($this->generateUrl('acme_cls_client_login'));
    }

    
    
    
}
