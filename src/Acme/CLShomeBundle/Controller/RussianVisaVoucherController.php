<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class RussianVisaVoucherController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-russian-visa-voucher');
        return $this->render('AcmeCLShomeBundle:RussianVisaVoucher:index.html.twig',
            array(
                'name_titles'=> $this->getNameTitles(),
                'countries'=> $this->getCountries(),
                'voucher_types'=>$this->getRussianVisaVoucherTypes()
                )
            );
    }

}
