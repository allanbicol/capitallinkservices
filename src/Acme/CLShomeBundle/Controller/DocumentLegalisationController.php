<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class DocumentLegalisationController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'cls-services-doc-legalisation');
        return $this->render('AcmeCLShomeBundle:DocumentLegalisation:index.html.twig',
            array(
                'clearances'=> $this->getPoliceClearances(),
                'countries'=> $this->getCountries(),
                'content'=> $this->getPageContentByNo(9),
                'popular_destinations'=> $this->getPopularDestinations(),
                'ads' => $this->getHomeAdsImages(),
                'doc_types'=>$this->getDocumentDeliveryTypes(),
                'visa_courier_options' => $this->getCourierOptions(),
                )
            );
    }

}
