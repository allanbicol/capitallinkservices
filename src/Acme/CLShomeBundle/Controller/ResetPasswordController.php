<?php

namespace Acme\CLShomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;


class ResetPasswordController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('user_type') == ''){
            return $this->render('AcmeCLShomeBundle:ResetPassword:index.html.twig',
                    array('countries'=> $this->getCountries())
                    );
        }else{
            // if admin, if tpn, if client
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('acme_cls_admin_home'));
            }else if($session->get('user_type') == 'tpn-user'){
                return $this->redirect($this->generateUrl('acme_cls_tpn_home'));
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_home'));
            }
        }
    }
    
    public function resetSuccessAction()
    {
        $session = $this->getRequest()->getSession();
        
        return $this->render('AcmeCLShomeBundle:ResetPassword:resetsuccess.html.twig',
                array('countries'=> $this->getCountries())
                );
    }
    
    public function checkEmailAction(){
        
        $mod = new Model\GlobalModel();
        if(!isset($_POST['email'])){
            exit('email_required');
        }
        if(trim($_POST['email']) == ''){
            exit('email_required');
        }
        if(!$mod->isEmailValid($_POST['email'])){
            exit('email_invalid');
        }
        
        $user_type = '';
        // check government user
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
        $query = $cust->createQueryBuilder('p')
            ->where('p.email = :email AND p.s_enabled = 1')
            ->setParameter('email', $_POST['email'])
            ->getQuery();
        $rows = $query->getArrayResult();

        if(count($rows) > 0 ){
            $user_type = $rows[0]['type'];
        }else{

            // check tpn user in database
            $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.s_enabled = 1')
                ->setParameter('email', $_POST['email'])
                ->getQuery();
            $rows = $query->getArrayResult();

            if(count($rows) > 0 ){
                $user_type = "tpn-user";
            }else{

                // check tpn user in database
                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.email = :email AND p.s_enabled = 1')
                    ->setParameter('email', $_POST['email'])
                    ->getQuery();
                $rows = $query->getArrayResult();
                if(count($rows) > 0){
                    $user_type = "admin-user";
                }
            }

        }
            
            
            
            
        
        if($user_type == '' ){
            exit('email_invalid');
        }
        
        $pincode = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        $edit_url = '';
        $user_log_type = '';
        $area = '';
        if($user_type == 'admin-user' ){
            $model = $em->getRepository('AcmeCLSadminBundle:AdminUser')->findOneBy(array('email'=>$_POST['email']));
            $edit_url = $this->generateUrl('acme_cls_admin_staff_edit');
            $user_log_type = 'admin';
            $area = 'admin';
        }else if($user_type == 'tpn-user' ){
            $model = $em->getRepository('AcmeCLStpnBundle:TpnUser')->findOneBy(array('email'=>$_POST['email']));
            $edit_url = $this->generateUrl('acme_cls_admin_tpn_staff_edit');
            $user_log_type = 'dfat';
            $area = 'dfat';
        }else if($user_type == 'embassy-user' ){
            $model = $em->getRepository('AcmeCLSadminBundle:EmbassyUser')->findOneBy(array('email'=>$_POST['email']));
            $edit_url = $this->generateUrl('acme_cls_admin_embassy_edit');
            $user_log_type = 'embassy';
            $area = 'embassy';
        }else{
            $model = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('email'=>$_POST['email']));
            $edit_url = $this->generateUrl('acme_cls_admin_clients_edit');
            $user_log_type = $model->getType();
            $area = 'client';
        }
        $model->setResetPin($pincode);
        $em->persist($model);
        $em->flush();
        $em->getConnection()->commit(); 
        
        
        /* + create log */
        $logDetails = "<a href='". $edit_url ."?id=". $model->getId() ."' target='_blank'>". $model->getFname() . " " . $model->getLname() . "</a> requested a password change.";
        $this->createLog($area, $model->getId(), $user_log_type, $logDetails);
        /* - create log */
        
        /** start: send notice */
        $this->sendEmail(
                    $_POST['email'], 
                    'info@capitallinkservices.com.au',
                    "CLS - Reset password", 
                    $this->renderView(
                        'AcmeCLShomeBundle:ResetPassword:email.html.twig',
                        array('fullname' => $rows[0]['fname'],
                            'page_title'=>'CLS',
                            'email_to'=>$_POST['email'],
                            'pincode'=>$pincode,
                            'request_uri'=>$this->getRequest()->getUriForPath('/cls/password/resetform'),
                            'domain'=>$mod->siteURL()
                            )
                    )
                );
        
        
        /** end: send notice */
        return new response('success');
    }
    
    
    
    public function resetFormAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('user_type') != ''){
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('acme_cls_admin_home'));
            }else if($session->get('user_type') == 'tpn-user'){
                return $this->redirect($this->generateUrl('acme_cls_tpn_home'));
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_home'));
            }
        }
        
        $mod = new Model\GlobalModel();
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $error_count = 0;
        if(!isset($_GET['em']) || !isset($_GET['rp'])){
             $error_count += 1;
        }else if(!$mod->isEmailValid($_GET['em'])){
            $error_count += 1;
        }else{
            
            
            $user_type = '';
            // check government user
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.s_enabled = 1 AND p.reset_pin = :reset_pin')
                ->setParameter('email', $_GET['em'])
                ->setParameter('reset_pin', $_GET['rp'])
                ->getQuery();
            $rows = $query->getArrayResult();

            if(count($rows) > 0 ){
                $user_type = $rows[0]['type'];
            }else{

                // check tpn user in database
                $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.email = :email AND p.s_enabled = 1 AND p.reset_pin = :reset_pin')
                    ->setParameter('email', $_GET['em'])
                    ->setParameter('reset_pin', $_GET['rp'])
                    ->getQuery();
                $rows = $query->getArrayResult();

                if(count($rows) > 0 ){
                    $user_type = "tpn-user";
                }else{

                    // check tpn user in database
                    $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
                    $query = $cust->createQueryBuilder('p')
                        ->where('p.email = :email AND p.s_enabled = 1 AND p.reset_pin = :reset_pin')
                        ->setParameter('email', $_GET['em'])
                        ->setParameter('reset_pin', $_GET['rp'])
                        ->getQuery();
                    $rows = $query->getArrayResult();
                    
                    if(count($rows) > 0){
                        $user_type = "admin-user";
                    }
                }

            }
            
            
            if($user_type == ''){
                $error_count += 1;
            }
        }
        
        
        if($error_count == 0 ){
            return $this->render('AcmeCLShomeBundle:ResetPassword:resetform.html.twig',
                    array('email'=>$_GET['em'],
                        'reset_pin'=>$_GET['rp'],
                        'countries'=> $this->getCountries()
                        )
                    );
        }else{
            return $this->render('AcmeCLShomeBundle:ResetPassword:error.html.twig',
                    array('countries'=> $this->getCountries())
                    );
        }
    }
    
    
    
    
    
    
    
    
    
    public function resetSubmitAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('user_type') != ''){
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('acme_cls_admin_home'));
            }else if($session->get('user_type') == 'tpn-user'){
                return $this->redirect($this->generateUrl('acme_cls_tpn_home'));
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_home'));
            }
        }
        $mod = new Model\GlobalModel();
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $error_count = 0;
        $error_msg = "";
        $user_type = '';
        if(!isset($_POST['email']) && !isset($_POST['reset_pin']) && !isset($_POST['password']) && !isset($_POST['confirm_password'])){
             $error_count += 1;
             $error_msg = "Invalid data posted.";
        }else if(trim($_POST['password']) == ''){
            $error_count += 1;
            $error_msg = "Password is required.";
        }else if(trim($_POST['confirm_password']) == ''){
            $error_count += 1;
            $error_msg = "Please confirm your password.";
        }else if(trim($_POST['password']) != trim($_POST['confirm_password'])){
            $error_count += 1;
            $error_msg = "Please confirm your password.";
        }else if(!$mod->isEmailValid($_POST['email'])){
            $error_count += 1;
            $error_msg = "Invalid email address.";
        }else{
            
            // check government user
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.s_enabled = 1 AND p.reset_pin = :reset_pin')
                ->setParameter('email', $_POST['email'])
                ->setParameter('reset_pin', $_POST['reset_pin'])
                ->getQuery();
            $rows = $query->getArrayResult();

            if(count($rows) > 0 ){
                $user_type = $rows[0]['type'];
            }else{

                // check tpn user in database
                $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.email = :email AND p.s_enabled = 1 AND p.reset_pin = :reset_pin')
                    ->setParameter('email', $_POST['email'])
                    ->setParameter('reset_pin', $_POST['reset_pin'])
                    ->getQuery();
                $rows = $query->getArrayResult();

                if(count($rows) > 0 ){
                    $user_type = "tpn-user";
                }else{

                    // check tpn user in database
                    $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
                    $query = $cust->createQueryBuilder('p')
                        ->where('p.email = :email AND p.s_enabled = 1 AND p.reset_pin = :reset_pin')
                        ->setParameter('email', $_POST['email'])
                        ->setParameter('reset_pin', $_POST['reset_pin'])
                        ->getQuery();
                    $rows = $query->getArrayResult();
                    if(count($rows) > 0){
                        $user_type = "admin-user";
                    }else{
                        // check embassy user in database
                        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:EmbassyUser');
                        $query = $cust->createQueryBuilder('p')
                            ->where('p.email = :email AND p.s_enabled = 1 AND p.reset_pin = :reset_pin')
                            ->setParameter('email', $_POST['email'])
                            ->setParameter('reset_pin', $_POST['reset_pin'])
                            ->getQuery();
                        $rows = $query->getArrayResult();
                        if(count($rows) > 0){
                            $user_type = "embassy-user";
                        }
                    }
                }

            }
            
            
            
            
            if($user_type == ''){
                $error_count += 1;
                $error_msg = "Invalid data posted.";
            }
            
        }
        
        if($error_count == 0 ){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $edit_url = '';
            $user_log_type = '';
            $area = '';
            if($user_type == 'admin-user' ){
                $model = $em->getRepository('AcmeCLSadminBundle:AdminUser')->findOneBy(array('email'=>$_POST['email']));
                $edit_url = $this->generateUrl('acme_cls_admin_staff_edit');
                $user_log_type = 'admin';
                $area = 'admin';
            }else if($user_type == 'tpn-user' ){
                $model = $em->getRepository('AcmeCLStpnBundle:TpnUser')->findOneBy(array('email'=>$_POST['email']));
                $edit_url = $this->generateUrl('acme_cls_admin_tpn_staff_edit');
                $user_log_type = 'dfat';
                $area = 'dfat';
            }else if($user_type == 'embassy-user' ){
                $model = $em->getRepository('AcmeCLSadminBundle:EmbassyUser')->findOneBy(array('email'=>$_POST['email']));
                $edit_url = $this->generateUrl('acme_cls_admin_embassy_edit');
                $user_log_type = 'embassy';
                $area = 'embassy';
            }else{
                $model = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('email'=>$_POST['email']));
                $edit_url = $this->generateUrl('acme_cls_admin_clients_edit');
                $user_log_type = $model->getType();
                $area = 'client';
            }
            $model->setPassword($mod->passGenerator($_POST['password']));
            $model->setResetPin("");
            $em->persist($model);
            $em->flush();
            $em->getConnection()->commit(); 
                
            
            $session->set('user_no_reset',$model->getId());
            $session->set('user_type',$user_type);
            
            /* + create log */
            $logDetails = "<a href='". $edit_url ."?id=". $model->getId() ."' target='_blank'>". $model->getFname() . " " . $model->getLname() . "</a> had a successful password reset.";
            $this->createLog($area, $model->getId(), $user_log_type, $logDetails);
            /* - create log */
            
            return $this->redirect($this->generateUrl('acme_cls_password_reset_submit_success')); 
        }else{
            return $this->render('AcmeCLShomeBundle:ResetPassword:resetform.html.twig',
                    array('email'=>$_POST['email'],
                        'reset_pin'=>$_POST['reset_pin'],
                        'password'=>$_POST['password'],
                        'confirm_password'=>$_POST['confirm_password'],
                        'countries'=> $this->getCountries(),
                        'error'=>$error_msg)
                    );
        }
        
        
    }
    
    public function setSessionsAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        
        
        if($session->get('user_type') == 'tpn-user'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id', $session->get('user_no_reset'))
                ->getQuery();
            $rows = $query->getArrayResult();

            $session->set('tpn_user_id', $rows[0]['id']); 
            $session->set('user_type', "tpn-user");
            $session->set('tpn_user_email', $rows[0]['email']); 
            $session->set('tpn_user_fname', $rows[0]['fname']); 
            $session->set('tpn_user_lname', $rows[0]['lname']);

        }elseif($session->get('user_type') == 'admin-user'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id', $session->get('user_no_reset'))
                ->getQuery();
            $rows = $query->getArrayResult();
            
            $session->set('admin_id', $rows[0]['id']); 
            $session->set('user_type', "admin-user");
            $session->set('admin_email', $rows[0]['email']); 
            $session->set('admin_fname', $rows[0]['fname']); 
            $session->set('admin_lname', $rows[0]['lname']);

            // last login
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $model = $em->getRepository('AcmeCLSadminBundle:AdminUser')->findOneBy(array('id'=>$rows[0]['id']));
            $model->setLastLogin($datetime->format('Y-m-d H:i:s'));
            $em->persist($model);
            $em->flush();
            $em->getConnection()->commit(); 

        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id', $session->get('user_no_reset'))
                ->getQuery();
            $rows = $query->getArrayResult();
            
            $session->set('client_id', $rows[0]['id']); 
            $session->set('user_type', $rows[0]['type']);
            $session->set('email', $rows[0]['email']); 
            $session->set('fname', $rows[0]['fname']); 
            $session->set('lname', $rows[0]['lname']);


        }
        
        
        
        
        
        
        
        
        return new Response('success');
    }
    
}
