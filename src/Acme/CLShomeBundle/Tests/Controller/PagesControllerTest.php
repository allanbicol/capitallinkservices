<?php

namespace Acme\CLShomeBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PagesControllerTest extends WebTestCase
{
    public function testGovernment()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/government');
    }

    public function testTravelvisas()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/travel-visas');
    }

    public function testTravelagents()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/travel-agents');
    }

    public function testCorporate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/corporate');
    }

    public function testAllservices()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'all-services');
    }

    public function testAboutcls()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/about-cls');
    }

    public function testContact()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact');
    }

}
