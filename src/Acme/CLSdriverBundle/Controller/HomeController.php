<?php

namespace Acme\CLSdriverBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;   

use Acme\CLSdriverBundle\Entity\ScanGroup;

class HomeController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_driver_login')); 
        }
        $order_num = '';
        if(isset($_POST['order_no'])){
            $order_num= $_POST['order_no'];
        }
         return $this->render('AcmeCLSdriverBundle:Home:index.html.twig',array('data'=>$this->orderlist($order_num),'order_no'=> $order_num));
    }
    
    public function orderlist($order_num){
       // $json = new Model\Json;
        if($order_num==''){
            $order_num='0';
        }
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT  o.order_no, o.status,o.order_type, c.country_name AS destination, o.primary_traveller_name AS trav_name,d.name AS organisation,od.visa_date_submitted_for_processing AS processing,o.police_clearance_date_submitted_for_processing as processing1,od.signature,o.signature as signature1,od.sig_name,o.sig_name as sig_name1,o.sender_name,o.sender_signature
            FROM tbl_orders o
            LEFT JOIN tbl_order_destinations od ON o.order_no = od.order_no
            LEFT JOIN tbl_countries c ON c.id = od.country_id
            LEFT JOIN tbl_departments d ON o.pri_dept_contact_department_id = d.id
            WHERE o.status >= 10 and (o.order_no='".$order_num."' or o.primary_traveller_name like '%".$order_num."%') GROUP BY o.order_no");
        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }
    
    public function grouplist($user_id){
       // $json = new Model\Json;
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT  o.order_no,t.primary_traveller_name
            FROM tbl_scan_group o
            LEFT JOIN tbl_orders t ON t.order_no = o.order_no
            WHERE o.user_id='".$user_id."'");
        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }
    
    public function detailsAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_driver_login')); 
        }
         if(isset($_GET['order_no'])){
             $order_num=$_GET['order_no'];
             $post = array(
                'order_no'=>$order_num
                 );
             
         }else{
             die();
         }
         $data = $this->orderlist($order_num);
         
        $process_date='';
        $signature = '';
        $sig_name = '';
        $sender_name = '';
        $sender_signature = '';
        if($data[0]['order_type']==7 || $data[0]['order_type']==9){
           $process_date=$data[0]['processing1'];
           $signature = $data[0]['signature1'];
           $sig_name = $data[0]['sig_name1'];
           $sender_name = $data[0]['sender_name'];
           $sender_signature = $data[0]['sender_signature'];
        }else{
           $process_date=$data[0]['processing'];
           $signature = $data[0]['signature'];
           $sig_name = $data[0]['sig_name'];
        }
         $post = array(
                'order_no'=>$order_num,
                'date_process'=>$process_date,
                 'signature'=>$signature,
                 'name'=>$sig_name,
                 'sender_name'=>$sender_name,
                 'sender_signature'=>$sender_signature
                 ); 
         
         if($data[0]['order_type']==7){
            if($signature!='' && $sender_signature!=''){
                return $this->render('AcmeCLSdriverBundle:Home:complete.html.twig',array('data'=>$this->orderlist($order_num),'post'=>$post));
            }else{
               return $this->render('AcmeCLSdriverBundle:Home:details.html.twig',array('data'=>$this->orderlist($order_num),'post'=>$post));
            }
         }else{
             if($signature!=''){
                return $this->render('AcmeCLSdriverBundle:Home:complete.html.twig',array('data'=>$this->orderlist($order_num),'post'=>$post));
            }else{
               return $this->render('AcmeCLSdriverBundle:Home:details.html.twig',array('data'=>$this->orderlist($order_num),'post'=>$post));
            }
         }
    }

    
    public function updateDetailsAction(){
        $session = $this->getRequest()->getSession();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        $mod = new Model\GlobalModel();
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_driver_login')); 
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        if(isset($_POST['output'])){
             
             $sig = $_POST['output'];
             $output = filter_input(INPUT_POST, 'output', FILTER_UNSAFE_RAW);
             if(isset($_POST['sender_output'])){
                $sender_sig = $_POST['sender_output'];
                $sender_output = filter_input(INPUT_POST, 'sender_output', FILTER_UNSAFE_RAW);
             }
             if (!json_decode($output)) {
                $errors['output'] = true;
              }
             $sigHash = sha1($output);
             
             if($_POST['order_type']==7 || $_POST['order_type']==9){
                $od = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST['order_no']));
                $od->setPoliceClearanceDateSubmittedForProcessing($datetime->format("Y-m-d H:i:s"));
                $od->setSignature($output);
                $od->setSigName($_POST['name']); 
                if(isset($_POST['sender_output'])){
                    $od->setSenderSignature($sender_output);
                    $od->setSenderName($_POST['sender_name']);
                    if( $od->getSenderSignedDatetime() == '' ){
                        $od->setSenderSignedDatetime($datetime->format("Y-m-d H:i:s"));
                    }
                }
             }else{
                $od = $em->getRepository('AcmeCLSclientGovBundle:OrderDestinations')->findOneBy(array('order_no'=>$_POST['order_no']));
                $od->setVisaDateSubmittedForProcessing($datetime->format("Y-m-d H:i:s"));
                $od->setSignature($output);
                $od->setSigHash($sigHash);
                $od->setSigName($_POST['name']);
             }
             
            $em->persist($od);
            $em->flush();
            $em->getConnection()->commit(); 
            
            
             if(isset($_POST['sender_output'])){
             $post = array(
                'order_no'=>$_POST['order_no'],
                'date_process'=>$datetime->format("Y-m-d H:i:s"),
                 'signature'=>$output,
                 'name'=>$_POST['name'],
                 'sender_name'=>$_POST['sender_name'],
                 'sender_signature'=>$sender_output
                 );
             }else{
                 $post = array(
                'order_no'=>$_POST['order_no'],
                'date_process'=>$datetime->format("Y-m-d H:i:s"),
                 'signature'=>$output,
                 'name'=>$_POST['name']
                 );
             }
             
             //send email
             if($_POST['order_type']==7){
                if(trim($output) != ''){
                    $details = $this->getOrderDetailsByOrderNo($_POST['order_no']);
                    $to = array();
                    $to[] = $details['doc_pickup_email'];
                    $to[] = $details['doc_delivery_primary_receipient_email'];
                    //$to[] = "leo@voodoocreative.com.au";
                    $cc = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au', 'leo@voodoocreative.com.au');
                    //$cc = array('leokarl2108@gmail.com');
                    
                    $body = $this->renderView('AcmeCLSdriverBundle:Home:received_email_report.html.twig',
                        array(
                            'site_url' => $mod->siteURL(),
                            'order_details' => $details
                        ));
                    
                    $this->driverSendEmail($to, $cc, "help@capitallinkservices.com.au", "CLS DOCUMENT DELIVERY -- ORDER SUMMARY", $body);
                }
             }
        }
        if($_POST['order_type']==7){
            return $this->redirect($this->generateURL('acme_cls_driver_order_details').'?order_no='.$_POST['order_no']);
        }else{
            return $this->render('AcmeCLSdriverBundle:Home:complete.html.twig',array('data'=>$this->orderlist($_POST['order_no']),'post'=>$post));
    }   }
    
    public function updateListAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_driver_login')); 
        }
        
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        if(isset($_POST['output'])){
             
             $sig = $_POST['output'];
             $output = filter_input(INPUT_POST, 'output', FILTER_UNSAFE_RAW);
             if (!json_decode($output)) {
                $errors['output'] = true;
              }
             $sigHash = sha1($output);
             
             $list = $this->grouplist($session->get('admin_id'));
             
             for($i=0;$i< count($list);$i++){
                 
                 $details = $this->orderlist($list[$i]['order_no']);
                 $em = $this->getDoctrine()->getManager();
                 $em->getConnection()->beginTransaction();
        
                 if($details[0]['order_type']==7 || $details[0]['order_type']==9){
                    $od = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$list[$i]['order_no']));
                    $od->setPoliceClearanceDateSubmittedForProcessing($datetime->format("Y-m-d H:i:s"));
                    $od->setSignature($output);
                    $od->setSigName($_POST['name']); 
                 }else{
                    $od = $em->getRepository('AcmeCLSclientGovBundle:OrderDestinations')->findOneBy(array('order_no'=>$list[$i]['order_no']));
                    $od->setVisaDateSubmittedForProcessing($datetime->format("Y-m-d H:i:s"));
                    $od->setSignature($output);
                    $od->setSigHash($sigHash);
                    $od->setSigName($_POST['name']);
                 }

                 $em->persist($od);
                 $em->flush();
                 $em->getConnection()->commit(); 
             }
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('DELETE AcmeCLSdriverBundle:ScanGroup t WHERE t.user_id = '.$session->get('admin_id'));
            $query->execute(); 
             
        }

        return $this->render('AcmeCLSdriverBundle:Home:group_complete.html.twig');
    }
    
    public function groupAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_driver_login')); 
        }
        $order_num = '';
        if(isset($_POST['order_no'])){
            $order_num= $_POST['order_no'];
        }
         return $this->render('AcmeCLSdriverBundle:Home:group.html.twig',array('data'=>$this->orderlist($order_num),'order_no'=> $order_num,'order_group'=>$this->grouplist($session->get('admin_id'))));
    }
    
    
    public function addToGroupListAction(){
        if (isset($_POST['order_num'])){
            $session = $this->getRequest()->getSession();
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = new ScanGroup();
            $model->setOrderNo($_POST['order_num']);
            $model->setUserId($session->get('admin_id'));
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
                
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                return new Response('success');
            }else{
                return new Response('error');
            }
        }
        
    }
    
    public function groupdetailsAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_driver_login')); 
        }
        
         $data = $this->grouplist($session->get('admin_id'));
         
        $process_date='';
        $signature = '';
        $sig_name = '';

             
         if($signature!=''){
             
             $post = array(
                'order_no'=>$order_num,
                'date_process'=>$process_date,
                 'signature'=>$signature,
                 'name'=>$sig_name
                 );
             return $this->render('AcmeCLSdriverBundle:Home:complete.html.twig',array('data'=>$this->orderlist($order_num),'post'=>$post));
         }else{
            return $this->render('AcmeCLSdriverBundle:Home:group_details.html.twig',array('data'=>$this->grouplist($session->get('admin_id'))));
         }
    }
    
    public function removeListAction(){
      if(isset($_GET['order_no'])){
          $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('DELETE AcmeCLSdriverBundle:ScanGroup t WHERE t.order_no = '.$_GET['order_no']);
            $query->execute(); 
            
            return $this->redirect($this->generateUrl('acme_cls_driver_group')); 
     
        }
    }
    
    public function driverSendEmail($to, $cc, $from, $subject, $body, $attachment = NULL){
            
        if($cc != ''){
                $message = \Swift_Message::newInstance()
                ->setEncoder(\Swift_Encoding::get8BitEncoding())
                ->setSubject($subject)
                ->setFrom($from,'Capital Link Services')
                ->setTo($to)
                ->setCc($cc)
                ->setBody($body)
                ->setContentType("text/html")
            ;
        }else{

            $message = \Swift_Message::newInstance()
                ->setEncoder(\Swift_Encoding::get8BitEncoding())
                ->setSubject($subject)
                ->setFrom($from,'Capital Link Services')
                ->setTo($to)
                ->setBody($body)
                ->setContentType("text/html")
            ;
        }
        
        if($attachment != NULL){
            for($i=0; $i<count($attachment); $i++){
                if(!is_dir($attachment[$i])){
                    if(file_exists($attachment[$i])){
                        $message->attach(\Swift_Attachment::fromPath($attachment[$i]));
                    }
                }
            }
        }
        $this->get('mailer')->send($message);
    }
}