<?php

namespace Acme\CLSdriverBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeCLSdriverBundle:Default:index.html.twig', array('name' => $name));
    }
}
