<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class VisaInformationController extends GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-visa-information');
        
        if($session->get('email') == '' || $session->get('user_type') != 'government'){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSclientGovBundle:VisaInformation:index.html.twig',
                array('countries'=>$this->getCountries()));
    }
    
    
    public function getInfoAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-visa-information');
        
        if($session->get('email') == '' || $session->get('user_type') != 'government'){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_GET['id'] = filter_var($_GET['id'], FILTER_SANITIZE_STRING);
        
        $data = $this->getVisaDetailsByCountryId($_GET['id']);
        
        return $this->render('AcmeCLSclientGovBundle:VisaInformation:visaInformation.html.twig',
                array('data'=>$data));
    }

}
