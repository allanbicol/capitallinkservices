<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Model;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'login');
        
        $mod = new Model\GlobalModel();
        $error = 0;
        $message = array();
        
        if($session->get('email') != '' && $session->get('user_type') == 'government'){
            return $this->redirect($this->generateUrl('cls_client_gov_home'));
        }
        
        
        if(isset($_POST['hid_submit'])){
            $error =0;
            // Check username and password
            if(!isset($_POST['email']) && !isset($_POST['password'])){
                return $this->redirect($this->generateUrl('acme_cls_client_login'));
            }
            
            if(trim($_POST['email']) == ''){
                $error += 1;
                $message = "Email address should not be blank.";
            }
            
            // check email validity
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $error == 0){
                $error += 1;
                $message = "Email ".$_POST['email']." is not a valid email address.";
            }
            
            if(trim($_POST['password']) == '' && $error == 0){
                $error += 1;
                $message = "Password should not be blank.";
            }
            
            // check user in database
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.type = :type AND p.email = :email AND p.password = :password AND p.s_enabled = 1')
                ->setParameter('type', 'government')
                ->setParameter('email', $_POST['email'])
                ->setParameter('password', $mod->passGenerator($_POST['password']))
                ->getQuery();
            $rows = $query->getArrayResult();
        
            if(count($rows) < 1 && $error == 0){
                $error += 1;
                $message = "The login details you've entered don't match any in our system.";
            }
            
            if($error == 0){

                // set session
                $session->set('client_id', $rows[0]['id']); 
                $session->set('user_type', $rows[0]['type']);
                $session->set('email', $rows[0]['email']); 
                $session->set('fname', $rows[0]['fname']); 
                $session->set('lname', $rows[0]['lname']);
                
                

                return $this->redirect($this->generateUrl('cls_client_gov_home')); 
            }else{
                
                return $this->render('AcmeCLSclientGovBundle:Default:index.html.twig',
                        array('error'=>$message,
                            'email'=>$_POST['email']
                            )
                        );
            }
        
        }else{
            return $this->render('AcmeCLSclientGovBundle:Default:index.html.twig');
        }
    }
    
    /**
     * 
     * @todo logout and clear all session
     * @author leokarl
     */
    public function logoutAction(){
        $session = $this->getRequest()->getSession();
        $session->set('client_id',''); 
        $session->set('user_type',''); 
        $session->set('email', ''); 
        $session->set('fname', ''); 
        $session->set('lname', ''); 
        //$this->get('session')->clear(); // clear all sessions
        
        return $this->redirect($this->generateUrl('acme_cls_client_login')); // redirect to login page
        
    }
}
