<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class PoliceClearancesInformationController extends GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-visa-information');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        
        
        return $this->render('AcmeCLSclientGovBundle:PoliceClearancesInformation:index.html.twig',
                array('clearances'=>$this->getPoliceClearances()));
        
    }
    
    
    
    public function getInfoAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_POST['clearance_id'] = filter_var($_POST['clearance_id'], FILTER_SANITIZE_STRING);
        
        $data = $this->getPoliceClearanceInfoById($_POST['clearance_id']);
        
        return new Response(json_encode($data));
    }

}
