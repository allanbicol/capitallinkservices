<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TravelAlertsController extends GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        $session->set('page_name', 'travel-alerts');
        return $this->render('AcmeCLSclientGovBundle:TravelAlerts:index.html.twig',
                array('post'=> $this->getTravelAlerts())
                );
    }

}
