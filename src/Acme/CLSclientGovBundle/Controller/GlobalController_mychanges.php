<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSadminBundle\Entity\Logs;

class GlobalController extends Controller
{
    /**
     * GLOBAL ACTIONS
     */
    
    /**
     * Return client's details
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getClientDetailsByIdAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') == 'admin-user'){
            $_POST['client'] = intval($_POST['client']);
            $data = $this->getClientDetailsById($_POST['client']);
        }else{
            $data = $this->getClientDetailsById($session->get("client_id"));
        }
        return new Response(json_encode($data));
    }
    
    
    public function getDiscountAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('email') != '' && isset($_POST["code"])){
            
            $_POST['code'] = filter_var($_POST['code'], FILTER_SANITIZE_STRING);
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('p')
                ->where('p.code = :code')
                ->setParameter('code', $_POST["code"])
                ->getQuery();
            $data = $query->getArrayResult();
            
            if(count($data) > 0){
                return new Response($data[0]["rate"]);
                
            }else{
                
                return new Response(0);
            }
            
        }else{
            return new Response("session expired");
        }
    }
    
    /**
     * GLOBAL FUNCTIONS
     */
    
    /**
     * return client details
     * @param type $id
     * @return type
     */
    public function getClientDetailsById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.type, p.title, p.fname, p.lname, p.email, p.phone, p.mobile, 
                    p.department_id, p.address, p.city, p.state, p.postcode, p.country_id,
                    p.mdda_address, p.mdda_city, p.mdda_state, p.mdda_postcode, p.mdda_country_id, 
                    p.mba_address, p.mba_city, p.mba_state, p.mba_postcode, p.mba_country_id,
                    p.account_no, p.can_charge_cost_to_account, p.can_get_special_price,
                    p.special_price, p.company")
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery();
        $data = $query->getArrayResult();
        return $data[0];
    }
    
    public function getEmbassyClientDetailsById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:EmbassyUser');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.country, c.countryName, p.title, p.fname, p.lname, p.email, p.phone, p.mobile")
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.country')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery();
        $data = $query->getArrayResult();
        return $data[0];
    }
    
    /**
     * Description: get name titles
     * @return type
     */
    public function getNameTitles(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:NameTitle');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.title")
            ->orderBy("p.priority, p.title")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    
    public function getNameTitlesInJson(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:NameTitle');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.title")
            ->orderBy("p.priority, p.title")
            ->getQuery();
        $data = $query->getArrayResult();
        $titles = array();
        for($i=0; $i<count($data); $i++){
            $titles[] = array($data[$i]['id']=> $data[$i]['title']);
        }
        
        return json_encode($titles);
    }
    /**
     * Description: get name titles
     * @return type
     */
    public function getCountries($priorities = null){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Countries');
        if($priorities != null){
            $query = $cust->createQueryBuilder('p')
                ->orderBy("p.priority, p.countryName")
                ->getQuery();
        }else{
            $query = $cust->createQueryBuilder('p')
            ->orderBy("p.countryName")
            ->getQuery();
        }
        return $query->getArrayResult();
    }
    
    public function getCountriesJson(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Countries');
            $query = $cust->createQueryBuilder('p')
            ->orderBy("p.countryName")
            ->getQuery();
        $data = $query->getArrayResult();
        
        $items = array();
        for($i=0; $i<count($data); $i++){
            $items[] = array($data[$i]['id']=> $data[$i]['countryName']);
        }
        
        return json_encode($items);
    }
    
    
    public function getCountryNameById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Countries');
        $query = $cust->createQueryBuilder('c')
            ->where("c.id =:country_id")
            ->setParameter("country_id", $id)
            ->getQuery();
        $country = $query->getArrayResult();
        return (count($country) > 0) ? $country[0]['countryName'] : '';
    }
    
    /**
     * Description: get popular destinations
     * @return type
     */
    public function getPopularDestinations(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Countries');
        $query = $cust->createQueryBuilder('p')
            ->where('p.s_popular_destination > 0')
            ->orderBy("p.s_popular_destination")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    /**
     * Description: get popular destinations
     * @return type
     */
    public function getNotPopularDestinations(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Countries');
        $query = $cust->createQueryBuilder('p')
            ->where('p.s_popular_destination < 1 OR p.s_popular_destination IS NULL')
            ->orderBy("p.countryName")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    
    /**
     * Description: get police clearances
     * @return type
     */
    public function getPoliceClearances(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PoliceClearances');
        $query = $cust->createQueryBuilder('p')
            ->orderBy("p.name")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    
    /**
     * Description: get document delivery typs
     * @return type
     */
    public function getDocumentDeliveryTypes(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsDocumentDelivery');
        $query = $cust->createQueryBuilder('p')
            ->orderBy("p.type")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    /**
     * Description: get document delivery type by id
     * @return type
     */
    public function getDocumentDeliveryTypeById($doc_type_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsDocumentDelivery');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id =:doc_type_id")
            ->setParameter("doc_type_id", $doc_type_id)
            ->getQuery();
        return $query->getArrayResult();
    }
    
    /**
     * Description: get police clearances
     * @return type
     */
    public function getPoliceClearanceInfoById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PoliceClearances');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id = :id")
            ->setParameter("id", $id)
            ->getQuery();
        return $query->getArrayResult();
    }
    /**
     * Description: get visa details
     * @return type
     */
    public function getVisaDetailsByCountryId($country_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Countries');
        $query = $cust->createQueryBuilder('c')
            ->select("c.id as country_id, c.countryCode, c.countryName, c.rep_name, c.visa_information,
                c.gov_s_no_visa_required, c.gov_s_no_visa_required_html")
            ->where("c.id = :country_id")
            ->setParameter("country_id", $country_id)
            ->getQuery();
        $data = $query->getArrayResult();
        
        $post = array('country_id'=>$data[0]["country_id"],
                'country_code'=>$data[0]["countryCode"],
                'country_name'=>$data[0]["countryName"],
                'visa_information'=>$data[0]["visa_information"],
                'visa_types'=>$this->getVisaTypesByCountryId($country_id),
                'gov_s_no_visa_required'=>$data[0]["gov_s_no_visa_required"],
                'gov_s_no_visa_required_html'=>$data[0]["gov_s_no_visa_required_html"]
            );
        
        return $post;
    }
    
    /**
     * Description: get visa details
     * @return type
     */
    public function getPublicVisaDetailsByCountryId($country_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Countries');
        $query = $cust->createQueryBuilder('c')
            ->select("c.id as country_id, c.countryCode, c.countryName, c.rep_name, c.visa_information,
                c.public_s_no_visa_required, c.public_s_no_visa_required_html")
            ->where("c.id = :country_id")
            ->setParameter("country_id", $country_id)
            ->getQuery();
        $data = $query->getArrayResult();
        
        $post = array('country_id'=>$data[0]["country_id"],
                'country_code'=>$data[0]["countryCode"],
                'country_name'=>$data[0]["countryName"],
                'visa_information'=>$data[0]["visa_information"],
                'visa_types'=>$this->getPublicVisaTypesByCountryId($country_id),
                'public_s_no_visa_required'=>$data[0]["public_s_no_visa_required"],
                'public_s_no_visa_required_html'=>$data[0]["public_s_no_visa_required_html"]
            );
        
        return $post;
    }
    
    
    /**
     * Description: get russian visa voucher types
     * @return array
     */
    public function getRussianVisaVoucherTypes(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:RussianVisaVoucher');
        $query = $cust->createQueryBuilder('v')
            ->select("v.id, v.type, v.name, v.three_days_process_fee, v.one_two_days_process_fee,
                v.twelve_hrs_process_fee, v.thirteen_days, v.four_days, v.s_active")
            ->orderBy('v.type_order')
            ->getQuery();
        return $query->getArrayResult();
    }
    
    
    public function getVisaTypesByCountryId($country_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaTypes');
        $query = $cust->createQueryBuilder('v')
            ->where("v.country_id = :country_id")
            ->setParameter("country_id", $country_id)
            ->getQuery();
        $data = $query->getArrayResult();
        
        $post = array();
        for($i=0; $i<count($data); $i++){
            $post[] = array('id'=>$data[$i]['id'],
                'country_id'=>$data[$i]['country_id'],
                'type'=>$data[$i]['type'],
                'cost'=>$data[$i]['cost'],
                'file_attachment'=>$data[$i]['file_attachment'],
                'status'=>$data[$i]['status'],
                'additional_requirements'=>$this->getVisaTypeAdditionalRequirementsByVisaTypeId($data[$i]['id'])
                );
        }
        
        return $post;
    }
    
    public function getPublicVisaTypesByCountryId($country_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaTypes');
        $query = $cust->createQueryBuilder('v')
            ->where("v.country_id = :country_id")
            ->setParameter("country_id", $country_id)
            ->getQuery();
        $data = $query->getArrayResult();
        
        $post = array();
        for($i=0; $i<count($data); $i++){
            $post[] = array('id'=>$data[$i]['id'],
                'country_id'=>$data[$i]['country_id'],
                'type'=>$data[$i]['type'],
                'cost'=>$data[$i]['cost'],
                'file_attachment'=>$data[$i]['file_attachment'],
                'status'=>$data[$i]['status'],
                'visa_information'=>$data[$i]['visa_information'],
                'additional_requirements'=>$this->getPublicVisaTypeAdditionalRequirementsByVisaTypeId($data[$i]['id'])
                );
        }
        
        return $post;
    }
    
    public function getPublicVisaTypesByTypeId($type_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaTypes');
        $query = $cust->createQueryBuilder('v')
            ->where("v.id = :id")
            ->setParameter("id", $type_id)
            ->getQuery();
        $data = $query->getArrayResult();
        if(count($data) > 0){
        $post = array('id'=>$data[0]['id'],
            'country_id'=>$data[0]['country_id'],
            'type'=>$data[0]['type'],
            'cost'=>$data[0]['cost'],
            'file_attachment'=>$data[0]['file_attachment'],
            'status'=>$data[0]['status'],
            'visa_information'=>$data[0]['visa_information'],
            'additional_requirements'=>$this->getPublicVisaTypeAdditionalRequirementsByVisaTypeId($data[0]['id'])
            );
        }else{
            $post = array();
        }
        return $post;
    }
    
    public function getVisaTypeAdditionalRequirementsByVisaTypeId($visa_type_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaAdditionalRequirements');
        $query = $cust->createQueryBuilder('v')
            ->where("v.visa_id = :visa_id")
            ->setParameter("visa_id", $visa_type_id)
            ->orderBy("v.item_order", "asc")
            ->getQuery();
        $data = $query->getArrayResult();
        return $data;
    }
    
    public function getPublicVisaTypeAdditionalRequirementsByVisaTypeId($visa_type_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaAdditionalRequirements');
        $query = $cust->createQueryBuilder('v')
            ->where("v.visa_id = :visa_id")
            ->setParameter("visa_id", $visa_type_id)
            ->orderBy("v.item_order", "asc")
            ->getQuery();
        $data = $query->getArrayResult();
        return $data;
    }
    
    public function isVisaRequirementSelected($destination_id,$selected_visa_type, $req_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDestinations');
        $query = $cust->createQueryBuilder('od')
            ->where("od.id = :destination_id")
            ->setParameter("destination_id", $destination_id)
            ->getQuery();
        $data = $query->getArrayResult();
        
        if(count($data) > 0){
            $data = $data[0]["selected_visa_type_requirements"];

            $data = explode(";", $data);

            $is_checked = 0;
            for($i=0; $i<count($data); $i++){
                $req = explode(",",$data[$i]);
                $is_checked += ($req[0] == $req_id) ? 1 : 0;
            }
            return ($is_checked > 0) ? true : false;
        }else{
            return false;
        }
    }
    
    /**
     * Description: get departments
     * @return type
     */
    public function getDepartments(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Departments');
        $query = $cust->createQueryBuilder('p')
            ->orderBy("p.name")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getDepartmentNameById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Departments');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id = :id")
            ->setParameter("id", $id)
            ->getQuery();
        $data =  $query->getArrayResult();
        return (count($data) > 0) ? $data[0]['name'] : '';
    }
    
    /**
     * Description: get passport types
     * @return type
     */
    public function getPassportTypes(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:PassportTypes');
        $query = $cust->createQueryBuilder('p')
            ->orderBy("p.type")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getPassportTypeNameById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:PassportTypes');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id = :id")
            ->setParameter("id", $id)
            ->getQuery();
        $data =  $query->getArrayResult();
        return (count($data) > 0) ? $data[0]['type'] : '';
    }
    
    /**
     * Description: get card types
     * @return type
     */
    public function getCardTypes(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:CardTypes');
        $query = $cust->createQueryBuilder('p')
            ->orderBy("p.name")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    /**
     * Description: get card types
     * @return type
     */
    public function getCourierOptions(){
        // updated by leokarl 5-11-2015
        $session = $this->getRequest()->getSession();
        if($session->get('user_type') == 'government'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaCourierOptions');
            $query = $cust->createQueryBuilder('p')
                ->where("p.s_available_for_gov = 1")
                ->orderBy("p.type")
                ->getQuery();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaCourierOptions');
            $query = $cust->createQueryBuilder('p')
                ->where("p.s_available_for_gov = 0")
                ->orderBy("p.type")
                ->getQuery();
        }
        return $query->getArrayResult();
    }
    
    public function getCourierOptionsJson(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaCourierOptions');
        $query = $cust->createQueryBuilder('p')
            ->orderBy("p.type")
            ->getQuery();
        $data = $query->getArrayResult();
        
        $items = array();
        for($i=0; $i<count($data); $i++){
            $items[] = array($data[$i]['id']=> $data[$i]['type'] . " - $" . $data[$i]['cost']);
        }
        
        return json_encode($items);
    }
    
    public function getCourierOptionById($id){
        // updated by leokarl 5-11-2015
        $session = $this->getRequest()->getSession();
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaCourierOptions');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id = :id")
            ->setParameter("id", $id)
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getVisaTickets(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDestinations');
            $query = $cust->createQueryBuilder('od')
            ->select("o.date_submitted, d.name as department, p.account_no, o.order_no, o.status, c.countryName,
                o.date_completed, o.primary_traveller_name, od.departure_date, p.s_paid, o.visa_cls_team_member,
                od.id")
            ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = od.order_no')
            ->leftJoin('AcmeCLSclientGovBundle:Payment', 'p', 'WITH', 'p.order_no = o.order_no')
            ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = od.country_id')
            ->where("o.status >= 10")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getVisaTicketsByClientId($client_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
            ->select("o.date_submitted, o.date_last_saved, d.name as department, p.account_no, o.order_no, o.order_type, o.status, 
                o.date_completed, o.primary_traveller_name, p.s_paid, o.visa_cls_team_member, o.client_id")
            ->leftJoin('AcmeCLSclientGovBundle:Payment', 'p', 'WITH', 'p.order_no = o.order_no')
            ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
            ->where("o.client_id = :client_id")
            ->setParameter("client_id", $client_id)
            ->orderBy("o.date_last_saved", "DESC")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getOrderDlQuotesByOrderNo($order_no, $group = null){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:OrderDlQuotes');
        if($group == null){
            $query = $cust->createQueryBuilder('oq')
            ->where("oq.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->getQuery();
        }else{
            $query = $cust->createQueryBuilder('oq')
            ->where("oq.order_no = :order_no AND oq.sent_group = :group")
            ->setParameter("order_no", $order_no)
            ->setParameter("group", $group)
            ->getQuery();
        }
        return $query->getArrayResult();
    }
    
    public function getOrderDlQuotesBySentGroupByOrderNo($order_no){
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $order_no = intval($order_no);
        $statement = $connection->prepare("SELECT order_no, sent_group, sent_date FROM tbl_order_dl_quotes WHERE order_no=".$order_no." GROUP BY sent_group");
        $statement->execute();
        $rows = $statement->fetchAll();
        
        $post = array();
        for($i=0; $i<count($rows); $i++){
            $post[] = array('sent_group'=>$rows[$i]['sent_group'],
                    'order_no'=>$rows[$i]['order_no'],
                    'sent_date'=>$rows[$i]['sent_date'],
                    'quotes'=> $this->getOrderDlQuotesByOrderNo($order_no, $rows[$i]['sent_group'])
                );
        }
        return $post;
    }
    
    public function getOrderDlQuotesLatestGroupByOrderNo($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:OrderDlQuotes');
        $query = $cust->createQueryBuilder('oq')
            ->where("oq.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->orderBy('oq.sent_group', 'DESC')
            ->setMaxResults(1)
            ->getQuery();
        
        $result= $query->getArrayResult();
        return (count($result) > 0) ? $result[0]['sent_group'] : 0;
    }
    
    
    public function generateOrderDlQuotesGroupByOrderNo($order_no){
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT sent_group FROM tbl_order_dl_quotes WHERE order_no = ".$order_no." ORDER BY sent_group DESC LIMIT 1");
        $statement->execute();
        $rows = $statement->fetchAll();

        return (isset($rows[0]['sent_group'])) ? $rows[0]['sent_group'] + 1 : 1;
    }
    
    /**
     * Return order details
     * @return type
     */
    public function getOrderDetailsByOrderNoAndClientId($order_no, $client_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
        $query = $cust->createQueryBuilder('p')
            ->select("p.order_no, p.client_id, p.date_last_saved, p.date_submitted, p.order_type, p.primary_traveller_name, p.destination,
                p.departure_date, p.pri_dept_contact_department_id, d.name as department_name, p.pri_dept_contact_fname,
                p.pri_dept_contact_lname, p.pri_dept_contact_email, p.pri_dept_contact_phone, p.status, p.discount_code, 
                p.discount_rate, p.tpn_qty, p.tpn_price, p.tpn_additional_qty, p.tpn_additional_price, p.grand_total,
                pay.date_paid, pay.fname as payer_fname, pay.lname as payer_lname, pay.email as payer_email, pay.phone as payer_phone, pay.mobile as payer_mobile,
                pay.address as payer_address, pay.city as payer_city, pay.state as payer_state, pay.postcode as payer_postcode,
                pay.mba_address, pay.mba_city, pay.mba_state, pay.mba_postcode, c.countryName as mba_country, pay.account_no, u.can_get_special_price, u.special_price, u.can_charge_cost_to_account,
                p.visa_courier, p.visa_mdd_company, p.visa_mdd_address, p.visa_mdd_city, p.visa_mdd_state, p.visa_mdd_postcode, p.visa_mdd_fname, p.visa_mdd_lname, p.visa_mdd_contact, p.visa_additional_comment,
                p.visa_courier_price, vco.type as selected_visa_courier, u.account_no as user_account_no,
                p.passport_office_booking_no, p.passport_office_booking_time, p.passport_office_booking_time_hr, p.passport_office_booking_time_min,
                p.s_doc_sent, p.date_doc_sent, p.date_completed, p.is_smart_traveller, u.type as client_type, u.fname as client_fname, u.lname as client_lname, pay.s_paid,
                
                p.doc_receiver_name, p.doc_pickup_address, p.doc_pickup_city, p.doc_pickup_postcode, p.doc_pickup_contact_no, p.doc_pickup_contact_area, p.doc_pickup_contact_area_alt1, p.doc_pickup_contact_area_alt2, p.doc_delivery_recipient_name, p.doc_delivery_recipient_name_alt1, p.doc_delivery_recipient_name_alt2, p.doc_delivery_company, p.doc_delivery_company_alt1, p.doc_delivery_company_alt2,
                p.doc_delivery_address, p.doc_delivery_address_alt1, p.doc_delivery_address_alt2, p.doc_delivery_city, p.doc_delivery_city_alt1, p.doc_delivery_city_alt2, p.doc_delivery_postcode, p.doc_delivery_postcode_alt1, p.doc_delivery_postcode_alt2, p.doc_delivery_contact_no, p.doc_delivery_contact_no_alt1, p.doc_delivery_contact_no_alt2, p.doc_package_total_pieces, p.doc_delivery_security_no,
                p.doc_package_pickup_date, p.doc_package_ready_hr, p.doc_package_ready_min, p.doc_package_ready_ampm, p.doc_package_office_close_hr, p.doc_package_office_close_min, p.doc_package_office_close_ampm,
                p.doc_delivery_type, dd.type as doc_delivery_type_name, dd.cost as doc_delivery_cost, p.police_clearance_id, pc.name as police_clearance_name, pc.price as police_clearance_price,
                pc.name_additional as police_clearance_name_additional, pc.price_additional as police_clearance_additional_price, pc.file_path as police_clearance_file,
                p.police_clearance_date_cls_received_all_items, p.police_clearance_date_submitted_for_processing, p.police_clearance_date_completed_and_received_at_cls,
                p.police_clearance_date_order_on_route_and_closed, pay.payment_option,
                p.rvv_first_entry_date, p.rvv_first_departure_date, p.rvv_second_entry_date, p.rvv_second_departure_date, p.rvv_list_of_cities, p.rvv_list_of_hotels,
                p.rvv_visa_applied_at, p.russian_visa_voucher_id, rvv.name as russian_visa_voucher_name, p.russian_visa_voucher_col_cost, p.russian_visa_voucher_col_no, p.rvv_file, p.rvv_comments,
                
                p.dl_company, p.dl_address, p.dl_city, p.dl_state, p.dl_postcode, p.dl_contact_name, p.dl_mobile, p.dl_email, p.dl_date_doc_returned, p.dl_embassy, cem.countryName as dl_embassy_name, p.dl_ref_no, p.dl_com_invoice_no, p.dl_payment_type, p.dl_nationality, 
                p.dl_visa_shipped_by, p.dl_visa_com_note_no, p.dl_visa_com_note_in, p.dl_visa_invoice_no,
                
                p.visa_courier_pickup_date, p.visa_courier_pickup_ready_by_time_hr, p.visa_courier_pickup_ready_by_time_min,
                p.visa_courier_pickup_close_time_hr, p.visa_courier_pickup_close_time_min, p.visa_courier_pickup_contact_person_name,
                p.visa_courier_pickup_contact_person_phone
                ")
            ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = p.pri_dept_contact_department_id')
            ->leftJoin('AcmeCLSclientGovBundle:Payment', 'pay', 'WITH', 'p.order_no = pay.order_no')
            ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = p.client_id')
            ->leftJoin('AcmeCLSadminBundle:VisaCourierOptions', 'vco', 'WITH', 'vco.id = p.visa_courier')
            ->leftJoin('AcmeCLSadminBundle:SettingsDocumentDelivery', 'dd', 'WITH', 'dd.id = p.doc_delivery_type')
            ->leftJoin('AcmeCLSadminBundle:PoliceClearances', 'pc', 'WITH', 'pc.id = p.police_clearance_id')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = pay.mba_country_id')
            ->leftJoin('AcmeCLSadminBundle:RussianVisaVoucher', 'rvv', 'WITH', 'rvv.id = p.russian_visa_voucher_id')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'cem', 'WITH', 'cem.id = p.dl_embassy')
            ->where("p.order_no = :order_no AND p.client_id = :client_id ")
            ->setParameter("order_no", $order_no)
            ->setParameter("client_id", $client_id)
            ->getQuery();
        $data = $query->getArrayResult();
        
        if(count($data) > 0){
            $post = array("travels"=>$this->getOrderDestinationsByOrderNo($order_no, $data[0]['client_type']),
                'tpns'=>$this->getOrderTpnsByOrderNo($order_no),
                'travellers' => $this->getOrderTravellersByOrderNo($order_no),
                'passport_applicants'=> $this->getOrderPassportApplicants($order_no),
                'police_clearance_applicants'=> $this->getOrderPoliceClearanceApplicants($order_no),
                'pri_dept_contact_department_id' => $data[0]['pri_dept_contact_department_id'],
                'pri_dept_contact_fname' => $data[0]['pri_dept_contact_fname'],
                'pri_dept_contact_lname' => $data[0]['pri_dept_contact_lname'],
                'pri_dept_contact_email' => $data[0]['pri_dept_contact_email'],
                'pri_dept_contact_phone' => $data[0]['pri_dept_contact_phone'],
                'department_name' =>$data[0]['department_name'],
                'status' => $data[0]['status'],
                'discount_code' => $data[0]['discount_code'],
                'discount_rate' => $data[0]['discount_rate'],
                'tpn_qty'=> $data[0]['tpn_qty'],
                'tpn_price'=> $data[0]['tpn_price'],
                'tpn_additional_qty'=> $data[0]['tpn_additional_qty'],
                'tpn_additional_price'=> $data[0]['tpn_additional_price'],
                'grand_total' => $data[0]['grand_total'],
                'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                'payment_option' => $data[0]['payment_option'],
                'date_last_saved' => $data[0]['date_last_saved'],
                'date_submitted' => $data[0]['date_submitted'],
                'order_notes'=> $this->getOrderNotesByOrderNo($order_no),
                'order_no' => $order_no,
                'order_type' => $data[0]['order_type'],
                'order_type_name' => $this->getOrderTypeName($data[0]['order_type']),
                's_paid'=> $data[0]['s_paid'],
                'date_paid' => $data[0]['date_paid'],
                'payer_fname' => $data[0]['payer_fname'],
                'payer_lname' => $data[0]['payer_lname'],
                'payer_email' => $data[0]['payer_email'],
                'payer_phone' => $data[0]['payer_phone'],
                'payer_mobile' => $data[0]['payer_mobile'],
                'payer_address' => $data[0]['payer_address'],
                'account_no' => $data[0]['account_no'],
                'user_account_no' => $data[0]['user_account_no'],
                'mba_address' => $data[0]['mba_address'],
                'mba_state' => $data[0]['mba_state'],
                'mba_city' => $data[0]['mba_city'],
                'mba_postcode' => $data[0]['mba_postcode'],
                'mba_country' => $data[0]['mba_country'],
                
                'client_id'=>$data[0]['client_id'],
                'client_fname'=> $data[0]['client_fname'],
                'client_lname'=> $data[0]['client_lname'],
                'can_get_special_price'=>$data[0]['can_get_special_price'],
                'special_price'=>$data[0]['special_price'],
                'can_charge_cost_to_account'=> $data[0]['can_charge_cost_to_account'],
                
                'visa_courier'=> $data[0]['visa_courier'],
                'selected_visa_courier'=> $data[0]['selected_visa_courier'],
                'visa_courier_price'=> $data[0]['visa_courier_price'],
                
                'visa_courier_pickup_date'=> $data[0]['visa_courier_pickup_date'],
                'visa_courier_pickup_ready_by_time_hr'=> $data[0]['visa_courier_pickup_ready_by_time_hr'],
                'visa_courier_pickup_ready_by_time_min'=> $data[0]['visa_courier_pickup_ready_by_time_min'],
                'visa_courier_pickup_close_time_hr'=> $data[0]['visa_courier_pickup_close_time_hr'],
                'visa_courier_pickup_close_time_min'=> $data[0]['visa_courier_pickup_close_time_min'],
                'visa_courier_pickup_contact_person_name'=> $data[0]['visa_courier_pickup_contact_person_name'],
                'visa_courier_pickup_contact_person_phone'=> $data[0]['visa_courier_pickup_contact_person_phone'],
                
                
                'visa_mdd_company'=> $data[0]['visa_mdd_company'],
                'visa_mdd_address'=> $data[0]['visa_mdd_address'],
                'visa_mdd_city'=> $data[0]['visa_mdd_city'],
                'visa_mdd_state'=> $data[0]['visa_mdd_state'],
                'visa_mdd_postcode'=> $data[0]['visa_mdd_postcode'],
                'visa_mdd_fname'=> $data[0]['visa_mdd_fname'],
                'visa_mdd_lname'=> $data[0]['visa_mdd_lname'],
                'visa_mdd_contact'=> $data[0]['visa_mdd_contact'],
                'visa_additional_comment'=> $data[0]['visa_additional_comment'],
                
                'passport_office_booking_no'=> $data[0]['passport_office_booking_no'],
                'passport_office_booking_time'=> $data[0]['passport_office_booking_time'],
                'passport_office_booking_time_hr'=> $data[0]['passport_office_booking_time_hr'],
                'passport_office_booking_time_min'=> $data[0]['passport_office_booking_time_min'],
                's_doc_sent'=> $data[0]['s_doc_sent'],
                'date_doc_sent'=> $data[0]['date_doc_sent'],
                'date_completed'=> $data[0]['date_completed'],
                'is_smart_traveller'=> $data[0]['is_smart_traveller'],
                
                'doc_receiver_name'=> $data[0]['doc_receiver_name'],
                'doc_pickup_address'=> $data[0]['doc_pickup_address'],
                'doc_pickup_city'=> $data[0]['doc_pickup_city'],
                'doc_pickup_postcode'=> $data[0]['doc_pickup_postcode'],
                'doc_pickup_contact_no'=> $data[0]['doc_pickup_contact_no'],
                
                'doc_pickup_contact_area'=> $data[0]['doc_pickup_contact_area'],
                'doc_delivery_company'=> $data[0]['doc_delivery_company'],
                'doc_delivery_recipient_name'=> $data[0]['doc_delivery_recipient_name'],
                'doc_delivery_address'=> $data[0]['doc_delivery_address'],
                'doc_delivery_city'=> $data[0]['doc_delivery_city'],
                'doc_delivery_postcode'=> $data[0]['doc_delivery_postcode'],
                'doc_delivery_contact_no'=> $data[0]['doc_delivery_contact_no'],
                
                'doc_pickup_contact_area_alt1'=> $data[0]['doc_pickup_contact_area_alt1'],
                'doc_delivery_company_alt1'=> $data[0]['doc_delivery_company_alt1'],
                'doc_delivery_recipient_name_alt1'=> $data[0]['doc_delivery_recipient_name_alt1'],
                'doc_delivery_address_alt1'=> $data[0]['doc_delivery_address_alt1'],
                'doc_delivery_city_alt1'=> $data[0]['doc_delivery_city_alt1'],
                'doc_delivery_postcode_alt1'=> $data[0]['doc_delivery_postcode_alt1'],
                'doc_delivery_contact_no_alt1'=> $data[0]['doc_delivery_contact_no_alt1'],
                
                'doc_pickup_contact_area_alt2'=> $data[0]['doc_pickup_contact_area_alt2'],
                'doc_delivery_company_alt2'=> $data[0]['doc_delivery_company_alt2'],
                'doc_delivery_recipient_name_alt2'=> $data[0]['doc_delivery_recipient_name_alt2'],
                'doc_delivery_address_alt2'=> $data[0]['doc_delivery_address_alt2'],
                'doc_delivery_city_alt2'=> $data[0]['doc_delivery_city_alt2'],
                'doc_delivery_postcode_alt2'=> $data[0]['doc_delivery_postcode_alt2'],
                'doc_delivery_contact_no_alt2'=> $data[0]['doc_delivery_contact_no_alt2'],
                
                'doc_package_total_pieces'=> $data[0]['doc_package_total_pieces'],
                'doc_package_pickup_date'=> $data[0]['doc_package_pickup_date'],
                'doc_package_ready_hr'=> $data[0]['doc_package_ready_hr'],
                'doc_package_ready_min'=> $data[0]['doc_package_ready_min'],
                'doc_package_ready_ampm'=> $data[0]['doc_package_ready_ampm'],
                'doc_package_office_close_hr'=> $data[0]['doc_package_office_close_hr'],
                'doc_package_office_close_min'=> $data[0]['doc_package_office_close_min'],
                'doc_package_office_close_ampm'=> $data[0]['doc_package_office_close_ampm'],
                'doc_delivery_type' => $data[0]['doc_delivery_type'],
                'doc_delivery_type_name'=> $data[0]['doc_delivery_type_name'],
                'doc_delivery_cost' => $data[0]['doc_delivery_cost'],
                'doc_delivery_security_no'=> $data[0]['doc_delivery_security_no'],
                'police_clearance_id' => $data[0]['police_clearance_id'],
                'police_clearance_name' => $data[0]['police_clearance_name'],
                'police_clearance_price' => $data[0]['police_clearance_price'],
                'police_clearance_name_additional'=> $data[0]['police_clearance_name_additional'],
                'police_clearance_additional_price' => $data[0]['police_clearance_additional_price'],
                'police_clearance_date_cls_received_all_items' => $data[0]['police_clearance_date_cls_received_all_items'],
                'police_clearance_date_submitted_for_processing' => $data[0]['police_clearance_date_submitted_for_processing'],
                'police_clearance_date_completed_and_received_at_cls' => $data[0]['police_clearance_date_completed_and_received_at_cls'],
                'police_clearance_date_order_on_route_and_closed' => $data[0]['police_clearance_date_order_on_route_and_closed'],
                'police_clearance_file' => $data[0]['police_clearance_file'],
                'rvv_first_entry_date'=> $data[0]['rvv_first_entry_date'],
                'rvv_first_departure_date'=> $data[0]['rvv_first_departure_date'],
                'rvv_second_entry_date'=> $data[0]['rvv_second_entry_date'],
                'rvv_second_departure_date'=> $data[0]['rvv_second_departure_date'],
                'rvv_list_of_cities'=> $data[0]['rvv_list_of_cities'],
                'rvv_list_of_hotels'=> $data[0]['rvv_list_of_hotels'],
                'rvv_visa_applied_at'=> $data[0]['rvv_visa_applied_at'],
                'russian_visa_voucher_id'=> $data[0]['russian_visa_voucher_id'],
                'russian_visa_voucher_name'=> $data[0]['russian_visa_voucher_name'],
                'russian_visa_voucher_col_no'=> $data[0]['russian_visa_voucher_col_no'],
                'russian_visa_voucher_col_cost'=> $data[0]['russian_visa_voucher_col_cost'],
                'rvv_file'=> $data[0]['rvv_file'],
                'rvv_comments'=> $data[0]['rvv_comments'],
                
                'dl_company'=> $data[0]['dl_company'],
                'dl_address'=> $data[0]['dl_address'],
                'dl_city'=> $data[0]['dl_city'],
                'dl_state'=> $data[0]['dl_state'],
                'dl_postcode'=> $data[0]['dl_postcode'],
                'dl_contact_name'=> $data[0]['dl_contact_name'],
                'dl_mobile'=> $data[0]['dl_mobile'],
                'dl_email'=> $data[0]['dl_email'],
                'dl_date_doc_returned'=> $data[0]['dl_date_doc_returned'],
                'dl_embassy'=> $data[0]['dl_embassy'],
                'dl_embassy_name'=> $data[0]['dl_embassy_name'],
                'dl_ref_no'=> $data[0]['dl_ref_no'],
                'dl_com_invoice_no'=> $data[0]['dl_com_invoice_no'],
                'dl_payment_type' => $data[0]['dl_payment_type'],
                'dl_checklist'=> $this->getDlOrderChecklistByOrderNo($order_no),
                'dl_nationality'=> $data[0]['dl_nationality'],
                'dl_visa_shipped_by'=> $data[0]['dl_visa_shipped_by'],
                'dl_visa_com_note_no'=> $data[0]['dl_visa_com_note_no'],
                'dl_visa_com_note_in'=> $data[0]['dl_visa_com_note_in'],
                'dl_visa_invoice_no'=> $data[0]['dl_visa_invoice_no'],
                'dl_quotes'=>  $this->getOrderDlQuotesBySentGroupByOrderNo($order_no),
                'dl_quotes_latest_group'=> $this->getOrderDlQuotesLatestGroupByOrderNo($order_no)
                );

            return $post;
        }else{
            return array();
        }
    }
    
    
    /**
     * Return order details
     * @return type
     */
    public function getOrderDetailsByOrderNo($order_no){
        $session = $this->getRequest()->getSession();
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
        $query = $cust->createQueryBuilder('p')
            ->select("p.order_no, p.client_id, p.date_last_saved, p.date_submitted, p.order_type, p.primary_traveller_name, p.destination,
                p.departure_date, p.pri_dept_contact_department_id, d.name as department_name, p.pri_dept_contact_fname,
                p.pri_dept_contact_lname, p.pri_dept_contact_email, p.pri_dept_contact_phone, p.status, p.discount_code, 
                p.discount_rate, p.tpn_qty, p.tpn_price, p.tpn_additional_qty, p.tpn_additional_price, p.grand_total,
                pay.date_paid, pay.fname as payer_fname, pay.lname as payer_lname, pay.email as payer_email, pay.phone as payer_phone, pay.mobile as payer_mobile,
                pay.address as payer_address, pay.city as payer_city, pay.state as payer_state, pay.postcode as payer_postcode, pay.additional_address_details as payer_additional_address_details,
                pay.mba_address, pay.mba_city, pay.mba_state, pay.mba_postcode, c.countryName as mba_country, pay.account_no,u.email as user_email, u.fname as user_fname, u.can_get_special_price, u.special_price, u.can_charge_cost_to_account,
                p.visa_courier, p.visa_mdd_company, p.visa_mdd_address, p.visa_mdd_city, p.visa_mdd_state, p.visa_mdd_postcode, p.visa_mdd_fname, p.visa_mdd_lname, p.visa_mdd_contact, p.visa_additional_comment,
                p.visa_courier_price, vco.type as selected_visa_courier, p.visa_cls_team_member, p.visa_is_delivered_to_embassy, p.visa_is_delivered_to_embassy_date, p.visa_next_embassy, u.account_no as user_account_no,
                p.passport_office_booking_no, p.passport_office_booking_time, p.passport_office_booking_time_hr, p.passport_office_booking_time_min,
                p.s_doc_sent, p.date_doc_sent, p.date_completed, pay.s_paid, u.type as client_type, u.fname as client_fname, u.lname as client_lname,
                
                
                p.doc_receiver_name, p.doc_pickup_address, p.doc_pickup_city, p.doc_pickup_postcode, p.doc_pickup_contact_no,p.doc_pickup_contact_area, p.doc_delivery_recipient_name, p.doc_delivery_company,
                p.doc_delivery_address, p.doc_delivery_city, p.doc_delivery_postcode, p.doc_delivery_contact_no,p.doc_delivery_security_no, p.doc_package_total_pieces,
                p.doc_package_pickup_date, p.doc_package_ready_hr, p.doc_package_ready_min, p.doc_package_ready_ampm, p.doc_package_office_close_hr, p.doc_package_office_close_min, p.doc_package_office_close_ampm,
                dd.type as doc_delivery_type, dd.cost as doc_delivery_cost, p.police_clearance_id, pc.name as police_clearance_name, pc.price as police_clearance_price,
                pc.name_additional as police_clearance_name_additional, pc.price_additional as police_clearance_additional_price,
                p.police_clearance_date_cls_received_all_items, p.police_clearance_date_submitted_for_processing, p.police_clearance_date_completed_and_received_at_cls,
                p.police_clearance_date_order_on_route_and_closed, pay.payment_option,
                p.rvv_first_entry_date, p.rvv_first_departure_date, p.rvv_second_entry_date, p.rvv_second_departure_date, p.rvv_list_of_cities, p.rvv_list_of_hotels,
                p.rvv_visa_applied_at, p.russian_visa_voucher_id, rvv.name as russian_visa_voucher_name, p.russian_visa_voucher_col_cost, p.russian_visa_voucher_col_no, p.rvv_file, p.rvv_comments,
                
                p.dl_company, p.dl_address, p.dl_city, p.dl_state, p.dl_postcode, p.dl_contact_name, p.dl_mobile, p.dl_email, p.dl_date_doc_returned, p.dl_embassy, cem.countryName as dl_embassy_name, p.dl_ref_no, p.dl_com_invoice_no, p.dl_payment_type, p.dl_nationality,
                p.dl_visa_shipped_by, p.dl_visa_com_note_no, p.dl_visa_com_note_in, p.dl_visa_invoice_no,
                
                ofud.follow_up_date
                ")
            ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = p.pri_dept_contact_department_id')
            ->leftJoin('AcmeCLSclientGovBundle:Payment', 'pay', 'WITH', 'p.order_no = pay.order_no')
            ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = p.client_id')
            ->leftJoin('AcmeCLSadminBundle:VisaCourierOptions', 'vco', 'WITH', 'vco.id = p.visa_courier')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = pay.mba_country_id')
            ->leftJoin('AcmeCLSadminBundle:SettingsDocumentDelivery', 'dd', 'WITH', 'dd.id = p.doc_delivery_type')
            ->leftJoin('AcmeCLSadminBundle:PoliceClearances', 'pc', 'WITH', 'pc.id = p.police_clearance_id')
            ->leftJoin('AcmeCLSadminBundle:RussianVisaVoucher', 'rvv', 'WITH', 'rvv.id = p.russian_visa_voucher_id')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'cem', 'WITH', 'cem.id = p.dl_embassy')
            ->leftJoin('AcmeCLSadminBundle:OrderFollowUpDate', 'ofud', 'WITH', 'ofud.admin_id = :admin_id AND ofud.order_id = p.order_no')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->setParameter("admin_id", $session->get("admin_id"))
            ->getQuery();
        $data = $query->getArrayResult();
        
        if(count($data) > 0){
            $post = array("travels"=>$this->getOrderTpnsByOrderNo($order_no),
                "destinations"=>$this->getOrderDestinationsByOrderNo($order_no, $data[0]['client_type']),
                'travellers' => $this->getOrderTravellersByOrderNo($order_no),
                'passport_applicants'=> $this->getOrderPassportApplicants($order_no),
                'police_clearance_applicants'=> $this->getOrderPoliceClearanceApplicants($order_no),
                'pri_dept_contact_department_id' => $data[0]['pri_dept_contact_department_id'],
                'pri_dept_contact_fname' => $data[0]['pri_dept_contact_fname'],
                'pri_dept_contact_lname' => $data[0]['pri_dept_contact_lname'],
                'pri_dept_contact_email' => $data[0]['pri_dept_contact_email'],
                'pri_dept_contact_phone' => $data[0]['pri_dept_contact_phone'],
                'order_no' => $data[0]['order_no'],
                'department_name' =>$data[0]['department_name'],
                'status' => $data[0]['status'],
                'discount_code' => $data[0]['discount_code'],
                'discount_rate' => $data[0]['discount_rate'],
                'tpn_qty'=> $data[0]['tpn_qty'],
                'tpn_price'=> $data[0]['tpn_price'],
                'tpn_additional_qty'=> $data[0]['tpn_additional_qty'],
                'tpn_additional_price'=> $data[0]['tpn_additional_price'],
                'grand_total' => $data[0]['grand_total'],
                'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                'payment_option' => $data[0]['payment_option'],
                'date_last_saved' => $data[0]['date_last_saved'],
                'date_submitted' => $data[0]['date_submitted'],
                'order_notes'=> $this->getOrderNotesByOrderNo($order_no),
                'order_no' => $order_no,
                'order_type' => $data[0]['order_type'],
                'order_type_name' => $this->getOrderTypeName($data[0]['order_type']),
                'date_paid' => $data[0]['date_paid'],
                'date_paid' => $data[0]['date_paid'],
                'payer_fname' => $data[0]['payer_fname'],
                'payer_lname' => $data[0]['payer_lname'],
                'user_email' => $data[0]['user_email'],
                'user_fname' => $data[0]['user_fname'],
                
                'payer_email' => $data[0]['payer_email'],
                'payer_phone' => $data[0]['payer_phone'],
                'payer_mobile' => $data[0]['payer_mobile'],
                'payer_address' => $data[0]['payer_address'],
                'payer_city' => $data[0]['payer_city'],
                'payer_state' => $data[0]['payer_state'],
                'payer_postcode' => $data[0]['payer_postcode'],
                'payer_additional_address_details'=> $data[0]['payer_additional_address_details'],
                'account_no' => $data[0]['account_no'],
                'user_account_no'=> $data[0]['user_account_no'],
                'mba_address' => $data[0]['mba_address'],
                'mba_state' => $data[0]['mba_state'],
                'mba_city' => $data[0]['mba_city'],
                'mba_postcode' => $data[0]['mba_postcode'],
                'mba_country' => $data[0]['mba_country'],
                
                'client_id'=>$data[0]['client_id'],
                'client_fname'=> $data[0]['client_fname'],
                'client_lname'=> $data[0]['client_lname'],
                'can_get_special_price'=>$data[0]['can_get_special_price'],
                'special_price'=>$data[0]['special_price'],
                'can_charge_cost_to_account'=> $data[0]['can_charge_cost_to_account'],
                'visa_courier'=> $data[0]['visa_courier'],
                'selected_visa_courier'=> $data[0]['selected_visa_courier'],
                'visa_courier_price'=> $data[0]['visa_courier_price'],
                'visa_mdd_company'=> $data[0]['visa_mdd_company'],
                'visa_mdd_address'=> $data[0]['visa_mdd_address'],
                'visa_mdd_city'=> $data[0]['visa_mdd_city'],
                'visa_mdd_state'=> $data[0]['visa_mdd_state'],
                'visa_mdd_postcode'=> $data[0]['visa_mdd_postcode'],
                'visa_mdd_fname'=> $data[0]['visa_mdd_fname'],
                'visa_mdd_lname'=> $data[0]['visa_mdd_lname'],
                'visa_mdd_contact'=> $data[0]['visa_mdd_contact'],
                'visa_additional_comment'=> $data[0]['visa_additional_comment'],
                'visa_cls_team_member'=> $data[0]['visa_cls_team_member'],
                
                'visa_is_delivered_to_embassy'=> $data[0]['visa_is_delivered_to_embassy'],
                'visa_is_delivered_to_embassy_date'=> $data[0]['visa_is_delivered_to_embassy_date'],
                'visa_next_embassy'=> $data[0]['visa_next_embassy'],
                
                'passport_office_booking_no'=> $data[0]['passport_office_booking_no'],
                'passport_office_booking_time'=> $data[0]['passport_office_booking_time'],
                'passport_office_booking_time_hr'=> $data[0]['passport_office_booking_time_hr'],
                'passport_office_booking_time_min'=> $data[0]['passport_office_booking_time_min'],
                's_doc_sent'=> $data[0]['s_doc_sent'],
                'date_doc_sent'=> $data[0]['date_doc_sent'],
                'date_completed'=> $data[0]['date_completed'],
                's_paid'=> $data[0]['s_paid'],
                
                'doc_receiver_name'=> $data[0]['doc_receiver_name'],
                'doc_pickup_address'=> $data[0]['doc_pickup_address'],
                'doc_pickup_city'=> $data[0]['doc_pickup_city'],
                'doc_pickup_postcode'=> $data[0]['doc_pickup_postcode'],
                'doc_pickup_contact_no'=> $data[0]['doc_pickup_contact_no'],
                'doc_pickup_contact_area'=> $data[0]['doc_pickup_contact_area'],
                'doc_delivery_company'=> $data[0]['doc_delivery_company'],
                'doc_delivery_recipient_name'=> $data[0]['doc_delivery_recipient_name'],
                'doc_delivery_address'=> $data[0]['doc_delivery_address'],
                'doc_delivery_city'=> $data[0]['doc_delivery_city'],
                'doc_delivery_postcode'=> $data[0]['doc_delivery_postcode'],
                'doc_delivery_contact_no'=> $data[0]['doc_delivery_contact_no'],
                'doc_delivery_security_no'=> $data[0]['doc_delivery_security_no'],
                'doc_package_total_pieces'=> $data[0]['doc_package_total_pieces'],
                'doc_package_pickup_date'=> $data[0]['doc_package_pickup_date'],
                'doc_package_ready_hr'=> $data[0]['doc_package_ready_hr'],
                'doc_package_ready_min'=> $data[0]['doc_package_ready_min'],
                'doc_package_ready_ampm'=> $data[0]['doc_package_ready_ampm'],
                'doc_package_office_close_hr'=> $data[0]['doc_package_office_close_hr'],
                'doc_package_office_close_min'=> $data[0]['doc_package_office_close_min'],
                'doc_package_office_close_ampm'=> $data[0]['doc_package_office_close_ampm'],
                'doc_delivery_type' => $data[0]['doc_delivery_type'],
                'doc_delivery_cost' => $data[0]['doc_delivery_cost'],
                'police_clearance_id' => $data[0]['police_clearance_id'],
                'police_clearance_name' => $data[0]['police_clearance_name'],
                'police_clearance_price' => $data[0]['police_clearance_price'],
                'police_clearance_name_additional'=> $data[0]['police_clearance_name_additional'],
                'police_clearance_additional_price' => $data[0]['police_clearance_additional_price'],
                'police_clearance_date_cls_received_all_items' => $data[0]['police_clearance_date_cls_received_all_items'],
                'police_clearance_date_submitted_for_processing' => $data[0]['police_clearance_date_submitted_for_processing'],
                'police_clearance_date_completed_and_received_at_cls' => $data[0]['police_clearance_date_completed_and_received_at_cls'],
                'police_clearance_date_order_on_route_and_closed' => $data[0]['police_clearance_date_order_on_route_and_closed'],
                'rvv_first_entry_date'=> $data[0]['rvv_first_entry_date'],
                'rvv_first_departure_date'=> $data[0]['rvv_first_departure_date'],
                'rvv_second_entry_date'=> $data[0]['rvv_second_entry_date'],
                'rvv_second_departure_date'=> $data[0]['rvv_second_departure_date'],
                'rvv_list_of_cities'=> $data[0]['rvv_list_of_cities'],
                'rvv_list_of_hotels'=> $data[0]['rvv_list_of_hotels'],
                'rvv_visa_applied_at'=> $data[0]['rvv_visa_applied_at'],
                'russian_visa_voucher_id'=> $data[0]['russian_visa_voucher_id'],
                'russian_visa_voucher_name'=> $data[0]['russian_visa_voucher_name'],
                'russian_visa_voucher_col_no'=> $data[0]['russian_visa_voucher_col_no'],
                'russian_visa_voucher_col_cost'=> $data[0]['russian_visa_voucher_col_cost'],
                'rvv_file'=> $data[0]['rvv_file'],
                'rvv_comments'=> $data[0]['rvv_comments'],
                
                'dl_company'=> $data[0]['dl_company'],
                'dl_address'=> $data[0]['dl_address'],
                'dl_city'=> $data[0]['dl_city'],
                'dl_state'=> $data[0]['dl_state'],
                'dl_postcode'=> $data[0]['dl_postcode'],
                'dl_contact_name'=> $data[0]['dl_contact_name'],
                'dl_mobile'=> $data[0]['dl_mobile'],
                'dl_email'=> $data[0]['dl_email'],
                'dl_date_doc_returned'=> $data[0]['dl_date_doc_returned'],
                'dl_embassy'=> $data[0]['dl_embassy'],
                'dl_embassy_name'=> $data[0]['dl_embassy_name'],
                'dl_ref_no'=> $data[0]['dl_ref_no'],
                'dl_com_invoice_no'=> $data[0]['dl_com_invoice_no'],
                'dl_payment_type'=> $data[0]['dl_payment_type'],
                'dl_checklist'=> $this->getDlOrderChecklistByOrderNo($order_no),
                'dl_nationality'=> $data[0]['dl_nationality'],
                'dl_visa_shipped_by'=> $data[0]['dl_visa_shipped_by'],
                'dl_visa_com_note_no'=> $data[0]['dl_visa_com_note_no'],
                'dl_visa_com_note_in'=> $data[0]['dl_visa_com_note_in'],
                'dl_visa_invoice_no'=> $data[0]['dl_visa_invoice_no'],
                'dl_quotes'=>  $this->getOrderDlQuotesBySentGroupByOrderNo($order_no),
                'dl_quotes_latest_group'=> $this->getOrderDlQuotesLatestGroupByOrderNo($order_no),
                    
                'follow_up_date'=> $data[0]['follow_up_date']
                );

            return $post;
        }else{
            return array();
        }
    }
    
    /**
     * get payment details by order no
     */
    public function getPaymentDetailsByOrderNo($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Payment');
        $query = $cust->createQueryBuilder('p')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->getQuery();
        $data = $query->getArrayResult();
        
        return (count($data) > 0) ? $data[0] : array();
    }
    
    public function getDlOrderChecklistByOrderNo($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDlChecklist');
        $query = $cust->createQueryBuilder('p')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->getQuery();
        return $query->getArrayResult();
    }
    
    
    /**
     * return discount rate
     * @param type $code
     * @return int
     */
    public function getDiscountRateByCode($code){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
        $query = $cust->createQueryBuilder('p')
            ->where("p.code = :code")
            ->setParameter("code", $code)
            ->getQuery();
        $data = $query->getArrayResult();
        
        if(count($data) > 0){
            return $data[0]["rate"];
        }else{
            return 0;
        }
    }
    
    /**
     * return discount rate
     * @param type $code
     * @return int
     */
    public function getCreditCardProcessingFee(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:CreditCardProcessing');
        $query = $cust->createQueryBuilder('p')
            ->where("p.id = :id")
            ->setParameter("id", 1)
            ->getQuery();
        $data = $query->getArrayResult();
        
        if(count($data) > 0){
            return $data[0]["fee"];
        }else{
            return 0;
        }
    }
    
    public function getBulkOrderFiles($bulk_order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisa');
        $query = $cust->createQueryBuilder('p')
            ->select("p.bulk_order_no, p.client_id, p.status, p.level, p.discount_rate, p.discount_code, p.grand_total,
                u.can_get_special_price, u.special_price, u.can_charge_cost_to_account, u.account_no as user_account_no, 
                u.type as client_type, u.fname as client_fname, u.lname as client_lname")
            ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = p.client_id')
            ->where('p.bulk_order_no = :bulk_order_no')
            ->setParameter('bulk_order_no', $bulk_order_no)
            ->getQuery();
        $data = $query->getArrayResult();
        
        
    }
    
    /**
     * return order destinations
     * @return type
     */
    public function getOrderDestinationsByOrderNo($order_no, $user_type = 'government'){
        
        if($user_type == 'government'){
            $filelocation = "visafiles";
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDestinations');
            $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.order_no, p.country_id, a.countryName, a.country_name_display, a.rep_name, a.visa_information, p.departure_date, p.entry_option, p.entry_date_country,
                p.departure_date_country, p.travel_purpose, p.s_primary, p.selected_visa_type, p.selected_visa_type_price, p.selected_visa_type_requirements,
                p.visa_date_cls_received_all_items, p.visa_date_submitted_for_processing, p.visa_date_completed_and_received_at_cls, 
                p.visa_shipped_by, p.visa_com_note_no, p.visa_com_note_in, p.visa_invoice_no,
                p.visa_date_order_on_route_and_closed, vt.type as selected_visa_type_name, vt.file_attachment, 
                p.status, a.public_s_no_visa_required, a.public_s_no_visa_required_html, a.gov_s_no_visa_required, a.gov_s_no_visa_required_html, p.tpn_stat, p.tpn_middle_src, p.tpn_date_issued,
                p.visa_follow_up_date,p.signature,p.sig_name")
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'a', 'WITH', 'a.id = p.country_id')
            ->leftJoin('AcmeCLSadminBundle:VisaTypes', 'vt', 'WITH', 'vt.id = p.selected_visa_type')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->orderBy("p.id")
            ->getQuery();
        }else if($user_type == 'public' || $user_type == 'corporate' || $user_type == 'bulk-visa'){
            $filelocation = "publicvisafiles";
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDestinations');
            $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.order_no, p.country_id, a.countryName, a.country_name_display, a.rep_name, a.visa_information, p.departure_date, p.entry_option, p.entry_date_country,
                p.departure_date_country, p.travel_purpose, p.s_primary, p.selected_visa_type, p.selected_visa_type_price, p.selected_visa_type_requirements,
                p.visa_date_cls_received_all_items, p.visa_date_submitted_for_processing, p.visa_date_completed_and_received_at_cls, 
                p.visa_shipped_by, p.visa_com_note_no, p.visa_com_note_in, p.visa_invoice_no,
                p.visa_date_order_on_route_and_closed, vt.type as selected_visa_type_name, vt.file_attachment, 
                p.status, a.public_s_no_visa_required, a.public_s_no_visa_required_html, a.gov_s_no_visa_required, a.gov_s_no_visa_required_html, p.tpn_stat, p.tpn_middle_src, p.tpn_date_issued,
                p.visa_follow_up_date,p.signature,p.sig_name")
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'a', 'WITH', 'a.id = p.country_id')
            ->leftJoin('AcmeCLSadminBundle:PublicVisaTypes', 'vt', 'WITH', 'vt.id = p.selected_visa_type')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->orderBy("p.id")
            ->getQuery();
        }
        $data = $query->getArrayResult();
        
        $post = array();
        for($i=0; $i<count($data); $i++){
            $post[] = array(
                    'id'=> $data[$i]['id'],
                    'order_no'=> $data[$i]['order_no'],
                    'country_id'=> $data[$i]['country_id'],
                    'countryName'=> $data[$i]['countryName'],
                    'country_name_display'=> $data[$i]['country_name_display'],
                    'rep_name'=> $data[$i]['rep_name'],
                    'visa_information'=> $data[$i]['visa_information'],
                    'departure_date'=> $data[$i]['departure_date'],
                    'entry_option'=> $data[$i]['entry_option'],
                    'entry_date_country'=> $data[$i]['entry_date_country'],
                    'departure_date_country'=> $data[$i]['departure_date_country'],
                    'travel_purpose'=> $data[$i]['travel_purpose'],
                    's_primary'=> $data[$i]['s_primary'],
                    'visa_types'=> $this->getVisaTypesByCountryId($data[$i]['country_id']),
                    'public_visa_types' => $this->getPublicVisaTypesByCountryId($data[$i]['country_id']),
                    'selected_visa_type'=> $data[$i]['selected_visa_type'],
                    'selected_visa_type_name'=> $data[$i]['selected_visa_type_name'],
                    'selected_visa_type_price'=> $data[$i]['selected_visa_type_price'],
                    'selected_visa_type_requirements'=> $data[$i]['selected_visa_type_requirements'],
                    'selected_visa_type_requirements_array'=> $this->extractSelectedVisaTypeRequirements($data[$i]['selected_visa_type_requirements'], $user_type),
                    'file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/'.$filelocation.'/'.$data[$i]['country_id'].'/'.$data[$i]['selected_visa_type'].'/'. $data[$i]['file_attachment'],
                    'file_attached'=>$data[$i]['file_attachment'],
                    'status'=>$data[$i]['status'],
                    'visa_date_cls_received_all_items'=>$data[$i]['visa_date_cls_received_all_items'],
                    'visa_date_submitted_for_processing'=>$data[$i]['visa_date_submitted_for_processing'],
                    'visa_date_completed_and_received_at_cls'=>$data[$i]['visa_date_completed_and_received_at_cls'],
                    'visa_date_order_on_route_and_closed'=>$data[$i]['visa_date_order_on_route_and_closed'],
                    'visa_shipped_by'=>$data[$i]['visa_shipped_by'],
                    'visa_com_note_no'=>$data[$i]['visa_com_note_no'],
                    'visa_com_note_in'=>$data[$i]['visa_com_note_in'],
                    'visa_invoice_no'=>$data[$i]['visa_invoice_no'],
                    'visa_notes'=> $this->getDestinationNotesByDestId( $data[$i]['id']),
                    'public_s_no_visa_required'=> $data[$i]['public_s_no_visa_required'],
                    'public_s_no_visa_required_html'=> $data[$i]['public_s_no_visa_required_html'],
                    'gov_s_no_visa_required'=> $data[$i]['gov_s_no_visa_required'],
                    'gov_s_no_visa_required_html'=> $data[$i]['gov_s_no_visa_required_html'],
                    'tpn_stat'=> $data[$i]['tpn_stat'],
                    'tpn_middle_src'=> $data[$i]['tpn_middle_src'],
                    'tpn_date_issued'=> $data[$i]['tpn_date_issued'],
                    'visa_follow_up_date'=> $data[$i]['visa_follow_up_date'],
                    'signature'=>$data[$i]['signature'],
                    'sig_name'=>$data[$i]['sig_name']
                );
        }
        
        return $post;
    }
    
    
    /**
     * get order passport applicants
     * @param type $order_no
     * @return type
     */
    public function getOrderPassportApplicants($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:OrderPassportApplicants');
        $query = $cust->createQueryBuilder('p')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->orderBy("p.id", "asc")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    /**
     * get order police clearance applicants
     * @param type $order_no
     * @return type
     */
    public function getOrderPoliceClearanceApplicants($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:OrderPoliceClearanceApplicants');
        $query = $cust->createQueryBuilder('p')
            ->select('p.id, p.order_no, p.fname, p.mname, p.lname, p.email, p.phone, p.mobile, p.address,
                p.city, p.postcode, p.state, p.country_id, c.countryName, p.passport_no, p.departure_date')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.country_id')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->orderBy("p.id", "asc")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    /**
     * return order destinations
     * @return type
     */
    public function getOrderDestinationDetails($destination_id, $client_id, $client_type = "government"){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDestinations');
        if($client_type == 'government'){
            $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.order_no, p.country_id, a.countryName, a.rep_name, a.visa_information, p.departure_date, p.entry_option, p.entry_date_country,
                p.departure_date_country, p.travel_purpose, p.s_primary, p.selected_visa_type, p.selected_visa_type_price, p.selected_visa_type_requirements,
                p.visa_date_cls_received_all_items, p.visa_date_submitted_for_processing, p.visa_date_completed_and_received_at_cls, 
                p.visa_date_order_on_route_and_closed, vt.type as selected_visa_type_name, vt.file_attachment, p.status, o.date_submitted")
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'a', 'WITH', 'a.id = p.country_id')
            ->leftJoin('AcmeCLSadminBundle:VisaTypes', 'vt', 'WITH', 'vt.id = p.selected_visa_type')
            ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = p.order_no')    
            ->where("p.id = :destination_id AND o.client_id = :client_id")
            ->setParameter("destination_id", $destination_id)
            ->setParameter("client_id", $client_id)
            ->getQuery();
        }else if($client_type == 'public' || $client_type == 'corporate' || $client_type == 'bulk-visa'){
            $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.order_no, p.country_id, a.countryName, a.rep_name, a.visa_information, p.departure_date, p.entry_option, p.entry_date_country,
                p.departure_date_country, p.travel_purpose, p.s_primary, p.selected_visa_type, p.selected_visa_type_price, p.selected_visa_type_requirements,
                p.visa_date_cls_received_all_items, p.visa_date_submitted_for_processing, p.visa_date_completed_and_received_at_cls, 
                p.visa_date_order_on_route_and_closed, vt.type as selected_visa_type_name, vt.file_attachment, p.status, o.date_submitted")
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'a', 'WITH', 'a.id = p.country_id')
            ->leftJoin('AcmeCLSadminBundle:PublicVisaTypes', 'vt', 'WITH', 'vt.id = p.selected_visa_type')
            ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = p.order_no')    
            ->where("p.id = :destination_id AND o.client_id = :client_id")
            ->setParameter("destination_id", $destination_id)
            ->setParameter("client_id", $client_id)
            ->getQuery();
        }
        $data = $query->getArrayResult();
        
        $post = array(
                'id'=> $data[0]['id'],
                'order_no'=> $data[0]['order_no'],
                'country_id'=> $data[0]['country_id'],
                'countryName'=> $data[0]['countryName'],
                'rep_name'=> $data[0]['rep_name'],
                'visa_information'=> $data[0]['visa_information'],
                'departure_date'=> $data[0]['departure_date'],
                'entry_option'=> $data[0]['entry_option'],
                'entry_date_country'=> $data[0]['entry_date_country'],
                'departure_date_country'=> $data[0]['departure_date_country'],
                'travel_purpose'=> $data[0]['travel_purpose'],
                's_primary'=> $data[0]['s_primary'],
                'visa_types'=> $this->getVisaTypesByCountryId($data[0]['country_id']),
                'selected_visa_type'=> $data[0]['selected_visa_type'],
                'selected_visa_type_name'=> $data[0]['selected_visa_type_name'],
                'selected_visa_type_price'=> $data[0]['selected_visa_type_price'],
                'selected_visa_type_requirements'=> $data[0]['selected_visa_type_requirements'],
                'selected_visa_type_requirements_array'=> $this->extractSelectedVisaTypeRequirements($data[0]['selected_visa_type_requirements'], $client_type),
                'file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/visafiles/'.$data[0]['country_id'].'/'.$data[0]['selected_visa_type'].'/'. $data[0]['file_attachment'],
                'file_attached'=>$data[0]['file_attachment'],
                'status'=>$data[0]['status'],
                'visa_date_cls_received_all_items'=>$data[0]['visa_date_cls_received_all_items'],
                'visa_date_submitted_for_processing'=>$data[0]['visa_date_submitted_for_processing'],
                'visa_date_completed_and_received_at_cls'=>$data[0]['visa_date_completed_and_received_at_cls'],
                'visa_date_order_on_route_and_closed'=>$data[0]['visa_date_order_on_route_and_closed'],
                'visa_notes'=> $this->getDestinationNotesByDestId($data[0]['id']),
                'date_submitted'=> $data[0]['date_submitted']
            );
        
        return $post;
    }
    
    public function getDestinationNotesByDestId($destination_id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:OrderDestinationNotes');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.destination_id, p.note, p.date_added, p.note_by, p.note_by_name, p.user_type,p.is_admin")
            ->where("p.destination_id = :destination_id")
            ->setParameter("destination_id", $destination_id)
            ->orderBy("p.id", "desc")
            ->getQuery();
        $data = $query->getArrayResult();
        return $data;
//        $post = array();
//        for($i=0; $i<count($data); $i++){
//            $post[] = array(
//                'id'=>$data[$i]['id'],
//                'destination_id'=>$data[$i]['destination_id'],
//                'note'=>$data[$i]['note'],
//                'date_added'=>$data[$i]['date_added'],
//                'note_by'=>$data[$i]['note_by'],
//                'note_by_name'=>$data[$i]['note_by_name'],
//                'user_type'=>$data[$i]['user_type'],
//                'fname'=>$data[$i]['fname'],
//                'lname'=>$data[$i]['lname']
//            );
//        }
    }
    
    
    public function getRequiredVisaFeeByVisaId($visa_id, $client_type = 'government'){
        if($client_type == 'government'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaAdditionalRequirements');
        }else if($client_type == 'public'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaAdditionalRequirements');
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaAdditionalRequirements');
        }
        $query = $cust->createQueryBuilder('p')
            ->where("p.visa_id = :visa_id and p.status = :status and p.s_required = :s_required")
            ->setParameter("visa_id", $visa_id)
            ->setParameter("status", 1)
            ->setParameter("s_required", 1)
            ->getQuery();
        $reqs = $query->getArrayResult();
        
        return $reqs;
    }
    
    public function extractSelectedVisaTypeRequirements($selected_visa_type_requirements, $client_type = 'government'){
        $data = explode(";", $selected_visa_type_requirements);
        $post = array();
        
        for($i=0; $i<count($data); $i++){
            $req = explode(",", $data[$i]);
            if($client_type == 'government'){
                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaAdditionalRequirements');
            }else if($client_type == 'public'){
                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaAdditionalRequirements');
            }else{
                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaAdditionalRequirements');
            }
            $query = $cust->createQueryBuilder('p')
                ->where("p.id = :id")
                ->setParameter("id", $req[0])
                ->getQuery();
            $reqs = $query->getArrayResult();
            
            for($e=0; $e<count($reqs); $e++){
                $post[] = array(
                    'id'=>$req[0],
                    'requirement'=>$reqs[$e]['requirement'],
                    'cost'=>$reqs[$e]['cost'],
                    's_required'=>$reqs[$e]['s_required']
                    //'cost'=>$req[1]
                );
            }
        }
        
        return $post;
    }
    
    public function extractSelectedVisaTypeRequirementsImplode($selected_visa_type_requirements, $client_type = 'government'){
        $data = explode(";", $selected_visa_type_requirements);
        $ids = array();
        $names = array();
        
        for($i=0; $i<count($data); $i++){
            $req = explode(",", $data[$i]);
            if($client_type == 'government'){
                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaAdditionalRequirements');
            }else if($client_type == 'public'){
                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaAdditionalRequirements');
            }else{
                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PublicVisaAdditionalRequirements');
            }
            $query = $cust->createQueryBuilder('p')
                ->where("p.id = :id")
                ->setParameter("id", $req[0])
                ->getQuery();
            $reqs = $query->getArrayResult();
            
            for($e=0; $e<count($reqs); $e++){
                $ids[] = intval($req[0]);
            }
            
            for($e=0; $e<count($reqs); $e++){
                $names[] = $reqs[$e]['requirement'] . ' - $' . $reqs[$e]['cost'];
            }
        }
        
        $ids = json_encode($ids);
        $names = implode(",",$names);
        
        return array('ids'=> $ids, 'names'=>$names);
    }
    
    
    
    /**
     * return order destinations
     * @return type
     */
    public function getOrderTpnsByOrderNo($order_no){
        $mod = new Model\GlobalModel();
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
        $query = $cust->createQueryBuilder('p')
            ->select("p.tpn_no, p.date_submitted, p.date_last_updated, p.order_no, p.client_id, p.tpn_src, c.countryName as destination, p.departure_date,
                p.status, p.entry_option, p.entry_date_country, p.departure_date_country, p.travel_purpose")
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.destination')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->orderBy("p.tpn_no")
            ->getQuery();
        $data = $query->getArrayResult();
        
        $post = array();
        
        if(count($data) > 0){
            for($i=0; $i <count($data); $i++){
                $post[] = array('tpn_no'=> $data[$i]["tpn_no"],
                    'tpn_short_no'=> $mod->getNumberFromString($data[$i]["tpn_no"]),
                    'date_submitted'=> $data[$i]["date_submitted"],
                    'date_last_updated'=> $data[$i]["date_last_updated"],
                    'order_no'=> $data[$i]["order_no"],
                    'client_id'=> $data[$i]["client_id"],
                    'tpn_src'=> $data[$i]["tpn_src"],
                    'destination'=> $data[$i]["destination"],
                    'departure_date'=> $data[$i]["departure_date"],
                    'status'=> $data[$i]["status"],
                    'entry_option'=> $data[$i]["entry_option"],
                    'entry_date_country'=> $data[$i]["entry_date_country"],
                    'departure_date_country'=> $data[$i]["departure_date_country"],
                    'travel_purpose'=> $data[$i]["travel_purpose"],
                    'tpn_notes' => $this->getTpnNotesByTpnNo($data[$i]["tpn_no"])
                    );
            }
            
            return $post;
            
        }else{
            return array();
        }
    }
    
    
    /**
     * get tpn notes
     * @param type $tpn_no
     * @return type
     */
    public function getTpnNotesByTpnNo($tpn_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:TpnNotes');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.note, p.date_added, p.note_by, p.user_type")
            ->leftJoin('AcmeCLStpnBundle:TpnUser', 'u', 'WITH', 'u.id = p.tpn_no')
            ->where('p.tpn_no = :tpn_no')
            ->setParameter('tpn_no', $tpn_no)
            ->getQuery();
        $results = $query->getArrayResult();
        
        $data = array();
        for($i=0; $i<count($results); $i++){
            $data[] = array('note'=> $results[$i]["note"],
                    'date_added'=> $results[$i]["date_added"],
                    'note_by'=> $this->getNoteOwnerByIdAndUserType($results[$i]["note_by"], $results[$i]["user_type"]),
                    'user_type' => $results[$i]["user_type"]);
        }
        
        return $data;
    }
    
    
    /**
     * get noted by details
     * @param type $user_id
     * @param type $user_type
     * @return type
     */
    public function getNoteOwnerByIdAndUserType($user_id, $user_type){
        if($user_type == 'DFAT'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUSer');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :user_id')
                ->setParameter('user_id', $user_id)
                ->getQuery();
            $data = $query->getArrayResult();
            
        }elseif($user_type == 'ADMIN'){
            
        }
        
        
        return (count($data) > 0) ? $data[0] : array();
    }
    
    /**
     * return order travellers
     * @return type
     */
    public function getOrderTravellersByOrderNo($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderTravellers');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.title, t.title as title_name, p.fname, p.mname, p.lname, p.email, p.organisation, p.occupation, p.phone, 
                p.birth_date, p.nationality, c.countryName as nationality_name, p.passport_number,
                p.passport_type, p.s_primary, pt.type as passport_type_name, p.rpinfo_fullname, p.rpinfo_position_at_post, 
                p.rpinfo_name_of_post, p.rpinfo_city, 
                p.rvv_citizenship, p.rvv_sex, p.rvv_birth_place, p.rvv_passport_issue_date, p.rvv_passport_exp_date,
                p.rvv_company, p.rvv_position, p.rvv_city, p.rvv_state, p.rvv_postcode, p.rvv_country, cfrvv.countryName as employment_country, p.rvv_company_fax, p.rvv_address,
                p.gender, p.nearest_capital_city
                ")
            ->leftJoin('AcmeCLSclientGovBundle:NameTitle', 't', 'WITH', 't.id = p.title')
            ->leftJoin('AcmeCLSclientGovBundle:PassportTypes', 'pt', 'WITH', 'pt.id = p.passport_type')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.nationality')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'cfrvv', 'WITH', 'cfrvv.id = p.rvv_country')
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->orderBy("p.id")
            ->getQuery();
        $data = $query->getArrayResult();
        return $data;
    }
    
    
    /**
     * return order travellers
     * @return type
     */
    public function getOrderNotesByOrderNo($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderNotes');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.order_no, p.note, p.date_added, p.note_by, p.note_by_name, p.user_type, p.is_admin")
            ->where("p.order_no = :order_no")
            ->setParameter("order_no", $order_no)
            ->getQuery();
        $data = $query->getArrayResult();
        return $data;
    }
    
    /**
     * generate tpn number
     * @return type
     */
    public function generateTpnNo(){
            $em = $this->getDoctrine()->getEntityManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT tpn_no FROM tbl_tpn ORDER BY tpn_no DESC LIMIT 1");
            $statement->execute();
            $rows = $statement->fetchAll();
        
            return (isset($rows[0]['tpn_no'])) ? 'FMB-' . sprintf("%08d", substr($rows[0]['tpn_no'], 4) + 1) : 'FMB-00000001';
    }
    
    
    /**
     * send email
     * @param type $recipients
     * @param type $from
     * @param type $subject
     * @param type $body
     */
    public function sendEmail($recipients, $from, $subject, $body){
        $message = \Swift_Message::newInstance()
            ->setEncoder(\Swift_Encoding::get8BitEncoding())
            ->setSubject($subject)
            ->setFrom($from,'Capital Link Services')
            ->setTo($recipients)
            ->setBody($body)
            ->setContentType("text/html")
        ;
        $this->get('mailer')->send($message);
    }
    
    public function sendEmailWithCc($recipients, $from, $subject, $body){
            $to = (isset($recipients['to'])) ? $recipients['to'] : $recipients;
            $cc = (isset($recipients['cc'])) ? $recipients['cc'] : '';
            $message = \Swift_Message::newInstance()
                ->setEncoder(\Swift_Encoding::get8BitEncoding())
                ->setSubject($subject)
                ->setFrom($from,'Capital Link Services')
                ->setTo($to)
                ->setCc($cc)
                ->setBody($body)
                ->setContentType("text/html")
            ;
        $this->get('mailer')->send($message);
    }
    
    /**
     * send email
     * @param type $recipients
     * @param type $from
     * @param type $subject
     * @param type $body
     */
    public function sendEmailWithAttachment($recipients, $from, $subject, $body, $attachment){
        
        $message = \Swift_Message::newInstance()
            ->setEncoder(\Swift_Encoding::get8BitEncoding())
            ->setSubject($subject)
            ->setFrom($from,'Capital Link Services')
            ->setTo($recipients)
            ->setBody($body)
            ->setContentType("text/html")
        ;
        
        for($i=0; $i<count($attachment); $i++){
            if(!is_dir($attachment[$i]['file_attachment'])){
                if(file_exists($attachment[$i]['file_attachment'])){
                    $message->attach(\Swift_Attachment::fromPath($attachment[$i]['file_attachment']));
                }
            }
        }
        
        $this->get('mailer')->send($message);
    }
    
    
    public function generateBarcode($basePath){
        //header('Location: html/index.php');
        require_once('class/BCGFontFile.php');
        require_once('class/BCGColor.php');
        require_once('class/BCGDrawing.php');

        require_once('class/BCGcode128.barcode.php');
        // The arguments are R, G, and B for color.
        $colorFront = new BCGColor(0, 0, 0);
        $colorBack = new BCGColor(255, 255, 255);

        $font = new BCGFontFile('./font/Arial.ttf', 18);

        $code = new BCGcode128; // Or another class name from the manual
        $code->setScale(2); // Resolution
        $code->setThickness(30); // Thickness
        $code->setForegroundColor($colorFront); // Color of bars
        $code->setBackgroundColor($colorBack); // Color of spaces
        $code->setFont($font); // Font (or 0)
        $code->parse('1234'); // Text

        $drawing = new BCGDrawing('', $colorBack);
        $drawing->setBarcode($code);
        $drawing->draw();

        header('Content-Type: image/png');
        $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
    }
    
    
    /**
     * return false if order tpns are still pending
     * return true if order tpns are all completed
     * @param type $order_no
     * @param type $tpn_status
     * @return type
     */
    public function sTpnOrderCompleted($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
        $query = $cust->createQueryBuilder('p')
            ->where('p.order_no = :order_no AND p.status = 0')
            ->setParameter('order_no', $order_no)
            ->getQuery();
        $results = $query->getArrayResult();
        
        return (count($results) > 0) ? false : true;
    }
    
    
    /**
     * check if email is still available
     * @param type $email
     */
    public function sEmailAvailable($email){
        // clients
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
        $query = $cust->createQueryBuilder('p')
            ->where('p.email = :email')
            ->setParameter('email', $email)
            ->getQuery();
        $client = $query->getArrayResult();
        
        if(count($client) > 0){
            return false; // not available
        }else{
            // admins
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email')
                ->setParameter('email', $email)
                ->getQuery();
            $admin = $query->getArrayResult();
            
            if(count($admin) > 0){
                return false; // not available
            }else{
                
                // tpn users
                $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.email = :email')
                    ->setParameter('email', $email)
                    ->getQuery();
                $tpn = $query->getArrayResult();
                
                if(count($tpn) > 0){
                    return false; // not available
                }else{
                    return true; // still available
                }
            }
        }
        
    }
    
    
    /**
     * check if email is still available
     * @param type $email
     */
    public function sEmailAvailableInDetailUpdate($email, $user_id, $user_type){
        $found = 0;
        // clients
        if($user_type == 'client-user'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.id != :owner_id')
                ->setParameter('email', $email)
                ->setParameter('owner_id', $user_id)
                ->getQuery();
            $client = $query->getArrayResult();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email')
                ->setParameter('email', $email)
                ->getQuery();
            $client = $query->getArrayResult();
        }
        
        if(count($client) > 0){
            $found += count($client);
        }
        
        // admins
        if($user_type == 'admin-user'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.id != :owner_id')
                ->setParameter('email', $email)
                ->setParameter('owner_id', $user_id)
                ->getQuery();
            $admin = $query->getArrayResult();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email')
                ->setParameter('email', $email)
                ->getQuery();
            $admin = $query->getArrayResult();
        }
        if(count($admin) > 0){
            $found += count($admin);
        }
        
        
        // embassy
        if($user_type == 'embassy-user'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:EmbassyUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.id != :owner_id')
                ->setParameter('email', $email)
                ->setParameter('owner_id', $user_id)
                ->getQuery();
            $embassy = $query->getArrayResult();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:EmbassyUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email')
                ->setParameter('email', $email)
                ->getQuery();
            $embassy = $query->getArrayResult();
        }
        if(count($embassy) > 0){
            $found += count($embassy);
        }
                
        // tpn users
        if($user_type == 'tpn-user'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.id != :owner_id')
                ->setParameter('email', $email)
                ->setParameter('owner_id', $user_id)
                ->getQuery();
            $tpn = $query->getArrayResult();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeCLStpnBundle:TpnUser');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email')
                ->setParameter('email', $email)
                ->getQuery();
            $tpn = $query->getArrayResult();
        }
        
        if(count($tpn) > 0){
            $found += count($tpn);
        }
        
        return ($found > 0) ? false : true;
    }
    
    
    
    
    /**
     * add tpn letter to database
     * upon payment success
     * @param type $orderNo
     * @param type $cliendId
     */
    public function generateTPNletter($orderNo, $cliendId){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
            
        // delete tpn
        //$query = $em->createQuery('DELETE AcmeCLSclientGovBundle:Tpn t WHERE t.order_no = '.$model->getOrderNo());
        //$query->execute(); 
        
        $data = $this->getOrderDetailsByOrderNoAndClientId($orderNo, $cliendId);
        
        if($data['order_type'] == 2 || $data['order_type'] == 3){
            for($i=0; $i<count($data["travels"]); $i++){
                $tpn_no = $this->generateTpnNo();

                //$tpn_src='<div id="dfat-header" style="height: 116px; width: 116px; background-color: #c9c9c9; margin: auto;"></div>';
                $tpn_src='<div id="dfat-header" style="height: 90px; width: 90px; background-color: #c9c9c9; margin: auto;"></div>';
                $tpn_src .= '<div><p style="line-height: 25px;">Note No: <span class="tpnno">'. $tpn_no .'</span></p></div>';
                $tpn_src .= '<br/>';
                $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The Department of Foreign Affairs and Trade presents its compliments to the ". $data["travels"][$i]["rep_name"] ." and has the honour to request the issue of a visa endorsement in respect to the following:</p></div>";
                $tpn_src .= '<br/>';
                
                for($e=0; $e<count($data["travellers"]); $e++){
                    $tpn_src .= "<div><p style='line-height: 25px;'>";
                        $tpn_src .= "<strong>". $data["travellers"][$e]["title_name"] ." ". $data["travellers"][$e]["fname"] ." ". $data["travellers"][$e]["mname"] ." ". strtoupper($data["travellers"][$e]["lname"]) ." </strong><br/>";
                        $tpn_src .= "<strong>Passport Number:</strong> ".$data["travellers"][$e]["passport_number"]."<br/>";
                        $tpn_src .= "<strong>Date of Birth:</strong> ".date('d F Y', strtotime($data["travellers"][$e]["birth_date"]))."<br/>";
                        $tpn_src .= "<strong>Agency:</strong> ".$data["department_name"]."<br/>";
                        $tpn_src .= "<strong>Position:</strong> ".$data["travellers"][$e]["occupation"]."";
                    $tpn_src .= "</p></div>";
                }

                $tpn_src .= '<br/>';
                if(count($data["travellers"]) == 1){
                    if(strtolower(trim($data["travellers"][0]["occupation"])) == 'a dependant'){
                        $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." 
                        ". strtoupper($data["travellers"][0]["lname"])." is a dependant of ". $data["travellers"][0]["rpinfo_fullname"] ." who is ". $data["travellers"][0]["rpinfo_position_at_post"] .
                        " at the Australian ". $data["travellers"][0]["rpinfo_name_of_post"] ." in ". $data["travellers"][0]["rpinfo_city"] .". 
                        ".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"]).
                        " will be accompanying ". $data["travellers"][0]["rpinfo_fullname"] ." to ". $data["travels"][$i]["country_name_display"] ." for the duration of his posting. 
                        ".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"]).
                        " will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";

                    }elseif(strtolower(trim($data["travellers"][0]["occupation"])) == 'a spouse'){

                        $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." 
                        ". strtoupper($data["travellers"][0]["lname"])." is the spouse of ". $data["travellers"][0]["rpinfo_fullname"] ." who is ". $data["travellers"][0]["rpinfo_position_at_post"] .
                        " at the Australian ". $data["travellers"][0]["rpinfo_name_of_post"] ." in ". $data["travellers"][0]["rpinfo_city"] .". 
                        ".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"]).
                        " will be accompanying ". $data["travellers"][0]["rpinfo_fullname"] ." to ". $data["travels"][$i]["country_name_display"] ." for the duration of his posting. 
                        ".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"]).
                        " will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";
                    }else{
                        $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." 
                            ". strtoupper($data["travellers"][0]["lname"])." will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of 
                            ".$data["travels"][$i]["travel_purpose"].".  ".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." 
                            ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"])." will arrive on 
                            ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";
                    }

                }elseif(count($data["travellers"]) > 1){
                    $separates = 0;
                    $not_separates = 0;

                    for($e=0; $e<count($data["travellers"]); $e++){
                        if(strtolower(trim($data["travellers"][$e]["occupation"])) == 'a dependant' || strtolower(trim($data["travellers"][$e]["occupation"])) == 'a spouse'){
                            $separates += 1;
                        }else{
                            $not_separates += 1;
                        }
                    }
                    

                   if($separates > 0 && count($data["travellers"]) > 1){ 
                        // For the first paragraph


                        // If only 1 not dependant/spouse
                        if($not_separates == 1){
                            for($e=0; $e<count($data["travellers"]); $e++){
                                if(strtolower(trim($data["travellers"][$e]["occupation"])) != 'a dependant' && strtolower(trim($data["travellers"][$e]["occupation"])) != 'a spouse'){
                                    $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." 
                                        ". strtoupper($data["travellers"][$e]["lname"])." will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of 
                                        ".$data["travels"][$i]["travel_purpose"].".  ".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." 
                                        ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"])." will arrive on 
                                        ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";

                                }
                            }

                        // If only 2 not dependant/spouse
                        }elseif($not_separates == 2){
                            $applicants = '';
                            for($e=0; $e<count($data["travellers"]); $e++){
                                if(strtolower(trim($data["travellers"][$e]["occupation"])) != 'a dependant' && strtolower(trim($data["travellers"][$e]["occupation"])) != 'a spouse'){
                                    if($applicants == ''){
                                        $applicants = $data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"]);
                                    }else{
                                        $applicants .= " and " . $data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"]);
                                    }
                                }
                            }

                            $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>". $applicants ." will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of ".$data["travels"][$i]["travel_purpose"].".
                                ". $applicants ." will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".
                                </p></div>";

                        // If more than 2 not dependant/spouse
                        }else{

                            $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The above named persons will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of 
                                ".$data["travels"][$i]["travel_purpose"].". The above named persons will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";
                        }

                        // Next paragraph after the paragraph for not dependants/spouse
                        for($e=0; $e<count($data["travellers"]); $e++){

                            if(strtolower(trim($data["travellers"][$e]["occupation"])) == 'a dependant'){

                                $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." 
                                    ". strtoupper($data["travellers"][$e]["lname"])." is a dependant of ". $data["travellers"][$e]["rpinfo_fullname"] ." who is ". $data["travellers"][$e]["rpinfo_position_at_post"] .
                                    " at the Australian ". $data["travellers"][$e]["rpinfo_name_of_post"] ." in ". $data["travellers"][$e]["rpinfo_city"] .". 
                                    ".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"]).
                                    " will be accompanying ". $data["travellers"][$e]["rpinfo_fullname"] ." to ". $data["travels"][$i]["country_name_display"] ." for the duration of his posting. 
                                    ".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"]).
                                    " will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";

                            }elseif(strtolower(trim($data["travellers"][$e]["occupation"])) == 'a spouse'){

                                $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." 
                                    ". strtoupper($data["travellers"][$e]["lname"])." is the spouse of ". $data["travellers"][$e]["rpinfo_fullname"] ." who is ". $data["travellers"][$e]["rpinfo_position_at_post"] .
                                    " at the Australian ". $data["travellers"][$e]["rpinfo_name_of_post"] ." in ". $data["travellers"][$e]["rpinfo_city"] .". 
                                    ".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"]).
                                    " will be accompanying ". $data["travellers"][$e]["rpinfo_fullname"] ." to ". $data["travels"][$i]["country_name_display"] ." for the duration of his posting. 
                                    ".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"]).
                                    " will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";
                            }
                        }
                        
                    }elseif($separates == 0 && count($data["travellers"]) == 2){

                        $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"])." and 
                            ".$data["travellers"][1]["title_name"]." ".$data["travellers"][1]["fname"]." ".$data["travellers"][1]["mname"]." ". strtoupper($data["travellers"][1]["lname"])." 
                            will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of ".$data["travels"][$i]["travel_purpose"].". 
                            ".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"])." and 
                            ".$data["travellers"][1]["title_name"]." ".$data["travellers"][1]["fname"]." ".$data["travellers"][1]["mname"]." ". strtoupper($data["travellers"][1]["lname"])." 
                            will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";

                    }else{
                        $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The above named persons will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of 
                            ".$data["travels"][$i]["travel_purpose"].". The above named persons will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";
                    }
                }

                
                
                $tpn_src .= '<br/>';
                $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The Department of Foreign Affairs and Trade requests that the appropriate visa be inscribed in the enclosed passport(s).  For further information on this application the ".$data["travels"][$i]["rep_name"]." may contact <strong>".$data["pri_dept_contact_fname"]." ".$data["pri_dept_contact_lname"]."</strong> on tel. <strong>". $data['pri_dept_contact_phone'] .".</strong></p></div>";
                $tpn_src .= "<div>";
                $tpn_src .= '<br/>';
                $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The Department of Foreign Affairs and Trade avails itself of this opportunity to renew to the ".$data["travels"][$i]["rep_name"]." the assurances of its highest consideration.</p></div>";
                $tpn_src .= '<br/><p></p>';
                $tpn_src .= '<div><div id="dfat-footer" style="height: 140px; width: 140px; background-color: #c9c9c9;"></div><br/>CANBERRA ACT<br/>[DateOfIssue]</div>';
                $tpn_src .= "</div>";
                // new tpn
                $model = new Tpn();
                $model->setTpnNo($tpn_no);
                $model->setOrderNo($orderNo);
                $model->setClientId($cliendId);
                $model->setTpnSrc($tpn_src);
                $model->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $model->setDateLastUpdated($datetime->format("Y-m-d H:i:s"));
                $model->setDestination($data["travels"][$i]["country_id"]);
                $model->setDepartureDate($data["travels"][$i]["departure_date"]);
                $model->setEntryOption($data["travels"][$i]["entry_option"]);
                $model->setEntryDateCountry($data["travels"][$i]["entry_date_country"]);
                $model->setDepartureDateCountry($data["travels"][$i]["departure_date_country"]);
                $model->setTravelPurpose($data["travels"][$i]["travel_purpose"]);
                $model->setStatus(0);

                $em->persist($model);
                $em->flush();

            }
        }
        $em->getConnection()->commit(); 
    }
    
    
    public function generateTPNletterArchive($orderNo, $cliendId){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        $baseurl = 'http://cls.vcdev.net/web';
            
        // delete tpn
        //$query = $em->createQuery('DELETE AcmeCLSclientGovBundle:Tpn t WHERE t.order_no = '.$model->getOrderNo());
        //$query->execute(); 
        
        $data = $this->getOrderDetailsByOrderNoAndClientId($orderNo, $cliendId);
        
        if($data['order_type'] == 2 || $data['order_type'] == 3){
            for($i=0; $i<count($data["travels"]); $i++){
                $tpn_no = $this->generateTpnNo();

                if($data["travels"][$i]["tpn_stat"] != 0){
                    $tpn_src='<div id="dfat-header" style="height: 90px; width: 90px; background-color: #ffffff; margin: auto;"><img src="'. $baseurl.'/img/dfat-header.png" style="height: 90px; width: auto;"/></div>';
                }else{
                    $tpn_src='<div id="dfat-header" style="height: 90px; width: 90px; background-color: #c9c9c9; margin: auto;"></div>';
                }
                
                $tpn_src .= '<div><p style="line-height: 25px;">Note No: <span class="tpnno">'. $tpn_no .'</span></p></div>';
                $tpn_src .= '<br/>';
                $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The Department of Foreign Affairs and Trade presents its compliments to the ". $data["travels"][$i]["rep_name"] ." and has the honour to request the issue of a visa endorsement in respect to the following:</p></div>";
                $tpn_src .= '<br/>';
                
                for($e=0; $e<count($data["travellers"]); $e++){
                    $tpn_src .= "<div><p style='line-height: 25px;'>";
                        $tpn_src .= "<strong>". $data["travellers"][$e]["title_name"] ." ". $data["travellers"][$e]["fname"] ." ". $data["travellers"][$e]["mname"] ." ". strtoupper($data["travellers"][$e]["lname"]) ." </strong><br/>";
                        $tpn_src .= "<strong>Passport Number:</strong> ".$data["travellers"][$e]["passport_number"]."<br/>";
                        $tpn_src .= "<strong>Date of Birth:</strong> ".date('d F Y', strtotime($data["travellers"][$e]["birth_date"]))."<br/>";
                        $tpn_src .= "<strong>Agency:</strong> ".$data["department_name"]."<br/>";
                        $tpn_src .= "<strong>Position:</strong> ".$data["travellers"][$e]["occupation"]."";
                    $tpn_src .= "</p></div>";
                }
                $tpn_src .= '<br/>';

                if(count($data["travellers"]) == 1){
                    $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." 
                        ". strtoupper($data["travellers"][0]["lname"])." will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of 
                        ".$data["travels"][$i]["travel_purpose"].".  ".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." 
                        ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"])." will arrive on 
                        ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";
                    

                }elseif(count($data["travellers"]) > 1){
                    $separates = 0;
                    $not_separates = 0;

                    for($e=0; $e<count($data["travellers"]); $e++){
                        if(strtolower(trim($data["travellers"][$e]["occupation"])) == 'a dependant' || strtolower(trim($data["travellers"][$e]["occupation"])) == 'a spouse'){
                            $separates += 1;
                        }else{
                            $not_separates += 1;
                        }
                    }
                    

                   if($separates > 0 && count($data["travellers"]) > 1){ 
                        // For the first paragraph


                        // If only 1 not dependant/spouse
                        if($not_separates == 1){
                            for($e=0; $e<count($data["travellers"]); $e++){
                                if(strtolower(trim($data["travellers"][$e]["occupation"])) != 'a dependant' && strtolower(trim($data["travellers"][$e]["occupation"])) != 'a spouse'){
                                    $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." 
                                        ". strtoupper($data["travellers"][$e]["lname"])." will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of 
                                        ".$data["travels"][$i]["travel_purpose"].".  ".$data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." 
                                        ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"])." will arrive on 
                                        ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";

                                }
                            }

                        // If only 2 not dependant/spouse
                        }elseif($not_separates == 2){
                            $applicants = '';
                            for($e=0; $e<count($data["travellers"]); $e++){
                                if(strtolower(trim($data["travellers"][$e]["occupation"])) != 'a dependant' && strtolower(trim($data["travellers"][$e]["occupation"])) != 'a spouse'){
                                    if($applicants == ''){
                                        $applicants = $data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"]);
                                    }else{
                                        $applicants .= " and " . $data["travellers"][$e]["title_name"]." ".$data["travellers"][$e]["fname"]." ".$data["travellers"][$e]["mname"]." ". strtoupper($data["travellers"][$e]["lname"]);
                                    }
                                }
                            }

                            $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>". $applicants ." will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of ".$data["travels"][$i]["travel_purpose"].".
                                ". $applicants ." will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".
                                </p></div>";

                        // If more than 2 not dependant/spouse
                        }else{

                            $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The above named persons will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of 
                                ".$data["travels"][$i]["travel_purpose"].". The above named persons will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";
                        }

                        
                        
                    }elseif($separates == 0 && count($data["travellers"]) == 2){

                        $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"])." and 
                            ".$data["travellers"][1]["title_name"]." ".$data["travellers"][1]["fname"]." ".$data["travellers"][1]["mname"]." ". strtoupper($data["travellers"][1]["lname"])." 
                            will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of ".$data["travels"][$i]["travel_purpose"].". 
                            ".$data["travellers"][0]["title_name"]." ".$data["travellers"][0]["fname"]." ".$data["travellers"][0]["mname"]." ". strtoupper($data["travellers"][0]["lname"])." and 
                            ".$data["travellers"][1]["title_name"]." ".$data["travellers"][1]["fname"]." ".$data["travellers"][1]["mname"]." ". strtoupper($data["travellers"][1]["lname"])." 
                            will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";

                    }else{
                        $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The above named persons will be travelling to ".$data["travels"][$i]["country_name_display"]." for the purpose of 
                            ".$data["travels"][$i]["travel_purpose"].". The above named persons will arrive on ".date('d F Y', strtotime($data["travels"][$i]["entry_date_country"]))." and depart on ".date('d F Y', strtotime($data["travels"][$i]["departure_date_country"])).".</p></div>";
                    }
                }

                
                $tpn_src .= "<div>";
                $tpn_src .= '<br/>';
                $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The Department of Foreign Affairs and Trade requests that the appropriate visa be inscribed in the enclosed passport(s).  For further information on this application the ".$data["travels"][$i]["rep_name"]." may contact <strong>".$data["pri_dept_contact_fname"]." ".$data["pri_dept_contact_lname"]."</strong> on tel. <strong>".$data["pri_dept_contact_phone"].".</strong></p></div>";
                $tpn_src .= '<br/>';
                $tpn_src .= "<div><p style='text-align: justify; line-height: 25px;'>The Department of Foreign Affairs and Trade avails itself of this opportunity to renew to the ".$data["travels"][$i]["rep_name"]." the assurances of its highest consideration.</p></div>";
                $tpn_src .= '<br/><p></p>';
                if($data["travels"][$i]["tpn_stat"] != 0){
                    $tpn_src .= '<div><div id="dfat-footer" style="height: 140px; width: 140px; background-color: #ffffff;"><img src="'. $baseurl.'/img/dfat-footer.png" style="height: 140px; width: auto;"/></div><br/>CANBERRA ACT<br/>'.date('d F Y', strtotime($data["travels"][$i]["tpn_date_issued"])).'</div>';
                }else{
                    $tpn_src .= '<div><div id="dfat-footer" style="height: 140px; width: 140px; background-color: #c9c9c9;"></div><br/>CANBERRA ACT<br/>[DateOfIssue]</div>';
                }
                $tpn_src .= "</div>";
                // new tpn
                $model = new Tpn();
                $model->setTpnNo($tpn_no);
                $model->setOrderNo($orderNo);
                $model->setClientId($cliendId);
                $model->setTpnSrc($tpn_src);
                $model->setDateSubmitted($data['date_submitted']);
                $model->setDateLastUpdated($data['date_submitted']);
                $model->setDestination($data["travels"][$i]["country_id"]);
                $model->setDepartureDate($data["travels"][$i]["departure_date"]);
                $model->setEntryOption($data["travels"][$i]["entry_option"]);
                $model->setEntryDateCountry($data["travels"][$i]["entry_date_country"]);
                $model->setDepartureDateCountry($data["travels"][$i]["departure_date_country"]);
                $model->setTravelPurpose($data["travels"][$i]["travel_purpose"]);
                $model->setStatus($data["travels"][$i]["tpn_stat"]);
                if($data["travels"][$i]["tpn_stat"] != 0){
                    $model->setDateIssued($data["travels"][$i]["tpn_date_issued"]);
                }
                $em->persist($model);
                $em->flush();

            }
        }
        $em->getConnection()->commit(); 
    }
    
    
    public function getAdminUsers(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:AdminUser');
        $query = $cust->createQueryBuilder('p')
            ->where('p.s_enabled = 1')
            ->getQuery();
        return $query->getArrayResult();
    }
    
    
    
    
    public function getPageContentByNo($no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:ContentPages');
        $query = $cust->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameter('id', $no)
            ->getQuery();
        $data =  $query->getArrayResult();
        return (count($data) > 0) ? $data[0] : array();
    }
    
    
    public function getCarouselImages(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:ImageSlider');
        $query = $cust->createQueryBuilder('p')
            ->where('p.s_enabled = 1')
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getHomeAdsImages(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:HomeAds');
        $query = $cust->createQueryBuilder('p')
            ->where('p.s_enabled = 1')
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getTravelAlerts(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:TravelAlerts');
        $query = $cust->createQueryBuilder('p')
            ->where('p.status = :status')
            ->setParameter('status','finalised')
            ->orderBy("p.alert_date", "Desc")
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getBulkOrderDetails($bulk_order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisa');
        $query = $cust->createQueryBuilder('p')
            ->select("p.bulk_order_no, p.client_id, p.status, p.level, p.discount_rate, p.discount_code, p.grand_total,
                u.can_get_special_price, u.special_price, u.can_charge_cost_to_account, u.account_no as user_account_no, 
                u.type as client_type, u.fname as client_fname, u.lname as client_lname, p.payment_option,
                p.dd_company, p.dd_doc_return_address, p.dd_city, p.dd_state, p.dd_postcode, p.dd_fname, p.dd_lname, p.dd_contact_no, p.dd_additional_comment ")
            ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = p.client_id')
            ->where('p.bulk_order_no = :bulk_order_no')
            ->setParameter('bulk_order_no', $bulk_order_no)
            ->getQuery();
        $data = $query->getArrayResult();
        
        if(count($data) > 0){
            $post = array(
                'bulk_order_no'=> $data[0]['bulk_order_no'],
                'client_id'=> $data[0]['client_id'],
                'status'=> $data[0]['status'],
                'level'=> $data[0]['level'],
                'discount_rate'=> $data[0]['discount_rate'],
                'discount_code'=> $data[0]['discount_code'],
                'grand_total'=> $data[0]['grand_total'],
                'payment_option'=> $data[0]['payment_option'],
                'items'=> $this->getBulkOrderItems($bulk_order_no),
                
                'can_get_special_price'=> $data[0]['can_get_special_price'],
                'special_price'=> $data[0]['special_price'],
                'can_charge_cost_to_account'=> $data[0]['can_charge_cost_to_account'],
                'user_account_no'=> $data[0]['user_account_no'],
                'client_type'=> $data[0]['client_type'],
                'client_fname'=> $data[0]['client_fname'],
                'client_lname'=> $data[0]['client_lname'],
                
                'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                
                'dd_company'=> $data[0]['dd_company'],
                'dd_doc_return_address'=> $data[0]['dd_doc_return_address'],
                'dd_city'=> $data[0]['dd_city'],
                'dd_state'=> $data[0]['dd_state'],
                'dd_postcode'=> $data[0]['dd_postcode'],
                'dd_fname'=> $data[0]['dd_fname'],
                'dd_lname'=> $data[0]['dd_lname'],
                'dd_contact_no'=> $data[0]['dd_contact_no'],
                'dd_additional_comment'=> $data[0]['dd_additional_comment']
                
            );
        }else{
            $post = array();
        }
        
        return $post;
    }
    
    public function getTitleNameById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:NameTitle');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.title as title_name")
            ->where("p.id= :id")
            ->setParameter("id",$id)
            ->getQuery();
        $result = $query->getArrayResult();
        return (count($result) > 0) ? $result[0]['title_name'] : "";
    }
    
    public function getBulkOrderItems($bulk_order_no){
        $filelocation = "publicvisafiles";
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisaDetails');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.bulk_order_no, p.destination, c.countryName as destination_name, p.departure_date, p.entry_date_country, p.departure_date_country, p.travel_purpose,
                p.selected_visa_type, CONCAT(CONCAT(vt.type, ' - $'), vt.cost) as selected_visa_type_name, vt.file_attachment, p.selected_visa_type_price, p.selected_visa_type_requirements, 
                p.visa_courier, co.type as visa_courier_type, p.visa_courier_price, p.is_smart_traveller, p.dd_company, p.dd_doc_return_address, p.dd_city, p.dd_state, p.dd_postcode,
                p.dd_fname, p.dd_lname, p.dd_contact_no, p.dd_additional_comment, p.discount_rate, p.discount_code, p.total, p.final_total, p.generated_order_no, p.travellers")
            ->leftJoin('AcmeCLSadminBundle:PublicVisaTypes', 'vt', 'WITH', 'vt.id = p.selected_visa_type')
            ->leftJoin('AcmeCLSadminBundle:VisaCourierOptions', 'co', 'WITH', 'co.id = p.visa_courier')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.destination')
            ->where('p.bulk_order_no = :bulk_order_no')
            ->setParameter('bulk_order_no', $bulk_order_no)
            ->getQuery();
        $data = $query->getArrayResult();
        
        $post = array();
        for($i=0; $i<count($data); $i++){
            $traveller_json = json_decode($data[$i]['travellers'], true);
            $traveller = array();
            for($e=0; $e<count($traveller_json); $e++){
                $traveller[] = array(
                    'title'=> $traveller_json[$e]['title'],
                    'title_name'=> $this->getTitleNameById($traveller_json[$e]['title']),
                    'fname'=> $traveller_json[$e]['fname'],
                    'mname'=> $traveller_json[$e]['mname'],
                    'lname'=> $traveller_json[$e]['lname'],
                    'email'=> $traveller_json[$e]['email'],
                    'phone'=> $traveller_json[$e]['phone'],
                    'nationality'=> $traveller_json[$e]['nationality'],
                    'nationality_name'=> $this->getCountryNameById($traveller_json[$e]['nationality']),
                    'passport_type'=> $traveller_json[$e]['passport_type'],
                    'passport_type_name'=> $this->getPassportTypeNameById($traveller_json[$e]['passport_type']),
                    'passport_number'=> $traveller_json[$e]['passport_number']
                );
            }
            
            $post[] = array(
                'id'=> $data[$i]['id'],
                'bulk_order_no'=> $data[$i]['bulk_order_no'],
                'destination'=> $data[$i]['destination'],
                'destination_name'=> $data[$i]['destination_name'],
                'departure_date'=> $data[$i]['departure_date'],
                'entry_date_country'=> $data[$i]['entry_date_country'],
                'departure_date_country'=> $data[$i]['departure_date_country'],
                'travel_purpose'=> $data[$i]['travel_purpose'],
                
                'visa_courier'=> $data[$i]['visa_courier'],
                'visa_courier_type'=> $data[$i]['visa_courier_type'],
                'visa_courier_price'=> $data[$i]['visa_courier_price'],
                
                'dd_company'=> $data[$i]['dd_company'],
                'dd_doc_return_address'=> $data[$i]['dd_doc_return_address'],
                'dd_city'=> $data[$i]['dd_city'],
                'dd_state'=> $data[$i]['dd_state'],
                'dd_postcode'=> $data[$i]['dd_postcode'],
                'dd_fname'=> $data[$i]['dd_fname'],
                'dd_lname'=> $data[$i]['dd_lname'],
                'dd_contact_no'=> $data[$i]['dd_contact_no'],
                'dd_additional_comment'=> $data[$i]['dd_additional_comment'],
                'is_smart_traveller'=> $data[$i]['is_smart_traveller'],
                'discount_rate'=> $data[$i]['discount_rate'],
                'discount_code'=> $data[$i]['discount_code'],
                'total'=> $data[$i]['total'],
                'final_total'=> $data[$i]['final_total'],
                'selected_visa_type'=> $data[$i]['selected_visa_type'],
                'selected_visa_type_price'=> $data[$i]['selected_visa_type_price'],
                'selected_visa_type_requirements'=> $data[$i]['selected_visa_type_requirements'],
                'file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/'.$filelocation.'/'.$data[$i]['destination'].'/'.$data[$i]['selected_visa_type'].'/'. $data[$i]['file_attachment'],
                'file_attached'=>$data[$i]['file_attachment'],
                'generated_order_no'=> $data[$i]['generated_order_no'],
                'travellers'=> $traveller
            );
        }
        
        return $post;
    }
    public function getBulkOrderItems1($bulk_order_no){
        $filelocation = "publicvisafiles";
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisaDetails');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id, p.bulk_order_no, p.destination, c.countryName as destination_name, p.departure_date, p.entry_date_country, p.departure_date_country, p.travel_purpose,
                p.selected_visa_type, CONCAT(CONCAT(vt.type, ' - $'), vt.cost) as selected_visa_type_name, vt.file_attachment, p.selected_visa_type_price, p.selected_visa_type_requirements, 
                p.visa_courier, co.type as visa_courier_type, p.visa_courier_price, p.traveller_title, p.traveller_fname, p.traveller_mname,
                p.traveller_lname, p.traveller_email, p.traveller_occupation, p.traveller_phone, p.traveller_bday, p.traveller_passport_type,
                p.traveller_nationality, p.traveller_passport_no, p.is_smart_traveller, p.dd_company, p.dd_doc_return_address, p.dd_city, p.dd_state, p.dd_postcode,
                p.dd_fname, p.dd_lname, p.dd_contact_no, p.dd_additional_comment, p.discount_rate, p.discount_code, p.total, p.final_total, p.generated_order_no")
            ->leftJoin('AcmeCLSadminBundle:PublicVisaTypes', 'vt', 'WITH', 'vt.id = p.selected_visa_type')
            ->leftJoin('AcmeCLSadminBundle:VisaCourierOptions', 'co', 'WITH', 'co.id = p.visa_courier')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.destination')
            ->where('p.bulk_order_no = :bulk_order_no')
            ->setParameter('bulk_order_no', $bulk_order_no)
            ->getQuery();
        $data = $query->getArrayResult();
        //print_r($this->extractSelectedVisaTypeRequirements($data[], 'public'));
        $post = array();
        for($i=0; $i<count($data); $i++){
            $vtrs =  $this->extractSelectedVisaTypeRequirementsImplode($data[$i]['selected_visa_type_requirements'], 'public');
            $post[] = array(
                'id'=> $data[$i]['id'],
                'bulk_order_no'=> $data[$i]['bulk_order_no'],
                'destination'=> $data[$i]['destination'],
                'destination_name'=> $data[$i]['destination_name'],
                'departure_date'=> $data[$i]['departure_date'],
                'entry_date_country'=> $data[$i]['entry_date_country'],
                'departure_date_country'=> $data[$i]['departure_date_country'],
                'travel_purpose'=> $data[$i]['travel_purpose'],
                'selected_visa_type'=> $data[$i]['selected_visa_type'],
                'selected_visa_type_name'=> $data[$i]['selected_visa_type_name'],
                'selected_visa_type_price'=> $data[$i]['selected_visa_type_price'],
                'selected_visa_type_requirements'=> $data[$i]['selected_visa_type_requirements'],
                'selected_visa_type_requirements_ids'=> $vtrs['ids'],
                'selected_visa_type_requirements_names'=> $vtrs['names'],
                'selected_visa_type_requirements_array'=> $this->extractSelectedVisaTypeRequirements($data[$i]['selected_visa_type_requirements'], 'public'),
                'visa_courier'=> $data[$i]['visa_courier'],
                'visa_courier_type'=> $data[$i]['visa_courier_type'],
                'visa_courier_price'=> $data[$i]['visa_courier_price'],
                'traveller_title'=> $data[$i]['traveller_title'],
                'traveller_fname'=> $data[$i]['traveller_fname'],
                'traveller_mname'=> $data[$i]['traveller_mname'],
                'traveller_lname'=> $data[$i]['traveller_lname'],
                'traveller_email'=> $data[$i]['traveller_email'],
                'traveller_occupation'=> $data[$i]['traveller_occupation'],
                'traveller_phone'=> $data[$i]['traveller_phone'],
                'traveller_bday'=> $data[$i]['traveller_bday'],
                'traveller_passport_type'=> $data[$i]['traveller_passport_type'],
                'traveller_nationality'=> $data[$i]['traveller_nationality'],
                'traveller_passport_no'=> $data[$i]['traveller_passport_no'],
                'dd_company'=> $data[$i]['dd_company'],
                'dd_doc_return_address'=> $data[$i]['dd_doc_return_address'],
                'dd_city'=> $data[$i]['dd_city'],
                'dd_state'=> $data[$i]['dd_state'],
                'dd_postcode'=> $data[$i]['dd_postcode'],
                'dd_fname'=> $data[$i]['dd_fname'],
                'dd_lname'=> $data[$i]['dd_lname'],
                'dd_contact_no'=> $data[$i]['dd_contact_no'],
                'dd_additional_comment'=> $data[$i]['dd_additional_comment'],
                'is_smart_traveller'=> $data[$i]['is_smart_traveller'],
                'discount_rate'=> $data[$i]['discount_rate'],
                'discount_code'=> $data[$i]['discount_code'],
                'total'=> $data[$i]['total'],
                'final_total'=> $data[$i]['final_total'],
                'file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/'.$filelocation.'/'.$data[$i]['destination'].'/'.$data[$i]['selected_visa_type'].'/'. $data[$i]['file_attachment'],
                'file_attached'=>$data[$i]['file_attachment'],
                'generated_order_no'=> $data[$i]['generated_order_no']
            );
        }
        
        return $post;
    }
    
    public function sOrderNoExists($order_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
        $query = $cust->createQueryBuilder('p')
            ->where('p.order_no = :order_no AND p.status >= 10')
            ->setParameter('order_no',$order_no)
            ->getQuery();
        $data = $query->getArrayResult();
        
        return (count($data) > 0) ? true : false;
    }
    
    
    function printPDF($html){

    include (dirname($this->container->get('kernel')->getRootdir()).'/web/api/pdfc/html2pdf.class.php');
    $html2pdf = new \HTML2PDF('L', array(200, 200), 'en', true, 'UTF-8', array(5, 3, 5, 3));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    $html2pdf->WriteHTML($html);
    $html2pdf->Output(); //
    }
    
    public function reprintAction(){
        if(isset($_POST['reprint'])){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $app = '';
        
        switch ($_POST['app_type']){
            case 'public-visa':
                $app = 'ApplicationPublicVisa';
                break;
            case 'visa':
                $app = 'ApplicationVisa';
                break;
            case 'police':
                $app = 'ApplicationPoliceClearance';
                break;
            case 'passport':
                $app = 'ApplicationPassportOfficePickupOrDelivery';
                break;
            case 'tpn':
                $app = 'ApplicationTPN';
                break;
            case 'russian-visa':
                $app = 'ApplicationRussianVisaVoucher';
                break;
            case 'delivery':
                $app = 'ApplicationDocumentDelivery';
                break;
        }
        
        $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
        $client_id = intval($client_id);

        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));

        $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);

        $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));

        $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
        
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsPassport');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = 1')
                ->getQuery();
            $settings = $query->getArrayResult();
            
        $this->printPDF(
                    $this->renderView('AcmeCLSclientGovBundle:'.$app.':invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNumber"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings,
                            'settings_passport'=>$settings[0]
                            )
                        )
                );

        // manual order email to admin
        if($session->get('user_type') == 'admin-user'){
                $this->printPDF(
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'tpn_settings'=>$tpn_settings,
                                    'manual_order'=>1
                                    )
                                )
                        );
        }

            if(trim($_SERVER['HTTP_HOST']) != 'localhost'){

                     $this->printPDF(
                    $this->renderView('AcmeCLSclientGovBundle:'.$app.':invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNumber"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings,
                            'settings_passport'=>$settings[0]
                            )
                        )
                );
            }


            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_home'));
            }
        }else{
            die();
        }
    }
    
    
    // added by leokarl 2-28-2015
    public function getNearestCapitalCities(){
        return array("Canberra", "Sydney", "Brisbane", "Darwin", "Perth", "Adelaide", "Melbourne", "Hobart");
    }
    
    // added by leokarl 3-2-2015
    public function createLog($area, $userId, $userType, $logDetails)
    {
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getManager();
        //$em->getConnection()->beginTransaction();
        
        $log = new Logs();
        $log->setArea($area);
        $log->setUserId($userId);
        $log->setUserType($userType);
        $log->setLogDatetime($datetime->format('Y-m-d H:i:s'));
        $log->setLogDetails($logDetails);
        $em->persist($log);
        $em->flush();
        
        //$em->getConnection()->commit(); 
    }
    
    // added by leokarl 3-3-2015
    public function getOrderTypeName($order_type_id){
        $order_type_name = '';
        if($order_type_id == 1){
            $order_type_name = 'Visa';
        }elseif($order_type_id == 2){
            $order_type_name = 'Third Person Note (TPN)';
        }elseif($order_type_id == 3){
            $order_type_name = 'TPN + Visa';
        }elseif($order_type_id == 4){
            $order_type_name = 'Passport Office Pickup or Delivery';
        }else if($order_type_id == 5){
            $order_type_name = 'Police Clearance';
        }else if($order_type_id == 6){
            $order_type_name = 'Public Visa';
        }else if($order_type_id == 7){
            $order_type_name = 'Document Delivery';
        }else if($order_type_id == 8){
            $order_type_name = 'Russian Visa Voucher';
        }else if($order_type_id == 9){
            $order_type_name = 'Document Legalisation';
        }
        
        return $order_type_name;
    }
    
    // added by leokarl 5-13-2015
    public function getStates(){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:States');
        $query = $cust->createQueryBuilder('p')
            ->orderBy("p.s_main", "ASC")
            ->getQuery();
        return $query->getArrayResult();
    }
}

