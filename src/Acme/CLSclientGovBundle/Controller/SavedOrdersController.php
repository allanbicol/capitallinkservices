<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Model;

class SavedOrdersController extends GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        $session->set('page_name', 'drafts');
        return $this->render('AcmeCLSclientGovBundle:SavedOrders:index.html.twig');
    }
    
    
    public function openSavedOrderAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        if($session->get('email') == '' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        if($session->get('user_type') == 'admin-user'){
            $client_id = $_GET['client'];
        }else{
            $client_id = $session->get('client_id');
        }
        $client_id = intval($client_id);
            
        
        if(!isset($_GET['type']) || trim($_GET['type']) == 'order'){
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_GET["order"], 'client_id'=>$client_id));



            if($model->getOrderType() == 1){ // visa application
                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_visa') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_visa') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_visa_options') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_visa_options') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_visa_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_visa_review_order') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 4){
                    // return to 4th step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_visa_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_visa_place_order') . "?order=".$model->getOrderNo());
                    }

                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }

            }elseif($model->getOrderType() == 2){ // tpn application

                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 4){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_place_order') . "?order=".$model->getOrderNo());
                    }
                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }
            }elseif($model->getOrderType() == 3){ // tpn + visa application

                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_tpn') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_tpn') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 4){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 5){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_place_order') . "?order=".$model->getOrderNo());
                    }
                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }



            }elseif($model->getOrderType() == 4){ // passport

                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_place_order') . "?order=".$model->getOrderNo());
                    }

                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }




            }elseif($model->getOrderType() == 5){ // police clearance

                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?order=".$model->getOrderNo());
                    }

                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }



            }elseif($model->getOrderType() == 6){ // visa application
                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_options') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_options') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_review_order') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 4){
                    // return to 4th step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_place_order') . "?order=".$model->getOrderNo());
                    }
                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }

            }elseif($model->getOrderType() == 7){ // document delivery

                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_place_order') . "?order=".$model->getOrderNo());
                    }

                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }



            }elseif($model->getOrderType() == 8){ // russian visa voucher

                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_details') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_details') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_options') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_options') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_place_order') . "?order=".$model->getOrderNo());
                    }

                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }



            }elseif($model->getOrderType() == 9){ // document legalisation

                if($model->getStatus() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_document_legalisation_details') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_document_legalisation_details') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 2){
                    // return to 2nd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_document_legalisation_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_document_legalisation_review_order') . "?order=".$model->getOrderNo());
                    }

                }elseif($model->getStatus() == 3){
                    // return to 3rd step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_place_order') . "?order=".$model->getOrderNo());
                    }

                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }



            }
        }else{
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisa')->findOneBy(array('bulk_order_no'=>$_GET["order"], 'client_id'=>$client_id));
            
            if($model->getStatus() == 'draft'){
                if($model->getLevel() == 1){
                    // return to 1st step
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa') . "?client=".$client_id."&order=".$model->getBulkOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa') . "?order=".$model->getBulkOrderNo());
                    }

                }elseif($model->getLevel() == 2){
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?client=".$client_id."&order=".$model->getBulkOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?order=".$model->getBulkOrderNo());
                    }

                }elseif($model->getLevel() == 3){
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_place_order') . "?client=".$client_id."&order=".$model->getBulkOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_place_order') . "?order=".$model->getBulkOrderNo());
                    }
                }else{
                    return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                            array('title'=> 'Error',
                                'message'=> 'Order Not Found!'));
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:SavedOrders:error.html.twig',
                        array('title'=> 'Error',
                            'message'=> 'Order Not Found!'));
            }
        }
        
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("(SELECT a.order_no, 
                    DATE_FORMAT(a.date_last_saved,'%d-%m-%Y %H:%i:%s') as date_last_saved, 
                    (CASE  
                        WHEN a.order_type = 1 THEN 'VISA' 
                        WHEN a.order_type = 2 THEN 'TPN'  
                        WHEN a.order_type = 3 THEN 'TPN+VISA'  
                        WHEN a.order_type = 4 THEN 'COURIER'  
                        WHEN a.order_type = 5 THEN 'POLICE CLEARANCE'  
                        WHEN a.order_type = 6 THEN 'VISA'  
                        WHEN a.order_type = 7 THEN 'DOCUMENT DELIVERY'  
                        WHEN a.order_type = 8 THEN 'RUSSIAN VISA VOUCHER'  
                        WHEN a.order_type = 9 THEN 'DOCUMENT LEGALISATION'  
                        ELSE 'Undefined'
                    END) as ticket_type,
                    a.primary_traveller_name,
                    b.country_name as destination,
                    a.departure_date,
                    'order' as order_type
                FROM tbl_orders a 
                LEFT JOIN tbl_countries b ON b.id = a.destination
                WHERE a.client_id=".$session->get('client_id')."
                    AND a.status < 10)
                    
                UNION

                (SELECT 
                        b.bulk_order_no AS order_no,
                        DATE_FORMAT(b.date_last_saved,'%d-%m-%Y %H:%i:%s') AS date_last_saved,
                        'BULK VISA' AS ticket_type,
                        '' AS primary_traveller_name,
                        '' AS destination,
                        '' AS departure_date,
                        'bulk' as order_type
                FROM tbl_order_bulk_public_visa b
                WHERE b.client_id=".$session->get('client_id')." 
                    AND b.status = 'draft')
                ");
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
            $fieldNames = array("date_last_saved","ticket_type","primary_traveller_name","destination","departure_date","order_type","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    /**
     * TPN saved order deletion
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteSavedOrderAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('email') != ''){
            $_POST['order_no'] = filter_var($_POST['order_no'], FILTER_SANITIZE_STRING);
            
            if($_POST['type'] == 'order'){
                $data = $this->getOrderDetailsByOrderNoAndClientId($_POST['order_no'], $session->get('client_id'));

                if(count($data) > 0){

                    $em = $this->getDoctrine()->getEntityManager();
                    $em->getConnection()->beginTransaction();
                    $connection = $em->getConnection();

                    // destinations
                    $statement = $connection->prepare("DELETE FROM tbl_order_destinations WHERE order_no=".$data['order_no']);
                    $statement->execute();

                    // travellers
                    $statement = $connection->prepare("DELETE FROM tbl_order_travellers WHERE order_no=".$data['order_no']);
                    $statement->execute();


                    // order
                    $statement = $connection->prepare("DELETE FROM tbl_orders WHERE order_no=".$data['order_no']);
                    $statement->execute();

                    $em->getConnection()->commit(); 

                    return new Response("success");
                }else{
                    return new Response("session expired");
                }
            }else{
                $_POST['order_no'] = intval($_POST['order_no']);
                $data = $this->getBulkOrderDetails($_POST['order_no']);

                if(count($data) > 0){

                    $em = $this->getDoctrine()->getEntityManager();
                    $em->getConnection()->beginTransaction();
                    $connection = $em->getConnection();

                    $statement = $connection->prepare("DELETE FROM tbl_order_bulk_public_visa_details WHERE bulk_order_no=".$_POST['order_no']);
                    $statement->execute();

                    $statement = $connection->prepare("DELETE FROM tbl_order_bulk_public_visa WHERE bulk_order_no=".$_POST['order_no']);
                    $statement->execute();


                    $em->getConnection()->commit(); 

                    return new Response("success");
                }else{
                    return new Response("session expired");
                }
            }
        }
    }

}
