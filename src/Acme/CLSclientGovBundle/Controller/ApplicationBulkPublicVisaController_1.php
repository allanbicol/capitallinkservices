<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

use Acme\CLSclientGovBundle\Entity\OrderBulkPublicVisa;
use Acme\CLSclientGovBundle\Entity\OrderBulkPublicVisaDetails;

class ApplicationBulkPublicVisaController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-bulk-public-visa');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getManager();
        
        if($session->get('user_type') != 'bulk-visa' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hid_submit'])){

            $em->getConnection()->beginTransaction();
            $error_count = 0;
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);

            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));

            // new order
            if(!isset($_POST["orderNumber"])){
                $model = new OrderBulkPublicVisa();
                $model->setClientId($client_id);
            // update order
            }else{
                $_POST["orderNumber"] = intval($_POST["orderNumber"]);
                $model = $em->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisa')->findOneBy(array('bulk_order_no'=>$_POST["orderNumber"]));

            }
            $model->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            $model->setStatus('draft'); 
            if($_POST["hid_submit"] == 1){
                $model->setLevel(2);
            }else{
                $model->setLevel(1);
            }

            $em->persist($model);
            $em->flush();


            $query = $em->createQuery('DELETE AcmeCLSclientGovBundle:OrderBulkPublicVisaDetails c WHERE c.bulk_order_no = '.$model->getBulkOrderNo());
            $query->execute(); 

            $grand_total = 0;
            $grand_gst = 0;
            for($i=0; $i<count($_POST['destination']); $i++){
                if(trim($_POST['destination'][$i]) != ""){
                    $total = 0;
                    $gst = 0;
                    
                    $visa_type = $em->getRepository('AcmeCLSadminBundle:PublicVisaTypes')->findOneBy(array('id'=>$_POST['visaType'][$i]));
                    $visa_courier = $em->getRepository('AcmeCLSadminBundle:VisaCourierOptions')->findOneBy(array('id'=>$_POST["courier"][$i]));
                    $visa_type_cost = (count($visa_type) > 0) ? $visa_type->getCost() : 0;
                    $visa_courier_cost = (count($visa_courier) > 0) ? $visa_courier->getCost() : 0;
                    
                    
                    $od = new OrderBulkPublicVisaDetails();
                    $od->setBulkOrderNo($model->getBulkOrderNo());
                    $od->setDestination($_POST['destination'][$i]);
                    $od->setDepartureDate($mod->changeFormatToOriginal($_POST['departure_date'][$i]));
                    $od->setEntryDateCountry($mod->changeFormatToOriginal($_POST['entry_date_country'][$i]));
                    $od->setDepartureDateCountry($mod->changeFormatToOriginal($_POST['departure_date_country'][$i]));
                    $od->setTravelPurpose($_POST['travelPurpose'][$i]);
                    $od->setSelectedVisaType($_POST['visaType'][$i]);
                    $od->setSelectedVisaTypePrice($visa_type_cost);
                    
                    /* computation */
                    $total += $visa_type_cost;
                    $gst += ($visa_type_cost * 0.10);
                    
                    $grand_total += $visa_type_cost;
                    $grand_gst += ($visa_type_cost * 0.10);
                    
                    $od->setSelectedVisaTypeRequirements($_POST['addReq'][$i]);
                    
                    /* computation */
                    $reqs = explode(";", $_POST['addReq'][$i]);
                    for($e=0; $e<count($reqs); $e++){
                        $extractReq = $this->extractSelectedVisaTypeRequirements($reqs[$e] . ";", 'public');
                        if(isset($extractReq[0]['cost'])){
                            $reqCost = $extractReq[0]['cost'];
                            
                            $total += $reqCost;
                            $grand_total += $reqCost;
                        }
                    }
                    
                    $od->setVisaCourier($_POST['courier'][$i]);
                    if($_POST["courier"] != "" && $_POST["courier"] != 0){
                        $od->setVisaCourierPrice($visa_courier_cost);
                        
                        /* computation */
                        if($user->getCanGetSpecialPrice() == 1){
                            $courier_price = $visa_courier_cost + ($visa_courier_cost * ($user->getSpecialPrice()/100));
                        }else{
                            $courier_price = $visa_courier_cost;
                        }
                        $total += $courier_price;
                        $gst += ($courier_price * 0.10);
                        
                        $grand_total += $courier_price;
                        $grand_gst += ($courier_price * 0.10);
                    }else{
                        $od->setVisaCourierPrice(0);
                    }
                    $od->setTravellerTitle($_POST['travellerTitle'][$i]);
                    $od->setTravellerFname($_POST['travellerFname'][$i]);
                    $od->setTravellerMname($_POST['travellerMname'][$i]);
                    $od->setTravellerLname($_POST['travellerLname'][$i]);
                    $od->setTravellerEmail($_POST['travellerEmail'][$i]);
                    $od->setTravellerOccupation($_POST['travellerOccupation'][$i]);
                    $od->setTravellerPhone($_POST['travellerPhone'][$i]);
                    $od->setTravellerBday($mod->changeFormatToOriginal($_POST['travellerBirthDate'][$i]));
                    $od->setTravellerPassportType($_POST['travellerPassportType'][$i]);
                    $od->setTravellerNationality($_POST['travellerNationality'][$i]);
                    $od->setTravellerPassportNo($_POST['travellerPassportNumber'][$i]);
                    $od->setIsSmartTraveller($_POST['smartTraveller'][$i]);
                    $od->setDdCompany($_POST['docReturnCompany'][$i]);
                    $od->setDdDocReturnAddress($_POST['docReturnAddress'][$i]);
                    $od->setDdCity($_POST['docReturnCity'][$i]);
                    $od->setDdState($_POST['docReturnState'][$i]);
                    $od->setDdPostcode($_POST['docReturnPostCode'][$i]);
                    $od->setDdFname($_POST['docReturnFname'][$i]);
                    $od->setDdLname($_POST['docReturnLname'][$i]);
                    $od->setDdContactNo($_POST['docReturnContactNo'][$i]);
                    $od->setDdAdditionalComment($_POST['additionalComments'][$i]);
                    $total = $total + $gst;
                    
                    if($model->getDiscountRate() > 0){
                        $od->setTotal($total - ($total * ($model->getDiscountRate()/100)));
                    }else{
                        $od->setTotal($total);
                    }
                    $em->persist($od);
                    $em->flush();
                }
            }
            
            $grand_total = $grand_total + $grand_gst;
            $order = $em->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisa')->findOneBy(array('bulk_order_no'=>$model->getBulkOrderNo()));
            if($order->getDiscountRate() > 0){
                $order->setGrandTotal($grand_total - ($grand_total * ($order->getDiscountRate()/100)));
            }else{
                $order->setGrandTotal($grand_total);
            }
            $em->persist($order);
            $em->flush();
                    
            if($error_count > 0 ){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();


            }else{
                // commit changes
                $em->getConnection()->commit(); 

                if($model->getLevel() == 1){
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Bulk order has been saved'
                        );

                    // return to tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa') . "?client=".$client_id."&order=".$model->getBulkOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa') . "?order=".$model->getBulkOrderNo());
                    }
                    
                    
                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?client=".$client_id."&order=".$model->getBulkOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?order=".$model->getBulkOrderNo());
                    }
                }
            }
                
           
        }else{
            
            if($session->get('user_type') == 'admin-user'){
                if(isset($_GET['client'])){
                    $_GET['client'] = intval($_GET['client']);
                }else{
                    return $this->redirect($this->generateUrl('acme_cls_admin_home'));
                }
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $user_details = '';
            }
            
            // if order number is set
            if(isset($_GET["order"])){
                $_GET["order"] = intval($_GET["order"]);
                $client_id = ($session->get('user_type') == 'admin-user') ? $_GET['client'] : $session->get('client_id');
                $post = $this->getBulkOrderDetails($_GET["order"], $client_id);
                
                
                if(count($post) > 0 ){
                    if($post["status"] == 'draft'){
                        return $this->render('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:index.html.twig',
                            array(
                                'countries'=> $this->getCountries(),
                                'countries_json'=> $this->getCountriesJson(),
                                'name_titles'=> $this->getNameTitlesInJson(),
                                'visa_courier_options' => $this->getCourierOptionsJson(),
                                'passport_types'=>$this->getPassportTypes(),
                                'departments'=>$this->getDepartments(),
                                'order_no'=> $_GET['order'],
                                'post'=>$post
                            ));
                    }else{
                        // return to review visa page
                        if($session->get('user_type') == 'admin-user'){
                            return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?client=".$client_id."&order=".$post['bulk_order_no']);
                        }else{
                            return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?order=".$post['bulk_order_no']);
                        }
                    }
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order"].' not found!' 
                        ));
                }
            }else{
                
                return $this->render('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'countries_json'=> $this->getCountriesJson(),
                        'name_titles'=> $this->getNameTitlesInJson(),
                        'visa_courier_options' => $this->getCourierOptionsJson(),
                        'passport_types'=>$this->getPassportTypes(),
                        'departments'=>$this->getDepartments()
                    ));
            }
            
        }
        
        
        
    }
    
    
    public function reviewOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-bulk-public-visa-review-order');
        
        if($session->get('user_type') != 'bulk-visa' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisa')->findOneBy(array('bulk_order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setLevel(3); // 3= Place Order
            }else{
                $model->setLevel(2); // 2= Review Order
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getLevel() == 2){

                // return to review visa page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?client=".$client_id."&order=".$model->getBulkOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?order=".$model->getBulkOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_place_order') . "?client=".$client_id."&order=".$model->getBulkOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_place_order') . "?order=".$model->getBulkOrderNo());
                }
            }
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $em = $this->getDoctrine()->getManager();
            $data = $this->getBulkOrderDetails($_GET["order"], $client_id);
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            if( count($data) > 0 && $data["status"] == 'draft'){

                return $this->render('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:reviewOrder.html.twig',
                    array('order_no'=> $_GET["order"],
                        'data'=> $data,
                        'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                    'special_price'=> $user->getSpecialPrice()
                                ),
                        'user_details'=>$user_details
                        ));

            }else{
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_place_order') . "?client=".$client_id."&order=".$_GET["order"]);
                }else{
                    return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_place_order') . "?order=".$_GET["order"]);
                }
            }

        }
        
        
    }
    
    
    public function discountUpdateAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') != '' && isset($_POST["discountCode"]) && isset($_POST["orderNumber"])){
            
            $_POST['orderNumber'] = filter_var($_POST['orderNumber'], FILTER_SANITIZE_STRING);
            $_POST['discountCode'] = filter_var($_POST['discountCode'], FILTER_SANITIZE_STRING);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('p')
                ->where('p.code = :code')
                ->setParameter('code', $_POST["discountCode"])
                ->getQuery();
            $data = $query->getArrayResult();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisa')->findOneBy(array('bulk_order_no'=>$_POST["orderNumber"], 'client_id'=>$client_id));
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            //$user->getCanGetSpecialPrice()
            
                
            
            $post = $this->getBulkOrderDetails($_POST["orderNumber"]);
            
            
            $total = 0;
            $gst = 0;

            
            for($i=0; $i<count($post['items']); $i++){
                $item_total = 0;
                $item_gst = 0;
                $od = $em->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisaDetails')->findOneBy(array('id'=>$post['items'][$i]['id']));
                
                $total += $post['items'][$i]['selected_visa_type_price'];
                $gst +=  ($post['items'][$i]['selected_visa_type_price'] * 0.10);
                
                $item_total += $post['items'][$i]['selected_visa_type_price'];
                $item_gst +=  ($post['items'][$i]['selected_visa_type_price'] * 0.10);

                for($e=0; $e<count($post['items'][$i]['selected_visa_type_requirements_array']); $e++){
                    $total += $post['items'][$i]['selected_visa_type_requirements_array'][$e]['cost'];
                    
                    $item_total += $post['items'][$i]['selected_visa_type_requirements_array'][$e]['cost'];
                }
                
                if($user->getCanGetSpecialPrice() == 1){
                    $courier_price = $post['items'][$i]['visa_courier_price'] + ($post['items'][$i]['visa_courier_price'] * ($user->getSpecialPrice()/100));
                }else{
                    $courier_price = $post['items'][$i]['visa_courier_price'];
                }

                $total += $courier_price;
                $gst += ($courier_price * 0.10);
                
                $item_total += $courier_price;
                $item_gst += ($courier_price * 0.10);
                
                if(count($data) > 0){
                    $od->setDiscountCode($_POST['discountCode']);
                    $od->setDiscountRate($data[0]["rate"]);
                }else{
                    $od->setDiscountCode("");
                    $od->setDiscountRate(0);
                }
                
                
                $item_total = $item_total + $item_gst;
                if(count($data) > 0 && $data[0]["rate"] > 0){
                    $od->setTotal($item_total - ($item_total * ($data[0]["rate"]/100)));
                }else{
                    $od->setTotal($item_total);
                }
                $em->persist($od);
                $em->flush();
                    
                
            }
            
            
            
            
            
            if(count($data) > 0){
                
                $model->setDiscountCode($_POST['discountCode']);
                $model->setDiscountRate($data[0]["rate"]);
                
                $discount_rate = $data[0]["rate"];
                $subtotal_discount = $total * ($discount_rate / 100);
                $gst_discount = $gst * ($discount_rate / 100);
            }else{
                
                $model->setDiscountCode("");
                $model->setDiscountRate(0);
                
                $discount_rate = 0;
                $subtotal_discount = 0;
                $gst_discount = 0;
            }

            // subtotal
            $subtotal = $total - $subtotal_discount;
            $gst = $gst - $gst_discount;
            
            $grand_total = $subtotal + $gst;
            
            $model->setGrandTotal($grand_total);
            
            
            $em->persist($model);
            $em->flush();
            
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?client=".$client_id."&order=".$model->getBulkOrderNo());
            }else{
                return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?order=".$model->getBulkOrderNo());
            }
        }else{
            
            return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?order=".$_POST['orderNumber']);
            
        }
    }
    
    
    public function placeOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $error_count = 0;
        
        if($session->get('user_type') != 'bulk-visa' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            $_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
            if(isset($_POST["accountNumber"])){
                $_POST["accountNumber"] = filter_var($_POST['accountNumber'], FILTER_SANITIZE_STRING);
            }
            
            
            
            
            $data = $this->getBulkOrderDetails($_POST["orderNumber"]);
            
            if($data['status'] == 'sent'){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$client_id);
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            $items = $data['items'];
            $em->getConnection()->beginTransaction();
            
            for($i=0; $i<count($items); $i++){
                
                $order = new Orders();
                $order->setClientId($client_id);
                $order->setOrderType(6);
                $order->setDateLastSaved($datetime->format('Y-m-d H:i:s'));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                if(trim($items[$i]['traveller_mname']) != ""){
                    $order->setPrimaryTravellerName($items[$i]['traveller_fname'] . " " . $items[$i]['traveller_mname'] . " " . $items[$i]['traveller_lname']);
                }else{
                    $order->setPrimaryTravellerName($items[$i]['traveller_fname'] . " " . $items[$i]['traveller_lname']);
                }
                $order->setPrimaryTravellerPassportNo($items[$i]['traveller_passport_no']);
                $order->setDestination($items[$i]['destination']);
                $order->setDepartureDate($items[$i]['departure_date']);
                $order->setIsSmartTraveller($items[$i]['is_smart_traveller']);
                $order->setStatus(10);
                $order->setVisaMddCompany($items[$i]['dd_company']);
                $order->setVisaMddAddress($items[$i]['dd_doc_return_address']);
                $order->setVisaMddCity($items[$i]['dd_city']);
                $order->setVisaMddState($items[$i]['dd_state']);
                $order->setVisaMddPostcode($items[$i]['dd_postcode']);
                $order->setVisaMddFname($items[$i]['dd_fname']);
                $order->setVisaMddLname($items[$i]['dd_lname']);
                $order->setVisaMddContact($items[$i]['dd_contact_no']);
                $order->setVisaAdditionalComment($items[$i]['dd_additional_comment']);
                $order->setGrandTotal($items[$i]['total']);
                $order->setDiscountCode($items[$i]['discount_code']);
                $order->setDiscountRate($items[$i]['discount_rate']);
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                $em->persist($order);
                $em->flush();
                
                
                
                
                
                $order_travel = new OrderDestinations();
                $order_travel->setOrderNo($order->getOrderNo());
                $order_travel->setCountryId($items[$i]['destination']);
                $order_travel->setDepartureDate($items[$i]['departure_date']);
                $order_travel->setEntryDateCountry($items[$i]['entry_date_country']);
                $order_travel->setDepartureDateCountry($items[$i]['departure_date_country']);
                $order_travel->setTravelPurpose($items[$i]['travel_purpose']);
                $order_travel->setSPrimary(1);
                $order_travel->setSelectedVisaType($items[$i]['selected_visa_type']);
                $order_travel->setSelectedVisaTypePrice($items[$i]['selected_visa_type_price']);
                $order_travel->setSelectedVisaTypeRequirements($items[$i]['selected_visa_type_requirements']);
                $em->persist($order_travel);
                $em->flush();
                
                
                $order_travellers = new OrderTravellers();
                $order_travellers->setOrderNo($order->getOrderNo());
                $order_travellers->setTitle($items[$i]['traveller_title']);
                $order_travellers->setFname($items[$i]['traveller_fname']);
                $order_travellers->setLname($items[$i]['traveller_lname']);
                $order_travellers->setMname($items[$i]['traveller_mname']);
                $order_travellers->setEmail($items[$i]['traveller_email']);
                $order_travellers->setOccupation($items[$i]['traveller_occupation']);
                $order_travellers->setPhone($items[$i]['traveller_phone']);
                $order_travellers->setBirthDate($items[$i]['traveller_bday']);
                $order_travellers->setNationality($items[$i]['traveller_nationality']);
                $order_travellers->setPassportType($items[$i]['traveller_passport_type']);
                $order_travellers->setPassportNumber($items[$i]['traveller_passport_no']);
                $order_travel->setSPrimary(1);
                $em->persist($order_travellers);
                $em->flush();
                
                
                $payment = new Payment();
                $payment->setDatePaid($datetime->format("Y-m-d H:i:s"));
                $payment->setClientId($client_id);
                $payment->setOrderNo($order->getOrderNo());
                $payment->setFname($_POST["fname"]);
                $payment->setLname($_POST["lname"]);
                $payment->setEmail($_POST["email"]);
                $payment->setPhone($_POST["phone"]);
                $payment->setMobile($_POST["mobile"]);
                $payment->setAddress($_POST["address"]);
                $payment->setCity($_POST["city"]);
                $payment->setState($_POST["state"]);
                $payment->setPostcode($_POST["postcode"]);
                $payment->setCountryId($_POST["country"]);
                $payment->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
                $payment->setMbaAddress($_POST["billingAddress"]);
                $payment->setMbaCity($_POST["billingCity"]);
                $payment->setMbaState($_POST["billingState"]);
                $payment->setMbaPostcode($_POST["billingPostcode"]);
                $payment->setMbaCountryId($_POST["billingCountry"]);
                $payment->setPaymentOption($_POST["paymentType"]);
                if(isset($_POST["accountNumber"])){
                    $payment->setAccountNo($_POST["accountNumber"]);
                }

                if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                    $total_order_price = $order->getGrandTotal();
                }else{
                    $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
                }

                $total_order_price = number_format($total_order_price, 2,'.','');
                $payment->setTotalOrderPrice($total_order_price);

                if($_POST["paymentType"] == 1){
                    $payment->setSPaid(1);
                }else{
                    $payment->setSPaid(2);
                }
                $em->persist($payment);
                $em->flush();
                
                
                $item_order = $em->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisaDetails')->findOneBy(array('id'=>$items[$i]['id']));
                $item_order->setGeneratedOrderNo($order->getOrderNo());
                $item_order->setFinalTotal($total_order_price);
                $em->persist($item_order);
                $em->flush();
            }
            
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'company'=>$_POST['company'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'paymentType'=>$_POST["paymentType"],
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            if($error_count == 0){
                if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors = array();
                    $special_error=1;
                }
                
                if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                    $error_count += 1;
                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors[] = array('message'=>'Account number is not correct!');
                    $error_count += 1;
                }
            }
            
            $total_order_price = 0;
            if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                $total_order_price = $data['grand_total'];
            }else{
                $total_order_price = $data['grand_total'] + ($data['grand_total'] * ($this->getCreditCardProcessingFee()/100));
            }

            $total_order_price = number_format($total_order_price, 2,'.','');

            if(trim($_SERVER['HTTP_HOST']) != 'localhost' && $error_count == 0){
                
                /**
                * Credit card payment
                * Position: Start
                */
                
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS Visa Application'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                   $root_dir = dirname($this->get('kernel')->getRootDir());
                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
                   
                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               
               /**
                * Credit card payment
                * Position: End
                */

                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }

            }else{
                
                $border = $em->getRepository('AcmeCLSclientGovBundle:OrderBulkPublicVisa')->findOneBy(array('bulk_order_no'=>$_POST["orderNumber"]));
                $border->setStatus('sent'); // 10= Order placed
                $border->setGrandTotal($total_order_price);
                $border->setPaymentOption($_POST["paymentType"]);
                $em->persist($border);
                $em->flush();
                
                
                $data = $this->getBulkOrderDetails($_POST["orderNumber"]);

                // commit changes
                $em->getConnection()->commit(); 

                
                $em = $this->getDoctrine()->getManager();
                    
                //$host = $request->getScheme().'://'.$request->getHost();
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                
                // email invoice
                $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Bulk Visa Application Reciept", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                            'special_price'=> $user->getSpecialPrice()
                                        )
                                    )
                                ),
                        $this->getBulkOrderItems($_POST["orderNumber"])
                        );
                
                if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                    // email invoice (copy to admins)
                    $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                    $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Bulk Visa Application Reciept (Copy)", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                            'special_price'=> $user->getSpecialPrice()
                                        )
                                    )
                                ),
                        $this->getBulkOrderItems($_POST["orderNumber"])
                    );
                }
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );

                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getBulkOrderDetails($_GET["order"]);
        
            // if order status done with the review order page
            // if not, send to review order page
            if( $data["level"] == 3 ){


                return $this->render('AcmeCLSclientGovBundle:ApplicationBulkPublicVisa:placeOrder.html.twig',
                    array('order_no'=> $_GET["order"],
                        'data'=> $data,
                        'countries'=> $this->getCountries(),
                        'departments'=> $this->getDepartments(),
                        'cardtypes'=> $this->getCardTypes(),
                        'user_details'=>$user_details
                        ));

            }else{

                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?client=".$client_id."&order=".$_GET["order"]);
                }else{
                    return $this->redirect($this->generateUrl('cls_client_application_bulk_corporate_visa_review') . "?order=".$_GET["order"]);
                }
            }
        }
    }
    
    
    
    
    
    
    
    public function getPublicVisaTypesAction(){
        $_POST['country_id'] = intval($_POST['country_id']);
        $data = $this->getPublicVisaTypesByCountryId($_POST['country_id']);
        
        $items = array();
        for($i=0; $i<count($data); $i++){
            $items[] = array($data[$i]['id']=> $data[$i]['type'] . " - $" . $data[$i]['cost']);
        }
        
        return new Response(json_encode($items));
    }
    
    public function getPublicVisaTypeFeesAction(){
        $_POST['visa_id'] = intval($_POST['visa_id']);
        $data = $this->getPublicVisaTypeAdditionalRequirementsByVisaTypeId($_POST['visa_id']);
        
        $items = array();
        for($i=0; $i<count($data); $i++){
            $items[] = array($data[$i]['id']=> $data[$i]['requirement'] . " - $" . $data[$i]['cost']);
        }
        
        return new Response(json_encode($items));
    }
    
    public function deleteItemsAction(){
        $_POST['bulk_select'] = implode(",",$_POST['bulk_select']);
        $connection = $em->getConnection();
        $statement = $connection->prepare("DELETE FROM tbl_order_bulk_public_visa_details WHERE bulk_order_no IN ('". $_POST['bulk_select'] ."')");
        $statement->execute();
        // set flash message
        $this->get('session')->getFlashBag()->add(
                'success',
                'Selected item(s) have been deleted'
            );
    }

}
