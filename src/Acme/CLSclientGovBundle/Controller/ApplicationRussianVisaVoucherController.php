<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class ApplicationRussianVisaVoucherController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-russian-visa-voucher');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            
            $session->set("redirect_url", $this->generateUrl('cls_client_russian_visa_voucher_application_details'));
            
            if(count($_POST) == 0){
                return $this->redirect($this->generateUrl('acme_cls_client_login'));
            }
            $session->set("russian_visa_form_post", $_POST);
            $session->set("public_reg", "signup");
            $session->set("russian_visa_form_file", $_FILES["file"]["name"]);
            if(trim($_FILES["file"]["name"]) != ''){
                move_uploaded_file($_FILES["file"]["tmp_name"], $root_dir ."/dev/temps/". $_FILES["file"]["name"]);
            }
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }else{
            if(isset($_POST['public'])){
                $session->set("russian_visa_form_post", $_POST);
                $session->set("russian_visa_form_file", $_FILES["file"]["name"]);
                if(trim($_FILES["file"]["name"]) != ''){
                    move_uploaded_file($_FILES["file"]["tmp_name"], $root_dir ."/dev/temps/". $_FILES["file"]["name"]);
                }
            }
        }
         
       
        if(isset($_POST["hid_submit"])){
            $_POST['rvv_first_entry_date'] = filter_var($_POST['rvv_first_entry_date'], FILTER_SANITIZE_STRING);
            $_POST['rvv_first_departure_date'] = filter_var($_POST['rvv_first_departure_date'], FILTER_SANITIZE_STRING);
            $_POST['rvv_second_entry_date'] = filter_var($_POST['rvv_second_entry_date'], FILTER_SANITIZE_STRING);
            $_POST['rvv_second_departure_date'] = filter_var($_POST['rvv_second_departure_date'], FILTER_SANITIZE_STRING);
            $_POST['rvv_list_of_cities'] = filter_var($_POST['rvv_list_of_cities'], FILTER_SANITIZE_STRING);
            $_POST['rvv_list_of_hotels'] = filter_var($_POST['rvv_list_of_hotels'], FILTER_SANITIZE_STRING);
            $_POST['rvv_visa_applied_at'] = filter_var($_POST['rvv_visa_applied_at'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_title'] = filter_var($_POST['rvv_applicant_title'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_title'] = intval($_POST['rvv_applicant_title']);
            $_POST['rvv_applicant_fname'] = filter_var($_POST['rvv_applicant_fname'], FILTER_SANITIZE_STRING);
            $_POST["rvv_applicant_fname"] = str_replace("&#39;","'", $_POST["rvv_applicant_fname"]);
            $_POST['rvv_applicant_mname'] = filter_var($_POST['rvv_applicant_mname'], FILTER_SANITIZE_STRING);
            $_POST["rvv_applicant_mname"] = str_replace("&#39;","'", $_POST["rvv_applicant_mname"]);
            $_POST['rvv_applicant_lname'] = filter_var($_POST['rvv_applicant_lname'], FILTER_SANITIZE_STRING);
            $_POST["rvv_applicant_lname"] = str_replace("&#39;","'", $_POST["rvv_applicant_lname"]);
            $_POST['rvv_applicant_email'] = filter_var($_POST['rvv_applicant_email'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_organisation'] = filter_var($_POST['rvv_applicant_organisation'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_citizenship'] = filter_var($_POST['rvv_applicant_citizenship'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_sex'] = filter_var($_POST['rvv_applicant_sex'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_phone'] = filter_var($_POST['rvv_applicant_phone'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_birth_place'] = filter_var($_POST['rvv_applicant_birth_place'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_birth_date'] = filter_var($_POST['rvv_applicant_birth_date'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_passport_no'] = filter_var($_POST['rvv_applicant_passport_no'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_passport_issue_date'] = filter_var($_POST['rvv_applicant_passport_issue_date'], FILTER_SANITIZE_STRING);
            $_POST['rvv_applicant_passport_exp_date'] = filter_var($_POST['rvv_applicant_passport_exp_date'], FILTER_SANITIZE_STRING);
            $_POST['rvv_company'] = filter_var($_POST['rvv_company'], FILTER_SANITIZE_STRING);
            $_POST["rvv_company"] = str_replace("&#39;","'", $_POST["rvv_company"]);
            $_POST['rvv_position'] = filter_var($_POST['rvv_position'], FILTER_SANITIZE_STRING);
            $_POST['rvv_city'] = filter_var($_POST['rvv_city'], FILTER_SANITIZE_STRING);
            $_POST['rvv_state'] = filter_var($_POST['rvv_state'], FILTER_SANITIZE_STRING);
            $_POST['rvv_postcode'] = filter_var($_POST['rvv_postcode'], FILTER_SANITIZE_STRING);
            $_POST['rvv_country'] = filter_var($_POST['rvv_country'], FILTER_SANITIZE_STRING);
            $_POST['rvv_company_fax'] = filter_var($_POST['rvv_company_fax'], FILTER_SANITIZE_STRING);
            $_POST['rvv_address'] = filter_var($_POST['rvv_address'], FILTER_SANITIZE_STRING);
            $_POST["rvv_address"] = str_replace("&#39;","'", $_POST["rvv_address"]);
            
            
            
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            $discount_rate = 0;
            $error_count = 0;
            // new order
            if(!isset($_POST["orderNumber"])){
                $order = new Orders();
                $order->setClientId($client_id);
                $order->setOrderType(8);
            // update order
            }else{
                $_POST["orderNumber"] = intval($_POST["orderNumber"]);
                $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            }
            
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            $order->setPrimaryTravellerName($_POST['rvv_applicant_fname'] . " " . $_POST['rvv_applicant_lname']);
            $order->setRvvFirstEntryDate($mod->changeFormatToOriginal($_POST['rvv_first_entry_date']));
            $order->setRvvSecondEntryDate($mod->changeFormatToOriginal($_POST['rvv_second_entry_date']));
            $order->setRvvFirstDepartureDate($mod->changeFormatToOriginal($_POST['rvv_first_departure_date']));
            $order->setRvvSecondDepartureDate($mod->changeFormatToOriginal($_POST['rvv_second_departure_date']));
            $order->setRvvListOfCities($_POST['rvv_list_of_cities']);
            $order->setRvvListOfHotels($_POST['rvv_list_of_hotels']);
            $order->setRvvVisaAppliedAt($_POST['rvv_visa_applied_at']);
            if($_POST["hid_submit"] == 1){
                $order->setStatus(2); // 2= Visa Options
            }else{
                $order->setStatus(1); // 1= Destination
            }
            
            if(trim($session->get("russian_visa_form_file")) != ''){
                if(file_exists($root_dir ."/dev/temps/". $session->get("russian_visa_form_file"))){
                    $order->setRvvFile($session->get("russian_visa_form_file"));
                }
            }
            
            
            
            if(count($session->get("russian_visa_form_post")) > 0 ){
                
                $form_post = $session->get("russian_visa_form_post");
                
                list($voucher_id, $col) = explode(":",$form_post["voucher"]);
                if($order->getRussianVisaVoucherId() == ''){
                    $order->setRussianVisaVoucherId($voucher_id);
                }
                if($order->getRussianVisaVoucherColNo() == ''){
                    $order->setRussianVisaVoucherColNo($col);
                }
            }
            
            
            
            
            $em->persist($order);
            $em->flush();
            
            
            if(!isset($_POST["orderNumber"])){
                $applicant = new OrderTravellers();
            }else{
                $applicant = $em->getRepository('AcmeCLSclientGovBundle:OrderTravellers')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            }
            $applicant->setOrderNo($discount_rate);
            $applicant->setTitle($_POST['rvv_applicant_title']);
            $applicant->setOrderNo($order->getOrderNo());
            $applicant->setFname($_POST['rvv_applicant_fname']);
            $applicant->setMname($_POST['rvv_applicant_mname']);
            $applicant->setLname($_POST['rvv_applicant_lname']);
            $applicant->setEmail($_POST['rvv_applicant_email']);
            $applicant->setOrganisation($_POST['rvv_applicant_organisation']);
            $applicant->setRvvCitizenship($_POST['rvv_applicant_citizenship']);
            $applicant->setRvvSex($_POST['rvv_applicant_sex']);
            $applicant->setPhone($_POST['rvv_applicant_phone']);
            $applicant->setRvvBirthPlace($_POST['rvv_applicant_birth_place']);
            $applicant->setBirthDate($mod->changeFormatToOriginal($_POST['rvv_applicant_birth_date']));
            $applicant->setPassportNumber($_POST['rvv_applicant_passport_no']);
            $applicant->setRvvPassportIssueDate($mod->changeFormatToOriginal($_POST['rvv_applicant_passport_issue_date']));
            $applicant->setRvvPassportExpDate($mod->changeFormatToOriginal($_POST['rvv_applicant_passport_exp_date']));
            $applicant->setRvvCompany($_POST['rvv_company']);
            $applicant->setRvvPosition($_POST['rvv_position']);
            $applicant->setRvvCity($_POST['rvv_city']);
            $applicant->setRvvState($_POST['rvv_state']);
            $applicant->setRvvPostcode($_POST['rvv_postcode']);
            $applicant->setRvvCountry($_POST['rvv_country']);
            $applicant->setRvvCompanyFax($_POST['rvv_company_fax']);
            $applicant->setRvvAddress($_POST['rvv_address']);
            $em->persist($applicant);
            $em->flush();
            
            
            
            
            $validation_error_count = 0;
            // input validation
            if($_POST["hid_submit"] == 1){
                $blank_inputs = array();
                if(trim($_POST["rvv_first_entry_date"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_first_entry_date';
                }

                if(trim($_POST["rvv_first_departure_date"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_first_departure_date';
                }

                if(trim($_POST["rvv_second_entry_date"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_second_entry_date';
                }

                if(trim($_POST["rvv_second_departure_date"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_second_departure_date';
                }

                if(trim($_POST["rvv_applicant_title"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_title';
                }
                if(trim($_POST["rvv_applicant_fname"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_fname';
                }
                if(trim($_POST["rvv_applicant_lname"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_lname';
                }
                if(trim($_POST["rvv_applicant_email"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_email';
                }
                if(trim($_POST["rvv_applicant_organisation"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_organisation';
                }
                if(trim($_POST["rvv_applicant_citizenship"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_citizenship';
                }
                if(trim($_POST["rvv_applicant_sex"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_sex';
                }if(trim($_POST["rvv_applicant_phone"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_phone';
                }
                if(trim($_POST["rvv_applicant_birth_place"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_birth_place';
                }
                if(trim($_POST["rvv_applicant_birth_date"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_birth_date';
                }
                if(trim($_POST["rvv_applicant_passport_no"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_passport_no';
                }
                if(trim($_POST["rvv_applicant_passport_issue_date"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_passport_issue_date';
                }
                if(trim($_POST["rvv_applicant_passport_exp_date"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_applicant_passport_exp_date';
                }
                if(trim($_POST["rvv_company"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_company';
                }
                if(trim($_POST["rvv_position"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_position';
                }
                if(trim($_POST["rvv_address"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_address';
                }
                
                if(trim($_POST["rvv_city"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_city';
                }
                if(trim($_POST["rvv_state"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_state';
                }
                if(trim($_POST["rvv_postcode"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_postcode';
                }
                if(trim($_POST["rvv_country"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'rvv_country';
                }
                
                
            }
            
            $post = array(
                'rvv_first_entry_date' => $_POST['rvv_first_entry_date'],
                'rvv_first_departure_date' => $_POST['rvv_first_departure_date'],
                'rvv_second_entry_date' => $_POST['rvv_second_entry_date'],
                'rvv_second_departure_date' => $_POST['rvv_second_departure_date'],
                'rvv_list_of_cities' => $_POST['rvv_list_of_cities'],
                'rvv_list_of_hotels' => $_POST['rvv_list_of_hotels'],
                'rvv_visa_applied_at' => $_POST['rvv_visa_applied_at'],
                'travellers'=> array(0=>array(
                    
                    'title' => $_POST['rvv_applicant_title'],
                    'fname' => $_POST['rvv_applicant_fname'],
                    'mname' => $_POST['rvv_applicant_mname'],
                    'lname' => $_POST['rvv_applicant_lname'],
                    'email' => $_POST['rvv_applicant_email'],
                    'organisation' => $_POST['rvv_applicant_organisation'],
                    'rvv_citizenship' => $_POST['rvv_applicant_citizenship'],
                    'rvv_sex' => $_POST['rvv_applicant_sex'],
                    'phone' => $_POST['rvv_applicant_phone'],
                    'rvv_birth_place' => $_POST['rvv_applicant_birth_place'],
                    'birth_date' => $_POST['rvv_applicant_birth_date'],
                    'passport_number' => $_POST['rvv_applicant_passport_no'],
                    'rvv_passport_issue_date' => $_POST['rvv_applicant_passport_issue_date'],
                    'rvv_passport_exp_date' => $_POST['rvv_applicant_passport_exp_date'],
                    'rvv_company' => $_POST['rvv_company'],
                    'rvv_position' => $_POST['rvv_position'],
                    'rvv_city' => $_POST['rvv_city'],
                    'rvv_state' => $_POST['rvv_state'],
                    'rvv_postcode' => $_POST['rvv_postcode'],
                    'rvv_country' => $_POST['rvv_country'],
                    'rvv_company_fax' => $_POST['rvv_company_fax'],
                    'rvv_address' => $_POST['rvv_address'])),
                'blank_inputs'=>(isset($blank_inputs)) ? $blank_inputs : array()
            );
            
            if($error_count > 0 || $validation_error_count > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($validation_error_count > 0){
                    $this->get('session')->getFlashBag()->add(
                            'validation-error',
                            'Please check the highlighted required fields and complete the form.'
                        );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors
                    );
                }
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                // return to tpn page
                return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'departments'=>$this->getDepartments(),
                        'user_details'=>$user_details,
                        'post'=> $post
                    )
                );
                
            }else{
                // commit changes
                $em->getConnection()->commit(); 
                
                if(trim($session->get("russian_visa_form_file")) != ''){
                    if(file_exists($root_dir ."/dev/temps/". $session->get("russian_visa_form_file"))){
                        $mod->createDirectory($root_dir ."/dev/rvv/", $client_id . "/" . $order->getOrderNo());
                        rename($root_dir ."/dev/temps/". $session->get("russian_visa_form_file"), 
                               $root_dir ."/dev/rvv/". $client_id . "/" . $order->getOrderNo() . "/" . $session->get("russian_visa_form_file"));
                    }
                }

                // if save only
                if($order->getStatus() == 1){
                    
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been saved'
                        );
                    
                    // return to tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_details') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_details') . "?order=".$order->getOrderNo());
                    }
                }else{
                    
                    // return to review tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_options') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_options') . "?order=".$order->getOrderNo());
                    }
                }
            }
            
            
        }else{
            if($session->get('user_type') == 'admin-user'){
                if(isset($_GET['client'])){
                    $_GET['client'] = intval($_GET['client']);
                }else{
                    return $this->redirect($this->generateUrl('acme_cls_admin_home'));
                }
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $user_details = '';
            }
            
            
            // if order number is set
            if(isset($_GET["order"])){
                $_GET["order"] = intval($_GET["order"]);
                $client_id = ($session->get('user_type') == 'admin-user') ? $_GET['client'] : $session->get('client_id');
                $post = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
                
                if(count($post) > 0 && $post['order_type'] == 8){
                    return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:index.html.twig',
                        array(
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles(),
                            'passport_types'=>$this->getPassportTypes(),
                            'departments'=>$this->getDepartments(),
                            'user_details'=>$user_details,
                            'order_no'=>$_GET["order"],
                            'post'=>$post
                        )
                    );
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order"].' not found!' 
                        ));
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'departments'=>$this->getDepartments(),
                        'user_details'=>$user_details
                    )
                );
            }
        }
    }
    
    
    
    
    
    
    public function optionsAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-russian-visa-voucher');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        if(isset($_POST["hidSubmit"])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            $subtotal = 0;
            $gst = 0;
            $error_count = 0;
            $_POST['comments'] = filter_var($_POST['comments'], FILTER_SANITIZE_STRING);
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $_POST["orderNumber"]= intval($_POST["orderNumber"]);
            
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            
            if(isset($_POST["voucher"]) && trim($_POST["voucher"]) != '' && strpos($_POST["voucher"],':') !== false){
                list($voucher_id, $col) = explode(":",$_POST["voucher"]);

                if($col == 1){
                    $column = 'three_days_process_fee';
                }elseif($col == 2){
                    $column = 'one_two_days_process_fee';
                }elseif($col == 3){
                    $column = 'twelve_hrs_process_fee';
                }elseif($col == 4){
                    $column = 'thirteen_days';
                }elseif($col == 5){
                    $column = 'four_days';
                }

                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:RussianVisaVoucher');
                $query = $cust->createQueryBuilder('p')
                    ->select('p.'.$column.' as cost')
                    ->where('p.id = :id')
                    ->setParameter('id', $voucher_id)
                    ->getQuery();
                $voucher = $query->getArrayResult();

                $order->setRussianVisaVoucherId($voucher_id);
                $order->setRussianVisaVoucherColNo($col);
            
            
                $order->setRvvComments($_POST['comments']);
                $subtotal += $voucher[0]['cost'];
                $gst += ($voucher[0]['cost'] * 0.10);

                $order->setRussianVisaVoucherColCost($voucher[0]['cost']);

                if(trim($_FILES["file"]["name"]) != ''){
                    $order->setRvvFile($_FILES["file"]["name"]);
                }
                $order->setGrandTotal($subtotal + $gst);

                $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
//                if($user->getCanGetSpecialPrice() == 1){
//                    $order->setGrandTotal($order->getGrandTotal() + ($order->getGrandTotal() * ($user->getSpecialPrice()/100)));
//                }

                if($order->getDiscountRate() > 0){
                    $order->setGrandTotal($order->getGrandTotal() - ($order->getGrandTotal() * ($order->getDiscountRate()/100)));
                }

                if($_POST["hidSubmit"] == 1){
                    $order->setStatus(3); // 3= Review Order
                }else{
                    $order->setStatus(2); // 2= Visa Options
                }
                $em->persist($order);
                $em->flush();


                if(trim($_FILES["file"]["name"] != "")){
                    $mod->createDirectory($root_dir ."/dev/rvv/", $client_id . "/" . $_POST["orderNumber"]);
                    move_uploaded_file($_FILES["file"]["tmp_name"], $root_dir ."/dev/rvv/".$client_id."/".$_POST["orderNumber"]."/". $_FILES["file"]["name"]);
                }
            }else{
                $error_count += 1;
            }
            if($error_count > 0 ){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                // set flash message
                $this->get('session')->getFlashBag()->add(
                        'error',
                        'Please select Voucher type'
                    );

                // return to review tpn page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_options') . "?client=".$client_id."&order=".$order->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_options') . "?order=".$order->getOrderNo());
                }

            }else{
                $em->getConnection()->commit(); 


                // if save only
                if($order->getStatus() == 2){

                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been saved'
                        );

                    // return to review tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_options') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_options') . "?order=".$order->getOrderNo());
                    }
                }else{

                    // return to review order page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?order=".$order->getOrderNo());
                    }
                }
            }
            
        }else{
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
        
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);

            if(count($data) > 0 && $data['order_type'] == 8){
                // if order status done with the destination page
                // if not, send to destination page
                if( $data["status"] >= 2 ){

                    $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
                    $query = $cust->createQueryBuilder('p')
                        ->where('p.id = :id')
                        ->setParameter('id', $client_id)
                        ->getQuery();
                    $user = $query->getArrayResult();
                    
                    return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:visaOptions.html.twig',
                        array('order_no'=> $_GET["order"],
                            'post' => $data,
                            'visa_courier_options' => $this->getCourierOptions(),
                            'user_details'=> $user_details,
                            'user'=>$user[0],
                            'voucher_types'=>$this->getRussianVisaVoucherTypes()
                            ));

                    
                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_details') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_details') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
        
    }
    
    
    
    public function reviewOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-visa-review-order');
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST['orderNumber'], $client_id);
            if($data["status"]>=10){
                return $this->redirect($this->generateUrl('cls_client_gov_view_order_russian_visa_order') . "?order_no=".$_POST['orderNumber']);
            }
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(4); // 4= Place Order
            }else{
                $model->setStatus(3); // 3= Review Order
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 3){

                // return to review visa page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_place_order') . "?order=".$model->getOrderNo());
                }
            }
            
        }else{
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $em = $this->getDoctrine()->getManager();
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            if(count($data) > 0 && $data['order_type'] == 8){
                // if order status done with the review tpn page
                // if not, send to review tpn page
                if( $data["status"] >= 3 ){
                    if($data["status"]>=10){
                        return $this->redirect($this->generateUrl('cls_client_gov_view_order_russian_visa_order') . "?order_no=".$_GET["order"]);
                    }else{
                        return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:reviewOrder.html.twig',
                            array('order_no'=> $_GET["order"],
                                'data'=> $data,
                                'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                            'special_price'=> $user->getSpecialPrice()
                                        ),
                                'user_details'=>$user_details
                                ));
                    }
                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_options') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_options') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }
    
    
    
    
    public function discountUpdateAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') != '' && isset($_POST["discountCode"]) && isset($_POST["orderNumber"])){
            
            $_POST['orderNumber'] = filter_var($_POST['orderNumber'], FILTER_SANITIZE_STRING);
            $_POST['discountCode'] = filter_var($_POST['discountCode'], FILTER_SANITIZE_STRING);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('p')
                ->where('p.code = :code')
                ->setParameter('code', $_POST["discountCode"])
                ->getQuery();
            $data = $query->getArrayResult();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"], 'client_id'=>$client_id));
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            //$user->getCanGetSpecialPrice()
            
                
            
            $post = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            
            $items_total_cost = 0;
            $items_total_gst = 0;
            $item_total = 0;

            $item_total += $post['russian_visa_voucher_col_cost'] + ($post['russian_visa_voucher_col_cost'] * 0.10);
            $items_total_cost += $post['russian_visa_voucher_col_cost'];
            $items_total_gst +=  $post['russian_visa_voucher_col_cost'] * 0.10;
            
                
            $subtotal = $items_total_cost;
            $gst = $items_total_gst;
            if(count($data) > 0){
                
                $model->setDiscountCode($_POST['discountCode']);
                $model->setDiscountRate($data[0]["rate"]);
                
                $discount_rate = $data[0]["rate"];
                $subtotal_discount = $subtotal * ($discount_rate / 100);
                $gst_discount = $gst * ($discount_rate / 100);
            }else{
                
                $model->setDiscountCode("");
                $model->setDiscountRate(0);
                
                $discount_rate = 0;
                $subtotal_discount = 0;
                $gst_discount = 0;
            }

            // subtotal
            $subtotal = $subtotal - $subtotal_discount;
            $gst = $gst - $gst_discount;
            
            
            $model->setGrandTotal($subtotal + $gst);
//            if($user->getCanGetSpecialPrice() == 1){
//                $model->setGrandTotal($model->getGrandTotal() + ($model->getGrandTotal() * ($user->getSpecialPrice()/100)));
//            }
            
            $em->persist($model);
            $em->flush();
            
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
            }else{
                return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?order=".$model->getOrderNo());
            }
        }else{
            
            return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?order=".$_POST['orderNumber']);
            
        }
    }
    
    
    
    
    
    /**
     * 4th Step: Place Order
     */
    
    public function placeOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-visa-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($order->getStatus() >= 10){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            $em->getConnection()->beginTransaction();

            
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            $_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
            if(isset($_POST["accountNumber"])){
                $_POST["accountNumber"] = filter_var($_POST['accountNumber'], FILTER_SANITIZE_STRING);
            }
            
            $model = new Payment();
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($client_id);
            $model->setOrderNo($_POST["orderNumber"]);
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption($_POST["paymentType"]);
            if(isset($_POST["accountNumber"])){
                $model->setAccountNo($_POST["accountNumber"]);
            }
            
            if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                $total_order_price = $order->getGrandTotal();
            }else{
                $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
            }
            
            $total_order_price = number_format($total_order_price, 2,'.','');
            $model->setTotalOrderPrice($total_order_price);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
            if($_POST["paymentType"] == 1){
                $model->setSPaid(1);
            }else{
                $model->setSPaid(2);
            }
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'company'=>$_POST['company'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'paymentType'=>$_POST["paymentType"],
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            if($error_count == 0){
                if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors = array();
                    $special_error=1;
                }
                
                if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                    $error_count += 1;
                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors[] = array('message'=>'Account number is not correct!');
                    $error_count += 1;
                }
            }
            
            
            if($error_count == 0){
                
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $data['mba_country']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS Visa Application'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                   $root_dir = dirname($this->get('kernel')->getRootDir());
                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
                   
                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               
               /**
                * Credit card payment
                * Position: End
                */

                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }

            }else{
                
                //$order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                $order->setStatus(10); // 10= Order placed
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $order->setGrandTotal($total_order_price);
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                
                $em->persist($order);
                $em->flush();
                
                
                
                

                // commit changes
                $em->getConnection()->commit(); 

                
                $em = $this->getDoctrine()->getManager();
                    
                //$host = $request->getScheme().'://'.$request->getHost();
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                // email invoice
                if(trim($data['rvv_file']) != ''){
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    ),
                            array('0'=>
                                array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                )
                            );
                    }
                    
                    // manual order email to admin
                    if($session->get('user_type') == 'admin-user'){ 
                        if(!isset($_POST['dont_send_invoice'])){
                            $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Russian Visa Voucher Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'manual_order'=>1
                                            )
                                        ),
                                array('0'=>
                                    array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                    )
                                );
                        }
                    }
                    if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                        // email invoice (copy to admins)
                        if(!isset($_POST['dont_send_invoice'])){
                            $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                            $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data
                                            )
                                        ),
                                array('0'=>
                                    array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                    )
                                );
                        }
                    }
                    
                    $admins = $this->getAdminUsers();
                    for($i=0; $i<count($admins); $i++){
                        if(!isset($_POST['dont_send_invoice'])){
                            $this->sendEmailWithAttachment($admins[$i]['email'], "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data
                                            )
                                        ),
                                array('0'=>
                                    array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                    )
                                );
                        }
                    }
                    
                }else{
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    )
                            );
                    }
                    $admins = $this->getAdminUsers();
                    for($i=0; $i<count($admins); $i++){
                        if(!isset($_POST['dont_send_invoice'])){
                            $this->sendEmail($admins[$i]['email'], "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data
                                            )
                                        )
                                );
                        }
                    }
                }
                
                if($session->get('user_type') == 'admin-user'){
                    /* + create log */
                    $logDetails = "New Russian Visa Voucher application (order no. ".$_POST["orderNumber"].") has been manually created and submitted by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }else{
                    /* + create log */
                    $logDetails = "New Russian Visa Voucher application (order no. ".$_POST["orderNumber"].") has been created and submitted by <a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$session->get('client_id')."' target='_blank'>" . $session->get('fname') . " " . $session->get('lname') . "</a>";
                    $this->createLog('client', $session->get('client_id'), $session->get('user_type'), $logDetails);
                    /* - create log */
                }
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );

                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
        
            if(count($data) > 0 && $data['order_type'] == 8){
                // if order status done with the review order page
                // if not, send to review order page
                if( $data["status"] == 4 ){


                    return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:placeOrder.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'user_details'=>$user_details
                            ));

                }else{

                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_russian_visa_voucher_application_review_order') . "?order=".$_GET["order"]);
                    }
                }

            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }

}
