<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class ApplicationTPNController extends GlobalController
{
    /**
     * 1st Step: Destination
     */
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        
        
        // new order
        if(isset($_POST["hid_submit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            // new order
            if(!isset($_POST["orderNumber"])){
                $model = new Orders();
                $model->setClientId($client_id);
                $model->setOrderType(2);
            // update order
            }else{
                $_POST["orderNumber"] = intval($_POST["orderNumber"]);
                $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                
            }
            
            // get tpn settings
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
//            $tpn_price = ($user->getCanGetSpecialPrice() == 1) ? ($tpn_settings->getTpn() + ($tpn_settings->getTpn() * ($user->getSpecialPrice()/100))) : $tpn_settings->getTpn();
//            $additional_tpn_price = ($user->getCanGetSpecialPrice() == 1) ? ($tpn_settings->getTpnAdditional() + ($tpn_settings->getTpnAdditional() * ($user->getSpecialPrice()/100))) : $tpn_settings->getTpnAdditional();
//            $total_tpn = $tpn_price * count($_POST["destination"]);
            
            $tpn_price = $tpn_settings->getTpn();
            $additional_tpn_price = $tpn_settings->getTpnAdditional();
            $total_tpn = $tpn_price * count($_POST["destination"]);
            
            $model->setDateLastSaved($datetime->format('Y-m-d H:i:s'));
            if(trim($_POST["travellerMname"][0]) != ""){
                $_POST["travellerFname"][0] = filter_var($_POST['travellerFname'][0], FILTER_SANITIZE_STRING);
                $_POST["travellerMname"][0] = filter_var($_POST['travellerMname'][0], FILTER_SANITIZE_STRING);
                $_POST["travellerLname"][0] = filter_var($_POST['travellerLname'][0], FILTER_SANITIZE_STRING);
                $_POST["travellerFname"][0] = str_replace("&#39;","'", $_POST["travellerFname"][0]);
                $_POST["travellerMname"][0] = str_replace("&#39;","'", $_POST["travellerMname"][0]);
                $_POST["travellerLname"][0] = str_replace("&#39;","'", $_POST["travellerLname"][0]);
                $model->setPrimaryTravellerName($_POST["travellerFname"][0] . " " . $_POST["travellerMname"][0] . " " . $_POST["travellerLname"][0]);
            }else{
                $_POST["travellerFname"][0] = filter_var($_POST['travellerFname'][0], FILTER_SANITIZE_STRING);
                $_POST["travellerLname"][0] = filter_var($_POST['travellerLname'][0], FILTER_SANITIZE_STRING);
                $_POST["travellerFname"][0] = str_replace("&#39;","'", $_POST["travellerFname"][0]);
                $_POST["travellerLname"][0] = str_replace("&#39;","'", $_POST["travellerLname"][0]);
                $model->setPrimaryTravellerName($_POST["travellerFname"][0] . " " . $_POST["travellerLname"][0]);
            }
            
            $_POST["destination"][0] = filter_var($_POST['destination'][0], FILTER_SANITIZE_STRING);
            $_POST["departureDate"][0] = filter_var($_POST['departureDate'][0], FILTER_SANITIZE_STRING);
            
            $model->setDestination($_POST["destination"][0]);
            $model->setDepartureDate($mod->changeFormatToOriginal($_POST["departureDate"][0]));
            
            
            $_POST["department"] = filter_var($_POST['department'], FILTER_SANITIZE_STRING);
            $_POST["contactFirstname"] = filter_var($_POST['contactFirstname'], FILTER_SANITIZE_STRING);
            $_POST["contactLastname"] = filter_var($_POST['contactLastname'], FILTER_SANITIZE_STRING);
            $_POST["contactEmail"] = filter_var($_POST['contactEmail'], FILTER_SANITIZE_STRING);
            $_POST["contactPhone"] = filter_var($_POST['contactPhone'], FILTER_SANITIZE_STRING);
            
            $model->setPriDeptContactDepartmentId($_POST["department"]);
            $model->setPriDeptContactFname($_POST["contactFirstname"]);
            $model->setPriDeptContactLname($_POST["contactLastname"]);
            $model->setPriDeptContactEmail($_POST["contactEmail"]);
            $model->setPriDeptContactPhone($_POST["contactPhone"]);
            
            
            
            $model->setTpnQty(count($_POST["destination"]));
            $model->setTpnPrice($tpn_price);
            $model->setTpnAdditionalQty(count($_POST["travellerTitle"]) - 1);
            $model->setTpnAdditionalPrice($additional_tpn_price);
            
            
            
            $total_tpn_additional = $additional_tpn_price * (count($_POST["travellerTitle"]) - 1);
            
            $subtotal = $total_tpn + $total_tpn_additional;
            
            
            if(!isset($_POST["orderNumber"])){
                $discount_rate = 0;
                $subtotal_discount = 0;
            }else{
                $discount_rate = $model->getDiscountRate();
                $subtotal_discount = $subtotal * ($discount_rate / 100);
            }
            // subtotal
            $subtotal = $subtotal - $subtotal_discount;
            
            
            $tpn_gst = $total_tpn * 0.10;
            $tpn_additional_gst = $total_tpn_additional * 0.10;
            
            $gst = $tpn_gst + $tpn_additional_gst;
            $gst_discount = $gst * ($discount_rate / 100);
            // gst
            $gst = $gst - $gst_discount;
            
            $model->setGrandTotal($subtotal + $gst);
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(2); // 2= Review TPN
            }else{
                $model->setStatus(1); // 1= Destination
            }
            $em->persist($model);
            $em->flush();
                
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if(isset($_POST["orderNumber"])){
                // delete destinations
                $query = $em->createQuery('DELETE AcmeCLSclientGovBundle:OrderDestinations c WHERE c.order_no = '.$model->getOrderNo());
                $query->execute(); 
                
                // delete travellers
                $query = $em->createQuery('DELETE AcmeCLSclientGovBundle:OrderTravellers c WHERE c.order_no = '.$model->getOrderNo());
                $query->execute(); 
            }
            
            // save travel details
            // $_POST["destination"] as travels counter
            for($i=0; $i<count($_POST["destination"]); $i++){
                
                $_POST["destination"][$i] = filter_var($_POST['destination'][$i], FILTER_SANITIZE_STRING);
                $_POST["departureDate"][$i] = filter_var($_POST['departureDate'][$i], FILTER_SANITIZE_STRING);
                $_POST["entryOption"][$i] = filter_var($_POST['entryOption'][$i], FILTER_SANITIZE_STRING);
                $_POST["entryDateCounty"][$i] = filter_var($_POST['entryDateCounty'][$i], FILTER_SANITIZE_STRING);
                $_POST["departureDateCountry"][$i] = filter_var($_POST['departureDateCountry'][$i], FILTER_SANITIZE_STRING);
                
                $order_travel = new OrderDestinations();
                $order_travel->setOrderNo($model->getOrderNo());
                $order_travel->setCountryId($_POST["destination"][$i]);
                $order_travel->setDepartureDate($mod->changeFormatToOriginal($_POST["departureDate"][$i]));
                $order_travel->setEntryOption($_POST["entryOption"][$i]);
                $order_travel->setEntryDateCountry($mod->changeFormatToOriginal($_POST["entryDateCounty"][$i]));
                $order_travel->setDepartureDateCountry($mod->changeFormatToOriginal($_POST["departureDateCountry"][$i]));
                $order_travel->setTravelPurpose($_POST["travelPurpose"][$i]);
                $order_travel->setTravelPurpose($_POST["travelPurpose"][$i]);
                if($i == 0){
                    $order_travel->setSPrimary(1);
                }else{
                    $order_travel->setSPrimary(0);
                }
                
                $em->persist($order_travel);
                $em->flush();
            }
            
            // save traveller details
            // $_POST["travellerTitle"] as travellers counter
            for($i=0; $i<count($_POST["travellerTitle"]); $i++){
                $_POST["travellerTitle"][$i] = filter_var($_POST['travellerTitle'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerFname"][$i] = filter_var($_POST['travellerFname'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerLname"][$i] = filter_var($_POST['travellerLname'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerMname"][$i] = filter_var($_POST['travellerMname'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerEmail"][$i] = filter_var($_POST['travellerEmail'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerOrganisation"][$i] = filter_var($_POST['travellerOrganisation'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerOccupation"][$i] = filter_var($_POST['travellerOccupation'][$i], FILTER_SANITIZE_STRING);
                
                
                $_POST["travellerFname"][$i] = str_replace("&#39;","'", $_POST["travellerFname"][$i]);
                $_POST["travellerMname"][$i] = str_replace("&#39;","'", $_POST["travellerMname"][$i]);
                $_POST["travellerLname"][$i] = str_replace("&#39;","'", $_POST["travellerLname"][$i]);
                
                $order_travellers = new OrderTravellers();
                $order_travellers->setOrderNo($model->getOrderNo());
                $order_travellers->setTitle($_POST["travellerTitle"][$i]);
                $order_travellers->setFname($_POST["travellerFname"][$i]);
                $order_travellers->setLname($_POST["travellerLname"][$i]);
                $order_travellers->setMname($_POST["travellerMname"][$i]);
                $order_travellers->setEmail($_POST["travellerEmail"][$i]);
                $order_travellers->setOrganisation($_POST["travellerOrganisation"][$i]);
                $order_travellers->setOccupation($_POST["travellerOccupation"][$i]);
                
                if(strtolower(trim($_POST["travellerOccupation"][$i])) == 'a dependant' || strtolower(trim($_POST["travellerOccupation"][$i])) == 'a spouse'){
                    
                    $_POST["rpinfoFullname"][$i] = filter_var($_POST['rpinfoFullname'][$i], FILTER_SANITIZE_STRING);
                    $_POST["rpinfoPositionAtPost"][$i] = filter_var($_POST['rpinfoPositionAtPost'][$i], FILTER_SANITIZE_STRING);
                    $_POST["rpinfoNameOfPost"][$i] = filter_var($_POST['rpinfoNameOfPost'][$i], FILTER_SANITIZE_STRING);
                    $_POST["rpinfoCity"][$i] = filter_var($_POST['rpinfoCity'][$i], FILTER_SANITIZE_STRING);
                    
                    $order_travellers->setRpinfoFullname($_POST['rpinfoFullname'][$i]);
                    $order_travellers->setRpinfoPositionAtPost($_POST['rpinfoPositionAtPost'][$i]);
                    $order_travellers->setRpinfoNameOfPost($_POST['rpinfoNameOfPost'][$i]);
                    $order_travellers->setRpinfoCity($_POST['rpinfoCity'][$i]);
                }
                
                $_POST["travellerPhone"][$i] = filter_var($_POST['travellerPhone'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerBirthDate"][$i] = filter_var($_POST['travellerBirthDate'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerGender"][$i] = filter_var($_POST['travellerGender'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerNearestCapitalCity"][$i] = filter_var($_POST['travellerNearestCapitalCity'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerNationality"][$i] = filter_var($_POST['travellerNationality'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerPassportType"][$i] = filter_var($_POST['travellerPassportType'][$i], FILTER_SANITIZE_STRING);
                $_POST["travellerPassportNumber"][$i] = filter_var($_POST['travellerPassportNumber'][$i], FILTER_SANITIZE_STRING);
                
                $order_travellers->setPhone($_POST["travellerPhone"][$i]);
                $order_travellers->setBirthDate($mod->changeFormatToOriginal($_POST["travellerBirthDate"][$i]));
                $order_travellers->setGender($_POST["travellerGender"][$i]);
                $order_travellers->setNearestCapitalCity($_POST["travellerNearestCapitalCity"][$i]);
                $order_travellers->setNationality($_POST["travellerNationality"][$i]);
                $order_travellers->setPassportType($_POST["travellerPassportType"][$i]);
                $order_travellers->setPassportNumber($_POST["travellerPassportNumber"][$i]);
                if($i == 0){
                    $order_travel->setSPrimary(1);
                }else{
                    $order_travel->setSPrimary(0);
                }
                $em->persist($order_travellers);
                $em->flush();
            }
            
            
            $validation_error_count = 0;
            // input validation
            if($_POST["hid_submit"] == 1){
                $blank_inputs = array();
                for($i=0; $i<count($_POST["destination"]); $i++){
                    if(trim($_POST["destination"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'destination'.$i;
                    }

                    if(trim($_POST["departureDate"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'departureDate'.$i;
                    }

                    if(trim($_POST["entryDateCounty"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'entryDateCounty'.$i;
                    }

                    if(trim($_POST["departureDateCountry"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'departureDateCountry'.$i;
                    }

                    if(trim($_POST["travelPurpose"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travelPurpose'.$i;
                    }
                }
                
                 for($i=0; $i<count($_POST["travellerTitle"]); $i++){
                    if(trim($_POST["travellerTitle"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerTitle'.$i;
                    }
                    
                    if(trim($_POST["travellerFname"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerFname'.$i;
                    }
                    
                    if(trim($_POST["travellerLname"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerLname'.$i;
                    }
                    
                    if(trim($_POST["travellerEmail"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerEmail'.$i;
                    }
                    
                    if(trim($_POST["travellerOrganisation"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerOrganisation'.$i;
                    }
                    
                    if(trim($_POST["travellerOccupation"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerOccupation'.$i;
                    }
                    
                    if(trim($_POST["travellerPhone"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerPhone'.$i;
                    }
                    
                    if(trim($_POST["travellerBirthDate"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerBirthDate'.$i;
                    }
                    
                    if(trim($_POST["travellerPassportType"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerPassportType'.$i;
                    }
                    
                    if(trim($_POST["travellerNationality"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerNationality'.$i;
                    }
                    
                    if(trim($_POST["travellerPassportNumber"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerPassportNumber'.$i;
                    }
                }
                
                if(trim($_POST['department']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'department';
                }
                if(trim($_POST['contactFirstname']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'contactFirstname';
                }
                if(trim($_POST['contactLastname']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'contactLastname';
                }
                if(trim($_POST['contactEmail']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'contactEmail';
                }
                if(trim($_POST['contactPhone']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'contactPhone';
                }
                
            }
            
            // get details and put them to array
            // for returning them to fields
            $travels = array();
            
            // $_POST["destination"] as travellers counter
            for($i=0; $i<count($_POST["destination"]); $i++){
                $travels[] = array("country_id"=>$_POST["destination"][$i],
                            "departure_date"=> $_POST["departureDate"][$i],
                            "entry_option"=> $_POST["entryOption"][$i],
                            "entry_date_country"=> $_POST["entryDateCounty"][$i],
                            "departure_date_country"=> $_POST["departureDateCountry"][$i],
                            "travel_purpose"=> $_POST["travelPurpose"][$i]
                    );
            }
            
            // get details and put them to array
            // for returning them to fields
            $travellers = array();
            
            // $_POST["title"] as travellers counter
            for($i=0; $i<count($_POST["travellerTitle"]); $i++){
                $travellers[] = array("title"=>$_POST["travellerTitle"][$i],
                            "fname"=> $_POST["travellerFname"][$i],
                            "mname"=> $_POST["travellerMname"][$i],
                            "lname"=> $_POST["travellerLname"][$i],
                            "email"=> $_POST["travellerEmail"][$i],
                            "organisation"=> $_POST["travellerOrganisation"][$i],
                            "occupation"=> $_POST["travellerOccupation"][$i],
                    
                            "rpinfo_fullname"=> $_POST['rpinfoFullname'][$i],
                            "rpinfo_position_at_post"=> $_POST['rpinfoPositionAtPost'][$i],
                            "rpinfo_name_of_post"=> $_POST['rpinfoNameOfPost'][$i],
                            "rpinfo_city"=> $_POST['rpinfoCity'][$i],
                    
                            "phone"=> $_POST["travellerPhone"][$i],
                            "birth_date"=> $_POST["travellerBirthDate"][$i],
                            "gender"=> $_POST["travellerGender"][$i],
                            "nearest_capital_city"=> $_POST["travellerNearestCapitalCity"][$i],
                            "nationality"=> $_POST["travellerNationality"][$i],
                            "passport_number"=> $_POST["travellerPassportNumber"][$i],
                            "passport_type"=> $_POST["travellerPassportType"][$i]
                    );
            }
            
            // post details
            $post = array(
                'travels' => $travels,
                'travellers' => $travellers,
                'pri_dept_contact_department_id' => $_POST['department'],
                'pri_dept_contact_fname' => $_POST['contactFirstname'],
                'pri_dept_contact_lname' => $_POST['contactLastname'],
                'pri_dept_contact_email' => $_POST['contactEmail'],
                'pri_dept_contact_phone' => $_POST['contactPhone'],
                'blank_inputs'=>(isset($blank_inputs)) ? $blank_inputs : array()
            );
            
            
            if($error_count == 0){
                
                if(trim($_POST['contactEmail'])!="" && $mod->isEmailValid($_POST['contactEmail']) == false){
                    $errors = array();
                    $errors[] = array('message'=>$_POST['contactEmail'].' is not a valid email.');
                    $error_count += 1;
                }
                
            }
            
            if($error_count > 0 || $validation_error_count > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($validation_error_count > 0){
                    $this->get('session')->getFlashBag()->add(
                            'validation-error',
                            'Please check the highlighted required fields and complete the form.'
                        );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors
                    );
                }
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                // return to tpn page
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'departments'=>$this->getDepartments(),
                        'nearest_capital_cities'=> $this->getNearestCapitalCities(),
                        'post'=> $post,
                        'user_details'=> $user_details
                    ));
                
            }else{
                // commit changes
                $em->getConnection()->commit(); 
                
                // if save only
                if($model->getStatus() == 1){
                    
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been saved'
                        );
                    
                    // return to tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn') . "?order=".$model->getOrderNo());
                    }
                }else{
                    
                    // return to review tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review') . "?order=".$model->getOrderNo());
                    }
                }
                
            }
            
            
            
            
            
            
            
            
        // page opening
        }else{
            
            if($session->get('user_type') == 'admin-user'){
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get("client_id");
                $user_details = '';
            }
            $client_id= intval($client_id);

            // if order number is set
            if(isset($_GET["order"])){
                $_GET["order"] = intval($_GET["order"]);

                $post = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
                
                if(count($post) > 0 && $post['order_type'] == 2){
                    
                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:index.html.twig',
                        array(
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles(),
                            'passport_types'=>$this->getPassportTypes(),
                            'departments'=>$this->getDepartments(),
                            'nearest_capital_cities'=> $this->getNearestCapitalCities(),
                            'order_no'=> $_GET["order"],
                            'post' => $post,
                            'user_details'=> $user_details
                        ));
                    
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order"].' not found!' 
                        ));
                }
                
            // if order number is not set
            }else{
            
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'departments'=>$this->getDepartments(),
                        'nearest_capital_cities'=> $this->getNearestCapitalCities(),
                        'user_details'=> $user_details
                    ));
            }
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * 2nd Step: Review TPN
     */
    public function reviewTPNAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-review');
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $_POST["orderNumber"]= intval($_POST["orderNumber"]);
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(3); // 3= Review Order
            }else{
                $model->setStatus(2); // 2= Review TPN
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 2){

                // set flash message
                $this->get('session')->getFlashBag()->add(
                        'success',
                        'Your order has been saved'
                    );

                // return to review tpn page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
            
        // review tpn
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            
            if(count($data) > 0 && $data['order_type'] == 2){
                // if order status done with the destination page
                // if not, send to destination page
                if( $data["status"] >= 2 ){ 

                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:reviewTPN.html.twig',
                        array('order_no'=> $_GET["order"],
                            'post' => $data,
                            'user_details'=> $user_details));

                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
    }
    
    
    
    
    
    
    /**
     * 3rd Step: Review Order
     */
    public function reviewOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-review-order');
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST['orderNumber'], $client_id);
            if($data["status"]>=10){
                return $this->redirect($this->generateUrl('cls_client_gov_view_order_tpn') . "?order_no=".$_POST['orderNumber']);
            }
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(4); // 4= Place Order
            }else{
                $model->setStatus(3); // 3= Review Order
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 3){

                // return to review tpn page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_place_order') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            if(count($data) > 0 && $data['order_type'] == 2){
                // if order status done with the review tpn page
                // if not, send to review tpn page
                if( $data["status"] >= 3 ){
                    if($data["status"]>=10){
                        return $this->redirect($this->generateUrl('cls_client_gov_view_order_tpn') . "?order_no=".$_GET["order"]);
                    }else{
                        $em = $this->getDoctrine()->getManager();
                        $model = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));

                        return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:reviewOrder.html.twig',
                            array('order_no'=> $_GET["order"],
                                'tpn_settings' => $model,
                                'data'=> $data,
                                'user_details'=>$user_details
                                ));
                    }
                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
        
    }
    
    
    public function discountUpdateAction(){ 
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') != '' && isset($_POST["discountCode"]) && isset($_POST["orderNumber"])){
            
            $_POST['orderNumber'] = filter_var($_POST['orderNumber'], FILTER_SANITIZE_STRING);
            $_POST['discountCode'] = filter_var($_POST['discountCode'], FILTER_SANITIZE_STRING);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('p')
                ->where('p.code = :code')
                ->setParameter('code', $_POST["discountCode"])
                ->getQuery();
            $data = $query->getArrayResult();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            // get tpn settings
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"], 'client_id'=>$client_id));
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));

            //$tpn_price = ($user->getCanGetSpecialPrice() == 1) ? ($tpn_settings->getTpn() + ($tpn_settings->getTpn() * ($user->getSpecialPrice()/100))) : $tpn_settings->getTpn();
            $tpn_price = $tpn_settings->getTpn();
            $total_tpn = $tpn_price * $model->getTpnQty();
            
            //$additional_tpn_price = ($user->getCanGetSpecialPrice() == 1) ? ($tpn_settings->getTpnAdditional() + ($tpn_settings->getTpnAdditional() * ($user->getSpecialPrice()/100))) : $tpn_settings->getTpnAdditional();
            $additional_tpn_price = $tpn_settings->getTpnAdditional();
            $total_tpn_additional = $additional_tpn_price * $model->getTpnAdditionalQty();

            $subtotal = $total_tpn + $total_tpn_additional;
                
            if(count($data) > 0){
                
                $model->setDiscountCode($_POST['discountCode']);
                $model->setDiscountRate($data[0]["rate"]);
                
                $discount_rate = $data[0]["rate"];
                $subtotal_discount = $subtotal * ($discount_rate / 100);
                
                
            }else{
                
                $model->setDiscountCode("");
                $model->setDiscountRate(0);
                
                $discount_rate = 0;
                $subtotal_discount = 0;
            }
            
            // subtotal
            $subtotal = $subtotal - $subtotal_discount;


            $tpn_gst = $total_tpn * 0.10;
            $tpn_additional_gst = $total_tpn_additional * 0.10;

            $gst = $tpn_gst + $tpn_additional_gst;
            $gst_discount = $gst * ($discount_rate / 100);
            // gst
            $gst = $gst - $gst_discount;

            $model->setGrandTotal($subtotal + $gst);
            
            $em->persist($model);
            $em->flush();
            
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?order=".$model->getOrderNo());
            }
        }else{
            
            return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?order=".$_POST['orderNumber']);
            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    /**
     * 4th Step: Place Order
     */
    
    public function placeOrderAction(){ 
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($order->getStatus() >= 10){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            $em->getConnection()->beginTransaction();

            
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            $_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
            if(isset($_POST["accountNumber"])){
                $_POST["accountNumber"] = filter_var($_POST['accountNumber'], FILTER_SANITIZE_STRING);
            }
            
            
            
            $model = new Payment();
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($client_id);
            $model->setOrderNo($_POST["orderNumber"]);
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption($_POST["paymentType"]);
            if(isset($_POST["accountNumber"])){
                $model->setAccountNo($_POST["accountNumber"]);
            }
//            $model->setNameOnCard($_POST["nameOnCard"]);
//            $model->setCardNumber($_POST["cardNumber"]);
//            $model->setCardExpiryMonth($_POST["cardExpiryMonth"]);
//            $model->setCardExpiryYear($_POST["cardExpiryYear"]);
//            //$model->setCardType($_POST["cardType"]);
//            $model->setCcvNumber($_POST["ccvNumber"]);
            
            if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                $total_order_price = $order->getGrandTotal();
            }else{
                $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
            }
            
            //$total_order_price = round($total_order_price);
            $total_order_price = number_format($total_order_price, 2,'.','');
            $model->setTotalOrderPrice($total_order_price);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
            if($_POST["paymentType"] == 1){
                $model->setSPaid(1);
            }else{
                $model->setSPaid(2);
            }
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'department'=>$_POST['department'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'paymentType'=>$_POST["paymentType"],
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            if($error_count == 0){
                if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors = array();
                    $special_error=1;
                }
                
                if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                    $error_count += 1;
                    
                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors[] = array('message'=>'Account number is not correct!');
                    $error_count += 1;
                }
            }
            
            
            
            if($error_count == 0){
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $data['mba_country']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS TPN Application'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                   $root_dir = dirname($this->get('kernel')->getRootDir());
                   
                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
                   
                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               /**
                * Credit card payment
                * Position: End
                */
                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();

                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'departments'=> $this->getDepartments(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'departments'=> $this->getDepartments(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }

            }else{
                
                //$order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                $order->setStatus(10); // 10= Order placed
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $order->setGrandTotal($total_order_price);
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                
                $em->persist($order);
                $em->flush();
                
                
                
                

                // commit changes
                $em->getConnection()->commit(); 

                // generate tpn letters
                $this->generateTPNletter($_POST["orderNumber"], $client_id);
                
                $em = $this->getDoctrine()->getManager();
                $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
                    
                //$host = $request->getScheme().'://'.$request->getHost();
                
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                // email invoice
                if(!isset($_POST['dont_send_invoice'])){
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings
                                        )
                                    )
                            );
                }
                
                // email submission summary
                if(!isset($_POST['dont_send_invoice'])){
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Submission", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:order_details_email_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'data'=> $this->getOrderDetailsByOrderNo($_POST["orderNumber"])
                                        )
                                    )
                            );
                }
                // manual order email to admin
                if($session->get('user_type') == 'admin-user'){
                    // email invoice
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings,
                                            'manual_order'=>1
                                            )
                                        )
                                );
                    }

                    // email submission summary
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Submission", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:order_details_email_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'data'=> $this->getOrderDetailsByOrderNo($_POST["orderNumber"]),
                                            'manual_order'=>1
                                            )
                                        )
                                );
                    }
                }
                
                if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                    // email invoice (copy to admins)
                    if(!isset($_POST['dont_send_invoice'])){
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmail($email_copies, "help@capitallinkservices.com.au", "TPN Order Reciept (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings
                                            )
                                        )
                                );
                    }
                }
                
                if($session->get('user_type') == 'admin-user'){
                    /* + create log */
                    $logDetails = "New Third Person Note (TPN) application (order no. ".$_POST["orderNumber"].") has been manually created and submitted by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }else{
                    /* + create log */
                    $logDetails = "New Third Person Note (TPN) application (order no. ".$_POST["orderNumber"].") has been created and submitted by <a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$session->get('client_id')."' target='_blank'>" . $session->get('fname') . " " . $session->get('lname') . "</a>";
                    $this->createLog('client', $session->get('client_id'), $session->get('user_type'), $logDetails);
                    /* - create log */
                }
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );

                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$client_id);
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
        
            if(count($data) > 0 && $data['order_type'] == 2){
                // if order status done with the review tpn page
                // if not, send to review tpn page
                if( $data["status"] == 4 ){


                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:placeOrder.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'user_details'=>$user_details
                            ));

                }else{

                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_review_order') . "?order=".$_GET["order"]);
                    }
                }

            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * TPN RESUBMISSION
     */
    
    public function tpnResubmissionAction(){
        $request = $this->getRequest();
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tpn-resubmission');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('email') == '' || $session->get('user_type') != 'government'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $_POST['tpnNo'] = filter_var($_POST['tpnNo'], FILTER_SANITIZE_STRING);
            
            $data = $this->getTpnDetailsById($_POST['tpnNo']);
            
            if($data["status"] == 2){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                $_POST["tpnSrc"] = strip_tags($_POST["tpnSrc"], '<p><b><strong><br><div><img><span>');
                $_POST["tpnSrc"] = str_replace('<span class="tpnno">xx xxx xxxxx</span>', '<span class="tpnno">'. trim($_POST["tpnNo"]) .'</span>', $_POST["tpnSrc"]);
                $_POST["tpnSrc"] = str_replace('<span class="tpnno"><b>xx xxx xxxxx</b></span>', '<span class="tpnno"><b>'. trim($_POST["tpnNo"]) .'</b></span>', $_POST["tpnSrc"]);
                $_POST["tpnSrc"] = str_replace('<span class="tpnno"><strong>xx xxx xxxxx</strong></span>', '<span class="tpnno"><strong>'. trim($_POST["tpnNo"]) .'</strong></span>', $_POST["tpnSrc"]);
                
                $model = $em->getRepository('AcmeCLSclientGovBundle:Tpn')->findOneBy(array('tpn_no'=>$_POST["tpnNo"]));
                $model->setStatus(0);
                $model->setDateLastUpdated($datetime->format("Y-m-d H:i:s"));
                $model->setTpnSrc($_POST['tpnSrc']);
                $em->persist($model);
                $em->flush();    


                $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$model->getOrderNo()));
                $order->setStatus(10);
                $em->persist($model);
                $em->flush();
                
                
                // commit changes
                $em->getConnection()->commit(); 


                //return $this->redirect($this->generateUrl('acme_cls_tpn_review')."?tpn=".$_POST["tpnNo"]);
                return $this->redirect($this->generateUrl('cls_client_gov_view_order_tpn')."?order_no=". $model->getOrderNo());
            }
            
        }else{
        
            $_GET['tpn'] = "FMB-" . filter_var($_GET['tpn'], FILTER_SANITIZE_STRING);
            
            $data = $this->getTpnDetailsById($_GET["tpn"]);
            
            if(count($data) > 0 && $data["status"] == 2){
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:tpn_resubmission.html.twig',
                    array('data'=> $data,
                        'site_url'=> $mod->siteURL(),
                        'tpn_notes' => $this->getTpnNotesByTpnNo($_GET["tpn"]))
                    );
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPN:error.html.twig',
                    array('title'=> 'Error',
                        'message' => 'TPN number not found!')
                    );
            }
        }
    }
    
    /**
     * get tpn details
     * @param type $tpn_no
     * @return type
     */
    public function getTpnDetailsById($tpn_no){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
        $query = $cust->createQueryBuilder('p')
            ->select("p.tpn_no, p.date_submitted, p.date_last_updated, p.order_no, p.client_id, o.primary_traveller_name, p.departure_date,
                d.code as department, c.countryName as destination, p.tpn_src, u.email, u.fname, o.pri_dept_contact_email, p.status")
            ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = p.order_no')
            ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
            ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.destination')
            ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = p.client_id')
            ->where('p.tpn_no = :tpn_no')
            ->setParameter('tpn_no', $tpn_no)
            ->getQuery();
        $results = $query->getArrayResult();
        
        if(count($results) > 0){
            
                $results[0]['tpn_src'] = str_replace('<span class="tpnno">'. $results[0]['tpn_no'] .'</span>', '<span class="tpnno">xx xxx xxxxx</span>', $results[0]['tpn_src']);
                $results[0]['tpn_src'] = str_replace('<span class="tpnno"><b>'. $results[0]['tpn_no'] .'</b></span>', '<span class="tpnno"><b>xx xxx xxxxx</b></span>', $results[0]['tpn_src']);
                $results[0]['tpn_src'] = str_replace('<span class="tpnno"><strong>'. $results[0]['tpn_no'] .'</strong></span>', '<span class="tpnno"><strong>xx xxx xxxxx</strong></span>', $results[0]['tpn_src']);
            
            $data = array('tpn_no'=> $results[0]['tpn_no'],
                'date_submitted'=> $results[0]['date_submitted'],
                'date_last_updated'=> $results[0]['date_last_updated'],
                'order_no'=> $results[0]['order_no'],
                'client_id'=> $results[0]['client_id'],
                'primary_traveller_name'=> $results[0]['primary_traveller_name'],
                'departure_date'=> $results[0]['departure_date'],
                'department'=> $results[0]['department'],
                'destination'=> $results[0]['destination'],
                'tpn_src'=> $results[0]['tpn_src'],
                'email'=> $results[0]['email'],
                'fname'=> $results[0]['fname'],
                'pri_dept_contact_email'=> $results[0]['pri_dept_contact_email'],
                'status'=> $results[0]['status']
                );
            
            return $data;
        }else{
            return array();
        }
    }
    
}
