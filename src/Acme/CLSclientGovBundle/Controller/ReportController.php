<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Model;

class ReportController extends Controller
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'report');
        
        if($session->get('email') == '' || ($session->get('user_type') != 'government' && $session->get('user_type') != 'corporate')){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        return $this->render('AcmeCLSclientGovBundle:Report:index.html.twig',
                array('get'=> $_GET)
                );
    }
    
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') == '' || ($session->get('user_type') != 'government' && $session->get('user_type') != 'corporate')){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND a.date_submitted >= "'.$_GET['from'].'"';
            }
        }

        // date to filter
        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND a.date_submitted <= "'.$_GET['to'].'"';
            }
        }

        // ticket type filter
        if(isset($_GET['ticket'])){

            if(trim($_GET['ticket']) != ''){
                $_GET['ticket'] = filter_var($_GET['ticket'], FILTER_SANITIZE_STRING);
                $_GET['ticket'] = trim($_GET['ticket']);
                $_GET['ticket'] = intval($_GET['ticket']);
                $condition .= ' AND a.order_type = '.$_GET['ticket'];
            }
        }

        // payment status filter
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND b.s_paid = '.$_GET['paymentstat'];
            }
        }

        // is paid on account filter
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND b.payment_option = '.$_GET['onaccount'];
            }
        }

        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT 
                a.date_submitted, 
                a.order_no, 
                a.order_type,
                (CASE  
                    WHEN a.order_type = 1 THEN 'VISA' 
                    WHEN a.order_type = 2 THEN 'TPN'  
                    WHEN a.order_type = 3 THEN 'TPN+VISA'  
                    WHEN a.order_type = 4 THEN 'COURIER-PASSPORT'  
                    WHEN a.order_type = 5 THEN 'POLICE CLEARANCE'  
                    WHEN a.order_type = 6 THEN 'VISA'  
                    WHEN a.order_type = 7 THEN 'DOCUMENT DELIVERY'  
                    WHEN a.order_type = 8 THEN 'RUSSIAN VISA VOUCHER'
                    WHEN a.order_type = 9 THEN 'DOCUMENT LEGALISATION'
                    ELSE 'Undefined'
                END) as ticket_type,
                a.primary_traveller_name,
                (CASE
                    WHEN b.s_paid = 1 THEN 'Paid'
                    ELSE 'Pending'
                END) as payment_status,
                (CASE
                    WHEN b.payment_option = 0 THEN 'Yes'
                    ELSE 'No'
                END) as on_account,
                b.total_order_price
            FROM tbl_orders a 
            LEFT JOIN tbl_payment b ON b.order_no = a.order_no
            WHERE a.client_id=".$session->get('client_id')." AND a.status >= 10 " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","order_no","order_type","ticket_type","primary_traveller_name","payment_status","on_account", "total_order_price", "order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    public function manifestAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'report-manifest');
        
        if($session->get('email') == '' || ($session->get('user_type') != 'government')){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSclientGovBundle:Report:manifest.html.twig',
                array('get'=> $_GET)
            );
    }
    
    public function listManifestAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('email') == '' || ($session->get('user_type') != 'government' && $session->get('user_type') != 'corporate')){
            return new Response('session expired');
        }
        
        $condition = '';
        $voptions = '';
        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).'"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).'"';
            }
        }
        
        
//        if(isset($_GET['department'])){
//
//            if(trim($_GET['department']) != ''){
//                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
//                $_GET['department'] = trim($_GET['department']);
//                $_GET['department'] = intval($_GET['department']);
//                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
//            }
//        }
        
        
//        if(isset($_GET['accountno'])){
//
//            if(trim($_GET['accountno']) != ''){
//                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
//                $_GET['accountno'] = trim($_GET['accountno']);
//                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
//            }
//        }
        
        if(isset($_GET['vvisa']) && $_GET['vvisa'] == 1){
            if(trim($voptions) != ''){
                $voptions .= ' OR ';
            }
            $voptions .= ' o.order_type = 1 OR o.order_type = 3 ';
        }
        
        if(isset($_GET['vtpn']) && $_GET['vtpn'] == 1){
            if(trim($voptions) != ''){
                $voptions .= ' OR ';
            }
            $voptions .= ' o.order_type = 2 OR o.order_type = 3 ';
        }
        
        if(isset($_GET['vpvisa']) && $_GET['vpvisa'] == 1){
            if(trim($voptions) != ''){
                $voptions .= ' OR ';
            }
            $voptions .= ' o.order_type = 6 ';
        }
        
        if(trim($voptions) == ''){
            $voptions = 'o.order_type = 1 OR o.order_type = 2 OR o.order_type = 3 OR o.order_type = 6';
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        // CONCAT(CONCAT(o.pri_dept_contact_fname, ' '), pri_dept_contact_lname) as client_name, 
        $statement = $connection->prepare("
            SELECT DATE_FORMAT(o.date_submitted,'%d-%m-%Y') AS date_submitted, 
                o.order_no, 
                o.status, 
                c.country_name,
                DATE_FORMAT(o.date_completed,'%d-%m-%Y') AS date_completed, 
                DATE_FORMAT(o.departure_date,'%d-%m-%Y') AS departure_date,
                (SELECT GROUP_CONCAT(CONCAT(opa.fname,' ',opa.lname))  FROM tbl_order_travellers opa WHERE opa.order_no=o.order_no) AS primary_traveller_name,
                (SELECT GROUP_CONCAT(opa.passport_number) FROM tbl_order_travellers opa WHERE opa.order_no=o.order_no) AS primary_traveller_passport_no
                
            FROM tbl_order_destinations od
            LEFT JOIN tbl_orders o ON o.order_no = od.order_no
            LEFT JOIN tbl_countries c ON c.id = od.country_id
            WHERE o.status >= 10 AND (". $voptions .")
                AND o.date_submitted != '' " . $condition);
        
        //o.order_type = 1 OR o.order_type = 3 OR o.order_type = 6
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","order_no","primary_traveller_name","primary_traveller_passport_no","country_name","departure_date");
        //print_r($results);
        //exit();
        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
}
//,
//                array('get'=> $_GET,
//                    'departments'=> $this->getDepartments(),
//                    'destinations' => $this->getCountries(),
//                    'cls_members' => $this->getAdminUsers()
//                )