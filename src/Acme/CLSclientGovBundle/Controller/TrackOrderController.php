<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSadminBundle\Entity\OrderDestinationNotes;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Entity\OrderNotes;
use Acme\CLSclientGovBundle\Model;

class TrackOrderController extends GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        $session->set('page_name', 'track-order');
        return $this->render('AcmeCLSclientGovBundle:TrackOrder:index.html.twig');
    }

    public function listVisaAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDestinations');
            $query = $cust->createQueryBuilder('v')
                ->select("v.id, o.date_submitted, v.order_no, v.country_id, v.status, c.countryName as destination, v.departure_date, o.primary_traveller_name, 
                    d.name as department, o.status as order_status, o.order_type, u.account_no")
                ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = v.order_no')
                ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = v.country_id')
                ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = o.client_id')
                ->where('o.client_id = :client_id AND o.status >= 10 AND (o.order_type = 1 OR o.order_type = 3 OR o.order_type = 6)')
                ->setParameter('client_id', $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","order_no","primary_traveller_name","destination","status","id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function listPoliceClearanceAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("
                SELECT o.date_submitted, p.account_no, o.order_no,  
                    (Select group_concat(concat(concat(opa.fname,' '), opa.lname))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicants,
                    (Select group_concat(opa.passport_no)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as passports,
                    (Select group_concat(opa.departure_date)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as departure_dates,
                     o.status, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem
                FROM tbl_orders o
                LEFT JOIN tbl_payment p ON p.order_no = o.order_no
                LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
                WHERE o.status >= 10 AND o.order_type = 5 AND o.client_id=".$session->get("client_id"));
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","order_no","applicants","passports","departure_dates","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function listDocLegalisationAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("
                SELECT o.date_submitted, p.account_no, o.order_no, o.status,  CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem
                FROM tbl_orders o
                LEFT JOIN tbl_payment p ON p.order_no = o.order_no
                LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
                WHERE o.status >= 10 AND o.order_type = 9 AND o.client_id=".$session->get("client_id"));
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","order_no","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function listDocDeliveryAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("
                SELECT o.date_submitted, p.account_no, o.order_no, CONCAT(CONCAT(p.fname, ' '), p.lname) as customer,
                    o.doc_package_total_pieces, o.doc_package_pickup_date, CONCAT(CONCAT(o.doc_package_ready_hr,':'), o.doc_package_ready_min) as pickup_time,
                    CONCAT(CONCAT(o.doc_package_office_close_hr,':'), o.doc_package_office_close_min) as office_close_time, 
                    dd.type as courier_type, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem,
                    o.status
                FROM tbl_orders o
                LEFT JOIN tbl_payment p ON p.order_no = o.order_no
                LEFT JOIN tbl_settings_document_delivery dd ON dd.id = o.doc_delivery_type
                LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
                WHERE o.status >= 10 AND o.order_type = 7 AND o.client_id=". $session->get('client_id') ."
                ORDER BY o.date_submitted DESC 
                LIMIT 30
                ");
            
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","account_no","order_no","customer","doc_package_total_pieces","doc_package_pickup_date",
                "pickup_time","office_close_time","courier_type","cls_team_mem","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function trackingDocDeliveryTemplateAction(){
        
        $session = $this->getRequest()->getSession();
        
        $_POST["order_no"] = intval($_POST["order_no"]);
        return new Response(
                $this->renderView("AcmeCLSclientGovBundle:TrackOrder:doc_delivery_tracking_template.html.twig",
                        array('data'=>$this->getOrderDetailsByOrderNoAndClientId($_POST['order_no'], $session->get("client_id"))))
            );
    }
    
    
    
    public function trackingTemplateAction(){
        
        $session = $this->getRequest()->getSession();
        $_POST["destination_id"] = intval($_POST["destination_id"]);
        return new Response(
                $this->renderView("AcmeCLSclientGovBundle:TrackOrder:visa_tracking_template.html.twig",
                        array('data'=>$this->getOrderDestinationDetails($_POST["destination_id"] ,$session->get("client_id"), 'government')))
            );
    }
    
    public function trackingPoliceClearanceTemplateAction(){
        
        $session = $this->getRequest()->getSession();
        
        $_POST["order_no"] = intval($_POST["order_no"]);
        return new Response(
                $this->renderView("AcmeCLSclientGovBundle:TrackOrder:police_clearance_tracking_template.html.twig",
                        array('data'=>$this->getOrderDetailsByOrderNoAndClientId($_POST['order_no'], $session->get("client_id"))))
            );
    }
    
    
    public function addVisaApplicationDestinationNoteAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        if($session->get('email') == ''){
            return new Response("session expired");
        }
        
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if(trim($_POST["destination_id"]) != '' && trim($_POST["note"]) != ''){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();

            $_POST['destination_id'] = filter_var($_POST['destination_id'], FILTER_SANITIZE_STRING);
            $_POST['destination_id'] = intval($_POST['destination_id']);
            $_POST["note"] = filter_var($_POST["note"], FILTER_SANITIZE_STRING);
            
            $model = new OrderDestinationNotes();
            $model->setDestinationId($_POST["destination_id"]);
            $model->setDateAdded($datetime->format("Y-m-d H:i:s"));
            $model->setNoteBy($session->get("client_id"));
            if($session->get('user_type') == 'government'){
                $model->setUserType('Gov. Client');
            }else if($session->get('user_type') == 'public'){
                $model->setUserType('Public Client');
            }else{
                $model->setUserType('Corporate');
            }
            $model->setNote($_POST["note"]);
            $model->setNoteByName($session->get("fname"));
            $em->persist($model);
            $em->flush();    

            
            
            

            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, u.fname, u.lname, u.email, o.order_type")
                ->leftJoin('AcmeCLSadminBundle:AdminUser', 'u', 'WITH', 'u.id = o.visa_cls_team_member')
                ->leftJoin('AcmeCLSclientGovBundle:OrderDestinations', 'od', 'WITH', 'od.order_no = o.order_no')
                ->leftJoin('AcmeCLSadminBundle:OrderDestinationNotes', 'odn', 'WITH', 'odn.destination_id = od.id')
                ->where("od.id = :destination_id")
                ->setParameter("destination_id", $_POST["destination_id"])
                ->getQuery();
            $order =  $query->getArrayResult();
            
            
            if(count($order) > 0){
                $this->sendEmail($order[0]['email'], "help@capitallinkservices.com.au", "CLS Order Note", 
                    $this->renderView('AcmeCLSclientGovBundle:TrackOrder:email_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'order_number'=>$order[0]['order_no'],
                                'order_type'=> 'VISA',
                                'admin_name'=>$order[0]['fname'] . ' ' . $order[0]['lname'],
                                'gov_client_name'=>$session->get("fname"),
                                'note'=>$_POST["note"]
                                )
                            )
                );
            }
            
            // commit changes
            $em->getConnection()->commit(); 
            
            $data = array('note'=>$_POST["note"], 'date_added'=>$datetime->format("d-m-Y H:i:s"), 'note_by_name'=>$session->get("fname"), 'user_type'=>$model->getUserType());
            
            return new Response(json_encode($data));
        }
    }
    
    
    public function addPoliceClearanceApplicationNoteAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        if($session->get('email') == ''){
            return new Response("session expired");
        }
       
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if(trim($_POST["order_no"]) != '' && trim($_POST["note"]) != ''){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();

            $_POST['order_no'] = filter_var($_POST['order_no'], FILTER_SANITIZE_STRING);
            $_POST['order_no'] = intval($_POST['order_no']);
            $_POST["note"] = filter_var($_POST["note"], FILTER_SANITIZE_STRING);
            
            $model = new OrderNotes();
            $model->setOrderNo($_POST['order_no']);
            $model->setDateAdded($datetime->format("Y-m-d H:i:s"));
            $model->setNoteBy($session->get("client_id"));
            if($session->get('user_type') == 'government'){
                $model->setUserType('Gov. Client');
            }else if($session->get('user_type') == 'public'){
                $model->setUserType('Public Client');
            }else{
                $model->setUserType('Corporate');
            }
            $model->setNote($_POST["note"]);
            $model->setNoteByName($session->get("fname"));
            $em->persist($model);
            $em->flush();    

            
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, u.fname, u.lname, u.email, o.order_type")
                ->leftJoin('AcmeCLSadminBundle:AdminUser', 'u', 'WITH', 'u.id = o.visa_cls_team_member')
                ->where("o.order_no = :order_no")
                ->setParameter("order_no", $_POST["order_no"])
                ->getQuery();
            $order =  $query->getArrayResult();
            
            
            if(count($order) > 0){
                $order_type = '';
                if($order[0]['order_type'] == 5){
                    $order_type = 'Police Clearance';
                }elseif($order[0]['order_type'] == 9){
                    $order_type = 'Document Legalisation';
                }
                
                $this->sendEmail($order[0]['email'], "help@capitallinkservices.com.au", "CLS Order Note", 
                    $this->renderView('AcmeCLSclientGovBundle:TrackOrder:email_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'order_number'=>$order[0]['order_no'],
                                'order_type'=> $order_type,
                                'admin_name'=>$order[0]['fname'] . ' ' . $order[0]['lname'],
                                'gov_client_name'=>$session->get("fname"),
                                'note'=>$_POST["note"]
                                )
                            )
                );
            }
            
            // commit changes
            $em->getConnection()->commit(); 
            

            $data = array('note'=>$_POST["note"], 'date_added'=>$datetime->format("d-m-Y H:i:s"), 'note_by_name'=>$session->get("fname"), 'user_type'=>$model->getUserType());
            
            return new Response(json_encode($data));
        }
    }
}
