<?php

namespace Acme\AphaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Acme\AphaBundle\Model;


class ResetPasswordController extends Controller
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('email') == ''){
            return $this->render('AcmeAphaBundle:ResetPassword:index.html.twig');
        }else{
            return $this->redirect($this->generateUrl('user_submit_home'));
        }
    }
    
    public function resetSuccessAction()
    {
        $session = $this->getRequest()->getSession();
        //if($session->get('email') == ''){
        
            return $this->render('AcmeAphaBundle:ResetPassword:resetsuccess.html.twig');
//        }else{
//            return $this->redirect($this->generateUrl('user_submit_home'));
//        }
    }
    
    public function checkEmailAction(){
        
        $mod = new Model\GlobalModel();
        if(!isset($_POST['email'])){
            exit('email_required');
        }
        if(trim($_POST['email']) == ''){
            exit('email_required');
        }
        if(!$mod->isEmailValid($_POST['email'])){
            exit('email_invalid');
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM tbl_user WHERE email='".$_POST['email']."'");
        $statement->execute();
        $rs = $statement->fetchAll();
        if(count($rs) <= 0 ){
            exit('email_invalid');
        }
        
        $pincode = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $statement = $connection->prepare("UPDATE tbl_user SET reset_pin = '".$pincode."' WHERE email='".$_POST['email']."'");
        $statement->execute();
        
        
        /** start: send notice */
        $mod->sendEmail(
        //$this->sendEmail(
                    $_POST['email'], 
                    'info@capitallinkservices.com.au',
                    "APHAAPP - Reset password", 
                    $this->renderView(
                        'AcmeAphaBundle:ResetPassword:email.html.twig',
                        array('fullname' => $rs[0]['fname'],
                            'page_title'=>'APHAAPP',
                            'email_to'=>$_POST['email'],
                            'pincode'=>$pincode,
                            'request_uri'=>$this->getRequest()->getUriForPath('/password/resetform')
                            )
                    )
                );
        
        
        /** end: send notice */
        return new response('success');
    }
    
    
    
    public function resetFormAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('email') != ''){
            return $this->redirect($this->generateUrl('user_submit_home'));
        }
        
        $mod = new Model\GlobalModel();
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $error_count = 0;
        if(!isset($_GET['em']) || !isset($_GET['rp'])){
             $error_count += 1;
        }else if(!$mod->isEmailValid($_GET['em'])){
            $error_count += 1;
        }else{
            $statement = $connection->prepare("SELECT * FROM tbl_user WHERE email='".$_GET['em']."'
                    AND reset_pin = '".$_GET['rp']."'");
            $statement->execute();
            $rs = $statement->fetchAll();
            if(count($rs) <= 0){
                $error_count += 1;
            }
        }
        
        
        if($error_count == 0 ){
            return $this->render('AcmeAphaBundle:ResetPassword:resetform.html.twig',
                    array('email'=>$_GET['em'],'reset_pin'=>$_GET['rp'])
                    );
        }else{
            return $this->render('AcmeAphaBundle:ResetPassword:error.html.twig');
        }
    }
    
    public function resetSubmitAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('email') != ''){
            return $this->redirect($this->generateUrl('acme_apha_user_login'));
        }
        $mod = new Model\GlobalModel();
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $error_count = 0;
        $error_msg = "";
        if(!isset($_POST['email']) && !isset($_POST['reset_pin']) && !isset($_POST['password']) && !isset($_POST['confirm_password'])){
             $error_count += 1;
             $error_msg = "Invalid data posted.";
        }else if(trim($_POST['password']) == ''){
            $error_count += 1;
            $error_msg = "Password is required.";
        }else if(trim($_POST['confirm_password']) == ''){
            $error_count += 1;
            $error_msg = "Please confirm your password.";
        }else if(trim($_POST['password']) != trim($_POST['confirm_password'])){
            $error_count += 1;
            $error_msg = "Please confirm your password.";
        }else if(!$mod->isEmailValid($_POST['email'])){
            $error_count += 1;
            $error_msg = "Invalid email address.";
        }else{
            $statement = $connection->prepare("SELECT * FROM tbl_user WHERE email='".$_POST['email']."'
                    AND reset_pin = '".$_POST['reset_pin']."'");
            $statement->execute();
            $rs = $statement->fetchAll();
            if(count($rs) <= 0){
                $error_count += 1;
                $error_msg = "Invalid data posted.";
            }
            
        }
        
        if($error_count == 0 ){
            $statement = $connection->prepare("UPDATE tbl_user SET reset_pin='',password='".$mod->passGenerator($_POST['password'])."' 
                WHERE email='".$_POST['email']."' AND reset_pin = '".$_POST['reset_pin']."'");
            $statement->execute();
            
            $session->set('user_no_reset',$rs[0]['user_id']);
            
            return $this->redirect($this->generateUrl('password_reset_submit_success')); 
        }else{
            return $this->render('AcmeAphaBundle:ResetPassword:resetform.html.twig',
                    array('email'=>$_POST['email'],
                        'reset_pin'=>$_POST['reset_pin'],
                        'password'=>$_POST['password'],
                        'confirm_password'=>$_POST['confirm_password'],
                        'error'=>$error_msg)
                    );
        }
    }
    
    public function setSessionsAction(){
        $session = $this->getRequest()->getSession();
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM tbl_user WHERE user_id='".$session->get('user_no_reset')."'");
        $statement->execute();
        $rows = $statement->fetchAll();

        $session->set('user_id', $rows[0]['user_id']); 
        $session->set('email', $rows[0]['email']); 
        $session->set('fname', $rows[0]['fname']); 
        $session->set('lname', $rows[0]['lname']);
        $session->set('position', $rows[0]['position']);
        $session->set('email', $rows[0]['email']);
        $session->set('postal_street', $rows[0]['postal_street']);
        $session->set('postal_code', $rows[0]['postal_code']);
        $session->set('postal_state', $rows[0]['postal_state']);
        
        return new Response('success');
    }
    
}
