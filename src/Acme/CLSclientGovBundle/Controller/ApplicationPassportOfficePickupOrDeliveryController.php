<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSadminBundle\Entity\OrderPassportApplicants;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class ApplicationPassportOfficePickupOrDeliveryController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'passport-office-pickup-delivery');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hid_submit'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            
            $_POST["passportOfficeBookingNumber"] = filter_var($_POST['passportOfficeBookingNumber'], FILTER_SANITIZE_STRING);
            $_POST["passportOfficeBookingTime"] = filter_var($_POST['passportOfficeBookingTime'], FILTER_SANITIZE_STRING);
            $_POST["passportOfficeBookingTimeHour"] = filter_var($_POST['passportOfficeBookingTimeHour'], FILTER_SANITIZE_STRING);
            $_POST["passportOfficeBookingTimeMin"] = filter_var($_POST['passportOfficeBookingTimeMin'], FILTER_SANITIZE_STRING);
            
            
            $error_count = 0;
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            // new order
            if(!isset($_POST["orderNumber"])){
                $order = new Orders();
                $order->setClientId($client_id);
                $order->setOrderType(4);
            // update order
            }else{
                $_POST["orderNumber"] = intval($_POST["orderNumber"]);
                $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                
            }
            $order->setPassportOfficeBookingNo($_POST['passportOfficeBookingNumber']);
            $order->setPassportOfficeBookingTime($mod->changeFormatToOriginal($_POST['passportOfficeBookingTime']));
            $order->setPassportOfficeBookingTimeHr($_POST['passportOfficeBookingTimeHour']);
            $order->setPassportOfficeBookingTimeMin($_POST['passportOfficeBookingTimeMin']);
            if($_POST["hid_submit"] == 0){
                $order->setStatus(1); // 1= Details level
            }else{
                $order->setStatus(2); // 2= Review Order
            }
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            if(isset($_POST['passportApplicantFullname'][0])){
            $order->setPrimaryTravellerName($_POST['passportApplicantFullname'][0]);
            }
            
            $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            $ccp = $em->getRepository('AcmeCLSclientGovBundle:CreditCardProcessing')->findOneBy(array('id'=>1));
            
            $cost = $passport->getCost();
            
            if(count($_POST['passportApplicantFullname']) > 1){
                $cost += (count($_POST['passportApplicantFullname']) -1) * $passport->getAdditionalCost();
            }
            
            
            
            
            if($user->getCanGetSpecialPrice() == 1){
                $cost = $cost + ($cost * ($user->getSpecialPrice()/100));
            }
            
            $cost = $cost + ($cost * (10/100));
            
            
            if($order->getDiscountRate() > 0){
                $cost = ($cost - ($cost * ($order->getDiscountRate()/100)));
            }
            
//            if($_POST["paymentType"] == 1){
//                $inc_card_fee = $cost * ($ccp->getFee() / 100);
//                $cost = $cost + $inc_card_fee;    
//            }
            
            //$cost = round($cost);
            $order->setGrandTotal($cost);
            
            if($session->get('user_type') == 'admin-user'){
                $order->setVisaClsTeamMember($session->get('admin_id'));
            }
                
            $em->persist($order);
            $em->flush();
            
            $passportApplicants = array();
            if(isset($_POST["orderNumber"])){
                
                // delete travellers
                $query = $em->createQuery('DELETE AcmeCLSadminBundle:OrderPassportApplicants c WHERE c.order_no = '.$order->getOrderNo());
                $query->execute(); 
            }
            
            for($i=0; $i<count($_POST["passportApplicantFullname"]); $i++){
                $order_pa = new OrderPassportApplicants();
                $order_pa->setOrderNo($order->getOrderNo());
                $order_pa->setFullname($_POST["passportApplicantFullname"][$i]);
                
                $personalPassport = (isset($_POST['personalPassport'][$i])) ? 1 : 0;
                $doPassport = (isset($_POST['doPassport'][$i])) ? 1 : 0;
                $birthCertificate = (isset($_POST['birthCertificate'][$i])) ? 1 : 0;
                $marriageCertificate = (isset($_POST['marriageCertificate'][$i])) ? 1 : 0;
                $certOfAustralianCitizenship = (isset($_POST['certOfAustralianCitizenship'][$i])) ? 1 : 0;
                
                $order_pa->setPersonalPassport($personalPassport);
                $order_pa->setDiplomaticOfficialPassport($doPassport);
                $order_pa->setBirthCertificate($birthCertificate);
                $order_pa->setMarriageCertificate($marriageCertificate);
                $order_pa->setCertificateOfAustralianCitizenship($certOfAustralianCitizenship);
                $em->persist($order_pa);
                $em->flush();
                
                
                $passportApplicants[] = array(
                    'fullname'=>$_POST["passportApplicantFullname"][$i],
                    'personal_passport'=>$personalPassport,
                    'diplomatic_official_passport'=>$doPassport,
                    'birth_certificate'=>$birthCertificate,
                    'marriage_certificate'=>$marriageCertificate,
                    'certificate_of_australian_citizenship'=>$certOfAustralianCitizenship
                );
            }
            
            
//            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
//            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
//            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
//            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
//            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
//            $_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
//            $_POST["accountNumber"] = filter_var($_POST['accountNumber'], FILTER_SANITIZE_STRING);
            
            
//            $model = new Payment();
//            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
//            $model->setClientId($client_id);
//            $model->setOrderNo($order->getOrderNo());
//            $model->setFname($_POST["fname"]);
//            $model->setLname($_POST["lname"]);
//            $model->setEmail($_POST["email"]);
//            $model->setPhone($_POST["phone"]);
//            $model->setMobile($_POST["mobile"]);
//            $model->setPaymentOption($_POST["paymentType"]);
//            $model->setAccountNo($_POST["accountNumber"]);
//            $model->setTotalOrderPrice($cost);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
//            $em->persist($model);
//            $em->flush();
//
//            $validator = $this->get('validator');
//            $errors = $validator->validate($model);
//            $error_count = count($errors);
            
            
            
            
            $validation_error_count = 0;
            // input validation
            if($_POST["hid_submit"] == 1){
                $blank_inputs = array();
                if(trim($_POST['passportOfficeBookingNumber']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'passportOfficeBookingNumber';
                }
                if(trim($_POST['passportOfficeBookingTime']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'passportOfficeBookingTime';
                }
                if(trim($_POST['passportOfficeBookingTimeHour']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'passportOfficeBookingTimeHour';
                }
                
                if(trim($_POST['passportOfficeBookingTimeMin']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'passportOfficeBookingTimeMin';
                }
                
                for($i=0; $i<count($_POST["passportApplicantFullname"]); $i++){
                    if(trim($_POST['passportApplicantFullname'][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'passportApplicantFullname'.$i;
                    }
                }
            }
            
            $post = array(
//                'fname'=>$_POST['fname'],
//                'lname'=>$_POST['lname'],
//                'email'=>$_POST['email'],
//                'phone'=>$_POST['phone'],
//                'mobile'=>$_POST['mobile'],
                'passport_office_booking_no'=> $_POST['passportOfficeBookingNumber'],
                'passport_office_booking_time'=> $_POST['passportOfficeBookingTime'],
                'passport_office_booking_time_hr'=> $_POST['passportOfficeBookingTimeHour'],
                'passport_office_booking_time_min'=> $_POST['passportOfficeBookingTimeMin'],
                'passport_applicants'=>$passportApplicants,
                'blank_inputs'=>(isset($blank_inputs)) ? $blank_inputs : array()
            );
            
//            if($error_count == 0){
//                if($user->getCanChargeCostToAccount() != 1 || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
//                    $errors = array();
//                    $special_error=1;
//                }
//                
//                if($user->getCanChargeCostToAccount() != 1){
//                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
//                    $error_count += 1;
//                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
//                    $errors[] = array('message'=>'Account number is not correct!');
//                    $error_count += 1;
//                }
//            }
            
            
//            if($error_count == 0){
//                /**
//                * Credit card payment
//                * Position: Start
//                */
//               $credit_card_error = 0;
//               if($_POST["paymentType"] == 1){
//                   $totalAmount = $cost * 100;
//                   $fields = array(
//                       'customerFirstName' => urlencode($_POST['fname']),
//                       'customerLastName' => urlencode($_POST['lname']),
//                       'customerEmail' => urlencode($_POST['email']),
//                       'customerAddress' => urlencode(''),
//                       'customerPostcode' => urlencode(''),
//                       'customerInvoiceDescription' => urlencode('CLS Passport Office Pickup or Delivery'),
//                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
//                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
//                       'cardNumber' => urlencode($_POST['cardNumber']),
//                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
//                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
//                       'trxnNumber' => urlencode('4230'),
//                       'totalAmount' => urlencode($totalAmount),
//                       'cvn' => urlencode($_POST['ccvNumber'])
//                   );
//
//
//
//                   //url-ify the data for the POST
//                   $fields_string = '';
//                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
//                   rtrim($fields_string, '&');
//
//
//
//                   //open connection
//                   $ch = curl_init();
//                   //set the url, number of POST vars, POST data
//                   curl_setopt($ch,CURLOPT_URL, 'http://cls.vcdev.net.tmp.anchor.net.au/eway/post-pay.php');
//                   curl_setopt($ch,CURLOPT_POST, count($fields));
//                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
//                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
//
//                   //execute post
//                   $credit_card_reponse = curl_exec($ch);
//
//                   $cr_result = json_decode($credit_card_reponse, true);
//
//
//                   if(isset($cr_result['error'])){
//                       $credit_card_error += 1;
//                   }
//
//
//                   //close connection
//                   curl_close($ch);
//               }
//               /**
//                * Credit card payment
//                * Position: End
//                */
//                
//                if($credit_card_error > 0){
//                    $errors = array();
//                    $special_error=1;
//                    
//                    $errors[] = array('message'=>$cr_result['error']);
//                    $error_count += 1;
//                }
//            }
            
            if($error_count > 0 || $validation_error_count > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                if(!isset($special_error)){
                    if($validation_error_count > 0){
                        $this->get('session')->getFlashBag()->add(
                                'validation-error',
                                'Please check the highlighted required fields and complete the form.'
                            );
                    }else{
                        $this->get('session')->getFlashBag()->add(
                            'error',
                            $errors
                        );
                    }
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:index.html.twig',
                        array(
                            'user'=>$this->getClientDetailsById($session->get("client_id")),
                            'credit_card_processing_fee'=>$this->getCreditCardProcessingFee(),
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles(),
                            'passport_types'=>$this->getPassportTypes(),
                            'cardtypes'=> $this->getCardTypes(),
                            'departments'=>$this->getDepartments(),
                            'cost'=>$passport,
                            'post'=>$post,
                            'user_details'=> $user_details
                        ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:index.html.twig',
                        array(
                            'user'=>$this->getClientDetailsById($session->get("client_id")),
                            'credit_card_processing_fee'=>$this->getCreditCardProcessingFee(),
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles(),
                            'passport_types'=>$this->getPassportTypes(),
                            'cardtypes'=> $this->getCardTypes(),
                            'departments'=>$this->getDepartments(),
                            'cost'=>$passport,
                            'errors'=>$errors,
                            'post'=>$post,
                            'user_details'=> $user_details
                        ));
                }
                
                
                
            
                
            
                
            }else{
                
                
                // commit changes
                $em->getConnection()->commit(); 
                
//                $data = $this->getOrderDetailsByOrderNoAndClientId($order->getOrderNo(), $client_id);
//                
//                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsPassport');
//                $query = $cust->createQueryBuilder('p')
//                    ->where('p.id = 1')
//                    ->getQuery();
//                $settings = $query->getArrayResult();
//
//                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
//                
//                // email invoice
//                $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Reciept", 
//                    $this->renderView('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:invoice_template.html.twig',
//                            array('domain'=>$mod->siteURL(),
//                                'order_no'=>$order->getOrderNo(),
//                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
//                                'settings_passport'=>$settings[0],
//                                'data'=>$data
//                                )
//                            )
//                    );
                
                // if save only
                if($order->getStatus() == 1){
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been saved'
                        );
                    
                    // return to the details page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery') . "?order=".$order->getOrderNo());
                    }
                    
                }else{
                    
                    // return to review tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?order=".$order->getOrderNo());
                    }
                    
                }
                
            }
            
        }else{
            
            if($session->get('user_type') == 'admin-user'){
                if(isset($_GET['client'])){
                    $_GET['client'] = intval($_GET['client']);
                }else{
                    return $this->redirect($this->generateUrl('acme_cls_admin_home'));
                }
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id', $client_id)
                ->getQuery();
            $user = $query->getArrayResult();
            
            
            // if order number is set
            if(isset($_GET["order"])){
                $_GET["order"] = intval($_GET["order"]);
                $post = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
                
                
                if(count($post) > 0 && $post['order_type'] == 4){
                    $em = $this->getDoctrine()->getManager();
                    $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:index.html.twig',
                        array(
                            'user'=>$this->getClientDetailsById($client_id),
                            'credit_card_processing_fee'=>$this->getCreditCardProcessingFee(),
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles(),
                            'passport_types'=>$this->getPassportTypes(),
                            'cardtypes'=> $this->getCardTypes(),
                            'departments'=>$this->getDepartments(),
                            'cost'=>$passport,
                            'user_details'=>$user_details,
                            'order_no'=>$_GET["order"],
                            'post'=>$post
                        ));
                }else{
                    
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order"].' not found!' 
                        ));
                    
                }
            }else{
                $em = $this->getDoctrine()->getManager();
                $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
                return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:index.html.twig',
                    array(
                        'user'=>$this->getClientDetailsById($client_id),
                        'credit_card_processing_fee'=>$this->getCreditCardProcessingFee(),
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'cardtypes'=> $this->getCardTypes(),
                        'departments'=>$this->getDepartments(),
                        'cost'=>$passport,
                        'user_details'=>$user_details
                    ));
            }
            
        }
    }
    
    
    
    /**
     * 2nd Step: Review Order
     */
    public function reviewOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'passport-office-pickup-delivery-review-order');
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST['orderNumber'], $client_id);
            if($data["status"]>=10){
                return $this->redirect($this->generateUrl('cls_client_gov_view_orders_courier_service') . "?order_no=".$_POST['orderNumber']);
            }
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(3); // 3= Place Order
            }else{
                $model->setStatus(2); // 2= Review Order
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 2){

                // return to review visa page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_place_order') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $em = $this->getDoctrine()->getManager();
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            if(count($data) > 0 && $data['order_type'] == 4){
                // if order status done with the review tpn page
                // if not, send to review tpn page
                if( $data["status"] >= 1 ){
                    if($data["status"]>=10){
                        return $this->redirect($this->generateUrl('cls_client_gov_view_orders_courier_service') . "?order_no=".$_GET["order"]);
                    }else{
                        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsPassport');
                        $query = $cust->createQueryBuilder('p')
                            ->where('p.id = 1')
                            ->getQuery();
                        $settings = $query->getArrayResult();

                        return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:reviewOrder.html.twig',
                            array('order_no'=> $_GET["order"],
                                'data'=> $data,
                                'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                            'special_price'=> $user->getSpecialPrice()
                                        ),
                                'user_details'=>$user_details,
                                'settings_passport'=>$settings[0]
                                ));
                    }
                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
        
    }
    
    
    
    
    public function discountUpdateAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') != '' && isset($_POST["discountCode"]) && isset($_POST["orderNumber"])){
            
            $_POST['orderNumber'] = filter_var($_POST['orderNumber'], FILTER_SANITIZE_STRING);
            $_POST['discountCode'] = filter_var($_POST['discountCode'], FILTER_SANITIZE_STRING);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('p')
                ->where('p.code = :code')
                ->setParameter('code', $_POST["discountCode"])
                ->getQuery();
            $data = $query->getArrayResult();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"], 'client_id'=>$client_id));
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            //$user->getCanGetSpecialPrice()
            
            $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            
            
            $post = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $items_total_cost = 0;
            $items_total_gst = 0;
            $item_total = 0;

            
            
            //$item_total += $post[0]['doc_delivery_cost'] + ($post[0]['doc_delivery_cost'] * 0.10);
            $items_total_cost += $passport->getCost();
            $items_total_gst +=  $items_total_cost * 0.10;
            $item_total = $items_total_cost + $items_total_gst;
                
            $subtotal = $items_total_cost;
            $gst = $items_total_gst;
            
            if(count($data) > 0){
                
                $model->setDiscountCode($_POST['discountCode']);
                $model->setDiscountRate($data[0]["rate"]);
                
                $discount_rate = $data[0]["rate"];
                $subtotal_discount = $subtotal * ($discount_rate / 100);
                $gst_discount = $gst * ($discount_rate / 100);
            }else{
                
                $model->setDiscountCode("");
                $model->setDiscountRate(0);
                
                $discount_rate = 0;
                $subtotal_discount = 0;
                $gst_discount = 0;
            }

            // subtotal
            $subtotal = $subtotal - $subtotal_discount;
            $gst = $gst - $gst_discount;
            
            
            $model->setGrandTotal($subtotal + $gst);
            if($user->getCanGetSpecialPrice() == 1){
                $model->setGrandTotal($model->getGrandTotal() + ($model->getGrandTotal() * ($user->getSpecialPrice()/100)));
            }
            
            $em->persist($model);
            $em->flush();
            
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?order=".$model->getOrderNo());
            }
        }else{
            
            return $this->redirect($this->generateUrl('cls_client_gov_passport_office_pickup_or_delivery_review_order') . "?order=".$_POST['orderNumber']);
            
        }
    }
    
    
    
    
    /**
     * 4th Step: Place Order
     */
    
    public function placeOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'passport-office-pickup-delivery-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($order->getStatus() >= 10){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            $em->getConnection()->beginTransaction();

            
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            $_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
            if(isset($_POST["accountNumber"])){
                $_POST["accountNumber"] = filter_var($_POST['accountNumber'], FILTER_SANITIZE_STRING);
            }
            
            $model = new Payment();
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($client_id);
            $model->setOrderNo($_POST["orderNumber"]);
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption($_POST["paymentType"]);
            if(isset($_POST["accountNumber"])){
                $model->setAccountNo($_POST["accountNumber"]);
            }
            
            if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                $total_order_price = $order->getGrandTotal();
            }else{
                $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
            }
            
            //$total_order_price = round($total_order_price);
            $total_order_price = number_format($total_order_price, 2,'.','');
            $model->setTotalOrderPrice($total_order_price);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
            if($_POST["paymentType"] == 1){
                $model->setSPaid(1);
            }else{
                $model->setSPaid(2);
            }
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'company'=>$_POST['company'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'paymentType'=>$_POST["paymentType"],
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            if($error_count == 0){
                if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors = array();
                    $special_error=1;
                }
                
                if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                    $error_count += 1;
                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors[] = array('message'=>'Account number is not correct!');
                    $error_count += 1;
                }
            }
            
            
            if($error_count == 0){
                
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $data['mba_country']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS Visa Application'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                    $root_dir = dirname($this->get('kernel')->getRootDir());
                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
                   
                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               
               /**
                * Credit card payment
                * Position: End
                */

                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }

            }else{
                
                //$order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                $order->setStatus(10); // 10= Order placed
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $order->setGrandTotal($total_order_price);
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                
                $em->persist($order);
                $em->flush();
                
                
                
                

                // commit changes
                $em->getConnection()->commit(); 

                
                $em = $this->getDoctrine()->getManager();
                    
                //$host = $request->getScheme().'://'.$request->getHost();
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                
                
                $data = $this->getOrderDetailsByOrderNoAndClientId($order->getOrderNo(), $client_id);
                
                $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsPassport');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.id = 1')
                    ->getQuery();
                $settings = $query->getArrayResult();

                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                // email invoice
                if(!isset($_POST['dont_send_invoice'])){
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Passport Office Pickup or Delivery Order Reciept", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$order->getOrderNo(),
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'settings_passport'=>$settings[0],
                                    'data'=>$data
                                    )
                                )
                        );
                }
                // manual order email to admin
                if($session->get('user_type') == 'admin-user'){ 
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "Passport Office Pickup or Delivery Manual Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'settings_passport'=>$settings[0],
                                        'data'=>$data,
                                        'manual_order'=>1
                                        )
                                    )
                            );
                    }
                }
                
                if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                    // email invoice (copy to admins)
                    if(!isset($_POST['dont_send_invoice'])){
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmail($email_copies, "help@capitallinkservices.com.au", "Passport Office Pickup or Delivery Order Reciept (Copy)", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'settings_passport'=>$settings[0],
                                        'data'=>$data
                                        )
                                    )
                            );
                    }
                }
                
                if($session->get('user_type') == 'admin-user'){
                    /* + create log */
                    $logDetails = "New Passport Office Pickup or Delivery application (order no. ".$order->getOrderNo().") has been manually created and submitted by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }else{
                    /* + create log */
                    $logDetails = "New Passport Office Pickup or Delivery application (order no. ".$order->getOrderNo().") has been created and submitted by <a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$session->get('client_id')."' target='_blank'>" . $session->get('fname') . " " . $session->get('lname') . "</a>";
                    $this->createLog('client', $session->get('client_id'), $session->get('user_type'), $logDetails);
                    /* - create log */
                }
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );

                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
        
            if(count($data) > 0 && $data['order_type'] == 4){
                // if order status done with the review order page
                // if not, send to review order page
                if( $data["status"] == 3 ){


                    return $this->render('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:placeOrder.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'user_details'=>$user_details
                            ));

                }else{

                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$_GET["order"]);
                    }
                }

            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }

}
