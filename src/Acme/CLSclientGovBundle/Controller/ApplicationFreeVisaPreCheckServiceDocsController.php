<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ApplicationFreeVisaPreCheckServiceDocsController extends Controller
{
    public function indexAction()
    {
        
        return $this->render('AcmeCLSclientGovBundle:ApplicationFreeVisaPreCheckServiceDocs:index.html.twig');
    }

}
