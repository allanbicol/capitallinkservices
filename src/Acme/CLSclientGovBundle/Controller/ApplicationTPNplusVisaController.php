<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class ApplicationTPNplusVisaController extends GlobalController
{
    /**
     * 1st Step: Destination
     */
    public function indexAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        
        
        // new order
        if(isset($_POST["hid_submit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            // new order
            if(!isset($_POST["orderNumber"])){
                $model = new Orders();
                $model->setClientId($client_id);
                $model->setOrderType(3);
            // update order
            }else{
                $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            }
            
            // get tpn settings
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
//            $tpn_price = ($user->getCanGetSpecialPrice() == 1) ? ($tpn_settings->getTpn() + ($tpn_settings->getTpn() * ($user->getSpecialPrice()/100))) : $tpn_settings->getTpn();
//            $additional_tpn_price = ($user->getCanGetSpecialPrice() == 1) ? ($tpn_settings->getTpnAdditional() + ($tpn_settings->getTpnAdditional() * ($user->getSpecialPrice()/100))) : $tpn_settings->getTpnAdditional();
            
            $tpn_price = $tpn_settings->getTpn();
            $additional_tpn_price = $tpn_settings->getTpnAdditional();
            $total_tpn = $tpn_price * count($_POST["destination"]);
            
            $model->setDateLastSaved($datetime->format('Y-m-d H:i:s'));
            if(trim($_POST["travellerMname"][0]) != ""){
                $model->setPrimaryTravellerName($_POST["travellerFname"][0] . " " . $_POST["travellerMname"][0] . " " . $_POST["travellerLname"][0]);
            }else{
                $model->setPrimaryTravellerName($_POST["travellerFname"][0] . " " . $_POST["travellerLname"][0]);
            }
            $model->setPrimaryTravellerPassportNo($_POST["travellerPassportNumber"][0]);
            
            $model->setDestination($_POST["destination"][0]);
            $model->setDepartureDate($mod->changeFormatToOriginal($_POST["departureDate"][0]));
            
            $model->setPriDeptContactDepartmentId($_POST["department"]);
            $model->setPriDeptContactFname($_POST["contactFirstname"]);
            $model->setPriDeptContactLname($_POST["contactLastname"]);
            $model->setPriDeptContactEmail($_POST["contactEmail"]);
            $model->setPriDeptContactPhone($_POST["contactPhone"]);
            
            
            
            $model->setTpnQty(count($_POST["destination"]));
            $model->setTpnPrice($tpn_price);
            $model->setTpnAdditionalQty(count($_POST["travellerTitle"]) - 1);
            $model->setTpnAdditionalPrice($additional_tpn_price);
            
            
            
            $total_tpn_additional = $additional_tpn_price * (count($_POST["travellerTitle"]) - 1);
            
            $subtotal = $total_tpn + $total_tpn_additional;
            
            if(!isset($_POST["orderNumber"])){
                $discount_rate = 0;
                $subtotal_discount = 0;
            }else{
                $discount_rate = $model->getDiscountRate();
                $subtotal_discount = $subtotal * ($discount_rate / 100);
            }
            
            // subtotal
            $subtotal = $subtotal - $subtotal_discount;
            
            
            $tpn_gst = $total_tpn * 0.10;
            $tpn_additional_gst = $total_tpn_additional * 0.10;
            
            $gst = $tpn_gst + $tpn_additional_gst;
            $gst_discount = $gst * ($discount_rate / 100);
            // gst
            $gst = $gst - $gst_discount;
            
            $model->setGrandTotal($subtotal + $gst);
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(2); // 2= Review TPN
            }else{
                $model->setStatus(1); // 1= Destination
            }
            $em->persist($model);
            $em->flush();
                
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if(isset($_POST["orderNumber"])){
                // delete destinations
                $query = $em->createQuery('DELETE AcmeCLSclientGovBundle:OrderDestinations c WHERE c.order_no = '.$model->getOrderNo());
                $query->execute(); 
                
                // delete travellers
                $query = $em->createQuery('DELETE AcmeCLSclientGovBundle:OrderTravellers c WHERE c.order_no = '.$model->getOrderNo());
                $query->execute(); 
            }
            
            // save travel details
            // $_POST["destination"] as travels counter
            for($i=0; $i<count($_POST["destination"]); $i++){
                $order_travel = new OrderDestinations();
                $order_travel->setOrderNo($model->getOrderNo());
                $order_travel->setCountryId($_POST["destination"][$i]);
                $order_travel->setDepartureDate($mod->changeFormatToOriginal($_POST["departureDate"][$i]));
                $order_travel->setEntryOption($_POST["entryOption"][$i]);
                $order_travel->setEntryDateCountry($mod->changeFormatToOriginal($_POST["entryDateCounty"][$i]));
                $order_travel->setDepartureDateCountry($mod->changeFormatToOriginal($_POST["departureDateCountry"][$i]));
                $order_travel->setTravelPurpose($_POST["travelPurpose"][$i]);
                $order_travel->setVisaFollowUpDate($mod->changeFormatToOriginal($_POST["followUpDate"][$i]));
                if($i == 0){
                    $order_travel->setSPrimary(1);
                }else{
                    $order_travel->setSPrimary(0);
                }
                
                $em->persist($order_travel);
                $em->flush();
            }
            
            // save traveller details
            // $_POST["travellerTitle"] as travellers counter
            for($i=0; $i<count($_POST["travellerTitle"]); $i++){
                $order_travellers = new OrderTravellers();
                $order_travellers->setOrderNo($model->getOrderNo());
                $order_travellers->setTitle($_POST["travellerTitle"][$i]);
                $order_travellers->setFname($_POST["travellerFname"][$i]);
                $order_travellers->setLname($_POST["travellerLname"][$i]);
                $order_travellers->setMname($_POST["travellerMname"][$i]);
                $order_travellers->setEmail($_POST["travellerEmail"][$i]);
                $order_travellers->setOrganisation($_POST["travellerOrganisation"][$i]);
                $order_travellers->setOccupation($_POST["travellerOccupation"][$i]);
                
                if(strtolower(trim($_POST["travellerOccupation"][$i])) == 'a dependant' || strtolower(trim($_POST["travellerOccupation"][$i])) == 'a spouse'){
                    $order_travellers->setRpinfoFullname($_POST['rpinfoFullname'][$i]);
                    $order_travellers->setRpinfoPositionAtPost($_POST['rpinfoPositionAtPost'][$i]);
                    $order_travellers->setRpinfoNameOfPost($_POST['rpinfoNameOfPost'][$i]);
                    $order_travellers->setRpinfoCity($_POST['rpinfoCity'][$i]);
                }
                
                $order_travellers->setPhone($_POST["travellerPhone"][$i]);
                $order_travellers->setBirthDate($mod->changeFormatToOriginal($_POST["travellerBirthDate"][$i]));
                $order_travellers->setGender($_POST["travellerGender"][$i]);
                $order_travellers->setNearestCapitalCity($_POST["travellerNearestCapitalCity"][$i]);
                $order_travellers->setNationality($_POST["travellerNationality"][$i]);
                $order_travellers->setPassportType($_POST["travellerPassportType"][$i]);
                $order_travellers->setPassportNumber($_POST["travellerPassportNumber"][$i]);
                if($i == 0){
                    $order_travel->setSPrimary(1);
                }else{
                    $order_travel->setSPrimary(0);
                }
                $em->persist($order_travellers);
                $em->flush();
            }
            
            
            $validation_error_count = 0;
            // input validation
            if($_POST["hid_submit"] == 1){
                $blank_inputs = array();
                for($i=0; $i<count($_POST["destination"]); $i++){
                    if(trim($_POST["destination"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'destination'.$i;
                    }

                    if(trim($_POST["departureDate"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'departureDate'.$i;
                    }

                    if(trim($_POST["entryDateCounty"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'entryDateCounty'.$i;
                    }

                    if(trim($_POST["departureDateCountry"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'departureDateCountry'.$i;
                    }

                    if(trim($_POST["followUpDate"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'followUpDate'.$i;
                    }
                    if(trim($_POST["travelPurpose"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travelPurpose'.$i;
                    }
                }
                
                 for($i=0; $i<count($_POST["travellerTitle"]); $i++){
                    if(trim($_POST["travellerTitle"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerTitle'.$i;
                    }
                    
                    if(trim($_POST["travellerFname"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerFname'.$i;
                    }
                    
                    if(trim($_POST["travellerLname"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerLname'.$i;
                    }
                    
                    if(trim($_POST["travellerEmail"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerEmail'.$i;
                    }
                    
                    if(trim($_POST["travellerOrganisation"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerOrganisation'.$i;
                    }
                    
                    if(trim($_POST["travellerOccupation"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerOccupation'.$i;
                    }
                    
                    if(trim($_POST["travellerPhone"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerPhone'.$i;
                    }
                    
                    if(trim($_POST["travellerBirthDate"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerBirthDate'.$i;
                    }
                    
                    if(trim($_POST["travellerPassportType"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerPassportType'.$i;
                    }
                    
                    if(trim($_POST["travellerNationality"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerNationality'.$i;
                    }
                    
                    if(trim($_POST["travellerPassportNumber"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'travellerPassportNumber'.$i;
                    }
                }
                
                if(trim($_POST['department']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'department';
                }
                if(trim($_POST['contactFirstname']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'contactFirstname';
                }
                if(trim($_POST['contactLastname']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'contactLastname';
                }
                if(trim($_POST['contactEmail']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'contactEmail';
                }
                if(trim($_POST['contactPhone']) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'contactPhone';
                }
                
            }
            
            // get details and put them to array
            // for returning them to fields
            $travels = array();
            
            // $_POST["destination"] as travellers counter
            for($i=0; $i<count($_POST["destination"]); $i++){
                $travels[] = array("country_id"=>$_POST["destination"][$i],
                            "departure_date"=> $_POST["departureDate"][$i],
                            "entry_option"=> $_POST["entryOption"][$i],
                            "entry_date_country"=> $_POST["entryDateCounty"][$i],
                            "departure_date_country"=> $_POST["departureDateCountry"][$i],
                            "travel_purpose"=> $_POST["travelPurpose"][$i],
                            "visa_follow_up_date"=> $_POST["followUpDate"][$i]
                    );
            }
            
            // get details and put them to array
            // for returning them to fields
            $travellers = array();
            
            // $_POST["title"] as travellers counter
            for($i=0; $i<count($_POST["travellerTitle"]); $i++){
                $travellers[] = array("title"=>$_POST["travellerTitle"][$i],
                            "fname"=> $_POST["travellerFname"][$i],
                            "mname"=> $_POST["travellerMname"][$i],
                            "lname"=> $_POST["travellerLname"][$i],
                            "email"=> $_POST["travellerEmail"][$i],
                            "organisation"=> $_POST["travellerOrganisation"][$i],
                            "occupation"=> $_POST["travellerOccupation"][$i],
                    
                            "rpinfo_fullname"=> $_POST['rpinfoFullname'][$i],
                            "rpinfo_position_at_post"=> $_POST['rpinfoPositionAtPost'][$i],
                            "rpinfo_name_of_post"=> $_POST['rpinfoNameOfPost'][$i],
                            "rpinfo_city"=> $_POST['rpinfoCity'][$i],
                    
                            "phone"=> $_POST["travellerPhone"][$i],
                            "birth_date"=> $_POST["travellerBirthDate"][$i],
                            "gender"=> $_POST["travellerGender"][$i],
                            "nearest_capital_city"=> $_POST["travellerNearestCapitalCity"][$i],
                            "nationality"=> $_POST["travellerNationality"][$i],
                            "passport_number"=> $_POST["travellerPassportNumber"][$i],
                            "passport_type"=> $_POST["travellerPassportType"][$i]
                    );
            }
            
            // post details
            $post = array(
                'travels' => $travels,
                'travellers' => $travellers,
                'pri_dept_contact_department_id' => $_POST['department'],
                'pri_dept_contact_fname' => $_POST['contactFirstname'],
                'pri_dept_contact_lname' => $_POST['contactLastname'],
                'pri_dept_contact_email' => $_POST['contactEmail'],
                'pri_dept_contact_phone' => $_POST['contactPhone'],
                'blank_inputs'=>(isset($blank_inputs)) ? $blank_inputs : array()
            );
            
            
            if($error_count == 0){
                
                if(trim($_POST['contactEmail'])!="" && $mod->isEmailValid($_POST['contactEmail']) == false){
                    $errors = array();
                    $errors[] = array('message'=>$_POST['contactEmail'].' is not a valid email.');
                    $error_count += 1;
                }
                
            }
            
            if($error_count > 0 || $validation_error_count > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($validation_error_count > 0){
                    $this->get('session')->getFlashBag()->add(
                            'validation-error',
                            'Please check the highlighted required fields and complete the form.'
                        );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors
                    );
                }
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                // return to tpn page
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'departments'=>$this->getDepartments(),
                        'nearest_capital_cities'=> $this->getNearestCapitalCities(),
                        'post'=> $post,
                        'user_details'=> $user_details
                    ));
                
            }else{
                // commit changes
                $em->getConnection()->commit(); 
                
                // if save only
                if($model->getStatus() == 1){
                    
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been saved'
                        );
                    
                    // return to tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa') . "?order=".$model->getOrderNo());
                    }
                }else{
                    
                    // return to visa options page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?client=".$client_id."&order=".$model->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?order=".$model->getOrderNo());
                    }
                }
                
            }
            
            
            
            
            
            
            
            
        // page opening
        }else{
            
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $user_details = '';
            }
            
            // if order number is set
            if(isset($_GET["order"])){
                $client_id = ($session->get('user_type') == 'admin-user') ? $_GET['client'] : $session->get('client_id');
                $post = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
                
                if(count($post) > 0 && $post['order_type'] == 3){
                    
                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:index.html.twig',
                        array(
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles(),
                            'passport_types'=>$this->getPassportTypes(),
                            'departments'=>$this->getDepartments(),
                            'nearest_capital_cities'=> $this->getNearestCapitalCities(),
                            'order_no'=> $_GET["order"],
                            'post' => $post,
                            'user_details'=> $user_details
                        ));
                    
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order"].' not found!' 
                        ));
                }
                
            // if order number is not set
            }else{
            
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'departments'=>$this->getDepartments(),
                        'nearest_capital_cities'=> $this->getNearestCapitalCities(),
                        'user_details'=> $user_details
                    ));
            }
        }
    }
    
    
    /**
     * Step 2: Visa Options
     */
    
    public function optionsAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-visa-options');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hidSubmit"])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            $subtotal = 0;
            $gst = 0;
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $visa_courier = $em->getRepository('AcmeCLSadminBundle:VisaCourierOptions')->findOneBy(array('id'=>$_POST["courier"]));
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
//            $tpn_price = ($user->getCanGetSpecialPrice() == 1) ? ($tpn_settings->getTpn() + ($tpn_settings->getTpn() * ($user->getSpecialPrice()/100))) : $tpn_settings->getTpn();
//            $additional_tpn_price = ($user->getCanGetSpecialPrice() == 1) ? ($tpn_settings->getTpnAdditional() + ($tpn_settings->getTpnAdditional() * ($user->getSpecialPrice()/100))) : $tpn_settings->getTpnAdditional();
            $tpn_price = $tpn_settings->getTpn();
            $additional_tpn_price = $tpn_settings->getTpnAdditional();
            
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderTravellers');
            $query = $cust->createQueryBuilder('p')
                ->where('p.order_no = :order_no')
                ->setParameter('order_no', $_POST["orderNumber"])
                ->getQuery();
            $order_travellers = $query->getArrayResult();
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            $model->setDateLastSaved($datetime->format('Y-m-d H:i:s'));
            
            $total_tpn = $tpn_settings->getTpn() * $model->getTpnQty();
            $total_tpn_additional = $tpn_settings->getTpnAdditional() * $model->getTpnAdditionalQty();
            
            $subtotal += $total_tpn + $total_tpn_additional;
            $gst += ($subtotal * 0.10);
            
            $model->setVisaCourier($_POST["courier"]);
            if($_POST["courier"] != "" && $_POST["courier"] != 0){
                $model->setVisaCourierPrice($visa_courier->getCost());
//                $subtotal += $visa_courier->getCost();
//                $gst += ($visa_courier->getCost() * 0.10);
            }else{
                $model->setVisaCourierPrice(0);
            }
            $model->setVisaCourierPickupDate($mod->changeFormatToOriginal($_POST['packagePickupDate']));
            $model->setVisaCourierPickupReadyByTimeHr($_POST['packageReadyHr']);
            $model->setVisaCourierPickupReadyByTimeMin($_POST['packageReadyMin']);
            $model->setVisaCourierPickupCloseTimeHr($_POST['packageOfficeCloseHr']);
            $model->setVisaCourierPickupCloseTimeMin($_POST['packageOfficeCloseMin']);
            $model->setVisaCourierPickupContactPersonName($_POST['pickupContactPersonName']);
            $model->setVisaCourierPickupContactPersonPhone($_POST['pickupContactPersonPhone']);
            $model->setVisaCourierPickupPackageLocation($_POST['pickupPackageLocation']);
            
            $model->setVisaMddCompany($_POST["docReturnCompany"]);
            $model->setVisaMddAddress($_POST["docReturnAddress"]);
            $model->setVisaMddCity($_POST["docReturnCity"]);
            $model->setVisaMddState($_POST["docReturnState"]);
            $model->setVisaMddPostcode($_POST["docReturnPostCode"]);
            $model->setVisaMddFname($_POST["docReturnFname"]);
            $model->setVisaMddLname($_POST["docReturnLname"]);
            $model->setVisaMddContact($_POST["docReturnContactNo"]);
            $model->setVisaAdditionalComment($_POST["additionalComments"]);
            
            if($_POST["hidSubmit"] == 1){
                $model->setStatus(3); // 3= Review TPN
            }else{
                $model->setStatus(2); // 2= Visa Options
            }
            $em->persist($model);
            $em->flush();
            
            $count_visas = 0;
            while ($visaType = current($_POST['visaType'])) {
                $count_visas += 1;
                $visa_type = $em->getRepository('AcmeCLSadminBundle:VisaTypes')->findOneBy(array('id'=>$visaType));
                
                $od = $em->getRepository('AcmeCLSclientGovBundle:OrderDestinations')->findOneBy(array('id'=>key($_POST["visaType"])));
                $od->setSelectedVisaType($visaType);
                if($user->getCanGetSpecialPrice() == 1){
                    $visa_type_cost = $visa_type->getCost() +  ($visa_type->getCost() * ($user->getSpecialPrice()/100));
                }else{
                    $visa_type_cost = $visa_type->getCost();
                }
                $od->setSelectedVisaTypePrice($visa_type->getCost());
                $subtotal += $visa_type_cost * count($order_travellers);
                $gst += (($visa_type_cost * count($order_travellers)) * 0.10);
                
                $setSelectedVisaTypeRequirements = "";
                if(isset($_POST['addReq'][$visaType])){
                    for($i=0; $i<count($_POST['addReq'][$visaType]); $i++){
                        $setSelectedVisaTypeRequirements .= $_POST['addReq'][$visaType][$i] .";";
                        $reqCost = explode(",", $_POST['addReq'][$visaType][$i]);
                        $reqTotalCost = $reqCost[1] * count($order_travellers);
                        $reqCost = floatval($reqTotalCost);
                        $subtotal += $reqCost;
                        
                        //$gst += ($reqCost * 0.10);
                    }
                }
                $od->setSelectedVisaTypeRequirements($setSelectedVisaTypeRequirements);
                $em->persist($od);
                $em->flush();
                
                next($_POST['visaType']);
            }
            
            if($_POST["hidSubmit"] == 1){
                if($count_visas == 0){
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'error1',
                            'Please Choose type of Visa!'
                        );
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?order=".$model->getOrderNo());
                }
                
                if(trim($_POST["docReturnState"]) != 'ACT' && $visa_courier->getSDhl() == 1){
                    $pickup_error = array();
                    if(trim($_POST['packagePickupDate']) == ''){
                        $pickup_error[] = array('message'=> 'The Pick-up date under Pickup Details section is required.');
                    }
                    if(trim($_POST['packageReadyHr']) == '' || trim($_POST['packageReadyMin']) == ''){
                        $pickup_error[] = array('message'=> 'The Package ready time under Pickup Details section is required.');
                    }
                    if(trim($_POST['packageOfficeCloseHr']) == '' || trim($_POST['packageOfficeCloseMin']) == ''){
                        $pickup_error[] = array('message'=> 'The Office closing time under Pickup Details section is required.');
                    }
                    if(trim($_POST['pickupContactPersonName']) == ''){
                        $pickup_error[] = array('message'=> 'The Contact Person under Pickup Details section is required.');
                    }
                    if(trim($_POST['pickupContactPersonPhone']) == ''){
                        $pickup_error[] = array('message'=> 'The Contact Person\'s phone number under Pickup Details section is required.');
                    }
                    if(trim($_POST['pickupPackageLocation']) == ''){
                        $pickup_error[] = array('message'=> 'The Package Location under Pickup Details section is required.');
                    }
                    
                    if(count($pickup_error) > 0){
                        $this->get('session')->getFlashBag()->add(
                            'multi-error',
                            $pickup_error
                        );
                        
                        // return to review tpn page
                        if($session->get('user_type') == 'admin-user'){
                            return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?client=".$client_id."&order=".$model->getOrderNo());
                        }else{
                            return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?order=".$model->getOrderNo());
                        }
                    }
                    
                }
            }
            
            if($_POST["courier"] != "" && $_POST["courier"] != 0){
                $ctotal = $visa_courier->getCost();
            }else{
                $ctotal = "";
            }
            
//            if($user->getCanGetSpecialPrice() == 1){
//                $ctotal = $ctotal + ($ctotal * ($user->getSpecialPrice()/100));
//                $cgst = ($ctotal * 0.10);
//            }else{
                $ctotal = $ctotal;
                $cgst = ($ctotal * 0.10);
//            }
            $subtotal += $ctotal;
            $gst += $cgst;
            
            $grand_total = $subtotal + $gst;
            
            // set grand total
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            $model->setGrandTotal($grand_total);
            
            
            
            if($model->getDiscountRate() > 0){
                $model->setGrandTotal($model->getGrandTotal() - ($model->getGrandTotal() * ($model->getDiscountRate()/100)));
            }
            $em->persist($model);
            $em->flush();
            
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 2){

                // set flash message
                $this->get('session')->getFlashBag()->add(
                        'success',
                        'Your order has been saved'
                    );

                // return to review tpn + visa page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review tpn page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_tpn') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_tpn') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
            
        // review tpn
        }else{
        
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);

            if(count($data) > 0 && $data['order_type'] == 3){
                // if order status done with the destination page
                // if not, send to destination page
                if( $data["status"] >= 2 ){ 

                    $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
                    $query = $cust->createQueryBuilder('p')
                        ->where('p.id = :id')
                        ->setParameter('id', $client_id)
                        ->getQuery();
                    $user = $query->getArrayResult();
                    
                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:visaOptions.html.twig',
                        array('order_no'=> $_GET["order"],
                            'post' => $data,
                            'visa_courier_options' => $this->getCourierOptions(),
                            'user_details'=> $user_details,
                            'user'=> $user[0],
                            'states'=> $this->getStates()
                            ));

                }else{

                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }
    
    
    
    public function getVisaTypeAdditionalRequirementsAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return new Response("session expired");
        }
        
        $_POST['type_id'] = filter_var($_POST['type_id'], FILTER_SANITIZE_STRING);
        $_POST['destination_id'] = filter_var($_POST['destination_id'], FILTER_SANITIZE_STRING);
        
        $data = $this->getVisaTypeAdditionalRequirementsByVisaTypeId($_POST['type_id']);
        $post = array();
        for($i=0; $i<count($data); $i++){
            $post[] = array(
                'id'=> $data[$i]['id'],
                'visa_id'=> $data[$i]['visa_id'],
                'requirement'=> $data[$i]['requirement'],
                'cost'=> $data[$i]['cost'],
                's_required'=> $data[$i]['s_required'],
                'status'=> $data[$i]['status'],
                'item_order'=> $data[$i]['item_order'],
                's_checked'=> $this->isVisaRequirementSelected($_POST['destination_id'], $data[$i]['visa_id'], $data[$i]['id'])
            );
        }
        return new Response(json_encode($post));
    }
    
    
    
    
    
    
    
    
    
    /**
     * 3rd Step: Review TPN
     */
    public function reviewTPNAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-review');
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(4); // 4= Review Order
            }else{
                $model->setStatus(3); // 3= Review TPN
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 3){

                // set flash message
                $this->get('session')->getFlashBag()->add(
                        'success',
                        'Your order has been saved'
                    );

                // return to review tpn page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_tpn') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_tpn') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
            
        // review tpn
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            
            if(count($data) > 0 && $data['order_type'] == 3){
                // if order status done with the destination page
                // if not, send to destination page
                if( $data["status"] >= 3 ){

                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:reviewTPN.html.twig',
                        array('order_no'=> $_GET["order"],
                            'post' => $data,
                            'user_details'=> $user_details
                            ));

                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_options') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
    }
    
    
    
    
    
    /**
     * 4th Step: Review Order
     */
    public function reviewOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-review-order');
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST['orderNumber'], $client_id);
            if($data["status"]>=10){
                return $this->redirect($this->generateUrl('cls_client_gov_view_order_tpn') . "?order_no=".$_POST['orderNumber']);
            }
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(5); // 4= Place Order
            }else{
                $model->setStatus(4); // 3= Review Order
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 4){

                // return to review tpn page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_place_order') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            if(count($data) > 0 && $data['order_type'] == 3){
                // if order status done with the review tpn page
                // if not, send to review tpn page
                if( $data["status"] >= 4 ){
                    if($data["status"]>=10){
                        return $this->redirect($this->generateUrl('cls_client_gov_view_order_tpn') . "?order_no=".$_GET["order"]);
                    }else{
                        $em = $this->getDoctrine()->getManager();
                        $model = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));

                        return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:reviewOrder.html.twig',
                            array('order_no'=> $_GET["order"],
                                'tpn_settings' => $model,
                                'data'=> $data,
                                'user_details'=>$user_details
                                ));
                    }
                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_tpn') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_tpn') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
        
    }
    
    
    
    public function discountUpdateAction(){ 
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') != '' && isset($_POST["discountCode"]) && isset($_POST["orderNumber"])){
            
            $_POST['orderNumber'] = filter_var($_POST['orderNumber'], FILTER_SANITIZE_STRING);
            $_POST['discountCode'] = filter_var($_POST['discountCode'], FILTER_SANITIZE_STRING);
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('p')
                ->where('p.code = :code')
                ->setParameter('code', $_POST["discountCode"])
                ->getQuery();
            $data = $query->getArrayResult();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"], 'client_id'=>$client_id));
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            //$user->getCanGetSpecialPrice()
            
                
            
            $post = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            
            $items_total_cost = 0;
            $items_total_gst = 0;
            $item_total = 0;

//            $item_total += $post['visa_courier_price'] + ($post['visa_courier_price'] * 0.10);
//            $items_total_cost += $post['visa_courier_price'];
//            $items_total_gst +=  $post['visa_courier_price'] * 0.10;
            
            for($i=0; $i<count($post['travels']); $i++){
                if($user->getCanGetSpecialPrice() == 1){
                    $post['travels'][$i]['selected_visa_type_price'] = $post['travels'][$i]['selected_visa_type_price'] + ($post['travels'][$i]['selected_visa_type_price'] * ($user->getSpecialPrice()/100));
                }
                $item_total += $post['travels'][$i]['selected_visa_type_price'] + ($post['travels'][$i]['selected_visa_type_price'] * 0.10);
                $items_total_cost += $post['travels'][$i]['selected_visa_type_price'] * count($post['travellers']);
                $items_total_gst +=  (($post['travels'][$i]['selected_visa_type_price'] * count($post['travellers'])) * 0.10);

                for($e=0; $e<count($post['travels'][$i]['selected_visa_type_requirements_array']); $e++){
                    $item_total += $post['travels'][$i]['selected_visa_type_requirements_array'][$e]['cost'] + ($post['travels'][$i]['selected_visa_type_requirements_array'][$e]['cost'] * 0.10);
                    $items_total_cost += $post['travels'][$i]['selected_visa_type_requirements_array'][$e]['cost'] * count($post['travellers']);
                    //$items_total_gst += ($post['travels'][$i]['selected_visa_type_requirements_array'][$e]['cost'] * count($post['travellers'])) * 0.10;
                }
            }
                
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            $total_tpn = $tpn_settings->getTpn() * $model->getTpnQty();
            $total_tpn += $total_tpn * 0.10;
            $total_tpn_additional = $tpn_settings->getTpnAdditional() * $model->getTpnAdditionalQty();
            $total_tpn_additional += $total_tpn_additional * 0.10;
            
            $items_total_cost += $total_tpn + $total_tpn_additional;
            
            
            $subtotal = $items_total_cost;
            $gst = $items_total_gst;
            
            
            $ctotal = $post['visa_courier_price'];
//            if($user->getCanGetSpecialPrice() == 1){
//                $ctotal = $ctotal + ($ctotal * ($user->getSpecialPrice()/100));
//                $cgst = ($ctotal * 0.10);
//            }else{
                $ctotal = $ctotal;
                $cgst = ($ctotal * 0.10);
//            }
            $subtotal += $ctotal;
            $gst += $cgst;
            
            
            
            if(count($data) > 0){
                
                $model->setDiscountCode($_POST['discountCode']);
                $model->setDiscountRate($data[0]["rate"]);
                
                $discount_rate = $data[0]["rate"];
                $subtotal_discount = $subtotal * ($discount_rate / 100);
                $gst_discount = $gst * ($discount_rate / 100);
            }else{
                
                $model->setDiscountCode("");
                $model->setDiscountRate(0);
                
                $discount_rate = 0;
                $subtotal_discount = 0;
                $gst_discount = 0;
            }

            
            
            // subtotal
            $subtotal = $subtotal - $subtotal_discount;
            $gst = $gst - $gst_discount;
            
            
            $model->setGrandTotal($subtotal + $gst);
//            if($user->getCanGetSpecialPrice() == 1){
//                $model->setGrandTotal($model->getGrandTotal() + ($model->getGrandTotal() * ($user->getSpecialPrice()/100)));
//            }
            
            $em->persist($model);
            $em->flush();
            
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?order=".$model->getOrderNo());
            }
        }else{
            
            return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?order=".$_POST['orderNumber']);
            
        }
    }
    
    
    
    /**
     * 5th Step: Place Order
     */
    
    public function placeOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        if($session->get('user_type') != 'government' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($order->getStatus() >= 10){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            $em->getConnection()->beginTransaction();

            $model = new Payment();
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($client_id);
            $model->setOrderNo($_POST["orderNumber"]);
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption($_POST["paymentType"]);
            if(isset($_POST["accountNumber"])){
                $model->setAccountNo($_POST["accountNumber"]);
            }
//            $model->setNameOnCard($_POST["nameOnCard"]);
//            $model->setCardNumber($_POST["cardNumber"]);
//            $model->setCardExpiryMonth($_POST["cardExpiryMonth"]);
//            $model->setCardExpiryYear($_POST["cardExpiryYear"]);
//            //$model->setCardType($_POST["cardType"]);
//            $model->setCcvNumber($_POST["ccvNumber"]);
            
            if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                $total_order_price = $order->getGrandTotal();
            }else{
                $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
            }
            
            //$total_order_price = round($total_order_price);
            $total_order_price = number_format($total_order_price, 2,'.','');
            $model->setTotalOrderPrice($total_order_price);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
            if($_POST["paymentType"] == 1){
                $model->setSPaid(1);
            }else{
                $model->setSPaid(2);
            }
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            
            
            
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'department'=>$_POST['department'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'paymentType'=>$_POST["paymentType"],
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            if($error_count == 0){
                if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors = array();
                    $special_error=1;
                }
                
                if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                    $error_count += 1;
                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors[] = array('message'=>'Account number is not correct!');
                    $error_count += 1;
                }
            }
            
            /**
             * DHL Integration
             * Position: Start
             */
            if($error_count == 0){
                $couriercity = strtolower($order->getVisaMddCity());
                $couriercity = trim($couriercity);
                
                if (strpos($couriercity,'canberra') === false) { 
                    
                    $dhl_error = 0;
                    // 7 = CLS to pick-up and deliver my passport
                    $courier_opt = $this->getCourierOptionById($order->getVisaCourier());

                    if(isset($courier_opt[0]['s_dhl']) && $courier_opt[0]['s_dhl'] == 1){
                        if($order->getVisaMddState() != 'ACT'){
                            $travels = $this->getOrderDestinationsByOrderNo($_POST["orderNumber"]);
                            $xmldata = '<?xml version="1.0" encoding="UTF-8"?>
                                <req:BookPURequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                xsi:schemaLocation="http://www.dhl.com book-pickup-global-req.xsd" schemaVersion="1.0">
                                    <Request>
                                        <ServiceHeader>
                                            <MessageTime>'.date('c').'</MessageTime>
                                            <MessageReference>1234567890123456789012345678901</MessageReference>
                                            <SiteID>'.$this->container->getParameter('dhl_xmlpi_siteid').'</SiteID>
                                            <Password>'.$this->container->getParameter('dhl_xmlpi_password').'</Password>
                                        </ServiceHeader>
                                   </Request>
                                   <RegionCode>'.$this->container->getParameter('dhl_xmlpi_regioncode').'</RegionCode>
                                   <Requestor>
                                        <AccountType>D</AccountType>
                                        <AccountNumber>'.$this->container->getParameter('dhl_xmlpi_account_no').'</AccountNumber>
                                        <RequestorContact>
                                            <PersonName>'.$this->container->getParameter('dhl_xmlpi_requestor_contact_person_name').'</PersonName>
                                            <Phone>'.$this->container->getParameter('dhl_xmlpi_requestor_contact_phone').'</Phone>
                                        </RequestorContact>
                                    </Requestor>

                                    <Place>
                                        <LocationType>B</LocationType>
                                        <CompanyName>'.$order->getVisaMddCompany().'</CompanyName>
                                        <Address1>'.$order->getVisaMddAddress().'</Address1>
                                        <Address2></Address2>
                                        <PackageLocation>'.$order->getVisaCourierPickupPackageLocation().'</PackageLocation>
                                        <City>'.$order->getVisaMddCity().'</City>
                                        <StateCode>'.$order->getVisaMddState().'</StateCode>
                                        <DivisionName>'.$order->getVisaMddCity().'</DivisionName>
                                        <CountryCode>'.$this->container->getParameter('dhl_xmlpi_country_code').'</CountryCode>
                                        <PostalCode>'.$order->getVisaMddPostcode().'</PostalCode>
                                    </Place>
                                    <Pickup>
                                        <PickupDate>'.$order->getVisaCourierPickupDate().'</PickupDate>
                                        <ReadyByTime>'.$order->getVisaCourierPickupReadyByTimeHr().':'.$order->getVisaCourierPickupReadyByTimeMin().'</ReadyByTime>
                                        <CloseTime>'.$order->getVisaCourierPickupCloseTimeHr().':'.$order->getVisaCourierPickupCloseTimeMin().'</CloseTime>
                                        <Pieces>'.count($travels).'</Pieces>
                                    </Pickup>
                                    <PickupContact>
                                        <PersonName>'.$order->getVisaCourierPickupContactPersonName().'</PersonName>
                                        <Phone>'.$order->getVisaCourierPickupContactPersonPhone().'</Phone>
                                    </PickupContact>
                                </req:BookPURequest>
                                ';

                            $tuCurl = curl_init();
                            curl_setopt($tuCurl, CURLOPT_URL, $this->container->getParameter('dhl_xmlpi_url'));
                            if (!empty($_SERVER['HTTPS'])) {
                                curl_setopt($tuCurl, CURLOPT_PORT , 443);
                            }
                            curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
                            curl_setopt($tuCurl, CURLOPT_HEADER, 0);
                            curl_setopt($tuCurl, CURLOPT_POST, 1);
                            curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $xmldata);
                            curl_setopt($tuCurl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml","SOAPAction: \"/soap/action/query\"", "Content-length: ".strlen($xmldata)));

                            $tuData = curl_exec($tuCurl);
                            curl_close($tuCurl);
                            $simple = $tuData;
                            $xml = simplexml_load_string($tuData);

                            
                            if($xml->Note->ActionNote!= '' && $xml->Note->ActionNote == 'Success'){
                                $query = $em->createQuery("UPDATE AcmeCLSclientGovBundle:OrderDestinations c SET c.visa_shipped_by = 'DHL', c.visa_com_note_in='".$xml->ConfirmationNumber."'  WHERE c.order_no = ".$_POST["orderNumber"]);
                                $query->execute(); 
                                
                                $xml_response = (string)$tuData;
                                $xml_response = str_replace("'", '"', $xml_response);
                                $cust = $this->getDoctrine()->getEntityManager();
                                $connection = $cust->getConnection();
                                $statement = $connection->prepare("UPDATE tbl_orders SET dhl_pickup_xml_request = '". $xmldata ."', dhl_pickup_xml_response = '". (string)$xml_response ."'  WHERE order_no = ".$_POST["orderNumber"]);
                                $statement->execute();
                                
                            }else{
                                $dhl_error += 1;
                            }

                            if($dhl_error > 0){
                                $errors = array();
                                $special_error=1;

                                //$errors[] = array('message'=>$xml->Response->Status->Condition->ConditionData . '. Check Pick-up Details in Visa Options!');
                                $errors[] = array('message'=>$xml->Response->Status->Condition->ConditionData );
                                $error_count += 1;
                            }
                            
                            /**
                             * DHL Integration
                             * Type: ShipmentValidation
                             * Position: Start
                             */
                            if($dhl_error == 0){
                                
                                
                                $xmldata = '<?xml version="1.0" encoding="UTF-8"?>
                                    <req:ShipmentRequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com ship-val-global-req.xsd" schemaVersion="1.0">
                                            <Request>
                                                <ServiceHeader>
                                                    <MessageTime>'.date('c').'</MessageTime>
                                                    <MessageReference>1234567890123456789012345678901</MessageReference>
                                                    <SiteID>'.$this->container->getParameter('dhl_xmlpi_siteid').'</SiteID>
                                                    <Password>'.$this->container->getParameter('dhl_xmlpi_password').'</Password>
                                                </ServiceHeader>
                                            </Request>
                                            <RegionCode>'.$this->container->getParameter('dhl_xmlpi_regioncode').'</RegionCode>
                                            <LanguageCode>en</LanguageCode>
                                            <PiecesEnabled>Y</PiecesEnabled>
                                            <Billing>
                                                    <ShipperAccountNumber>'.$this->container->getParameter('dhl_xmlpi_account_no').'</ShipperAccountNumber>
                                                    <ShippingPaymentType>S</ShippingPaymentType>
                                                    <DutyPaymentType>R</DutyPaymentType>
                                                    <!--DutyAccountNumber></DutyAccountNumber-->
                                            </Billing>
                                            <Consignee>
                                                    <CompanyName>'.$this->container->getParameter('dhl_xmlpi_company_name').'</CompanyName>
                                                    <AddressLine>'.$this->container->getParameter('dhl_xmlpi_company_address_line1').'</AddressLine>
                                                    <AddressLine>'.$this->container->getParameter('dhl_xmlpi_company_address_line2').'</AddressLine>
                                                    <!--AddressLine></AddressLine--> 
                                                    <City>'.$this->container->getParameter('dhl_xmlpi_city').'</City>
                                                    <PostalCode>'.$this->container->getParameter('dhl_xmlpi_postal_code').'</PostalCode>
                                                    <CountryCode>'.$this->container->getParameter('dhl_xmlpi_country_code').'</CountryCode>
                                                    <CountryName>Australia</CountryName>
                                                    <Contact>
                                                            <PersonName>'.$this->container->getParameter('dhl_xmlpi_requestor_contact_person_name').'</PersonName>
                                                            <PhoneNumber>'.$this->container->getParameter('dhl_xmlpi_requestor_contact_phone').'</PhoneNumber>
                                                            <FaxNumber>'.$this->container->getParameter('dhl_xmlpi_requestor_contact_fax').'</FaxNumber>
                                                            <Email>'.$this->container->getParameter('dhl_xmlpi_requestor_contact_email').'</Email>
                                                    </Contact>
                                            </Consignee>
                                            <Commodity>
                                                    <CommodityCode>'.$this->container->getParameter('dhl_xmlpi_commodity_code').'</CommodityCode>
                                            </Commodity>
                                            <Dutiable> 
                                                    <DeclaredValue>200.00</DeclaredValue>
                                                    <DeclaredCurrency>AUD</DeclaredCurrency>
                                                    <TermsOfTrade>DDU</TermsOfTrade>
                                            </Dutiable>
                                            <ExportDeclaration> 
                                                    <ExportReasonCode>P</ExportReasonCode> 
                                                    <ExportLineItem>
                                                            <LineNumber>1</LineNumber> 
                                                            <Quantity>'.count($travels).'</Quantity> 
                                                            <QuantityUnit>PACKAGE</QuantityUnit> 
                                                            <Description>Documents</Description> 
                                                            <Value>200.00</Value> 
                                                    </ExportLineItem>
                                            </ExportDeclaration>
                                            <Reference>
                                                    <ReferenceID>'. $_POST["orderNumber"] .'</ReferenceID>
                                            </Reference>
                                            <ShipmentDetails>
                                                    <NumberOfPieces>'.count($travels).'</NumberOfPieces>
                                                    <Pieces>
                                                            <Piece>
                                                                <PieceID>1</PieceID>
                                                                <PackageType>CP</PackageType>
                                                                <Weight>0.5</Weight>
                                                                <DimWeight>1.8</DimWeight>
                                                                <PieceContents>DOCUMENTS</PieceContents>
                                                            </Piece>
                                                    </Pieces>
                                                    <Weight>0.5</Weight>
                                                    <WeightUnit>K</WeightUnit>
                                                    <GlobalProductCode>N</GlobalProductCode>
                                                    <LocalProductCode>N</LocalProductCode>
                                                    <Date>'.$order->getVisaCourierPickupDate().'</Date>
                                                    <Contents>DOCUMENTS</Contents>
                                                    <DoorTo>DD</DoorTo>
                                                    <DimensionUnit>C</DimensionUnit>
                                                    <PackageType>CP</PackageType>
                                                    <IsDutiable>N</IsDutiable>
                                                    <CurrencyCode>AUD</CurrencyCode>
                                            </ShipmentDetails>
                                            <Shipper>
                                                    <ShipperID>'.$this->container->getParameter('dhl_xmlpi_account_no').'</ShipperID>
                                                    <CompanyName>'.$order->getVisaMddCompany().'</CompanyName>
                                                    <AddressLine>'.$order->getVisaMddAddress().'</AddressLine>
                                                    <AddressLine></AddressLine>
                                                    <City>'.$order->getVisaMddCity().'</City>
                                                    <PostalCode>'.$order->getVisaMddPostcode().'</PostalCode>
                                                    <CountryCode>'.$this->container->getParameter('dhl_xmlpi_country_code').'</CountryCode>
                                                    <CountryName>Australia</CountryName>
                                                    <Contact>
                                                            <PersonName>'.$order->getVisaCourierPickupContactPersonName().'</PersonName>
                                                            <PhoneNumber>'.$order->getVisaCourierPickupContactPersonPhone().'</PhoneNumber>
                                                    </Contact>
                                            </Shipper>
                                            <LabelImageFormat>PDF</LabelImageFormat>
                                    <Label>
                                            <LabelTemplate>8X4_A4_PDF</LabelTemplate>
                                    </Label>	
                                    </req:ShipmentRequest>
                                    ';

                                $tuCurl = curl_init();
                                curl_setopt($tuCurl, CURLOPT_URL, $this->container->getParameter('dhl_xmlpi_url'));
                                if (!empty($_SERVER['HTTPS'])) {
                                    curl_setopt($tuCurl, CURLOPT_PORT , 443);
                                }
                                curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
                                curl_setopt($tuCurl, CURLOPT_HEADER, 0);
                                curl_setopt($tuCurl, CURLOPT_POST, 1);
                                curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $xmldata);
                                curl_setopt($tuCurl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml","SOAPAction: \"/soap/action/query\"", "Content-length: ".strlen($xmldata)));

                                $tuData = curl_exec($tuCurl);
                                curl_close($tuCurl);
                                $simple = $tuData;
                                $xml = simplexml_load_string($tuData);

                                if($xml->Note->ActionNote!= '' && $xml->Note->ActionNote == 'Success'){
                                    $query = $em->createQuery("UPDATE AcmeCLSclientGovBundle:OrderDestinations c SET c.dhl_airwaybill_number = '".$xml->AirwayBillNumber."'  WHERE c.order_no = ".$_POST["orderNumber"]);
                                    $query->execute(); 
                                    
                                    $xml_response = (string)$tuData;
                                    $xml_response = str_replace("'", '"', $xml_response);
                                    $cust = $this->getDoctrine()->getEntityManager();
                                    $connection = $cust->getConnection();
                                    $statement = $connection->prepare("UPDATE tbl_orders SET dhl_shipment_validate_xml_request = '". $xmldata ."', dhl_shipment_validate_xml_response = '". (string)$xml_response ."'  WHERE order_no = ".$_POST["orderNumber"]);
                                    $statement->execute();
                                    
                                    file_put_contents($root_dir .'/dev/dhl-labels/'.$_POST["orderNumber"].'-dhl-label.pdf', base64_decode($xml->LabelImage->OutputImage));
                                }else{
                                    $dhl_error += 1;
                                }

                                if($dhl_error > 0){
                                    $errors = array();
                                    $special_error=1;

                                    //$errors[] = array('message'=>$xml->Response->Status->Condition->ConditionData . '. Check Pick-up Details in Visa Options!');
                                    $errors[] = array('message'=>$xml->Response->Status->Condition->ConditionData );
                                    $error_count += 1;
                                }
                                
                            }
                            
                            
                        }
                    }
                }
            }
            
            /**
             * DHL Integration
             * Position: End
             */
            
            
            
            if($error_count == 0){
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $data['mba_country']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS Passport Office Pickup or Delivery'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                   $root_dir = dirname($this->get('kernel')->getRootDir());
                   //open connection
                   $ch = curl_init();
                   
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");

                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               
               /**
                * Credit card payment
                * Position: End
                */
            
                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();

                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'departments'=> $this->getDepartments(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'departments'=> $this->getDepartments(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }

            }else{
                
                //$order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                $order->setStatus(10); // 10= Order placed
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $order->setGrandTotal($total_order_price);
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                
                $em->persist($order);
                $em->flush();
                
                
                
                

                // commit changes
                $em->getConnection()->commit(); 

                
                // generate tpn letters
                $this->generateTPNletter($_POST["orderNumber"], $client_id);
                
                
                $em = $this->getDoctrine()->getManager();
                $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
                    
                //$host = $request->getScheme().'://'.$request->getHost();
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                $attachments = $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government');
                
                $attachments[]['file_attachment'] = $root_dir .'/dev/dhl-labels/'.$_POST["orderNumber"].'-dhl-label.pdf';
                // email invoice
                if(!isset($_POST['dont_send_invoice'])){
                    $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "TPN+Visa Order Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings
                                        )
                                    ),
                            $attachments
                            );
                }
                // email submission summary
                if(!isset($_POST['dont_send_invoice'])){
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Submission", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:order_details_email_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'data'=> $data
                                        )
                                    )
                            );
                }
                
                // manual order email to admin
                if($session->get('user_type') == 'admin-user'){
                    // email invoice
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN+Visa Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings,
                                            'manual_order'=>1
                                            )
                                        ),
                                $attachments
                                );
                    }

                    // email submission summary
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Submission", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:order_details_email_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'data'=> $data,
                                            'manual_order'=>1
                                            )
                                        )
                                );
                    }
                }
                
                if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                    // email invoice (copy to admins)
                    if(!isset($_POST['dont_send_invoice'])){
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "TPN+Visa Order Reciept (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'tpn_settings'=>$tpn_settings
                                            )
                                        ),
                                $attachments
                                );
                    }
                }
                
                if($session->get('user_type') == 'admin-user'){
                    /* + create log */
                    $logDetails = "New TPN + Visa application (order no. ".$_POST["orderNumber"].") has been manually created and submitted by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }else{
                    /* + create log */
                    $logDetails = "New TPN + Visa application (order no. ".$_POST["orderNumber"].") has been created and submitted by <a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$session->get('client_id')."' target='_blank'>" . $session->get('fname') . " " . $session->get('lname') . "</a>";
                    $this->createLog('client', $session->get('client_id'), $session->get('user_type'), $logDetails);
                    /* - create log */
                }
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );

                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$client_id);
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
        
            if(count($data) > 0 && $data['order_type'] == 3){
                // if order status done with the review order page
                // if not, send to review order page
                if( $data["status"] == 5 ){


                    return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:placeOrder.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'user_details'=>$user_details
                            ));

                }else{

                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_tpn_visa_review_order') . "?order=".$_GET["order"]);
                    }
                }

            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }

}
