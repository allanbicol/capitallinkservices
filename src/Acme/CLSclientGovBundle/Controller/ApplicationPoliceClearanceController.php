<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSadminBundle\Entity\OrderPoliceClearanceApplicants;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class ApplicationPoliceClearanceController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-police-clearance');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        
        if(isset($_POST["hid_submit"])){
            
            $_POST['policeClearance'] = filter_var($_POST['policeClearance'], FILTER_SANITIZE_STRING);
            
            if(trim($_POST["policeClearance"]) == ""){
                $this->get('session')->getFlashBag()->add(
                        'validation-error',
                        'Please select a police clearance and complete the form.'
                    );
                return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance'));
            }
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            $discount_rate = 0;
            // new order
            if(!isset($_POST["orderNumber"])){
                $order = new Orders();
                $order->setClientId($client_id);
                $order->setOrderType(5);
            // update order
            }else{
                $_POST["orderNumber"] = intval($_POST["orderNumber"]);
                $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                
            }
            
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            $order->setPoliceClearanceId($_POST['policeClearance']);
            if(isset($_POST['fname'][0])){
            $order->setPrimaryTravellerName($_POST['fname'][0] . " " . $_POST['lname'][0]);
            //$order->setDepartureDate($mod->changeFormatToOriginal($_POST['departureDate'][0]));
            }
            $clearance = $this->getPoliceClearanceInfoById($_POST['policeClearance']);
            
            $total_order_price = $clearance[0]['price'];
            
            $total_order_price += ((count($_POST['fname']) - 1) * $clearance[0]['price_additional']);
            
            
            
            
            if($user->getCanGetSpecialPrice() == 1){
                $total_order_price = $total_order_price + ($total_order_price * ($user->getSpecialPrice()/100));
            }
            
            $total_order_price = $total_order_price + ($total_order_price * (10/100));
            
            if($order->getDiscountRate() > 0){
                $total_order_price = ($total_order_price - ($total_order_price * ($order->getDiscountRate()/100)));
            }
            
//            if($_POST["paymentType"] == 1){
//                $total_order_price += ($total_order_price * ($this->getCreditCardProcessingFee()/100));
//            }
            
            //$total_order_price = round($total_order_price);
            
            $order->setGrandTotal($total_order_price);
            if($_POST["hid_submit"] == 0){
                $order->setStatus(1); // 1= Details level
            }else{
                $order->setStatus(2); // 2= Review Order
            }
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            
            if($session->get('user_type') == 'admin-user'){
                $order->setVisaClsTeamMember($session->get('admin_id'));
            }
            
            
            $_POST["drFname"] = filter_var($_POST['drFname'], FILTER_SANITIZE_STRING);
            $_POST["drFname"] = str_replace("&#39;","'", $_POST["drFname"]);
            $_POST["drLname"] = filter_var($_POST['drLname'], FILTER_SANITIZE_STRING);
            $_POST["drLname"] = str_replace("&#39;","'", $_POST["drLname"]);
            $_POST["drContactNo"] = filter_var($_POST['drContactNo'], FILTER_SANITIZE_STRING);
            $_POST["drCompany"] = filter_var($_POST['drCompany'], FILTER_SANITIZE_STRING);
            $_POST["drCompany"] = str_replace("&#39;","'", $_POST["drCompany"]);
            $_POST["drAddress"] = filter_var($_POST['drAddress'], FILTER_SANITIZE_STRING);
            $_POST["drAddress"] = str_replace("&#39;","'", $_POST["drAddress"]);
            $_POST["drCity"] = filter_var($_POST['drCity'], FILTER_SANITIZE_STRING);
            $_POST["drState"] = filter_var($_POST['drState'], FILTER_SANITIZE_STRING);
            $_POST["drPostcode"] = filter_var($_POST['drPostcode'], FILTER_SANITIZE_STRING);
            $_POST["drAdditionalComment"] = filter_var($_POST['drAdditionalComment'], FILTER_SANITIZE_STRING);
            
            $order->setVisaMddCompany($_POST["drCompany"]);
            $order->setVisaMddAddress($_POST["drAddress"]);
            $order->setVisaMddCity($_POST["drCity"]);
            $order->setVisaMddState($_POST["drState"]);
            $order->setVisaMddPostcode($_POST["drPostcode"]);
            $order->setVisaMddFname($_POST["drFname"]);
            $order->setVisaMddLname($_POST["drLname"]);
            $order->setVisaMddContact($_POST["drContactNo"]);
            $order->setVisaAdditionalComment($_POST["drAdditionalComment"]);
            
            $em->persist($order);
            $em->flush();
            
            $applicants = array();
            
            if(isset($_POST["orderNumber"])){
                
                // delete travellers
                $query = $em->createQuery('DELETE AcmeCLSadminBundle:OrderPoliceClearanceApplicants c WHERE c.order_no = '.$order->getOrderNo());
                $query->execute(); 
            }
            
            for($i=0; $i<count($_POST['fname']); $i++){
                
                $_POST['fname'][$i] = filter_var($_POST['fname'][$i], FILTER_SANITIZE_STRING);
                $_POST["fname"] = str_replace("&#39;","'", $_POST["fname"]);
                $_POST['mname'][$i] = filter_var($_POST['mname'][$i], FILTER_SANITIZE_STRING);
                $_POST["mname"] = str_replace("&#39;","'", $_POST["mname"]);
                $_POST['lname'][$i] = filter_var($_POST['lname'][$i], FILTER_SANITIZE_STRING);
                $_POST["lname"] = str_replace("&#39;","'", $_POST["lname"]);
                $_POST['email'][$i] = filter_var($_POST['email'][$i], FILTER_SANITIZE_STRING);
                $_POST['phone'][$i] = filter_var($_POST['phone'][$i], FILTER_SANITIZE_STRING);
                $_POST['mobile'][$i] = filter_var($_POST['mobile'][$i], FILTER_SANITIZE_STRING);
                $_POST['address'][$i] = filter_var($_POST['address'][$i], FILTER_SANITIZE_STRING);
                $_POST["address"] = str_replace("&#39;","'", $_POST["address"]);
                $_POST['city'][$i] = filter_var($_POST['city'][$i], FILTER_SANITIZE_STRING);
                $_POST['state'][$i] = filter_var($_POST['state'][$i], FILTER_SANITIZE_STRING);
                $_POST['postcode'][$i] = filter_var($_POST['postcode'][$i], FILTER_SANITIZE_STRING);
                $_POST['country'][$i] = filter_var($_POST['country'][$i], FILTER_SANITIZE_STRING);
                $_POST['passportNumber'][$i] = filter_var($_POST['passportNumber'][$i], FILTER_SANITIZE_STRING);
                //$_POST['departureDate'][$i] = filter_var($_POST['departureDate'][$i], FILTER_SANITIZE_STRING);
                
                $opca = new OrderPoliceClearanceApplicants();
                $opca->setOrderNo($order->getOrderNo());
                $opca->setFname($_POST['fname'][$i]);
                $opca->setMname($_POST['mname'][$i]);
                $opca->setLname($_POST['lname'][$i]);
                $opca->setEmail($_POST['email'][$i]);
                $opca->setPhone($_POST['phone'][$i]);
                $opca->setMobile($_POST['mobile'][$i]);
                $opca->setAddress($_POST['address'][$i]);
                $opca->setCity($_POST['city'][$i]);
                $opca->setState($_POST['state'][$i]);
                $opca->setPostcode($_POST['postcode'][$i]);
                $opca->setCountryId($_POST['country'][$i]);
                $opca->setPassportNo($_POST['passportNumber'][$i]);
                //$opca->setDepartureDate($mod->changeFormatToOriginal($_POST['departureDate'][$i]));
                $em->persist($opca);
                $em->flush();
                
                $applicants[] = array(
                    'fname' => $_POST['fname'][$i],
                    'mname' => $_POST['mname'][$i],
                    'lname' => $_POST['lname'][$i],
                    'email' => $_POST['email'][$i],
                    'phone' => $_POST['phone'][$i],
                    'mobile' => $_POST['mobile'][$i],
                    'address' => $_POST['address'][$i],
                    'city' => $_POST['city'][$i],
                    'postcode' => $_POST['postcode'][$i],
                    'state' => $_POST['state'][$i],
                    'country_id' => $_POST['country'][$i],
                    //'departure_date' => $_POST['departureDate'][$i],
                    'passport_no' => $_POST['passportNumber'][$i]
                );
            }
            
//            $_POST["drFname"] = filter_var($_POST['drFname'], FILTER_SANITIZE_STRING);
//            $_POST["drLname"] = filter_var($_POST['drLname'], FILTER_SANITIZE_STRING);
//            $_POST["drEmail"] = filter_var($_POST['drEmail'], FILTER_SANITIZE_STRING);
//            $_POST["drContactNo"] = filter_var($_POST['drContactNo'], FILTER_SANITIZE_STRING);
//            $_POST["drAddress"] = filter_var($_POST['drAddress'], FILTER_SANITIZE_STRING);
//            $_POST["drCity"] = filter_var($_POST['drCity'], FILTER_SANITIZE_STRING);
//            $_POST["drPostcode"] = filter_var($_POST['drPostcode'], FILTER_SANITIZE_STRING);
//            $_POST["drAdditionalComment"] = filter_var($_POST['drAdditionalComment'], FILTER_SANITIZE_STRING);
//            $_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
//            $_POST["accountNumber"] = filter_var($_POST['accountNumber'], FILTER_SANITIZE_STRING);
//
//            $model = new Payment();
//            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
//            $model->setClientId($client_id);
//            $model->setOrderNo($order->getOrderNo());
//            $model->setFname($_POST["drFname"]);
//            $model->setLname($_POST["drLname"]);
//            $model->setEmail($_POST["drEmail"]);
//            $model->setPhone($_POST["drContactNo"]);
//            $model->setAddress($_POST["drAddress"]);
//            $model->setCity($_POST["drCity"]);
//            $model->setPostcode($_POST["drPostcode"]);
//            $model->setAdditionalAddressDetails($_POST["drAdditionalComment"]);
//            $model->setPaymentOption($_POST["paymentType"]);
//            $model->setAccountNo($_POST["accountNumber"]);
//            $model->setTotalOrderPrice($total_order_price);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
//
//            $validator = $this->get('validator');
//            $errors = $validator->validate($model);
//            $error_count = count($errors);
            $error_count = 0;
            
            
            $validation_error_count = 0;
            // input validation
            if($_POST["hid_submit"] == 1){
                $blank_inputs = array();
                if(trim($_POST["policeClearance"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'policeClearance';
                }
                
                if(trim($_POST["drAddress"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'drAddress';
                }
                
                if(trim($_POST["drCity"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'drCity';
                }
                if(trim($_POST["drState"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'drState';
                }
                if(trim($_POST["drPostcode"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'drPostcode';
                }
                if(trim($_POST["drFname"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'drFname';
                }
                if(trim($_POST["drLname"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'drLname';
                }
                if(trim($_POST["drContactNo"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'drContactNo';
                }
                
                for($i=0; $i<count($_POST['fname']); $i++){
                
                    if(trim($_POST["fname"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'fname'.$i;
                    }
                    if(trim($_POST["lname"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'lname'.$i;
                    }
                    if(trim($_POST["email"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'email'.$i;
                    }
                    if(trim($_POST["phone"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'phone'.$i;
                    }
                    if(trim($_POST["mobile"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'mobile'.$i;
                    }
                    if(trim($_POST["address"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'address'.$i;
                    }
                    if(trim($_POST["city"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'city'.$i;
                    }
                    if(trim($_POST["postcode"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'postcode'.$i;
                    }
                    if(trim($_POST["state"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'state'.$i;
                    }
                    if(trim($_POST["country"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'country'.$i;
                    }
                    if(trim($_POST["passportNumber"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'passportNumber'.$i;
                    }
                }
                
            }
            $post = array(
                'police_clearance_id'=>$_POST['policeClearance'],
                'police_clearance_applicants' => $applicants,
                'visa_mdd_address'=>$_POST['drAddress'],
                'visa_mdd_city'=>$_POST['drCity'],
                'visa_mdd_postcode'=>$_POST['drPostcode'],
                'visa_mdd_fname'=>$_POST['drFname'],
                'visa_mdd_lname'=>$_POST['drLname'],
                'visa_mdd_contact'=>$_POST['drContactNo'],
                'visa_additional_comment'=>$_POST['drAdditionalComment'],
                'blank_inputs'=>(isset($blank_inputs)) ? $blank_inputs : array()
            );
//            
//            if($error_count == 0){
//                if($user->getCanChargeCostToAccount() != 1 || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
//                    $errors = array();
//                    $special_error=1;
//                }
//                
//                if($user->getCanChargeCostToAccount() != 1){
//                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
//                    $error_count += 1;
//                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
//                    $errors[] = array('message'=>'Account number is not correct!');
//                    $error_count += 1;
//                }
//            }
            
//            
//            if($error_count == 0){
//                
//                /**
//                * Credit card payment
//                * Position: Start
//                */
//               $credit_card_error = 0;
//               if($_POST["paymentType"] == 1){
//                   $totalAmount = $total_order_price * 100;
//                   $fields = array(
//                       'customerFirstName' => urlencode($_POST['drFname']),
//                       'customerLastName' => urlencode($_POST['drLname']),
//                       'customerEmail' => urlencode($_POST['drEmail']),
//                       'customerAddress' => urlencode($_POST['drAddress'] . ' ' . $_POST['drCity'] . ' ' . $_POST['drPostcode']),
//                       'customerPostcode' => urlencode($_POST['drPostcode']),
//                       'customerInvoiceDescription' => urlencode('CLS Police Clearance Application'),
//                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
//                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
//                       'cardNumber' => urlencode($_POST['cardNumber']),
//                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
//                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
//                       'trxnNumber' => urlencode('4230'),
//                       'totalAmount' => urlencode($totalAmount),
//                       'cvn' => urlencode($_POST['ccvNumber'])
//                   );
//
//
//
//                   //url-ify the data for the POST
//                   $fields_string = '';
//                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
//                   rtrim($fields_string, '&');
//
//
//
//                   //open connection
//                   $ch = curl_init();
//                   //set the url, number of POST vars, POST data
//                   curl_setopt($ch,CURLOPT_URL, 'http://cls.vcdev.net.tmp.anchor.net.au/eway/post-pay.php');
//                   curl_setopt($ch,CURLOPT_POST, count($fields));
//                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
//                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
//
//                   //execute post
//                   $credit_card_reponse = curl_exec($ch);
//
//                   $cr_result = json_decode($credit_card_reponse, true);
//
//
//                   if(isset($cr_result['error'])){
//                       $credit_card_error += 1;
//                   }
//
//
//                   //close connection
//                   curl_close($ch);
//               }
//               /**
//                * Credit card payment
//                * Position: End
//                */
//
//                if($credit_card_error > 0){
//                    $errors = array();
//                    $special_error=1;
//                    
//                    $errors[] = array('message'=>$cr_result['error']);
//                    $error_count += 1;
//                }
//            }
            
            if($error_count > 0 || $validation_error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();

                $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.id = :id')
                    ->setParameter('id', $client_id)
                    ->getQuery();
                $user = $query->getArrayResult();
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }

                if(!isset($special_error) || $validation_error_count > 0){
                   if($validation_error_count > 0){
                        $this->get('session')->getFlashBag()->add(
                                'validation-error',
                                'Please check the highlighted required fields and complete the form.'
                            );
                    }else{
                        $this->get('session')->getFlashBag()->add(
                            'error',
                            $errors
                        );
                    }

                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:index.html.twig',
                            array(
                                'clearances'=> $this->getPoliceClearances(),
                                'user'=>$user[0],
                                'credit_card_processing_fee'=> $this->getCreditCardProcessingFee(),
                                'cardtypes'=> $this->getCardTypes(),
                                'countries'=> $this->getCountries(),
                                'name_titles'=> $this->getNameTitles(),
                                'passport_types'=>$this->getPassportTypes(),
                                'departments'=>$this->getDepartments(),
                                'post' => $post,
                                'user_details'=> $user_details
                            ));
                }else{
                    
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:index.html.twig',
                            array(
                                'clearances'=> $this->getPoliceClearances(),
                                'user'=>$user[0],
                                'credit_card_processing_fee'=> $this->getCreditCardProcessingFee(),
                                'cardtypes'=> $this->getCardTypes(),
                                'countries'=> $this->getCountries(),
                                'name_titles'=> $this->getNameTitles(),
                                'passport_types'=>$this->getPassportTypes(),
                                'departments'=>$this->getDepartments(),
                                'errors'=>$errors,
                                'post' => $post,
                                'user_details'=> $user_details
                            ));
                }

            }else{
                
//                $em->persist($model);
//                $em->flush();
                

                

                
                $data = $this->getOrderDetailsByOrderNoAndClientId($order->getOrderNo(), $client_id);
                
                // commit changes
                $em->getConnection()->commit(); 
//                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
//                
//                // email invoice
//                $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Reciept", 
//                    $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
//                            array('domain'=>$mod->siteURL(),
//                                'order_no'=>$order->getOrderNo(),
//                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
//                                'data'=>$data,
//                                'applicants' => $applicants
//                                )
//                            )
//                    );
                
                
                
                
                // if save only
                if($order->getStatus() == 1){
                    
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been saved'
                        );
                    
                    // return to the details page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance') . "?order=".$order->getOrderNo());
                    }
                }else{
                    
                    // return to review tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?order=".$order->getOrderNo());
                    }
                }

                
                
                
//                if($session->get('user_type') == 'admin-user'){
//                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$client_id);
//                }else{
//                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
//                }
                
            }
            
            
            
        }else{
            
            if($session->get('user_type') == 'admin-user'){
                if(isset($_GET['client'])){
                    $_GET['client'] = intval($_GET['client']);
                }else{
                    return $this->redirect($this->generateUrl('acme_cls_admin_home'));
                }
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $user_details = '';
            }
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_GET['client'] : $session->get('client_id');
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id', $client_id)
                ->getQuery();
            $user = $query->getArrayResult();

            // if order number is set
            if(isset($_GET["order"])){
                $_GET["order"] = intval($_GET["order"]);
                
                $post = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
                
                if(count($post) > 0 && $post['order_type'] == 5){
                    
                    


                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:index.html.twig',
                            array(
                                'clearances'=> $this->getPoliceClearances(),
                                'user'=>$user[0],
                                'credit_card_processing_fee'=> $this->getCreditCardProcessingFee(),
                                'cardtypes'=> $this->getCardTypes(),
                                'countries'=> $this->getCountries(),
                                'name_titles'=> $this->getNameTitles(),
                                'passport_types'=>$this->getPassportTypes(),
                                'departments'=>$this->getDepartments(),
                                'user_details'=>$user_details,
                                'order_no'=>$_GET["order"],
                                'post'=> $post
                            ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order"].' not found!' 
                        ));
                }
                
            }else{
                
                return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:index.html.twig',
                            array(
                                'clearances'=> $this->getPoliceClearances(),
                                'user'=>$user[0],
                                'credit_card_processing_fee'=> $this->getCreditCardProcessingFee(),
                                'cardtypes'=> $this->getCardTypes(),
                                'countries'=> $this->getCountries(),
                                'name_titles'=> $this->getNameTitles(),
                                'passport_types'=>$this->getPassportTypes(),
                                'departments'=>$this->getDepartments(),
                                'user_details'=>$user_details
                            ));
                
            }
            
            
        }
    }
    
    
    
    
    /**
     * 2nd Step: Review Order
     */
    public function reviewOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-police-clearance-review-order');
        
        if($session->get('user_type') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST['orderNumber'], $client_id);
            if($data["status"]>=10){
                return $this->redirect($this->generateUrl('cls_client_gov_view_order_police_clearance') . "?order_no=".$_POST['orderNumber']);
            }
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(3); // 3= Place Order
            }else{
                $model->setStatus(2); // 2= Review Order
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 2){

                // return to review visa page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_public_application_visa_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_public_application_visa_review_order') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_place_order') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $em = $this->getDoctrine()->getManager();
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            if(count($data) > 0 && $data['order_type'] == 5){
                // if order status done with the review tpn page
                // if not, send to review tpn page
                if( $data["status"] >= 1 ){
                    
                    if($data["status"]>=10){
                        return $this->redirect($this->generateUrl('cls_client_gov_view_order_police_clearance') . "?order_no=".$_GET["order"]);
                    }else{
                        return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:reviewOrder.html.twig',
                            array('order_no'=> $_GET["order"],
                                'data'=> $data,
                                'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                            'special_price'=> $user->getSpecialPrice()
                                        ),
                                'user_details'=>$user_details
                                ));
                    }
                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
        
    }
    
    
    
    public function discountUpdateAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') != '' && isset($_POST["discountCode"]) && isset($_POST["orderNumber"])){
            
            $_POST['orderNumber'] = filter_var($_POST['orderNumber'], FILTER_SANITIZE_STRING);
            $_POST['discountCode'] = filter_var($_POST['discountCode'], FILTER_SANITIZE_STRING);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('p')
                ->where('p.code = :code')
                ->setParameter('code', $_POST["discountCode"])
                ->getQuery();
            $data = $query->getArrayResult();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"], 'client_id'=>$client_id));
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            //$user->getCanGetSpecialPrice()
            
                
            
            $post = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            
            $items_total_cost = 0;
            $items_total_gst = 0;
            $item_total = 0;

            
            $clearance = $this->getPoliceClearanceInfoById($post['police_clearance_id']);
            
            $item_total += $clearance[0]['price'] + ($clearance[0]['price'] * 0.10);
            $items_total_cost += $clearance[0]['price'];
            if((count($post['police_clearance_applicants']) - 1) > 0){
                $items_total_cost += ((count($post['police_clearance_applicants']) - 1) *  $clearance[0]['price_additional']);
            }
            $items_total_gst +=  $items_total_cost * 0.10;
            $item_total = $items_total_cost + $items_total_gst;
                
            $subtotal = $items_total_cost;
            $gst = $items_total_gst;
            
            if(count($data) > 0){
                
                $model->setDiscountCode($_POST['discountCode']);
                $model->setDiscountRate($data[0]["rate"]);
                
                $discount_rate = $data[0]["rate"];
                $subtotal_discount = $subtotal * ($discount_rate / 100);
                $gst_discount = $gst * ($discount_rate / 100);
            }else{
                
                $model->setDiscountCode("");
                $model->setDiscountRate(0);
                
                $discount_rate = 0;
                $subtotal_discount = 0;
                $gst_discount = 0;
            }

            // subtotal
            $subtotal = $subtotal - $subtotal_discount;
            $gst = $gst - $gst_discount;
            
            
            $model->setGrandTotal($subtotal + $gst);
            if($user->getCanGetSpecialPrice() == 1){
                $model->setGrandTotal($model->getGrandTotal() + ($model->getGrandTotal() * ($user->getSpecialPrice()/100)));
            }
            
            $em->persist($model);
            $em->flush();
            
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
            }else{
                return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?order=".$model->getOrderNo());
            }
        }else{
            
            return $this->redirect($this->generateUrl('cls_client_gov_application_police_clearance_review_order') . "?order=".$_POST['orderNumber']);
            
        }
    }
    
    
    
    
    
    
    
    /**
     * 4th Step: Place Order
     */
    
    public function placeOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($order->getStatus() >= 10){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            $em->getConnection()->beginTransaction();

            
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            $_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
            if(isset($_POST["accountNumber"])){
                $_POST["accountNumber"] = filter_var($_POST['accountNumber'], FILTER_SANITIZE_STRING);
            }
            
            $model = new Payment();
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($client_id);
            $model->setOrderNo($_POST["orderNumber"]);
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption($_POST["paymentType"]);
            if(isset($_POST["accountNumber"])){
                $model->setAccountNo($_POST["accountNumber"]);
            }
//            $model->setNameOnCard($_POST["nameOnCard"]);
//            $model->setCardNumber($_POST["cardNumber"]);
//            $model->setCardExpiryMonth($_POST["cardExpiryMonth"]);
//            $model->setCardExpiryYear($_POST["cardExpiryYear"]);
//            //$model->setCardType($_POST["cardType"]);
//            $model->setCcvNumber($_POST["ccvNumber"]);
            
            if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                $total_order_price = $order->getGrandTotal();
            }else{
                $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
            }
            
            //$total_order_price = round($total_order_price);
            $total_order_price = number_format($total_order_price, 2,'.','');
            $model->setTotalOrderPrice($total_order_price);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
            if($_POST["paymentType"] == 1){
                $model->setSPaid(1);
            }else{
                $model->setSPaid(2);
            }
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'company'=>$_POST['company'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'paymentType'=>$_POST["paymentType"],
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            if($error_count == 0){
                if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors = array();
                    $special_error=1;
                }
                
                if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                    $error_count += 1;
                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors[] = array('message'=>'Account number is not correct!');
                    $error_count += 1;
                }
            }
            
            
            if($error_count == 0){
                
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $data['mba_country']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS Visa Application'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                   $root_dir = dirname($this->get('kernel')->getRootDir());
                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
                   
                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               
               /**
                * Credit card payment
                * Position: End
                */

                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }

            }else{
                
                //$order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                $order->setStatus(10); // 10= Order placed
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $order->setGrandTotal($total_order_price);
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                
                $em->persist($order);
                $em->flush();
                
                
                
                

                // commit changes
                $em->getConnection()->commit(); 

                
                $em = $this->getDoctrine()->getManager();
                    
                //$host = $request->getScheme().'://'.$request->getHost();
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                // email invoice
                if(count($data) > 0){
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Police Clearance Application Reciept", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data
                                    )
                                ),
                                //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                array('0'=>
                                    array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/pclearancefiles/' . $data['police_clearance_id'] . '/'. $data['police_clearance_file'])
                                )
                        );
                    }
                    // manual order email to admin
                    if($session->get('user_type') == 'admin-user'){
                        if(!isset($_POST['dont_send_invoice'])){
                            $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Police Clearance Application Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'manual_order'=>1
                                        )
                                    ),
                                    //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                    array('0'=>
                                        array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/pclearancefiles/' . $data['police_clearance_id'] . '/'. $data['police_clearance_file'])
                                    )
                            );
                        }
                    }
                    if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                        // email invoice (copy to admins)
                        if(!isset($_POST['dont_send_invoice'])){
                            $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                            $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Police Clearance Application Receipt (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    ),
                                    //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                    array('0'=>
                                        array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/pclearancefiles/' . $data['police_clearance_id'] . '/'. $data['police_clearance_file'])
                                    )
                            );
                        }
                    }
                }
                
                // email submission summary
//                $this->sendEmail($session->get("email"), "help@capitallinkservices.com.au", "TPN Submission", 
//                        $this->renderView('AcmeCLSclientGovBundle:ApplicationPublicVisa:order_details_email_template.html.twig',
//                                array('domain'=>$mod->siteURL(),
//                                    'data'=> $data
//                                    )
//                                )
//                        );
                
                if($session->get('user_type') == 'admin-user'){
                    /* + create log */
                    $logDetails = "New Police Clearance application (order no. ".$_POST["orderNumber"].") has been manually created and submitted by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }else{
                    /* + create log */
                    $logDetails = "New Police Clearance application (order no. ".$_POST["orderNumber"].") has been created and submitted by <a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$session->get('client_id')."' target='_blank'>" . $session->get('fname') . " " . $session->get('lname') . "</a>";
                    $this->createLog('client', $session->get('client_id'), $session->get('user_type'), $logDetails);
                    /* - create log */
                }
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );

                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
        
            if(count($data) > 0 && $data['order_type'] == 5){
                // if order status done with the review order page
                // if not, send to review order page
                if( $data["status"] == 3 ){


                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:placeOrder.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'user_details'=>$user_details
                            ));

                }else{

                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_review_order') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_public_application_visa_review_order') . "?order=".$_GET["order"]);
                    }
                }

            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }
    
    
    
    
    public function removeLogAction(){
        $session = $this->getRequest()->getSession();
        $session->set("selected_police_clearance_id", "");
        return new Response("success");
    }
    

}
