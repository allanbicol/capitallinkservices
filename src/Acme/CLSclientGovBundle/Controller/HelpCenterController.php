<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class HelpCenterController extends GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'help-center');
        
        if($session->get('email') == '' || $session->get('user_type') != 'government'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        
        return $this->render('AcmeCLSclientGovBundle:HelpCenter:index.html.twig',
                array('items'=> $this->getVideoTutorials('government'))
                );
    }
    
    public function tutorialAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'help-center');
        
        if($session->get('email') == '' || $session->get('user_type') != 'government'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        if(!isset($_GET['vid'])){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        $_GET['vid'] = intval($_GET['vid']);
        
        return $this->render('AcmeCLSclientGovBundle:HelpCenter:tutorial.html.twig',
                array('item'=> $this->getVideoTutorials('government', $_GET['vid']))
                );
    }
    
}
