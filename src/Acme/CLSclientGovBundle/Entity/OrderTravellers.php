<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderTravellers
 *
 * @ORM\Table(name="tbl_order_travellers")
 * @ORM\Entity
 */
class OrderTravellers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="title", type="integer")
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_no", type="integer")
     */
    private $order_no;

    /**
     * @var string
     *
     * @ORM\Column(name="fname", type="string", length=100)
     */
    private $fname;

    /**
     * @var string
     *
     * @ORM\Column(name="lname", type="string", length=100)
     */
    private $lname;

    /**
     * @var string
     *
     * @ORM\Column(name="mname", type="string", length=100)
     */
    private $mname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="organisation", type="string", length=1000)
     */
    private $organisation;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=10)
     */
    private $gender;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nearest_capital_city", type="string", length=100)
     */
    private $nearest_capital_city;
    /**
     * @var string
     *
     * @ORM\Column(name="occupation", type="string", length=500)
     */
    private $occupation;

    
    /**
     * @var string
     *
     * @ORM\Column(name="rpinfo_fullname", type="string", length=225)
     */
    private $rpinfo_fullname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rpinfo_position_at_post", type="string", length=225)
     */
    private $rpinfo_position_at_post;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="rpinfo_name_of_post", type="string", length=500)
     */
    private $rpinfo_name_of_post;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rpinfo_city", type="string", length=500)
     */
    private $rpinfo_city;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="birth_date", type="string", length=100)
     */
    private $birth_date;

    /**
     * @var integer
     *
     * @ORM\Column(name="nationality", type="integer")
     */
    private $nationality;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_type", type="integer")
     */
    private $passport_type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="passport_number", type="string", length=100)
     */
    private $passport_number;


    /**
     * @var string
     *
     * @ORM\Column(name="s_primary", type="integer")
     */
    private $s_primary;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_citizenship", type="string", length=1000)
     */
    private $rvv_citizenship;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_sex", type="string", length=10)
     */
    private $rvv_sex;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_birth_place", type="string", length=1000)
     */
    private $rvv_birth_place;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_passport_issue_date", type="string", length=50)
     */
    private $rvv_passport_issue_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_passport_exp_date", type="string", length=50)
     */
    private $rvv_passport_exp_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_company", type="string", length=1000)
     */
    private $rvv_company;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_position", type="string", length=1000)
     */
    private $rvv_position;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_city", type="string", length=1000)
     */
    private $rvv_city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_state", type="string", length=1000)
     */
    private $rvv_state;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_postcode", type="string", length=1000)
     */
    private $rvv_postcode;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="rvv_country", type="integer", length=11)
     */
    private $rvv_country;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_company_fax", type="string", length=225)
     */
    private $rvv_company_fax;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_address", type="string", length=1000)
     */
    private $rvv_address;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param integer $title
     * @return OrderTravellers
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return integer 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set order_no
     *
     * @param integer $orderNo
     * @return OrderTravellers
     */
    public function setOrderNo($orderNo)
    {
        $this->order_no = $orderNo;
    
        return $this;
    }

    /**
     * Get order_no
     *
     * @return integer 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return OrderTravellers
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return OrderTravellers
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set mname
     *
     * @param string $mname
     * @return OrderTravellers
     */
    public function setMname($mname)
    {
        $this->mname = $mname;
    
        return $this;
    }

    /**
     * Get mname
     *
     * @return string 
     */
    public function getMname()
    {
        return $this->mname;
    }

    /**
     * Set organisation
     *
     * @param string $organisation
     * @return OrderTravellers
     */
    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;
    
        return $this;
    }

    /**
     * Get organisation
     *
     * @return string 
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return OrderTravellers
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return OrderTravellers
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set nearest_capital_city
     *
     * @param string $nearestCapitalCity
     * @return OrderTravellers
     */
    public function setNearestCapitalCity($nearestCapitalCity)
    {
        $this->nearest_capital_city = $nearestCapitalCity;
    
        return $this;
    }

    /**
     * Get nearest_capital_city
     *
     * @return string 
     */
    public function getNearestCapitalCity()
    {
        return $this->nearest_capital_city;
    }

    /**
     * Set occupation
     *
     * @param string $occupation
     * @return OrderTravellers
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
    
        return $this;
    }

    /**
     * Get occupation
     *
     * @return string 
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Set rpinfo_fullname
     *
     * @param string $rpinfoFullname
     * @return OrderTravellers
     */
    public function setRpinfoFullname($rpinfoFullname)
    {
        $this->rpinfo_fullname = $rpinfoFullname;
    
        return $this;
    }

    /**
     * Get rpinfo_fullname
     *
     * @return string 
     */
    public function getRpinfoFullname()
    {
        return $this->rpinfo_fullname;
    }

    /**
     * Set rpinfo_position_at_post
     *
     * @param string $rpinfoPositionAtPost
     * @return OrderTravellers
     */
    public function setRpinfoPositionAtPost($rpinfoPositionAtPost)
    {
        $this->rpinfo_position_at_post = $rpinfoPositionAtPost;
    
        return $this;
    }

    /**
     * Get rpinfo_position_at_post
     *
     * @return string 
     */
    public function getRpinfoPositionAtPost()
    {
        return $this->rpinfo_position_at_post;
    }

    /**
     * Set rpinfo_name_of_post
     *
     * @param string $rpinfoNameOfPost
     * @return OrderTravellers
     */
    public function setRpinfoNameOfPost($rpinfoNameOfPost)
    {
        $this->rpinfo_name_of_post = $rpinfoNameOfPost;
    
        return $this;
    }

    /**
     * Get rpinfo_name_of_post
     *
     * @return string 
     */
    public function getRpinfoNameOfPost()
    {
        return $this->rpinfo_name_of_post;
    }

    /**
     * Set rpinfo_city
     *
     * @param string $rpinfoCity
     * @return OrderTravellers
     */
    public function setRpinfoCity($rpinfoCity)
    {
        $this->rpinfo_city = $rpinfoCity;
    
        return $this;
    }

    /**
     * Get rpinfo_city
     *
     * @return string 
     */
    public function getRpinfoCity()
    {
        return $this->rpinfo_city;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return OrderTravellers
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set birth_date
     *
     * @param string $birthDate
     * @return OrderTravellers
     */
    public function setBirthDate($birthDate)
    {
        $this->birth_date = $birthDate;
    
        return $this;
    }

    /**
     * Get birth_date
     *
     * @return string 
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * Set nationality
     *
     * @param integer $nationality
     * @return OrderTravellers
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    
        return $this;
    }

    /**
     * Get nationality
     *
     * @return integer 
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set passport_type
     *
     * @param integer $passportType
     * @return OrderTravellers
     */
    public function setPassportType($passportType)
    {
        $this->passport_type = $passportType;
    
        return $this;
    }

    /**
     * Get passport_type
     *
     * @return integer 
     */
    public function getPassportType()
    {
        return $this->passport_type;
    }

    /**
     * Set passport_number
     *
     * @param string $passportNumber
     * @return OrderTravellers
     */
    public function setPassportNumber($passportNumber)
    {
        $this->passport_number = $passportNumber;
    
        return $this;
    }

    /**
     * Get passport_number
     *
     * @return string 
     */
    public function getPassportNumber()
    {
        return $this->passport_number;
    }

    /**
     * Set s_primary
     *
     * @param integer $sPrimary
     * @return OrderTravellers
     */
    public function setSPrimary($sPrimary)
    {
        $this->s_primary = $sPrimary;
    
        return $this;
    }

    /**
     * Get s_primary
     *
     * @return integer 
     */
    public function getSPrimary()
    {
        return $this->s_primary;
    }

    /**
     * Set rvv_citizenship
     *
     * @param string $rvvCitizenship
     * @return OrderTravellers
     */
    public function setRvvCitizenship($rvvCitizenship)
    {
        $this->rvv_citizenship = $rvvCitizenship;
    
        return $this;
    }

    /**
     * Get rvv_citizenship
     *
     * @return string 
     */
    public function getRvvCitizenship()
    {
        return $this->rvv_citizenship;
    }

    /**
     * Set rvv_sex
     *
     * @param string $rvvSex
     * @return OrderTravellers
     */
    public function setRvvSex($rvvSex)
    {
        $this->rvv_sex = $rvvSex;
    
        return $this;
    }

    /**
     * Get rvv_sex
     *
     * @return string 
     */
    public function getRvvSex()
    {
        return $this->rvv_sex;
    }

    /**
     * Set rvv_birth_place
     *
     * @param string $rvvBirthPlace
     * @return OrderTravellers
     */
    public function setRvvBirthPlace($rvvBirthPlace)
    {
        $this->rvv_birth_place = $rvvBirthPlace;
    
        return $this;
    }

    /**
     * Get rvv_birth_place
     *
     * @return string 
     */
    public function getRvvBirthPlace()
    {
        return $this->rvv_birth_place;
    }

    /**
     * Set rvv_passport_issue_date
     *
     * @param string $rvvPassportIssueDate
     * @return OrderTravellers
     */
    public function setRvvPassportIssueDate($rvvPassportIssueDate)
    {
        $this->rvv_passport_issue_date = $rvvPassportIssueDate;
    
        return $this;
    }

    /**
     * Get rvv_passport_issue_date
     *
     * @return string 
     */
    public function getRvvPassportIssueDate()
    {
        return $this->rvv_passport_issue_date;
    }

    /**
     * Set rvv_passport_exp_date
     *
     * @param string $rvvPassportExpDate
     * @return OrderTravellers
     */
    public function setRvvPassportExpDate($rvvPassportExpDate)
    {
        $this->rvv_passport_exp_date = $rvvPassportExpDate;
    
        return $this;
    }

    /**
     * Get rvv_passport_exp_date
     *
     * @return string 
     */
    public function getRvvPassportExpDate()
    {
        return $this->rvv_passport_exp_date;
    }

    /**
     * Set rvv_company
     *
     * @param string $rvvCompany
     * @return OrderTravellers
     */
    public function setRvvCompany($rvvCompany)
    {
        $this->rvv_company = $rvvCompany;
    
        return $this;
    }

    /**
     * Get rvv_company
     *
     * @return string 
     */
    public function getRvvCompany()
    {
        return $this->rvv_company;
    }

    /**
     * Set rvv_position
     *
     * @param string $rvvPosition
     * @return OrderTravellers
     */
    public function setRvvPosition($rvvPosition)
    {
        $this->rvv_position = $rvvPosition;
    
        return $this;
    }

    /**
     * Get rvv_position
     *
     * @return string 
     */
    public function getRvvPosition()
    {
        return $this->rvv_position;
    }

    /**
     * Set rvv_city
     *
     * @param string $rvvCity
     * @return OrderTravellers
     */
    public function setRvvCity($rvvCity)
    {
        $this->rvv_city = $rvvCity;
    
        return $this;
    }

    /**
     * Get rvv_city
     *
     * @return string 
     */
    public function getRvvCity()
    {
        return $this->rvv_city;
    }

    /**
     * Set rvv_state
     *
     * @param string $rvvState
     * @return OrderTravellers
     */
    public function setRvvState($rvvState)
    {
        $this->rvv_state = $rvvState;
    
        return $this;
    }

    /**
     * Get rvv_state
     *
     * @return string 
     */
    public function getRvvState()
    {
        return $this->rvv_state;
    }

    /**
     * Set rvv_postcode
     *
     * @param string $rvvPostcode
     * @return OrderTravellers
     */
    public function setRvvPostcode($rvvPostcode)
    {
        $this->rvv_postcode = $rvvPostcode;
    
        return $this;
    }

    /**
     * Get rvv_postcode
     *
     * @return string 
     */
    public function getRvvPostcode()
    {
        return $this->rvv_postcode;
    }

    /**
     * Set rvv_country
     *
     * @param integer $rvvCountry
     * @return OrderTravellers
     */
    public function setRvvCountry($rvvCountry)
    {
        $this->rvv_country = $rvvCountry;
    
        return $this;
    }

    /**
     * Get rvv_country
     *
     * @return integer 
     */
    public function getRvvCountry()
    {
        return $this->rvv_country;
    }

    /**
     * Set rvv_company_fax
     *
     * @param string $rvvCompanyFax
     * @return OrderTravellers
     */
    public function setRvvCompanyFax($rvvCompanyFax)
    {
        $this->rvv_company_fax = $rvvCompanyFax;
    
        return $this;
    }

    /**
     * Get rvv_company_fax
     *
     * @return string 
     */
    public function getRvvCompanyFax()
    {
        return $this->rvv_company_fax;
    }

    /**
     * Set rvv_address
     *
     * @param string $rvvAddress
     * @return OrderTravellers
     */
    public function setRvvAddress($rvvAddress)
    {
        $this->rvv_address = $rvvAddress;
    
        return $this;
    }

    /**
     * Get rvv_address
     *
     * @return string 
     */
    public function getRvvAddress()
    {
        return $this->rvv_address;
    }
}