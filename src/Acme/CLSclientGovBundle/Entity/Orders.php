<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="tbl_orders")
 * @ORM\Entity
 */
class Orders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="order_no", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $order_no;

    /**
     * @var integer
     *
     * @ORM\Column(name="client_id", type="integer")
     */
    private $client_id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_last_saved", type="string", length=100)
     */
    private $date_last_saved;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_submitted", type="string", length=100)
     */
    private $date_submitted;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="order_type", type="integer")
     */
    private $order_type;

    /**
     * @var string
     *
     * @ORM\Column(name="primary_traveller_name", type="string", length=500)
     */
    private $primary_traveller_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="primary_traveller_passport_no", type="string", length=500)
     */
    private $primary_traveller_passport_no;

    /**
     * @var integer
     *
     * @ORM\Column(name="destination", type="integer")
     */
    private $destination;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_date", type="string", length=100)
     */
    private $departure_date;

    /**
     * @var integer
     *
     * @ORM\Column(name="pri_dept_contact_department_id", type="integer")
     */
    private $pri_dept_contact_department_id;

    /**
     * @var string
     *
     * @ORM\Column(name="pri_dept_contact_fname", type="string", length=100)
     */
    private $pri_dept_contact_fname;

    /**
     * @var string
     *
     * @ORM\Column(name="pri_dept_contact_lname", type="string", length=100)
     */
    private $pri_dept_contact_lname;

    /**
     * @var string
     *
     * @ORM\Column(name="pri_dept_contact_email", type="string", length=100)
     */
    private $pri_dept_contact_email;

    /**
     * @var string
     *
     * @ORM\Column(name="pri_dept_contact_phone", type="string", length=50)
     */
    private $pri_dept_contact_phone;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;
    
    /**
     * @var string
     *
     * @ORM\Column(name="discount_code", type="string", length=225)
     */
    private $discount_code;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="discount_rate", type="float")
     */
    private $discount_rate;
    
    /**
     * @var float
     *
     * @ORM\Column(name="grand_total", type="float")
     */
    
    private $grand_total;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="tpn_qty", type="integer")
     */
    
    private $tpn_qty;
    
    /**
     * @var float
     *
     * @ORM\Column(name="tpn_price", type="float")
     */
    
    private $tpn_price;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="tpn_additional_qty", type="integer")
     */
    
    private $tpn_additional_qty;
    
    /**
     * @var float
     *
     * @ORM\Column(name="tpn_additional_price", type="float")
     */
    
    private $tpn_additional_price;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="visa_courier", type="integer")
     */
    
    private $visa_courier;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="visa_courier_price", type="integer")
     */
    
    private $visa_courier_price;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_courier_pickup_date", type="string", length=50)
     */
    
    private $visa_courier_pickup_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_courier_pickup_ready_by_time_hr", type="string", length=10)
     */
    
    private $visa_courier_pickup_ready_by_time_hr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_courier_pickup_ready_by_time_min", type="string", length=10)
     */
    
    private $visa_courier_pickup_ready_by_time_min;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_courier_pickup_close_time_hr", type="string", length=10)
     */
    
    private $visa_courier_pickup_close_time_hr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_courier_pickup_close_time_min", type="string", length=10)
     */
    
    private $visa_courier_pickup_close_time_min;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_courier_pickup_contact_person_name", type="string", length=200)
     */
    
    private $visa_courier_pickup_contact_person_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_courier_pickup_contact_person_phone", type="string", length=100)
     */
    
    private $visa_courier_pickup_contact_person_phone;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_courier_pickup_package_location", type="string", length=1000)
     */
    
    private $visa_courier_pickup_package_location;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_mdd_company", type="string", length=1000)
     */
    
    private $visa_mdd_company;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_mdd_address", type="string", length=1000)
     */
    
    private $visa_mdd_address;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_mdd_city", type="string", length=500)
     */
    
    private $visa_mdd_city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_mdd_state", type="string", length=500)
     */
    
    private $visa_mdd_state;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_mdd_postcode", type="string", length=100)
     */
    
    private $visa_mdd_postcode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_mdd_fname", type="string", length=100)
     */
    
    private $visa_mdd_fname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_mdd_lname", type="string", length=100)
     */
    
    private $visa_mdd_lname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_mdd_contact", type="string", length=50)
     */
    
    private $visa_mdd_contact;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_additional_comment", type="string")
     */
    
    private $visa_additional_comment;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="visa_cls_team_member", type="integer")
     */
    
    private $visa_cls_team_member;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="visa_is_delivered_to_embassy", type="integer")
     */
    
    private $visa_is_delivered_to_embassy;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="visa_is_delivered_to_embassy_date", type="string")
     */
    
    private $visa_is_delivered_to_embassy_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_next_embassy", type="string", length=500)
     */
    
    private $visa_next_embassy;

    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="passport_office_booking_no", type="string", length=100)
     */
    
    private $passport_office_booking_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="passport_office_booking_time", type="string", length=50)
     */
    
    private $passport_office_booking_time;
    
    /**
     * @var string
     *
     * @ORM\Column(name="passport_office_booking_time_hr", type="string", length=2)
     */
    
    private $passport_office_booking_time_hr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="passport_office_booking_time_min", type="string", length=2)
     */
    
    private $passport_office_booking_time_min;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_completed", type="string", length=20)
     */
    
    private $date_completed;

    /**
     * @var integer
     *
     * @ORM\Column(name="s_doc_sent", type="integer")
     */
    
    private $s_doc_sent;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_doc_sent", type="string", length=20)
     */
    
    private $date_doc_sent;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="police_clearance_id", type="integer")
     */
    
    private $police_clearance_id;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="is_smart_traveller", type="integer")
     */
    
    private $is_smart_traveller;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="doc_delivery_type", type="integer")
     */
    
    private $doc_delivery_type;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="police_clearance_date_cls_received_all_items", type="string", length=20)
     */
    
    private $police_clearance_date_cls_received_all_items;
    
    /**
     * @var string
     *
     * @ORM\Column(name="police_clearance_date_submitted_for_processing", type="string", length=20)
     */
    
    private $police_clearance_date_submitted_for_processing;
    
    /**
     * @var string
     *
     * @ORM\Column(name="police_clearance_date_completed_and_received_at_cls", type="string", length=20)
     */
    
    private $police_clearance_date_completed_and_received_at_cls;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="police_clearance_date_order_on_route_and_closed", type="string", length=20)
     */
    
    private $police_clearance_date_order_on_route_and_closed;
    
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_receiver_name", type="string", length=225)
     */
    private $doc_receiver_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_address", type="string", length=1000)
     */
    private $doc_pickup_address;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_city", type="string", length=1000)
     */
    private $doc_pickup_city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_postcode", type="string", length=100)
     */
    private $doc_pickup_postcode;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_contact_no", type="string", length=100)
     */
    private $doc_pickup_contact_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_contact_area", type="string", length=1000)
     */
    private $doc_pickup_contact_area;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_contact_area_alt1", type="string", length=1000)
     */
    private $doc_pickup_contact_area_alt1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_contact_area_alt2", type="string", length=1000)
     */
    private $doc_pickup_contact_area_alt2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_company", type="string", length=100)
     */
    private $doc_delivery_company;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_company", type="string", length=100)
     */
    private $doc_pickup_company;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_contact_name", type="string", length=100)
     */
    private $doc_pickup_contact_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_company_alt1", type="string", length=100)
     */
    private $doc_delivery_company_alt1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_company_alt2", type="string", length=100)
     */
    private $doc_delivery_company_alt2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_recipient_name", type="string", length=225)
     */
    private $doc_delivery_recipient_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_recipient_name_alt1", type="string", length=225)
     */
    private $doc_delivery_recipient_name_alt1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_recipient_name_alt2", type="string", length=225)
     */
    private $doc_delivery_recipient_name_alt2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_address", type="string", length=1000)
     */
    private $doc_delivery_address;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_address_alt1", type="string", length=1000)
     */
    private $doc_delivery_address_alt1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_address_alt2", type="string", length=1000)
     */
    private $doc_delivery_address_alt2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_city", type="string", length=1000)
     */
    private $doc_delivery_city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_city_alt1", type="string", length=1000)
     */
    private $doc_delivery_city_alt1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_city_alt2", type="string", length=1000)
     */
    private $doc_delivery_city_alt2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_postcode", type="string", length=100)
     */
    private $doc_delivery_postcode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_postcode_alt1", type="string", length=100)
     */
    private $doc_delivery_postcode_alt1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_postcode_alt2", type="string", length=100)
     */
    private $doc_delivery_postcode_alt2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_contact_no", type="string", length=100)
     */
    private $doc_delivery_contact_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_contact_no_alt1", type="string", length=100)
     */
    private $doc_delivery_contact_no_alt1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_contact_no_alt2", type="string", length=100)
     */
    private $doc_delivery_contact_no_alt2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_security_no", type="string", length=100)
     */
    private $doc_delivery_security_no;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="doc_package_total_pieces", type="integer")
     */
    private $doc_package_total_pieces;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_pickup_date", type="string", length=20)
     */
    private $doc_package_pickup_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_ready_hr", type="string", length=2)
     */
    private $doc_package_ready_hr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_ready_min", type="string", length=2)
     */
    private $doc_package_ready_min;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_ready_ampm", type="string", length=2)
     */
    private $doc_package_ready_ampm;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_office_close_hr", type="string", length=2)
     */
    private $doc_package_office_close_hr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_office_close_min", type="string", length=2)
     */
    private $doc_package_office_close_min;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_office_close_ampm", type="string", length=2)
     */
    private $doc_package_office_close_ampm;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_email", type="string", length=100)
     */
    private $doc_pickup_email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_email", type="string", length=100)
     */
    private $doc_delivery_email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_primary_receipient_contact_name", type="string", length=220)
     */
    private $doc_delivery_primary_receipient_contact_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_primary_receipient_contact_area", type="string", length=220)
     */
    private $doc_delivery_primary_receipient_contact_area;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_primary_receipient_contact_no", type="string", length=100)
     */
    private $doc_delivery_primary_receipient_contact_no;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_primary_receipient_email", type="string", length=100)
     */
    private $doc_delivery_primary_receipient_email;
    
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="russian_visa_voucher_col_no", type="integer", length=11)
     */
    private $russian_visa_voucher_col_no;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="russian_visa_voucher_id", type="integer", length=11)
     */
    private $russian_visa_voucher_id;
    
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="russian_visa_voucher_col_cost", type="float", length=11)
     */
    private $russian_visa_voucher_col_cost;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_first_entry_date", type="string", length=50)
     */
    private $rvv_first_entry_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_first_departure_date", type="string", length=50)
     */
    private $rvv_first_departure_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_second_entry_date", type="string", length=50)
     */
    private $rvv_second_entry_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_second_departure_date", type="string", length=50)
     */
    private $rvv_second_departure_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_list_of_cities", type="string", length=50)
     */
    private $rvv_list_of_cities;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_list_of_hotels", type="string", length=50)
     */
    private $rvv_list_of_hotels;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_visa_applied_at", type="string", length=50)
     */
    private $rvv_visa_applied_at;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_file", type="string")
     */
    private $rvv_file;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rvv_comments", type="string")
     */
    private $rvv_comments;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_company", type="string", length=1000)
     */
    private $dl_company;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_address", type="string", length=1000)
     */
    private $dl_address;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_contact_name", type="string", length=220)
     */
    private $dl_contact_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_mobile", type="string", length=220)
     */
    private $dl_mobile;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_email", type="string", length=220)
     */
    private $dl_email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_date_doc_returned", type="string", length=20)
     */
    private $dl_date_doc_returned;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dl_embassy", type="integer")
     */
    private $dl_embassy;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_ref_no", type="string", length=220)
     */
    private $dl_ref_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_com_invoice_no", type="string", length=220)
     */
    private $dl_com_invoice_no;

    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_payment_type", type="string", length=100)
     */
    private $dl_payment_type;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dl_nationality", type="integer")
     */
    private $dl_nationality;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_visa_shipped_by", type="string", length=1000)
     */
    private $dl_visa_shipped_by;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_visa_com_note_no", type="string", length=1000)
     */
    private $dl_visa_com_note_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_visa_com_note_in", type="string", length=1000)
     */
    private $dl_visa_com_note_in;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_visa_invoice_no", type="string", length=1000)
     */
    private $dl_visa_invoice_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_city", type="string", length=220)
     */
    private $dl_city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_state", type="string", length=220)
     */
    private $dl_state;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dl_postcode", type="string", length=220)
     */
    private $dl_postcode;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_bulk_order", type="integer")
     */
    private $s_bulk_order;
    
    /**
     * @var string
     *
     * @ORM\Column(name="signature", type="string")
     */
    private $signature;
    
    /**
     * @var string
     *
     * @ORM\Column(name="sig_name", type="string")
     */
    private $sig_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dhl_pickup_xml_request", type="string")
     */
    private $dhl_pickup_xml_request;

    /**
     * @var string
     *
     * @ORM\Column(name="dhl_pickup_xml_response", type="string")
     */
    private $dhl_pickup_xml_response;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dhl_shipment_validate_xml_request", type="string")
     */
    private $dhl_shipment_validate_xml_request;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dhl_shipment_validate_xml_response", type="string")
     */
    private $dhl_shipment_validate_xml_response;

    /**
     * @var string
     *
     * @ORM\Column(name="sender_name", type="string")
     */
    private $sender_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="sender_signature", type="string")
     */
    private $sender_signature;
    
    /**
     * @var string
     *
     * @ORM\Column(name="sender_signed_datetime", type="string")
     */
    private $sender_signed_datetime;

    /**
     * Get order_no
     *
     * @return integer 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set client_id
     *
     * @param integer $clientId
     * @return Orders
     */
    public function setClientId($clientId)
    {
        $this->client_id = $clientId;
    
        return $this;
    }

    /**
     * Get client_id
     *
     * @return integer 
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * Set date_last_saved
     *
     * @param string $dateLastSaved
     * @return Orders
     */
    public function setDateLastSaved($dateLastSaved)
    {
        $this->date_last_saved = $dateLastSaved;
    
        return $this;
    }

    /**
     * Get date_last_saved
     *
     * @return string 
     */
    public function getDateLastSaved()
    {
        return $this->date_last_saved;
    }

    /**
     * Set date_submitted
     *
     * @param string $dateSubmitted
     * @return Orders
     */
    public function setDateSubmitted($dateSubmitted)
    {
        $this->date_submitted = $dateSubmitted;
    
        return $this;
    }

    /**
     * Get date_submitted
     *
     * @return string 
     */
    public function getDateSubmitted()
    {
        return $this->date_submitted;
    }

    /**
     * Set order_type
     *
     * @param integer $orderType
     * @return Orders
     */
    public function setOrderType($orderType)
    {
        $this->order_type = $orderType;
    
        return $this;
    }

    /**
     * Get order_type
     *
     * @return integer 
     */
    public function getOrderType()
    {
        return $this->order_type;
    }

    /**
     * Set primary_traveller_name
     *
     * @param string $primaryTravellerName
     * @return Orders
     */
    public function setPrimaryTravellerName($primaryTravellerName)
    {
        $this->primary_traveller_name = $primaryTravellerName;
    
        return $this;
    }

    /**
     * Get primary_traveller_name
     *
     * @return string 
     */
    public function getPrimaryTravellerName()
    {
        return $this->primary_traveller_name;
    }

    /**
     * Set primary_traveller_passport_no
     *
     * @param string $primaryTravellerPassportNo
     * @return Orders
     */
    public function setPrimaryTravellerPassportNo($primaryTravellerPassportNo)
    {
        $this->primary_traveller_passport_no = $primaryTravellerPassportNo;
    
        return $this;
    }

    /**
     * Get primary_traveller_passport_no
     *
     * @return string 
     */
    public function getPrimaryTravellerPassportNo()
    {
        return $this->primary_traveller_passport_no;
    }

    /**
     * Set destination
     *
     * @param integer $destination
     * @return Orders
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    
        return $this;
    }

    /**
     * Get destination
     *
     * @return integer 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set departure_date
     *
     * @param string $departureDate
     * @return Orders
     */
    public function setDepartureDate($departureDate)
    {
        $this->departure_date = $departureDate;
    
        return $this;
    }

    /**
     * Get departure_date
     *
     * @return string 
     */
    public function getDepartureDate()
    {
        return $this->departure_date;
    }

    /**
     * Set pri_dept_contact_department_id
     *
     * @param integer $priDeptContactDepartmentId
     * @return Orders
     */
    public function setPriDeptContactDepartmentId($priDeptContactDepartmentId)
    {
        $this->pri_dept_contact_department_id = $priDeptContactDepartmentId;
    
        return $this;
    }

    /**
     * Get pri_dept_contact_department_id
     *
     * @return integer 
     */
    public function getPriDeptContactDepartmentId()
    {
        return $this->pri_dept_contact_department_id;
    }

    /**
     * Set pri_dept_contact_fname
     *
     * @param string $priDeptContactFname
     * @return Orders
     */
    public function setPriDeptContactFname($priDeptContactFname)
    {
        $this->pri_dept_contact_fname = $priDeptContactFname;
    
        return $this;
    }

    /**
     * Get pri_dept_contact_fname
     *
     * @return string 
     */
    public function getPriDeptContactFname()
    {
        return $this->pri_dept_contact_fname;
    }

    /**
     * Set pri_dept_contact_lname
     *
     * @param string $priDeptContactLname
     * @return Orders
     */
    public function setPriDeptContactLname($priDeptContactLname)
    {
        $this->pri_dept_contact_lname = $priDeptContactLname;
    
        return $this;
    }

    /**
     * Get pri_dept_contact_lname
     *
     * @return string 
     */
    public function getPriDeptContactLname()
    {
        return $this->pri_dept_contact_lname;
    }

    /**
     * Set pri_dept_contact_email
     *
     * @param string $priDeptContactEmail
     * @return Orders
     */
    public function setPriDeptContactEmail($priDeptContactEmail)
    {
        $this->pri_dept_contact_email = $priDeptContactEmail;
    
        return $this;
    }

    /**
     * Get pri_dept_contact_email
     *
     * @return string 
     */
    public function getPriDeptContactEmail()
    {
        return $this->pri_dept_contact_email;
    }

    /**
     * Set pri_dept_contact_phone
     *
     * @param string $priDeptContactPhone
     * @return Orders
     */
    public function setPriDeptContactPhone($priDeptContactPhone)
    {
        $this->pri_dept_contact_phone = $priDeptContactPhone;
    
        return $this;
    }

    /**
     * Get pri_dept_contact_phone
     *
     * @return string 
     */
    public function getPriDeptContactPhone()
    {
        return $this->pri_dept_contact_phone;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Orders
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set discount_code
     *
     * @param string $discountCode
     * @return Orders
     */
    public function setDiscountCode($discountCode)
    {
        $this->discount_code = $discountCode;
    
        return $this;
    }

    /**
     * Get discount_code
     *
     * @return string 
     */
    public function getDiscountCode()
    {
        return $this->discount_code;
    }

    /**
     * Set discount_rate
     *
     * @param float $discountRate
     * @return Orders
     */
    public function setDiscountRate($discountRate)
    {
        $this->discount_rate = $discountRate;
    
        return $this;
    }

    /**
     * Get discount_rate
     *
     * @return float 
     */
    public function getDiscountRate()
    {
        return $this->discount_rate;
    }

    /**
     * Set grand_total
     *
     * @param float $grandTotal
     * @return Orders
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grand_total = $grandTotal;
    
        return $this;
    }

    /**
     * Get grand_total
     *
     * @return float 
     */
    public function getGrandTotal()
    {
        return $this->grand_total;
    }

    /**
     * Set tpn_qty
     *
     * @param integer $tpnQty
     * @return Orders
     */
    public function setTpnQty($tpnQty)
    {
        $this->tpn_qty = $tpnQty;
    
        return $this;
    }

    /**
     * Get tpn_qty
     *
     * @return integer 
     */
    public function getTpnQty()
    {
        return $this->tpn_qty;
    }

    /**
     * Set tpn_price
     *
     * @param float $tpnPrice
     * @return Orders
     */
    public function setTpnPrice($tpnPrice)
    {
        $this->tpn_price = $tpnPrice;
    
        return $this;
    }

    /**
     * Get tpn_price
     *
     * @return float 
     */
    public function getTpnPrice()
    {
        return $this->tpn_price;
    }

    /**
     * Set tpn_additional_qty
     *
     * @param integer $tpnAdditionalQty
     * @return Orders
     */
    public function setTpnAdditionalQty($tpnAdditionalQty)
    {
        $this->tpn_additional_qty = $tpnAdditionalQty;
    
        return $this;
    }

    /**
     * Get tpn_additional_qty
     *
     * @return integer 
     */
    public function getTpnAdditionalQty()
    {
        return $this->tpn_additional_qty;
    }

    /**
     * Set tpn_additional_price
     *
     * @param float $tpnAdditionalPrice
     * @return Orders
     */
    public function setTpnAdditionalPrice($tpnAdditionalPrice)
    {
        $this->tpn_additional_price = $tpnAdditionalPrice;
    
        return $this;
    }

    /**
     * Get tpn_additional_price
     *
     * @return float 
     */
    public function getTpnAdditionalPrice()
    {
        return $this->tpn_additional_price;
    }

    /**
     * Set visa_courier
     *
     * @param integer $visaCourier
     * @return Orders
     */
    public function setVisaCourier($visaCourier)
    {
        $this->visa_courier = $visaCourier;
    
        return $this;
    }

    /**
     * Get visa_courier
     *
     * @return integer 
     */
    public function getVisaCourier()
    {
        return $this->visa_courier;
    }

    /**
     * Set visa_courier_price
     *
     * @param integer $visaCourierPrice
     * @return Orders
     */
    public function setVisaCourierPrice($visaCourierPrice)
    {
        $this->visa_courier_price = $visaCourierPrice;
    
        return $this;
    }

    /**
     * Get visa_courier_price
     *
     * @return integer 
     */
    public function getVisaCourierPrice()
    {
        return $this->visa_courier_price;
    }

    /**
     * Set visa_courier_pickup_date
     *
     * @param string $visaCourierPickupDate
     * @return Orders
     */
    public function setVisaCourierPickupDate($visaCourierPickupDate)
    {
        $this->visa_courier_pickup_date = $visaCourierPickupDate;
    
        return $this;
    }

    /**
     * Get visa_courier_pickup_date
     *
     * @return string 
     */
    public function getVisaCourierPickupDate()
    {
        return $this->visa_courier_pickup_date;
    }

    /**
     * Set visa_courier_pickup_ready_by_time_hr
     *
     * @param string $visaCourierPickupReadyByTimeHr
     * @return Orders
     */
    public function setVisaCourierPickupReadyByTimeHr($visaCourierPickupReadyByTimeHr)
    {
        $this->visa_courier_pickup_ready_by_time_hr = $visaCourierPickupReadyByTimeHr;
    
        return $this;
    }

    /**
     * Get visa_courier_pickup_ready_by_time_hr
     *
     * @return string 
     */
    public function getVisaCourierPickupReadyByTimeHr()
    {
        return $this->visa_courier_pickup_ready_by_time_hr;
    }

    /**
     * Set visa_courier_pickup_ready_by_time_min
     *
     * @param string $visaCourierPickupReadyByTimeMin
     * @return Orders
     */
    public function setVisaCourierPickupReadyByTimeMin($visaCourierPickupReadyByTimeMin)
    {
        $this->visa_courier_pickup_ready_by_time_min = $visaCourierPickupReadyByTimeMin;
    
        return $this;
    }

    /**
     * Get visa_courier_pickup_ready_by_time_min
     *
     * @return string 
     */
    public function getVisaCourierPickupReadyByTimeMin()
    {
        return $this->visa_courier_pickup_ready_by_time_min;
    }

    /**
     * Set visa_courier_pickup_close_time_hr
     *
     * @param string $visaCourierPickupCloseTimeHr
     * @return Orders
     */
    public function setVisaCourierPickupCloseTimeHr($visaCourierPickupCloseTimeHr)
    {
        $this->visa_courier_pickup_close_time_hr = $visaCourierPickupCloseTimeHr;
    
        return $this;
    }

    /**
     * Get visa_courier_pickup_close_time_hr
     *
     * @return string 
     */
    public function getVisaCourierPickupCloseTimeHr()
    {
        return $this->visa_courier_pickup_close_time_hr;
    }

    /**
     * Set visa_courier_pickup_close_time_min
     *
     * @param string $visaCourierPickupCloseTimeMin
     * @return Orders
     */
    public function setVisaCourierPickupCloseTimeMin($visaCourierPickupCloseTimeMin)
    {
        $this->visa_courier_pickup_close_time_min = $visaCourierPickupCloseTimeMin;
    
        return $this;
    }

    /**
     * Get visa_courier_pickup_close_time_min
     *
     * @return string 
     */
    public function getVisaCourierPickupCloseTimeMin()
    {
        return $this->visa_courier_pickup_close_time_min;
    }

    /**
     * Set visa_courier_pickup_contact_person_name
     *
     * @param string $visaCourierPickupContactPersonName
     * @return Orders
     */
    public function setVisaCourierPickupContactPersonName($visaCourierPickupContactPersonName)
    {
        $this->visa_courier_pickup_contact_person_name = $visaCourierPickupContactPersonName;
    
        return $this;
    }

    /**
     * Get visa_courier_pickup_contact_person_name
     *
     * @return string 
     */
    public function getVisaCourierPickupContactPersonName()
    {
        return $this->visa_courier_pickup_contact_person_name;
    }

    /**
     * Set visa_courier_pickup_contact_person_phone
     *
     * @param string $visaCourierPickupContactPersonPhone
     * @return Orders
     */
    public function setVisaCourierPickupContactPersonPhone($visaCourierPickupContactPersonPhone)
    {
        $this->visa_courier_pickup_contact_person_phone = $visaCourierPickupContactPersonPhone;
    
        return $this;
    }

    /**
     * Get visa_courier_pickup_contact_person_phone
     *
     * @return string 
     */
    public function getVisaCourierPickupContactPersonPhone()
    {
        return $this->visa_courier_pickup_contact_person_phone;
    }

    /**
     * Set visa_courier_pickup_package_location
     *
     * @param string $visaCourierPickupPackageLocation
     * @return Orders
     */
    public function setVisaCourierPickupPackageLocation($visaCourierPickupPackageLocation)
    {
        $this->visa_courier_pickup_package_location = $visaCourierPickupPackageLocation;
    
        return $this;
    }

    /**
     * Get visa_courier_pickup_package_location
     *
     * @return string 
     */
    public function getVisaCourierPickupPackageLocation()
    {
        return $this->visa_courier_pickup_package_location;
    }

    /**
     * Set visa_mdd_company
     *
     * @param string $visaMddCompany
     * @return Orders
     */
    public function setVisaMddCompany($visaMddCompany)
    {
        $this->visa_mdd_company = $visaMddCompany;
    
        return $this;
    }

    /**
     * Get visa_mdd_company
     *
     * @return string 
     */
    public function getVisaMddCompany()
    {
        return $this->visa_mdd_company;
    }

    /**
     * Set visa_mdd_address
     *
     * @param string $visaMddAddress
     * @return Orders
     */
    public function setVisaMddAddress($visaMddAddress)
    {
        $this->visa_mdd_address = $visaMddAddress;
    
        return $this;
    }

    /**
     * Get visa_mdd_address
     *
     * @return string 
     */
    public function getVisaMddAddress()
    {
        return $this->visa_mdd_address;
    }

    /**
     * Set visa_mdd_city
     *
     * @param string $visaMddCity
     * @return Orders
     */
    public function setVisaMddCity($visaMddCity)
    {
        $this->visa_mdd_city = $visaMddCity;
    
        return $this;
    }

    /**
     * Get visa_mdd_city
     *
     * @return string 
     */
    public function getVisaMddCity()
    {
        return $this->visa_mdd_city;
    }

    /**
     * Set visa_mdd_state
     *
     * @param string $visaMddState
     * @return Orders
     */
    public function setVisaMddState($visaMddState)
    {
        $this->visa_mdd_state = $visaMddState;
    
        return $this;
    }

    /**
     * Get visa_mdd_state
     *
     * @return string 
     */
    public function getVisaMddState()
    {
        return $this->visa_mdd_state;
    }

    /**
     * Set visa_mdd_postcode
     *
     * @param string $visaMddPostcode
     * @return Orders
     */
    public function setVisaMddPostcode($visaMddPostcode)
    {
        $this->visa_mdd_postcode = $visaMddPostcode;
    
        return $this;
    }

    /**
     * Get visa_mdd_postcode
     *
     * @return string 
     */
    public function getVisaMddPostcode()
    {
        return $this->visa_mdd_postcode;
    }

    /**
     * Set visa_mdd_fname
     *
     * @param string $visaMddFname
     * @return Orders
     */
    public function setVisaMddFname($visaMddFname)
    {
        $this->visa_mdd_fname = $visaMddFname;
    
        return $this;
    }

    /**
     * Get visa_mdd_fname
     *
     * @return string 
     */
    public function getVisaMddFname()
    {
        return $this->visa_mdd_fname;
    }

    /**
     * Set visa_mdd_lname
     *
     * @param string $visaMddLname
     * @return Orders
     */
    public function setVisaMddLname($visaMddLname)
    {
        $this->visa_mdd_lname = $visaMddLname;
    
        return $this;
    }

    /**
     * Get visa_mdd_lname
     *
     * @return string 
     */
    public function getVisaMddLname()
    {
        return $this->visa_mdd_lname;
    }

    /**
     * Set visa_mdd_contact
     *
     * @param string $visaMddContact
     * @return Orders
     */
    public function setVisaMddContact($visaMddContact)
    {
        $this->visa_mdd_contact = $visaMddContact;
    
        return $this;
    }

    /**
     * Get visa_mdd_contact
     *
     * @return string 
     */
    public function getVisaMddContact()
    {
        return $this->visa_mdd_contact;
    }

    /**
     * Set visa_additional_comment
     *
     * @param string $visaAdditionalComment
     * @return Orders
     */
    public function setVisaAdditionalComment($visaAdditionalComment)
    {
        $this->visa_additional_comment = $visaAdditionalComment;
    
        return $this;
    }

    /**
     * Get visa_additional_comment
     *
     * @return string 
     */
    public function getVisaAdditionalComment()
    {
        return $this->visa_additional_comment;
    }

    /**
     * Set visa_cls_team_member
     *
     * @param integer $visaClsTeamMember
     * @return Orders
     */
    public function setVisaClsTeamMember($visaClsTeamMember)
    {
        $this->visa_cls_team_member = $visaClsTeamMember;
    
        return $this;
    }

    /**
     * Get visa_cls_team_member
     *
     * @return integer 
     */
    public function getVisaClsTeamMember()
    {
        return $this->visa_cls_team_member;
    }

    /**
     * Set visa_is_delivered_to_embassy
     *
     * @param integer $visaIsDeliveredToEmbassy
     * @return Orders
     */
    public function setVisaIsDeliveredToEmbassy($visaIsDeliveredToEmbassy)
    {
        $this->visa_is_delivered_to_embassy = $visaIsDeliveredToEmbassy;
    
        return $this;
    }

    /**
     * Get visa_is_delivered_to_embassy
     *
     * @return integer 
     */
    public function getVisaIsDeliveredToEmbassy()
    {
        return $this->visa_is_delivered_to_embassy;
    }

    /**
     * Set visa_is_delivered_to_embassy_date
     *
     * @param string $visaIsDeliveredToEmbassyDate
     * @return Orders
     */
    public function setVisaIsDeliveredToEmbassyDate($visaIsDeliveredToEmbassyDate)
    {
        $this->visa_is_delivered_to_embassy_date = $visaIsDeliveredToEmbassyDate;
    
        return $this;
    }

    /**
     * Get visa_is_delivered_to_embassy_date
     *
     * @return string 
     */
    public function getVisaIsDeliveredToEmbassyDate()
    {
        return $this->visa_is_delivered_to_embassy_date;
    }

    /**
     * Set visa_next_embassy
     *
     * @param string $visaNextEmbassy
     * @return Orders
     */
    public function setVisaNextEmbassy($visaNextEmbassy)
    {
        $this->visa_next_embassy = $visaNextEmbassy;
    
        return $this;
    }

    /**
     * Get visa_next_embassy
     *
     * @return string 
     */
    public function getVisaNextEmbassy()
    {
        return $this->visa_next_embassy;
    }

    /**
     * Set passport_office_booking_no
     *
     * @param string $passportOfficeBookingNo
     * @return Orders
     */
    public function setPassportOfficeBookingNo($passportOfficeBookingNo)
    {
        $this->passport_office_booking_no = $passportOfficeBookingNo;
    
        return $this;
    }

    /**
     * Get passport_office_booking_no
     *
     * @return string 
     */
    public function getPassportOfficeBookingNo()
    {
        return $this->passport_office_booking_no;
    }

    /**
     * Set passport_office_booking_time
     *
     * @param string $passportOfficeBookingTime
     * @return Orders
     */
    public function setPassportOfficeBookingTime($passportOfficeBookingTime)
    {
        $this->passport_office_booking_time = $passportOfficeBookingTime;
    
        return $this;
    }

    /**
     * Get passport_office_booking_time
     *
     * @return string 
     */
    public function getPassportOfficeBookingTime()
    {
        return $this->passport_office_booking_time;
    }

    /**
     * Set passport_office_booking_time_hr
     *
     * @param string $passportOfficeBookingTimeHr
     * @return Orders
     */
    public function setPassportOfficeBookingTimeHr($passportOfficeBookingTimeHr)
    {
        $this->passport_office_booking_time_hr = $passportOfficeBookingTimeHr;
    
        return $this;
    }

    /**
     * Get passport_office_booking_time_hr
     *
     * @return string 
     */
    public function getPassportOfficeBookingTimeHr()
    {
        return $this->passport_office_booking_time_hr;
    }

    /**
     * Set passport_office_booking_time_min
     *
     * @param string $passportOfficeBookingTimeMin
     * @return Orders
     */
    public function setPassportOfficeBookingTimeMin($passportOfficeBookingTimeMin)
    {
        $this->passport_office_booking_time_min = $passportOfficeBookingTimeMin;
    
        return $this;
    }

    /**
     * Get passport_office_booking_time_min
     *
     * @return string 
     */
    public function getPassportOfficeBookingTimeMin()
    {
        return $this->passport_office_booking_time_min;
    }

    /**
     * Set date_completed
     *
     * @param string $dateCompleted
     * @return Orders
     */
    public function setDateCompleted($dateCompleted)
    {
        $this->date_completed = $dateCompleted;
    
        return $this;
    }

    /**
     * Get date_completed
     *
     * @return string 
     */
    public function getDateCompleted()
    {
        return $this->date_completed;
    }

    /**
     * Set s_doc_sent
     *
     * @param integer $sDocSent
     * @return Orders
     */
    public function setSDocSent($sDocSent)
    {
        $this->s_doc_sent = $sDocSent;
    
        return $this;
    }

    /**
     * Get s_doc_sent
     *
     * @return integer 
     */
    public function getSDocSent()
    {
        return $this->s_doc_sent;
    }

    /**
     * Set date_doc_sent
     *
     * @param string $dateDocSent
     * @return Orders
     */
    public function setDateDocSent($dateDocSent)
    {
        $this->date_doc_sent = $dateDocSent;
    
        return $this;
    }

    /**
     * Get date_doc_sent
     *
     * @return string 
     */
    public function getDateDocSent()
    {
        return $this->date_doc_sent;
    }

    /**
     * Set police_clearance_id
     *
     * @param integer $policeClearanceId
     * @return Orders
     */
    public function setPoliceClearanceId($policeClearanceId)
    {
        $this->police_clearance_id = $policeClearanceId;
    
        return $this;
    }

    /**
     * Get police_clearance_id
     *
     * @return integer 
     */
    public function getPoliceClearanceId()
    {
        return $this->police_clearance_id;
    }

    /**
     * Set is_smart_traveller
     *
     * @param integer $isSmartTraveller
     * @return Orders
     */
    public function setIsSmartTraveller($isSmartTraveller)
    {
        $this->is_smart_traveller = $isSmartTraveller;
    
        return $this;
    }

    /**
     * Get is_smart_traveller
     *
     * @return integer 
     */
    public function getIsSmartTraveller()
    {
        return $this->is_smart_traveller;
    }

    /**
     * Set doc_delivery_type
     *
     * @param integer $docDeliveryType
     * @return Orders
     */
    public function setDocDeliveryType($docDeliveryType)
    {
        $this->doc_delivery_type = $docDeliveryType;
    
        return $this;
    }

    /**
     * Get doc_delivery_type
     *
     * @return integer 
     */
    public function getDocDeliveryType()
    {
        return $this->doc_delivery_type;
    }

    /**
     * Set police_clearance_date_cls_received_all_items
     *
     * @param string $policeClearanceDateClsReceivedAllItems
     * @return Orders
     */
    public function setPoliceClearanceDateClsReceivedAllItems($policeClearanceDateClsReceivedAllItems)
    {
        $this->police_clearance_date_cls_received_all_items = $policeClearanceDateClsReceivedAllItems;
    
        return $this;
    }

    /**
     * Get police_clearance_date_cls_received_all_items
     *
     * @return string 
     */
    public function getPoliceClearanceDateClsReceivedAllItems()
    {
        return $this->police_clearance_date_cls_received_all_items;
    }

    /**
     * Set police_clearance_date_submitted_for_processing
     *
     * @param string $policeClearanceDateSubmittedForProcessing
     * @return Orders
     */
    public function setPoliceClearanceDateSubmittedForProcessing($policeClearanceDateSubmittedForProcessing)
    {
        $this->police_clearance_date_submitted_for_processing = $policeClearanceDateSubmittedForProcessing;
    
        return $this;
    }

    /**
     * Get police_clearance_date_submitted_for_processing
     *
     * @return string 
     */
    public function getPoliceClearanceDateSubmittedForProcessing()
    {
        return $this->police_clearance_date_submitted_for_processing;
    }

    /**
     * Set police_clearance_date_completed_and_received_at_cls
     *
     * @param string $policeClearanceDateCompletedAndReceivedAtCls
     * @return Orders
     */
    public function setPoliceClearanceDateCompletedAndReceivedAtCls($policeClearanceDateCompletedAndReceivedAtCls)
    {
        $this->police_clearance_date_completed_and_received_at_cls = $policeClearanceDateCompletedAndReceivedAtCls;
    
        return $this;
    }

    /**
     * Get police_clearance_date_completed_and_received_at_cls
     *
     * @return string 
     */
    public function getPoliceClearanceDateCompletedAndReceivedAtCls()
    {
        return $this->police_clearance_date_completed_and_received_at_cls;
    }

    /**
     * Set police_clearance_date_order_on_route_and_closed
     *
     * @param string $policeClearanceDateOrderOnRouteAndClosed
     * @return Orders
     */
    public function setPoliceClearanceDateOrderOnRouteAndClosed($policeClearanceDateOrderOnRouteAndClosed)
    {
        $this->police_clearance_date_order_on_route_and_closed = $policeClearanceDateOrderOnRouteAndClosed;
    
        return $this;
    }

    /**
     * Get police_clearance_date_order_on_route_and_closed
     *
     * @return string 
     */
    public function getPoliceClearanceDateOrderOnRouteAndClosed()
    {
        return $this->police_clearance_date_order_on_route_and_closed;
    }

    /**
     * Set doc_receiver_name
     *
     * @param string $docReceiverName
     * @return Orders
     */
    public function setDocReceiverName($docReceiverName)
    {
        $this->doc_receiver_name = $docReceiverName;
    
        return $this;
    }

    /**
     * Get doc_receiver_name
     *
     * @return string 
     */
    public function getDocReceiverName()
    {
        return $this->doc_receiver_name;
    }

    /**
     * Set doc_pickup_address
     *
     * @param string $docPickupAddress
     * @return Orders
     */
    public function setDocPickupAddress($docPickupAddress)
    {
        $this->doc_pickup_address = $docPickupAddress;
    
        return $this;
    }

    /**
     * Get doc_pickup_address
     *
     * @return string 
     */
    public function getDocPickupAddress()
    {
        return $this->doc_pickup_address;
    }

    /**
     * Set doc_pickup_city
     *
     * @param string $docPickupCity
     * @return Orders
     */
    public function setDocPickupCity($docPickupCity)
    {
        $this->doc_pickup_city = $docPickupCity;
    
        return $this;
    }

    /**
     * Get doc_pickup_city
     *
     * @return string 
     */
    public function getDocPickupCity()
    {
        return $this->doc_pickup_city;
    }

    /**
     * Set doc_pickup_postcode
     *
     * @param string $docPickupPostcode
     * @return Orders
     */
    public function setDocPickupPostcode($docPickupPostcode)
    {
        $this->doc_pickup_postcode = $docPickupPostcode;
    
        return $this;
    }

    /**
     * Get doc_pickup_postcode
     *
     * @return string 
     */
    public function getDocPickupPostcode()
    {
        return $this->doc_pickup_postcode;
    }

    /**
     * Set doc_pickup_contact_no
     *
     * @param string $docPickupContactNo
     * @return Orders
     */
    public function setDocPickupContactNo($docPickupContactNo)
    {
        $this->doc_pickup_contact_no = $docPickupContactNo;
    
        return $this;
    }

    /**
     * Get doc_pickup_contact_no
     *
     * @return string 
     */
    public function getDocPickupContactNo()
    {
        return $this->doc_pickup_contact_no;
    }

    /**
     * Set doc_pickup_contact_area
     *
     * @param string $docPickupContactArea
     * @return Orders
     */
    public function setDocPickupContactArea($docPickupContactArea)
    {
        $this->doc_pickup_contact_area = $docPickupContactArea;
    
        return $this;
    }

    /**
     * Get doc_pickup_contact_area
     *
     * @return string 
     */
    public function getDocPickupContactArea()
    {
        return $this->doc_pickup_contact_area;
    }

    /**
     * Set doc_pickup_contact_area_alt1
     *
     * @param string $docPickupContactAreaAlt1
     * @return Orders
     */
    public function setDocPickupContactAreaAlt1($docPickupContactAreaAlt1)
    {
        $this->doc_pickup_contact_area_alt1 = $docPickupContactAreaAlt1;
    
        return $this;
    }

    /**
     * Get doc_pickup_contact_area_alt1
     *
     * @return string 
     */
    public function getDocPickupContactAreaAlt1()
    {
        return $this->doc_pickup_contact_area_alt1;
    }

    /**
     * Set doc_pickup_contact_area_alt2
     *
     * @param string $docPickupContactAreaAlt2
     * @return Orders
     */
    public function setDocPickupContactAreaAlt2($docPickupContactAreaAlt2)
    {
        $this->doc_pickup_contact_area_alt2 = $docPickupContactAreaAlt2;
    
        return $this;
    }

    /**
     * Get doc_pickup_contact_area_alt2
     *
     * @return string 
     */
    public function getDocPickupContactAreaAlt2()
    {
        return $this->doc_pickup_contact_area_alt2;
    }

    /**
     * Set doc_delivery_company
     *
     * @param string $docDeliveryCompany
     * @return Orders
     */
    public function setDocDeliveryCompany($docDeliveryCompany)
    {
        $this->doc_delivery_company = $docDeliveryCompany;
    
        return $this;
    }

    /**
     * Get doc_delivery_company
     *
     * @return string 
     */
    public function getDocDeliveryCompany()
    {
        return $this->doc_delivery_company;
    }

    /**
     * Set doc_delivery_company_alt1
     *
     * @param string $docDeliveryCompanyAlt1
     * @return Orders
     */
    public function setDocDeliveryCompanyAlt1($docDeliveryCompanyAlt1)
    {
        $this->doc_delivery_company_alt1 = $docDeliveryCompanyAlt1;
    
        return $this;
    }

    /**
     * Get doc_delivery_company_alt1
     *
     * @return string 
     */
    public function getDocDeliveryCompanyAlt1()
    {
        return $this->doc_delivery_company_alt1;
    }

    /**
     * Set doc_delivery_company_alt2
     *
     * @param string $docDeliveryCompanyAlt2
     * @return Orders
     */
    public function setDocDeliveryCompanyAlt2($docDeliveryCompanyAlt2)
    {
        $this->doc_delivery_company_alt2 = $docDeliveryCompanyAlt2;
    
        return $this;
    }

    /**
     * Get doc_delivery_company_alt2
     *
     * @return string 
     */
    public function getDocDeliveryCompanyAlt2()
    {
        return $this->doc_delivery_company_alt2;
    }

    /**
     * Set doc_delivery_recipient_name
     *
     * @param string $docDeliveryRecipientName
     * @return Orders
     */
    public function setDocDeliveryRecipientName($docDeliveryRecipientName)
    {
        $this->doc_delivery_recipient_name = $docDeliveryRecipientName;
    
        return $this;
    }

    /**
     * Get doc_delivery_recipient_name
     *
     * @return string 
     */
    public function getDocDeliveryRecipientName()
    {
        return $this->doc_delivery_recipient_name;
    }

    /**
     * Set doc_delivery_recipient_name_alt1
     *
     * @param string $docDeliveryRecipientNameAlt1
     * @return Orders
     */
    public function setDocDeliveryRecipientNameAlt1($docDeliveryRecipientNameAlt1)
    {
        $this->doc_delivery_recipient_name_alt1 = $docDeliveryRecipientNameAlt1;
    
        return $this;
    }

    /**
     * Get doc_delivery_recipient_name_alt1
     *
     * @return string 
     */
    public function getDocDeliveryRecipientNameAlt1()
    {
        return $this->doc_delivery_recipient_name_alt1;
    }

    /**
     * Set doc_delivery_recipient_name_alt2
     *
     * @param string $docDeliveryRecipientNameAlt2
     * @return Orders
     */
    public function setDocDeliveryRecipientNameAlt2($docDeliveryRecipientNameAlt2)
    {
        $this->doc_delivery_recipient_name_alt2 = $docDeliveryRecipientNameAlt2;
    
        return $this;
    }

    /**
     * Get doc_delivery_recipient_name_alt2
     *
     * @return string 
     */
    public function getDocDeliveryRecipientNameAlt2()
    {
        return $this->doc_delivery_recipient_name_alt2;
    }

    /**
     * Set doc_delivery_address
     *
     * @param string $docDeliveryAddress
     * @return Orders
     */
    public function setDocDeliveryAddress($docDeliveryAddress)
    {
        $this->doc_delivery_address = $docDeliveryAddress;
    
        return $this;
    }

    /**
     * Get doc_delivery_address
     *
     * @return string 
     */
    public function getDocDeliveryAddress()
    {
        return $this->doc_delivery_address;
    }

    /**
     * Set doc_delivery_address_alt1
     *
     * @param string $docDeliveryAddressAlt1
     * @return Orders
     */
    public function setDocDeliveryAddressAlt1($docDeliveryAddressAlt1)
    {
        $this->doc_delivery_address_alt1 = $docDeliveryAddressAlt1;
    
        return $this;
    }

    /**
     * Get doc_delivery_address_alt1
     *
     * @return string 
     */
    public function getDocDeliveryAddressAlt1()
    {
        return $this->doc_delivery_address_alt1;
    }

    /**
     * Set doc_delivery_address_alt2
     *
     * @param string $docDeliveryAddressAlt2
     * @return Orders
     */
    public function setDocDeliveryAddressAlt2($docDeliveryAddressAlt2)
    {
        $this->doc_delivery_address_alt2 = $docDeliveryAddressAlt2;
    
        return $this;
    }

    /**
     * Get doc_delivery_address_alt2
     *
     * @return string 
     */
    public function getDocDeliveryAddressAlt2()
    {
        return $this->doc_delivery_address_alt2;
    }

    /**
     * Set doc_delivery_city
     *
     * @param string $docDeliveryCity
     * @return Orders
     */
    public function setDocDeliveryCity($docDeliveryCity)
    {
        $this->doc_delivery_city = $docDeliveryCity;
    
        return $this;
    }

    /**
     * Get doc_delivery_city
     *
     * @return string 
     */
    public function getDocDeliveryCity()
    {
        return $this->doc_delivery_city;
    }

    /**
     * Set doc_delivery_city_alt1
     *
     * @param string $docDeliveryCityAlt1
     * @return Orders
     */
    public function setDocDeliveryCityAlt1($docDeliveryCityAlt1)
    {
        $this->doc_delivery_city_alt1 = $docDeliveryCityAlt1;
    
        return $this;
    }

    /**
     * Get doc_delivery_city_alt1
     *
     * @return string 
     */
    public function getDocDeliveryCityAlt1()
    {
        return $this->doc_delivery_city_alt1;
    }

    /**
     * Set doc_delivery_city_alt2
     *
     * @param string $docDeliveryCityAlt2
     * @return Orders
     */
    public function setDocDeliveryCityAlt2($docDeliveryCityAlt2)
    {
        $this->doc_delivery_city_alt2 = $docDeliveryCityAlt2;
    
        return $this;
    }

    /**
     * Get doc_delivery_city_alt2
     *
     * @return string 
     */
    public function getDocDeliveryCityAlt2()
    {
        return $this->doc_delivery_city_alt2;
    }

    /**
     * Set doc_delivery_postcode
     *
     * @param string $docDeliveryPostcode
     * @return Orders
     */
    public function setDocDeliveryPostcode($docDeliveryPostcode)
    {
        $this->doc_delivery_postcode = $docDeliveryPostcode;
    
        return $this;
    }

    /**
     * Get doc_delivery_postcode
     *
     * @return string 
     */
    public function getDocDeliveryPostcode()
    {
        return $this->doc_delivery_postcode;
    }

    /**
     * Set doc_delivery_postcode_alt1
     *
     * @param string $docDeliveryPostcodeAlt1
     * @return Orders
     */
    public function setDocDeliveryPostcodeAlt1($docDeliveryPostcodeAlt1)
    {
        $this->doc_delivery_postcode_alt1 = $docDeliveryPostcodeAlt1;
    
        return $this;
    }

    /**
     * Get doc_delivery_postcode_alt1
     *
     * @return string 
     */
    public function getDocDeliveryPostcodeAlt1()
    {
        return $this->doc_delivery_postcode_alt1;
    }

    /**
     * Set doc_delivery_postcode_alt2
     *
     * @param string $docDeliveryPostcodeAlt2
     * @return Orders
     */
    public function setDocDeliveryPostcodeAlt2($docDeliveryPostcodeAlt2)
    {
        $this->doc_delivery_postcode_alt2 = $docDeliveryPostcodeAlt2;
    
        return $this;
    }

    /**
     * Get doc_delivery_postcode_alt2
     *
     * @return string 
     */
    public function getDocDeliveryPostcodeAlt2()
    {
        return $this->doc_delivery_postcode_alt2;
    }

    /**
     * Set doc_delivery_contact_no
     *
     * @param string $docDeliveryContactNo
     * @return Orders
     */
    public function setDocDeliveryContactNo($docDeliveryContactNo)
    {
        $this->doc_delivery_contact_no = $docDeliveryContactNo;
    
        return $this;
    }

    /**
     * Get doc_delivery_contact_no
     *
     * @return string 
     */
    public function getDocDeliveryContactNo()
    {
        return $this->doc_delivery_contact_no;
    }

    /**
     * Set doc_delivery_contact_no_alt1
     *
     * @param string $docDeliveryContactNoAlt1
     * @return Orders
     */
    public function setDocDeliveryContactNoAlt1($docDeliveryContactNoAlt1)
    {
        $this->doc_delivery_contact_no_alt1 = $docDeliveryContactNoAlt1;
    
        return $this;
    }

    /**
     * Get doc_delivery_contact_no_alt1
     *
     * @return string 
     */
    public function getDocDeliveryContactNoAlt1()
    {
        return $this->doc_delivery_contact_no_alt1;
    }

    /**
     * Set doc_delivery_contact_no_alt2
     *
     * @param string $docDeliveryContactNoAlt2
     * @return Orders
     */
    public function setDocDeliveryContactNoAlt2($docDeliveryContactNoAlt2)
    {
        $this->doc_delivery_contact_no_alt2 = $docDeliveryContactNoAlt2;
    
        return $this;
    }

    /**
     * Get doc_delivery_contact_no_alt2
     *
     * @return string 
     */
    public function getDocDeliveryContactNoAlt2()
    {
        return $this->doc_delivery_contact_no_alt2;
    }

    /**
     * Set doc_delivery_security_no
     *
     * @param string $docDeliverySecurityNo
     * @return Orders
     */
    public function setDocDeliverySecurityNo($docDeliverySecurityNo)
    {
        $this->doc_delivery_security_no = $docDeliverySecurityNo;
    
        return $this;
    }

    /**
     * Get doc_delivery_security_no
     *
     * @return string 
     */
    public function getDocDeliverySecurityNo()
    {
        return $this->doc_delivery_security_no;
    }

    /**
     * Set doc_package_total_pieces
     *
     * @param integer $docPackageTotalPieces
     * @return Orders
     */
    public function setDocPackageTotalPieces($docPackageTotalPieces)
    {
        $this->doc_package_total_pieces = $docPackageTotalPieces;
    
        return $this;
    }

    /**
     * Get doc_package_total_pieces
     *
     * @return integer 
     */
    public function getDocPackageTotalPieces()
    {
        return $this->doc_package_total_pieces;
    }

    /**
     * Set doc_package_pickup_date
     *
     * @param string $docPackagePickupDate
     * @return Orders
     */
    public function setDocPackagePickupDate($docPackagePickupDate)
    {
        $this->doc_package_pickup_date = $docPackagePickupDate;
    
        return $this;
    }

    /**
     * Get doc_package_pickup_date
     *
     * @return string 
     */
    public function getDocPackagePickupDate()
    {
        return $this->doc_package_pickup_date;
    }

    /**
     * Set doc_package_ready_hr
     *
     * @param string $docPackageReadyHr
     * @return Orders
     */
    public function setDocPackageReadyHr($docPackageReadyHr)
    {
        $this->doc_package_ready_hr = $docPackageReadyHr;
    
        return $this;
    }

    /**
     * Get doc_package_ready_hr
     *
     * @return string 
     */
    public function getDocPackageReadyHr()
    {
        return $this->doc_package_ready_hr;
    }

    /**
     * Set doc_package_ready_min
     *
     * @param string $docPackageReadyMin
     * @return Orders
     */
    public function setDocPackageReadyMin($docPackageReadyMin)
    {
        $this->doc_package_ready_min = $docPackageReadyMin;
    
        return $this;
    }

    /**
     * Get doc_package_ready_min
     *
     * @return string 
     */
    public function getDocPackageReadyMin()
    {
        return $this->doc_package_ready_min;
    }

    /**
     * Set doc_package_ready_ampm
     *
     * @param string $docPackageReadyAmpm
     * @return Orders
     */
    public function setDocPackageReadyAmpm($docPackageReadyAmpm)
    {
        $this->doc_package_ready_ampm = $docPackageReadyAmpm;
    
        return $this;
    }

    /**
     * Get doc_package_ready_ampm
     *
     * @return string 
     */
    public function getDocPackageReadyAmpm()
    {
        return $this->doc_package_ready_ampm;
    }

    /**
     * Set doc_package_office_close_hr
     *
     * @param string $docPackageOfficeCloseHr
     * @return Orders
     */
    public function setDocPackageOfficeCloseHr($docPackageOfficeCloseHr)
    {
        $this->doc_package_office_close_hr = $docPackageOfficeCloseHr;
    
        return $this;
    }

    /**
     * Get doc_package_office_close_hr
     *
     * @return string 
     */
    public function getDocPackageOfficeCloseHr()
    {
        return $this->doc_package_office_close_hr;
    }

    /**
     * Set doc_package_office_close_min
     *
     * @param string $docPackageOfficeCloseMin
     * @return Orders
     */
    public function setDocPackageOfficeCloseMin($docPackageOfficeCloseMin)
    {
        $this->doc_package_office_close_min = $docPackageOfficeCloseMin;
    
        return $this;
    }

    /**
     * Get doc_package_office_close_min
     *
     * @return string 
     */
    public function getDocPackageOfficeCloseMin()
    {
        return $this->doc_package_office_close_min;
    }

    /**
     * Set doc_package_office_close_ampm
     *
     * @param string $docPackageOfficeCloseAmpm
     * @return Orders
     */
    public function setDocPackageOfficeCloseAmpm($docPackageOfficeCloseAmpm)
    {
        $this->doc_package_office_close_ampm = $docPackageOfficeCloseAmpm;
    
        return $this;
    }

    /**
     * Get doc_package_office_close_ampm
     *
     * @return string 
     */
    public function getDocPackageOfficeCloseAmpm()
    {
        return $this->doc_package_office_close_ampm;
    }

    /**
     * Set russian_visa_voucher_col_no
     *
     * @param integer $russianVisaVoucherColNo
     * @return Orders
     */
    public function setRussianVisaVoucherColNo($russianVisaVoucherColNo)
    {
        $this->russian_visa_voucher_col_no = $russianVisaVoucherColNo;
    
        return $this;
    }

    /**
     * Get russian_visa_voucher_col_no
     *
     * @return integer 
     */
    public function getRussianVisaVoucherColNo()
    {
        return $this->russian_visa_voucher_col_no;
    }

    /**
     * Set russian_visa_voucher_id
     *
     * @param integer $russianVisaVoucherId
     * @return Orders
     */
    public function setRussianVisaVoucherId($russianVisaVoucherId)
    {
        $this->russian_visa_voucher_id = $russianVisaVoucherId;
    
        return $this;
    }

    /**
     * Get russian_visa_voucher_id
     *
     * @return integer 
     */
    public function getRussianVisaVoucherId()
    {
        return $this->russian_visa_voucher_id;
    }

    /**
     * Set russian_visa_voucher_col_cost
     *
     * @param float $russianVisaVoucherColCost
     * @return Orders
     */
    public function setRussianVisaVoucherColCost($russianVisaVoucherColCost)
    {
        $this->russian_visa_voucher_col_cost = $russianVisaVoucherColCost;
    
        return $this;
    }

    /**
     * Get russian_visa_voucher_col_cost
     *
     * @return float 
     */
    public function getRussianVisaVoucherColCost()
    {
        return $this->russian_visa_voucher_col_cost;
    }

    /**
     * Set rvv_first_entry_date
     *
     * @param string $rvvFirstEntryDate
     * @return Orders
     */
    public function setRvvFirstEntryDate($rvvFirstEntryDate)
    {
        $this->rvv_first_entry_date = $rvvFirstEntryDate;
    
        return $this;
    }

    /**
     * Get rvv_first_entry_date
     *
     * @return string 
     */
    public function getRvvFirstEntryDate()
    {
        return $this->rvv_first_entry_date;
    }

    /**
     * Set rvv_first_departure_date
     *
     * @param string $rvvFirstDepartureDate
     * @return Orders
     */
    public function setRvvFirstDepartureDate($rvvFirstDepartureDate)
    {
        $this->rvv_first_departure_date = $rvvFirstDepartureDate;
    
        return $this;
    }

    /**
     * Get rvv_first_departure_date
     *
     * @return string 
     */
    public function getRvvFirstDepartureDate()
    {
        return $this->rvv_first_departure_date;
    }

    /**
     * Set rvv_second_entry_date
     *
     * @param string $rvvSecondEntryDate
     * @return Orders
     */
    public function setRvvSecondEntryDate($rvvSecondEntryDate)
    {
        $this->rvv_second_entry_date = $rvvSecondEntryDate;
    
        return $this;
    }

    /**
     * Get rvv_second_entry_date
     *
     * @return string 
     */
    public function getRvvSecondEntryDate()
    {
        return $this->rvv_second_entry_date;
    }

    /**
     * Set rvv_second_departure_date
     *
     * @param string $rvvSecondDepartureDate
     * @return Orders
     */
    public function setRvvSecondDepartureDate($rvvSecondDepartureDate)
    {
        $this->rvv_second_departure_date = $rvvSecondDepartureDate;
    
        return $this;
    }

    /**
     * Get rvv_second_departure_date
     *
     * @return string 
     */
    public function getRvvSecondDepartureDate()
    {
        return $this->rvv_second_departure_date;
    }

    /**
     * Set rvv_list_of_cities
     *
     * @param string $rvvListOfCities
     * @return Orders
     */
    public function setRvvListOfCities($rvvListOfCities)
    {
        $this->rvv_list_of_cities = $rvvListOfCities;
    
        return $this;
    }

    /**
     * Get rvv_list_of_cities
     *
     * @return string 
     */
    public function getRvvListOfCities()
    {
        return $this->rvv_list_of_cities;
    }

    /**
     * Set rvv_list_of_hotels
     *
     * @param string $rvvListOfHotels
     * @return Orders
     */
    public function setRvvListOfHotels($rvvListOfHotels)
    {
        $this->rvv_list_of_hotels = $rvvListOfHotels;
    
        return $this;
    }

    /**
     * Get rvv_list_of_hotels
     *
     * @return string 
     */
    public function getRvvListOfHotels()
    {
        return $this->rvv_list_of_hotels;
    }

    /**
     * Set rvv_visa_applied_at
     *
     * @param string $rvvVisaAppliedAt
     * @return Orders
     */
    public function setRvvVisaAppliedAt($rvvVisaAppliedAt)
    {
        $this->rvv_visa_applied_at = $rvvVisaAppliedAt;
    
        return $this;
    }

    /**
     * Get rvv_visa_applied_at
     *
     * @return string 
     */
    public function getRvvVisaAppliedAt()
    {
        return $this->rvv_visa_applied_at;
    }

    /**
     * Set rvv_file
     *
     * @param string $rvvFile
     * @return Orders
     */
    public function setRvvFile($rvvFile)
    {
        $this->rvv_file = $rvvFile;
    
        return $this;
    }

    /**
     * Get rvv_file
     *
     * @return string 
     */
    public function getRvvFile()
    {
        return $this->rvv_file;
    }

    /**
     * Set rvv_comments
     *
     * @param string $rvvComments
     * @return Orders
     */
    public function setRvvComments($rvvComments)
    {
        $this->rvv_comments = $rvvComments;
    
        return $this;
    }

    /**
     * Get rvv_comments
     *
     * @return string 
     */
    public function getRvvComments()
    {
        return $this->rvv_comments;
    }

    /**
     * Set dl_company
     *
     * @param string $dlCompany
     * @return Orders
     */
    public function setDlCompany($dlCompany)
    {
        $this->dl_company = $dlCompany;
    
        return $this;
    }

    /**
     * Get dl_company
     *
     * @return string 
     */
    public function getDlCompany()
    {
        return $this->dl_company;
    }

    /**
     * Set dl_address
     *
     * @param string $dlAddress
     * @return Orders
     */
    public function setDlAddress($dlAddress)
    {
        $this->dl_address = $dlAddress;
    
        return $this;
    }

    /**
     * Get dl_address
     *
     * @return string 
     */
    public function getDlAddress()
    {
        return $this->dl_address;
    }

    /**
     * Set dl_contact_name
     *
     * @param string $dlContactName
     * @return Orders
     */
    public function setDlContactName($dlContactName)
    {
        $this->dl_contact_name = $dlContactName;
    
        return $this;
    }

    /**
     * Get dl_contact_name
     *
     * @return string 
     */
    public function getDlContactName()
    {
        return $this->dl_contact_name;
    }

    /**
     * Set dl_mobile
     *
     * @param string $dlMobile
     * @return Orders
     */
    public function setDlMobile($dlMobile)
    {
        $this->dl_mobile = $dlMobile;
    
        return $this;
    }

    /**
     * Get dl_mobile
     *
     * @return string 
     */
    public function getDlMobile()
    {
        return $this->dl_mobile;
    }

    /**
     * Set dl_email
     *
     * @param string $dlEmail
     * @return Orders
     */
    public function setDlEmail($dlEmail)
    {
        $this->dl_email = $dlEmail;
    
        return $this;
    }

    /**
     * Get dl_email
     *
     * @return string 
     */
    public function getDlEmail()
    {
        return $this->dl_email;
    }

    /**
     * Set dl_date_doc_returned
     *
     * @param string $dlDateDocReturned
     * @return Orders
     */
    public function setDlDateDocReturned($dlDateDocReturned)
    {
        $this->dl_date_doc_returned = $dlDateDocReturned;
    
        return $this;
    }

    /**
     * Get dl_date_doc_returned
     *
     * @return string 
     */
    public function getDlDateDocReturned()
    {
        return $this->dl_date_doc_returned;
    }

    /**
     * Set dl_embassy
     *
     * @param integer $dlEmbassy
     * @return Orders
     */
    public function setDlEmbassy($dlEmbassy)
    {
        $this->dl_embassy = $dlEmbassy;
    
        return $this;
    }

    /**
     * Get dl_embassy
     *
     * @return integer 
     */
    public function getDlEmbassy()
    {
        return $this->dl_embassy;
    }

    /**
     * Set dl_ref_no
     *
     * @param string $dlRefNo
     * @return Orders
     */
    public function setDlRefNo($dlRefNo)
    {
        $this->dl_ref_no = $dlRefNo;
    
        return $this;
    }

    /**
     * Get dl_ref_no
     *
     * @return string 
     */
    public function getDlRefNo()
    {
        return $this->dl_ref_no;
    }

    /**
     * Set dl_com_invoice_no
     *
     * @param string $dlComInvoiceNo
     * @return Orders
     */
    public function setDlComInvoiceNo($dlComInvoiceNo)
    {
        $this->dl_com_invoice_no = $dlComInvoiceNo;
    
        return $this;
    }

    /**
     * Get dl_com_invoice_no
     *
     * @return string 
     */
    public function getDlComInvoiceNo()
    {
        return $this->dl_com_invoice_no;
    }

    /**
     * Set dl_payment_type
     *
     * @param string $dlPaymentType
     * @return Orders
     */
    public function setDlPaymentType($dlPaymentType)
    {
        $this->dl_payment_type = $dlPaymentType;
    
        return $this;
    }

    /**
     * Get dl_payment_type
     *
     * @return string 
     */
    public function getDlPaymentType()
    {
        return $this->dl_payment_type;
    }

    /**
     * Set dl_nationality
     *
     * @param integer $dlNationality
     * @return Orders
     */
    public function setDlNationality($dlNationality)
    {
        $this->dl_nationality = $dlNationality;
    
        return $this;
    }

    /**
     * Get dl_nationality
     *
     * @return integer 
     */
    public function getDlNationality()
    {
        return $this->dl_nationality;
    }

    /**
     * Set dl_visa_shipped_by
     *
     * @param string $dlVisaShippedBy
     * @return Orders
     */
    public function setDlVisaShippedBy($dlVisaShippedBy)
    {
        $this->dl_visa_shipped_by = $dlVisaShippedBy;
    
        return $this;
    }

    /**
     * Get dl_visa_shipped_by
     *
     * @return string 
     */
    public function getDlVisaShippedBy()
    {
        return $this->dl_visa_shipped_by;
    }

    /**
     * Set dl_visa_com_note_no
     *
     * @param string $dlVisaComNoteNo
     * @return Orders
     */
    public function setDlVisaComNoteNo($dlVisaComNoteNo)
    {
        $this->dl_visa_com_note_no = $dlVisaComNoteNo;
    
        return $this;
    }

    /**
     * Get dl_visa_com_note_no
     *
     * @return string 
     */
    public function getDlVisaComNoteNo()
    {
        return $this->dl_visa_com_note_no;
    }

    /**
     * Set dl_visa_com_note_in
     *
     * @param string $dlVisaComNoteIn
     * @return Orders
     */
    public function setDlVisaComNoteIn($dlVisaComNoteIn)
    {
        $this->dl_visa_com_note_in = $dlVisaComNoteIn;
    
        return $this;
    }

    /**
     * Get dl_visa_com_note_in
     *
     * @return string 
     */
    public function getDlVisaComNoteIn()
    {
        return $this->dl_visa_com_note_in;
    }

    /**
     * Set dl_visa_invoice_no
     *
     * @param string $dlVisaInvoiceNo
     * @return Orders
     */
    public function setDlVisaInvoiceNo($dlVisaInvoiceNo)
    {
        $this->dl_visa_invoice_no = $dlVisaInvoiceNo;
    
        return $this;
    }

    /**
     * Get dl_visa_invoice_no
     *
     * @return string 
     */
    public function getDlVisaInvoiceNo()
    {
        return $this->dl_visa_invoice_no;
    }

    /**
     * Set dl_city
     *
     * @param string $dlCity
     * @return Orders
     */
    public function setDlCity($dlCity)
    {
        $this->dl_city = $dlCity;
    
        return $this;
    }

    /**
     * Get dl_city
     *
     * @return string 
     */
    public function getDlCity()
    {
        return $this->dl_city;
    }

    /**
     * Set dl_state
     *
     * @param string $dlState
     * @return Orders
     */
    public function setDlState($dlState)
    {
        $this->dl_state = $dlState;
    
        return $this;
    }

    /**
     * Get dl_state
     *
     * @return string 
     */
    public function getDlState()
    {
        return $this->dl_state;
    }

    /**
     * Set dl_postcode
     *
     * @param string $dlPostcode
     * @return Orders
     */
    public function setDlPostcode($dlPostcode)
    {
        $this->dl_postcode = $dlPostcode;
    
        return $this;
    }

    /**
     * Get dl_postcode
     *
     * @return string 
     */
    public function getDlPostcode()
    {
        return $this->dl_postcode;
    }

    /**
     * Set s_bulk_order
     *
     * @param integer $sBulkOrder
     * @return Orders
     */
    public function setSBulkOrder($sBulkOrder)
    {
        $this->s_bulk_order = $sBulkOrder;
    
        return $this;
    }

    /**
     * Get s_bulk_order
     *
     * @return integer 
     */
    public function getSBulkOrder()
    {
        return $this->s_bulk_order;
    }

    /**
     * Set signature
     *
     * @param string $signature
     * @return Orders
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    
        return $this;
    }

    /**
     * Get signature
     *
     * @return string 
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set sig_name
     *
     * @param string $sigName
     * @return Orders
     */
    public function setSigName($sigName)
    {
        $this->sig_name = $sigName;
    
        return $this;
    }

    /**
     * Get sig_name
     *
     * @return string 
     */
    public function getSigName()
    {
        return $this->sig_name;
    }

    /**
     * Set dhl_pickup_xml_request
     *
     * @param string $dhlPickupXmlRequest
     * @return Orders
     */
    public function setDhlPickupXmlRequest($dhlPickupXmlRequest)
    {
        $this->dhl_pickup_xml_request = $dhlPickupXmlRequest;
    
        return $this;
    }

    /**
     * Get dhl_pickup_xml_request
     *
     * @return string 
     */
    public function getDhlPickupXmlRequest()
    {
        return $this->dhl_pickup_xml_request;
    }

    /**
     * Set dhl_pickup_xml_response
     *
     * @param string $dhlPickupXmlResponse
     * @return Orders
     */
    public function setDhlPickupXmlResponse($dhlPickupXmlResponse)
    {
        $this->dhl_pickup_xml_response = $dhlPickupXmlResponse;
    
        return $this;
    }

    /**
     * Get dhl_pickup_xml_response
     *
     * @return string 
     */
    public function getDhlPickupXmlResponse()
    {
        return $this->dhl_pickup_xml_response;
    }

    /**
     * Set dhl_shipment_validate_xml_request
     *
     * @param string $dhlShipmentValidateXmlRequest
     * @return Orders
     */
    public function setDhlShipmentValidateXmlRequest($dhlShipmentValidateXmlRequest)
    {
        $this->dhl_shipment_validate_xml_request = $dhlShipmentValidateXmlRequest;
    
        return $this;
    }

    /**
     * Get dhl_shipment_validate_xml_request
     *
     * @return string 
     */
    public function getDhlShipmentValidateXmlRequest()
    {
        return $this->dhl_shipment_validate_xml_request;
    }

    /**
     * Set dhl_shipment_validate_xml_response
     *
     * @param string $dhlShipmentValidateXmlResponse
     * @return Orders
     */
    public function setDhlShipmentValidateXmlResponse($dhlShipmentValidateXmlResponse)
    {
        $this->dhl_shipment_validate_xml_response = $dhlShipmentValidateXmlResponse;
    
        return $this;
    }

    /**
     * Get dhl_shipment_validate_xml_response
     *
     * @return string 
     */
    public function getDhlShipmentValidateXmlResponse()
    {
        return $this->dhl_shipment_validate_xml_response;
    }

    /**
     * Set sender_name
     *
     * @param string $senderName
     * @return Orders
     */
    public function setSenderName($senderName)
    {
        $this->sender_name = $senderName;
    
        return $this;
    }

    /**
     * Get sender_name
     *
     * @return string 
     */
    public function getSenderName()
    {
        return $this->sender_name;
    }

    /**
     * Set sender_signature
     *
     * @param string $senderSignature
     * @return Orders
     */
    public function setSenderSignature($senderSignature)
    {
        $this->sender_signature = $senderSignature;
    
        return $this;
    }

    /**
     * Get sender_signature
     *
     * @return string 
     */
    public function getSenderSignature()
    {
        return $this->sender_signature;
    }

    /**
     * Set sender_signed_datetime
     *
     * @param string $senderSignedDatetime
     * @return Orders
     */
    public function setSenderSignedDatetime($senderSignedDatetime)
    {
        $this->sender_signed_datetime = $senderSignedDatetime;
    
        return $this;
    }

    /**
     * Get sender_signed_datetime
     *
     * @return string 
     */
    public function getSenderSignedDatetime()
    {
        return $this->sender_signed_datetime;
    }

    /**
     * Set doc_pickup_email
     *
     * @param string $docPickupEmail
     * @return Orders
     */
    public function setDocPickupEmail($docPickupEmail)
    {
        $this->doc_pickup_email = $docPickupEmail;
    
        return $this;
    }

    /**
     * Get doc_pickup_email
     *
     * @return string 
     */
    public function getDocPickupEmail()
    {
        return $this->doc_pickup_email;
    }

    /**
     * Set doc_delivery_email
     *
     * @param string $docDeliveryEmail
     * @return Orders
     */
    public function setDocDeliveryEmail($docDeliveryEmail)
    {
        $this->doc_delivery_email = $docDeliveryEmail;
    
        return $this;
    }

    /**
     * Get doc_delivery_email
     *
     * @return string 
     */
    public function getDocDeliveryEmail()
    {
        return $this->doc_delivery_email;
    }

    /**
     * Set doc_delivery_primary_receipient_contact_name
     *
     * @param string $docDeliveryPrimaryReceipientContactName
     * @return Orders
     */
    public function setDocDeliveryPrimaryReceipientContactName($docDeliveryPrimaryReceipientContactName)
    {
        $this->doc_delivery_primary_receipient_contact_name = $docDeliveryPrimaryReceipientContactName;
    
        return $this;
    }

    /**
     * Get doc_delivery_primary_receipient_contact_name
     *
     * @return string 
     */
    public function getDocDeliveryPrimaryReceipientContactName()
    {
        return $this->doc_delivery_primary_receipient_contact_name;
    }

    /**
     * Set doc_delivery_primary_receipient_contact_area
     *
     * @param string $docDeliveryPrimaryReceipientContactArea
     * @return Orders
     */
    public function setDocDeliveryPrimaryReceipientContactArea($docDeliveryPrimaryReceipientContactArea)
    {
        $this->doc_delivery_primary_receipient_contact_area = $docDeliveryPrimaryReceipientContactArea;
    
        return $this;
    }

    /**
     * Get doc_delivery_primary_receipient_contact_area
     *
     * @return string 
     */
    public function getDocDeliveryPrimaryReceipientContactArea()
    {
        return $this->doc_delivery_primary_receipient_contact_area;
    }

    /**
     * Set doc_delivery_primary_receipient_contact_no
     *
     * @param string $docDeliveryPrimaryReceipientContactNo
     * @return Orders
     */
    public function setDocDeliveryPrimaryReceipientContactNo($docDeliveryPrimaryReceipientContactNo)
    {
        $this->doc_delivery_primary_receipient_contact_no = $docDeliveryPrimaryReceipientContactNo;
    
        return $this;
    }

    /**
     * Get doc_delivery_primary_receipient_contact_no
     *
     * @return string 
     */
    public function getDocDeliveryPrimaryReceipientContactNo()
    {
        return $this->doc_delivery_primary_receipient_contact_no;
    }

    /**
     * Set doc_delivery_primary_receipient_email
     *
     * @param string $docDeliveryPrimaryReceipientEmail
     * @return Orders
     */
    public function setDocDeliveryPrimaryReceipientEmail($docDeliveryPrimaryReceipientEmail)
    {
        $this->doc_delivery_primary_receipient_email = $docDeliveryPrimaryReceipientEmail;
    
        return $this;
    }

    /**
     * Get doc_delivery_primary_receipient_email
     *
     * @return string 
     */
    public function getDocDeliveryPrimaryReceipientEmail()
    {
        return $this->doc_delivery_primary_receipient_email;
    }

    /**
     * Set doc_pickup_company
     *
     * @param string $docPickupCompany
     * @return Orders
     */
    public function setDocPickupCompany($docPickupCompany)
    {
        $this->doc_pickup_company = $docPickupCompany;
    
        return $this;
    }

    /**
     * Get doc_pickup_company
     *
     * @return string 
     */
    public function getDocPickupCompany()
    {
        return $this->doc_pickup_company;
    }

    /**
     * Set doc_pickup_contact_name
     *
     * @param string $docPickupContactName
     * @return Orders
     */
    public function setDocPickupContactName($docPickupContactName)
    {
        $this->doc_pickup_contact_name = $docPickupContactName;
    
        return $this;
    }

    /**
     * Get doc_pickup_contact_name
     *
     * @return string 
     */
    public function getDocPickupContactName()
    {
        return $this->doc_pickup_contact_name;
    }
}