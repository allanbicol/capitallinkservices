<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderBulkPublicVisaDetails
 * 
 * @ORM\Table(name="tbl_order_bulk_public_visa_details")
 * @ORM\Entity
 */
class OrderBulkPublicVisaDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="bulk_order_no", type="integer")
     */
    private $bulk_order_no;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="destination", type="integer")
     */
    private $destination;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_date", type="string", length=50)
     */
    private $departure_date;

    /**
     * @var string
     *
     * @ORM\Column(name="entry_date_country", type="string", length=50)
     */
    private $entry_date_country;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_date_country", type="string", length=50)
     */
    private $departure_date_country;

    /**
     * @var string
     *
     * @ORM\Column(name="travel_purpose", type="string")
     */
    private $travel_purpose;

    /**
     * @var integer
     *
     * @ORM\Column(name="selected_visa_type", type="integer")
     */
    private $selected_visa_type;
    
    /**
     * @var float
     *
     * @ORM\Column(name="selected_visa_type_price", type="float")
     */
    private $selected_visa_type_price;

    /**
     * @var string
     *
     * @ORM\Column(name="selected_visa_type_requirements", type="string")
     */
    private $selected_visa_type_requirements;

    /**
     * @var integer
     *
     * @ORM\Column(name="visa_courier", type="integer")
     */
    private $visa_courier;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="visa_courier_price", type="float")
     */
    private $visa_courier_price;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="traveller_title", type="integer")
     */
    private $traveller_title;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_fname", type="string", length=220)
     */
    private $traveller_fname;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_mname", type="string", length=220)
     */
    private $traveller_mname;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_lname", type="string", length=220)
     */
    private $traveller_lname;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_email", type="string", length=100)
     */
    private $traveller_email;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_occupation", type="string", length=220)
     */
    private $traveller_occupation;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_phone", type="string", length=50)
     */
    private $traveller_phone;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_bday", type="string", length=50)
     */
    private $traveller_bday;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_passport_type", type="string", length=255)
     */
    private $traveller_passport_type;

    /**
     * @var integer
     *
     * @ORM\Column(name="traveller_nationality", type="integer")
     */
    private $traveller_nationality;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="is_smart_traveller", type="integer")
     */
    private $is_smart_traveller;

    /**
     * @var string
     *
     * @ORM\Column(name="traveller_passport_no", type="string", length=220)
     */
    private $traveller_passport_no;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_company", type="string", length=220)
     */
    private $dd_company;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_doc_return_address", type="string", length=1000)
     */
    private $dd_doc_return_address;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_city", type="string", length=220)
     */
    private $dd_city;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_state", type="string", length=220)
     */
    private $dd_state;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_postcode", type="string", length=50)
     */
    private $dd_postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_fname", type="string", length=220)
     */
    private $dd_fname;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_lname", type="string", length=220)
     */
    private $dd_lname;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_contact_no", type="string", length=100)
     */
    private $dd_contact_no;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_additional_comment", type="string", length=255)
     */
    private $dd_additional_comment;
    
    /**
     * @var float
     *
     * @ORM\Column(name="discount_rate", type="float")
     */
    private $discount_rate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="discount_code", type="string", length=255)
     */
    private $discount_code;
    
    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;
    
    /**
     * @var float
     *
     * @ORM\Column(name="final_total", type="float")
     */
    private $final_total;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="generated_order_no", type="integer")
     */
    private $generated_order_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="travellers", type="string")
     */
    private $travellers;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bulk_order_no
     *
     * @param integer $bulkOrderNo
     * @return OrderBulkPublicVisaDetails
     */
    public function setBulkOrderNo($bulkOrderNo)
    {
        $this->bulk_order_no = $bulkOrderNo;
    
        return $this;
    }

    /**
     * Get bulk_order_no
     *
     * @return integer 
     */
    public function getBulkOrderNo()
    {
        return $this->bulk_order_no;
    }

    /**
     * Set destination
     *
     * @param integer $destination
     * @return OrderBulkPublicVisaDetails
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    
        return $this;
    }

    /**
     * Get destination
     *
     * @return integer 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set departure_date
     *
     * @param string $departureDate
     * @return OrderBulkPublicVisaDetails
     */
    public function setDepartureDate($departureDate)
    {
        $this->departure_date = $departureDate;
    
        return $this;
    }

    /**
     * Get departure_date
     *
     * @return string 
     */
    public function getDepartureDate()
    {
        return $this->departure_date;
    }

    /**
     * Set entry_date_country
     *
     * @param string $entryDateCountry
     * @return OrderBulkPublicVisaDetails
     */
    public function setEntryDateCountry($entryDateCountry)
    {
        $this->entry_date_country = $entryDateCountry;
    
        return $this;
    }

    /**
     * Get entry_date_country
     *
     * @return string 
     */
    public function getEntryDateCountry()
    {
        return $this->entry_date_country;
    }

    /**
     * Set departure_date_country
     *
     * @param string $departureDateCountry
     * @return OrderBulkPublicVisaDetails
     */
    public function setDepartureDateCountry($departureDateCountry)
    {
        $this->departure_date_country = $departureDateCountry;
    
        return $this;
    }

    /**
     * Get departure_date_country
     *
     * @return string 
     */
    public function getDepartureDateCountry()
    {
        return $this->departure_date_country;
    }

    /**
     * Set travel_purpose
     *
     * @param string $travelPurpose
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravelPurpose($travelPurpose)
    {
        $this->travel_purpose = $travelPurpose;
    
        return $this;
    }

    /**
     * Get travel_purpose
     *
     * @return string 
     */
    public function getTravelPurpose()
    {
        return $this->travel_purpose;
    }

    /**
     * Set selected_visa_type
     *
     * @param integer $selectedVisaType
     * @return OrderBulkPublicVisaDetails
     */
    public function setSelectedVisaType($selectedVisaType)
    {
        $this->selected_visa_type = $selectedVisaType;
    
        return $this;
    }

    /**
     * Get selected_visa_type
     *
     * @return integer 
     */
    public function getSelectedVisaType()
    {
        return $this->selected_visa_type;
    }

    /**
     * Set selected_visa_type_requirements
     *
     * @param string $selectedVisaTypeRequirements
     * @return OrderBulkPublicVisaDetails
     */
    public function setSelectedVisaTypeRequirements($selectedVisaTypeRequirements)
    {
        $this->selected_visa_type_requirements = $selectedVisaTypeRequirements;
    
        return $this;
    }

    /**
     * Get selected_visa_type_requirements
     *
     * @return string 
     */
    public function getSelectedVisaTypeRequirements()
    {
        return $this->selected_visa_type_requirements;
    }

    /**
     * Set visa_courier
     *
     * @param integer $visaCourier
     * @return OrderBulkPublicVisaDetails
     */
    public function setVisaCourier($visaCourier)
    {
        $this->visa_courier = $visaCourier;
    
        return $this;
    }

    /**
     * Get visa_courier
     *
     * @return integer 
     */
    public function getVisaCourier()
    {
        return $this->visa_courier;
    }

    /**
     * Set traveller_fname
     *
     * @param string $travellerFname
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerFname($travellerFname)
    {
        $this->traveller_fname = $travellerFname;
    
        return $this;
    }

    /**
     * Get traveller_fname
     *
     * @return string 
     */
    public function getTravellerFname()
    {
        return $this->traveller_fname;
    }

    /**
     * Set traveller_mname
     *
     * @param string $travellerMname
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerMname($travellerMname)
    {
        $this->traveller_mname = $travellerMname;
    
        return $this;
    }

    /**
     * Get traveller_mname
     *
     * @return string 
     */
    public function getTravellerMname()
    {
        return $this->traveller_mname;
    }

    /**
     * Set traveller_lname
     *
     * @param string $travellerLname
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerLname($travellerLname)
    {
        $this->traveller_lname = $travellerLname;
    
        return $this;
    }

    /**
     * Get traveller_lname
     *
     * @return string 
     */
    public function getTravellerLname()
    {
        return $this->traveller_lname;
    }

    /**
     * Set traveller_email
     *
     * @param string $travellerEmail
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerEmail($travellerEmail)
    {
        $this->traveller_email = $travellerEmail;
    
        return $this;
    }

    /**
     * Get traveller_email
     *
     * @return string 
     */
    public function getTravellerEmail()
    {
        return $this->traveller_email;
    }

    /**
     * Set traveller_occupation
     *
     * @param string $travellerOccupation
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerOccupation($travellerOccupation)
    {
        $this->traveller_occupation = $travellerOccupation;
    
        return $this;
    }

    /**
     * Get traveller_occupation
     *
     * @return string 
     */
    public function getTravellerOccupation()
    {
        return $this->traveller_occupation;
    }

    /**
     * Set traveller_phone
     *
     * @param string $travellerPhone
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerPhone($travellerPhone)
    {
        $this->traveller_phone = $travellerPhone;
    
        return $this;
    }

    /**
     * Get traveller_phone
     *
     * @return string 
     */
    public function getTravellerPhone()
    {
        return $this->traveller_phone;
    }

    /**
     * Set traveller_bday
     *
     * @param string $travellerBday
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerBday($travellerBday)
    {
        $this->traveller_bday = $travellerBday;
    
        return $this;
    }

    /**
     * Get traveller_bday
     *
     * @return string 
     */
    public function getTravellerBday()
    {
        return $this->traveller_bday;
    }

    /**
     * Set traveller_passport_type
     *
     * @param string $travellerPassportType
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerPassportType($travellerPassportType)
    {
        $this->traveller_passport_type = $travellerPassportType;
    
        return $this;
    }

    /**
     * Get traveller_passport_type
     *
     * @return string 
     */
    public function getTravellerPassportType()
    {
        return $this->traveller_passport_type;
    }

    /**
     * Set traveller_nationality
     *
     * @param integer $travellerNationality
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerNationality($travellerNationality)
    {
        $this->traveller_nationality = $travellerNationality;
    
        return $this;
    }

    /**
     * Get traveller_nationality
     *
     * @return integer 
     */
    public function getTravellerNationality()
    {
        return $this->traveller_nationality;
    }

    /**
     * Set traveller_passport_no
     *
     * @param string $travellerPassportNo
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerPassportNo($travellerPassportNo)
    {
        $this->traveller_passport_no = $travellerPassportNo;
    
        return $this;
    }

    /**
     * Get traveller_passport_no
     *
     * @return string 
     */
    public function getTravellerPassportNo()
    {
        return $this->traveller_passport_no;
    }

    /**
     * Set dd_company
     *
     * @param string $ddCompany
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdCompany($ddCompany)
    {
        $this->dd_company = $ddCompany;
    
        return $this;
    }

    /**
     * Get dd_company
     *
     * @return string 
     */
    public function getDdCompany()
    {
        return $this->dd_company;
    }

    /**
     * Set dd_doc_return_address
     *
     * @param string $ddDocReturnAddress
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdDocReturnAddress($ddDocReturnAddress)
    {
        $this->dd_doc_return_address = $ddDocReturnAddress;
    
        return $this;
    }

    /**
     * Get dd_doc_return_address
     *
     * @return string 
     */
    public function getDdDocReturnAddress()
    {
        return $this->dd_doc_return_address;
    }

    /**
     * Set dd_city
     *
     * @param string $ddCity
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdCity($ddCity)
    {
        $this->dd_city = $ddCity;
    
        return $this;
    }

    /**
     * Get dd_city
     *
     * @return string 
     */
    public function getDdCity()
    {
        return $this->dd_city;
    }

    /**
     * Set dd_state
     *
     * @param string $ddState
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdState($ddState)
    {
        $this->dd_state = $ddState;
    
        return $this;
    }

    /**
     * Get dd_state
     *
     * @return string 
     */
    public function getDdState()
    {
        return $this->dd_state;
    }

    /**
     * Set dd_postcode
     *
     * @param string $ddPostcode
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdPostcode($ddPostcode)
    {
        $this->dd_postcode = $ddPostcode;
    
        return $this;
    }

    /**
     * Get dd_postcode
     *
     * @return string 
     */
    public function getDdPostcode()
    {
        return $this->dd_postcode;
    }

    /**
     * Set dd_fname
     *
     * @param string $ddFname
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdFname($ddFname)
    {
        $this->dd_fname = $ddFname;
    
        return $this;
    }

    /**
     * Get dd_fname
     *
     * @return string 
     */
    public function getDdFname()
    {
        return $this->dd_fname;
    }

    /**
     * Set dd_lname
     *
     * @param string $ddLname
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdLname($ddLname)
    {
        $this->dd_lname = $ddLname;
    
        return $this;
    }

    /**
     * Get dd_lname
     *
     * @return string 
     */
    public function getDdLname()
    {
        return $this->dd_lname;
    }

    /**
     * Set dd_contact_no
     *
     * @param string $ddContactNo
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdContactNo($ddContactNo)
    {
        $this->dd_contact_no = $ddContactNo;
    
        return $this;
    }

    /**
     * Get dd_contact_no
     *
     * @return string 
     */
    public function getDdContactNo()
    {
        return $this->dd_contact_no;
    }

    /**
     * Set dd_additional_comment
     *
     * @param string $ddAdditionalComment
     * @return OrderBulkPublicVisaDetails
     */
    public function setDdAdditionalComment($ddAdditionalComment)
    {
        $this->dd_additional_comment = $ddAdditionalComment;
    
        return $this;
    }

    /**
     * Get dd_additional_comment
     *
     * @return string 
     */
    public function getDdAdditionalComment()
    {
        return $this->dd_additional_comment;
    }

    /**
     * Set traveller_title
     *
     * @param integer $travellerTitle
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellerTitle($travellerTitle)
    {
        $this->traveller_title = $travellerTitle;
    
        return $this;
    }

    /**
     * Get traveller_title
     *
     * @return integer 
     */
    public function getTravellerTitle()
    {
        return $this->traveller_title;
    }

    /**
     * Set is_smart_traveller
     *
     * @param integer $isSmartTraveller
     * @return OrderBulkPublicVisaDetails
     */
    public function setIsSmartTraveller($isSmartTraveller)
    {
        $this->is_smart_traveller = $isSmartTraveller;
    
        return $this;
    }

    /**
     * Get is_smart_traveller
     *
     * @return integer 
     */
    public function getIsSmartTraveller()
    {
        return $this->is_smart_traveller;
    }

    /**
     * Set selected_visa_type_price
     *
     * @param float $selectedVisaTypePrice
     * @return OrderBulkPublicVisaDetails
     */
    public function setSelectedVisaTypePrice($selectedVisaTypePrice)
    {
        $this->selected_visa_type_price = $selectedVisaTypePrice;
    
        return $this;
    }

    /**
     * Get selected_visa_type_price
     *
     * @return float 
     */
    public function getSelectedVisaTypePrice()
    {
        return $this->selected_visa_type_price;
    }

    /**
     * Set visa_courier_price
     *
     * @param float $visaCourierPrice
     * @return OrderBulkPublicVisaDetails
     */
    public function setVisaCourierPrice($visaCourierPrice)
    {
        $this->visa_courier_price = $visaCourierPrice;
    
        return $this;
    }

    /**
     * Get visa_courier_price
     *
     * @return float 
     */
    public function getVisaCourierPrice()
    {
        return $this->visa_courier_price;
    }

    /**
     * Set discount_rate
     *
     * @param float $discountRate
     * @return OrderBulkPublicVisaDetails
     */
    public function setDiscountRate($discountRate)
    {
        $this->discount_rate = $discountRate;
    
        return $this;
    }

    /**
     * Get discount_rate
     *
     * @return float 
     */
    public function getDiscountRate()
    {
        return $this->discount_rate;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return OrderBulkPublicVisaDetails
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set discount_code
     *
     * @param string $discountCode
     * @return OrderBulkPublicVisaDetails
     */
    public function setDiscountCode($discountCode)
    {
        $this->discount_code = $discountCode;
    
        return $this;
    }

    /**
     * Get discount_code
     *
     * @return string 
     */
    public function getDiscountCode()
    {
        return $this->discount_code;
    }

    /**
     * Set generated_order_no
     *
     * @param integer $generatedOrderNo
     * @return OrderBulkPublicVisaDetails
     */
    public function setGeneratedOrderNo($generatedOrderNo)
    {
        $this->generated_order_no = $generatedOrderNo;
    
        return $this;
    }

    /**
     * Get generated_order_no
     *
     * @return integer 
     */
    public function getGeneratedOrderNo()
    {
        return $this->generated_order_no;
    }

    /**
     * Set final_total
     *
     * @param float $finalTotal
     * @return OrderBulkPublicVisaDetails
     */
    public function setFinalTotal($finalTotal)
    {
        $this->final_total = $finalTotal;
    
        return $this;
    }

    /**
     * Get final_total
     *
     * @return float 
     */
    public function getFinalTotal()
    {
        return $this->final_total;
    }

    /**
     * Set travellers
     *
     * @param string $travellers
     * @return OrderBulkPublicVisaDetails
     */
    public function setTravellers($travellers)
    {
        $this->travellers = $travellers;
    
        return $this;
    }

    /**
     * Get travellers
     *
     * @return string 
     */
    public function getTravellers()
    {
        return $this->travellers;
    }
}