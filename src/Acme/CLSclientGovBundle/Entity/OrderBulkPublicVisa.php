<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderBulkPublicVisa
 *
 * @ORM\Table(name="tbl_order_bulk_public_visa")
 * @ORM\Entity
 */
class OrderBulkPublicVisa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bulk_order_no", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $bulk_order_no;

    /**
     * @var integer
     *
     * @ORM\Column(name="client_id", type="integer")
     */
    private $client_id;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;
    
    /**
     * @var float
     *
     * @ORM\Column(name="discount_rate", type="float")
     */
    private $discount_rate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="discount_code", type="string", length=255)
     */
    private $discount_code;
    
    /**
     * @var float
     *
     * @ORM\Column(name="grand_total", type="float")
     */
    private $grand_total;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="payment_option", type="integer")
     */
    private $payment_option;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_last_saved", type="string", length=50)
     */
    private $date_last_saved;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dd_company", type="string", length=220)
     */
    private $dd_company;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_doc_return_address", type="string", length=1000)
     */
    private $dd_doc_return_address;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_city", type="string", length=220)
     */
    private $dd_city;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_state", type="string", length=220)
     */
    private $dd_state;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_postcode", type="string", length=50)
     */
    private $dd_postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_fname", type="string", length=220)
     */
    private $dd_fname;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_lname", type="string", length=220)
     */
    private $dd_lname;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_contact_no", type="string", length=100)
     */
    private $dd_contact_no;

    /**
     * @var string
     *
     * @ORM\Column(name="dd_additional_comment", type="string", length=255)
     */
    private $dd_additional_comment;
    

    /**
     * Get bulk_order_no
     *
     * @return integer 
     */
    public function getBulkOrderNo()
    {
        return $this->bulk_order_no;
    }

    /**
     * Set client_id
     *
     * @param integer $clientId
     * @return OrderBulkPublicVisa
     */
    public function setClientId($clientId)
    {
        $this->client_id = $clientId;
    
        return $this;
    }

    /**
     * Get client_id
     *
     * @return integer 
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return OrderBulkPublicVisa
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return OrderBulkPublicVisa
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set discount_rate
     *
     * @param float $discountRate
     * @return OrderBulkPublicVisa
     */
    public function setDiscountRate($discountRate)
    {
        $this->discount_rate = $discountRate;
    
        return $this;
    }

    /**
     * Get discount_rate
     *
     * @return float 
     */
    public function getDiscountRate()
    {
        return $this->discount_rate;
    }

    /**
     * Set discount_code
     *
     * @param string $discountCode
     * @return OrderBulkPublicVisa
     */
    public function setDiscountCode($discountCode)
    {
        $this->discount_code = $discountCode;
    
        return $this;
    }

    /**
     * Get discount_code
     *
     * @return string 
     */
    public function getDiscountCode()
    {
        return $this->discount_code;
    }

    /**
     * Set grand_total
     *
     * @param float $grandTotal
     * @return OrderBulkPublicVisa
     */
    public function setGrandTotal($grandTotal)
    {
        $this->grand_total = $grandTotal;
    
        return $this;
    }

    /**
     * Get grand_total
     *
     * @return float 
     */
    public function getGrandTotal()
    {
        return $this->grand_total;
    }

    /**
     * Set payment_option
     *
     * @param integer $paymentOption
     * @return OrderBulkPublicVisa
     */
    public function setPaymentOption($paymentOption)
    {
        $this->payment_option = $paymentOption;
    
        return $this;
    }

    /**
     * Get payment_option
     *
     * @return integer 
     */
    public function getPaymentOption()
    {
        return $this->payment_option;
    }

    /**
     * Set date_last_saved
     *
     * @param string $dateLastSaved
     * @return OrderBulkPublicVisa
     */
    public function setDateLastSaved($dateLastSaved)
    {
        $this->date_last_saved = $dateLastSaved;
    
        return $this;
    }

    /**
     * Get date_last_saved
     *
     * @return string 
     */
    public function getDateLastSaved()
    {
        return $this->date_last_saved;
    }

    /**
     * Set dd_company
     *
     * @param string $ddCompany
     * @return OrderBulkPublicVisa
     */
    public function setDdCompany($ddCompany)
    {
        $this->dd_company = $ddCompany;
    
        return $this;
    }

    /**
     * Get dd_company
     *
     * @return string 
     */
    public function getDdCompany()
    {
        return $this->dd_company;
    }

    /**
     * Set dd_doc_return_address
     *
     * @param string $ddDocReturnAddress
     * @return OrderBulkPublicVisa
     */
    public function setDdDocReturnAddress($ddDocReturnAddress)
    {
        $this->dd_doc_return_address = $ddDocReturnAddress;
    
        return $this;
    }

    /**
     * Get dd_doc_return_address
     *
     * @return string 
     */
    public function getDdDocReturnAddress()
    {
        return $this->dd_doc_return_address;
    }

    /**
     * Set dd_city
     *
     * @param string $ddCity
     * @return OrderBulkPublicVisa
     */
    public function setDdCity($ddCity)
    {
        $this->dd_city = $ddCity;
    
        return $this;
    }

    /**
     * Get dd_city
     *
     * @return string 
     */
    public function getDdCity()
    {
        return $this->dd_city;
    }

    /**
     * Set dd_state
     *
     * @param string $ddState
     * @return OrderBulkPublicVisa
     */
    public function setDdState($ddState)
    {
        $this->dd_state = $ddState;
    
        return $this;
    }

    /**
     * Get dd_state
     *
     * @return string 
     */
    public function getDdState()
    {
        return $this->dd_state;
    }

    /**
     * Set dd_postcode
     *
     * @param string $ddPostcode
     * @return OrderBulkPublicVisa
     */
    public function setDdPostcode($ddPostcode)
    {
        $this->dd_postcode = $ddPostcode;
    
        return $this;
    }

    /**
     * Get dd_postcode
     *
     * @return string 
     */
    public function getDdPostcode()
    {
        return $this->dd_postcode;
    }

    /**
     * Set dd_fname
     *
     * @param string $ddFname
     * @return OrderBulkPublicVisa
     */
    public function setDdFname($ddFname)
    {
        $this->dd_fname = $ddFname;
    
        return $this;
    }

    /**
     * Get dd_fname
     *
     * @return string 
     */
    public function getDdFname()
    {
        return $this->dd_fname;
    }

    /**
     * Set dd_lname
     *
     * @param string $ddLname
     * @return OrderBulkPublicVisa
     */
    public function setDdLname($ddLname)
    {
        $this->dd_lname = $ddLname;
    
        return $this;
    }

    /**
     * Get dd_lname
     *
     * @return string 
     */
    public function getDdLname()
    {
        return $this->dd_lname;
    }

    /**
     * Set dd_contact_no
     *
     * @param string $ddContactNo
     * @return OrderBulkPublicVisa
     */
    public function setDdContactNo($ddContactNo)
    {
        $this->dd_contact_no = $ddContactNo;
    
        return $this;
    }

    /**
     * Get dd_contact_no
     *
     * @return string 
     */
    public function getDdContactNo()
    {
        return $this->dd_contact_no;
    }

    /**
     * Set dd_additional_comment
     *
     * @param string $ddAdditionalComment
     * @return OrderBulkPublicVisa
     */
    public function setDdAdditionalComment($ddAdditionalComment)
    {
        $this->dd_additional_comment = $ddAdditionalComment;
    
        return $this;
    }

    /**
     * Get dd_additional_comment
     *
     * @return string 
     */
    public function getDdAdditionalComment()
    {
        return $this->dd_additional_comment;
    }
}