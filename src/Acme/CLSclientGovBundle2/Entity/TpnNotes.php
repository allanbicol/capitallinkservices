<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TpnNotes
 *
 * @ORM\Table(name="tbl_tpn_notes")
 * @ORM\Entity
 */
class TpnNotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tpn_no", type="integer")
     */
    private $tpn_no;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="date_added", type="string", length=50)
     */
    private $date_added;

    /**
     * @var integer
     *
     * @ORM\Column(name="note_by", type="integer")
     */
    private $note_by;


    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=50)
     */
    private $user_type;
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tpn_no
     *
     * @param integer $tpnNo
     * @return TpnNotes
     */
    public function setTpnNo($tpnNo)
    {
        $this->tpn_no = $tpnNo;
    
        return $this;
    }

    /**
     * Get tpn_no
     *
     * @return integer 
     */
    public function getTpnNo()
    {
        return $this->tpn_no;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return TpnNotes
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set date_added
     *
     * @param string $dateAdded
     * @return TpnNotes
     */
    public function setDateAdded($dateAdded)
    {
        $this->date_added = $dateAdded;
    
        return $this;
    }

    /**
     * Get date_added
     *
     * @return string 
     */
    public function getDateAdded()
    {
        return $this->date_added;
    }

    /**
     * Set note_by
     *
     * @param integer $noteBy
     * @return TpnNotes
     */
    public function setNoteBy($noteBy)
    {
        $this->note_by = $noteBy;
    
        return $this;
    }

    /**
     * Get note_by
     *
     * @return integer 
     */
    public function getNoteBy()
    {
        return $this->note_by;
    }

    /**
     * Set user_type
     *
     * @param string $userType
     * @return TpnNotes
     */
    public function setUserType($userType)
    {
        $this->user_type = $userType;
    
        return $this;
    }

    /**
     * Get user_type
     *
     * @return string 
     */
    public function getUserType()
    {
        return $this->user_type;
    }
}