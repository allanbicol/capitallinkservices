<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="tbl_user_client")
 * @ORM\Entity
 * @UniqueEntity(
 *      fields={"email"},
 *      message="Email is already used"
 * )
 */
class User
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;
    
    /**
     * @Assert\NotBlank(message="Title should not be blank.")
     * @ORM\Column(name="title", type="integer", length=11)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message="First name should not be blank")
     * @Assert\Length(
     *      min = "1",
     *      max = "50",
     *      minMessage = "Firstname must be at least {{ limit }} characters length",
     *      maxMessage = "Firstname cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="fname", type="string", length=50)
     */
    private $fname;

    /**
     * @var string
     * @Assert\NotBlank(message="Last name should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "50",
     *      minMessage = "Lastname must be at least {{ limit }} characters length",
     *      maxMessage = "Lastname cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="lname", type="string", length=50)
     */
    private $lname;


    /**
     * @var string
     * 
     * @Assert\NotBlank(message="Email should not be blank")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Email must be at least {{ limit }} characters length",
     *      maxMessage = "Email cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;


    /**
     * @var string
     * @Assert\NotBlank(message="Password should not be blank")
     * @ORM\Column(name="password", type="string", length=100)
     */
    private $password;

    /**
     * @var string
     *
     * @Assert\Length(
     *      min = "0",
     *      max = "50",
     *      minMessage = "Phone number must be at least {{ limit }} characters length",
     *      maxMessage = "Phone number cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="phone", type="string", length=50)
     */
    private $phone;
    
    /**
     * @var string
     * @Assert\Length(
     *      min = "0",
     *      max = "50",
     *      minMessage = "Mobile number must be at least {{ limit }} characters length",
     *      maxMessage = "Mobile number cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="mobile", type="string", length=50)
     */
    private $mobile;


    /**
     * @ORM\Column(name="department_id", type="integer", length=11)
     */
    private $department_id;
    
    /**
     * @ORM\Column(name="company", type="string", length=1000)
     */
    private $company;
    
    /**
     * @Assert\NotBlank(message="Address should not be blank.")
     * @Assert\Length(
     *      min = "2",
     *      max = "225",
     *      minMessage = "Address must be at least {{ limit }} characters length",
     *      maxMessage = "Address cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="address", type="string", length=225)
     */
    private $address;
    
    /**
     * @Assert\NotBlank(message="City should not be blank.")
     * @Assert\Length(
     *      min = "2",
     *      max = "225",
     *      minMessage = "City must be at least {{ limit }} characters length",
     *      maxMessage = "City cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="city", type="string", length=225)
     */
    private $city;
    
    /**
     * @Assert\NotBlank(message="State should not be blank.")
     * @Assert\Length(
     *      min = "2",
     *      max = "225",
     *      minMessage = "State must be at least {{ limit }} characters length",
     *      maxMessage = "State cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="state", type="string", length=225)
     */
    private $state;
    
    /**
     * @Assert\NotBlank(message="Postcode should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "50",
     *      minMessage = "Postcode must be at least {{ limit }} characters length",
     *      maxMessage = "Postcode cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="postcode", type="string", length=50)
     */
    private $postcode;
    
    /**
     * @Assert\NotBlank(message="Country should not be blank.")
     * @ORM\Column(name="country_id", type="integer", length=11)
     */
    private $country_id;
    
    
    
    /**
     * @Assert\NotBlank(message="Main document delivery address should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Main document delivery address must be at least {{ limit }} characters length",
     *      maxMessage = "Main document delivery address cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="mdda_address", type="string", length=225)
     */
    private $mdda_address;
    
    /**
     * @Assert\NotBlank(message="Main document delivery city should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Main document delivery city must be at least {{ limit }} characters length",
     *      maxMessage = "Main document delivery city cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="mdda_city", type="string", length=225)
     */
    private $mdda_city;
    
    /**
     * @Assert\NotBlank(message="Main document delivery state should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Main document delivery state must be at least {{ limit }} characters length",
     *      maxMessage = "Main document delivery state cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="mdda_state", type="string", length=225)
     */
    private $mdda_state;
    
    /**
     * @Assert\NotBlank(message="Main document delivery postcode should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "50",
     *      minMessage = "Main document delivery postcode must be at least {{ limit }} characters length",
     *      maxMessage = "Main document delivery postcode cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="mdda_postcode", type="string", length=50)
     */
    private $mdda_postcode;
    
    /**
     * @Assert\NotBlank(message="Main document delivery country should not be blank.")
     * @ORM\Column(name="mdda_country_id", type="integer", length=11)
     */
    private $mdda_country_id;
    
    
    /**
     * @Assert\NotBlank(message="Main billing address should not be blank.")
     * @Assert\NotBlank(message="Main document delivery postcode should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Main billing address must be at least {{ limit }} characters length",
     *      maxMessage = "Main billing address cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="mba_address", type="string", length=225)
     */
    private $mba_address;
    
    /**
     * @Assert\NotBlank(message="Main billing city should not be blank.")
     * @ORM\Column(name="mba_city", type="string", length=225)
     */
    private $mba_city;
    
    /**
     * @Assert\NotBlank(message="Main billing state should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Main billing state must be at least {{ limit }} characters length",
     *      maxMessage = "Main billing state cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="mba_state", type="string", length=225)
     */
    private $mba_state;
    
    /**
     * @Assert\NotBlank(message="Main billing postcode should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "50",
     *      minMessage = "Main billing postcode must be at least {{ limit }} characters length",
     *      maxMessage = "Main billing postcode cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="mba_postcode", type="string", length=50)
     */
    private $mba_postcode;
    
    /**
     * @Assert\NotBlank(message="Main billing country should not be blank.")
     * @ORM\Column(name="mba_country_id", type="integer", length=11)
     */
    private $mba_country_id;
    
    /**
     * @ORM\Column(name="can_charge_cost_to_account", type="integer", length=1)
     */
    private $can_charge_cost_to_account;
    
    
    /**
     * @ORM\Column(name="account_no", type="string", length=50)
     */
    private $account_no;
    
    
    /**
     * @ORM\Column(name="can_get_special_price", type="integer", length=1)
     */
    private $can_get_special_price;
    
    /**
     * @ORM\Column(name="special_price", type="integer", length=3)
     */
    private $special_price;
    
    
    /**
     * @ORM\Column(name="reset_pin", type="string", length=10)
     */
    private $reset_pin;
    
    /**
     * @ORM\Column(name="s_enabled", type="integer", length=1)
     */
    private $s_enabled;
    
    
    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set title
     *
     * @param integer $title
     * @return User
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return integer 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return User
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return User
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return User
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    
        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set department_id
     *
     * @param integer $departmentId
     * @return User
     */
    public function setDepartmentId($departmentId)
    {
        $this->department_id = $departmentId;
    
        return $this;
    }

    /**
     * Get department_id
     *
     * @return integer 
     */
    public function getDepartmentId()
    {
        return $this->department_id;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return User
     */
    public function setCompany($company)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return User
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return User
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return User
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;
    
        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set mdda_address
     *
     * @param string $mddaAddress
     * @return User
     */
    public function setMddaAddress($mddaAddress)
    {
        $this->mdda_address = $mddaAddress;
    
        return $this;
    }

    /**
     * Get mdda_address
     *
     * @return string 
     */
    public function getMddaAddress()
    {
        return $this->mdda_address;
    }

    /**
     * Set mdda_city
     *
     * @param string $mddaCity
     * @return User
     */
    public function setMddaCity($mddaCity)
    {
        $this->mdda_city = $mddaCity;
    
        return $this;
    }

    /**
     * Get mdda_city
     *
     * @return string 
     */
    public function getMddaCity()
    {
        return $this->mdda_city;
    }

    /**
     * Set mdda_state
     *
     * @param string $mddaState
     * @return User
     */
    public function setMddaState($mddaState)
    {
        $this->mdda_state = $mddaState;
    
        return $this;
    }

    /**
     * Get mdda_state
     *
     * @return string 
     */
    public function getMddaState()
    {
        return $this->mdda_state;
    }

    /**
     * Set mdda_postcode
     *
     * @param string $mddaPostcode
     * @return User
     */
    public function setMddaPostcode($mddaPostcode)
    {
        $this->mdda_postcode = $mddaPostcode;
    
        return $this;
    }

    /**
     * Get mdda_postcode
     *
     * @return string 
     */
    public function getMddaPostcode()
    {
        return $this->mdda_postcode;
    }

    /**
     * Set mdda_country_id
     *
     * @param integer $mddaCountryId
     * @return User
     */
    public function setMddaCountryId($mddaCountryId)
    {
        $this->mdda_country_id = $mddaCountryId;
    
        return $this;
    }

    /**
     * Get mdda_country_id
     *
     * @return integer 
     */
    public function getMddaCountryId()
    {
        return $this->mdda_country_id;
    }

    /**
     * Set mba_address
     *
     * @param string $mbaAddress
     * @return User
     */
    public function setMbaAddress($mbaAddress)
    {
        $this->mba_address = $mbaAddress;
    
        return $this;
    }

    /**
     * Get mba_address
     *
     * @return string 
     */
    public function getMbaAddress()
    {
        return $this->mba_address;
    }

    /**
     * Set mba_city
     *
     * @param string $mbaCity
     * @return User
     */
    public function setMbaCity($mbaCity)
    {
        $this->mba_city = $mbaCity;
    
        return $this;
    }

    /**
     * Get mba_city
     *
     * @return string 
     */
    public function getMbaCity()
    {
        return $this->mba_city;
    }

    /**
     * Set mba_state
     *
     * @param string $mbaState
     * @return User
     */
    public function setMbaState($mbaState)
    {
        $this->mba_state = $mbaState;
    
        return $this;
    }

    /**
     * Get mba_state
     *
     * @return string 
     */
    public function getMbaState()
    {
        return $this->mba_state;
    }

    /**
     * Set mba_postcode
     *
     * @param string $mbaPostcode
     * @return User
     */
    public function setMbaPostcode($mbaPostcode)
    {
        $this->mba_postcode = $mbaPostcode;
    
        return $this;
    }

    /**
     * Get mba_postcode
     *
     * @return string 
     */
    public function getMbaPostcode()
    {
        return $this->mba_postcode;
    }

    /**
     * Set mba_country_id
     *
     * @param integer $mbaCountryId
     * @return User
     */
    public function setMbaCountryId($mbaCountryId)
    {
        $this->mba_country_id = $mbaCountryId;
    
        return $this;
    }

    /**
     * Get mba_country_id
     *
     * @return integer 
     */
    public function getMbaCountryId()
    {
        return $this->mba_country_id;
    }

    /**
     * Set can_charge_cost_to_account
     *
     * @param integer $canChargeCostToAccount
     * @return User
     */
    public function setCanChargeCostToAccount($canChargeCostToAccount)
    {
        $this->can_charge_cost_to_account = $canChargeCostToAccount;
    
        return $this;
    }

    /**
     * Get can_charge_cost_to_account
     *
     * @return integer 
     */
    public function getCanChargeCostToAccount()
    {
        return $this->can_charge_cost_to_account;
    }

    /**
     * Set account_no
     *
     * @param string $accountNo
     * @return User
     */
    public function setAccountNo($accountNo)
    {
        $this->account_no = $accountNo;
    
        return $this;
    }

    /**
     * Get account_no
     *
     * @return string 
     */
    public function getAccountNo()
    {
        return $this->account_no;
    }

    /**
     * Set can_get_special_price
     *
     * @param integer $canGetSpecialPrice
     * @return User
     */
    public function setCanGetSpecialPrice($canGetSpecialPrice)
    {
        $this->can_get_special_price = $canGetSpecialPrice;
    
        return $this;
    }

    /**
     * Get can_get_special_price
     *
     * @return integer 
     */
    public function getCanGetSpecialPrice()
    {
        return $this->can_get_special_price;
    }

    /**
     * Set special_price
     *
     * @param integer $specialPrice
     * @return User
     */
    public function setSpecialPrice($specialPrice)
    {
        $this->special_price = $specialPrice;
    
        return $this;
    }

    /**
     * Get special_price
     *
     * @return integer 
     */
    public function getSpecialPrice()
    {
        return $this->special_price;
    }

    /**
     * Set reset_pin
     *
     * @param string $resetPin
     * @return User
     */
    public function setResetPin($resetPin)
    {
        $this->reset_pin = $resetPin;
    
        return $this;
    }

    /**
     * Get reset_pin
     *
     * @return string 
     */
    public function getResetPin()
    {
        return $this->reset_pin;
    }

    /**
     * Set s_enabled
     *
     * @param integer $sEnabled
     * @return User
     */
    public function setSEnabled($sEnabled)
    {
        $this->s_enabled = $sEnabled;
    
        return $this;
    }

    /**
     * Get s_enabled
     *
     * @return integer 
     */
    public function getSEnabled()
    {
        return $this->s_enabled;
    }
}