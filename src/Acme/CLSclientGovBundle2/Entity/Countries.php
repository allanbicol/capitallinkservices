<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Countries
 *
 * @ORM\Table(name="tbl_countries")
 * @ORM\Entity
 * @UniqueEntity(
 *      fields={"countryCode"},
 *      message="Country code is already used"
 * )
 */
class Countries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="country_code", type="string", length=5)
     */
    private $countryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=255)
     */
    private $countryName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="country_name_display", type="string", length=255)
     */
    private $country_name_display;

    /**
     * @var string
     *
     * @ORM\Column(name="rep_name", type="string", length=255)
     */
    private $rep_name;

    /**
     * @var string
     *
     * @ORM\Column(name="visa_information", type="string")
     */
    private $visa_information;
    
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="main_display", type="integer")
     */
    private $main_display;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_popular_destination", type="integer")
     */
    private $s_popular_destination;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="public_s_no_visa_required", type="integer")
     */
    private $public_s_no_visa_required;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="public_s_no_visa_required_html", type="string")
     */
    private $public_s_no_visa_required_html;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="gov_s_no_visa_required", type="integer")
     */
    private $gov_s_no_visa_required;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="gov_s_no_visa_required_html", type="string")
     */
    private $gov_s_no_visa_required_html;
    
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return Countries
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    
        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     * @return Countries
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
    
        return $this;
    }

    /**
     * Get countryName
     *
     * @return string 
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * Set rep_name
     *
     * @param string $repName
     * @return Countries
     */
    public function setRepName($repName)
    {
        $this->rep_name = $repName;
    
        return $this;
    }

    /**
     * Get rep_name
     *
     * @return string 
     */
    public function getRepName()
    {
        return $this->rep_name;
    }

    /**
     * Set visa_information
     *
     * @param string $visaInformation
     * @return Countries
     */
    public function setVisaInformation($visaInformation)
    {
        $this->visa_information = $visaInformation;
    
        return $this;
    }

    /**
     * Get visa_information
     *
     * @return string 
     */
    public function getVisaInformation()
    {
        return $this->visa_information;
    }

    /**
     * Set main_display
     *
     * @param integer $mainDisplay
     * @return Countries
     */
    public function setMainDisplay($mainDisplay)
    {
        $this->main_display = $mainDisplay;
    
        return $this;
    }

    /**
     * Get main_display
     *
     * @return integer 
     */
    public function getMainDisplay()
    {
        return $this->main_display;
    }

    /**
     * Set s_popular_destination
     *
     * @param integer $sPopularDestination
     * @return Countries
     */
    public function setSPopularDestination($sPopularDestination)
    {
        $this->s_popular_destination = $sPopularDestination;
    
        return $this;
    }

    /**
     * Get s_popular_destination
     *
     * @return integer 
     */
    public function getSPopularDestination()
    {
        return $this->s_popular_destination;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return Countries
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    
        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set public_s_no_visa_required
     *
     * @param integer $publicSNoVisaRequired
     * @return Countries
     */
    public function setPublicSNoVisaRequired($publicSNoVisaRequired)
    {
        $this->public_s_no_visa_required = $publicSNoVisaRequired;
    
        return $this;
    }

    /**
     * Get public_s_no_visa_required
     *
     * @return integer 
     */
    public function getPublicSNoVisaRequired()
    {
        return $this->public_s_no_visa_required;
    }

    /**
     * Set public_s_no_visa_required_html
     *
     * @param string $publicSNoVisaRequiredHtml
     * @return Countries
     */
    public function setPublicSNoVisaRequiredHtml($publicSNoVisaRequiredHtml)
    {
        $this->public_s_no_visa_required_html = $publicSNoVisaRequiredHtml;
    
        return $this;
    }

    /**
     * Get public_s_no_visa_required_html
     *
     * @return string 
     */
    public function getPublicSNoVisaRequiredHtml()
    {
        return $this->public_s_no_visa_required_html;
    }

    /**
     * Set gov_s_no_visa_required
     *
     * @param integer $govSNoVisaRequired
     * @return Countries
     */
    public function setGovSNoVisaRequired($govSNoVisaRequired)
    {
        $this->gov_s_no_visa_required = $govSNoVisaRequired;
    
        return $this;
    }

    /**
     * Get gov_s_no_visa_required
     *
     * @return integer 
     */
    public function getGovSNoVisaRequired()
    {
        return $this->gov_s_no_visa_required;
    }

    /**
     * Set gov_s_no_visa_required_html
     *
     * @param string $govSNoVisaRequiredHtml
     * @return Countries
     */
    public function setGovSNoVisaRequiredHtml($govSNoVisaRequiredHtml)
    {
        $this->gov_s_no_visa_required_html = $govSNoVisaRequiredHtml;
    
        return $this;
    }

    /**
     * Get gov_s_no_visa_required_html
     *
     * @return string 
     */
    public function getGovSNoVisaRequiredHtml()
    {
        return $this->gov_s_no_visa_required_html;
    }

    /**
     * Set country_name_display
     *
     * @param string $countryNameDisplay
     * @return Countries
     */
    public function setCountryNameDisplay($countryNameDisplay)
    {
        $this->country_name_display = $countryNameDisplay;
    
        return $this;
    }

    /**
     * Get country_name_display
     *
     * @return string 
     */
    public function getCountryNameDisplay()
    {
        return $this->country_name_display;
    }
}