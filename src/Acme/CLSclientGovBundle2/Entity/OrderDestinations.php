<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderDestinations
 *
 * @ORM\Table(name="tbl_order_destinations")
 * @ORM\Entity
 */
class OrderDestinations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_no", type="integer")
     */
    private $order_no;

    /**
     * @var integer
     *
     * @ORM\Column(name="country_id", type="integer")
     */
    private $country_id;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_date", type="string", length=100)
     */
    private $departure_date;

    /**
     * @var integer
     *
     * @ORM\Column(name="entry_option", type="integer")
     */
    private $entry_option;

    /**
     * @var string
     *
     * @ORM\Column(name="entry_date_country", type="string", length=100)
     */
    private $entry_date_country;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_date_country", type="string", length=100)
     */
    private $departure_date_country;
    
    /**
     * @var string
     *
     * @ORM\Column(name="travel_purpose", type="string")
     */
    private $travel_purpose;


    /**
     * @var integer
     *
     * @ORM\Column(name="s_primary", type="integer")
     */
    private $s_primary;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="selected_visa_type", type="integer")
     */
    private $selected_visa_type;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="selected_visa_type_price", type="float")
     */
    private $selected_visa_type_price;
    
    /**
     * @var string
     *
     * @ORM\Column(name="selected_visa_type_requirements", type="string", length=1000)
     */
    private $selected_visa_type_requirements;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_date_cls_received_all_items", type="string", length=100)
     */
    private $visa_date_cls_received_all_items;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_date_submitted_for_processing", type="string", length=100)
     */
    private $visa_date_submitted_for_processing;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_date_completed_and_received_at_cls", type="string", length=100)
     */
    private $visa_date_completed_and_received_at_cls;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_date_order_on_route_and_closed", type="string", length=100)
     */
    private $visa_date_order_on_route_and_closed;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_shipped_by", type="string", length=1000)
     */
    private $visa_shipped_by;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_com_note_no", type="string", length=1000)
     */
    private $visa_com_note_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_com_note_in", type="string", length=1000)
     */
    private $visa_com_note_in;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="visa_invoice_no", type="string", length=1000)
     */
    private $visa_invoice_no;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="tpn_stat", type="integer")
     */
    private $tpn_stat;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tpn_middle_src", type="string")
     */
    private $tpn_middle_src;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tpn_date_issued", type="string")
     */
    private $tpn_date_issued;

    /**
     * @var string
     *
     * @ORM\Column(name="visa_follow_up_date", type="string")
     */
    private $visa_follow_up_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="signature", type="string")
     */
    private $signature;
    
    /**
     * @var string
     *
     * @ORM\Column(name="sig_hash", type="string")
     */
    private $sig_hash;

       /**
     * @var string
     *
     * @ORM\Column(name="sig_name", type="string")
     */
    private $sig_name;

     /**
     * @var string
     *
     * @ORM\Column(name="dhl_airwaybill_number", type="string")
     */
    private $dhl_airwaybill_number;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order_no
     *
     * @param integer $orderNo
     * @return OrderDestinations
     */
    public function setOrderNo($orderNo)
    {
        $this->order_no = $orderNo;
    
        return $this;
    }

    /**
     * Get order_no
     *
     * @return integer 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return OrderDestinations
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;
    
        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set departure_date
     *
     * @param string $departureDate
     * @return OrderDestinations
     */
    public function setDepartureDate($departureDate)
    {
        $this->departure_date = $departureDate;
    
        return $this;
    }

    /**
     * Get departure_date
     *
     * @return string 
     */
    public function getDepartureDate()
    {
        return $this->departure_date;
    }

    /**
     * Set entry_option
     *
     * @param integer $entryOption
     * @return OrderDestinations
     */
    public function setEntryOption($entryOption)
    {
        $this->entry_option = $entryOption;
    
        return $this;
    }

    /**
     * Get entry_option
     *
     * @return integer 
     */
    public function getEntryOption()
    {
        return $this->entry_option;
    }

    /**
     * Set entry_date_country
     *
     * @param string $entryDateCountry
     * @return OrderDestinations
     */
    public function setEntryDateCountry($entryDateCountry)
    {
        $this->entry_date_country = $entryDateCountry;
    
        return $this;
    }

    /**
     * Get entry_date_country
     *
     * @return string 
     */
    public function getEntryDateCountry()
    {
        return $this->entry_date_country;
    }

    /**
     * Set departure_date_country
     *
     * @param string $departureDateCountry
     * @return OrderDestinations
     */
    public function setDepartureDateCountry($departureDateCountry)
    {
        $this->departure_date_country = $departureDateCountry;
    
        return $this;
    }

    /**
     * Get departure_date_country
     *
     * @return string 
     */
    public function getDepartureDateCountry()
    {
        return $this->departure_date_country;
    }

    /**
     * Set travel_purpose
     *
     * @param string $travelPurpose
     * @return OrderDestinations
     */
    public function setTravelPurpose($travelPurpose)
    {
        $this->travel_purpose = $travelPurpose;
    
        return $this;
    }

    /**
     * Get travel_purpose
     *
     * @return string 
     */
    public function getTravelPurpose()
    {
        return $this->travel_purpose;
    }

    /**
     * Set s_primary
     *
     * @param integer $sPrimary
     * @return OrderDestinations
     */
    public function setSPrimary($sPrimary)
    {
        $this->s_primary = $sPrimary;
    
        return $this;
    }

    /**
     * Get s_primary
     *
     * @return integer 
     */
    public function getSPrimary()
    {
        return $this->s_primary;
    }

    /**
     * Set selected_visa_type
     *
     * @param integer $selectedVisaType
     * @return OrderDestinations
     */
    public function setSelectedVisaType($selectedVisaType)
    {
        $this->selected_visa_type = $selectedVisaType;
    
        return $this;
    }

    /**
     * Get selected_visa_type
     *
     * @return integer 
     */
    public function getSelectedVisaType()
    {
        return $this->selected_visa_type;
    }

    /**
     * Set selected_visa_type_price
     *
     * @param float $selectedVisaTypePrice
     * @return OrderDestinations
     */
    public function setSelectedVisaTypePrice($selectedVisaTypePrice)
    {
        $this->selected_visa_type_price = $selectedVisaTypePrice;
    
        return $this;
    }

    /**
     * Get selected_visa_type_price
     *
     * @return float 
     */
    public function getSelectedVisaTypePrice()
    {
        return $this->selected_visa_type_price;
    }

    /**
     * Set selected_visa_type_requirements
     *
     * @param string $selectedVisaTypeRequirements
     * @return OrderDestinations
     */
    public function setSelectedVisaTypeRequirements($selectedVisaTypeRequirements)
    {
        $this->selected_visa_type_requirements = $selectedVisaTypeRequirements;
    
        return $this;
    }

    /**
     * Get selected_visa_type_requirements
     *
     * @return string 
     */
    public function getSelectedVisaTypeRequirements()
    {
        return $this->selected_visa_type_requirements;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return OrderDestinations
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set visa_date_cls_received_all_items
     *
     * @param string $visaDateClsReceivedAllItems
     * @return OrderDestinations
     */
    public function setVisaDateClsReceivedAllItems($visaDateClsReceivedAllItems)
    {
        $this->visa_date_cls_received_all_items = $visaDateClsReceivedAllItems;
    
        return $this;
    }

    /**
     * Get visa_date_cls_received_all_items
     *
     * @return string 
     */
    public function getVisaDateClsReceivedAllItems()
    {
        return $this->visa_date_cls_received_all_items;
    }

    /**
     * Set visa_date_submitted_for_processing
     *
     * @param string $visaDateSubmittedForProcessing
     * @return OrderDestinations
     */
    public function setVisaDateSubmittedForProcessing($visaDateSubmittedForProcessing)
    {
        $this->visa_date_submitted_for_processing = $visaDateSubmittedForProcessing;
    
        return $this;
    }

    /**
     * Get visa_date_submitted_for_processing
     *
     * @return string 
     */
    public function getVisaDateSubmittedForProcessing()
    {
        return $this->visa_date_submitted_for_processing;
    }

    /**
     * Set visa_date_completed_and_received_at_cls
     *
     * @param string $visaDateCompletedAndReceivedAtCls
     * @return OrderDestinations
     */
    public function setVisaDateCompletedAndReceivedAtCls($visaDateCompletedAndReceivedAtCls)
    {
        $this->visa_date_completed_and_received_at_cls = $visaDateCompletedAndReceivedAtCls;
    
        return $this;
    }

    /**
     * Get visa_date_completed_and_received_at_cls
     *
     * @return string 
     */
    public function getVisaDateCompletedAndReceivedAtCls()
    {
        return $this->visa_date_completed_and_received_at_cls;
    }

    /**
     * Set visa_date_order_on_route_and_closed
     *
     * @param string $visaDateOrderOnRouteAndClosed
     * @return OrderDestinations
     */
    public function setVisaDateOrderOnRouteAndClosed($visaDateOrderOnRouteAndClosed)
    {
        $this->visa_date_order_on_route_and_closed = $visaDateOrderOnRouteAndClosed;
    
        return $this;
    }

    /**
     * Get visa_date_order_on_route_and_closed
     *
     * @return string 
     */
    public function getVisaDateOrderOnRouteAndClosed()
    {
        return $this->visa_date_order_on_route_and_closed;
    }

    /**
     * Set visa_shipped_by
     *
     * @param string $visaShippedBy
     * @return OrderDestinations
     */
    public function setVisaShippedBy($visaShippedBy)
    {
        $this->visa_shipped_by = $visaShippedBy;
    
        return $this;
    }

    /**
     * Get visa_shipped_by
     *
     * @return string 
     */
    public function getVisaShippedBy()
    {
        return $this->visa_shipped_by;
    }

    /**
     * Set visa_com_note_no
     *
     * @param string $visaComNoteNo
     * @return OrderDestinations
     */
    public function setVisaComNoteNo($visaComNoteNo)
    {
        $this->visa_com_note_no = $visaComNoteNo;
    
        return $this;
    }

    /**
     * Get visa_com_note_no
     *
     * @return string 
     */
    public function getVisaComNoteNo()
    {
        return $this->visa_com_note_no;
    }

    /**
     * Set visa_com_note_in
     *
     * @param string $visaComNoteIn
     * @return OrderDestinations
     */
    public function setVisaComNoteIn($visaComNoteIn)
    {
        $this->visa_com_note_in = $visaComNoteIn;
    
        return $this;
    }

    /**
     * Get visa_com_note_in
     *
     * @return string 
     */
    public function getVisaComNoteIn()
    {
        return $this->visa_com_note_in;
    }

    /**
     * Set visa_invoice_no
     *
     * @param string $visaInvoiceNo
     * @return OrderDestinations
     */
    public function setVisaInvoiceNo($visaInvoiceNo)
    {
        $this->visa_invoice_no = $visaInvoiceNo;
    
        return $this;
    }

    /**
     * Get visa_invoice_no
     *
     * @return string 
     */
    public function getVisaInvoiceNo()
    {
        return $this->visa_invoice_no;
    }

    /**
     * Set tpn_stat
     *
     * @param integer $tpnStat
     * @return OrderDestinations
     */
    public function setTpnStat($tpnStat)
    {
        $this->tpn_stat = $tpnStat;
    
        return $this;
    }

    /**
     * Get tpn_stat
     *
     * @return integer 
     */
    public function getTpnStat()
    {
        return $this->tpn_stat;
    }

    /**
     * Set tpn_middle_src
     *
     * @param string $tpnMiddleSrc
     * @return OrderDestinations
     */
    public function setTpnMiddleSrc($tpnMiddleSrc)
    {
        $this->tpn_middle_src = $tpnMiddleSrc;
    
        return $this;
    }

    /**
     * Get tpn_middle_src
     *
     * @return string 
     */
    public function getTpnMiddleSrc()
    {
        return $this->tpn_middle_src;
    }

    /**
     * Set tpn_date_issued
     *
     * @param string $tpnDateIssued
     * @return OrderDestinations
     */
    public function setTpnDateIssued($tpnDateIssued)
    {
        $this->tpn_date_issued = $tpnDateIssued;
    
        return $this;
    }

    /**
     * Get tpn_date_issued
     *
     * @return string 
     */
    public function getTpnDateIssued()
    {
        return $this->tpn_date_issued;
    }

    /**
     * Set visa_follow_up_date
     *
     * @param string $visaFollowUpDate
     * @return OrderDestinations
     */
    public function setVisaFollowUpDate($visaFollowUpDate)
    {
        $this->visa_follow_up_date = $visaFollowUpDate;
    
        return $this;
    }

    /**
     * Get visa_follow_up_date
     *
     * @return string 
     */
    public function getVisaFollowUpDate()
    {
        return $this->visa_follow_up_date;
    }

    /**
     * Set signature
     *
     * @param string $signature
     * @return OrderDestinations
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    
        return $this;
    }

    /**
     * Get signature
     *
     * @return string 
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set sig_hash
     *
     * @param string $sigHash
     * @return OrderDestinations
     */
    public function setSigHash($sigHash)
    {
        $this->sig_hash = $sigHash;
    
        return $this;
    }

    /**
     * Get sig_hash
     *
     * @return string 
     */
    public function getSigHash()
    {
        return $this->sig_hash;
    }

    /**
     * Set sig_name
     *
     * @param string $sigName
     * @return OrderDestinations
     */
    public function setSigName($sigName)
    {
        $this->sig_name = $sigName;
    
        return $this;
    }

    /**
     * Get sig_name
     *
     * @return string 
     */
    public function getSigName()
    {
        return $this->sig_name;
    }

    /**
     * Set dhl_airwaybill_number
     *
     * @param string $dhlAirwaybillNumber
     * @return OrderDestinations
     */
    public function setDhlAirwaybillNumber($dhlAirwaybillNumber)
    {
        $this->dhl_airwaybill_number = $dhlAirwaybillNumber;
    
        return $this;
    }

    /**
     * Get dhl_airwaybill_number
     *
     * @return string 
     */
    public function getDhlAirwaybillNumber()
    {
        return $this->dhl_airwaybill_number;
    }
}