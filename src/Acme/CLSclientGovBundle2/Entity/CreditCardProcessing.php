<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CreditCardProcessing
 *
 * @ORM\Table(name="tbl_credit_card_processing")
 * @ORM\Entity
 * 
 */
class CreditCardProcessing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     * @Assert\NotBlank(message="Credit Card Processing Fee should not be blank.")
     * @Assert\Type(type="numeric", message="Credit Card Processing Fee should be of type numeric.")
     * @ORM\Column(name="fee", type="float")
     */
    private $fee;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fee
     *
     * @param float $fee
     * @return CreditCardProcessing
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
    
        return $this;
    }

    /**
     * Get fee
     *
     * @return float 
     */
    public function getFee()
    {
        return $this->fee;
    }
}