<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Departments
 *
 * @ORM\Table(name="tbl_departments")
 * @ORM\Entity
 */
class Departments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Department Code should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "20",
     *      minMessage = "Department Code must be at least {{ limit }} characters length",
     *      maxMessage = "Department Code cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="code", type="string", length=20)
     */
    private $code;

    /**
     * @var string
     * @Assert\NotBlank(message="Department Name should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Department Name must be at least {{ limit }} characters length",
     *      maxMessage = "Department Name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Departments
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Departments
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}