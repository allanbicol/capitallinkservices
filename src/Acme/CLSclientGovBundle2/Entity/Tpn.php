<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tpn
 *
 * @ORM\Table(name="tbl_tpn")
 * @ORM\Entity
 */
class Tpn
{
    /**
     * @var string
     *
     * @ORM\Column(name="tpn_no", type="string")
     * @ORM\Id
     */
    private $tpn_no;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_no", type="integer")
     */
    private $order_no;

    /**
     * @var integer
     *
     * @ORM\Column(name="client_id", type="integer")
     */
    private $client_id;

    /**
     * @var string
     *
     * @ORM\Column(name="tpn_src", type="string")
     */
    private $tpn_src;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tpn_src_original_approved", type="string")
     */
    private $tpn_src_original_approved;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tpn_src_previous", type="string")
     */
    private $tpn_src_previous;


    /**
     * @var string
     *
     * @ORM\Column(name="date_submitted", type="string")
     */
    private $date_submitted;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_last_updated", type="string")
     */
    private $date_last_updated;

    /**
     * @var string
     *
     * @ORM\Column(name="date_issued", type="string")
     */
    private $date_issued;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="destination", type="integer")
     */
    private $destination;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="departure_date", type="string")
     */
    private $departure_date;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="entry_option", type="integer")
     */
    private $entry_option;

    /**
     * @var string
     *
     * @ORM\Column(name="entry_date_country", type="string", length=100)
     */
    private $entry_date_country;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_date_country", type="string", length=100)
     */
    private $departure_date_country;
    
    /**
     * @var string
     *
     * @ORM\Column(name="travel_purpose", type="string")
     */
    private $travel_purpose;
    
    

    

    /**
     * Set tpn_no
     *
     * @param string $tpnNo
     * @return Tpn
     */
    public function setTpnNo($tpnNo)
    {
        $this->tpn_no = $tpnNo;
    
        return $this;
    }

    /**
     * Get tpn_no
     *
     * @return string 
     */
    public function getTpnNo()
    {
        return $this->tpn_no;
    }

    /**
     * Set order_no
     *
     * @param integer $orderNo
     * @return Tpn
     */
    public function setOrderNo($orderNo)
    {
        $this->order_no = $orderNo;
    
        return $this;
    }

    /**
     * Get order_no
     *
     * @return integer 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set client_id
     *
     * @param integer $clientId
     * @return Tpn
     */
    public function setClientId($clientId)
    {
        $this->client_id = $clientId;
    
        return $this;
    }

    /**
     * Get client_id
     *
     * @return integer 
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * Set tpn_src
     *
     * @param string $tpnSrc
     * @return Tpn
     */
    public function setTpnSrc($tpnSrc)
    {
        $this->tpn_src = $tpnSrc;
    
        return $this;
    }

    /**
     * Get tpn_src
     *
     * @return string 
     */
    public function getTpnSrc()
    {
        return $this->tpn_src;
    }

    /**
     * Set tpn_src_original_approved
     *
     * @param string $tpnSrcOriginalApproved
     * @return Tpn
     */
    public function setTpnSrcOriginalApproved($tpnSrcOriginalApproved)
    {
        $this->tpn_src_original_approved = $tpnSrcOriginalApproved;
    
        return $this;
    }

    /**
     * Get tpn_src_original_approved
     *
     * @return string 
     */
    public function getTpnSrcOriginalApproved()
    {
        return $this->tpn_src_original_approved;
    }

    /**
     * Set tpn_src_previous
     *
     * @param string $tpnSrcPrevious
     * @return Tpn
     */
    public function setTpnSrcPrevious($tpnSrcPrevious)
    {
        $this->tpn_src_previous = $tpnSrcPrevious;
    
        return $this;
    }

    /**
     * Get tpn_src_previous
     *
     * @return string 
     */
    public function getTpnSrcPrevious()
    {
        return $this->tpn_src_previous;
    }

    /**
     * Set date_submitted
     *
     * @param string $dateSubmitted
     * @return Tpn
     */
    public function setDateSubmitted($dateSubmitted)
    {
        $this->date_submitted = $dateSubmitted;
    
        return $this;
    }

    /**
     * Get date_submitted
     *
     * @return string 
     */
    public function getDateSubmitted()
    {
        return $this->date_submitted;
    }

    /**
     * Set date_last_updated
     *
     * @param string $dateLastUpdated
     * @return Tpn
     */
    public function setDateLastUpdated($dateLastUpdated)
    {
        $this->date_last_updated = $dateLastUpdated;
    
        return $this;
    }

    /**
     * Get date_last_updated
     *
     * @return string 
     */
    public function getDateLastUpdated()
    {
        return $this->date_last_updated;
    }

    /**
     * Set date_issued
     *
     * @param string $dateIssued
     * @return Tpn
     */
    public function setDateIssued($dateIssued)
    {
        $this->date_issued = $dateIssued;
    
        return $this;
    }

    /**
     * Get date_issued
     *
     * @return string 
     */
    public function getDateIssued()
    {
        return $this->date_issued;
    }

    /**
     * Set destination
     *
     * @param integer $destination
     * @return Tpn
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    
        return $this;
    }

    /**
     * Get destination
     *
     * @return integer 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set departure_date
     *
     * @param string $departureDate
     * @return Tpn
     */
    public function setDepartureDate($departureDate)
    {
        $this->departure_date = $departureDate;
    
        return $this;
    }

    /**
     * Get departure_date
     *
     * @return string 
     */
    public function getDepartureDate()
    {
        return $this->departure_date;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Tpn
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set entry_option
     *
     * @param integer $entryOption
     * @return Tpn
     */
    public function setEntryOption($entryOption)
    {
        $this->entry_option = $entryOption;
    
        return $this;
    }

    /**
     * Get entry_option
     *
     * @return integer 
     */
    public function getEntryOption()
    {
        return $this->entry_option;
    }

    /**
     * Set entry_date_country
     *
     * @param string $entryDateCountry
     * @return Tpn
     */
    public function setEntryDateCountry($entryDateCountry)
    {
        $this->entry_date_country = $entryDateCountry;
    
        return $this;
    }

    /**
     * Get entry_date_country
     *
     * @return string 
     */
    public function getEntryDateCountry()
    {
        return $this->entry_date_country;
    }

    /**
     * Set departure_date_country
     *
     * @param string $departureDateCountry
     * @return Tpn
     */
    public function setDepartureDateCountry($departureDateCountry)
    {
        $this->departure_date_country = $departureDateCountry;
    
        return $this;
    }

    /**
     * Get departure_date_country
     *
     * @return string 
     */
    public function getDepartureDateCountry()
    {
        return $this->departure_date_country;
    }

    /**
     * Set travel_purpose
     *
     * @param string $travelPurpose
     * @return Tpn
     */
    public function setTravelPurpose($travelPurpose)
    {
        $this->travel_purpose = $travelPurpose;
    
        return $this;
    }

    /**
     * Get travel_purpose
     *
     * @return string 
     */
    public function getTravelPurpose()
    {
        return $this->travel_purpose;
    }
}