<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SettingsTPN
 *
 * @ORM\Table(name="tbl_settings_tpn")
 * @ORM\Entity
 */
class SettingsTPN
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     * @Assert\NotBlank(message="Third Person Note (TPN) should not be blank.")
     * @Assert\Type(type="numeric", message="Third Person Note (TPN) should be of type numeric.")
     * @ORM\Column(name="tpn", type="float")
     */
    private $tpn;

    /**
     * @var float
     * @Assert\NotBlank(message="Third Person Note (TPN) Additional Applicant should not be blank.")
     * @Assert\Type(type="numeric", message="Third Person Note (TPN) Additional Applicant should be of type numeric.")
     * @ORM\Column(name="tpn_additional", type="float")
     */
    private $tpnAdditional;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tpn
     *
     * @param float $tpn
     * @return SettingsTPN
     */
    public function setTpn($tpn)
    {
        $this->tpn = $tpn;
    
        return $this;
    }

    /**
     * Get tpn
     *
     * @return float 
     */
    public function getTpn()
    {
        return $this->tpn;
    }

    /**
     * Set tpnAdditional
     *
     * @param float $tpnAdditional
     * @return SettingsTPN
     */
    public function setTpnAdditional($tpnAdditional)
    {
        $this->tpnAdditional = $tpnAdditional;
    
        return $this;
    }

    /**
     * Get tpnAdditional
     *
     * @return float 
     */
    public function getTpnAdditional()
    {
        return $this->tpnAdditional;
    }
}