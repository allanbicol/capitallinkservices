<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment
 *
 * @ORM\Table(name="tbl_payment")
 * @ORM\Entity
 */
class Payment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_no", type="integer")
     */
    private $order_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date_paid", type="string", length=100)
     */
    private $date_paid;

    /**
     * @var string
     *
     * @ORM\Column(name="fname", type="string", length=225)
     */
    private $fname;

    /**
     * @var string
     *
     * @ORM\Column(name="lname", type="string", length=225)
     */
    private $lname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=225)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=50)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=5000)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=225)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=100)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=50)
     */
    private $postcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="country_id", type="integer")
     */
    private $country_id;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_address_details", type="string")
     */
    private $additional_address_details;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="mba_organisation_name", type="string", length=1000)
     */
    private $mba_organisation_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="mba_fname", type="string", length=225)
     */
    private $mba_fname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="mba_lname", type="string", length=225)
     */
    private $mba_lname;

    /**
     * @var string
     *
     * @ORM\Column(name="mba_address", type="string", length=5000)
     */
    private $mba_address;

    /**
     * @var string
     *
     * @ORM\Column(name="mba_city", type="string", length=225)
     */
    private $mba_city;

    /**
     * @var string
     *
     * @ORM\Column(name="mba_state", type="string", length=100)
     */
    private $mba_state;

    /**
     * @var string
     *
     * @ORM\Column(name="mba_postcode", type="string", length=50)
     */
    private $mba_postcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="mba_country_id", type="integer")
     */
    private $mba_country_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_option", type="integer")
     */
    private $payment_option;

    /**
     * @var string
     *
     * @ORM\Column(name="account_no", type="string", length=50)
     */
    private $account_no;

    /**
     * @var string
     *
     * @ORM\Column(name="name_on_card", type="string", length=1000)
     */
    private $name_on_card;

    /**
     * @var string
     *
     * @ORM\Column(name="card_number", type="string", length=50)
     */
    private $card_number;

    /**
     * @var integer
     *
     * @ORM\Column(name="card_expiry_month", type="integer")
     */
    private $card_expiry_month;

    /**
     * @var integer
     *
     * @ORM\Column(name="card_expiry_year", type="integer")
     */
    private $card_expiry_year;

    /**
     * @var integer
     *
     * @ORM\Column(name="card_type", type="integer")
     */
    private $card_type;

    /**
     * @var string
     *
     * @ORM\Column(name="ccv_number", type="string", length=50)
     */
    private $ccv_number;


    /**
     * @var float
     *
     * @ORM\Column(name="total_order_price", type="float")
     */
    private $total_order_price;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="client_id", type="integer")
     */
    private $client_id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="department_id", type="integer")
     */
    private $department_id;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_paid", type="integer")
     */
    private $s_paid;

    
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_receiver_name", type="string", length=225)
     */
    private $doc_receiver_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_address", type="string", length=1000)
     */
    private $doc_pickup_address;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_city", type="string", length=1000)
     */
    private $doc_pickup_city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_postcode", type="string", length=100)
     */
    private $doc_pickup_postcode;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_pickup_contact_no", type="string", length=100)
     */
    private $doc_pickup_contact_no;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_recipient_name", type="string", length=225)
     */
    private $doc_delivery_recipient_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_address", type="string", length=1000)
     */
    private $doc_delivery_address;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_city", type="string", length=1000)
     */
    private $doc_delivery_city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_postcode", type="string", length=100)
     */
    private $doc_delivery_postcode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_delivery_contact_no", type="string", length=100)
     */
    private $doc_delivery_contact_no;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="doc_package_total_pieces", type="integer")
     */
    private $doc_package_total_pieces;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_pickup_date", type="string", length=20)
     */
    private $doc_package_pickup_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_ready_hr", type="string", length=2)
     */
    private $doc_package_ready_hr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_ready_min", type="string", length=2)
     */
    private $doc_package_ready_min;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_office_close_hr", type="string", length=2)
     */
    private $doc_package_office_close_hr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="doc_package_office_close_min", type="string", length=2)
     */
    private $doc_package_office_close_min;
    
    
    
    
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order_no
     *
     * @param integer $orderNo
     * @return Payment
     */
    public function setOrderNo($orderNo)
    {
        $this->order_no = $orderNo;
    
        return $this;
    }

    /**
     * Get order_no
     *
     * @return integer 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return Payment
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return Payment
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Payment
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Payment
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return Payment
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    
        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Payment
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Payment
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Payment
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Payment
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return Payment
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;
    
        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set additional_address_details
     *
     * @param string $additionalAddressDetails
     * @return Payment
     */
    public function setAdditionalAddressDetails($additionalAddressDetails)
    {
        $this->additional_address_details = $additionalAddressDetails;
    
        return $this;
    }

    /**
     * Get additional_address_details
     *
     * @return string 
     */
    public function getAdditionalAddressDetails()
    {
        return $this->additional_address_details;
    }

    /**
     * Set mba_address
     *
     * @param string $mbaAddress
     * @return Payment
     */
    public function setMbaAddress($mbaAddress)
    {
        $this->mba_address = $mbaAddress;
    
        return $this;
    }

    /**
     * Get mba_address
     *
     * @return string 
     */
    public function getMbaAddress()
    {
        return $this->mba_address;
    }

    /**
     * Set mba_city
     *
     * @param string $mbaCity
     * @return Payment
     */
    public function setMbaCity($mbaCity)
    {
        $this->mba_city = $mbaCity;
    
        return $this;
    }

    /**
     * Get mba_city
     *
     * @return string 
     */
    public function getMbaCity()
    {
        return $this->mba_city;
    }

    /**
     * Set mba_state
     *
     * @param string $mbaState
     * @return Payment
     */
    public function setMbaState($mbaState)
    {
        $this->mba_state = $mbaState;
    
        return $this;
    }

    /**
     * Get mba_state
     *
     * @return string 
     */
    public function getMbaState()
    {
        return $this->mba_state;
    }

    /**
     * Set mba_postcode
     *
     * @param string $mbaPostcode
     * @return Payment
     */
    public function setMbaPostcode($mbaPostcode)
    {
        $this->mba_postcode = $mbaPostcode;
    
        return $this;
    }

    /**
     * Get mba_postcode
     *
     * @return string 
     */
    public function getMbaPostcode()
    {
        return $this->mba_postcode;
    }

    /**
     * Set mba_country_id
     *
     * @param integer $mbaCountryId
     * @return Payment
     */
    public function setMbaCountryId($mbaCountryId)
    {
        $this->mba_country_id = $mbaCountryId;
    
        return $this;
    }

    /**
     * Get mba_country_id
     *
     * @return integer 
     */
    public function getMbaCountryId()
    {
        return $this->mba_country_id;
    }

    /**
     * Set payment_option
     *
     * @param integer $paymentOption
     * @return Payment
     */
    public function setPaymentOption($paymentOption)
    {
        $this->payment_option = $paymentOption;
    
        return $this;
    }

    /**
     * Get payment_option
     *
     * @return integer 
     */
    public function getPaymentOption()
    {
        return $this->payment_option;
    }

    /**
     * Set account_no
     *
     * @param string $accountNo
     * @return Payment
     */
    public function setAccountNo($accountNo)
    {
        $this->account_no = $accountNo;
    
        return $this;
    }

    /**
     * Get account_no
     *
     * @return string 
     */
    public function getAccountNo()
    {
        return $this->account_no;
    }

    /**
     * Set name_on_card
     *
     * @param string $nameOnCard
     * @return Payment
     */
    public function setNameOnCard($nameOnCard)
    {
        $this->name_on_card = $nameOnCard;
    
        return $this;
    }

    /**
     * Get name_on_card
     *
     * @return string 
     */
    public function getNameOnCard()
    {
        return $this->name_on_card;
    }

    /**
     * Set card_number
     *
     * @param string $cardNumber
     * @return Payment
     */
    public function setCardNumber($cardNumber)
    {
        $this->card_number = $cardNumber;
    
        return $this;
    }

    /**
     * Get card_number
     *
     * @return string 
     */
    public function getCardNumber()
    {
        return $this->card_number;
    }

    /**
     * Set card_expiry_month
     *
     * @param integer $cardExpiryMonth
     * @return Payment
     */
    public function setCardExpiryMonth($cardExpiryMonth)
    {
        $this->card_expiry_month = $cardExpiryMonth;
    
        return $this;
    }

    /**
     * Get card_expiry_month
     *
     * @return integer 
     */
    public function getCardExpiryMonth()
    {
        return $this->card_expiry_month;
    }

    /**
     * Set card_expiry_year
     *
     * @param integer $cardExpiryYear
     * @return Payment
     */
    public function setCardExpiryYear($cardExpiryYear)
    {
        $this->card_expiry_year = $cardExpiryYear;
    
        return $this;
    }

    /**
     * Get card_expiry_year
     *
     * @return integer 
     */
    public function getCardExpiryYear()
    {
        return $this->card_expiry_year;
    }

    /**
     * Set card_type
     *
     * @param integer $cardType
     * @return Payment
     */
    public function setCardType($cardType)
    {
        $this->card_type = $cardType;
    
        return $this;
    }

    /**
     * Get card_type
     *
     * @return integer 
     */
    public function getCardType()
    {
        return $this->card_type;
    }

    /**
     * Set ccv_number
     *
     * @param string $ccvNumber
     * @return Payment
     */
    public function setCcvNumber($ccvNumber)
    {
        $this->ccv_number = $ccvNumber;
    
        return $this;
    }

    /**
     * Get ccv_number
     *
     * @return string 
     */
    public function getCcvNumber()
    {
        return $this->ccv_number;
    }

    /**
     * Set total_order_price
     *
     * @param float $totalOrderPrice
     * @return Payment
     */
    public function setTotalOrderPrice($totalOrderPrice)
    {
        $this->total_order_price = $totalOrderPrice;
    
        return $this;
    }

    /**
     * Get total_order_price
     *
     * @return float 
     */
    public function getTotalOrderPrice()
    {
        return $this->total_order_price;
    }

    /**
     * Set client_id
     *
     * @param integer $clientId
     * @return Payment
     */
    public function setClientId($clientId)
    {
        $this->client_id = $clientId;
    
        return $this;
    }

    /**
     * Get client_id
     *
     * @return integer 
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * Set department_id
     *
     * @param integer $departmentId
     * @return Payment
     */
    public function setDepartmentId($departmentId)
    {
        $this->department_id = $departmentId;
    
        return $this;
    }

    /**
     * Get department_id
     *
     * @return integer 
     */
    public function getDepartmentId()
    {
        return $this->department_id;
    }

    /**
     * Set date_paid
     *
     * @param string $datePaid
     * @return Payment
     */
    public function setDatePaid($datePaid)
    {
        $this->date_paid = $datePaid;
    
        return $this;
    }

    /**
     * Get date_paid
     *
     * @return string 
     */
    public function getDatePaid()
    {
        return $this->date_paid;
    }

    /**
     * Set s_paid
     *
     * @param integer $sPaid
     * @return Payment
     */
    public function setSPaid($sPaid)
    {
        $this->s_paid = $sPaid;
    
        return $this;
    }

    /**
     * Get s_paid
     *
     * @return integer 
     */
    public function getSPaid()
    {
        return $this->s_paid;
    }

    /**
     * Set doc_receiver_name
     *
     * @param string $docReceiverName
     * @return Payment
     */
    public function setDocReceiverName($docReceiverName)
    {
        $this->doc_receiver_name = $docReceiverName;
    
        return $this;
    }

    /**
     * Get doc_receiver_name
     *
     * @return string 
     */
    public function getDocReceiverName()
    {
        return $this->doc_receiver_name;
    }

    /**
     * Set doc_pickup_address
     *
     * @param string $docPickupAddress
     * @return Payment
     */
    public function setDocPickupAddress($docPickupAddress)
    {
        $this->doc_pickup_address = $docPickupAddress;
    
        return $this;
    }

    /**
     * Get doc_pickup_address
     *
     * @return string 
     */
    public function getDocPickupAddress()
    {
        return $this->doc_pickup_address;
    }

    /**
     * Set doc_pickup_city
     *
     * @param string $docPickupCity
     * @return Payment
     */
    public function setDocPickupCity($docPickupCity)
    {
        $this->doc_pickup_city = $docPickupCity;
    
        return $this;
    }

    /**
     * Get doc_pickup_city
     *
     * @return string 
     */
    public function getDocPickupCity()
    {
        return $this->doc_pickup_city;
    }

    /**
     * Set doc_pickup_postcode
     *
     * @param string $docPickupPostcode
     * @return Payment
     */
    public function setDocPickupPostcode($docPickupPostcode)
    {
        $this->doc_pickup_postcode = $docPickupPostcode;
    
        return $this;
    }

    /**
     * Get doc_pickup_postcode
     *
     * @return string 
     */
    public function getDocPickupPostcode()
    {
        return $this->doc_pickup_postcode;
    }

    /**
     * Set doc_pickup_contact_no
     *
     * @param string $docPickupContactNo
     * @return Payment
     */
    public function setDocPickupContactNo($docPickupContactNo)
    {
        $this->doc_pickup_contact_no = $docPickupContactNo;
    
        return $this;
    }

    /**
     * Get doc_pickup_contact_no
     *
     * @return string 
     */
    public function getDocPickupContactNo()
    {
        return $this->doc_pickup_contact_no;
    }

    /**
     * Set doc_delivery_recipient_name
     *
     * @param string $docDeliveryRecipientName
     * @return Payment
     */
    public function setDocDeliveryRecipientName($docDeliveryRecipientName)
    {
        $this->doc_delivery_recipient_name = $docDeliveryRecipientName;
    
        return $this;
    }

    /**
     * Get doc_delivery_recipient_name
     *
     * @return string 
     */
    public function getDocDeliveryRecipientName()
    {
        return $this->doc_delivery_recipient_name;
    }

    /**
     * Set doc_delivery_address
     *
     * @param string $docDeliveryAddress
     * @return Payment
     */
    public function setDocDeliveryAddress($docDeliveryAddress)
    {
        $this->doc_delivery_address = $docDeliveryAddress;
    
        return $this;
    }

    /**
     * Get doc_delivery_address
     *
     * @return string 
     */
    public function getDocDeliveryAddress()
    {
        return $this->doc_delivery_address;
    }

    /**
     * Set doc_delivery_city
     *
     * @param string $docDeliveryCity
     * @return Payment
     */
    public function setDocDeliveryCity($docDeliveryCity)
    {
        $this->doc_delivery_city = $docDeliveryCity;
    
        return $this;
    }

    /**
     * Get doc_delivery_city
     *
     * @return string 
     */
    public function getDocDeliveryCity()
    {
        return $this->doc_delivery_city;
    }

    /**
     * Set doc_delivery_postcode
     *
     * @param string $docDeliveryPostcode
     * @return Payment
     */
    public function setDocDeliveryPostcode($docDeliveryPostcode)
    {
        $this->doc_delivery_postcode = $docDeliveryPostcode;
    
        return $this;
    }

    /**
     * Get doc_delivery_postcode
     *
     * @return string 
     */
    public function getDocDeliveryPostcode()
    {
        return $this->doc_delivery_postcode;
    }

    /**
     * Set doc_delivery_contact_no
     *
     * @param string $docDeliveryContactNo
     * @return Payment
     */
    public function setDocDeliveryContactNo($docDeliveryContactNo)
    {
        $this->doc_delivery_contact_no = $docDeliveryContactNo;
    
        return $this;
    }

    /**
     * Get doc_delivery_contact_no
     *
     * @return string 
     */
    public function getDocDeliveryContactNo()
    {
        return $this->doc_delivery_contact_no;
    }

    /**
     * Set doc_package_total_pieces
     *
     * @param integer $docPackageTotalPieces
     * @return Payment
     */
    public function setDocPackageTotalPieces($docPackageTotalPieces)
    {
        $this->doc_package_total_pieces = $docPackageTotalPieces;
    
        return $this;
    }

    /**
     * Get doc_package_total_pieces
     *
     * @return integer 
     */
    public function getDocPackageTotalPieces()
    {
        return $this->doc_package_total_pieces;
    }

    /**
     * Set doc_package_pickup_date
     *
     * @param string $docPackagePickupDate
     * @return Payment
     */
    public function setDocPackagePickupDate($docPackagePickupDate)
    {
        $this->doc_package_pickup_date = $docPackagePickupDate;
    
        return $this;
    }

    /**
     * Get doc_package_pickup_date
     *
     * @return string 
     */
    public function getDocPackagePickupDate()
    {
        return $this->doc_package_pickup_date;
    }

    /**
     * Set doc_package_ready_hr
     *
     * @param string $docPackageReadyHr
     * @return Payment
     */
    public function setDocPackageReadyHr($docPackageReadyHr)
    {
        $this->doc_package_ready_hr = $docPackageReadyHr;
    
        return $this;
    }

    /**
     * Get doc_package_ready_hr
     *
     * @return string 
     */
    public function getDocPackageReadyHr()
    {
        return $this->doc_package_ready_hr;
    }

    /**
     * Set doc_package_ready_min
     *
     * @param string $docPackageReadyMin
     * @return Payment
     */
    public function setDocPackageReadyMin($docPackageReadyMin)
    {
        $this->doc_package_ready_min = $docPackageReadyMin;
    
        return $this;
    }

    /**
     * Get doc_package_ready_min
     *
     * @return string 
     */
    public function getDocPackageReadyMin()
    {
        return $this->doc_package_ready_min;
    }

    /**
     * Set doc_package_office_close_hr
     *
     * @param string $docPackageOfficeCloseHr
     * @return Payment
     */
    public function setDocPackageOfficeCloseHr($docPackageOfficeCloseHr)
    {
        $this->doc_package_office_close_hr = $docPackageOfficeCloseHr;
    
        return $this;
    }

    /**
     * Get doc_package_office_close_hr
     *
     * @return string 
     */
    public function getDocPackageOfficeCloseHr()
    {
        return $this->doc_package_office_close_hr;
    }

    /**
     * Set doc_package_office_close_min
     *
     * @param string $docPackageOfficeCloseMin
     * @return Payment
     */
    public function setDocPackageOfficeCloseMin($docPackageOfficeCloseMin)
    {
        $this->doc_package_office_close_min = $docPackageOfficeCloseMin;
    
        return $this;
    }

    /**
     * Get doc_package_office_close_min
     *
     * @return string 
     */
    public function getDocPackageOfficeCloseMin()
    {
        return $this->doc_package_office_close_min;
    }

    /**
     * Set mba_organisation_name
     *
     * @param string $mbaOrganisationName
     * @return Payment
     */
    public function setMbaOrganisationName($mbaOrganisationName)
    {
        $this->mba_organisation_name = $mbaOrganisationName;
    
        return $this;
    }

    /**
     * Get mba_organisation_name
     *
     * @return string 
     */
    public function getMbaOrganisationName()
    {
        return $this->mba_organisation_name;
    }

    /**
     * Set mba_fname
     *
     * @param string $mbaFname
     * @return Payment
     */
    public function setMbaFname($mbaFname)
    {
        $this->mba_fname = $mbaFname;
    
        return $this;
    }

    /**
     * Get mba_fname
     *
     * @return string 
     */
    public function getMbaFname()
    {
        return $this->mba_fname;
    }

    /**
     * Set mba_lname
     *
     * @param string $mbaLname
     * @return Payment
     */
    public function setMbaLname($mbaLname)
    {
        $this->mba_lname = $mbaLname;
    
        return $this;
    }

    /**
     * Get mba_lname
     *
     * @return string 
     */
    public function getMbaLname()
    {
        return $this->mba_lname;
    }
}