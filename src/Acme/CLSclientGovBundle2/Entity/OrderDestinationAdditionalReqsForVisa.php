<?php

namespace Acme\CLSclientGovBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderDestinationAdditionalReqsForVisa
 *
 * @ORM\Table(name="tbl_order_destination_add_reqs_for_visa")
 * @ORM\Entity
 */
class OrderDestinationAdditionalReqsForVisa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="visa_req_id", type="integer")
     */
    private $visa_req_id;

    /**
     * @var float
     *
     * @ORM\Column(name="visa_req_price", type="float")
     */
    private $visa_req_price;


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visa_req_id
     *
     * @param integer $visaReqId
     * @return OrderDestinationAdditionalReqsForVisa
     */
    public function setVisaReqId($visaReqId)
    {
        $this->visa_req_id = $visaReqId;
    
        return $this;
    }

    /**
     * Get visa_req_id
     *
     * @return integer 
     */
    public function getVisaReqId()
    {
        return $this->visa_req_id;
    }

    /**
     * Set visa_req_price
     *
     * @param float $visaReqPrice
     * @return OrderDestinationAdditionalReqsForVisa
     */
    public function setVisaReqPrice($visaReqPrice)
    {
        $this->visa_req_price = $visaReqPrice;
    
        return $this;
    }

    /**
     * Get visa_req_price
     *
     * @return float 
     */
    public function getVisaReqPrice()
    {
        return $this->visa_req_price;
    }
}