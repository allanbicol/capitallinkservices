<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Model;

class OrderDlQuotesPaymentController extends GlobalController
{
    public function paymentAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'order-dl-quote-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            $redirect_url = $mod->getCurrentFullURL();
            $session->set("redirect_url", $redirect_url);
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        if(isset($_POST["hidSubmit"])){
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $_POST["orderNumber"] = intval($_POST["orderNumber"]);
            
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            $em->getConnection()->beginTransaction();
            
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["organization"] = filter_var($_POST['organization'], FILTER_SANITIZE_STRING);
            $_POST["billingFname"] = filter_var($_POST['billingFname'], FILTER_SANITIZE_STRING);
            $_POST["billingLname"] = filter_var($_POST['billingLname'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($model->getSPaid() >= 1){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($client_id);
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaOrganisationName($_POST["organization"]);
            $model->setMbaFname($_POST["billingFname"]);
            $model->setMbaLname($_POST["billingLname"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption(1);
            $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
            
            
            //$total_order_price = round($total_order_price);
            $total_order_price = number_format($total_order_price, 2,'.','');
            $model->setTotalOrderPrice($total_order_price);
            $model->setSPaid(1);
            $em->persist($model);
            $em->flush();
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            $quotations = $this->getOrderDlQuotesByOrderNo($_POST["orderNumber"], $_POST["group"]);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            
            if(trim($_SERVER['HTTP_HOST']) != 'localhost' && $error_count == 0){
                
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               //if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $data['mba_country']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS Visa Application'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                   $root_dir = dirname($this->get('kernel')->getRootDir());
                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");

                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               //}
               
               /**
                * Credit card payment
                * Position: End
                */

                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            
            if($error_count > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSclientGovBundle:OrderDlQuotesPayment:payment.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'group'=> $_POST['group'],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details,
                                'quotations'=>$quotations
                                ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:OrderDlQuotesPayment:payment.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'group'=> $_POST['group'],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details,
                                'quotations'=>$quotations
                                ));
                }
            }else{
                
                
                $order->setGrandTotal($total_order_price);
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                
                $em->persist($order);
                $em->flush();
                
                
                
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                // email invoice
                $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Document Legalisation Quotation Receipt", 
                    $this->renderView('AcmeCLSclientGovBundle:OrderDlQuotesPayment:invoice_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'order_no'=>$order->getOrderNo(),
                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                'data'=>$data,
                                'quotations'=>$quotations
                                )
                            )
                    );
                
                if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                    // email invoice (copy to admins)
                    $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                    $this->sendEmail($email_copies, "help@capitallinkservices.com.au", "Document Legalisation Quotation Receipt (Copy)", 
                        $this->renderView('AcmeCLSclientGovBundle:OrderDlQuotesPayment:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$order->getOrderNo(),
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'quotations'=>$quotations
                                    )
                                )
                        );
                }
                
                $this->get('session')->getFlashBag()->add(
                        'success',
                        'Payment has been accepted!'
                    );
                
                // commit changes
                $em->getConnection()->commit(); 
                
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
                
            }
            
        }else{
            $_GET["group"] = intval($_GET["group"]);
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            $quotations = $this->getOrderDlQuotesByOrderNo($_GET["order"], $_GET["group"]);
            
            if(count($data) > 0 && $data['order_type'] == 9){
                return $this->render('AcmeCLSclientGovBundle:OrderDlQuotesPayment:payment.html.twig',
                        array('order_no'=> $_GET["order"],
                            'group'=> $_GET['group'],
                            'data'=> $data,
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'user_details'=>$user_details,
                            'quotations'=>$quotations
                            ));
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }

}
