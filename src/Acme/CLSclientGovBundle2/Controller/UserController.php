<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Model;

class UserController extends GlobalController
{
    public function govRegistrationAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'registration');
        
        if($session->get('email') != ''){
            return $this->redirect($this->generateUrl('cls_client_gov_home'));
        }
        
        $session->set("public_reg", "");
        
        include(dirname($this->get('kernel')->getRootDir()).'/web/api/simple-php-captcha-master/simple-php-captcha.php');
        
        
        if(isset($_POST['hid_submit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            
            $model = new User();
            $model->setType('government');
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPassword($mod->passGenerator($_POST['password']));
            $model->setEmail($_POST['email']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            $model->setDepartmentId($_POST['department']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountryId($_POST['country']);
            
            $model->setMddaAddress($_POST['mddaAddress']);
            $model->setMddaCity($_POST['mddaCity']);
            $model->setMddaState($_POST['mddaState']);
            $model->setMddaPostcode($_POST['mddaPostcode']);
            $model->setMddaCountryId($_POST['mddaCountry']);
            
            $model->setMbaAddress($_POST['mbaAddress']);
            $model->setMbaCity($_POST['mbaCity']);
            $model->setMbaState($_POST['mbaState']);
            $model->setMbaPostcode($_POST['mbaPostcode']);
            $model->setMbaCountryId($_POST['mbaCountry']);
            
            $model->setSEnabled(1);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'password'=>$_POST['password'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'department'=>$_POST['department'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'mddaAddress'=>$_POST['mddaAddress'],
                'mddaCity'=>$_POST['mddaCity'],
                'mddaState'=>$_POST['mddaState'],
                'mddaPostcode'=>$_POST['mddaPostcode'],
                'mddaCountry'=>$_POST['mddaCountry'],
                'mbaAddress'=>$_POST['mbaAddress'],
                'mbaCity'=>$_POST['mbaCity'],
                'mbaState'=>$_POST['mbaState'],
                'mbaPostcode'=>$_POST['mbaPostcode'],
                'mbaCountry'=>$_POST['mbaCountry'],
            );
            

            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['passwordConfirm']) || $this->sEmailAvailable($_POST['email']) == false || strpos($_POST['email'],'.gov.au') === false){
                    $errors = array();
                }
                
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }
                
                if (strpos($_POST['email'],'.gov.au') === false) {
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email address for a government user.');
                    $error_count += 1;
                }


                if($_POST['password'] != $_POST['passwordConfirm']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
                
                if($this->sEmailAvailable($_POST['email']) == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
            }
            
            $resp = (strtolower($_POST['captcha']) == strtolower($session->get('captcha_code'))) ? true : false;
            if ($resp == false) {
                $error_count +=1;
                $recaptcha_error="The captcha wasn't entered correctly.";
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $password_error= 'You must Follow the password required combination.';
                $error_count += 1;
            }
            
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);
                
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                 $this->sendEmail($_POST['email'], "help@capitallinkservices.com.au", "CLS Registration", 
                    $this->renderView('AcmeCLSclientGovBundle:User:registration_email.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'fname'=>$_POST['fname']
                                )
                            )
                    );
                 
                 $this->sendEmail('info@capitallinkservices.com.au', "help@capitallinkservices.com.au", "CLS Registration", 
                    $this->renderView('AcmeCLSclientGovBundle:User:registration_admin_email.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'post'=>$_POST,
                                'dep'=> $this->getDepartmentNameById($_POST['department']),
                                'type'=> 'Government',
                                'address_country'=> $this->getCountryNameById($_POST['country']),
                                'mdda_country'=> $this->getCountryNameById($_POST['mddaCountry']),
                                'mba_country'=> $this->getCountryNameById($_POST['mbaCountry']),
                                )
                            )
                    );
                 
                /* + create log */
                $logDetails = "<a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$model->getId()."' target='_blank'>" . $model->getFname() . " " . $model->getLname() . "</a> has successfully registered as a Government user.";
                $this->createLog('client', $model->getId(), $model->getType() , $logDetails);
                /* - create log */
                 
                return $this->render('AcmeCLSclientGovBundle:User:registration_success.html.twig',
                        array('countries'=> $this->getCountries('priority'))
                        );
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSclientGovBundle:User:govRegistration.html.twig',
                        array('errors'=>$errors,
                            'recaptcha_error'=>(isset($recaptcha_error)) ? $recaptcha_error : '',
                            'password_error'=>(isset($password_error)) ? $password_error : '',
                            'post'=>$post,
                            'captcha'=>$captcha['image_src'],
                            'name_titles'=>$this->getNameTitles(),
                            'countries'=> $this->getCountries('priority'),
                            'departments'=> $this->getDepartments()
                        ));
            }
        }else{
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);
            
            return $this->render('AcmeCLSclientGovBundle:User:govRegistration.html.twig',
                array('captcha'=>$captcha['image_src'],
                    'name_titles'=>$this->getNameTitles(),
                    'countries'=> $this->getCountries('priority'),
                    'departments'=> $this->getDepartments()
                    )
                );
        }
    }
    
    /**
     * update profile
     * @return type
     */
    public function govUpdateProfileAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'profile');
        
        if($session->get('email') == '' || $session->get('user_type') != 'government'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        if(isset($_POST['hid_submit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$session->get('client_id')));
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            $model->setEmail($_POST['email']);
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setDepartmentId($_POST['department']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountryId($_POST['country']);
            
            $model->setMddaAddress($_POST['mddaAddress']);
            $model->setMddaCity($_POST['mddaCity']);
            $model->setMddaState($_POST['mddaState']);
            $model->setMddaPostcode($_POST['mddaPostcode']);
            $model->setMddaCountryId($_POST['mddaCountry']);
            
            $model->setMbaAddress($_POST['mbaAddress']);
            $model->setMbaCity($_POST['mbaCity']);
            $model->setMbaState($_POST['mbaState']);
            $model->setMbaPostcode($_POST['mbaPostcode']);
            $model->setMbaCountryId($_POST['mbaCountry']);
            
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'email'=>$_POST['email'],
                'department_id'=>$_POST['department'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country_id'=>$_POST['country'],
                'mdda_address'=>$_POST['mddaAddress'],
                'mdda_city'=>$_POST['mddaCity'],
                'mdda_state'=>$_POST['mddaState'],
                'mdda_postcode'=>$_POST['mddaPostcode'],
                'mdda_country_id'=>$_POST['mddaCountry'],
                'mba_address'=>$_POST['mbaAddress'],
                'mba_city'=>$_POST['mbaCity'],
                'mba_state'=>$_POST['mbaState'],
                'mba_postcode'=>$_POST['mbaPostcode'],
                'mba_country_id'=>$_POST['mbaCountry'],
            );
            
                
            if($error_count == 0){
                if(($_POST['password'] != '') && ($_POST['password'] != $_POST['passwordConfirm'])){
                    $errors = array();
                }
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $errors[] = array('message'=>'Password must be repeated exactly.');
                        $error_count += 1;
                    }
                }
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $password_error= 'You must Follow the password required combination.';
                $error_count += 1;
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$model->getId()."' target='_blank'>" . $model->getFname() . " " . $model->getLname() . "</a>, a Government user, has updated his/her profile.";
                $this->createLog('client', $model->getId(), $model->getType() , $logDetails);
                /* - create log */
                
                 $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Your profile has been updated!'
                );
                 
                return $this->redirect($this->generateUrl('cls_client_gov_edit_details'));
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSclientGovBundle:User:govEditDetails.html.twig',
                        array('errors'=>$errors,
                            'recaptcha_error'=>(isset($recaptcha_error)) ? $recaptcha_error : '',
                            'password_error'=>(isset($password_error)) ? $password_error : '',
                            'post'=>$post,
                            'name_titles'=>$this->getNameTitles(),
                            'countries'=> $this->getCountries('priority'),
                            'departments'=> $this->getDepartments()
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:User:govEditDetails.html.twig',
                array('name_titles'=>$this->getNameTitles(),
                    'countries'=> $this->getCountries('priority'),
                    'departments'=> $this->getDepartments(),
                    'post'=>$this->getClientDetailsById($session->get('client_id'))
                    )
                );
        }
    }
    
    
    
    
    
    
    
    public function publicRegistrationAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'public-registration');
        
        if($session->get('email') != ''){
            return $this->redirect($this->generateUrl('cls_client_gov_home'));
        }
        
       // $session->set("public_reg", "");
        
        include(dirname($this->get('kernel')->getRootDir()).'/web/api/simple-php-captcha-master/simple-php-captcha.php');
        
        
        if(isset($_POST['hid_submit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = new User();
            $model->setType('public');
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPassword($mod->passGenerator($_POST['password']));
            $model->setEmail($_POST['email']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            //$model->setDepartmentId($_POST['department']);
            $model->setCompany($_POST['company']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountryId($_POST['country']);
            
            $model->setMddaAddress($_POST['mddaAddress']);
            $model->setMddaCity($_POST['mddaCity']);
            $model->setMddaState($_POST['mddaState']);
            $model->setMddaPostcode($_POST['mddaPostcode']);
            $model->setMddaCountryId($_POST['mddaCountry']);
            
            $model->setMbaAddress($_POST['mbaAddress']);
            $model->setMbaCity($_POST['mbaCity']);
            $model->setMbaState($_POST['mbaState']);
            $model->setMbaPostcode($_POST['mbaPostcode']);
            $model->setMbaCountryId($_POST['mbaCountry']);
            
            $model->setSEnabled(1);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'password'=>$_POST['password'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'company'=>$_POST['company'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'mddaAddress'=>$_POST['mddaAddress'],
                'mddaCity'=>$_POST['mddaCity'],
                'mddaState'=>$_POST['mddaState'],
                'mddaPostcode'=>$_POST['mddaPostcode'],
                'mddaCountry'=>$_POST['mddaCountry'],
                'mbaAddress'=>$_POST['mbaAddress'],
                'mbaCity'=>$_POST['mbaCity'],
                'mbaState'=>$_POST['mbaState'],
                'mbaPostcode'=>$_POST['mbaPostcode'],
                'mbaCountry'=>$_POST['mbaCountry'],
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['passwordConfirm']) || $this->sEmailAvailable($_POST['email']) == false){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }
                

                if($_POST['password'] != $_POST['passwordConfirm']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
                
                if($this->sEmailAvailable($_POST['email']) == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
            }
            
            $resp = (strtolower($_POST['captcha']) == strtolower($session->get('captcha_code'))) ? true : false;
            if ($resp == false) {
                $error_count +=1;
                $recaptcha_error="The captcha wasn't entered correctly.";
            }
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $password_error= 'You must Follow the password required combination.';
                $error_count += 1;
            }
                
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);
                
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->sendEmail($_POST['email'], "help@capitallinkservices.com.au", "CLS Registration", 
                    $this->renderView('AcmeCLSclientGovBundle:User:registration_email.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'fname'=>$_POST['fname']
                                )
                            )
                    );
                
                $this->sendEmail('info@capitallinkservices.com.au', "help@capitallinkservices.com.au", "CLS Registration", 
                    $this->renderView('AcmeCLSclientGovBundle:User:registration_admin_email.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'post'=>$_POST,
                                'dep'=> $_POST['company'],
                                'type'=> 'Public',
                                'address_country'=> $this->getCountryNameById($_POST['country']),
                                'mdda_country'=> $this->getCountryNameById($_POST['mddaCountry']),
                                'mba_country'=> $this->getCountryNameById($_POST['mbaCountry']),
                                )
                            )
                    );
                 
                if($session->get("redirect_url") != ''){
                    $session->set('client_id', $model->getId()); 
                    $session->set('user_type', $model->getType());
                    $session->set('email', $model->getEmail()); 
                    $session->set('fname', $model->getFname()); 
                    $session->set('lname', $model->getLname());
                }
                
                /* + create log */
                $logDetails = "<a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$model->getId()."' target='_blank'>" . $model->getFname() . " " . $model->getLname() . "</a> has successfully registered as a Public user.";
                $this->createLog('client', $model->getId(), $model->getType() , $logDetails);
                /* - create log */
                
                return $this->render('AcmeCLSclientGovBundle:User:registration_success.html.twig',
                        array('countries'=> $this->getCountries())
                        );
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSclientGovBundle:User:publicRegistration.html.twig',
                        array('errors'=>$errors,
                            'recaptcha_error'=>(isset($recaptcha_error)) ? $recaptcha_error : '',
                            'password_error'=>(isset($password_error)) ? $password_error : '',
                            'post'=>$post,
                            'captcha'=>$captcha['image_src'],
                            'name_titles'=>$this->getNameTitles(),
                            'countries'=> $this->getCountries('priority')
                        ));
            }
        }else{
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);
            
            return $this->render('AcmeCLSclientGovBundle:User:publicRegistration.html.twig',
                array('captcha'=>$captcha['image_src'],
                    'name_titles'=>$this->getNameTitles(),
                    'countries'=> $this->getCountries('priority')
                    )
                );
        }
    }
    
    public function passwordHardeningAction(){
        $mod = new Model\GlobalModel();
        $result = $mod->passwordHardening($_POST['pass']);
        return new Response($result);
        
    }
    
   
    
    public function publicUpdateProfileAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'profile');
        
        if($session->get('email') == '' || $session->get('user_type') != 'public'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        if(isset($_POST['hid_submit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$session->get('client_id')));
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            $model->setEmail($_POST['email']);
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setCompany($_POST['company']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountryId($_POST['country']);
            
            $model->setMddaAddress($_POST['mddaAddress']);
            $model->setMddaCity($_POST['mddaCity']);
            $model->setMddaState($_POST['mddaState']);
            $model->setMddaPostcode($_POST['mddaPostcode']);
            $model->setMddaCountryId($_POST['mddaCountry']);
            
            $model->setMbaAddress($_POST['mbaAddress']);
            $model->setMbaCity($_POST['mbaCity']);
            $model->setMbaState($_POST['mbaState']);
            $model->setMbaPostcode($_POST['mbaPostcode']);
            $model->setMbaCountryId($_POST['mbaCountry']);
            
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'email'=>$_POST['email'],
                'company'=>$_POST['company'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country_id'=>$_POST['country'],
                'mdda_address'=>$_POST['mddaAddress'],
                'mdda_city'=>$_POST['mddaCity'],
                'mdda_state'=>$_POST['mddaState'],
                'mdda_postcode'=>$_POST['mddaPostcode'],
                'mdda_country_id'=>$_POST['mddaCountry'],
                'mba_address'=>$_POST['mbaAddress'],
                'mba_city'=>$_POST['mbaCity'],
                'mba_state'=>$_POST['mbaState'],
                'mba_postcode'=>$_POST['mbaPostcode'],
                'mba_country_id'=>$_POST['mbaCountry'],
            );
            
                
            if($error_count == 0){
                if(($_POST['password'] != '') && ($_POST['password'] != $_POST['passwordConfirm'])){
                    $errors = array();
                }
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $errors[] = array('message'=>'Password must be repeated exactly.');
                        $error_count += 1;
                    }
                }
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $password_error= 'You must Follow the password required combination.';
                $error_count += 1;
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$model->getId()."' target='_blank'>" . $model->getFname() . " " . $model->getLname() . "</a>, a Public user, has updated his/her profile.";
                $this->createLog('client', $model->getId(), $model->getType() , $logDetails);
                /* - create log */
                
                 $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Your profile has been updated!'
                );
                 
                return $this->redirect($this->generateUrl('cls_client_public_edit_details'));
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSclientGovBundle:User:publicEditDetails.html.twig',
                        array('errors'=>$errors,
                            'password_error'=>(isset($password_error)) ? $password_error : '',
                            'post'=>$post,
                            'name_titles'=>$this->getNameTitles(),
                            'countries'=> $this->getCountries('priority')
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:User:publicEditDetails.html.twig',
                array('name_titles'=>$this->getNameTitles(),
                    'countries'=> $this->getCountries('priority'),
                    'post'=>$this->getClientDetailsById($session->get('client_id'))
                    )
                );
        }
    }
    
    
    
    
    
    public function corporateRegistrationAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'corporate-registration');
        
        if($session->get('email') != ''){
            return $this->redirect($this->generateUrl('cls_client_gov_home'));
        }
        
        $session->set("public_reg", "");
        
        include(dirname($this->get('kernel')->getRootDir()).'/web/api/simple-php-captcha-master/simple-php-captcha.php');
        
        
        if(isset($_POST['hid_submit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = new User();
            $model->setType('corporate');
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPassword($mod->passGenerator($_POST['password']));
            $model->setEmail($_POST['email']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            //$model->setDepartmentId($_POST['department']);
            $model->setCompany($_POST['company']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountryId($_POST['country']);
            
            $model->setMddaAddress($_POST['mddaAddress']);
            $model->setMddaCity($_POST['mddaCity']);
            $model->setMddaState($_POST['mddaState']);
            $model->setMddaPostcode($_POST['mddaPostcode']);
            $model->setMddaCountryId($_POST['mddaCountry']);
            
            $model->setMbaAddress($_POST['mbaAddress']);
            $model->setMbaCity($_POST['mbaCity']);
            $model->setMbaState($_POST['mbaState']);
            $model->setMbaPostcode($_POST['mbaPostcode']);
            $model->setMbaCountryId($_POST['mbaCountry']);
            
            $model->setSEnabled(1);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'password'=>$_POST['password'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'company'=>$_POST['company'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'mddaAddress'=>$_POST['mddaAddress'],
                'mddaCity'=>$_POST['mddaCity'],
                'mddaState'=>$_POST['mddaState'],
                'mddaPostcode'=>$_POST['mddaPostcode'],
                'mddaCountry'=>$_POST['mddaCountry'],
                'mbaAddress'=>$_POST['mbaAddress'],
                'mbaCity'=>$_POST['mbaCity'],
                'mbaState'=>$_POST['mbaState'],
                'mbaPostcode'=>$_POST['mbaPostcode'],
                'mbaCountry'=>$_POST['mbaCountry'],
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['passwordConfirm']) || $this->sEmailAvailable($_POST['email']) == false){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['passwordConfirm']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
                
                if($this->sEmailAvailable($_POST['email']) == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
            }
            
            $resp = (strtolower($_POST['captcha']) == strtolower($session->get('captcha_code'))) ? true : false;
            if ($resp == false) {
                $error_count +=1;
                $recaptcha_error="The captcha wasn't entered correctly.";
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $password_error= 'You must Follow the password required combination.';
                $error_count += 1;
            }
            
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);
                
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->sendEmail($_POST['email'], "help@capitallinkservices.com.au", "CLS Registration", 
                    $this->renderView('AcmeCLSclientGovBundle:User:registration_email.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'fname'=>$_POST['fname']
                                )
                            )
                    );
                
                
                $this->sendEmail('info@capitallinkservices.com.au', "help@capitallinkservices.com.au", "CLS Registration", 
                    $this->renderView('AcmeCLSclientGovBundle:User:registration_admin_email.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'post'=>$_POST,
                                'dep'=> $_POST['company'],
                                'type'=> 'Corporate',
                                'address_country'=> $this->getCountryNameById($_POST['country']),
                                'mdda_country'=> $this->getCountryNameById($_POST['mddaCountry']),
                                'mba_country'=> $this->getCountryNameById($_POST['mbaCountry']),
                                )
                            )
                    );
                
                 
                /* + create log */
                $logDetails = "<a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$model->getId()."' target='_blank'>" . $model->getFname() . " " . $model->getLname() . "</a> has successfully registered as a Corporate user.";
                $this->createLog('client', $model->getId(), $model->getType() , $logDetails);
                /* - create log */
                
                return $this->render('AcmeCLSclientGovBundle:User:registration_success.html.twig',
                        array('countries'=> $this->getCountries())
                        );
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSclientGovBundle:User:corporateRegistration.html.twig',
                        array('errors'=>$errors,
                            'recaptcha_error'=>(isset($recaptcha_error)) ? $recaptcha_error : '',
                            'password_error'=>(isset($password_error)) ? $password_error : '',
                            'post'=>$post,
                            'captcha'=>$captcha['image_src'],
                            'name_titles'=>$this->getNameTitles(),
                            'countries'=> $this->getCountries('priority')
                        ));
            }
        }else{
            $captcha = simple_php_captcha();
            $session->set('captcha_code', $captcha['code']);
            
            return $this->render('AcmeCLSclientGovBundle:User:corporateRegistration.html.twig',
                array('captcha'=>$captcha['image_src'],
                    'name_titles'=>$this->getNameTitles(),
                    'countries'=> $this->getCountries('priority')
                    )
                );
        }
    }
    
    
    
    public function corporateUpdateProfileAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'profile');
        
        if($session->get('email') == '' || ($session->get('user_type') != 'corporate' && $session->get('user_type') != 'bulk-visa')){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        if(isset($_POST['hid_submit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$session->get('client_id')));
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            $model->setEmail($_POST['email']);
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setCompany($_POST['company']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountryId($_POST['country']);
            
            $model->setMddaAddress($_POST['mddaAddress']);
            $model->setMddaCity($_POST['mddaCity']);
            $model->setMddaState($_POST['mddaState']);
            $model->setMddaPostcode($_POST['mddaPostcode']);
            $model->setMddaCountryId($_POST['mddaCountry']);
            
            $model->setMbaAddress($_POST['mbaAddress']);
            $model->setMbaCity($_POST['mbaCity']);
            $model->setMbaState($_POST['mbaState']);
            $model->setMbaPostcode($_POST['mbaPostcode']);
            $model->setMbaCountryId($_POST['mbaCountry']);
            
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'email'=>$_POST['email'],
                'company'=>$_POST['company'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country_id'=>$_POST['country'],
                'mdda_address'=>$_POST['mddaAddress'],
                'mdda_city'=>$_POST['mddaCity'],
                'mdda_state'=>$_POST['mddaState'],
                'mdda_postcode'=>$_POST['mddaPostcode'],
                'mdda_country_id'=>$_POST['mddaCountry'],
                'mba_address'=>$_POST['mbaAddress'],
                'mba_city'=>$_POST['mbaCity'],
                'mba_state'=>$_POST['mbaState'],
                'mba_postcode'=>$_POST['mbaPostcode'],
                'mba_country_id'=>$_POST['mbaCountry'],
            );
            
            if($error_count == 0){
                if(($_POST['password'] != '') && ($_POST['password'] != $_POST['passwordConfirm'])){
                    $errors = array();
                }
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $errors[] = array('message'=>'Password must be repeated exactly.');
                        $error_count += 1;
                    }
                }
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $password_error= 'You must Follow the password required combination.';
                $error_count += 1;
            }
                
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$model->getId()."' target='_blank'>" . $model->getFname() . " " . $model->getLname() . "</a>, a Corporate user, has updated his/her profile.";
                $this->createLog('client', $model->getId(), $model->getType() , $logDetails);
                /* - create log */
                
                 $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Your profile has been updated!'
                );
                 
                return $this->redirect($this->generateUrl('cls_client_corporate_edit_details'));
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSclientGovBundle:User:corporateEditDetails.html.twig',
                        array('errors'=>$errors,
                            'password_error'=>(isset($password_error)) ? $password_error : '',
                            'post'=>$post,
                            'name_titles'=>$this->getNameTitles(),
                            'countries'=> $this->getCountries('priority')
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:User:corporateEditDetails.html.twig',
                array('name_titles'=>$this->getNameTitles(),
                    'countries'=> $this->getCountries('priority'),
                    'post'=>$this->getClientDetailsById($session->get('client_id'))
                    )
                );
        }
    }
    
    public function redirectUrlAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        return $this->redirect($session->get("redirect_url"));
    }
    
    
}
