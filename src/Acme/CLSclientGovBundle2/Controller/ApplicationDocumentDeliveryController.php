<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSadminBundle\Entity\OrderPoliceClearanceApplicants;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class ApplicationDocumentDeliveryController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-document-delivery');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'government' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            $session->set("redirect_url", $this->generateUrl('cls_client_aplication_document_delivery'));
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        if(isset($_POST["hid_submit"])){
            
            $_POST['paymentDocType'] = filter_var($_POST['paymentDocType'], FILTER_SANITIZE_STRING);
            //$_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
            if($session->get('user_type') == 'admin-user'){
                $_POST["client_id"] = filter_var($_POST['client_id'], FILTER_SANITIZE_STRING);
                $_POST["client_id"] = intval($_POST["client_id"]);
            }
            $total_order_price = 0;
            $error_count = 0;
            $doc_delivery = '';
            $_POST["receiverName"] = filter_var($_POST['receiverName'], FILTER_SANITIZE_STRING);
            $_POST["pickupAddress"] = filter_var($_POST['pickupAddress'], FILTER_SANITIZE_STRING);
            $_POST["pickupCity"] = filter_var($_POST['pickupCity'], FILTER_SANITIZE_STRING);
            $_POST["pickupPostcode"] = filter_var($_POST['pickupPostcode'], FILTER_SANITIZE_STRING);
            $_POST["pickupContactNo"] = filter_var($_POST['pickupContactNo'], FILTER_SANITIZE_STRING);
            
            $_POST['pickupContactArea'] = filter_var($_POST['pickupContactArea'], FILTER_SANITIZE_STRING);
            $_POST["deliveryRecipientName"] = filter_var($_POST['deliveryRecipientName'], FILTER_SANITIZE_STRING);
            $_POST["deliveryCompany"] = filter_var($_POST['deliveryCompany'], FILTER_SANITIZE_STRING);
            $_POST["deliveryAddress"] = filter_var($_POST['deliveryAddress'], FILTER_SANITIZE_STRING);
            $_POST["deliveryCity"] = filter_var($_POST['deliveryCity'], FILTER_SANITIZE_STRING);
            $_POST["deliveryPostcode"] = filter_var($_POST['deliveryPostcode'], FILTER_SANITIZE_STRING);
            $_POST["deliveryContactNo"] = filter_var($_POST['deliveryContactNo'], FILTER_SANITIZE_STRING);
            
            $_POST['pickupContactAreaAlt1'] = filter_var($_POST['pickupContactAreaAlt1'], FILTER_SANITIZE_STRING);
            $_POST["deliveryRecipientNameAlt1"] = filter_var($_POST['deliveryRecipientNameAlt1'], FILTER_SANITIZE_STRING);
            $_POST["deliveryCompanyAlt1"] = filter_var($_POST['deliveryCompanyAlt1'], FILTER_SANITIZE_STRING);
            $_POST["deliveryAddressAlt1"] = filter_var($_POST['deliveryAddressAlt1'], FILTER_SANITIZE_STRING);
            $_POST["deliveryCityAlt1"] = filter_var($_POST['deliveryCityAlt1'], FILTER_SANITIZE_STRING);
            $_POST["deliveryPostcodeAlt1"] = filter_var($_POST['deliveryPostcodeAlt1'], FILTER_SANITIZE_STRING);
            $_POST["deliveryContactNoAlt1"] = filter_var($_POST['deliveryContactNoAlt1'], FILTER_SANITIZE_STRING);
            
            $_POST['pickupContactAreaAlt2'] = filter_var($_POST['pickupContactAreaAlt2'], FILTER_SANITIZE_STRING);
            $_POST["deliveryRecipientNameAlt2"] = filter_var($_POST['deliveryRecipientNameAlt2'], FILTER_SANITIZE_STRING);
            $_POST["deliveryCompanyAlt2"] = filter_var($_POST['deliveryCompanyAlt2'], FILTER_SANITIZE_STRING);
            $_POST["deliveryAddressAlt2"] = filter_var($_POST['deliveryAddressAlt2'], FILTER_SANITIZE_STRING);
            $_POST["deliveryCityAlt2"] = filter_var($_POST['deliveryCityAlt2'], FILTER_SANITIZE_STRING);
            $_POST["deliveryPostcodeAlt2"] = filter_var($_POST['deliveryPostcodeAlt2'], FILTER_SANITIZE_STRING);
            $_POST["deliveryContactNoAlt2"] = filter_var($_POST['deliveryContactNoAlt2'], FILTER_SANITIZE_STRING);
            
            $_POST["packageTotalPieces"] = filter_var($_POST['packageTotalPieces'], FILTER_SANITIZE_STRING);
            $_POST["packagePickupDate"] = filter_var($_POST['packagePickupDate'], FILTER_SANITIZE_STRING);
            $_POST["packageReadyHr"] = filter_var($_POST['packageReadyHr'], FILTER_SANITIZE_STRING);
            $_POST["packageReadyMin"] = filter_var($_POST['packageReadyMin'], FILTER_SANITIZE_STRING);
            $_POST["packageReadyAmPm"] = filter_var($_POST['packageReadyAmPm'], FILTER_SANITIZE_STRING);
            $_POST["packageOfficeCloseHr"] = filter_var($_POST['packageOfficeCloseHr'], FILTER_SANITIZE_STRING);
            $_POST["packageOfficeCloseMin"] = filter_var($_POST['packageOfficeCloseMin'], FILTER_SANITIZE_STRING);
            $_POST["packageOfficeCloseAmPm"] = filter_var($_POST['packageOfficeCloseAmPm'], FILTER_SANITIZE_STRING);
            $_POST["securityNumber"] = filter_var($_POST['securityNumber'], FILTER_SANITIZE_STRING);
            
            $_POST["receiverName"] = str_replace("&#39;","'", $_POST["receiverName"]);
            $_POST["pickupAddress"] = str_replace("&#39;","'", $_POST["pickupAddress"]);
            $_POST["deliveryRecipientName"] = str_replace("&#39;","'", $_POST["deliveryRecipientName"]);
            $_POST["deliveryCompany"] = str_replace("&#39;","'", $_POST["deliveryCompany"]);
            $_POST["deliveryAddress"] = str_replace("&#39;","'", $_POST["deliveryAddress"]);
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            // new order
            if(!isset($_POST["orderNumber"])){
                $order = new Orders();
                $order->setClientId($client_id);
                $order->setOrderType(7);
            // update order
            }else{
                $_POST["orderNumber"] = intval($_POST["orderNumber"]);
                $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                
            }
            $order->setClientId($client_id);
            $order->setOrderType(7);
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            $order->setDocDeliveryType($_POST["paymentDocType"]);
            $order->setDocReceiverName($_POST["receiverName"]);
            $order->setDocPickupAddress($_POST["pickupAddress"]);
            $order->setDocPickupCity($_POST["pickupCity"]);
            $order->setDocPickupPostcode($_POST["pickupPostcode"]);
            $order->setDocPickupContactNo($_POST["pickupContactNo"]);
            
            $order->setDocPickupContactArea($_POST["pickupContactArea"]);
            $order->setDocDeliveryRecipientName($_POST["deliveryRecipientName"]);
            $order->setDocDeliveryCompany($_POST["deliveryCompany"]);
            $order->setDocDeliveryAddress($_POST["deliveryAddress"]);
            $order->setDocDeliveryCity($_POST["deliveryCity"]);
            $order->setDocDeliveryPostcode($_POST["deliveryPostcode"]);
            $order->setDocDeliveryContactNo($_POST["deliveryContactNo"]);
            
            $order->setDocPickupContactAreaAlt1($_POST["pickupContactAreaAlt1"]);
            $order->setDocDeliveryRecipientNameAlt1($_POST["deliveryRecipientNameAlt1"]);
            $order->setDocDeliveryCompanyAlt1($_POST["deliveryCompanyAlt1"]);
            $order->setDocDeliveryAddressAlt1($_POST["deliveryAddressAlt1"]);
            $order->setDocDeliveryCityAlt1($_POST["deliveryCityAlt1"]);
            $order->setDocDeliveryPostcodeAlt1($_POST["deliveryPostcodeAlt1"]);
            $order->setDocDeliveryContactNoAlt1($_POST["deliveryContactNoAlt1"]);
            
            $order->setDocPickupContactAreaAlt2($_POST["pickupContactAreaAlt2"]);
            $order->setDocDeliveryRecipientNameAlt2($_POST["deliveryRecipientNameAlt2"]);
            $order->setDocDeliveryCompanyAlt2($_POST["deliveryCompanyAlt2"]);
            $order->setDocDeliveryAddressAlt2($_POST["deliveryAddressAlt2"]);
            $order->setDocDeliveryCityAlt2($_POST["deliveryCityAlt2"]);
            $order->setDocDeliveryPostcodeAlt2($_POST["deliveryPostcodeAlt2"]);
            $order->setDocDeliveryContactNoAlt2($_POST["deliveryContactNoAlt2"]);
            
            $order->setDocPackageTotalPieces($_POST["packageTotalPieces"]);
            $order->setDocPackagePickupDate($mod->changeFormatToOriginal($_POST["packagePickupDate"]));
            $order->setDocPackageReadyHr($_POST["packageReadyHr"]);
            $order->setDocPackageReadyMin($_POST["packageReadyMin"]);
            $order->setDocPackageReadyAmpm($_POST["packageReadyAmPm"]);
            $order->setDocPackageOfficeCloseHr($_POST["packageOfficeCloseHr"]);
            $order->setDocPackageOfficeCloseMin($_POST["packageOfficeCloseMin"]);
            $order->setDocPackageOfficeCloseAmpm($_POST["packageOfficeCloseAmPm"]);
            $order->setDocDeliverySecurityNo($_POST['securityNumber']);
            
            if(trim($_POST['paymentDocType']) != '' && is_numeric($_POST['paymentDocType'])){
                $doc_delivery = $em->getRepository('AcmeCLSadminBundle:SettingsDocumentDelivery')->findOneBy(array('id'=>$_POST['paymentDocType']));
                $total_order_price = $doc_delivery->getCost();
            }
            
            if($user->getCanGetSpecialPrice() == 1){
                $total_order_price = $total_order_price + ($total_order_price * ($user->getSpecialPrice()/100));
            }
            
            $total_order_price = $total_order_price + ($total_order_price * (10/100));
            
            if($order->getDiscountRate() > 0){
                $total_order_price = ($total_order_price - ($total_order_price * ($order->getDiscountRate()/100)));
            }
            
            
            //$total_order_price = round($total_order_price);
            $order->setGrandTotal($total_order_price);
            if($_POST["hid_submit"] == 0){
                $order->setStatus(1); // 1= Details level
            }else{
                $order->setStatus(2); // 2= Review Order
            }
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            
            if($session->get('user_type') == 'admin-user'){
                $order->setVisaClsTeamMember($session->get('admin_id'));
            }
            $em->persist($order);
            $em->flush();
            
            
            $validation_error_count = 0;
            if($_POST["hid_submit"] == 1){
                $blank_inputs = array();
                if(trim($_POST["paymentDocType"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'paymentDocType';
                }
                
                if(trim($_POST["receiverName"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'receiverName';
                }
                
                if(trim($_POST["securityNumber"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'securityNumber';
                }
                
                if(trim($_POST["pickupAddress"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'pickupAddress';
                }
                
                if(trim($_POST["deliveryRecipientName"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'deliveryRecipientName';
                }
                
                if(trim($_POST["deliveryCompany"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'deliveryCompany';
                }
                
                if(trim($_POST["deliveryAddress"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'deliveryAddress';
                }
            }
            
            $post = array(
                'doc_delivery_type'=>$_POST['paymentDocType'],
//                'fname'=>$_POST['fname'],
//                'lname'=>$_POST['lname'],
//                'email'=>$_POST['email'],
//                'phone'=>$_POST['phone'],
//                'mobile'=>$_POST['mobile'],
                'doc_receiver_name'=>$_POST['receiverName'],
                'doc_delivery_security_no'=>$_POST['securityNumber'],
                'doc_pickup_address'=>$_POST['pickupAddress'],
                'doc_pickup_city'=>$_POST['pickupCity'],
                'doc_pickup_postcode'=>$_POST['pickupPostcode'],
                'doc_pickup_contact_no'=>$_POST['pickupContactNo'],
                
                'doc_pickup_contact_area'=>$_POST['pickupContactArea'],
                'doc_delivery_company'=>$_POST['deliveryCompany'],
                'doc_delivery_recipient_name'=>$_POST['deliveryRecipientName'],
                'doc_delivery_address'=>$_POST['deliveryAddress'],
                'doc_delivery_city'=>$_POST['deliveryCity'],
                'doc_delivery_postcode'=>$_POST['deliveryPostcode'],
                'doc_delivery_contact_no'=>$_POST['deliveryContactNo'],
                
                'doc_pickup_contact_area_alt1'=>$_POST['pickupContactAreaAlt1'],
                'doc_delivery_company_alt1'=>$_POST['deliveryCompanyAlt1'],
                'doc_delivery_recipient_name_alt1'=>$_POST['deliveryRecipientNameAlt1'],
                'doc_delivery_address_alt1'=>$_POST['deliveryAddressAlt1'],
                'doc_delivery_city_alt1'=>$_POST['deliveryCityAlt1'],
                'doc_delivery_postcode_alt1'=>$_POST['deliveryPostcodeAlt1'],
                'doc_delivery_contact_no_alt1'=>$_POST['deliveryContactNoAlt1'],
                
                'doc_pickup_contact_area_alt2'=>$_POST['pickupContactAreaAlt2'],
                'doc_delivery_company_alt2'=>$_POST['deliveryCompanyAlt2'],
                'doc_delivery_recipient_name_alt2'=>$_POST['deliveryRecipientNameAlt2'],
                'doc_delivery_address_alt2'=>$_POST['deliveryAddressAlt2'],
                'doc_delivery_city_alt2'=>$_POST['deliveryCityAlt2'],
                'doc_delivery_postcode_alt2'=>$_POST['deliveryPostcodeAlt2'],
                'doc_delivery_contact_no_alt2'=>$_POST['deliveryContactNoAlt2'],
                
                'doc_package_total_pieces'=>$_POST['packageTotalPieces'],
                'doc_package_pickup_date'=>$_POST['packagePickupDate'],
                'doc_package_ready_hr'=>$_POST['packageReadyHr'],
                'doc_package_ready_min'=>$_POST['packageReadyMin'],
                'doc_package_ready_ampm'=>$_POST['packageReadyAmPm'],
                'doc_package_office_close_hr'=>$_POST['packageOfficeCloseHr'],
                'doc_package_office_close_min'=>$_POST['packageOfficeCloseMin'],
                'doc_package_office_close_ampm'=>$_POST['packageOfficeCloseAmPm'],
//                'paymentType'=>$_POST['paymentType'],
                'blank_inputs'=>(isset($blank_inputs)) ? $blank_inputs : array()
            );
            

            
            
            
            if($error_count > 0 || $validation_error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }

                $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.id = :id')
                    ->setParameter('id', $client_id)
                    ->getQuery();
                $user = $query->getArrayResult();

                
                if(!isset($special_error) || $validation_error_count > 0){
                    if($validation_error_count > 0){
                    $this->get('session')->getFlashBag()->add(
                                'validation-error',
                                'Please check the highlighted required fields and complete the form.'
                            );
                    }else{
                        $this->get('session')->getFlashBag()->add(
                            'error',
                            $errors
                        );
                    }

                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:index.html.twig',
                            array(
                                'doc_types'=> $this->getDocumentDeliveryTypes(),
                                'user'=>$user[0],
                                'credit_card_processing_fee'=> $this->getCreditCardProcessingFee(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post' => $post,
                                'user_details'=> $user_details
                            ));
                }else{
                    
                    

                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:index.html.twig',
                            array(
                                'doc_types'=> $this->getDocumentDeliveryTypes(),
                                'user'=>$user[0],
                                'credit_card_processing_fee'=> $this->getCreditCardProcessingFee(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post' => $post,
                                'user_details'=> $user_details
                            ));
                }

            }else{

                // commit changes
                $em->getConnection()->commit(); 
                
                
                

//                $data = $this->getOrderDetailsByOrderNoAndClientId($order->getOrderNo(), $client_id);
//                
//                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
//                
                
                
                // if save only
                if($order->getStatus() == 1){
                    
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been saved'
                        );
                    
                    // return to the details page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery') . "?order=".$order->getOrderNo());
                    }
                    
                }else{
                    
                    // return to review tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$order->getOrderNo());
                    }
                    
                }
            }
            
        }else{
            
            
            if($session->get('user_type') == 'admin-user'){
                if(isset($_GET['client'])){
                    $_GET['client'] = intval($_GET['client']);
                }else{
                    return $this->redirect($this->generateUrl('acme_cls_admin_home'));
                }
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:User');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id', $client_id)
                ->getQuery();
            $user = $query->getArrayResult();
                    
            // if order number is set
            if(isset($_GET["order"])){
                $_GET["order"] = intval($_GET["order"]);
                $post = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
                
                if(count($post) > 0 && $post['order_type'] == 7){
                    

                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:index.html.twig',
                            array(
                                'user'=>$user[0],
                                'credit_card_processing_fee'=> $this->getCreditCardProcessingFee(),
                                'cardtypes'=> $this->getCardTypes(),
                                'doc_types'=> $this->getDocumentDeliveryTypes(),
                                'user_details'=>$user_details,
                                'order_no'=>$_GET["order"],
                                'post'=> $post
                            ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order"].' not found!' 
                        ));
                }
            }else{
                if(isset($_POST['public'])){
                    
                    $post = array(
                        'doc_delivery_type'=>$_POST['doc_delivery_type'],
                        'doc_receiver_name'=>$_POST['doc_receiver_name'],
                        //'doc_delivery_security_no'=>$_POST['doc_delivery_security_no'],
                        'doc_pickup_address'=>$_POST['doc_pickup_address'],
                        'doc_pickup_city'=>$_POST['doc_pickup_city'],
                        'doc_pickup_postcode'=>$_POST['doc_pickup_postcode'],
                        'doc_pickup_contact_no'=>$_POST['doc_pickup_contact_no'],

                        'doc_pickup_contact_area'=>$_POST['doc_pickup_contact_area'],
                        'doc_delivery_company'=>$_POST['doc_delivery_company'],
                        'doc_delivery_recipient_name'=>$_POST['doc_delivery_recipient_name'],
                        'doc_delivery_address'=>$_POST['doc_delivery_address'],
                        'doc_delivery_city'=>$_POST['doc_delivery_city'],
                        'doc_delivery_postcode'=>$_POST['doc_delivery_postcode'],
                        'doc_delivery_contact_no'=>$_POST['doc_delivery_contact_no'],

                        'doc_pickup_contact_area_alt1'=>$_POST['doc_pickup_contact_area_alt1'],
                        'doc_delivery_company_alt1'=>$_POST['doc_delivery_company_alt1'],
                        'doc_delivery_recipient_name_alt1'=>$_POST['doc_delivery_recipient_name_alt1'],
                        'doc_delivery_address_alt1'=>$_POST['doc_delivery_address_alt1'],
                        'doc_delivery_city_alt1'=>$_POST['doc_delivery_city_alt1'],
                        'doc_delivery_postcode_alt1'=>$_POST['doc_delivery_postcode_alt1'],
                        'doc_delivery_contact_no_alt1'=>$_POST['doc_delivery_contact_no_alt1'],

                        'doc_pickup_contact_area_alt2'=>$_POST['doc_pickup_contact_area_alt2'],
                        'doc_delivery_company_alt2'=>$_POST['doc_delivery_company_alt2'],
                        'doc_delivery_recipient_name_alt2'=>$_POST['doc_delivery_recipient_name_alt2'],
                        'doc_delivery_address_alt2'=>$_POST['doc_delivery_address_alt2'],
                        'doc_delivery_city_alt2'=>$_POST['doc_delivery_city_alt2'],
                        'doc_delivery_postcode_alt2'=>$_POST['doc_delivery_postcode_alt2'],
                        'doc_delivery_contact_no_alt2'=>$_POST['doc_delivery_contact_no_alt2'],

                        'doc_package_total_pieces'=>$_POST['doc_package_total_pieces'],
                        'doc_package_pickup_date'=>$_POST['doc_package_pickup_date'],
                        'doc_package_ready_hr'=>$_POST['doc_package_ready_hr'],
                        'doc_package_ready_min'=>$_POST['doc_package_ready_min'],
                        'doc_package_ready_ampm'=>$_POST['doc_package_ready_ampm'],
                        'doc_package_office_close_hr'=>$_POST['doc_package_office_close_hr'],
                        'doc_package_office_close_min'=>$_POST['doc_package_office_close_min'],
                        'doc_package_office_close_ampm'=>$_POST['doc_package_office_close_ampm'],
                        'blank_inputs'=> array()
                    );
                    
                    
                }
                
                
                return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:index.html.twig',
                            array(
                                'user'=>$user[0],
                                'credit_card_processing_fee'=> $this->getCreditCardProcessingFee(),
                                'cardtypes'=> $this->getCardTypes(),
                                'doc_types'=> $this->getDocumentDeliveryTypes(),
                                'user_details'=>$user_details,
                                'post'=> (isset($post)) ? $post : array()
                            ));
            }
            
            
        }
    }
    
    
    
    
    /**
     * 2nd Step: Review Order
     */
    public function reviewOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-document-delivery-review-order');
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'government' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hid_submit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hid_submit"] == 1){
                $model->setStatus(3); // 3= Place Order
            }else{
                $model->setStatus(2); // 2= Review Order
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 2){

                // return to review visa page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_place_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_place_order') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $em = $this->getDoctrine()->getManager();
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            if(count($data) > 0 && $data['order_type'] == 7){
                // if order status done with the review tpn page
                // if not, send to review tpn page
                if( $data["status"] >= 1 ){

                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:reviewOrder.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                        'special_price'=> $user->getSpecialPrice()
                                    ),
                            'user_details'=>$user_details
                            ));

                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
        
    }
    
    
    
    public function discountUpdateAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('user_type') != '' && isset($_POST["discountCode"]) && isset($_POST["orderNumber"])){
            
            $_POST['orderNumber'] = filter_var($_POST['orderNumber'], FILTER_SANITIZE_STRING);
            $_POST['discountCode'] = filter_var($_POST['discountCode'], FILTER_SANITIZE_STRING);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('p')
                ->where('p.code = :code')
                ->setParameter('code', $_POST["discountCode"])
                ->getQuery();
            $data = $query->getArrayResult();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"], 'client_id'=>$client_id));
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            //$user->getCanGetSpecialPrice()
            
                
            
            $post = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $items_total_cost = 0;
            $items_total_gst = 0;
            $item_total = 0;

            
            
            //$item_total += $post[0]['doc_delivery_cost'] + ($post[0]['doc_delivery_cost'] * 0.10);
            $items_total_cost += $post['doc_delivery_cost'];
            $items_total_gst +=  $items_total_cost * 0.10;
            $item_total = $items_total_cost + $items_total_gst;
                
            $subtotal = $items_total_cost;
            $gst = $items_total_gst;
            
            if(count($data) > 0){
                
                $model->setDiscountCode($_POST['discountCode']);
                $model->setDiscountRate($data[0]["rate"]);
                
                $discount_rate = $data[0]["rate"];
                $subtotal_discount = $subtotal * ($discount_rate / 100);
                $gst_discount = $gst * ($discount_rate / 100);
            }else{
                
                $model->setDiscountCode("");
                $model->setDiscountRate(0);
                
                $discount_rate = 0;
                $subtotal_discount = 0;
                $gst_discount = 0;
            }

            // subtotal
            $subtotal = $subtotal - $subtotal_discount;
            $gst = $gst - $gst_discount;
            
            
            $model->setGrandTotal($subtotal + $gst);
            if($user->getCanGetSpecialPrice() == 1){
                $model->setGrandTotal($model->getGrandTotal() + ($model->getGrandTotal() * ($user->getSpecialPrice()/100)));
            }
            
            $em->persist($model);
            $em->flush();
            
            if($session->get('user_type') == 'admin-user'){
                return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
            }else{
                return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$model->getOrderNo());
            }
        }else{
            
            return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$_POST['orderNumber']);
            
        }
    }
    
    
    
    
    /**
     * 4th Step: Place Order
     */
    
    public function placeOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-document-delivery-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'government' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST["hidSubmit"])){
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($order->getStatus() >= 10){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            $em->getConnection()->beginTransaction();

            
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            $_POST["paymentType"] = filter_var($_POST['paymentType'], FILTER_SANITIZE_STRING);
            if(isset($_POST["accountNumber"])){
                $_POST["accountNumber"] = filter_var($_POST['accountNumber'], FILTER_SANITIZE_STRING);
            }
            
            $model = new Payment();
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($client_id);
            $model->setOrderNo($_POST["orderNumber"]);
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaOrganisationName($_POST['company']);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption($_POST["paymentType"]);
            if(isset($_POST["accountNumber"])){
                $model->setAccountNo($_POST["accountNumber"]);
            }
            
            if($_POST["paymentType"] == 0 || ($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2)){
                $total_order_price = $order->getGrandTotal();
            }else{
                $total_order_price = $order->getGrandTotal() + ($order->getGrandTotal() * ($this->getCreditCardProcessingFee()/100));
            }
            
            //$total_order_price = round($total_order_price);
            $total_order_price = number_format($total_order_price, 2,'.','');
            $model->setTotalOrderPrice($total_order_price);
//            if($session->get('user_type') == 'admin-user' && $_POST["paymentType"] == 2){
//                $model->setSPaid(1);
//            }else{
//                $model->setSPaid(0);
//            }
            if($_POST["paymentType"] == 1){
                $model->setSPaid(1);
            }else{
                $model->setSPaid(2);
            }
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'company'=>$_POST['company'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'paymentType'=>$_POST["paymentType"],
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            if($error_count == 0){
                if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors = array();
                    $special_error=1;
                }
                
                if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                    $error_count += 1;
                }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                    $errors[] = array('message'=>'Account number is not correct!');
                    $error_count += 1;
                }
            }
            
            
            if($error_count == 0){
                
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $totalAmount = $total_order_price * 100;
                   
                   $fields = array(
                       'customerFirstName' => urlencode($_POST['fname']),
                       'customerLastName' => urlencode($_POST['lname']),
                       'customerEmail' => urlencode($_POST['email']),
                       'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $data['mba_country']),
                       'customerPostcode' => urlencode($_POST['postcode']),
                       'customerInvoiceDescription' => urlencode('CLS Visa Application'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');


                  $root_dir = dirname($this->get('kernel')->getRootDir());
                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               
               /**
                * Credit card payment
                * Position: End
                */

                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );


                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationPoliceClearance:placeOrder.html.twig',
                            array('order_no'=> $_POST["orderNumber"],
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'cardtypes'=> $this->getCardTypes(),
                                'errors'=>$errors,
                                'post'=>$post,
                                'user_details'=>$user_details
                                ));
                }

            }else{
                
                //$order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                $order->setStatus(10); // 10= Order placed
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $order->setGrandTotal($total_order_price);
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                
                $em->persist($order);
                $em->flush();
                
                
                
                

                

                
                //$em = $this->getDoctrine()->getManager();
                    
                //$host = $request->getScheme().'://'.$request->getHost();
                
                
                
                $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
                
                // email invoice
                if(!isset($_POST['dont_send_invoice'])){
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Document Delivery Order Receipt", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$order->getOrderNo(),
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data
                                    )
                                )
                        );


                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Courier Booking Submission",
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:booking_submission.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$order->getOrderNo(),
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data
                                    )
                                )
                        );
                }
                // manual order email to admin
                if($session->get('user_type') == 'admin-user'){ 
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "Document Delivery Manual Order Receipt", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'manual_order'=>1
                                        )
                                    )
                            );


                        $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "Courier Booking Submission - Manual Order",
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:booking_submission.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'manual_order'=>1
                                        )
                                    )
                            );
                    }
                }
                
                
                if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                    // email invoice (copy to admins)
                    if(!isset($_POST['dont_send_invoice'])){
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmail($email_copies, "help@capitallinkservices.com.au", "Document Delivery Order Receipt (Copy)", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    )
                            );
                    }
                }
                if($session->get('user_type') == 'admin-user'){
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Application submitted!'
                        );
                }else{
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'A link to print the consignment note has been emailed to you, along with the tax invoice. Please ensure the consignment note is provided to CLS upon pickup.'
                        );
                }

                if($session->get('user_type') == 'admin-user'){
                    /* + create log */
                    $logDetails = "New Document Delivery application (order no. ".$_POST['orderNumber'].") has been manually created and submitted by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }else{
                    /* + create log */
                    $logDetails = "New Document Delivery application (order no. ".$_POST['orderNumber'].") has been created and submitted by <a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$session->get('client_id')."' target='_blank'>" . $session->get('fname') . " " . $session->get('lname') . "</a>";
                    $this->createLog('client', $session->get('client_id'), $session->get('user_type'), $logDetails);
                    /* - create log */
                }
                
                // commit changes
                $em->getConnection()->commit(); 
                
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
        
            if(count($data) > 0 && $data['order_type'] == 7){
                // if order status done with the review order page
                // if not, send to review order page
                if( $data["status"] == 3 ){


                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:placeOrder.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'user_details'=>$user_details
                            ));

                }else{

                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$_GET["order"]);
                    }
                }

            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
    }

}
