<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDlChecklist;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class ApplicationDocumentLegalisationController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-document-legalisation');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
//        if($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
//            return $this->redirect($this->generateUrl('acme_cls_client_login'));
//        }
        if($session->get('user_type') == ''){
            $session->set("redirect_url", $this->generateUrl('cls_client_application_document_legalisation_details'));
            if(!isset($_POST['dl_type'])){
                return $this->redirect($this->generateUrl('acme_cls_client_login'));
            }
            
            $session->set("public_reg", "signup");
            $dl_checklist = array();
            for($i=0; $i<count($_POST['dl_type']); $i++){
                $dl_checklist[] = array('type'=>$_POST["dl_type"][$i],
                        'number'=>$_POST["dl_number"][$i],
                        'note'=>$_POST["dl_note"][$i]
                    );
            }

            $form_post = array(
                'dl_company'=>$_POST["dl_company"],
                'dl_address'=>$_POST["dl_address"],
                'dl_city'=>$_POST["dl_city"],
                'dl_state'=>$_POST["dl_state"],
                'dl_postcode'=>$_POST["dl_postcode"],
                'dl_contact_name'=>$_POST["dl_contact_name"],
                'dl_mobile'=>$_POST["dl_mobile"],
                'dl_email'=>$_POST["dl_email"],
                'dl_date_doc_returned'=>$_POST["dl_date_doc_returned"],
                'dl_embassy'=>$_POST["dl_embassy"],
                'dl_ref_no'=>$_POST["dl_ref_no"],
                'dl_com_invoice_no'=>$_POST["dl_com_invoice_no"],
                'dl_payment_type'=>$_POST["dl_payment_type"],
                'visa_courier'=>$_POST["visa_courier"],
                'visa_mdd_company'=>$_POST["visa_mdd_company"],
                'visa_mdd_address'=>$_POST["visa_mdd_address"],
                'visa_mdd_city'=>$_POST["visa_mdd_city"],
                'visa_mdd_state'=>$_POST["visa_mdd_state"],
                'visa_mdd_postcode'=>$_POST["visa_mdd_postcode"],
                'visa_mdd_fname'=>$_POST["visa_mdd_fname"],
                'visa_mdd_lname'=>$_POST["visa_mdd_lname"],
                'visa_mdd_contact'=>$_POST["visa_mdd_contact"],
                'visa_additional_comment'=>$_POST["visa_additional_comment"],
                'visa_additional_comment'=>$_POST["visa_additional_comment"],
                'dl_checklist'=>$dl_checklist
            );
            $session->set("doc_legalisation_post", $form_post);
                
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }else{
            if(isset($_POST['public'])){
                $dl_checklist = array();
                for($i=0; $i<count($_POST['dl_type']); $i++){
                    $dl_checklist[] = array('type'=>$_POST["dl_type"][$i],
                            'number'=>$_POST["dl_number"][$i],
                            'note'=>$_POST["dl_note"][$i]
                        );
                }

                $form_post = array(
                    'dl_company'=>$_POST["dl_company"],
                    'dl_address'=>$_POST["dl_address"],
                    'dl_city'=>$_POST["dl_city"],
                    'dl_state'=>$_POST["dl_state"],
                    'dl_postcode'=>$_POST["dl_postcode"],
                    'dl_contact_name'=>$_POST["dl_contact_name"],
                    'dl_mobile'=>$_POST["dl_mobile"],
                    'dl_email'=>$_POST["dl_email"],
                    'dl_date_doc_returned'=>$_POST["dl_date_doc_returned"],
                    'dl_embassy'=>$_POST["dl_embassy"],
                    'dl_ref_no'=>$_POST["dl_ref_no"],
                    'dl_com_invoice_no'=>$_POST["dl_com_invoice_no"],
                    'dl_payment_type'=>$_POST["dl_payment_type"],
                    'visa_courier'=>$_POST["visa_courier"],
                    'visa_mdd_company'=>$_POST["visa_mdd_company"],
                    'visa_mdd_address'=>$_POST["visa_mdd_address"],
                    'visa_mdd_city'=>$_POST["visa_mdd_city"],
                    'visa_mdd_state'=>$_POST["visa_mdd_state"],
                    'visa_mdd_postcode'=>$_POST["visa_mdd_postcode"],
                    'visa_mdd_fname'=>$_POST["visa_mdd_fname"],
                    'visa_mdd_lname'=>$_POST["visa_mdd_lname"],
                    'visa_mdd_contact'=>$_POST["visa_mdd_contact"],
                    'visa_additional_comment'=>$_POST["visa_additional_comment"],
                    'visa_additional_comment'=>$_POST["visa_additional_comment"],
                    'dl_checklist'=>$dl_checklist
                );
                $session->set("doc_legalisation_post", $form_post);
            }
        }
        
        if(isset($_POST["hidSubmit"])){
            $session->set("doc_legalisation_post","");
            
            if($_POST['dltime'] != $session->get("dltime")){
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_gov_home'));
                }
            }
            
            
//            if($session->get('user_type') == 'admin-user'){
//                $_POST["client_id"] = filter_var($_POST['client_id'], FILTER_SANITIZE_STRING);
//                $_POST["client_id"] = intval($_POST["client_id"]);
//            }
            
            $_POST["dl_company"] = filter_var($_POST['dl_company'], FILTER_SANITIZE_STRING);
            $_POST["dl_company"] = str_replace("&#39;","'", $_POST["dl_company"]);
            $_POST["dl_address"] = filter_var($_POST['dl_address'], FILTER_SANITIZE_STRING);
            $_POST["dl_address"] = str_replace("&#39;","'", $_POST["dl_address"]);
            $_POST["dl_city"] = filter_var($_POST['dl_city'], FILTER_SANITIZE_STRING);
            $_POST["dl_state"] = filter_var($_POST['dl_state'], FILTER_SANITIZE_STRING);
            $_POST["dl_postcode"] = filter_var($_POST['dl_postcode'], FILTER_SANITIZE_STRING);
            $_POST["dl_contact_name"] = filter_var($_POST['dl_contact_name'], FILTER_SANITIZE_STRING);
            $_POST["dl_contact_name"] = str_replace("&#39;","'", $_POST["dl_contact_name"]);
            $_POST["dl_mobile"] = filter_var($_POST['dl_mobile'], FILTER_SANITIZE_STRING);
            $_POST["dl_email"] = filter_var($_POST['dl_email'], FILTER_SANITIZE_STRING);
            $_POST["dl_date_doc_returned"] = filter_var($_POST['dl_date_doc_returned'], FILTER_SANITIZE_STRING);
            $_POST["dl_embassy"] = filter_var($_POST['dl_embassy'], FILTER_SANITIZE_STRING);
            $_POST["dl_embassy"] = str_replace("&#39;","'", $_POST["dl_embassy"]);
            $_POST["dl_ref_no"] = filter_var($_POST['dl_ref_no'], FILTER_SANITIZE_STRING);
            $_POST["dl_com_invoice_no"] = filter_var($_POST['dl_com_invoice_no'], FILTER_SANITIZE_STRING);
            
            $_POST["courier"] = filter_var($_POST['courier'], FILTER_SANITIZE_STRING);
            
            $_POST["docReturnCompany"] = filter_var($_POST['docReturnCompany'], FILTER_SANITIZE_STRING);
            $_POST["docReturnCompany"] = str_replace("&#39;","'", $_POST["docReturnCompany"]);
            $_POST["docReturnAddress"] = filter_var($_POST['docReturnAddress'], FILTER_SANITIZE_STRING);
            $_POST["docReturnAddress"] = str_replace("&#39;","'", $_POST["docReturnAddress"]);
            $_POST["docReturnCity"] = filter_var($_POST['docReturnCity'], FILTER_SANITIZE_STRING);
            $_POST["docReturnState"] = filter_var($_POST['docReturnState'], FILTER_SANITIZE_STRING);
            $_POST["docReturnPostCode"] = filter_var($_POST['docReturnPostCode'], FILTER_SANITIZE_STRING);
            $_POST["docReturnFname"] = filter_var($_POST['docReturnFname'], FILTER_SANITIZE_STRING);
            $_POST["docReturnFname"] = str_replace("&#39;","'", $_POST["docReturnFname"]);
            $_POST["docReturnLname"] = filter_var($_POST['docReturnLname'], FILTER_SANITIZE_STRING);
            $_POST["docReturnLname"] = str_replace("&#39;","'", $_POST["docReturnLname"]);
            $_POST["docReturnContactNo"] = filter_var($_POST['docReturnContactNo'], FILTER_SANITIZE_STRING);
            $_POST["additionalComments"] = filter_var($_POST['additionalComments'], FILTER_SANITIZE_STRING);
            
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            
            
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            // new order
            if(!isset($_POST["orderNumber"])){
                $order = new Orders();
                $order->setClientId($client_id);
                $order->setOrderType(9);
            // update order
            }else{
                $_POST["orderNumber"] = intval($_POST["orderNumber"]);
                $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                if(count($order) == 0){
                    $order = new Orders();
                    $order->setClientId($client_id);
                    $order->setOrderType(9);
                }
            }
            
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            
            $order->setDlCompany($_POST["dl_company"]);
            $order->setDlAddress($_POST["dl_address"]);
            $order->setDlCity($_POST["dl_city"]);
            $order->setDlState($_POST["dl_state"]);
            $order->setDlPostcode($_POST["dl_postcode"]);
            $order->setDlNationality($_POST["dl_nationality"]);
            $order->setDlContactName($_POST["dl_contact_name"]);
            $order->setDlMobile($_POST["dl_mobile"]);
            $order->setDlEmail($_POST["dl_email"]);
            $order->setDlDateDocReturned($mod->changeFormatToOriginal($_POST["dl_date_doc_returned"]));
            $order->setDlEmbassy($_POST["dl_embassy"]);
            $order->setDlRefNo($_POST["dl_ref_no"]);
            $order->setDlComInvoiceNo($_POST["dl_com_invoice_no"]);
            $order->setDlPaymentType($_POST["dl_payment_type"]);
            
            
            $order->setVisaMddCompany($_POST["docReturnCompany"]);
            $order->setVisaMddAddress($_POST["docReturnAddress"]);
            $order->setVisaMddCity($_POST["docReturnCity"]);
            $order->setVisaMddState($_POST["docReturnState"]);
            $order->setVisaMddPostcode($_POST["docReturnPostCode"]);
            $order->setVisaMddFname($_POST["docReturnFname"]);
            $order->setVisaMddLname($_POST["docReturnLname"]);
            $order->setVisaMddContact($_POST["docReturnContactNo"]);
            $order->setVisaAdditionalComment($_POST["additionalComments"]);
            
            $order->setVisaCourier($_POST["courier"]);
            
            if($_POST["hidSubmit"] == 1){
                $order->setStatus(10); 
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
            }else{
                $order->setStatus(1); 
            }
            if($session->get('user_type') == 'admin-user'){
                $order->setVisaClsTeamMember($session->get('admin_id'));
            }
                
            $em->persist($order);
            $em->flush();
            
            
            
            if($order->getStatus() == 10){
                $payment = new Payment();
                $payment->setClientId($client_id);
                $payment->setOrderNo($order->getOrderNo());
                
                $em->persist($payment);
                $em->flush();
            }
            
            if(isset($_POST["orderNumber"])){
                // delete destinations
                $query = $em->createQuery('DELETE AcmeCLSclientGovBundle:OrderDlChecklist c WHERE c.order_no = '.$order->getOrderNo());
                $query->execute(); 
            }
            
            $order_dl_checklist = array();
            for($i=0; $i<count($_POST['dl_type']); $i++){
                $_POST["dl_type"][$i] = filter_var($_POST["dl_type"][$i], FILTER_SANITIZE_STRING);
                $_POST["dl_number"][$i] = filter_var($_POST["dl_number"][$i], FILTER_SANITIZE_STRING);
                $_POST["dl_note"][$i] = filter_var($_POST["dl_note"][$i], FILTER_SANITIZE_STRING);
                
                $checklist = new OrderDlChecklist();
                $checklist->setOrderNo($order->getOrderNo());
                $checklist->setType($_POST["dl_type"][$i]);
                $checklist->setNumber($_POST["dl_number"][$i]);
                $checklist->setNote($_POST["dl_note"][$i]);
                $em->persist($checklist);
                $em->flush();
                
                $order_dl_checklist[] = array('type'=>$_POST["dl_type"][$i],
                        'number'=>$_POST["dl_number"][$i],
                        'note'=>$_POST["dl_note"][$i]
                    );
            }
            
            
            $validation_error_count = 0;
            if($order->getStatus() == 10){
                $blank_inputs = array();
                if(trim($_POST["dl_company"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_company';
                }
                
                if(trim($_POST["dl_nationality"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_nationality';
                }
                
                if(trim($_POST["dl_address"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_address';
                }
                
                if(trim($_POST["dl_city"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_city';
                }
                
                if(trim($_POST["dl_state"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_state';
                }
                
                if(trim($_POST["dl_postcode"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_postcode';
                }
                
                if(trim($_POST["dl_contact_name"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_contact_name';
                }
                if(trim($_POST["dl_mobile"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_mobile';
                }
                if(trim($_POST["dl_email"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_email';
                }
                if(trim($_POST["dl_date_doc_returned"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_date_doc_returned';
                }
                if(trim($_POST["dl_embassy"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_embassy';
                }
                if(trim($_POST["dl_ref_no"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_ref_no';
                }
                if(trim($_POST["dl_com_invoice_no"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'dl_com_invoice_no';
                }
                
                if(trim($_POST["courier"]) == ""){
                    $validation_error_count += 1;
                    $blank_inputs[] = 'courier';
                }
                
                for($i=0; $i<count($_POST['dl_type']); $i++){
                    if(trim($_POST["dl_type"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'dl_type'.$i;
                    }
                    if(trim($_POST["dl_number"][$i]) == ""){
                        $validation_error_count += 1;
                        $blank_inputs[] = 'dl_number'.$i;
                    }
                }
            }
            
            $post = array(
                'dl_company' => $_POST["dl_company"],
                'dl_address' => $_POST["dl_address"],
                'dl_city'=>$_POST["dl_city"],
                'dl_state'=>$_POST["dl_state"],
                'dl_postcode'=>$_POST["dl_postcode"],
                'dl_contact_name' => $_POST["dl_contact_name"],
                'dl_mobile' => $_POST["dl_mobile"],
                'dl_email' => $_POST["dl_email"],
                'dl_date_doc_returned' => $_POST["dl_date_doc_returned"],
                'dl_embassy' => $_POST["dl_embassy"],
                'dl_nationality'=> $_POST["dl_nationality"],
                'courier' => $_POST["courier"],
                'docReturnCompany' => $_POST["docReturnCompany"],
                'docReturnAddress' => $_POST["docReturnAddress"],
                'docReturnCity' => $_POST["docReturnCity"],
                'docReturnState' => $_POST["docReturnState"],
                'docReturnPostCode' => $_POST["docReturnPostCode"],
                'docReturnFname' => $_POST["docReturnFname"],
                'docReturnLname' => $_POST["docReturnLname"],
                'docReturnContactNo' => $_POST["docReturnContactNo"],
                'additionalComments' => $_POST["additionalComments"],
                'dl_checklist' => $order_dl_checklist,
                'dl_ref_no'=> $_POST['dl_ref_no'],
                'dl_com_invoice_no'=> $_POST['dl_com_invoice_no'],
                'blank_inputs'=>(isset($blank_inputs)) ? $blank_inputs : array()
            );
            
            $validator = $this->get('validator');
            $errors = $validator->validate($order);
            $error_count = count($errors);
            
            if($error_count > 0 || $validation_error_count > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                if($validation_error_count > 0){
                    $this->get('session')->getFlashBag()->add(
                            'validation-error',
                            'Please check the highlighted required fields and complete the form.'
                        );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors
                    );
                }
                
                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($client_id);
                }else{
                    $user_details = '';
                }
                
                
                return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:index.html.twig',
                        array(
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles(),
                            'user_details'=>$user_details,
                            'order_no'=>(isset($_GET["order"])) ? $_GET["order"] : '',
                            'visa_courier_options' => $this->getCourierOptions(),
                            'special_price'=> $user->getSpecialPrice(),
                            'post'=>$post
                        )
                    );
                
            }else{
                
                // commit changes
                $em->getConnection()->commit(); 
                
                $session->set("dltime", time());
                // if save only
                if($order->getStatus() == 1){
                    // set flash message
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been saved'
                        );
                    
                    // return to tpn page
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_document_legalisation_details') . "?client=".$client_id."&order=".$order->getOrderNo());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_document_legalisation_details') . "?order=".$order->getOrderNo());
                    }
                    
                }else{
                    $user = $this->getClientDetailsById($client_id);
                    $data = $this->getOrderDetailsByOrderNoAndClientId($order->getOrderNo(), $client_id);
                    
                    if($session->get('user_type') == 'admin-user'){
                        /* + create log */
                        $logDetails = "New Document Legalisation application (order no. ".$order->getOrderNo().") has been manually created and submitted by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }else{
                        /* + create log */
                        $logDetails = "New Document Legalisation application (order no. ".$order->getOrderNo().") has been created and submitted by <a href='".$this->generateUrl('acme_cls_admin_clients_edit')."?id=".$session->get('client_id')."' target='_blank'>" . $session->get('fname') . " " . $session->get('lname') . "</a>";
                        $this->createLog('client', $session->get('client_id'), $session->get('user_type'), $logDetails);
                        /* - create log */
                    }
                    if(!isset($_POST['dont_send_invoice'])){
                        $this->sendEmailWithAttachment($user['email'], "help@capitallinkservices.com.au", "Document Legalisation", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:email.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$order->getOrderNo(),
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'post'=>$data,
                                    'user'=>$user
                                    )
                                ),
                                //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                array(
                                    '0'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Doc-Legalisation_150319.pdf')
                                    //'1'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Document_Legalisation_order_form_.pdf')
                                )
                        );
                    }
                    
                    // manual order email to admin
                    if($session->get('user_type') == 'admin-user'){
                        if(!isset($_POST['dont_send_invoice'])){
                            $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Document Legalisation - Manual order", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:email.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'post'=>$data,
                                        'user'=>$user,
                                        'manual_order'=>1
                                        )
                                    ),
                                    //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                    array(
                                    '0'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Doc-Legalisation_150319.pdf')
                                    //'1'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Document_Legalisation_order_form_.pdf')
                                )
                            );
                        }
                    }
                    
                    if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                        // email invoice (copy to admins)
                        if(!isset($_POST['dont_send_invoice'])){
                            $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');

                            $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Document Legalisation (Copy)", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:email.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$order->getOrderNo(),
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'post'=>$data,
                                        'user'=>$user
                                        )
                                    ),
                                    //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                                    array(
                                    '0'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Doc-Legalisation_150319.pdf')
                                    //'1'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Document_Legalisation_order_form_.pdf')
                                )
                            );
                        }
                    }
                    
                    $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );
                    
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('acme_cls_admin_clients_edit')."?user=".$order->getClientId());
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_gov_home'));
                    }
                    
                }
            }
        }else{
            if($session->get('user_type') == 'admin-user'){
                if(isset($_GET['client'])){
                    $_GET['client'] = intval($_GET['client']);
                }else{
                    return $this->redirect($this->generateUrl('acme_cls_admin_home'));
                }
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $user_details = '';
            }
            
            
            // if order number is set
            if(isset($_GET["order"])){
                $_GET["order"] = intval($_GET["order"]);
                $client_id = ($session->get('user_type') == 'admin-user') ? $_GET['client'] : $session->get('client_id');
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
                $post = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
                
                if(count($post) > 0 && $post['order_type'] == 9){
                    $session->set("dltime", time());
                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:index.html.twig',
                        array(
                            'countries'=> $this->getCountries(),
                            'name_titles'=> $this->getNameTitles(),
                            'user_details'=>$user_details,
                            'order_no'=>$_GET["order"],
                            'visa_courier_options' => $this->getCourierOptions(),
                            'special_price'=> $user->getSpecialPrice(),
                            'post'=>$post
                        )
                    );
                }else{
                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order"].' not found!' 
                        ));
                }
            }else{
                
                if($session->get('user_type') == 'admin-user'){
                    $_GET['client'] = intval($_GET['client']);
                    $client_id = $_GET['client'];
                    $user_details = $this->getClientDetailsById($_GET['client']);
                }else{
                    $client_id = $session->get('client_id');
                    $user_details = '';
                }
                
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));

                $session->set("dltime", time());
                return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:index.html.twig',
                    array(
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'user_details'=>$user_details,
                        'visa_courier_options' => $this->getCourierOptions(),
                        'special_price'=> $user->getSpecialPrice()
                    )
                );
            }
        }
    }
    
    
    
    
    /**
     * 2nd Step: Review Order
     */
    public function reviewOrderAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-document-legalisation-review-order');
        
        if($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate' && $session->get('user_type') != 'admin-user'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        // if save progress
        if(isset($_POST["hidSubmit"])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
            $client_id = intval($client_id);
            $_POST['orderNumber'] = intval($_POST['orderNumber']);
            
            
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            
            if($_POST["hidSubmit"] == 1){
                $model->setStatus(3); // 3= Place Order
            }else{
                $model->setStatus(2); // 2= Review Order
            }
            $em->persist($model);
            $em->flush();
            
            $em->getConnection()->commit(); 
            
            
            // if save only
            if($model->getStatus() == 2){

                // return to review visa page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$model->getOrderNo());
                }
            }else{

                // return to review order page
                if($session->get('user_type') == 'admin-user'){
                    return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?client=".$client_id."&order=".$model->getOrderNo());
                }else{
                    return $this->redirect($this->generateUrl('cls_client_aplication_document_delivery_review_order') . "?order=".$model->getOrderNo());
                }
            }
            
            
            
        }else{
            
            $_GET["order"] = intval($_GET["order"]);
            if($session->get('user_type') == 'admin-user'){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $client_id = $session->get('client_id');
                $user_details = '';
            }
            
            $em = $this->getDoctrine()->getManager();
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            
            if(count($data) > 0 && $data['order_type'] == 9){
                // if order status done with the review tpn page
                // if not, send to review tpn page
                if( $data["status"] >= 1 ){

                    return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:reviewOrder.html.twig',
                        array('order_no'=> $_GET["order"],
                            'data'=> $data,
                            'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                        'special_price'=> $user->getSpecialPrice()
                                    ),
                            'user_details'=>$user_details
                            ));

                }else{
                    if($session->get('user_type') == 'admin-user'){
                        return $this->redirect($this->generateUrl('cls_client_application_document_legalisation_details') . "?client=".$client_id."&order=".$_GET["order"]);
                    }else{
                        return $this->redirect($this->generateUrl('cls_client_application_document_legalisation_details') . "?order=".$_GET["order"]);
                    }
                }
            }else{
                return $this->render('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
        }
        
        
    }

}
