<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Model;

class ViewOrderController extends GlobalController
{
    public function tpnOrderAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-view-order');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
        $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order_no"], $session->get("client_id"));
        
        if(count($data) > 0 && ($data['order_type'] == 2 || $data['order_type'] == 3)){
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:view_tpn_order.html.twig',
                array('post'=> $data,
                        'countries' => $this->getCountries(),
                        'name_titles' => $this->getNameTitles(),
                        'passport_types' => $this->getPassportTypes(),
                        'departments' => $this->getDepartments(),
                        'site_url' => $mod->siteURL()
                    )
                );
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:error.html.twig',
                array(
                    'title'=> 'Not Found',
                    'message'=> 'Order number '.$_GET["order_no"].' not found!'
                ));
        }
    }
    
    public function courierServiceOrderAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-view-order');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
        $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order_no"], $session->get("client_id"));
        
        if(count($data) > 0 && $data['order_type'] == 4){
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:passport.html.twig',
                array('post'=> $data,
                        'countries' => $this->getCountries(),
                        'name_titles' => $this->getNameTitles(),
                        'passport_types' => $this->getPassportTypes(),
                        'departments' => $this->getDepartments(),
                        'site_url' => $mod->siteURL()
                    )
                );
        }else{
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:error.html.twig',
                        array(
                            'title'=> 'Not Found',
                            'message'=> 'Order number '.$_GET["order_no"].' not found!' 
                        ));
        }
    }
    
    
    
    public function publicVisaOrderAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-view-order');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
        $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order_no"], $session->get("client_id"));
        
        if(count($data) > 0 && $data['order_type'] == 6){
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:view_public_visa_order.html.twig',
                array('post'=> $data,
                        'countries' => $this->getCountries(),
                        'name_titles' => $this->getNameTitles(),
                        'passport_types' => $this->getPassportTypes(),
                        'departments' => $this->getDepartments(),
                        'site_url' => $mod->siteURL()
                    )
                );
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:error.html.twig',
                array(
                    'title'=> 'Not Found',
                    'message'=> 'Order number '.$_GET["order_no"].' not found!'
                ));
        }
    }
    
    
    public function visaOrderAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-view-order');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(isset($_POST['order_no'])){
            $em = $this->getDoctrine()->getManager();
            for($i=0; $i<count($_POST['travelId']); $i++){
                $model = $em->getRepository('AcmeCLSclientGovBundle:OrderDestinations')->findOneBy(array('id'=>$_POST['travelId'][$i]));
                $model->setVisaFollowUpDate($mod->changeFormatToOriginal($_POST["followUpDate"][$i]));
                $em->persist($model);
                $em->flush();
            }
        }
        
        $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
        $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order_no"], $session->get("client_id"));
        
        
        
        
        if(count($data) > 0 && ($data['order_type'] == 1 || $data['order_type'] == 3 || $data['order_type'] == 6)){
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:view_visa_order.html.twig',
                array('post'=> $data,
                        'countries' => $this->getCountries(),
                        'name_titles' => $this->getNameTitles(),
                        'passport_types' => $this->getPassportTypes(),
                        'departments' => $this->getDepartments(),
                        'site_url' => $mod->siteURL()
                    )
                );
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:error.html.twig',
                array(
                    'title'=> 'Not Found',
                    'message'=> 'Order number '.$_GET["order_no"].' not found!'
                ));
        }
    }
    
    
    public function policeClearanceOrderAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-view-order');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
        $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order_no"], $session->get("client_id"));
        
        if(count($data) > 0 && $data['order_type'] == 5){
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:view_police_clearance_order.html.twig',
                array('post'=> $data,
                        'countries' => $this->getCountries(),
                        'name_titles' => $this->getNameTitles(),
                        'passport_types' => $this->getPassportTypes(),
                        'departments' => $this->getDepartments(),
                        'site_url' => $mod->siteURL()
                    )
                );
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:error.html.twig',
                array(
                    'title'=> 'Not Found',
                    'message'=> 'Order number '.$_GET["order_no"].' not found!'
                ));
        }
    }
    
    
    
    public function documentDeliveryOrderAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-view-order');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
        $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order_no"], $session->get("client_id"));
        
        if(count($data) > 0 && $data['order_type'] == 7){
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:docDelivery.html.twig',
                        array('post'=> $data, 'site_url'=> $mod->siteURL())
                        );
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:error.html.twig',
                array(
                    'title'=> 'Not Found',
                    'message'=> 'Order number '.$_GET["order_no"].' not found!'
                ));
        }
    }
    
    public function russianVisaVoucherOrderAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-view-order');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
        $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order_no"], $session->get("client_id"));
        
        
        if(count($data) > 0 && $data['order_type'] == 8){
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:russianVisaVoucher.html.twig',
                        array('post'=> $data)
                        );
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:error.html.twig',
                array(
                    'title'=> 'Not Found',
                    'message'=> 'Order number '.$_GET["order_no"].' not found!'
                ));
        }
    }
    
    public function documentLegalisationOrderAction()
    {
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'gov-view-order');
        
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
        $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order_no"], $session->get("client_id"));
        
        
        if(count($data) > 0 && $data['order_type'] == 9){
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:docLegalisation.html.twig',
                        array('post'=> $data)
                        );
        }else{
            
            return $this->render('AcmeCLSclientGovBundle:ViewOrder:error.html.twig',
                array(
                    'title'=> 'Not Found',
                    'message'=> 'Order number '.$_GET["order_no"].' not found!'
                ));
        }
    }
    
    public function printDhlLabelAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        $_GET['order'] = intval($_GET['order']);
        $xml = $this->getShipmentValidationResponse($_GET['order']);
        $response = simplexml_load_string($xml);
        //print_r($response->LabelImage->); exit();

        // Store it as a . PDF file in the filesystem
        file_put_contents($root_dir .'/dev/dhl-label.pdf', base64_decode($response->LabelImage->OutputImage));

        // If you want to display it in the browser
        $data = base64_decode($response->LabelImage->OutputImage);
        
        if ($data)
        {
            header('Content-Type: application/pdf');
            header('Content-Length: ' . strlen($data));
            echo $data;
        }
    }

}
