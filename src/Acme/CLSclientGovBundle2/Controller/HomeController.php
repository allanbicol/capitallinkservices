<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;

class HomeController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel;
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'home');
        $new_pass =0;
        if ($session->get('pass')!=''){
            if($mod->passwordHardening($session->get('pass')) != '1#1#1#1'){
                $new_pass = 1;
                $session->set('pass', '');
            }
        }
        if($session->get('email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSclientGovBundle:Home:index.html.twig',array('new_pass'=>$new_pass));
    }
    
    
    public function paymentCurlAction(){
        
        $fields = array(
            'customerFirstName' => urlencode('Leo karl'),
            'customerLastName' => urlencode('Ladeno'),
            'customerEmail' => urlencode('leokarl2108@gmail.com'),
            'customerAddress' => urlencode('123 Someplace Street, Somewhere ACT'),
            'customerPostcode' => urlencode('2609'),
            'customerInvoiceDescription' => urlencode('Testing'),
            'customerInvoiceRef' => urlencode('INV120394'),
            'cardHoldersName' => urlencode('TestUser'),
            'cardNumber' => urlencode('4444333322221111'),
            'cardExpiryMonth' => urlencode('04'),
            'cardExpiryYear' => urlencode('14'),
            'trxnNumber' => urlencode('4230'),
            'totalAmount' => urlencode(200),
            'cvn' => urlencode(123)
        );
        
        
        //url-ify the data for the POST
        $fields_string = '';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        
        $root_dir = dirname($this->get('kernel')->getRootDir());
        //open connection
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");

        //execute post
        $response = curl_exec($ch);
        
        $result = json_decode($response, true);
        
        echo (isset($result['success'])) ? $result['success'] : '';
        echo '<br/>';
        echo (isset($result['error'])) ? $result['error'] : '';
        
        
        //close connection
        curl_close($ch);
    }
    
    public function changePasswordAction(){
        
        if(isset($_POST['password'])){
            
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$session->get('client_id')));
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }        
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            $error_code = 0;
            
                
            if($error_count == 0){
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $error_code =1;
                        $error_count += 1;
                    }
                }
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $error_code = 2;//'You must Follow the password required standard.';
                $error_count += 1;
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
               
                $this->sendEmail($session->get('email'), "help@capitallinkservices.com.au", "Change Password",
                                        $this->renderView('AcmeCLSadminBundle:Home:changePassword.html.twig',
                                        array('fname'=>$session->get('fname'),
                                            'password'=> $_POST['password'],
                                            'domain'=>$mod->siteURL()
                                            )
                                        ));
    
            }else{
                $em->getConnection()->rollback();
                $em->close();
                $error_code = 3;
            }
            
            return new Response($error_code);
        }
    }
}
