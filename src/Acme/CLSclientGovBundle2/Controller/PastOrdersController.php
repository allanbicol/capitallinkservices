<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSclientGovBundle\Model;

class PastOrdersController extends GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('email') == '' || $session->get('user_type') != 'government'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        $session->set('page_name', 'order-history');
        
        return $this->render('AcmeCLSclientGovBundle:PastOrders:index.html.twig');
        
    }
    
    
    public function publicAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('email') == '' || ($session->get('user_type') != 'public' && $session->get('user_type') != 'corporate')){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        $session->set('page_name', 'order-history');
        
        return $this->render('AcmeCLSclientGovBundle:PastOrders:public.html.twig');
        
    }
    
    
    
    
    
    public function pendingTpnlistAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, o.primary_traveller_name, c.countryName as destination, o.departure_date ")
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = o.destination')
                ->where('(o.status >= 10 AND o.status < 12) AND o.client_id = :client_id AND (o.order_type = 2 OR o.order_type = 3)')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("order_no","date_submitted","primary_traveller_name","destination","departure_date","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function pastTpnlistAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, o.primary_traveller_name, c.countryName as destination, o.departure_date ")
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = o.destination')
                ->where('o.status = 12 AND o.client_id = :client_id AND (o.order_type = 2 OR o.order_type = 3)')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("order_no","date_submitted","primary_traveller_name","destination","departure_date","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    
    public function pendingVisalistAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, o.primary_traveller_name, c.countryName as destination, o.departure_date ")
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = o.destination')
                ->where('(o.status >= 10 AND o.status < 12) AND o.client_id = :client_id AND (o.order_type = 1 OR o.order_type = 3 OR o.order_type = 6)')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("order_no","date_submitted","primary_traveller_name","destination","departure_date","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function pastVisalistAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, o.primary_traveller_name, c.countryName as destination, o.departure_date ")
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = o.destination')
                ->where('o.status = 12 AND o.client_id = :client_id AND (o.order_type = 1 OR o.order_type = 3 OR o.order_type = 6)')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("order_no","date_submitted","primary_traveller_name","destination","departure_date","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function courierlistAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, p.payment_option, p.total_order_price, p.s_paid, o.status")
                ->leftJoin('AcmeCLSclientGovBundle:Payment', 'p', 'WITH', 'p.order_no = o.order_no')
                ->where('o.status >= 10 AND o.client_id = :client_id AND o.order_type = 4')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("order_no","date_submitted","payment_option","total_order_price","status","s_paid");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function policeClearanceListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, o.primary_traveller_name,  c.name as police_clearance , p.payment_option, p.total_order_price, p.s_paid, o.status")
                ->leftJoin('AcmeCLSclientGovBundle:Payment', 'p', 'WITH', 'p.order_no = o.order_no')
                ->leftJoin('AcmeCLSadminBundle:PoliceClearances', 'c', 'WITH', 'c.id = o.police_clearance_id')
                ->where('o.status >= 10 AND o.client_id = :client_id AND o.order_type = 5')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("order_no","date_submitted","primary_traveller_name","police_clearance","payment_option","total_order_price","status","s_paid");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function documentDeliveryListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted,  d.type as doc_type , p.payment_option, p.total_order_price, o.status")
                ->leftJoin('AcmeCLSclientGovBundle:Payment', 'p', 'WITH', 'p.order_no = o.order_no')
                ->leftJoin('AcmeCLSadminBundle:SettingsDocumentDelivery', 'd', 'WITH', 'd.id = o.doc_delivery_type')
                ->where('o.status >= 10 AND o.client_id = :client_id AND o.order_type = 7')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("order_no","date_submitted","doc_type","payment_option","total_order_price","status");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function russianVisaVoucherListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, o.primary_traveller_name, rvv.name as russian_visa_voucher, p.total_order_price, o.status")
                ->leftJoin('AcmeCLSclientGovBundle:Payment', 'p', 'WITH', 'p.order_no = o.order_no')
                ->leftJoin('AcmeCLSadminBundle:RussianVisaVoucher', 'rvv', 'WITH', 'rvv.id = o.russian_visa_voucher_id')
                ->where('o.status >= 10 AND o.client_id = :client_id AND o.order_type = 8')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","order_no","primary_traveller_name","russian_visa_voucher","total_order_price","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function documentLegalisationAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, o.status")
                ->leftJoin('AcmeCLSclientGovBundle:Payment', 'p', 'WITH', 'p.order_no = o.order_no')
                ->where('o.status >= 10 AND o.client_id = :client_id AND o.order_type = 9')
                ->setParameter("client_id", $session->get("client_id"))
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","order_no","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    
    /**
     * TPN order deletion
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteTpnOrderAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('email') != '' && $session->get('user_type') == 'government'){
            $_POST['order_no'] = filter_var($_POST['order_no'], FILTER_SANITIZE_STRING);
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_POST['order_no'], $session->get('client_id'));
            
            if(count($data) > 0){
                
                $em = $this->getDoctrine()->getEntityManager();
                $em->getConnection()->beginTransaction();
                $connection = $em->getConnection();
                
                // destinations
                $statement = $connection->prepare("DELETE FROM tbl_order_destinations WHERE order_no=".$data['order_no']);
                $statement->execute();
                
                // travellers
                $statement = $connection->prepare("DELETE FROM tbl_order_travellers WHERE order_no=".$data['order_no']);
                $statement->execute();
                
                for($i=0; $i<count($data['tpns']); $i++){
                    // tpn notes
                    $statement = $connection->prepare("DELETE FROM tbl_tpn_notes WHERE tpn_no='".$data['tpns'][$i]['tpn_no']."'");
                    $statement->execute();
                }
                
                // tpns
                $statement = $connection->prepare("DELETE FROM tbl_tpn WHERE order_no=".$data['order_no']);
                $statement->execute();
                
                // tpns
                $statement = $connection->prepare("DELETE FROM tbl_orders WHERE order_no=".$data['order_no']);
                $statement->execute();
                
                $em->getConnection()->commit(); 
                
                return new Response("success");
            }else{
                return new Response("session expired");
            }
        }
    }
    
    
    

}
