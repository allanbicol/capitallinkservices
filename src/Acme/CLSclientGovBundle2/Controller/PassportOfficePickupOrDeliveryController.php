<?php

namespace Acme\CLSclientGovBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Entity\OrderDestinations;
use Acme\CLSclientGovBundle\Entity\OrderTravellers;
use Acme\CLSadminBundle\Entity\OrderPassportApplicants;
use Acme\CLSclientGovBundle\Entity\Payment;
use Acme\CLSclientGovBundle\Entity\Tpn;
use Acme\CLSclientGovBundle\Model;

class PassportOfficePickupOrDeliveryController extends GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'passport-office-pickup-delivery');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('email') == '' && $session->get('user_type') != 'government'){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$session->get('client_id')));
            
            // new order
            if(!isset($_POST["orderNumber"])){
                $order = new Orders();
                $order->setClientId($session->get("client_id"));
                $order->setOrderType(4);
            // update order
            }else{
                $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
                
            }
            $order->setPassportOfficeBookingNo($_POST['passportOfficeBookingNumber']);
            $order->setPassportOfficeBookingTime($_POST['passportOfficeBookingTime']);
            $order->setPassportOfficeBookingTimeHr($_POST['passportOfficeBookingTimeHour']);
            $order->setPassportOfficeBookingTimeMin($_POST['passportOfficeBookingTimeMin']);
            $order->setStatus(10); // 10= Order placed
            $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
            $em->persist($order);
            $em->flush();
            
            
            for($i=0; $i<count($_POST["passportApplicantFullname"]); $i++){
                $order_pa = new OrderPassportApplicants();
                $order_pa->setOrderNo($order->getOrderNo());
                $order_pa->setP($_POST["passportApplicantFullname"][$i]);
                $order_pa->setPersonalPassport($_POST['personalPassport'][$i]);
                $order_pa->setDiplomaticOfficialPassport($_POST['doPassport'][$i]);
                $order_pa->setBirthCertificate($_POST['birthCertificate'][$i]);
                $order_pa->setMarriageCertificate($_POST['marriageCertificate'][$i]);
                $order_pa->setCertificateOfAustralianCitizenship($_POST['certOfAustralianCitizenship'][$i]);
                $em->persist($order_pa);
                $em->flush();
            }
            
            
            $model = new Payment();
            $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
            $model->setClientId($session->get("client_id"));
            $model->setOrderNo($order->getOrderNo());
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $model->setAddress($_POST["address"]);
            $model->setCity($_POST["city"]);
            $model->setState($_POST["state"]);
            $model->setPostcode($_POST["postcode"]);
            $model->setCountryId($_POST["country"]);
            $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
            $model->setMbaAddress($_POST["billingAddress"]);
            $model->setMbaCity($_POST["billingCity"]);
            $model->setMbaState($_POST["billingState"]);
            $model->setMbaPostcode($_POST["billingPostcode"]);
            $model->setMbaCountryId($_POST["billingCountry"]);
            $model->setPaymentOption($_POST["paymentType"]);
            $model->setAccountNo($_POST["accountNumber"]);
            $model->setNameOnCard($_POST["nameOnCard"]);
            $model->setCardNumber($_POST["cardNumber"]);
            $model->setCardExpiryMonth($_POST["cardExpiryMonth"]);
            $model->setCardExpiryYear($_POST["cardExpiryYear"]);
            $model->setCardType($_POST["cardType"]);
            $model->setCcvNumber($_POST["ccvNumber"]);
            
            $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            
            $cost = $passport->getCost();
            
            if(count($_POST['passportApplicantFullname']) > 1){
                $cost += (count($_POST['passportApplicantFullname']) -1) * $passport->getAdditionalCost();
            }
            
            $model->setTotalOrderPrice($cost);
            
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'orderNumber'=>$_POST['orderNumber'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'department'=>$_POST['department'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry']
            );
            
            if($error_count > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors
                    );
                
                $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            
                return $this->render('AcmeCLSclientGovBundle:PassportOfficePickupOrDelivery:index.html.twig',
                    array(
                        'user'=>$this->getClientDetailsById($session->get("client_id")),
                        'credit_card_processing_fee'=>$this->getCreditCardProcessingFee(),
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'cardtypes'=> $this->getCardTypes(),
                        'departments'=>$this->getDepartments(),
                        'cost'=>$passport,
                        'post'=>$post
                    ));
            
                
            }else{
                
                
                // commit changes
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your order has been sent'
                        );
                return $this->redirect($this->generateUrl('cls_client_gov_home'));
                
            }
            
        }else{
            $em = $this->getDoctrine()->getManager();
            $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            
            return $this->render('AcmeCLSclientGovBundle:PassportOfficePickupOrDelivery:index.html.twig',
                    array(
                        'user'=>$this->getClientDetailsById($session->get("client_id")),
                        'credit_card_processing_fee'=>$this->getCreditCardProcessingFee(),
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'cardtypes'=> $this->getCardTypes(),
                        'departments'=>$this->getDepartments(),
                        'cost'=>$passport
                    ));
        }
    }

}
