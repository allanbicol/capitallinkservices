<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageContentPagesController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        
        
        
        if(isset($_POST['hidSubmit'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $_POST['page'] = intval($_POST['page']);
            $_POST['manageContent'] = strip_tags($_POST['manageContent'], $mod->acceptTagsExceptScript());
            
            $model = $em->getRepository('AcmeCLSadminBundle:ContentPages')->findOneBy(array('id'=>$_POST['page']));
            $model->setHtml($_POST['manageContent']);
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_content_pages')."?page=".$_POST['page']); 
                
            }else{
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors);
                
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSadminBundle:ManageContentPages:index.html.twig',
                    array('content'=> $this->getPageContentByNo($_POST['page'])
                    )
                );
            }
            
        }else{
        
            $_GET['page'] = intval($_GET['page']);
            $session->set('page_name', 'admin-content-pages-'.$_GET['page']);
            $session->set('page_class', 'admin-content-management');
            
            $data = $this->getPageContentByNo($_GET['page']);
            if(count($data) > 0){
                return $this->render('AcmeCLSadminBundle:ManageContentPages:index.html.twig',
                    array('content'=> $data)
                    );
            }else{
                return $this->render('AcmeCLSadminBundle:ManageContentPages:error.html.twig',
                        array('title' => 'Error',
                            'message' => "Can't find page.")
                        );
            }
        }
    }

}
