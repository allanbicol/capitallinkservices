<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;

class OffsiteBackupController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'offsite-backup');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        $directory = dirname($this->get('kernel')->getRootDir()) .'/web/db_backup';
        $files = $mod->getFilesInDirectory($directory);
        
        return $this->render('AcmeCLSadminBundle:OffsiteBackup:index.html.twig',
                    array(
                        'files'=>$files
                    )
                );
    }

}
