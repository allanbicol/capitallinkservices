<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSadminBundle\Entity\OrderDestinationNotes;
use Acme\CLSclientGovBundle\Entity\OrderNotes;
use Acme\CLSclientGovBundle\Entity\OrderDlChecklist;
use Acme\CLSadminBundle\Entity\OrderFollowUpDate;

class ViewOrderController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-view-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        $attachment_url = $mod->siteURL()."/web/";

        if ($mod->siteURL()=='http://localhost'){
            $attachment_url = $mod->siteURL()."/cls/web/";
        }
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        
        if(isset($_POST['updateTicket'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $odetails = $this->getOrderDetailsByOrderNo($_POST['orderNo']);
            $destinations = $this->getOrderDestinationsByOrderNo($_POST['orderNo']);
            $count_order_closed = 0;
            
            for($i=0; $i<count($destinations); $i++){
                $model = $em->getRepository('AcmeCLSclientGovBundle:OrderDestinations')->findOneBy(array('id'=>$destinations[$i]['id'], 'order_no'=>$_POST['orderNo']));  
                
                $notice_message = '';
                if($model->getVisaDateClsReceivedAllItems() != $mod->changeFormatToOriginal($_POST['allItemsReceivedAtCLS'][$destinations[$i]['id']])){
                    $notice_message = 'This is a courtesy email to inform you that your package has been received by CLS Capital Link Services, Canberra.';
                    
                    /* + create log */
                    $logDetails =$odetails['order_type_name'] . " application  (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been received by CLS Capital Link Services, Canberra - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */

                }elseif($model->getVisaDateSubmittedForProcessing() != $mod->changeFormatToOriginal($_POST['submittedForProcessing'][$destinations[$i]['id']])){
                    $notice_message = 'This is a courtesy email to inform you that your application has been submitted for visa processing';
                    
                    /* + create log */
                    $logDetails = $odetails['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been submitted for visa processing - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                    
                }elseif($model->getVisaDateCompletedAndReceivedAtCls() != $mod->changeFormatToOriginal($_POST['completedReceivedAtCLS'][$destinations[$i]['id']])){
                    //$notice_message = 'This is a courtesy email to inform you that your visa has been issued from the EMBASSY OF '. strtoupper($_POST['nextEmbassy']) .', and that it will be sent to you today. Thank you for using our services, we look forward to assisting you in the future.';
                    $notice_message = 'This is a courtesy email to inform you that your visa has been issued from the EMBASSY OF '. strtoupper($destinations[$i]['country_name_display']) .', and that it will be sent to you today. Thank you for using our services, we look forward to assisting you in the future.';
                    
                    /* + create log */
                    $logDetails = $odetails['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been issued from the EMBASSY OF ". strtoupper($_POST['nextEmbassy']) ." - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                    
                }elseif($model->getVisaDateOrderOnRouteAndClosed() != $mod->changeFormatToOriginal($_POST['orderOnRouteAndClosed'][$destinations[$i]['id']])){
                    
                    /* + create log */
                    $logDetails = $odetails['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) on route and closed - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                    
                }else{
                    $notice_message = 'Your current Order has been processed by our staff recently';
                    
                    /* + create log */
                    $logDetails = $odetails['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been processed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
                
                
                
                
                
                if(trim($_POST['allItemsReceivedAtCLS'][$destinations[$i]['id']]) != ''){
                    $model->setVisaDateClsReceivedAllItems($mod->changeFormatToOriginal($_POST['allItemsReceivedAtCLS'][$destinations[$i]['id']]));
                }else{
                    $model->setVisaDateClsReceivedAllItems(null);
                }
                if(trim($_POST['submittedForProcessing'][$destinations[$i]['id']]) != ''){
                    $model->setVisaDateSubmittedForProcessing($mod->changeFormatToOriginal($_POST['submittedForProcessing'][$destinations[$i]['id']]));
                }else{
                    $model->setVisaDateSubmittedForProcessing(null);
                }
                if(trim($_POST['completedReceivedAtCLS'][$destinations[$i]['id']]) != ''){
                    $model->setVisaDateCompletedAndReceivedAtCls($mod->changeFormatToOriginal($_POST['completedReceivedAtCLS'][$destinations[$i]['id']]));
                }else{
                    $model->setVisaDateCompletedAndReceivedAtCls(null);
                }
                if(trim($_POST['orderOnRouteAndClosed'][$destinations[$i]['id']]) != ''){
                    $model->setVisaDateOrderOnRouteAndClosed($mod->changeFormatToOriginal($_POST['orderOnRouteAndClosed'][$destinations[$i]['id']]));
                    $count_order_closed += 1;
                }else{
                    $model->setVisaDateOrderOnRouteAndClosed(null);
                }
                $model->setVisaShippedBy($_POST["shippedBy"][$destinations[$i]['id']]);
                $model->setVisaComNoteNo($_POST["comNoteNo"][$destinations[$i]['id']]);
                $model->setVisaComNoteIn($_POST["comNoteIn"][$destinations[$i]['id']]);
                $model->setVisaInvoiceNo($_POST["invoiceNo"][$destinations[$i]['id']]);
                
                $em->persist($model);
                $em->flush();
                
                if(trim($_POST['ticketComment'][$destinations[$i]['id']]) != ''){
                    $model = new OrderDestinationNotes();
                    $model->setNote($_POST['ticketComment'][$destinations[$i]['id']]);
                    $model->setDateAdded($datetime->format('Y-m-d H:i:s'));
                    $model->setDestinationId($destinations[$i]['id']);
                    $model->setNoteBy($session->get('admin_id'));
                    $model->setNoteByName($session->get('admin_fname'));
                    $model->setUserType('Admin');
                    $model->setIsAdmin(0);
                    if(trim($_FILES["comment_attachment"]["name"]) != ''){
                        $model->setAttachment(trim($_FILES["comment_attachment"]["name"]));
                    }
                    $em->persist($model);
                    $em->flush();
                    
                    if(trim($_FILES["comment_attachment"]["name"]) != ''){
                        //$mod->createDirectory($root_dir ."/dev/email-attachments/", $model->getNoticeId());
                        move_uploaded_file($_FILES["comment_attachment"]["tmp_name"], $root_dir ."/dev/destination_notes_file/". $_FILES["comment_attachment"]["name"]);
                    }
                
                    /* + create log */
                    $logDetails = $odetails['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) - comment was added by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
                if(trim($_POST['ticketAdminComment'][$destinations[$i]['id']]) != ''){
                    $model = new OrderDestinationNotes();
                    $model->setNote($_POST['ticketAdminComment'][$destinations[$i]['id']]);
                    $model->setDateAdded($datetime->format('Y-m-d H:i:s'));
                    $model->setDestinationId($destinations[$i]['id']);
                    $model->setNoteBy($session->get('admin_id'));
                    $model->setNoteByName($session->get('admin_fname'));
                    $model->setUserType('Admin');
                    $model->setIsAdmin(1);
                    if(trim($_FILES["admin_attachment"]["name"]) != ''){
                        $model->setAttachment(trim($_FILES["admin_attachment"]["name"]));
                    }
                    $em->persist($model);
                    $em->flush();
                    
                    if(trim($_FILES["admin_attachment"]["name"]) != ''){
                        //$mod->createDirectory($root_dir ."/dev/email-attachments/", $model->getNoticeId());
                        move_uploaded_file($_FILES["admin_attachment"]["tmp_name"], $root_dir ."/dev/destination_notes_file/". $_FILES["admin_attachment"]["name"]);
                    }
                    /* + create log */
                    $logDetails = $odetails['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) - admin comment was added by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
            }
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST['orderNo']));  
            $model->setVisaClsTeamMember($_POST['clsTeamMember']);
            $sDeliveredToEmbassy = (isset($_POST['sDeliveredToEmbassy'])) ? 1 : 0;
            $model->setVisaIsDeliveredToEmbassy($sDeliveredToEmbassy);
            $model->setVisaIsDeliveredToEmbassyDate($mod->changeFormatToOriginal($_POST['embassy_delivered_date']));
            $model->setVisaNextEmbassy($_POST['nextEmbassy']);
            
            
            if(count($destinations) == $count_order_closed){
                $model->setStatus(12);
            }
            $em->persist($model);
            $em->flush();
            
            
            // follow up date
            if(trim($_POST['follow_up_date']) != ''){
                $_POST['orderNo'] = intval($_POST['orderNo']);
                $query = $em->createQuery('DELETE AcmeCLSadminBundle:OrderFollowUpDate d WHERE d.order_id = '.$_POST['orderNo'].' AND d.admin_id='.$session->get("admin_id"));
                $query->execute(); 
                
                $fuDate = new OrderFollowUpDate();
                $fuDate->setAdminId($session->get("admin_id"));
                $fuDate->setOrderId($_POST['orderNo']);
                $old_fdate = $fuDate->getFollowUpDate();
                $fuDate->setFollowUpDate($mod->changeFormatToOriginal($_POST['follow_up_date']));
                $em->persist($fuDate);
                $em->flush();
                
            }
            
            $em->getConnection()->commit(); 
            
            $data = $this->getOrderDetailsByOrderNo($_POST["orderNo"]);
            
            if(!isset($_POST['chkSendUpdate'])){
                // email invoice
                $this->sendEmail($data['user_email'], "help@capitallinkservices.com.au", "Your current Order has been processed", 
                        $this->renderView('AcmeCLSadminBundle:ViewOrder:email_user.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNo"],
                                    'data'=>$data,
                                    'message'=>$notice_message
                                    )
                                )
                        );
            }
            return $this->redirect($this->generateUrl('acme_cls_admin_view_order') . "?order_no=".$_POST['orderNo']);
            
            
        }elseif(isset($_POST['updateStatus'])){
            
            $_POST['orderNo'] = filter_var($_POST['orderNo'], FILTER_SANITIZE_STRING);
            $data = $this->getOrderDetailsByOrderNo($_POST['orderNo']);
            
            
            if(count($data) > 0){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST['orderNo']));  
                $old_stat = $model->getStatus();
                $model->setStatus($_POST['orderStatus']);
                $em->persist($model);
                $em->flush();
                
                if($old_stat != $_POST['orderStatus']){
                    if($model->getStatus() == 10){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }elseif($model->getStatus() == 12){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to completed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }
                }

                $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST['orderNo']));  
                $old_stat = $model->getSpaid();
                $_POST['paymentStatus'] = intval($_POST['paymentStatus']);
                $model->setSPaid($_POST['paymentStatus']);    
                $em->persist($model);
                $em->flush();
                
                if($old_stat != $_POST['paymentStatus']){
                    if($_POST['paymentStatus'] == 0){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }elseif($_POST['paymentStatus'] == 1){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid online by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }elseif($_POST['paymentStatus'] == 2){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid by account by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }
                }
                
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Order has been updated.'
                );

                $em->getConnection()->commit(); 

                return $this->redirect($this->generateUrl('acme_cls_admin_view_order') . "?order_no=".$_POST['orderNo']);
            }
            
        }elseif(isset($_POST['updatePoliceClearanceOrder'])){
            $_POST['orderNo'] = filter_var($_POST['orderNo'], FILTER_SANITIZE_STRING);
            $data = $this->getOrderDetailsByOrderNo($_POST['orderNo']);
            
            if(count($data) > 0){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                
                
                $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST['orderNo']));  
                
                
                if($model->getPoliceClearanceDateClsReceivedAllItems() != $mod->changeFormatToOriginal($_POST['allItemsReceivedAtCLS'])){
                    
                    /* + create log */
                    $logDetails =$data['order_type_name'] . " application  (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been received by CLS Capital Link Services, Canberra - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */

                }elseif($model->getPoliceClearanceDateSubmittedForProcessing() != $mod->changeFormatToOriginal($_POST['submittedForProcessing'])){
                   
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been submitted for processing - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                    
                }elseif($model->getPoliceClearanceDateCompletedAndReceivedAtCls() != $mod->changeFormatToOriginal($_POST['completedReceivedAtCLS'])){
                    
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been completed and received at CLS - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                    
                }elseif($model->getPoliceClearanceDateOrderOnRouteAndClosed() != $mod->changeFormatToOriginal($_POST['orderOnRouteAndClosed'])){
                    
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) on route and closed - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                    
                }else{
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been processed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
                
                
                $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST['orderNo']));  
                if(trim($_POST['allItemsReceivedAtCLS']) != ''){
                    $model->setPoliceClearanceDateClsReceivedAllItems($mod->changeFormatToOriginal($_POST['allItemsReceivedAtCLS']));
                }
                if(trim($_POST['submittedForProcessing']) != ''){
                    $model->setPoliceClearanceDateSubmittedForProcessing($mod->changeFormatToOriginal($_POST['submittedForProcessing']));
                }
                if(trim($_POST['completedReceivedAtCLS']) != ''){
                    $model->setPoliceClearanceDateCompletedAndReceivedAtCls($mod->changeFormatToOriginal($_POST['completedReceivedAtCLS']));
                }
                if(trim($_POST['orderOnRouteAndClosed']) != ''){
                    $model->setPoliceClearanceDateOrderOnRouteAndClosed($mod->changeFormatToOriginal($_POST['orderOnRouteAndClosed']));
                }
                
                
                
                
                
                $model->setVisaClsTeamMember($_POST['clsTeamMember']);
                
                $model->setVisaMddCompany($_POST['drCompany']);
                $model->setVisaMddAddress($_POST['drAddress']);
                $model->setVisaMddCity($_POST['drCity']);
                $model->setVisaMddState($_POST['drState']);
                $model->setVisaMddPostcode($_POST['drPostcode']);
                $model->setVisaMddFname($_POST['drFname']);
                $model->setVisaMddLname($_POST['drLname']);
                $model->setVisaMddContact($_POST['drContactNo']);
                $model->setVisaAdditionalComment($_POST['drAdditionalComment']);
                
                $old_stat = $model->getStatus();
                if(trim($_POST['orderOnRouteAndClosed']) != ''){
                    $model->setStatus(12);
                }else{
                    if($_POST['orderStatus'] == 10){
                        $model->setStatus(10);
                    }else if($_POST['orderStatus'] == 11){
                        $model->setStatus(11);
                    }else if($_POST['orderStatus'] == 12){
                        $model->setStatus(12);
                    }
                }
                
                $em->persist($model);
                $em->flush();
                
                if($old_stat != $model->getStatus()){
                    if($_POST['orderStatus'] == 10){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }elseif($_POST['orderStatus'] == 11){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to paid by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }elseif($_POST['orderStatus'] == 12){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to completed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }
                }
                
                
                $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST['orderNo']));  
                $old_stat = $model->getSPaid();
//                $model->setAddress($_POST['drAddress']);
//                $model->setCity($_POST['drCity']);
//                $model->setPostcode($_POST['drPostcode']);
//                $model->setFname($_POST['drFname']);
//                $model->setLname($_POST['drLname']);
//                $model->setPhone($_POST['drContactNo']);
//                $model->setEmail($_POST['drEmail']);
//                $model->setAdditionalAddressDetails($_POST['drAdditionalComment']);
                $_POST['paymentStatus'] = intval($_POST['paymentStatus']);
                $model->setSPaid($_POST['paymentStatus']);
                
                $em->persist($model);
                $em->flush();
                
                if($old_stat != $_POST['paymentStatus']){
                    if($_POST['paymentStatus'] == 0){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }elseif($_POST['paymentStatus'] == 1){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid online by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }elseif($_POST['paymentStatus'] == 2){
                        /* + create log */
                        $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid by account by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                        /* - create log */
                    }
                }
                
                while ($applicant = current($_POST["fname"])) {
                    $key = key($_POST["fname"]);
                    
                    $app = $em->getRepository('AcmeCLSadminBundle:OrderPoliceClearanceApplicants')->findOneBy(array('order_no'=>$_POST['orderNo'], 'id'=>$key));  
                    $app->setFname($_POST['fname'][$key]);
                    $app->setMname($_POST['mname'][$key]);
                    $app->setLname($_POST['lname'][$key]);
                    $app->setEmail($_POST['email'][$key]);
                    $app->setPhone($_POST['phone'][$key]);
                    $app->setMobile($_POST['mobile'][$key]);
                    $app->setAddress($_POST['address'][$key]);
                    $app->setCity($_POST['city'][$key]);
                    $app->setPostcode($_POST['postcode'][$key]);
                    $app->setState($_POST['state'][$key]);
                    $app->setCountryId($_POST['country'][$key]);
                    $app->setPassportNo($_POST['passportNo'][$key]);
                    //$app->setDepartureDate($mod->changeFormatToOriginal($_POST['departureDate'][$key]));
                    $em->persist($app);
                    $em->flush();
                    next($_POST['fname']);
                }
                
                // order notes here
                
                if(trim($_POST['ticketComments']) != ''){
                    $comment = new OrderNotes();
                    $comment->setOrderNo($_POST['orderNo']);
                    $comment->setDateAdded($datetime->format('Y-m-d H:i:s'));
                    $comment->setNote($_POST['ticketComments']);
                    $comment->setNoteBy($session->get('admin_id'));
                    $comment->setNoteByName($session->get('admin_fname'));
                    $comment->setUserType('Admin');
                    $em->persist($comment);
                    $em->flush();
                    
                }
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Order has been updated.'
                );

                $em->getConnection()->commit(); 

                return $this->redirect($this->generateUrl('acme_cls_admin_view_order') . "?order_no=".$_POST['orderNo']);
            }
            
            
            
        }else if(isset($_POST['updateDocumentDeliveryOrder'])){
            
            $_POST['orderNo'] = filter_var($_POST['orderNo'], FILTER_SANITIZE_STRING);
            
            $data = $this->getOrderDetailsByOrderNo($_POST['orderNo']);
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST['orderNo']));
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            $old_stat = $order->getStatus();
            if($_POST['orderStatus'] == 10){
                $order->setStatus(10);
            }else if($_POST['orderStatus'] == 12){
                $order->setStatus(12);
            }
            
            $em->persist($order);
            $em->flush();
            
            if($old_stat != $order->getStatus()){
                if($_POST['orderStatus'] == 10){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['orderStatus'] == 11){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to paid by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['orderStatus'] == 12){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to completed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
            }
            

            $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST['orderNo']));
            $old_stat = $model->getSPaid();
            $model->setFname($_POST["fname"]);
            $model->setLname($_POST["lname"]);
            $model->setEmail($_POST["email"]);
            $model->setPhone($_POST["phone"]);
            $model->setMobile($_POST["mobile"]);
            $_POST['paymentStatus'] = intval($_POST['paymentStatus']);
            $model->setSPaid($_POST['paymentStatus']);
            $em->persist($model);
            $em->flush();
            
            
            if($old_stat != $_POST['paymentStatus']){
                if($_POST['paymentStatus'] == 0){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['paymentStatus'] == 1){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid online by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['paymentStatus'] == 2){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid by account by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
            }

            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNo"]));
            $order->setDocReceiverName($_POST["receiverName"]);
            $order->setDocPickupAddress($_POST["pickupAddress"]);
            $order->setDocPickupCity($_POST["pickupCity"]);
            $order->setDocPickupPostcode($_POST["pickupPostcode"]);
            $order->setDocPickupContactNo($_POST["pickupContactNo"]);
            $order->setDocPickupContactArea($_POST["pickupContactArea"]);
            $order->setDocPickupContactName($_POST["doc_pickup_contact_name"]);
            $order->setDocPickupEmail($_POST["doc_pickup_email"]);
//            $order->setDocDeliveryRecipientName($_POST["deliveryRecipientName"]);
            $order->setDocDeliveryCompany($_POST["deliveryCompany"]);
            $order->setDocDeliveryAddress($_POST["deliveryAddress"]);
            $order->setDocDeliveryCity($_POST["deliveryCity"]);
            $order->setDocDeliveryPostcode($_POST["deliveryPostcode"]);
            $order->setDocDeliveryContactNo($_POST["deliveryContactNo"]);
            $order->setDocDeliverySecurityNo($_POST["securityNumber"]);
            $order->setDocPackageTotalPieces($_POST["packageTotalPieces"]);
            $order->setDocPackagePickupDate($mod->changeFormatToOriginal($_POST["packagePickupDate"]));
            $order->setDocPackageReadyHr($_POST["packageReadyHr"]);
            $order->setDocPackageReadyMin($_POST["packageReadyMin"]);
            $order->setDocPackageReadyAmpm($_POST["packageReadyAmPm"]);
            $order->setDocPackageOfficeCloseHr($_POST["packageOfficeCloseHr"]);
            $order->setDocPackageOfficeCloseMin($_POST["packageOfficeCloseMin"]);
            $order->setDocPackageOfficeCloseAmpm($_POST["packageOfficeCloseAmPm"]);
            $sDeliveredToEmbassy = (isset($_POST['sDeliveredToEmbassy'])) ? 1 : 0;
            $order->setVisaIsDeliveredToEmbassy($sDeliveredToEmbassy);
            $order->setVisaIsDeliveredToEmbassyDate($mod->changeFormatToOriginal($_POST['embassy_delivered_date']));
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            
            $post = array(
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'receiverName'=>$_POST['receiverName'],
                'pickupAddress'=>$_POST['pickupAddress'],
                'pickupCity'=>$_POST['pickupCity'],
                'pickupPostcode'=>$_POST['pickupPostcode'],
                'pickupContactNo'=>$_POST['pickupContactNo'],
                'pickupContactArea'=>$_POST['pickupContactArea'],
                'doc_pickup_contact_name'=>$_POST['doc_pickup_contact_name'],
                'doc_pickup_email'=>$_POST['doc_pickup_email'],
                'deliveryCompany'=> $_POST['deliveryCompany'],
//                'deliveryRecipientName'=>$_POST['deliveryRecipientName'],
                'deliveryAddress'=>$_POST['deliveryAddress'],
                'deliveryCity'=>$_POST['deliveryCity'],
                'deliveryPostcode'=>$_POST['deliveryPostcode'],
                'deliveryContactNo'=>$_POST['deliveryContactNo'],
                'packageTotalPieces'=>$_POST['packageTotalPieces'],
                'packagePickupDate'=>$_POST['packagePickupDate'],
                'packageReadyHr'=>$_POST['packageReadyHr'],
                'packageReadyMin'=>$_POST['packageReadyMin'],
                'packageOfficeCloseHr'=>$_POST['packageOfficeCloseHr'],
                'packageOfficeCloseMin'=>$_POST['packageOfficeCloseMin'],
                'visa_is_delivered_to_embassy'=> $sDeliveredToEmbassy,
                'visa_is_delivered_to_embassy_date'=> $_POST['embassy_delivered_date']
            );
            
//            if($error_count == 0){
//                if($user->getCanChargeCostToAccount() != 1 || $user->getAccountNo() != $_POST["accountNumber"]){
//                    $errors = array();
//                    $special_error=1;
//                }
//                
//                if($user->getCanChargeCostToAccount() != 1){
//                    $errors[] = array('message'=>'You are not allowed to Pay on Account!');
//                    $error_count += 1;
//                }else if($user->getAccountNo() != $_POST["accountNumber"]){
//                    $errors[] = array('message'=>'Account number is not correct!');
//                    $error_count += 1;
//                }
//            }
            
            
            if($error_count > 0){

                // roll back changes
                $em->getConnection()->rollback();
                $em->close();


                $this->get('session')->getFlashBag()->add(
                            'error',
                            $errors
                        );

                return $this->render('AcmeCLSadminBundle:ViewOrder:docDelivery.html.twig',
                        array(
                            'post' => $post,

                        ));

            }else{
                
                $em->persist($model);
                $em->flush();
                

                // commit changes
                $em->getConnection()->commit(); 

                /* + create log */
                $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been processed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Order has been updated.'
                        );

                return $this->redirect($this->generateUrl('acme_cls_admin_view_order'). "?order_no=".$_POST['orderNo']);
            }
            
            
        }else if(isset($_POST['updateRussianVisaVoucherOrder'])){
            
            $_POST['orderNo'] = filter_var($_POST['orderNo'], FILTER_SANITIZE_STRING);
            
            $data = $this->getOrderDetailsByOrderNo($_POST['orderNo']);
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST['orderNo']));
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            $old_stat = $order->getStatus();
            if($_POST['orderStatus'] == 10){
                $order->setStatus(10);
            }else if($_POST['orderStatus'] == 12){
                $order->setStatus(12);
            }
            
            $em->persist($order);
            $em->flush();
            
            if($old_stat != $order->getStatus()){
                if($_POST['orderStatus'] == 10){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['orderStatus'] == 11){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to paid by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['orderStatus'] == 12){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to completed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
            }
            

            $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST['orderNo']));
            $_POST['paymentStatus'] = intval($_POST['paymentStatus']);
            $old_stat = $model->getSPaid();
            $model->setSPaid($_POST['paymentStatus']);
            $em->persist($model);
            $em->flush();
            
            if($old_stat != $_POST['paymentStatus']){
                if($_POST['paymentStatus'] == 0){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['paymentStatus'] == 1){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid online by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['paymentStatus'] == 2){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid by account by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
            }
            
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNo"]));
            $order->setPrimaryTravellerName($_POST['rvv_applicant_fname'] . " " . $_POST['rvv_applicant_lname']);
            $order->setRvvFirstEntryDate($mod->changeFormatToOriginal($_POST['rvv_first_entry_date']));
            $order->setRvvSecondEntryDate($mod->changeFormatToOriginal($_POST['rvv_second_entry_date']));
            $order->setRvvFirstDepartureDate($mod->changeFormatToOriginal($_POST['rvv_first_departure_date']));
            $order->setRvvSecondDepartureDate($mod->changeFormatToOriginal($_POST['rvv_second_departure_date']));
            $order->setRvvListOfCities($_POST['rvv_list_of_cities']);
            $order->setRvvListOfHotels($_POST['rvv_list_of_hotels']);
            $order->setRvvVisaAppliedAt($_POST['rvv_visa_applied_at']);
            
            
            $em->persist($order);
            $em->flush();

            $applicant = $em->getRepository('AcmeCLSclientGovBundle:OrderTravellers')->findOneBy(array('order_no'=>$_POST["orderNo"]));
            $applicant->setTitle($_POST['rvv_applicant_title']);
            $applicant->setOrderNo($order->getOrderNo());
            $applicant->setFname($_POST['rvv_applicant_fname']);
            $applicant->setMname($_POST['rvv_applicant_mname']);
            $applicant->setLname($_POST['rvv_applicant_lname']);
            $applicant->setEmail($_POST['rvv_applicant_email']);
            $applicant->setRvvCitizenship($_POST['rvv_applicant_citizenship']);
            $applicant->setRvvSex($_POST['rvv_applicant_sex']);
            $applicant->setPhone($_POST['rvv_applicant_phone']);
            $applicant->setRvvBirthPlace($_POST['rvv_applicant_birth_place']);
            $applicant->setBirthDate($mod->changeFormatToOriginal($_POST['rvv_applicant_birth_date']));
            $applicant->setPassportNumber($_POST['rvv_applicant_passport_no']);
            $applicant->setRvvPassportIssueDate($mod->changeFormatToOriginal($_POST['rvv_applicant_passport_issue_date']));
            $applicant->setRvvPassportExpDate($mod->changeFormatToOriginal($_POST['rvv_applicant_passport_exp_date']));
            $applicant->setRvvCompany($_POST['rvv_company']);
            $applicant->setRvvPosition($_POST['rvv_position']);
            $applicant->setRvvCity($_POST['rvv_city']);
            $applicant->setRvvState($_POST['rvv_state']);
            $applicant->setRvvPostcode($_POST['rvv_postcode']);
            $applicant->setRvvCountry($_POST['rvv_country']);
            $applicant->setRvvCompanyFax($_POST['rvv_company_fax']);
            $applicant->setRvvAddress($_POST['rvv_address']);
            
            $em->persist($applicant);
            $em->flush();

            // commit changes
            $em->getConnection()->commit(); 

            /* + create log */
            $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been processed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
            $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
            /* - create log */

            $this->get('session')->getFlashBag()->add(
                        'success',
                        'Order has been updated.'
                    );

            return $this->redirect($this->generateUrl('acme_cls_admin_view_order'). "?order_no=".$_POST['orderNo']);
            
            
        }elseif(isset($_POST['updateDocLegalisation'])){
            $_POST["dl_company"] = filter_var($_POST['dl_company'], FILTER_SANITIZE_STRING);
            $_POST["dl_address"] = filter_var($_POST['dl_address'], FILTER_SANITIZE_STRING);
            $_POST["dl_city"] = filter_var($_POST['dl_city'], FILTER_SANITIZE_STRING);
            $_POST["dl_state"] = filter_var($_POST['dl_state'], FILTER_SANITIZE_STRING);
            $_POST["dl_postcode"] = filter_var($_POST['dl_postcode'], FILTER_SANITIZE_STRING);
            $_POST["dl_contact_name"] = filter_var($_POST['dl_contact_name'], FILTER_SANITIZE_STRING);
            $_POST["dl_mobile"] = filter_var($_POST['dl_mobile'], FILTER_SANITIZE_STRING);
            $_POST["dl_email"] = filter_var($_POST['dl_email'], FILTER_SANITIZE_STRING);
            $_POST["dl_date_doc_returned"] = filter_var($_POST['dl_date_doc_returned'], FILTER_SANITIZE_STRING);
            $_POST["dl_embassy"] = filter_var($_POST['dl_embassy'], FILTER_SANITIZE_STRING);
            $_POST["dl_ref_no"] = filter_var($_POST['dl_ref_no'], FILTER_SANITIZE_STRING);
            $_POST["dl_com_invoice_no"] = filter_var($_POST['dl_com_invoice_no'], FILTER_SANITIZE_STRING);
            
            
            $_POST["docReturnCompany"] = filter_var($_POST['docReturnCompany'], FILTER_SANITIZE_STRING);
            $_POST["docReturnAddress"] = filter_var($_POST['docReturnAddress'], FILTER_SANITIZE_STRING);
            $_POST["docReturnCity"] = filter_var($_POST['docReturnCity'], FILTER_SANITIZE_STRING);
            $_POST["docReturnState"] = filter_var($_POST['docReturnState'], FILTER_SANITIZE_STRING);
            $_POST["docReturnPostCode"] = filter_var($_POST['docReturnPostCode'], FILTER_SANITIZE_STRING);
            $_POST["docReturnFname"] = filter_var($_POST['docReturnFname'], FILTER_SANITIZE_STRING);
            $_POST["docReturnLname"] = filter_var($_POST['docReturnLname'], FILTER_SANITIZE_STRING);
            $_POST["docReturnContactNo"] = filter_var($_POST['docReturnContactNo'], FILTER_SANITIZE_STRING);
            $_POST["additionalComments"] = filter_var($_POST['additionalComments'], FILTER_SANITIZE_STRING);
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            $_POST["orderNo"] = intval($_POST["orderNo"]);
            $data = $this->getOrderDetailsByOrderNo($_POST['orderNo']);
            
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNo"]));
            
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            
            $order->setDlCompany($_POST["dl_company"]);
            $order->setDlAddress($_POST["dl_address"]);
            $order->setDlCity($_POST["dl_city"]);
            $order->setDlState($_POST["dl_state"]);
            $order->setDlPostCode($_POST["dl_postcode"]);
            $order->setDlNationality($_POST["dl_nationality"]);
            $order->setDlContactName($_POST["dl_contact_name"]);
            $order->setDlMobile($_POST["dl_mobile"]);
            $order->setDlEmail($_POST["dl_email"]);
            $order->setDlDateDocReturned($mod->changeFormatToOriginal($_POST["dl_date_doc_returned"]));
            $order->setDlEmbassy($_POST["dl_embassy"]);
            $order->setDlRefNo($_POST["dl_ref_no"]);
            $order->setDlComInvoiceNo($_POST["dl_com_invoice_no"]);
            
            
            $order->setVisaMddCompany($_POST["docReturnCompany"]);
            $order->setVisaMddAddress($_POST["docReturnAddress"]);
            $order->setVisaMddCity($_POST["docReturnCity"]);
            $order->setVisaMddState($_POST["docReturnState"]);
            $order->setVisaMddPostcode($_POST["docReturnPostCode"]);
            $order->setVisaMddFname($_POST["docReturnFname"]);
            $order->setVisaMddLname($_POST["docReturnLname"]);
            $order->setVisaMddContact($_POST["docReturnContactNo"]);
            $order->setVisaAdditionalComment($_POST["additionalComments"]);
            //$order->setVisaClsTeamMember($_POST['clsTeamMember']);
            $old_stat = $order->getStatus();
            $order->setStatus($_POST['orderStatus']);
            
            
            $em->persist($order);
            $em->flush();
            
            //if($old_stat != $model->getStatus()){
            if($old_stat != $_POST['orderStatus']){
                if($_POST['orderStatus'] == 10){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['orderStatus'] == 11){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to paid by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['orderStatus'] == 12){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) order status has been updated to completed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
            }
            

            $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$order->getOrderNo()));  
            $_POST['paymentStatus'] = intval($_POST['paymentStatus']);
            $old_stat = $model->getSPaid();
            $model->setSPaid($_POST['paymentStatus']);    
            $em->persist($model);
            $em->flush();

            if($old_stat != $_POST['paymentStatus']){
                if($_POST['paymentStatus'] == 0){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to pending by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['paymentStatus'] == 1){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid online by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($_POST['paymentStatus'] == 2){
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) payment status has been updated to paid by account by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
            }
            
            $query = $em->createQuery('DELETE AcmeCLSclientGovBundle:OrderDlChecklist c WHERE c.order_no = '.$order->getOrderNo());
            $query->execute(); 
                
            for($i=0; $i<count($_POST['dl_type']); $i++){
                $_POST["dl_type"][$i] = filter_var($_POST["dl_type"][$i], FILTER_SANITIZE_STRING);
                $_POST["dl_number"][$i] = filter_var($_POST["dl_number"][$i], FILTER_SANITIZE_STRING);
                $_POST["dl_note"][$i] = filter_var($_POST["dl_note"][$i], FILTER_SANITIZE_STRING);
                
                $checklist = new OrderDlChecklist();
                $checklist->setOrderNo($order->getOrderNo());
                $checklist->setType($_POST["dl_type"][$i]);
                $checklist->setNumber($_POST["dl_number"][$i]);
                $checklist->setNote($_POST["dl_note"][$i]);
                $em->persist($checklist);
                $em->flush();
                
            }
            
            // commit changes
            $em->getConnection()->commit(); 



            $this->get('session')->getFlashBag()->add(
                        'success',
                        'Order has been updated.'
                    );

            return $this->redirect($this->generateUrl('acme_cls_admin_view_order'). "?order_no=".$_POST['orderNo']);
            
        }elseif(isset($_POST['updateDocLegalisationOrderProcess'])){
            
            $_POST['orderNo'] = filter_var($_POST['orderNo'], FILTER_SANITIZE_STRING);
            $data = $this->getOrderDetailsByOrderNo($_POST['orderNo']);
            
            if(count($data) > 0){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                
                
                
                $model = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST['orderNo']));  
                
                
                
                $notice_message = '';
                if($model->getPoliceClearanceDateClsReceivedAllItems() != $mod->changeFormatToOriginal($_POST['allItemsReceivedAtCLS'])){
                    $notice_message = 'This is a courtesy email to inform you that your package has been received by CLS Capital Link Services, Canberra.';
                    
                    /* + create log */
                    $logDetails =$data['order_type_name'] . " application  (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been received by CLS Capital Link Services, Canberra - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($model->getPoliceClearanceDateSubmittedForProcessing() != $mod->changeFormatToOriginal($_POST['submittedForProcessing'])){
                    $notice_message = 'This is a courtesy email to inform you that your application has been submitted for visa processing';
                    
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been submitted for visa processing - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($model->getPoliceClearanceDateCompletedAndReceivedAtCls() != $mod->changeFormatToOriginal($_POST['completedReceivedAtCLS'])){
                    $notice_message = 'This is a courtesy email to inform you that your visa has been issued from the EMBASSY OF '. strtoupper($_POST['nextEmbassy']) .', and that it will be sent to you today. Thank you for using our services, we look forward to assisting you in the future.';
                    
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been issued from the EMBASSY OF ". strtoupper($_POST['nextEmbassy']) ." - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }elseif($model->getPoliceClearanceDateOrderOnRouteAndClosed() != $mod->changeFormatToOriginal($_POST['orderOnRouteAndClosed'])){
                    
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) on route and closed - updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }else{
                    $notice_message = 'Your current Order has been processed by our staff recently';
                    
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) has been processed by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
                
                if(trim($_POST['allItemsReceivedAtCLS']) != ''){
                    $model->setPoliceClearanceDateClsReceivedAllItems($mod->changeFormatToOriginal($_POST['allItemsReceivedAtCLS']));
                }
                if(trim($_POST['submittedForProcessing']) != ''){
                    $model->setPoliceClearanceDateSubmittedForProcessing($mod->changeFormatToOriginal($_POST['submittedForProcessing']));
                }
                if(trim($_POST['completedReceivedAtCLS']) != ''){
                    $model->setPoliceClearanceDateCompletedAndReceivedAtCls($mod->changeFormatToOriginal($_POST['completedReceivedAtCLS']));
                }
                if(trim($_POST['orderOnRouteAndClosed']) != ''){
                    $model->setPoliceClearanceDateOrderOnRouteAndClosed($mod->changeFormatToOriginal($_POST['orderOnRouteAndClosed']));
                }
                
                $model->setVisaClsTeamMember($_POST['clsTeamMember']);
                
                $sDeliveredToEmbassy = (isset($_POST['sDeliveredToEmbassy'])) ? 1 : 0;
                $model->setVisaIsDeliveredToEmbassy($sDeliveredToEmbassy);
                $model->setVisaIsDeliveredToEmbassyDate($mod->changeFormatToOriginal($_POST['embassy_delivered_date']));
                $model->setVisaNextEmbassy($_POST['nextEmbassy']);
                $model->setDlVisaShippedBy($_POST["dl_visa_shipped_by"]);
                $model->setDlVisaInvoiceNo($_POST["dl_visa_invoice_no"]);
                $model->setDlVisaComNoteNo($_POST["dl_visa_com_note_no"]);
                $model->setDlVisaComNoteIn($_POST["dl_visa_com_note_in"]);
                
                if(trim($_POST['orderOnRouteAndClosed']) != ''){
                    $model->setStatus(12);
                }
                $em->persist($model);
                $em->flush();
                
                // follow up date
            if(trim($_POST['follow_up_date']) != ''){
                $_POST['orderNo'] = intval($_POST['orderNo']);
                $query = $em->createQuery('DELETE AcmeCLSadminBundle:OrderFollowUpDate d WHERE d.order_id = '.$_POST['orderNo'].' AND d.admin_id='.$session->get("admin_id"));
                $query->execute(); 
                
                $fuDate = new OrderFollowUpDate();
                $fuDate->setAdminId($session->get("admin_id"));
                $fuDate->setOrderId($_POST['orderNo']);
                $fuDate->setFollowUpDate($mod->changeFormatToOriginal($_POST['follow_up_date']));
                $em->persist($fuDate);
                $em->flush();
            }
                
                // order notes here
                
                if(trim($_POST['ticketComments']) != ''){
                    $comment = new OrderNotes();
                    $comment->setOrderNo($_POST['orderNo']);
                    $comment->setDateAdded($datetime->format('Y-m-d H:i:s'));
                    $comment->setNote($_POST['ticketComments']);
                    $comment->setNoteBy($session->get('admin_id'));
                    $comment->setNoteByName($session->get('admin_fname'));
                    $comment->setUserType('Admin');
                    $comment->setIsAdmin(0);
                    $em->persist($comment);
                    $em->flush();
                    
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) - comment was added by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
                if(trim($_POST['ticketAdminComments']) != ''){
                    $comment = new OrderNotes();
                    $comment->setOrderNo($_POST['orderNo']);
                    $comment->setDateAdded($datetime->format('Y-m-d H:i:s'));
                    $comment->setNote($_POST['ticketAdminComments']);
                    $comment->setNoteBy($session->get('admin_id'));
                    $comment->setNoteByName($session->get('admin_fname'));
                    $comment->setUserType('Admin');
                    $comment->setIsAdmin(1);
                    $em->persist($comment);
                    $em->flush();
                    
                    /* + create log */
                    $logDetails = $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$_POST['orderNo']."' target='_blank'>". $_POST['orderNo'] ."</a>) - admin comment was added by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */
                }
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Order has been updated.'
                );

                $em->getConnection()->commit(); 

                if(!isset($_POST['chkSendUpdate'])){
                    // email invoice
                    $this->sendEmail($data['user_email'], "help@capitallinkservices.com.au", "Your current Visa Order has been processed", 
                            $this->renderView('AcmeCLSadminBundle:ViewOrder:email_user.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNo"],
                                        'data'=>$data,
                                        'message'=> $notice_message
                                        )
                                    )
                            );
                }
                return $this->redirect($this->generateUrl('acme_cls_admin_view_order') . "?order_no=".$_POST['orderNo']);
            }
        }elseif(isset($_POST['reprint'])){    
            $this->reprint();
        }else{
        
            $_GET["order_no"] = filter_var($_GET["order_no"], FILTER_SANITIZE_STRING);
            $_GET["order_no"] = intval($_GET["order_no"]);
           
            $post = $this->getOrderDetailsByOrderNo($_GET["order_no"]);
            
            if(count($post) > 0){
                
                if($post['order_type'] == 4){
                    return $this->render('AcmeCLSadminBundle:ViewOrder:passport.html.twig',
                        array('post'=> $this->getOrderDetailsByOrderNo($_GET["order_no"]),
                                'site_url' => $mod->siteURL(),
                                'attachment_url'=>$attachment_url,
                            )
                        );
                }else if($post['order_type'] == 5){
                    return $this->render('AcmeCLSadminBundle:ViewOrder:policeClearance.html.twig',
                        array('post'=> $this->getOrderDetailsByOrderNo($_GET["order_no"]),
                                'site_url' => $mod->siteURL(),
                                'countries' => $this->getCountries(),
                                'cls_staff' => $this->getAdminUsers(),
                                'attachment_url'=>$attachment_url,
                            )
                        );
                    
                }else if($post['order_type'] == 7){
                    return $this->render('AcmeCLSadminBundle:ViewOrder:docDelivery.html.twig',
                        array(
                            'post'=> $this->getOrderDetailsByOrderNo($_GET["order_no"]),
                            'site_url' => $mod->siteURL(),
                            'attachment_url'=>$attachment_url,
                            )
                        );
                    
                    
                }else if($post['order_type'] == 8){
                    return $this->render('AcmeCLSadminBundle:ViewOrder:russianVisaVoucher.html.twig',
                        array('post'=> $this->getOrderDetailsByOrderNo($_GET["order_no"]),
                            'countries' => $this->getCountries(),
                            'name_titles' => $this->getNameTitles(),
                            'attachment_url'=>$attachment_url,
                            )
                        );
                    
                }else if($post['order_type'] == 9){
                    return $this->render('AcmeCLSadminBundle:ViewOrder:docLegalisation.html.twig',
                        array('post'=> $this->getOrderDetailsByOrderNo($_GET["order_no"]),
                            'countries' => $this->getCountries(),
                            'name_titles' => $this->getNameTitles(),
                            'visa_courier_options' => $this->getCourierOptions(),
                            'cls_staff' => $this->getAdminUsers(),
                            'site_url' => $mod->siteURL(),
                            'attachment_url'=>$attachment_url,
                            )
                        );
                    
                }else{
                    return $this->render('AcmeCLSadminBundle:ViewOrder:index.html.twig',
                        array('post'=> $this->getOrderDetailsByOrderNo($_GET["order_no"]),
                                'countries' => $this->getCountries(),
                                'name_titles' => $this->getNameTitles(),
                                'passport_types' => $this->getPassportTypes(),
                                'departments' => $this->getDepartments(),
                                'site_url' => $mod->siteURL(),
                                'cls_staff' => $this->getAdminUsers(),
                                'attachment_url'=>$attachment_url,
                            )
                        );
                }
            }else{
                return $this->render('AcmeCLSadminBundle:ViewOrder:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order_no"].' not found!'
                    ));
            }
            
        }
    }
    
    
    public function updateDestCommentAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            exit("session_expired");
        }
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        $_POST['id'] = intval($_POST['id']);
        $_POST['comment'] = filter_var($_POST['comment'], FILTER_SANITIZE_STRING);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        $model = $em->getRepository('AcmeCLSadminBundle:OrderDestinationNotes')->findOneBy(array('id'=>$_POST['id'])); 
        $old_note = $model->getNote();
        $model->setNote($_POST['comment']);
        $em->persist($model);
        $em->flush();
        
        $od = $em->getRepository('AcmeCLSclientGovBundle:OrderDestinations')->findOneBy(array('id'=> $model->getDestinationId() )); 
        
        /* + create log */
        $logDetails = "A comment for order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=". $od->getOrderNo() ."' target='_blank'>". $od->getOrderNo() ."</a> has been edited by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */
        
        $em->getConnection()->commit(); 
        return new Response("success");
    }

    
    public function deleteDestCommentAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            exit("session_expired");
        }
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        $_POST['id'] = intval($_POST['id']);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        
        $odn = $em->getRepository('AcmeCLSadminBundle:OrderDestinationNotes')->findOneBy(array('id'=>$_POST['id'])); 
        $od = $em->getRepository('AcmeCLSclientGovBundle:OrderDestinations')->findOneBy(array('id'=> $odn->getDestinationId() )); 
        
        $query = $em->createQuery('DELETE AcmeCLSadminBundle:OrderDestinationNotes d WHERE d.id = '.$_POST['id']);
        $query->execute(); 
        
        /* + create log */
        $logDetails = "A comment for order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=".$od->getOrderNo() ."' target='_blank'>". $od->getOrderNo() ."</a> has been deleted by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */

        $em->getConnection()->commit(); 
        return new Response("success");
    }
    
    public function updateDlCommentAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('admin_email') == ''){
            exit("session_expired");
        }
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        $_POST['id'] = intval($_POST['id']);
        $_POST['comment'] = filter_var($_POST['comment'], FILTER_SANITIZE_STRING);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        $model = $em->getRepository('AcmeCLSclientGovBundle:OrderNotes')->findOneBy(array('id'=>$_POST['id'])); 
        $old_note = $model->getNote();
        $model->setNote($_POST['comment']);
        $em->persist($model);
        $em->flush();
        
        /* + create log */
        $logDetails = "A comment for order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=". $model->getOrderNo() ."' target='_blank'>". $model->getOrderNo() ."</a> has been edited by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */
        
        $em->getConnection()->commit(); 
        return new Response("success");
    }
    
    public function sendInvoiceAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
        $client_id = intval($client_id);
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
        $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
        
        $client_email = ($session->get('user_type') == 'admin-user') ? $user->getEmail() : $session->get("email");
        
        $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNumber"], $client_id);
        
        
        # Document Delivery
        if($_POST['type']== 'doc-delivery'){
            
            $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Document Delivery Order Receipt", 
                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$order->getOrderNo(),
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data
                            )
                        )
                );


            $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Courier Booking Submission",
                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:booking_submission.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$order->getOrderNo(),
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data
                            )
                        )
                );
            
            
            $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "Document Delivery Manual Order Receipt", 
                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$order->getOrderNo(),
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'manual_order'=>1
                            )
                        )
                );


            $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "Courier Booking Submission - Manual Order",
                $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentDelivery:booking_submission.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$order->getOrderNo(),
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'manual_order'=>1
                            )
                        )
                );
            
            
        # Doc Legalisation
        }elseif($_POST['type']== 'doc-legalisation'){
            
            $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Document Legalisation", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:email.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$order->getOrderNo(),
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'post'=>$data,
                            'user'=>$user
                            )
                        ),
                        //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                        array(
                            '0'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Document-Legalisation.PDF')
                            //'1'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Document_Legalisation_order_form_.pdf')
                        )
                );
            
            $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Document Legalisation - Manual order", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationDocumentLegalisation:email.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$order->getOrderNo(),
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'post'=>$data,
                            'user'=>$user,
                            'manual_order'=>1
                            )
                        ),
                        //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                        array(
                        '0'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Document-Legalisation.PDF')
                        //'1'=>array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/documentlegalisationfiles/CLS_Document_Legalisation_order_form_.pdf')
                    )
                );
        
            
                
        # Passport
        }elseif($_POST['type']== 'passport'){
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsPassport');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = 1')
                ->getQuery();
            $settings = $query->getArrayResult();

            $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Passport Office Pickup or Delivery Order Reciept", 
                $this->renderView('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$order->getOrderNo(),
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'settings_passport'=>$settings[0],
                            'data'=>$data
                            )
                        )
                );
            
            $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "Passport Office Pickup or Delivery Manual Order Reciept", 
                $this->renderView('AcmeCLSclientGovBundle:ApplicationPassportOfficePickupOrDelivery:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$order->getOrderNo(),
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'settings_passport'=>$settings[0],
                            'data'=>$data,
                            'manual_order'=>1
                            )
                        )
                );
        
        # Police Clearance
        }elseif($_POST['type']== 'police-clearance'){
            $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Police Clearance Application Reciept", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNumber"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data
                            )
                        ),
                        //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                        array('0'=>
                            array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/pclearancefiles/' . $data['police_clearance_id'] . '/'. $data['police_clearance_file'])
                        )
                );
            
            $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Police Clearance Application Manual Order Reciept", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationPoliceClearance:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNumber"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'manual_order'=>1
                            )
                        ),
                        //$this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                        array('0'=>
                            array('file_attachment'=> dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/pclearancefiles/' . $data['police_clearance_id'] . '/'. $data['police_clearance_file'])
                        )
                );
        
        # Russian Visa Voucher
        }elseif($_POST['type']== 'rvv'){
            
            if(trim($data['rvv_file']) != ''){
                    $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data
                                    )
                                ),
                        array('0'=>
                            array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                            )
                        );
                    
                    // manual order email to admin
                    if($session->get('user_type') == 'admin-user'){ 
                            $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Russian Visa Voucher Manual Order Reciept", 
                                $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                        array('domain'=>$mod->siteURL(),
                                            'order_no'=>$_POST["orderNumber"],
                                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                            'data'=>$data,
                                            'manual_order'=>1
                                            )
                                        ),
                                array('0'=>
                                    array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                    )
                                );
                        
                    }
                    if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                        // email invoice (copy to admins)
                        $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                        $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    ),
                            array('0'=>
                                array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                )
                            );
                    }
                    
                    $admins = $this->getAdminUsers();
                    for($i=0; $i<count($admins); $i++){
                        $this->sendEmailWithAttachment($admins[$i]['email'], "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    ),
                            array('0'=>
                                array('file_attachment'=>  dirname($this->container->get('kernel')->getRootdir()) . '/web/dev/rvv/'.$client_id.'/'.$_POST["orderNumber"].'/'.$data['rvv_file'])
                                )
                            );
                        
                    }
                    
                }else{
                    $this->sendEmail($client_email, "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data
                                    )
                                )
                        );
                    
                    $admins = $this->getAdminUsers();
                    for($i=0; $i<count($admins); $i++){
                        $this->sendEmail($admins[$i]['email'], "help@capitallinkservices.com.au", "Russian Visa Voucher Order Reciept (Copy)", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationRussianVisaVoucher:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data
                                        )
                                    )
                            );

                    }
                }
                
        # TPN
        }elseif($_POST['type']== 'tpn'){
            
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            
             $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Order Reciept", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'order_no'=>$_POST["orderNumber"],
                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                'data'=>$data,
                                'tpn_settings'=>$tpn_settings
                                )
                            )
                    );
             
             $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Submission", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:order_details_email_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'data'=> $this->getOrderDetailsByOrderNo($_POST["orderNumber"])
                                )
                            )
                    );
             // manual order email to admin
            if($session->get('user_type') == 'admin-user'){
                // email invoice
                $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Manual Order Reciept", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'tpn_settings'=>$tpn_settings,
                                    'manual_order'=>1
                                    )
                                )
                        );
                

                // email submission summary
                $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Submission", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:order_details_email_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'data'=> $this->getOrderDetailsByOrderNo($_POST["orderNumber"]),
                                    'manual_order'=>1
                                    )
                                )
                        );
                
            }
            
            if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                // email invoice (copy to admins)
                if(!isset($_POST['dont_send_invoice'])){
                    $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                    $this->sendEmail($email_copies, "help@capitallinkservices.com.au", "TPN Order Reciept (Copy)", 
                            $this->renderView('AcmeCLSclientGovBundle:ApplicationTPN:invoice_template.html.twig',
                                    array('domain'=>$mod->siteURL(),
                                        'order_no'=>$_POST["orderNumber"],
                                        'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                        'data'=>$data,
                                        'tpn_settings'=>$tpn_settings
                                        )
                                    )
                            );
                }
            }
            
            
        # TPN+VISA
        }elseif($_POST['type']== 'tpn_visa'){
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "TPN+Visa Order Reciept", 
                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNumber"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings
                            )
                        ),
                $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                );
            
             $this->sendEmail($client_email, "help@capitallinkservices.com.au", "TPN Submission", 
                $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:order_details_email_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'data'=> $data
                            )
                        )
                );
             
             // manual order email to admin
            if($session->get('user_type') == 'admin-user'){
                // email invoice
                $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN+Visa Manual Order Reciept", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'tpn_settings'=>$tpn_settings,
                                    'manual_order'=>1
                                    )
                                ),
                        $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                        );
                

                // email submission summary
                $this->sendEmail($session->get("admin_email"), "help@capitallinkservices.com.au", "TPN Submission", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:order_details_email_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'data'=> $data,
                                    'manual_order'=>1
                                    )
                                )
                        );
                
            }

            if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                // email invoice (copy to admins)
                $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "TPN+Visa Order Reciept (Copy)", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationTPNplusVisa:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'tpn_settings'=>$tpn_settings
                                    )
                                ),
                        $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                        );
                
            }
            
            
        # VISA
        }elseif($_POST['type']== 'visa'){
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Visa Order Reciept", 
                $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNumber"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings
                            )
                        ),
                $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                );
            
            // manual order email to admin
            if($session->get('user_type') == 'admin-user'){
                // email invoice
                $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Visa Manual Order Reciept", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'tpn_settings'=>$tpn_settings,
                                    'manual_order'=>1
                                    )
                                ),
                        $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                        );
                
            }
            
            if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                // email invoice (copy to admins)
                $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Visa Order Reciept (Copy)", 
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNumber"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'tpn_settings'=>$tpn_settings
                                    )
                                ),
                        $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'government')
                        );
                
            }
            
        # VISA
        }elseif($_POST['type']== 'public_visa'){
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            $this->sendEmailWithAttachment($client_email, "help@capitallinkservices.com.au", "Visa Order Reciept", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationPublicVisa:invoice_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'order_no'=>$_POST["orderNumber"],
                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                'data'=>$data,
                                'tpn_settings'=>$tpn_settings
                                )
                            ),
                    $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                    );
            
            // manual order email to admin
            if($session->get('user_type') == 'admin-user'){ 
                // email invoice
                $this->sendEmailWithAttachment($session->get("admin_email"), "help@capitallinkservices.com.au", "Visa Manual Order Reciept", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationPublicVisa:invoice_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'order_no'=>$_POST["orderNumber"],
                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                'data'=>$data,
                                'tpn_settings'=>$tpn_settings,
                                'manual_order'=>1
                                )
                            ),
                    $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                    );
                
            }
            if(trim($_SERVER['HTTP_HOST']) != 'localhost'){
                // email invoice (copy to admins)
                $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                $this->sendEmailWithAttachment($email_copies, "help@capitallinkservices.com.au", "Visa Order Reciept", 
                    $this->renderView('AcmeCLSclientGovBundle:ApplicationPublicVisa:invoice_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'order_no'=>$_POST["orderNumber"],
                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                'data'=>$data,
                                'tpn_settings'=>$tpn_settings
                                )
                            ),
                    $this->getOrderDestinationsByOrderNo($_POST["orderNumber"], 'public')
                );
                
            }
        }
        
        /* + create log */
        $logDetails = "Invoice for ". $data['order_type_name'] . " application (order no. <a href='". $this->generateUrl('acme_cls_admin_view_order') ."?order_no=". $order->getOrderNo() ."' target='_blank'>". $order->getOrderNo() ."</a>) has been re-sent to " . $user->getFname() . " " . $user->getLname() . " by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */
        
        $this->get('session')->getFlashBag()->add(
                'success',
                'Invoice Sent!'
            );
        
        return new Response("success");
    }
    
    
    public function reprint(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'application-tpn-place-order');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        require_once($root_dir.'/plugins/dompdf/dompdf_config.inc.php');
        
        $app = '';
        
        switch ($_POST['appType']){
            case 'public_visa':
                $app = 'ApplicationPublicVisa';
                break;
            case 'visa':
                $app = 'ApplicationVisa';
                break;
            case 'police':
                $app = 'ApplicationPoliceClearance';
                break;
            case 'passport':
                $app = 'ApplicationPassportOfficePickupOrDelivery';
                break;
            case 'tpn':
                $app = 'ApplicationTPN';
                break;
            case 'tpn_visa':
                $app = 'ApplicationTPNplusVisa';
                break;
            case 'rvv':
                $app = 'ApplicationRussianVisaVoucher';
                break;
            case 'doc-delivery':
                $app = 'ApplicationDocumentDelivery';
                break;
        }
        
        $client_id = ($session->get('user_type') == 'admin-user') ? $_POST['client_id'] : $session->get('client_id');
        $client_id = intval($client_id);

        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNo"]));
        
        $items = $this->getManualPayment($_POST["orderNo"]);
        if(count($items)>0){
            $post = array('order_no'=>$items[0]['order_no'],'items'=>json_decode($items[0]['items'],true));
        }
        
        $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["orderNo"], $client_id);

        $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));

        $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
        
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsPassport');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = 1')
                ->getQuery();
            $settings = $query->getArrayResult();
        if(count($items)>0){  
            $content = $this->renderView('AcmeCLSclientGovBundle:'.$app.':invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNo"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings,
                            'settings_passport'=>$settings[0],
                            'post'=>$post
                            )
                        );
        }else{
            $content = $this->renderView('AcmeCLSclientGovBundle:'.$app.':invoice_template.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNo"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings,
                            'settings_passport'=>$settings[0]
                            )
                        );
        }
        $stylesheet = file_get_contents($root_dir.'/css/reprint-invoice-pdf.css'); /// here call you external css file 
        $stylesheet = '<style>'.$stylesheet.'</style>';
        
        $dompdf = new \DOMPDF();
        $dompdf->set_paper( 'A4' );
        $dompdf->load_html( $stylesheet . $content );
        $dompdf->render();
        $dompdf->stream('invoice.pdf',array('Attachment'=>0));
        
        $response = new Response("success");
        $response->setContent('success');
        $response->headers->set('Content-Type', 'application/pdf');
        return $response;
        
//        $this->printPDF($content);
        
        // manual order email to admin
        if($session->get('user_type') == 'admin-user'){
                $this->printPDF(
                        $this->renderView('AcmeCLSclientGovBundle:ApplicationVisa:invoice_template_pdf.html.twig',
                                array('domain'=>$mod->siteURL(),
                                    'order_no'=>$_POST["orderNo"],
                                    'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                    'data'=>$data,
                                    'tpn_settings'=>$tpn_settings,
                                    'manual_order'=>1
                                    )
                                )
                        );
        }

            if(trim($_SERVER['HTTP_HOST']) != 'localhost'){

                     $this->printPDF(
                    $this->renderView('AcmeCLSclientGovBundle:'.$app.':invoice_template_pdf.html.twig',
                        array('domain'=>$mod->siteURL(),
                            'order_no'=>$_POST["orderNo"],
                            'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                            'data'=>$data,
                            'tpn_settings'=>$tpn_settings,
                            'settings_passport'=>$settings[0]
                            )
                        )
                );
            }


            return new Response("success");
    }
    
    public function getManualPayment($orderNo){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:ManualPayment');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id,p.order_no,p.cust_name,p.cust_email,p.items,p.payment_details,p.billing_details,p.grand_total")
            ->where('p.order_no = :order_no ')
            ->setParameter('order_no', $orderNo)
            ->orderBy("p.id","ASC")
            ->setMaxResults(1)
            ->getQuery();
        $data = $query->getArrayResult();
       
        return $data;
    }
    
}
