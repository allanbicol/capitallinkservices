<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Countries;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;


class ManageCountriesController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-countries');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(isset($_POST['hidSubmit'])){
            $_POST['id'] = intval($_POST['id']);
            $_POST['countryCode'] = filter_var($_POST['countryCode'], FILTER_SANITIZE_STRING);
            $_POST['countryName'] = filter_var($_POST['countryName'], FILTER_SANITIZE_STRING);
            $_POST['embassy'] = filter_var($_POST['embassy'], FILTER_SANITIZE_STRING);

            if($_POST["countrySave"] == 1){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction(); 
                

                if($_POST['hidSubmit'] == 1){
                    $model = new Countries();
                }else{
                    $model = $em->getRepository('AcmeCLSclientGovBundle:Countries')->findOneBy(array('id'=>$_POST['id']));
                }
                $model->setCountryName($_POST['countryName']);
                $model->setCountryCode($_POST['countryCode']);
                $model->setRepName($_POST['embassy']);
                $model->setCountryNameDisplay($_POST['countryNameDisplay']);
                $model->setPriority(999);
                $em->persist($model);
                $em->flush();

                $validator = $this->get('validator');
                $errors = $validator->validate($model);
                $error_count = count($errors);


                $post = array(
                    'countryCode'=>$_POST['countryCode'],
                    'countryName'=>$_POST['countryName'],
                    'embassy'=>$_POST['embassy']
                );

                if($error_count == 0){

                    $em->getConnection()->commit(); 

                    /* + create log */
                    if($_POST['hidSubmit'] == 1){
                        $logDetails = $_POST['countryName'] . " has been added to the <a href='". $this->generateUrl('acme_cls_admin_manage_countries') ."' target='_blank'>list of countries</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    }else{
                        $logDetails = $_POST['countryName'] . ", a country, its details have been updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                    }
                    $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                    /* - create log */

                    if($_POST['hidSubmit'] == 1){
                        $this->get('session')->getFlashBag()->add(
                            'success',
                            $_POST['countryName'] . ' has been added'
                        );
                    }else{
                        $this->get('session')->getFlashBag()->add(
                            'success',
                            $_POST['countryName'] . ' has been updated'
                        );
                    }

                    return $this->redirect($this->generateUrl('acme_cls_admin_manage_countries')); 

                }else{

                    $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors);

                    $em->getConnection()->rollback();
                    $em->close();

                    return $this->render('AcmeCLSadminBundle:ManageCountries:index.html.twig',
                        array('post'=>$post,
                            'countries'=>$this->getCountries()
                        )
                    );
                }
            }else{
                

                $em = $this->getDoctrine()->getManager();
                $query = $em->createQuery('DELETE AcmeCLSclientGovBundle:Countries t WHERE t.id = '.$_POST['id']);
                $query->execute(); 
                
                $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['countryName'] . ' has been deleted'
                    );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_countries')); 
            }
        }
        
        
        return $this->render('AcmeCLSadminBundle:ManageCountries:index.html.twig',
                array('countries'=>$this->getCountries()));
    }

}
