<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\SettingsDiscount;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageDiscountController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-discount');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:ManageDiscount:index.html.twig');
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:SettingsDiscount');
            $query = $cust->createQueryBuilder('d')
                ->select("d.name, d.code, d.rate, d.id")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("name","code","rate","id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    /**
     * new discount
     * @return type
     */
    public function newAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-discount');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['code'] = filter_var($_POST['code'], FILTER_SANITIZE_STRING);
            $_POST['name'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
            $_POST['rate'] = filter_var($_POST['rate'], FILTER_SANITIZE_STRING);
            
            $_POST['rate'] = floatval($_POST['rate']);
            
            $model = new SettingsDiscount();
            $model->setCode($_POST['code']);
            $model->setName($_POST['name']);
            $model->setRate($_POST['rate']);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'code'=>$_POST['code'],
                'name'=>$_POST['name'],
                'rate'=>$_POST['rate']
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_discount_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['name']. "</a> has been added to the <a href='".$this->generateUrl('acme_cls_admin_manage_discount')."' target='_blank'>list of discount codes</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['name'] . ' has been added.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_discount'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageDiscount:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSadminBundle:ManageDiscount:new.html.twig',
                array(
                    )
                );
        }
    }
    
    /**
     * edit discount
     * @return type
     */
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-discount');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['code'] = filter_var($_POST['code'], FILTER_SANITIZE_STRING);
            $_POST['name'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
            $_POST['rate'] = filter_var($_POST['rate'], FILTER_SANITIZE_STRING);
            $_POST['rate'] = floatval($_POST['rate']);
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:SettingsDiscount')->findOneBy(array('id'=>$_POST["id"]));
            $model->setCode($_POST['code']);
            $model->setName($_POST['name']);
            $model->setRate($_POST['rate']);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'code'=>$_POST['code'],
                'name'=>$_POST['name'],
                'rate'=>$_POST['rate']
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_discount_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['name']. "</a>, a discount code, its details have been updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['name'] . ' has been updated.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_discount'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageDiscount:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            $em = $this->getDoctrine()->getManager(); 
            
            $_GET['id'] = filter_var($_GET['id'], FILTER_SANITIZE_STRING);

            $model = $em->getRepository('AcmeCLSclientGovBundle:SettingsDiscount')->findOneBy(array('id'=>$_GET["id"]));
            
            return $this->render('AcmeCLSadminBundle:ManageDiscount:edit.html.twig',
                array(
                    'post'=>$model
                    )
                );
        }
    }
    
    
    public function deleteAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $model = $em->getRepository('AcmeCLSclientGovBundle:SettingsDiscount')->findOneBy(array('id'=>$_POST['id']));
        
        $statement = $connection->prepare("DELETE FROM tbl_settings_discount WHERE id=".$_POST['id']);
        $statement->execute();
        
         /* + create log */
        $logDetails = $model->getName() . ", a discount code, has been been deleted and removed from the <a href='".$this->generateUrl('acme_cls_admin_manage_discount')."' target='_blank'>list of discount codes</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */
        
        return new Response("success");
        
    }
}
