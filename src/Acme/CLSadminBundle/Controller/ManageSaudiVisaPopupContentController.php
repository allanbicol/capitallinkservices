<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Countries;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;


class ManageSaudiVisaPopupContentController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-saudi-visa-popup-content');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(isset($_POST['content'])){
            //$_POST['content'] = filter_var($_POST['content'], FILTER_SANITIZE_STRING);

            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 


            $model = $em->getRepository('AcmeCLSclientGovBundle:VisaPopupContent')->findOneBy(array('id'=>1));
            $model->setContent($_POST['content']);
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);


            $post = $_POST;

            if($error_count == 0){

                $em->getConnection()->commit(); 


                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Saudi Visa popup content has been updated'
                );

                return $this->redirect($this->generateUrl('acme_cls_admin_saudi_visa_popup_content')); 

            }else{

                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors);

                $em->getConnection()->rollback();
                $em->close();

                $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:VisaPopupContent');
                $query = $cust->createQueryBuilder('p')
                        ->where('p.id =:id')
                        ->setParameter("id", 1)
                        ->getQuery();
                $content = $query->getArrayResult();
                        
                return $this->render('AcmeCLSadminBundle:ManageCountries:index.html.twig',
                    array('post'=>$post,
                        'content'=> (isset($content[0]['content'])) ? $content[0]['content'] : '' 
                    )
                );
            }
            
        }
        
        
        $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:VisaPopupContent');
        $query = $cust->createQueryBuilder('p')
                ->where('p.id =:id')
                ->setParameter("id", 1)
                ->getQuery();
        $content = $query->getArrayResult();
                
        return $this->render('AcmeCLSadminBundle:ManageSaudiVisaPopupContent:index.html.twig',
                array(
                    'content'=> (isset($content[0]['content'])) ? $content[0]['content'] : '' 
                ));
    }
    

}
