<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSadminBundle\Entity\SettingsDocumentDelivery;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageDocumentDeliveryController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-document-delivery');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:ManageDocumentDelivery:index.html.twig');
    }
    
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:SettingsDocumentDelivery');
            $query = $cust->createQueryBuilder('d')
                ->select("d.type, d.cost, d.status, d.id")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("type","cost","status", "id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    
    
    
    /**
     * new type of document delivery
     * @return type
     */
    public function newAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-document-delivery');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['type'] = filter_var($_POST['type'], FILTER_SANITIZE_STRING);
            $_POST['cost'] = filter_var($_POST['cost'], FILTER_SANITIZE_STRING);
            
            $model = new SettingsDocumentDelivery();
            $model->setType($_POST['type']);
            $model->setCost($_POST['cost']);
            $status = (isset($_POST['status'])) ? 1 : 0;
            $model->setStatus($status);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'type'=>$_POST['type'],
                'cost'=>$_POST['cost'],
                'status'=>$status
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['type'] . ' has been added.'
                );
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_document_delivery_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['type']. "</a> has been added to the <a href='".$this->generateUrl('acme_cls_admin_manage_document_delivery')."' target='_blank'>list of document delivery types</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_document_delivery'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageDocumentDelivery:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSadminBundle:ManageDocumentDelivery:new.html.twig',
                array(
                    )
                );
        }
    }
    
    
    
    
    /**
     * edit document delivery type
     * @return type
     */
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-document-delivery');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
            $_POST['id'] = intval($_POST['id']);
            $_POST['type'] = filter_var($_POST['type'], FILTER_SANITIZE_STRING);
            $_POST['cost'] = filter_var($_POST['cost'], FILTER_SANITIZE_STRING);
            
            $model = $em->getRepository('AcmeCLSadminBundle:SettingsDocumentDelivery')->findOneBy(array('id'=>$_POST["id"]));
            $model->setType($_POST['type']);
            $model->setCost($_POST['cost']);
            $status = (isset($_POST['status'])) ? 1 : 0;
            $model->setStatus($status);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'id'=>$_POST['id'],
                'type'=>$_POST['type'],
                'cost'=>$_POST['cost'],
                'status'=>$status
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_document_delivery_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['type']. "</a>, a type of document delivery, its details have been updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['type'] . ' has been updated.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_document_delivery'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageDocumentDelivery:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            $em = $this->getDoctrine()->getManager(); 
            
            $_GET['id'] = filter_var($_GET['id'], FILTER_SANITIZE_STRING);
            $_GET['id'] = intval($_GET['id']);
            
            $model = $em->getRepository('AcmeCLSadminBundle:SettingsDocumentDelivery')->findOneBy(array('id'=>$_GET["id"]));
            
            return $this->render('AcmeCLSadminBundle:ManageDocumentDelivery:edit.html.twig',
                array(
                    'post'=>$model
                    )
                );
        }
    }
    
    
    
    
    public function deleteAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $model = $em->getRepository('AcmeCLSadminBundle:SettingsDocumentDelivery')->findOneBy(array('id'=>$_POST['id']));
        
        /* + create log */
        $logDetails = $model->getType() . ", a type of document delivery, has been been deleted and removed from the <a href='".$this->generateUrl('acme_cls_admin_manage_document_delivery')."' target='_blank'>list of document delivery types</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */
        
        $statement = $connection->prepare("DELETE FROM tbl_settings_document_delivery WHERE id=".$_POST['id']);
        $statement->execute();
        
        
        return new Response("success");
        
    }

}
