<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class HomeController extends Controller
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel;
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-home');
        $new_pass =0;
        if ($session->get('pass')!=''){
            if($mod->passwordHardening($session->get('pass')) != '1#1#1#1'){
                $new_pass = 1;
                $session->set('pass', '');
            }
        }
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:Home:index.html.twig',array('new_pass'=>$new_pass));
    }

    public function govVisaListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDestinations');
            $query = $cust->createQueryBuilder('v')
                ->select("v.id, o.date_submitted, v.order_no, v.country_id, v.status, c.countryName as destination, v.departure_date, o.primary_traveller_name, 
                    d.name as department, o.status as order_status, o.order_type, u.account_no")
                ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = v.order_no')
                ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = v.country_id')
                ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = o.client_id')
                ->where('(o.order_type = 1 OR o.order_type = 3) AND o.status >= 10')
                ->orderBy("o.date_submitted", "DESC")
                ->setMaxResults(30)
                //->setParameter('status', "")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","primary_traveller_name","destination","departure_date","order_status","order_type","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function tpnListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Tpn');
            $query = $cust->createQueryBuilder('p')
                ->select("p.tpn_no, o.date_submitted, p.date_last_updated, p.order_no, p.client_id, o.primary_traveller_name, p.departure_date,
                    d.name as department, c.countryName as destination, p.status, o.status as order_status, o.order_type, o.order_no,
                    u.account_no")
                ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = p.order_no')
                ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = p.destination')
                ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = o.client_id')
                ->orderBy("o.date_submitted", "DESC")
                ->setMaxResults(30)
                //->where('p.status = 0')
                //->setParameter('status', "")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","department","account_no","tpn_no","primary_traveller_name","destination","departure_date","order_status","status","order_type","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function passportListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
//            
//            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
//            $query = $cust->createQueryBuilder('o')
//                ->select("o.date_submitted, d.name as department, u.account_no, o.order_no, CONCAT(CONCAT(pay.fname, ' '), pay.lname) as name, 
//                    CONCAT(CONCAT(CONCAT(CONCAT(o.passport_office_booking_time,' '), o.passport_office_booking_time_hr), ':'), o.passport_office_booking_time_min) as booking_time, 
//                    o.status as order_status")
//                ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = o.client_id')
//                ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = u.department_id')
//                ->leftJoin('AcmeCLSclientGovBundle:Payment', 'pay', 'WITH', 'pay.order_no = o.order_no')    
//                ->where('o.order_type = 4 AND o.status >= 10')
//                ->orderBy("o.date_submitted", "DESC")
//                ->setMaxResults(30)
//                //->setParameter('status', "")
//                ->getQuery();
//            $results = $query->getArrayResult();
//            
            
            $em = $this->getDoctrine()->getEntityManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("
                SELECT o.date_submitted, u.account_no, o.order_no, CONCAT(CONCAT(pay.fname, ' '), pay.lname) AS `name`, 
                    CONCAT(CONCAT(CONCAT(CONCAT(o.passport_office_booking_time,' '), o.passport_office_booking_time_hr), ':'), o.passport_office_booking_time_min) AS booking_time, 
                    o.status AS order_status, 
                    (SELECT GROUP_CONCAT(opa.fullname)  FROM tbl_order_passport_applicants opa WHERE opa.order_no=o.order_no) AS applicants

                FROM tbl_orders o
                LEFT JOIN tbl_payment pay ON pay.order_no = o.order_no
                LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
                LEFT JOIN tbl_user_client u ON u.id = o.client_id
                WHERE o.status >= 10 AND o.order_type = 4 
                ORDER BY o.date_submitted DESC 
                LIMIT 30
                ");
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","applicants","account_no","order_no","name","booking_time","order_status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    
    public function policeClearancesListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $connection = $em->getConnection();
            $statement = $connection->prepare("
                SELECT o.date_submitted, p.account_no, o.order_no,  
                    (Select group_concat(concat(concat(opa.fname,' '), opa.lname))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicants,
                    (Select group_concat(opa.passport_no)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as passports,
                    (Select group_concat(opa.departure_date)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as departure_dates,
                     o.status, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem, pc.name as police_clearance_type
                FROM tbl_orders o
                LEFT JOIN tbl_payment p ON p.order_no = o.order_no
                LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
                LEFT JOIN tbl_police_clearances pc ON pc.id = o.police_clearance_id
                WHERE o.status >= 10 AND o.order_type = 5 
                ORDER BY o.date_submitted DESC 
                LIMIT 30
                ");
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","account_no","order_no","applicants","police_clearance_type","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    
    
    
    public function publicVisaListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:OrderDestinations');
            $query = $cust->createQueryBuilder('v')
                ->select("v.id, o.date_submitted, v.order_no, v.country_id, v.status, c.countryName as destination, v.departure_date, o.primary_traveller_name, 
                    u.company as department, o.status as order_status, o.order_type, u.account_no")
                ->leftJoin('AcmeCLSclientGovBundle:Orders', 'o', 'WITH', 'o.order_no = v.order_no')
                ->leftJoin('AcmeCLSclientGovBundle:Departments', 'd', 'WITH', 'd.id = o.pri_dept_contact_department_id')
                ->leftJoin('AcmeCLSclientGovBundle:Countries', 'c', 'WITH', 'c.id = v.country_id')
                ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = o.client_id')
                ->where('(o.order_type = 6) AND o.status >= 10')
                ->orderBy("o.date_submitted", "DESC")
                ->setMaxResults(30)
                //->setParameter('status', "")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","primary_traveller_name","destination","departure_date","order_status","order_type","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    
    
    public function docDeliveryListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("
                SELECT o.date_submitted, p.account_no, o.order_no, CONCAT(CONCAT(p.fname, ' '), p.lname) as customer,
                    o.doc_package_total_pieces, o.doc_package_pickup_date, CONCAT(CONCAT(o.doc_package_ready_hr,':'), o.doc_package_ready_min) as pickup_time,
                    CONCAT(CONCAT(o.doc_package_office_close_hr,':'), o.doc_package_office_close_min) as office_close_time, 
                    dd.type as courier_type, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem,
                    o.status
                FROM tbl_orders o
                LEFT JOIN tbl_payment p ON p.order_no = o.order_no
                LEFT JOIN tbl_settings_document_delivery dd ON dd.id = o.doc_delivery_type
                LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
                WHERE o.status >= 10 AND o.order_type = 7 
                ORDER BY o.date_submitted DESC 
                LIMIT 30
                ");
            
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","account_no","order_no","customer","doc_package_total_pieces","doc_package_pickup_date",
                "pickup_time","office_close_time","courier_type","cls_team_mem","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function russianVisaVoucherListAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("
                SELECT o.date_submitted, p.account_no, o.order_no, o.primary_traveller_name as applicant,
                    rvv.name as russian_visa_voucher_name, o.grand_total, o.status
                FROM tbl_orders o
                LEFT JOIN tbl_payment p ON p.order_no = o.order_no
                LEFT JOIN tbl_russian_visa_voucher_types rvv ON rvv.id = o.russian_visa_voucher_id
                LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
                WHERE o.status >= 10 AND o.order_type = 8 
                GROUP BY o.order_no
                ORDER BY o.date_submitted DESC 
                LIMIT 30
                ");
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","account_no","order_no","applicant","russian_visa_voucher_name","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function documentLegalisationAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Orders');
            $query = $cust->createQueryBuilder('o')
                ->select("o.order_no, o.date_submitted, o.status, u.fname, u.lname")
                ->leftJoin('AcmeCLSclientGovBundle:Payment', 'p', 'WITH', 'p.order_no = o.order_no')
                ->leftJoin('AcmeCLSclientGovBundle:User', 'u', 'WITH', 'u.id = o.client_id')
                ->where('o.status >= 10 AND o.order_type = 9')
                ->orderBy("o.date_submitted", "DESC")
                ->setMaxResults(30)
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("date_submitted","date_submitted","order_no","fname","lname","status","order_no");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    public function changePasswordAction(){
        if(isset($_POST['password'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = $em->getRepository('AcmeCLSadminBundle:AdminUser')->findOneBy(array('id'=>$session->get('admin_id')));
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }        
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            $error_code = 0;
            
                
            if($error_count == 0){
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $error_code =1;
                        $error_count += 1;
                    }
                }
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $error_code = 2;//'You must Follow the password required standard.';
                $error_count += 1;
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
               
                $this->sendEmail($session->get('admin_email'), "help@capitallinkservices.com.au", "Change Password",
                                        $this->renderView('AcmeCLSadminBundle:Home:changePassword.html.twig',
                                        array('fname'=>$session->get('admin_fname'),
                                            'password'=> $_POST['password'],
                                            'domain'=>$mod->siteURL()
                                            )
                                        ));
      
            }else{
                $em->getConnection()->rollback();
                $em->close();
                $error_code = 3;
            }
            
            return new Response($error_code);
        }
    }
    
    public function sendEmail($recipients, $from, $subject, $body){
        $message = \Swift_Message::newInstance()
            ->setEncoder(\Swift_Encoding::get8BitEncoding())
            ->setSubject($subject)
            ->setFrom($from,'Capital Link Services')
            ->setTo($recipients)
            ->setBody($body)
            ->setContentType("text/html")
        ;
        $this->get('mailer')->send($message);
    }
}
