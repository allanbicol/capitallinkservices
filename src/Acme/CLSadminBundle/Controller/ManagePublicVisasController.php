<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\Countries;
use Acme\CLSadminBundle\Entity\PublicVisaTypes;
use Acme\CLSadminBundle\Entity\PublicVisaAdditionalRequirements;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManagePublicVisasController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-public-visas');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:ManagePublicVisas:list.html.twig',
                array('countries'=>$this->getCountries()));
    }
    
    public function manageVisaAction(){
        
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-public-visas');
        $request = $this->getRequest();
        
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        if(isset($_POST['hidSubmit'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $connection = $em->getConnection();
            
            $error_count = 0;
            
            $visa_types = array();
            $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
            
            
            $country = $em->getRepository('AcmeCLSclientGovBundle:Countries')->findOneBy(array('id'=>$_POST['id']));
            if(isset($_POST['sNoVisaRequired'])){
                $country->setPublicSNoVisaRequired(1);
                $country->setPublicSNoVisaRequiredHtml($_POST['noVisaRequiredInformation']);
            }else{
                $country->setPublicSNoVisaRequired(0);
            }
            $em->persist($country);
            $em->flush();
            
            
            if(isset($_POST['visaTypeRemove']) && trim($_POST['visaTypeRemove']) != ''){
                $_POST['visaTypeRemove'] = filter_var($_POST['visaTypeRemove'], FILTER_SANITIZE_STRING);
                $remove_type_ids = explode(",", $_POST['visaTypeRemove']);
                for($e=0; $e<count($remove_type_ids); $e++){
                    $statement = $connection->prepare("DELETE FROM tbl_public_visa_types WHERE  id=".$remove_type_ids[$e]." AND country_id=".$_POST['id']."");
                    $statement->execute();   
                }
            }
            
            for($i=0; $i<count($_POST['visaType']); $i++){
                // visa type
                
                
                if(isset($_POST['visaTypeId'][$i])){
                    $vs = $em->getRepository('AcmeCLSadminBundle:PublicVisaTypes')->findOneBy(array('id'=>$_POST['visaTypeId'][$i]));
                }else{
                    $vs = new PublicVisaTypes();
                }
                $vs->setCountryId($_POST['id']);
                $vs->setType($_POST['visaType'][$i]);
                $vs->setCost($_POST['visaCost'][$i]);
                if(trim($_FILES["visaFile"]["name"][$i]) != ''){
                    $vs->setFileAttachment($_FILES["visaFile"]["name"][$i]);
                }
                $vs->setVisaInformation($_POST['visaInformation'][$i]);
                $vs->setStatus($_POST['visaStatus'][$i]);
                $em->persist($vs);
                $em->flush();
                
                if(trim($_FILES["visaFile"]["name"][$i] != "")){
                    $mod->createDirectory($root_dir ."/dev/publicvisafiles/", $_POST['id'] . "/" . $vs->getId());
                    move_uploaded_file($_FILES["visaFile"]["tmp_name"][$i], $root_dir ."/dev/publicvisafiles/".$_POST['id']."/".$vs->getId()."/". $_FILES["visaFile"]["name"][$i]);
                }
                
                // visa type additional requirements
                $visa_type_requirements = array();
                if(isset($_POST['additionalRequirement'][$i])){ 
                    
                    if(isset($_POST['additionalRequirementRemove'][$i]) && trim($_POST['additionalRequirementRemove'][$i]) != ''){
                        $_POST['additionalRequirementRemove'][$i] = filter_var($_POST['additionalRequirementRemove'][$i], FILTER_SANITIZE_STRING);
                        $remove_req_ids = explode(",", $_POST['additionalRequirementRemove'][$i]);
                        
                        for($e=0; $e<count($remove_req_ids); $e++){
                            $statement = $connection->prepare("DELETE FROM tbl_public_visa_additional_requirements WHERE  id=".$remove_req_ids[$e]." AND visa_id=".$vs->getId()."");
                            $statement->execute();   
                        }
                    }
                    
                    for($e=0; $e<count($_POST['additionalRequirement'][$i]); $e++){
                        if(isset($_POST['additionalRequirementId'][$i][$e])){
                            $var = $em->getRepository('AcmeCLSadminBundle:PublicVisaAdditionalRequirements')->findOneBy(array('id'=>$_POST['additionalRequirementId'][$i][$e]));
                        }else{
                            $var = new PublicVisaAdditionalRequirements();
                        }
                        $var->setVisaId($vs->getId());
                        $var->setRequirement($_POST['additionalRequirement'][$i][$e]);
                        $var->setCost($_POST['additionalRequirementCost'][$i][$e]);
                        $var->setSRequired($_POST['additionalRequirementIsRequired'][$i][$e]);
                        $var->setStatus($_POST['additionalRequirementIsActive'][$i][$e]);
                        $var->setItemOrder($e);
                        $em->persist($var);
                        $em->flush();
                        
                        $visa_type_requirements[] = array(
                            'id'=>$var->getId(),
                            'visa_id'=>$var->getVisaId(),
                            'requirement'=>$_POST['additionalRequirement'][$i][$e],
                            'cost'=>$_POST['additionalRequirementCost'][$i][$e],
                            's_required'=>$_POST['additionalRequirementIsRequired'][$i][$e],
                            'status'=>$_POST['additionalRequirementIsActive'][$i][$e]
                        );
                    }
                }
                
                
                
                $visa_types[] =array(
                    'id'=>$vs->getId(),
                    'type'=>$_POST['visaType'][$i],
                    'cost'=>$_POST['visaCost'][$i],
                    'status'=>$_POST['visaStatus'][$i],
                    'file_attachment'=>'',
                    'additional_requirements'=>$visa_type_requirements
                    );
                
                
                
            }
            
            
            
            $post = array(
                'country_id'=>$_POST['id'],
                'country_name'=>$_POST['countryName'],
                'visa_information'=>$_POST['visaInformation'],
                'visa_types'=>$visa_types
            );
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_public_visas_management') ."?country=". $_POST['id'] ."' target='_blank'>". $_POST['countryName']  . " public visa</a>, its details have been updated  by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                 $this->get('session')->getFlashBag()->add(
                        'success',
                        'Visa has been updated'
                    );
                 
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_public_visas_management').'?country='.$_POST['id']); 
            }else{
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors);
                $em->getConnection()->rollback();
                $em->close();
                
                return $this->render('AcmeCLSadminBundle:ManagePublicVisas:manage.html.twig',
                    array('post'=>$post,
                        'fileUrl'=>dirname($request->getBaseUrl())
                    )
                );
            }
            
            
            
        }else{
            
            return $this->render('AcmeCLSadminBundle:ManagePublicVisas:manage.html.twig',
                array('post'=>$this->getPublicVisaDetailsByCountryId($_GET['country']),
                    'fileUrl'=>dirname($request->getBaseUrl())
                    )
            );
        }
        
        
    }

}
