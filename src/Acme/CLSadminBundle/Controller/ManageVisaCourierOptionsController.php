<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSadminBundle\Entity\VisaCourierOptions;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageVisaCourierOptionsController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-visa-courier-options');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:ManageVisaCourierOptions:index.html.twig');
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaCourierOptions');
            $query = $cust->createQueryBuilder('c')
                ->select("c.type, c.cost, c.sActive, c.id")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("type","cost","sActive","id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    /**
     * new option
     * @return type
     */
    public function newAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-visa-courier-options');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['type'] = filter_var($_POST['type'], FILTER_SANITIZE_STRING);
            $_POST['cost'] = filter_var($_POST['cost'], FILTER_SANITIZE_STRING);
            
            $_POST['cost'] = floatval($_POST['cost']);
            
            $model = new VisaCourierOptions();
            $model->setType($_POST['type']);
            $model->setCost($_POST['cost']);
            
            $sActive = (isset($_POST['sActive'])) ? 1 : 0;
			$s_available_for_gov = (isset($_POST['s_available_for_gov'])) ? 1 : 0;
			$s_available_for_public = (isset($_POST['s_available_for_public'])) ? 1 : 0;
			$s_dhl = (isset($_POST['s_dhl'])) ? 1 : 0;
            $model->setSActive($sActive);
			$model->setSAvailableForPublic($s_available_for_public);
			$model->setSAvailableForGov($s_available_for_gov);
			$model->setSDhl($s_dhl);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'type'=>$_POST['type'],
                'cost'=>$_POST['cost'],
                'sActive'=>$sActive,
				's_available_for_public'=>$s_available_for_public,
				's_available_for_gov'=>$s_available_for_gov,
				's_dhl'=>$s_dhl,
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_visa_courier_options_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['type']. "</a> has been added to the <a href='".$this->generateUrl('acme_cls_admin_manage_visa_courier_options')."' target='_blank'>list of visa courier options</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['type'] . ' has been added.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_visa_courier_options'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageVisaCourierOptions:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSadminBundle:ManageVisaCourierOptions:new.html.twig',
                array(
                    )
                );
        }
    }
    
    /**
     * edit option
     * @return type
     */
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-visa-courier-options');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST["id"] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
            $_POST['type'] = filter_var($_POST['type'], FILTER_SANITIZE_STRING);
            $_POST['cost'] = filter_var($_POST['cost'], FILTER_SANITIZE_STRING);
            
            $_POST['cost'] = floatval($_POST['cost']);
            
            
            $model = $em->getRepository('AcmeCLSadminBundle:VisaCourierOptions')->findOneBy(array('id'=>$_POST["id"]));
            $model->setType($_POST['type']);
            $model->setCost($_POST['cost']);
            
            $sActive = (isset($_POST['sActive'])) ? 1 : 0;
            $s_available_for_gov = (isset($_POST['s_available_for_gov'])) ? 1 : 0;
			$s_available_for_public = (isset($_POST['s_available_for_public'])) ? 1 : 0;
			$s_dhl = (isset($_POST['s_dhl'])) ? 1 : 0;
            $model->setSActive($sActive);
			$model->setSAvailableForPublic($s_available_for_public);
			$model->setSAvailableForGov($s_available_for_gov);
			$model->setSDhl($s_dhl);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'id'=>$_POST['id'],
                'type'=>$_POST['type'],
                'cost'=>$_POST['cost'],
                'sActive'=>$sActive,
				's_available_for_public'=>$s_available_for_public,
				's_available_for_gov'=>$s_available_for_gov,
				's_dhl'=>$s_dhl,
            );
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_visa_courier_options_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['type']. "</a>, a visa courier, its details have been updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['type'] . ' has been updated.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_visa_courier_options'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageVisaCourierOptions:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            $_GET['id'] = intval($_GET['id']);

            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:VisaCourierOptions');
            $query = $cust->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id', $_GET["id"])
                ->getQuery();
            $data = $query->getArrayResult();
			
            return $this->render('AcmeCLSadminBundle:ManageVisaCourierOptions:edit.html.twig',
                array(
                    'post'=>(count($data) > 0) ? $data[0] : array()
                    )
                );
        }
    }
    
    
    public function deleteAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $model = $em->getRepository('AcmeCLSadminBundle:VisaCourierOptions')->findOneBy(array('id'=>$_POST['id']));
        
        $statement = $connection->prepare("DELETE FROM tbl_visa_courier_options WHERE id=".$_POST['id']);
        $statement->execute();
        
        
        /* + create log */
        $logDetails = $model->getType() . ", a visa courier option, has been been deleted and removed from the <a href='".$this->generateUrl('acme_cls_admin_manage_visa_courier_options')."' target='_blank'>list of visa courier options</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */

        return new Response("success");
        
    }

}
