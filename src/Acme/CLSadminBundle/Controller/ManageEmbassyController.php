<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\User;
use Acme\CLSclientGovBundle\Entity\Countries;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;


class ManageEmbassyController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-embassy');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(isset($_POST['hidSubmit'])){
            $_POST['id'] = intval($_POST['id']);
            $_POST['embassy_address_line1'] = filter_var($_POST['embassy_address_line1'], FILTER_SANITIZE_STRING);
            $_POST['embassy_address_line2'] = filter_var($_POST['embassy_address_line2'], FILTER_SANITIZE_STRING);
            $_POST['embassy_street'] = filter_var($_POST['embassy_street'], FILTER_SANITIZE_STRING);
            $_POST['embassy_city'] = filter_var($_POST['embassy_city'], FILTER_SANITIZE_STRING);
            $_POST['embassy_state'] = filter_var($_POST['embassy_state'], FILTER_SANITIZE_STRING);
            $_POST['embassy_postcode'] = filter_var($_POST['embassy_postcode'], FILTER_SANITIZE_STRING);
            $_POST['embassy_phone'] = filter_var($_POST['embassy_phone'], FILTER_SANITIZE_STRING);
            

            if($_POST["countrySave"] == 1){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction(); 
                
                $model = $em->getRepository('AcmeCLSclientGovBundle:Countries')->findOneBy(array('id'=>$_POST['id']));
                $model->setEmbassyAddressLine1($_POST['embassy_address_line1']);
                $model->setEmbassyAddressLine2($_POST['embassy_address_line2']);
                $model->setEmbassyCity($_POST['embassy_city']);
                $model->setEmbassyPhone($_POST['embassy_phone']);
                $model->setEmbassyPostcode($_POST['embassy_postcode']);
                $model->setEmbassyState($_POST['embassy_state']);
                $model->setEmbassyStreet($_POST['embassy_street']);
                $model->setPriority(999);
                $em->persist($model);
                $em->flush();

                $validator = $this->get('validator');
                $errors = $validator->validate($model);
                $error_count = count($errors);


                $post = $_POST;

                if($error_count == 0){

                    $em->getConnection()->commit(); 

                    $this->get('session')->getFlashBag()->add(
                        'success',
                        'Contact details have been updated'
                    );

                    return $this->redirect($this->generateUrl('acme_cls_admin_manage_embassy')); 

                }else{

                    $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors);

                    $em->getConnection()->rollback();
                    $em->close();

                    return $this->render('AcmeCLSadminBundle:ManageEmbassy:index.html.twig',
                        array('post'=>$post,
                            'countries'=>$this->getCountries()
                        )
                    );
                }
            }
        }
        
        
        return $this->render('AcmeCLSadminBundle:ManageEmbassy:index.html.twig',
                array('countries'=>$this->getCountries()));
    }

}
