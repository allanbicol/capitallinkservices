<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\SettingsTPN;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class TicketsReportController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    
    /**
     * Start: Visa
     */
    public function visaAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tickets-report-visa');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:TicketsReport:visa.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function visaListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['ddfrom'])){
            if(trim($_GET['ddfrom']) != ''){
                $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
                $_GET['ddfrom'] = trim($_GET['ddfrom']);
                $condition .= ' AND od.departure_date >= "'. $mod->changeFormatToOriginal($_GET['ddfrom']).' 00:00:59"';
            }
        }

        if(isset($_GET['ddto'])){

            if(trim($_GET['ddto']) != ''){
                $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
                $_GET['ddto'] = trim($_GET['ddto']);
                $condition .= ' AND od.departure_date <= "'.$mod->changeFormatToOriginal($_GET['ddto']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        if(isset($_GET['destination'])){

            if(trim($_GET['destination']) != ''){
                $_GET['destination'] = filter_var($_GET['destination'], FILTER_SANITIZE_STRING);
                $_GET['destination'] = trim($_GET['destination']);
                $_GET['destination'] = intval($_GET['destination']);
                $condition .= ' AND od.country_id = '.$_GET['destination'];
            }
        }
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, d.name as department, p.account_no, o.order_no, o.status, c.country_name,
                o.date_completed, o.primary_traveller_name, vt.type as visa_type, DATE_FORMAT(od.departure_date,'%d-%m-%Y') as departure_date, p.s_paid, CONCAT(CONCAT(au.fname, ' '), au.lname) as team_mem,
                od.id
            FROM tbl_order_destinations od
            LEFT JOIN tbl_orders o ON o.order_no = od.order_no
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = od.country_id
            LEFT JOIN tbl_visa_types vt ON vt.id = od.selected_visa_type
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND (o.order_type = 1 OR o.order_type = 3)
            " . $condition . " GROUP BY o.order_no");
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","status","country_name","date_completed","primary_traveller_name","visa_type", "departure_date", "s_paid", "team_mem", "id");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    /**
     * Public Visa
     */
    
    public function publicVisaAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tickets-report-public-visa');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:TicketsReport:publicVisa.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function publicVisaListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['ddfrom'])){
            if(trim($_GET['ddfrom']) != ''){
                $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
                $_GET['ddfrom'] = trim($_GET['ddfrom']);
                $condition .= ' AND od.departure_date >= "'. $mod->changeFormatToOriginal($_GET['ddfrom']).' 00:00:59"';
            }
        }

        if(isset($_GET['ddto'])){

            if(trim($_GET['ddto']) != ''){
                $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
                $_GET['ddto'] = trim($_GET['ddto']);
                $condition .= ' AND od.departure_date <= "'.$mod->changeFormatToOriginal($_GET['ddto']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        if(isset($_GET['destination'])){

            if(trim($_GET['destination']) != ''){
                $_GET['destination'] = filter_var($_GET['destination'], FILTER_SANITIZE_STRING);
                $_GET['destination'] = trim($_GET['destination']);
                $_GET['destination'] = intval($_GET['destination']);
                $condition .= ' AND od.country_id = '.$_GET['destination'];
            }
        }
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, d.name as department, p.account_no, o.order_no, o.status, c.country_name,
                o.date_completed, o.primary_traveller_name, vt.type as visa_type, DATE_FORMAT(od.departure_date,'%d-%m-%Y') as departure_date, p.s_paid, CONCAT(CONCAT(au.fname, ' '), au.lname) as team_mem,
                od.id
            FROM tbl_order_destinations od
            LEFT JOIN tbl_orders o ON o.order_no = od.order_no
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = od.country_id
            LEFT JOIN tbl_visa_types vt ON vt.id = od.selected_visa_type
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 6
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","status","country_name","date_completed","primary_traveller_name","visa_type", "departure_date", "s_paid", "team_mem", "id");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    /**
     * TPN (Third Person Note)
     */
    
    
    public function tpnAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tickets-report-tpn');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:TicketsReport:tpn.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function tpnListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['ddfrom'])){
            if(trim($_GET['ddfrom']) != ''){
                $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
                $_GET['ddfrom'] = trim($_GET['ddfrom']);
                $condition .= ' AND od.departure_date >= "'. $mod->changeFormatToOriginal($_GET['ddfrom']).' 00:00:59"';
            }
        }

        if(isset($_GET['ddto'])){

            if(trim($_GET['ddto']) != ''){
                $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
                $_GET['ddto'] = trim($_GET['ddto']);
                $condition .= ' AND od.departure_date <= "'.$mod->changeFormatToOriginal($_GET['ddto']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['department'])){

            if(trim($_GET['department']) != ''){
                $_GET['department'] = filter_var($_GET['department'], FILTER_SANITIZE_STRING);
                $_GET['department'] = trim($_GET['department']);
                $_GET['department'] = intval($_GET['department']);
                $condition .= ' AND o.pri_dept_contact_department_id = '.$_GET['department'];
            }
        }
        
        if(isset($_GET['destination'])){

            if(trim($_GET['destination']) != ''){
                $_GET['destination'] = filter_var($_GET['destination'], FILTER_SANITIZE_STRING);
                $_GET['destination'] = trim($_GET['destination']);
                $_GET['destination'] = intval($_GET['destination']);
                $condition .= ' AND t.destination = '.$_GET['destination'];
            }
        }
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, d.name as department, p.account_no, o.order_no, t.tpn_no, 
                o.primary_traveller_name, c.country_name, DATE_FORMAT(t.departure_date,'%d-%m-%Y') as departure_date, t.status,
                t.tpn_no, p.s_paid
            FROM tbl_tpn t
            LEFT JOIN tbl_orders o ON o.order_no = t.order_no
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_departments d ON d.id = o.pri_dept_contact_department_id
            LEFT JOIN tbl_countries c ON c.id = t.destination
            WHERE o.status >= 10 AND (o.order_type = 2 OR o.order_type = 3)
            " . $condition . " GROUP BY t.tpn_no");
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","department","account_no","order_no","tpn_no","primary_traveller_name","country_name","departure_date","status","s_paid", "tpn_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }

    
    
    
    /**
     * Passport Office Pickup or Delivery
     */
    
    public function passportAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tickets-report-passport');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:TicketsReport:passport.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    public function passportListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        if(isset($_GET['ddfrom'])){
            if(trim($_GET['ddfrom']) != ''){
                $_GET['ddfrom'] = filter_var($_GET['ddfrom'], FILTER_SANITIZE_STRING);
                $_GET['ddfrom'] = trim($_GET['ddfrom']);
                $condition .= ' AND o.passport_office_booking_time >= "'. $mod->changeFormatToOriginal($_GET['ddfrom']).' 00:00:59"';
            }
        }

        if(isset($_GET['ddto'])){

            if(trim($_GET['ddto']) != ''){
                $_GET['ddto'] = filter_var($_GET['ddto'], FILTER_SANITIZE_STRING);
                $_GET['ddto'] = trim($_GET['ddto']);
                $condition .= ' AND o.passport_office_booking_time <= "'.$mod->changeFormatToOriginal($_GET['ddto']).' 23:59:59"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no,  
                (Select group_concat(opa.fullname) as applicants from tbl_order_passport_applicants opa where opa.order_no=o.order_no) as applicants,
                o.passport_office_booking_no, p.s_paid, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 4
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","account_no","order_no","applicants","passport_office_booking_no","s_paid","cls_team_mem","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    
    /**
     * Passport Police Clearance
     */
    
    public function policeClearanceAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tickets-report-police-clearance');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:TicketsReport:policeClearance.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function policeClearanceListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no, o.order_type,
                (Select group_concat(concat(concat(opa.fname,' '), opa.lname))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as applicants,
                (Select group_concat(opa.passport_no)  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as passports,
                (Select group_concat(DATE_FORMAT(opa.departure_date,'%d-%m-%Y'))  from tbl_order_police_clearance_applicants opa where opa.order_no=o.order_no) as departure_dates,
                 o.status, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 5
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","account_no","order_no","applicants","passports","departure_dates","status","cls_team_mem","order_type");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    
    
    /**
     * Document Delivery
     */
    
    public function docDeliveryAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tickets-report-doc-delivery');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:TicketsReport:docDelivery.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    
    public function docDeliveryListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no, CONCAT(CONCAT(p.fname, ' '), p.lname) as customer,
                o.doc_package_total_pieces, DATE_FORMAT(o.doc_package_pickup_date,'%d-%m-%Y') as doc_package_pickup_date, CONCAT(CONCAT(o.doc_package_ready_hr,':'), o.doc_package_ready_min) as pickup_time,
                CONCAT(CONCAT(o.doc_package_office_close_hr,':'), o.doc_package_office_close_min) as office_close_time, 
                dd.type as courier_type, o.order_no, CONCAT(CONCAT(au.fname, ' '), au.lname) as cls_team_mem,visa_is_delivered_to_embassy_date as embassy_delivered_date
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_settings_document_delivery dd ON dd.id = o.doc_delivery_type
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 7
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","account_no","order_no","customer","doc_package_total_pieces","doc_package_pickup_date",
            "pickup_time","office_close_time","embassy_delivered_date","courier_type","cls_team_mem","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    public function russianVisaVoucherAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tickets-report-russian-visa-voucher');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:TicketsReport:russianVisaVoucher.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    public function russianVisaVoucherListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['onaccount'])){

            if(trim($_GET['onaccount']) != ''){
                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
                $_GET['onaccount'] = trim($_GET['onaccount']);
                $_GET['onaccount'] = intval($_GET['onaccount']);
                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
            }
        }
        
        if(isset($_GET['accountno'])){

            if(trim($_GET['accountno']) != ''){
                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
                $_GET['accountno'] = trim($_GET['accountno']);
                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
            }
        }

        if(isset($_GET['clsteammem'])){

            if(trim($_GET['clsteammem']) != ''){
                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
                $_GET['clsteammem'] = trim($_GET['clsteammem']);
                $_GET['clsteammem'] = intval($_GET['clsteammem']);
                $condition .= ' AND au.id = '.$_GET['clsteammem'];
            }
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, p.account_no, o.order_no, o.primary_traveller_name as applicant,
                rvv.name as russian_visa_voucher_name, o.grand_total, o.status
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_russian_visa_voucher_types rvv ON rvv.id = o.russian_visa_voucher_id
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 8
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","account_no","order_no","applicant","russian_visa_voucher_name","status","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
    
    
    public function documentLegalisationAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'tickets-report-document-legalisation');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:TicketsReport:docLegalisation.html.twig',
                array('get'=> $_GET,
                    'departments'=> $this->getDepartments(),
                    'destinations' => $this->getCountries(),
                    'cls_members' => $this->getAdminUsers()
                )
            );
    }
    
    
    public function documentLegalisationListAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        $json = new Model\Json;
        if($session->get('admin_email') == ''){
            return new Response('session expired');
        }
        
        $condition = '';

        // date from filter
        if(isset($_GET['from'])){
            if(trim($_GET['from']) != ''){
                $_GET['from'] = filter_var($_GET['from'], FILTER_SANITIZE_STRING);
                $_GET['from'] = trim($_GET['from']);
                $condition .= ' AND o.date_submitted >= "'. $mod->changeFormatToOriginal($_GET['from']).' 00:00:59"';
            }
        }

        if(isset($_GET['to'])){

            if(trim($_GET['to']) != ''){
                $_GET['to'] = filter_var($_GET['to'], FILTER_SANITIZE_STRING);
                $_GET['to'] = trim($_GET['to']);
                $condition .= ' AND o.date_submitted <= "'.$mod->changeFormatToOriginal($_GET['to']).' 23:59:59"';
            }
        }
        
        
        if(isset($_GET['paymentstat'])){

            if(trim($_GET['paymentstat']) != ''){
                $_GET['paymentstat'] = filter_var($_GET['paymentstat'], FILTER_SANITIZE_STRING);
                $_GET['paymentstat'] = trim($_GET['paymentstat']);
                $_GET['paymentstat'] = intval($_GET['paymentstat']);
                $condition .= ' AND p.s_paid = '.$_GET['paymentstat'];
            }
        }
        
        if(isset($_GET['orderstat'])){

            if(trim($_GET['orderstat']) != ''){
                $_GET['orderstat'] = filter_var($_GET['orderstat'], FILTER_SANITIZE_STRING);
                $_GET['orderstat'] = trim($_GET['orderstat']);
                $_GET['orderstat'] = intval($_GET['orderstat']);
                $condition .= ' AND o.status = '.$_GET['orderstat'];
            }
        }
        
//        if(isset($_GET['onaccount'])){
//
//            if(trim($_GET['onaccount']) != ''){
//                $_GET['onaccount'] = filter_var($_GET['onaccount'], FILTER_SANITIZE_STRING);
//                $_GET['onaccount'] = trim($_GET['onaccount']);
//                $_GET['onaccount'] = intval($_GET['onaccount']);
//                $condition .= ' AND p.payment_option = '.$_GET['onaccount'];
//            }
//        }
//        
//        if(isset($_GET['accountno'])){
//
//            if(trim($_GET['accountno']) != ''){
//                $_GET['accountno'] = filter_var($_GET['accountno'], FILTER_SANITIZE_STRING);
//                $_GET['accountno'] = trim($_GET['accountno']);
//                $condition .= ' AND p.account_no = "'.$_GET['accountno'].'"';
//            }
//        }

//        if(isset($_GET['clsteammem'])){
//
//            if(trim($_GET['clsteammem']) != ''){
//                $_GET['clsteammem'] = filter_var($_GET['clsteammem'], FILTER_SANITIZE_STRING);
//                $_GET['clsteammem'] = trim($_GET['clsteammem']);
//                $_GET['clsteammem'] = intval($_GET['clsteammem']);
//                $condition .= ' AND au.id = '.$_GET['clsteammem'];
//            }
//        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("
            SELECT o.date_submitted, o.order_no, CONCAT(CONCAT(uc.fname, ' '), uc.lname) AS applicant, o.status,
                p.s_paid, c.country_name
            FROM tbl_orders o
            LEFT JOIN tbl_countries c ON c.id = o.dl_embassy
            LEFT JOIN tbl_payment p ON p.order_no = o.order_no
            LEFT JOIN tbl_user_client uc ON uc.id = o.client_id
            LEFT JOIN tbl_user_admin au ON au.id = o.visa_cls_team_member
            WHERE o.status >= 10 AND o.order_type = 9
            " . $condition);
        $statement->execute();
        $results = $statement->fetchAll();
        $total_pages= count($results);
        $fieldNames = array("date_submitted","date_submitted","order_no","applicant","country_name","s_paid","status","order_no");

        return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        
    }
}
