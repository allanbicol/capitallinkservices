<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSadminBundle\Entity\TravelAlerts;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageTravelAlertsController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-travel-alerts');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:ManageTravelAlerts:index.html.twig');
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:TravelAlerts');
            $query = $cust->createQueryBuilder('d')
                ->select("d.alert_date, d.subject, d.body, d.status, d.id")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("alert_date","subject","status", "id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function newAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-travel-alerts');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['alert_date'] = filter_var($_POST['alert_date'], FILTER_SANITIZE_STRING);
            $_POST['subject'] = filter_var($_POST['subject'], FILTER_SANITIZE_STRING);
            $_POST['body'] = filter_var($_POST['body'], FILTER_SANITIZE_STRING);
            $_POST['status'] = filter_var($_POST['status'], FILTER_SANITIZE_STRING);
            
            
            $model = new TravelAlerts();
            $model->setAdminId($session->get('admin_id'));
            $model->setAlertDate($mod->changeFormatToOriginal($_POST['alert_date']));
            $model->setBody($_POST['body']);
            $model->setSubject($_POST['subject']);
            $model->setStatus($_POST['status']);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'alert_date'=>$_POST['alert_date'],
                'subject'=>$_POST['subject'],
                'body'=>$_POST['body'],
                'status'=>$_POST['status']
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_travel_alerts_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['subject']. "</a> has been added to the <a href='".$this->generateUrl('acme_cls_admin_manage_travel_alerts')."' target='_blank'>list of travel alerts</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['subject'] . ' has been added.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_travel_alerts'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageTravelAlerts:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSadminBundle:ManageTravelAlerts:new.html.twig',
                array(
                    )
                );
        }
    }
    
    
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-travel-alerts');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['alert_date'] = filter_var($_POST['alert_date'], FILTER_SANITIZE_STRING);
            $_POST['subject'] = filter_var($_POST['subject'], FILTER_SANITIZE_STRING);
            $_POST['body'] = filter_var($_POST['body'], FILTER_SANITIZE_STRING);
            $_POST['status'] = filter_var($_POST['status'], FILTER_SANITIZE_STRING);
            
            $_POST['id'] = intval($_POST['id']);
            
            $model = $em->getRepository('AcmeCLSadminBundle:TravelAlerts')->findOneBy(array('id'=>$_POST["id"]));
            $model->setAlertDate($mod->changeFormatToOriginal($_POST['alert_date']));
            $model->setBody($_POST['body']);
            $model->setSubject($_POST['subject']);
            $model->setStatus($_POST['status']);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'alert_date'=>$_POST['alert_date'],
                'subject'=>$_POST['subject'],
                'body'=>$_POST['body'],
                'status'=>$_POST['status']
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_travel_alerts_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['subject']. "</a>, a travel alert, its details have been updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['subject'] . ' has been updated.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_travel_alerts'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageTravelAlerts:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:TravelAlerts');
            $query = $cust->createQueryBuilder('d')
                ->select("d.alert_date, d.subject, d.body, d.status, d.id")
                ->getQuery();
            $post = $query->getArrayResult();
            
            return $this->render('AcmeCLSadminBundle:ManageTravelAlerts:edit.html.twig',
                array(
                    'post'=> (count($post) > 0) ? $post[0] : array()
                    )
                );
        }
    }
    
    
    
    
    public function deleteAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        $_POST['id'] = intval($_POST['id']);
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $model = $em->getRepository('AcmeCLSadminBundle:TravelAlerts')->findOneBy(array('id'=>$_POST['id']));
        
        $statement = $connection->prepare("DELETE FROM tbl_travel_alerts WHERE id=".$_POST['id']);
        $statement->execute();
        
        /* + create log */
        $logDetails = $model->getSubject() . ", a travel alert, has been been deleted and removed from the <a href='".$this->generateUrl('acme_cls_admin_manage_travel_alerts')."' target='_blank'>list of travel alerts</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */
        
        return new Response("success");
        
    }

}
