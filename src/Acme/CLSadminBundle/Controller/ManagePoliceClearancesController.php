<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSadminBundle\Entity\PoliceClearances;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManagePoliceClearancesController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-police-clearances');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:ManagePoliceClearances:index.html.twig');
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:PoliceClearances');
            $query = $cust->createQueryBuilder('c')
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("name","price","status","id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    /**
     * new clearance
     * @return type
     */
    public function newAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-police-clearances');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['name1'] = filter_var($_POST['name1'], FILTER_SANITIZE_STRING);
            $_POST['price1'] = filter_var($_POST['price1'], FILTER_SANITIZE_STRING);
            
            $_POST['name2'] = filter_var($_POST['name2'], FILTER_SANITIZE_STRING);
            $_POST['price2'] = filter_var($_POST['price2'], FILTER_SANITIZE_STRING);
            
            $_POST['price1'] = floatval($_POST['price1']);
            $_POST['price2'] = floatval($_POST['price2']);
            
            $model = new PoliceClearances();
            $model->setName($_POST['name1']);
            $model->setPrice($_POST['price1']);
            $model->setNameAdditional($_POST['name2']);
            $model->setPriceAdditional($_POST['price2']);
            $model->setGenInfo($_POST["genInfo"]);
            
            $sActive = (isset($_POST['sActive'])) ? 1 : 0;
            $model->setStatus($sActive);
            if(trim($_FILES["clearanceFile"]["name"]) != ''){
                $model->setFilePath($_FILES["clearanceFile"]["name"]);
            }
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'name1'=>$_POST['name1'],
                'price1'=>$_POST['price1'],
                'name2'=>$_POST['name2'],
                'price2'=>$_POST['price2'],
                'sActive'=>$sActive
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                
                if(trim($_FILES["clearanceFile"]["name"]) != ''){
                    $mod->createDirectory($root_dir ."/dev/pclearancefiles/", $model->getId());
                    move_uploaded_file($_FILES["clearanceFile"]["tmp_name"], $root_dir ."/dev/pclearancefiles/".$model->getId()."/". $_FILES["clearanceFile"]["name"]);
                }
                
                
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_police_clearances_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['name1']. "</a> has been added to the <a href='".$this->generateUrl('acme_cls_admin_manage_police_clearances')."' target='_blank'>list of visa police clearances</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'New clearance has been added.'
                );
                
                
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_police_clearances'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManagePoliceClearances:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSadminBundle:ManagePoliceClearances:new.html.twig',
                array(
                    )
                );
        }
    }
    
    
    
    
    /**
     * edit clearance
     * @return type
     */
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-police-clearances');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['name1'] = filter_var($_POST['name1'], FILTER_SANITIZE_STRING);
            $_POST['price1'] = filter_var($_POST['price1'], FILTER_SANITIZE_STRING);
            
            $_POST['name2'] = filter_var($_POST['name2'], FILTER_SANITIZE_STRING);
            $_POST['price2'] = filter_var($_POST['price2'], FILTER_SANITIZE_STRING);
            
            $_POST['price1'] = floatval($_POST['price1']);
            $_POST['price2'] = floatval($_POST['price2']);
            
            $model = $em->getRepository('AcmeCLSadminBundle:PoliceClearances')->findOneBy(array('id'=>$_POST['id']));
            $model->setName($_POST['name1']);
            $model->setPrice($_POST['price1']);
            $model->setNameAdditional($_POST['name2']);
            $model->setPriceAdditional($_POST['price2']);
            $model->setGenInfo($_POST["genInfo"]);
            
            $sActive = (isset($_POST['sActive'])) ? 1 : 0;
            $model->setStatus($sActive);
            if(trim($_FILES["clearanceFile"]["name"]) != ''){
                $model->setFilePath($_FILES["clearanceFile"]["name"]);
            }
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'name1'=>$_POST['name1'],
                'price1'=>$_POST['price1'],
                'name2'=>$_POST['name2'],
                'price2'=>$_POST['price2'],
                'genInfo'=>$_POST['genInfo'],
                'sActive'=>$sActive
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                
                if(trim($_FILES["clearanceFile"]["name"]) != ''){
                    $mod->createDirectory($root_dir ."/dev/pclearancefiles/", $model->getId());
                    move_uploaded_file($_FILES["clearanceFile"]["tmp_name"], $root_dir ."/dev/pclearancefiles/".$model->getId()."/". $_FILES["clearanceFile"]["name"]);
                }
                
                
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_police_clearances_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['name1']. "</a>, a police clearance, its details have been updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['name1'] . ' has been updated.'
                );
                
                
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_police_clearances'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManagePoliceClearances:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            $em = $this->getDoctrine()->getManager();
            $model = $em->getRepository('AcmeCLSadminBundle:PoliceClearances')->findOneBy(array('id'=>$_GET['id']));
            
            return $this->render('AcmeCLSadminBundle:ManagePoliceClearances:edit.html.twig',
                array('post'=> $model)
                );
        }
    }
    
    
    
    public function deleteAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $model = $em->getRepository('AcmeCLSadminBundle:PoliceClearances')->findOneBy(array('id'=>$_POST['id']));
        
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        $mod->deleteDir($root_dir."/dev/pclearancefiles/".$_POST['id']);
        
        $statement = $connection->prepare("DELETE FROM tbl_police_clearances WHERE id=".$_POST['id']);
        $statement->execute();
        
        
        /* + create log */
        
        
        $logDetails = $model->getName() . ", a police clearance, has been been deleted and removed from the <a href='".$this->generateUrl('acme_cls_admin_manage_police_clearances')."' target='_blank'>list of police clearances</a> by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
        $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
        /* - create log */
        
        return new Response("success");
        
    }

}
