<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\Departments;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageDepartmentsController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-departments');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSadminBundle:ManageDepartments:index.html.twig');
    }
    
    public function listAction()
    {
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('admin_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $cust = $this->getDoctrine()->getRepository('AcmeCLSclientGovBundle:Departments');
            $query = $cust->createQueryBuilder('d')
                ->select("d.name, d.code, d.id")
                ->getQuery();
            $results = $query->getArrayResult();
        
            $total_pages= count($results);
            $fieldNames = array("name","code","id");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    /**
     * new department
     * @return type
     */
    public function newAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-departments');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['code'] = filter_var($_POST['code'], FILTER_SANITIZE_STRING);
            $_POST['name'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
            
            $model = new Departments();
            $model->setCode($_POST['code']);
            $model->setName($_POST['name']);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'code'=>$_POST['code'],
                'name'=>$_POST['name']
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_departments_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['name'] . "</a> has been to list of departments by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['name'] . ' has been added.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_departments'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageDepartments:new.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            
            return $this->render('AcmeCLSadminBundle:ManageDepartments:new.html.twig',
                array(
                    )
                );
        }
    }
    
    
    /**
     * edit department
     * @return type
     */
    public function editAction(){
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-departments');
        
        
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
            $_POST['code'] = filter_var($_POST['code'], FILTER_SANITIZE_STRING);
            $_POST['name'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Departments')->findOneBy(array('id'=>$_POST["id"]));
            $model->setCode($_POST['code']);
            $model->setName($_POST['name']);
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'id'=>$_POST['id'],
                'code'=>$_POST['code'],
                'name'=>$_POST['name']
            );
            
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_departments_edit') ."?id=". $model->getId() ."' target='_blank'>". $_POST['name'] . "</a>, a department, its details have been updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['name'] . ' has been updated.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_departments'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageDepartments:edit.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
        }else{
            $em = $this->getDoctrine()->getManager(); 
            
            $_GET['id'] = filter_var($_GET['id'], FILTER_SANITIZE_STRING);

            $model = $em->getRepository('AcmeCLSclientGovBundle:Departments')->findOneBy(array('id'=>$_GET["id"]));
            
            return $this->render('AcmeCLSadminBundle:ManageDepartments:edit.html.twig',
                array(
                    'post'=>$model
                    )
                );
        }
    }
    
    
    public function deleteAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('admin_email') == ''){
            return new Response("session expired");
        }
        
        if(!is_numeric($_POST['id'])){
            return new Response("invalid id");
        }
        
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        
        $statement = $connection->prepare("DELETE FROM tbl_departments WHERE id=".$_POST['id']);
        $statement->execute();
        
        
        return new Response("success");
        
    }

}
