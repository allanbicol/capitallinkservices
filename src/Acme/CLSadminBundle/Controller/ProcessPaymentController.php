<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSadminBundle\Entity\ManualPayment;
use Acme\CLSclientGovBundle\Entity\Payment;

class ProcessPaymentController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'process-payment');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        if(isset($_POST['hidSubmit'])){
            $em = $this->getDoctrine()->getManager();
            
            $em->getConnection()->beginTransaction();
            
            $_POST["fname"] = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
            $_POST["lname"] = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
            $_POST["email"] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST["phone"] = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
            $_POST["mobile"] = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
            $_POST["address"] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);
            $_POST["city"] = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
            $_POST["state"] = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
            $_POST["postcode"] = filter_var($_POST['postcode'], FILTER_SANITIZE_STRING);
            $_POST["country"] = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
            $_POST["additionalAddressDetails"] = filter_var($_POST['additionalAddressDetails'], FILTER_SANITIZE_STRING);
            $_POST["billingAddress"] = filter_var($_POST['billingAddress'], FILTER_SANITIZE_STRING);
            $_POST["billingCity"] = filter_var($_POST['billingCity'], FILTER_SANITIZE_STRING);
            $_POST["billingState"] = filter_var($_POST['billingState'], FILTER_SANITIZE_STRING);
            $_POST["billingPostcode"] = filter_var($_POST['billingPostcode'], FILTER_SANITIZE_STRING);
            $_POST["billingCountry"] = filter_var($_POST['billingCountry'], FILTER_SANITIZE_STRING);
            
            $_POST["nameOnCard"] = filter_var($_POST['nameOnCard'], FILTER_SANITIZE_STRING);
            $_POST["cardNumber"] = filter_var($_POST['cardNumber'], FILTER_SANITIZE_STRING);
            $_POST["cardExpiryMonth"] = filter_var($_POST['cardExpiryMonth'], FILTER_SANITIZE_STRING);
            $_POST["cardExpiryYear"] = filter_var($_POST['cardExpiryYear'], FILTER_SANITIZE_STRING);
            $_POST["ccvNumber"] = filter_var($_POST['ccvNumber'], FILTER_SANITIZE_STRING);
            
            
            $items = array();
            for($i=0; $i<count($_POST['description']); $i++){
                if(trim($_POST['description'][$i]) != ""){
                    $items[] = array(
                        'description'=> $_POST['description'][$i],
                        'price'=> $_POST['price'][$i],
                        'quantity'=> $_POST['quantity'][$i],
                        'gst'=> $_POST['gst'][$i]
                    );
                }
            }
            
            $post = array(
                'order_no'=>$_POST['order_no'],
                'items'=> $items,
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails'],
                'company'=>$_POST['company'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry'],
                
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            $paymentDetails = array(
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile'],
                'address'=>$_POST['address'],
                'city'=>$_POST['city'],
                'state'=>$_POST['state'],
                'postcode'=>$_POST['postcode'],
                'country'=>$_POST['country'],
                'additionalAddressDetails'=>$_POST['additionalAddressDetails']
            );
            
            $billingDetails = array(
                'company'=>$_POST['company'],
                'billingAddress'=>$_POST['billingAddress'],
                'billingCity'=>$_POST['billingCity'],
                'billingState'=>$_POST['billingState'],
                'billingPostcode'=>$_POST['billingPostcode'],
                'billingCountry'=>$_POST['billingCountry']
            );
            $cardDetails = array(
                'nameOnCard' => $_POST['nameOnCard'],
                'cardNumber' => $_POST['cardNumber'],
                'cardExpiryMonth' => $_POST['cardExpiryMonth'],
                'cardExpiryYear' => $_POST['cardExpiryYear'],
                'ccvNumber' => $_POST['ccvNumber']
            );
            
            $error_count = 0;
            if(isset($_POST['client_id'])){
                $_POST['client_id'] = intval($_POST['client_id']);
                $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$_POST['client_id']));

                if($error_count == 0){
                    if(($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1) || ($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                        $errors = array();
                        $special_error=1;
                    }

                    if($_POST["paymentType"] == 0 && $user->getCanChargeCostToAccount() != 1){
                        $errors[] = array('message'=>'You are not allowed to Pay on Account!');
                        $error_count += 1;
                    }else if(($_POST["paymentType"] == 0 && ($user->getAccountNo() != $_POST["accountNumber"]))){
                        $errors[] = array('message'=>'Account number is not correct!');
                        $error_count += 1;
                    }
                }
            }
            
            
            $mp = new ManualPayment();
            $mp->setOrderNo($_POST['order_no']);
            $mp->setCustName($_POST['fname'] . " " . $_POST['lname']);
            $mp->setCustEmail($_POST['email']);
            $mp->setPaymentDetails(json_encode($paymentDetails));
            $mp->setBillingDetails(json_encode($billingDetails));
            $mp->setCardDetails(json_encode($cardDetails));
            $mp->setItems(json_encode($items));
            $mp->setGrandTotal($_POST['grand_total']);
            $em->persist($mp);
            $em->flush();
            
            if($this->sOrderNoExists($_POST['order_no'])){
                $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["order_no"]));
                
                $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST["order_no"]));
                if(count($model) > 0){
                    $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST["order_no"]));
                }else{
                    $model = new Payment();
                    $model->setOrderNo($_POST["order_no"]);
                    $model->setClientId($order->getClientId());
                }
                $model->setDatePaid($datetime->format("Y-m-d H:i:s"));
                $model->setFname($_POST["fname"]);
                $model->setLname($_POST["lname"]);
                $model->setEmail($_POST["email"]);
                $model->setPhone($_POST["phone"]);
                $model->setMobile($_POST["mobile"]);
                $model->setAddress($_POST["address"]);
                $model->setCity($_POST["city"]);
                $model->setState($_POST["state"]);
                $model->setPostcode($_POST["postcode"]);
                $model->setCountryId($_POST["country"]);
                $model->setAdditionalAddressDetails($_POST["additionalAddressDetails"]);
                $model->setMbaAddress($_POST["billingAddress"]);
                $model->setMbaCity($_POST["billingCity"]);
                $model->setMbaState($_POST["billingState"]);
                $model->setMbaPostcode($_POST["billingPostcode"]);
                $model->setMbaCountryId($_POST["billingCountry"]);
                if($_POST["paymentType"] == 1){
                    $model->setPaymentOption(1);
                }else{
                    $model->setPaymentOption(0);
                }
                $model->setTotalOrderPrice($_POST['grand_total']);
                if($_POST["paymentType"] == 1){
                    $model->setSPaid(1);
                }else{
                    $model->setSPaid(2);
                }
                $em->persist($model);
                $em->flush();
            }
            
            if($error_count == 0){
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                    $totalAmount = $_POST['grand_total'] * 100;

                    $fields = array(
                        'customerFirstName' => urlencode($_POST['fname']),
                        'customerLastName' => urlencode($_POST['lname']),
                        'customerEmail' => urlencode($_POST['email']),
                        'customerAddress' => urlencode($_POST['address'] . ' ' . $_POST['city'] . ' ' . $_POST['state'] . ' ' . $this->getCountryNameById($_POST["country"])),
                        'customerPostcode' => urlencode($_POST['postcode']),
                        'customerInvoiceDescription' => urlencode('CLS Manual Payment'),
                        'customerInvoiceRef' => urlencode($_POST['order_no']),
                        'cardHoldersName' => urlencode($_POST['nameOnCard']),
                        'cardNumber' => urlencode($_POST['cardNumber']),
                        'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                        'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                        'trxnNumber' => urlencode('4230'),
                        'totalAmount' => urlencode($totalAmount),
                        'cvn' => urlencode($_POST['ccvNumber'])
                    );



                    //url-ify the data for the POST
                    $fields_string = '';
                    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                    rtrim($fields_string, '&');


                   $root_dir = dirname($this->get('kernel')->getRootDir());
                    //open connection
                    $ch = curl_init();
                    //set the url, number of POST vars, POST data
                    curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                    curl_setopt($ch,CURLOPT_POST, count($fields));
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                    curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");
                    //execute post
                    $credit_card_reponse = curl_exec($ch);

                    $cr_result = json_decode($credit_card_reponse, true);


                    if(isset($cr_result['error'])){
                        $credit_card_error += 1;
                    }


                    //close connection
                    curl_close($ch);
                }
                /**
                * Credit card payment
                * Position: End
                */
                
                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            if($error_count > 0){
                // roll back changes
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();

                if($session->get('user_type') == 'admin-user'){
                    $user_details = $this->getClientDetailsById($_POST['client_id']);
                }else{
                    $user_details = '';
                }
                
                
                $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["order_no"], $_POST['client_id']);
                $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
                if(!isset($special_error)){
                    $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );
                            
                    
                    return $this->render('AcmeCLSadminBundle:ProcessPayment:index.html.twig',
                        array(
                            'client_id'=> isset($_POST['client_id']) ? $_POST['client_id'] : '',
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                            'user_details'=>$user_details,
                            'post'=> $post,
                            'data'=> $data,
                            'tpn_settings' => $tpn_settings
                            )
                    );
                }else{
                    
                    return $this->render('AcmeCLSadminBundle:ProcessPayment:index.html.twig',
                        array(
                            'client_id'=> isset($_POST['client_id']) ? $_POST['client_id'] : '',
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                            'user_details'=>$user_details,
                            'post'=> $post,
                            'data'=> $data,
                            'tpn_settings' => $tpn_settings,
                            'errors'=>$errors,
                            )
                    );
                    
                }


                
            }else{
                
                if($order->getStatus() <= 10){
                    $order->setStatus(10); // 10= Order placed
                }
                $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
                $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
                $order->setGrandTotal($_POST['grand_total']);
                
                if($session->get('user_type') == 'admin-user'){
                    $order->setVisaClsTeamMember($session->get('admin_id'));
                }
                $em->persist($order);
                $em->flush();
                
                
                // email invoice
                $email_copies = array('admin@capitallinkservices.com.au','naim@capitallinkservices.com.au', 'karen@capitallinkservices.com.au');
                $recipients = array(
                  'to'=> $_POST['email'],
                  'cc'=> $email_copies
                );
                
                if($this->sOrderNoExists($_POST['order_no']) && isset($_POST['client_id'])){
                    
                    $data = $this->getOrderDetailsByOrderNoAndClientId($_POST["order_no"], $_POST['client_id']);
                }
                
                $this->sendEmailWithCc($recipients, "help@capitallinkservices.com.au", "Payment Receipt", 
                    $this->renderView('AcmeCLSadminBundle:ProcessPayment:invoice_template.html.twig',
                            array('domain'=>$mod->siteURL(),
                                'date_submitted'=>$datetime->format("Y-m-d H:i:s"),
                                'post'=>$post,
                                'payment_type'=> $_POST["paymentType"],
                                'data'=> (count($data) > 0) ? $data : array(),
                                'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                                )
                            )
                    );
                
                /* + create log */
                $payment_type = '';
                if($_POST['paymentType'] == 0){
                    $payment_type = "via account (no. ".$_POST["accountNumber"].")";
                }elseif($_POST['paymentType'] == 1){
                    $payment_type = "via credit card";
                }else{
                    $payment_type = "and marked as paid";
                }
                $worderno = '';
                if(isset($_POST['order_no']) && trim($_POST['order_no']) != ''){
                    $worderno = "- order number " . trim($_POST['order_no']);
                }
                $logDetails = "<a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a> processed payment ". $payment_type . " for " . $_POST["fname"] . " " . $_POST["lname"] ." amounting AUD $" . $_POST["grand_total"] . " " .$worderno;
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Payment has been processed.'
                        );

                // commit changes
                $em->getConnection()->commit();
                
                return $this->redirect($this->generateUrl('acme_cls_admin_process_payment'));
                
                    
            }
        }else{
            if(!isset($_GET["order"])){
                return $this->render('AcmeCLSadminBundle:ProcessPayment:index.html.twig',
                        array(
                            'countries'=> $this->getCountries(),
                            'departments'=> $this->getDepartments(),
                            'cardtypes'=> $this->getCardTypes(),
                            'credit_card_processing_fee' => $this->getCreditCardProcessingFee()
                        )
                    );
            }
            
            $_GET["order"] = intval($_GET["order"]);
            $post='';
            $items = $this->getManualPayment($_GET["order"]);
            if(count($items)>0){
                $post = array('order_no'=>$items[0]['order_no'],'items'=>json_decode($items[0]['items'],true));
            }
            
            if(isset($_GET['client'])){
                $_GET['client'] = intval($_GET['client']);
                $client_id = $_GET['client'];
                $user_details = $this->getClientDetailsById($_GET['client']);
            }else{
                $costumer = $this->getOrderDetailsByOrderNo($_GET["order"]);
                $client_id = (isset($costumer['client_id'])) ? $costumer['client_id'] : '';
                $user_details = '';
            }
            
            $em = $this->getDoctrine()->getManager();
            
            $data = $this->getOrderDetailsByOrderNoAndClientId($_GET["order"], $client_id);
            $user = $em->getRepository('AcmeCLSclientGovBundle:User')->findOneBy(array('id'=>$client_id));
            $tpn_settings = $em->getRepository('AcmeCLSclientGovBundle:SettingsTPN')->findOneBy(array('id'=>1));
            if(count($data) > 0){
                // if order status done with the review order page
                // if not, send to review order page
                if( $data["status"] >= 2 ){

                    if($post!=''){
                        return $this->render('AcmeCLSadminBundle:ProcessPayment:index.html.twig',
                            array(
                                'order_no'=> $_GET["order"],
                                'client_id'=> $client_id,
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'departments'=> $this->getDepartments(),
                                'cardtypes'=> $this->getCardTypes(),
                                'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                                'user_details'=>$user_details,
                                'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                            'special_price'=> $user->getSpecialPrice()
                                        ),
                                'tpn_settings' => $tpn_settings,
                                'post'=>$post
                            )
                        );
                    }else{
                        return $this->render('AcmeCLSadminBundle:ProcessPayment:index.html.twig',
                            array(
                                'order_no'=> $_GET["order"],
                                'client_id'=> $client_id,
                                'data'=> $data,
                                'countries'=> $this->getCountries(),
                                'departments'=> $this->getDepartments(),
                                'cardtypes'=> $this->getCardTypes(),
                                'credit_card_processing_fee' => $this->getCreditCardProcessingFee(),
                                'user_details'=>$user_details,
                                'user'=>array('can_get_special_price'=>$user->getCanGetSpecialPrice(),
                                            'special_price'=> $user->getSpecialPrice()
                                        ),
                                'tpn_settings' => $tpn_settings
                            )
                        );
                    }

                }
            }else{
                return $this->render('AcmeCLSadminBundle:ViewOrder:error.html.twig',
                    array(
                        'title'=> 'Not Found',
                        'message'=> 'Order number '.$_GET["order"].' not found!' 
                    ));
            }
            
            
        }
    }
    
    public function getManualPayment($orderNo){
        $cust = $this->getDoctrine()->getRepository('AcmeCLSadminBundle:ManualPayment');
        $query = $cust->createQueryBuilder('p')
            ->select("p.id,p.order_no,p.cust_name,p.cust_email,p.items,p.payment_details,p.billing_details,p.grand_total")
            ->where('p.order_no = :order_no ')
            ->setParameter('order_no', $orderNo)
            ->orderBy("p.id","ASC")
            ->setMaxResults(1)
            ->getQuery();
        $data = $query->getArrayResult();
       
        return $data;
    }

}
