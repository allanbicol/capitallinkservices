<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\CreditCardProcessing;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class ManageCreditCardProcessingFeeController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'admin-manage-credit-card-processing-fee');
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:CreditCardProcessing')->findOneBy(array('id'=>1));
            $_POST['fee'] = floatval($_POST['fee']);
            $model->setFee($_POST['fee']);
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'fee'=>$_POST['fee']
            );
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                /* + create log */
                $logDetails = "<a href='". $this->generateUrl('acme_cls_admin_manage_credit_card_processing_fee') ."' target='_blank'>Credit Card</a> details have been updated by <a href='".$this->generateUrl('acme_cls_admin_staff_edit')."?id=".$session->get('admin_id')."' target='_blank'>" . $session->get('admin_fname') . " " . $session->get('admin_lname') . "</a>";
                $this->createLog('admin', $session->get('admin_id'), 'admin', $logDetails);
                /* - create log */
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Credit Card Processing Fee has been updated!'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_admin_manage_credit_card_processing_fee'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSadminBundle:ManageCreditCardProcessingFee:index.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                        ));
            }
            
        }else{
            $em = $this->getDoctrine()->getManager();
            $model = $em->getRepository('AcmeCLSclientGovBundle:CreditCardProcessing')->findOneBy(array('id'=>1));
            
            return $this->render('AcmeCLSadminBundle:ManageCreditCardProcessingFee:index.html.twig',
                    array('post'=>$model)
                    );
        }
        
    }

}
