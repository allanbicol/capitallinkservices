<?php

namespace Acme\CLSadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSadminBundle\Entity\OrderPassportApplicants;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class EditOrderController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function passportAction()
    {
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'passport-office-pickup-delivery');
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('admin_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            
            // update order
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            $order->setPassportOfficeBookingNo($_POST['passportOfficeBookingNumber']);
            $order->setPassportOfficeBookingTime($_POST['passportOfficeBookingTime']);
            $order->setPassportOfficeBookingTimeHr($_POST['passportOfficeBookingTimeHour']);
            $order->setPassportOfficeBookingTimeMin($_POST['passportOfficeBookingTimeMin']);
            
            $order->setDateSubmitted($datetime->format("Y-m-d H:i:s"));
            $order->setDateLastSaved($datetime->format("Y-m-d H:i:s"));
            
            
            $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            $ccp = $em->getRepository('AcmeCLSclientGovBundle:CreditCardProcessing')->findOneBy(array('id'=>1));
            
            $cost = $passport->getCost();
            
            if(count($_POST['passportApplicantFullname']) > 1){
                $cost += (count($_POST['passportApplicantFullname']) -1) * $passport->getAdditionalCost();
            }
            
            if($_POST["paymentType"] == 1){
                $inc_card_fee = $cost * ($ccp->getFee() / 100);
                $cost = $cost + $inc_card_fee;    
            }
            
            $order->setGrandTotal($cost);
            
            if(isset($_POST['sDocSent'])){
                $order->setSDocSent(1);
                $order->setDateDocSent($_POST['dateDocSent']);
            }else{
                $order->setSDocSent(0);
                $order->setDateDocSent("");
            }
                
            if(isset($_POST['sPassportSentAndCompleted'])){
                $order->setStatus(12); // 10= Order placed
                $order->setDateCompleted($_POST['datePassportSentAndCompleted']);
            }else{
                $order->setStatus(10);
                $order->setDateCompleted(null);
            }
            
            $em->persist($order);
            $em->flush();
            
            $passportApplicants = array();
            
            // add/update
            while ($applicant = current($_POST["passportApplicantFullname"])) {
                $key = key($_POST["passportApplicantFullname"]);
                
                $s_exist = $em->getRepository('AcmeCLSadminBundle:OrderPassportApplicants')->findOneBy(array('id'=> $key, 'order_no'=>$_POST["orderNumber"]));
                
                
                if(count($s_exist) == 0){
                    $order_pa = new OrderPassportApplicants();
                    $order_pa->setOrderNo($order->getOrderNo());
                    
                }else{
                    $order_pa = $em->getRepository('AcmeCLSadminBundle:OrderPassportApplicants')->findOneBy(array('id'=> $key, 'order_no'=>$_POST["orderNumber"]));
                }
                
                $order_pa->setFullname($applicant);
                $personalPassport = (isset($_POST['personalPassport'][$key])) ? 1 : 0;
                $doPassport = (isset($_POST['doPassport'][$key])) ? 1 : 0;
                $birthCertificate = (isset($_POST['birthCertificate'][$key])) ? 1 : 0;
                $marriageCertificate = (isset($_POST['marriageCertificate'][$key])) ? 1 : 0;
                $certOfAustralianCitizenship = (isset($_POST['certOfAustralianCitizenship'][$key])) ? 1 : 0;
                
                $order_pa->setPersonalPassport($personalPassport);
                $order_pa->setDiplomaticOfficialPassport($doPassport);
                $order_pa->setBirthCertificate($birthCertificate);
                $order_pa->setMarriageCertificate($marriageCertificate);
                $order_pa->setCertificateOfAustralianCitizenship($certOfAustralianCitizenship);
                
                
                
                
                $em->persist($order_pa);
                $em->flush();
                
                
                $passportApplicants[] = array(
                    'passportApplicantFullname'=>$applicant,
                    'personalPassport'=>$personalPassport,
                    'doPassport'=>$doPassport,
                    'birthCertificate'=>$birthCertificate,
                    'marriageCertificate'=>$marriageCertificate,
                    'certOfAustralianCitizenship'=>$certOfAustralianCitizenship
                );
                next($_POST['passportApplicantFullname']);
            }
            
            // delete
            if(isset($_POST['remApplicants']) && trim($_POST['remApplicants']) != ''){
                $_POST['remApplicants'] = filter_var($_POST['remApplicants'], FILTER_SANITIZE_STRING);
                
                $remApplicants = explode(";", $_POST['remApplicants']);
                
                for($i=0; $i<count($remApplicants); $i++){
                    if($remApplicants[$i] != ''){
                    $query = $em->createQuery('DELETE AcmeCLSadminBundle:OrderPassportApplicants a WHERE a.order_no = '.$order->getOrderNo().' AND a.id='.$remApplicants[$i]);
                    $query->execute(); 
                    }
                }
            }
            
            $model = $em->getRepository('AcmeCLSclientGovBundle:Payment')->findOneBy(array('order_no'=>$_POST["orderNumber"]));
            $model->setPaymentOption($_POST["paymentType"]);
            $model->setAccountNo($_POST["accountNumber"]);
//            $model->setNameOnCard($_POST["nameOnCard"]);
//            $model->setCardNumber($_POST["cardNumber"]);
//            $model->setCardExpiryMonth($_POST["cardExpiryMonth"]);
//            $model->setCardExpiryYear($_POST["cardExpiryYear"]);
//            //$model->setCardType($_POST["cardType"]);
//            $model->setCcvNumber($_POST["ccvNumber"]);
            $model->setTotalOrderPrice($cost);
            
            $model->setSPaid(1);
            $em->persist($model);
            $em->flush();

            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'passportOfficeBookingNumber'=> $_POST['passportOfficeBookingNumber'],
                'passportOfficeBookingTime'=> $_POST['passportOfficeBookingTime'],
                'passportOfficeBookingTimeHour'=> $_POST['passportOfficeBookingTimeHour'],
                'passportOfficeBookingTimeMin'=> $_POST['passportOfficeBookingTimeMin'],
                'passportApplicants'=>$passportApplicants,
            );
            
            if($error_count == 0){
                /**
                * Credit card payment
                * Position: Start
                */
               $credit_card_error = 0;
               if($_POST["paymentType"] == 1){
                   $user_details = $this->getClientDetailsById($order->getClientId());
                   $totalAmount = $cost * 100;
                   $fields = array(
                       'customerFirstName' => urlencode($user_details['fname']),
                       'customerLastName' => urlencode($user_details['lname']),
                       'customerEmail' => urlencode($user_details['email']),
                       'customerAddress' => urlencode(''),
                       'customerPostcode' => urlencode(''),
                       'customerInvoiceDescription' => urlencode('CLS Passport Office Pickup or Delivery'),
                       'customerInvoiceRef' => urlencode($order->getOrderNo()),
                       'cardHoldersName' => urlencode($_POST['nameOnCard']),
                       'cardNumber' => urlencode($_POST['cardNumber']),
                       'cardExpiryMonth' => urlencode($_POST['cardExpiryMonth']),
                       'cardExpiryYear' => urlencode($_POST['cardExpiryYear']),
                       'trxnNumber' => urlencode('4230'),
                       'totalAmount' => urlencode($totalAmount),
                       'cvn' => urlencode($_POST['ccvNumber'])
                   );



                   //url-ify the data for the POST
                   $fields_string = '';
                   foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                   rtrim($fields_string, '&');

                   $root_dir = dirname($this->get('kernel')->getRootDir());

                   //open connection
                   $ch = curl_init();
                   //set the url, number of POST vars, POST data
                   curl_setopt($ch,CURLOPT_URL, 'https://www.capitallinkservices.com.au/eway/post-pay.php');
                   curl_setopt($ch,CURLOPT_POST, count($fields));
                   curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                   curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
                   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                   curl_setopt($ch, CURLOPT_CAINFO, $root_dir. "/eway/cert/GeoTrustPrimaryCertificationAuthority.crt");

                   //execute post
                   $credit_card_reponse = curl_exec($ch);

                   $cr_result = json_decode($credit_card_reponse, true);


                   if(isset($cr_result['error'])){
                       $credit_card_error += 1;
                   }


                   //close connection
                   curl_close($ch);
               }
               /**
                * Credit card payment
                * Position: End
                */
                
                if($credit_card_error > 0){
                    $errors = array();
                    $special_error=1;
                    
                    $errors[] = array('message'=>$cr_result['error']);
                    $error_count += 1;
                }
            }
            
            if($error_count > 0){
                // roll back changes
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors
                    );
                
                $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            
                return $this->render('AcmeCLSadminBundle:EditOrder:passport.html.twig',
                    array(
                        'user'=>$this->getClientDetailsById($order->getClientId()),
                        'credit_card_processing_fee'=>$this->getCreditCardProcessingFee(),
                        'countries'=> $this->getCountries(),
                        'name_titles'=> $this->getNameTitles(),
                        'passport_types'=>$this->getPassportTypes(),
                        'cardtypes'=> $this->getCardTypes(),
                        'departments'=>$this->getDepartments(),
                        'cost'=>$passport,
                        'post'=>$post,
                        'payment'=> $this->getPaymentDetailsByOrderNo($_POST["orderNumber"])
                    ));
            
                
            }else{
                
                
                // commit changes
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Order has been updated'
                        );
                return $this->redirect($this->generateUrl('acme_cls_admin_edit_passport_order') . '?order_no='.$_POST["orderNumber"]);
                
            }
            
        }else{
            $em = $this->getDoctrine()->getManager();
            $passport = $em->getRepository('AcmeCLSadminBundle:SettingsPassport')->findOneBy(array('id'=>1));
            
            $_GET['order_no'] = filter_var($_GET['order_no'], FILTER_SANITIZE_STRING);
            
            $order = $em->getRepository('AcmeCLSclientGovBundle:Orders')->findOneBy(array('order_no'=>$_GET["order_no"]));
            
            return $this->render('AcmeCLSadminBundle:EditOrder:passport.html.twig',
                    array(
                        'user'=>$this->getClientDetailsById($order->getClientId()),
                        'credit_card_processing_fee'=>$this->getCreditCardProcessingFee(),
                        'cardtypes'=> $this->getCardTypes(),
                        'cost'=>$passport,
                        'post'=>$this->getOrderDetailsByOrderNo($_GET["order_no"]),
                        'payment'=> $this->getPaymentDetailsByOrderNo($_GET["order_no"])
                    ));
        }
    }

}
