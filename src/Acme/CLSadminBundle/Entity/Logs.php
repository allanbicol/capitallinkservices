<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logs
 *
 * @ORM\Table(name="tbl_logs")
 * @ORM\Entity
 */
class Logs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="log_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $log_id;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=10)
     */
    private $area;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $user_id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string")
     */
    private $user_type;

    /**
     * @var string
     *
     * @ORM\Column(name="log_datetime", type="string", length=50)
     */
    private $log_datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="log_details", type="string", length=255)
     */
    private $log_details;



    /**
     * Get log_id
     *
     * @return integer 
     */
    public function getLogId()
    {
        return $this->log_id;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return Logs
     */
    public function setArea($area)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return string 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return Logs
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
    
        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set log_datetime
     *
     * @param string $logDatetime
     * @return Logs
     */
    public function setLogDatetime($logDatetime)
    {
        $this->log_datetime = $logDatetime;
    
        return $this;
    }

    /**
     * Get log_datetime
     *
     * @return string 
     */
    public function getLogDatetime()
    {
        return $this->log_datetime;
    }

    /**
     * Set log_details
     *
     * @param string $logDetails
     * @return Logs
     */
    public function setLogDetails($logDetails)
    {
        $this->log_details = $logDetails;
    
        return $this;
    }

    /**
     * Get log_details
     *
     * @return string 
     */
    public function getLogDetails()
    {
        return $this->log_details;
    }

    /**
     * Set user_type
     *
     * @param string $userType
     * @return Logs
     */
    public function setUserType($userType)
    {
        $this->user_type = $userType;
    
        return $this;
    }

    /**
     * Get user_type
     *
     * @return string 
     */
    public function getUserType()
    {
        return $this->user_type;
    }
}