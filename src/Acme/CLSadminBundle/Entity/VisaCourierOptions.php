<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * VisaCourierOptions
 *
 * @ORM\Table(name="tbl_visa_courier_options")
 * @ORM\Entity
 */
class VisaCourierOptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Courier type should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Courier type must be at least {{ limit }} characters length",
     *      maxMessage = "Courier type cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="type", type="string", length=225)
     */
    private $type;

    /**
     * @var float
     * @Assert\NotBlank(message="Courier cost should not be blank.")
     * @Assert\Type(type="numeric", message="Courier cost should be of type numeric.")
     * @ORM\Column(name="cost", type="float")
     */
    private $cost;

    /**
     * @var integer
     * 
     * @ORM\Column(name="s_active", type="integer")
     */
    private $sActive;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="s_available_for_gov", type="integer")
     */
    private $s_available_for_gov;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="s_available_for_public", type="integer")
     */
    private $s_available_for_public;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="s_dhl", type="integer")
     */
    private $s_dhl;


    

    




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return VisaCourierOptions
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return VisaCourierOptions
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set sActive
     *
     * @param integer $sActive
     * @return VisaCourierOptions
     */
    public function setSActive($sActive)
    {
        $this->sActive = $sActive;
    
        return $this;
    }

    /**
     * Get sActive
     *
     * @return integer 
     */
    public function getSActive()
    {
        return $this->sActive;
    }

    /**
     * Set s_available_for_gov
     *
     * @param integer $sAvailableForGov
     * @return VisaCourierOptions
     */
    public function setSAvailableForGov($sAvailableForGov)
    {
        $this->s_available_for_gov = $sAvailableForGov;
    
        return $this;
    }

    /**
     * Get s_available_for_gov
     *
     * @return integer 
     */
    public function getSAvailableForGov()
    {
        return $this->s_available_for_gov;
    }

    /**
     * Set s_dhl
     *
     * @param integer $sDhl
     * @return VisaCourierOptions
     */
    public function setSDhl($sDhl)
    {
        $this->s_dhl = $sDhl;
    
        return $this;
    }

    /**
     * Get s_dhl
     *
     * @return integer 
     */
    public function getSDhl()
    {
        return $this->s_dhl;
    }

    /**
     * Set s_available_for_public
     *
     * @param integer $sAvailableForPublic
     * @return VisaCourierOptions
     */
    public function setSAvailableForPublic($sAvailableForPublic)
    {
        $this->s_available_for_public = $sAvailableForPublic;
    
        return $this;
    }

    /**
     * Get s_available_for_public
     *
     * @return integer 
     */
    public function getSAvailableForPublic()
    {
        return $this->s_available_for_public;
    }
}