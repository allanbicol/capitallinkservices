<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * VisaTypes
 *
 * @ORM\Table(name="tbl_visa_types")
 * @ORM\Entity
 */
class VisaTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * 
     * @ORM\Column(name="country_id", type="integer")
     */
    private $country_id;

    /**
     * @var string
     * @Assert\NotBlank(message="Visa Type should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Visa type must be at least {{ limit }} characters length",
     *      maxMessage = "Visa type cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;

    /**
     * @var float
     * @Assert\NotBlank(message="Visa Type cost should not be blank.")
     * @ORM\Column(name="cost", type="float")
     */
    private $cost;

    /**
     * @var string
     *
     * @ORM\Column(name="file_attachment", type="string", length=1000)
     */
    private $file_attachment;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return VisaTypes
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;
    
        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return VisaTypes
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return VisaTypes
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set file_attachment
     *
     * @param string $fileAttachment
     * @return VisaTypes
     */
    public function setFileAttachment($fileAttachment)
    {
        $this->file_attachment = $fileAttachment;
    
        return $this;
    }

    /**
     * Get file_attachment
     *
     * @return string 
     */
    public function getFileAttachment()
    {
        return $this->file_attachment;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return VisaTypes
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}