<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TravelAlerts
 *
 * @ORM\Table(name="tbl_travel_alerts")
 * @ORM\Entity
 */
class TravelAlerts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alert_date", type="string", length=20)
     */
    private $alert_date;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=1000)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string")
     */
    private $body;

    /**
     * @var integer
     *
     * @ORM\Column(name="admin_id", type="integer")
     */
    private $admin_id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alert_date
     *
     * @param string $alertDate
     * @return TravelAlerts
     */
    public function setAlertDate($alertDate)
    {
        $this->alert_date = $alertDate;
    
        return $this;
    }

    /**
     * Get alert_date
     *
     * @return string 
     */
    public function getAlertDate()
    {
        return $this->alert_date;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return TravelAlerts
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return TravelAlerts
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set admin_id
     *
     * @param integer $adminId
     * @return TravelAlerts
     */
    public function setAdminId($adminId)
    {
        $this->admin_id = $adminId;
    
        return $this;
    }

    /**
     * Get admin_id
     *
     * @return integer 
     */
    public function getAdminId()
    {
        return $this->admin_id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TravelAlerts
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}