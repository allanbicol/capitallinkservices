<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AdminUser
 *
 * @ORM\Table(name="tbl_user_admin")
 * @ORM\Entity
 * @UniqueEntity(
 *      fields={"email"},
 *      message="Email is already used"
 * )
 */
class AdminUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Firstname should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Firstname must be at least {{ limit }} characters length",
     *      maxMessage = "Firstname cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="fname", type="string", length=255)
     */
    private $fname;

    /**
     * @var string
     * @Assert\NotBlank(message="Lastname should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Lastname must be at least {{ limit }} characters length",
     *      maxMessage = "Lastname cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="lname", type="string", length=255)
     */
    private $lname;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Email should not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "225",
     *      minMessage = "Email must be at least {{ limit }} characters length",
     *      maxMessage = "Email cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message="Password should not be blank.")
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="reset_pin", type="string", length=10)
     */
    private $reset_pin;

    /**
     * @var string
     *
     * @ORM\Column(name="last_login", type="string", length=20)
     */
    private $last_login;
    /**
     * @var integer
     *
     * @ORM\Column(name="s_enabled", type="integer")
     */
    private $s_enabled;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_driver", type="integer")
     */
    private $s_driver;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return AdminUser
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return AdminUser
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return AdminUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return AdminUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set reset_pin
     *
     * @param string $resetPin
     * @return AdminUser
     */
    public function setResetPin($resetPin)
    {
        $this->reset_pin = $resetPin;
    
        return $this;
    }

    /**
     * Get reset_pin
     *
     * @return string 
     */
    public function getResetPin()
    {
        return $this->reset_pin;
    }

    /**
     * Set last_login
     *
     * @param string $lastLogin
     * @return AdminUser
     */
    public function setLastLogin($lastLogin)
    {
        $this->last_login = $lastLogin;
    
        return $this;
    }

    /**
     * Get last_login
     *
     * @return string 
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * Set s_enabled
     *
     * @param integer $sEnabled
     * @return AdminUser
     */
    public function setSEnabled($sEnabled)
    {
        $this->s_enabled = $sEnabled;
    
        return $this;
    }

    /**
     * Get s_enabled
     *
     * @return integer 
     */
    public function getSEnabled()
    {
        return $this->s_enabled;
    }

    /**
     * Set s_driver
     *
     * @param integer $sDriver
     * @return AdminUser
     */
    public function setSDriver($sDriver)
    {
        $this->s_driver = $sDriver;
    
        return $this;
    }

    /**
     * Get s_driver
     *
     * @return integer 
     */
    public function getSDriver()
    {
        return $this->s_driver;
    }
}