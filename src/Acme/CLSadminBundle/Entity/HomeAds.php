<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HomeAds
 *
 * @ORM\Table(name="tbl_home_ads")
 * @ORM\Entity
 */
class HomeAds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=1000)
     */
    private $image;
    
    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=1000)
     */
    private $link;

    /**
     * @var integer
     *
     * @ORM\Column(name="s_enabled", type="integer")
     */
    private $s_enabled;


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return ImageSlider
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set s_enabled
     *
     * @param integer $sEnabled
     * @return ImageSlider
     */
    public function setSEnabled($sEnabled)
    {
        $this->s_enabled = $sEnabled;
    
        return $this;
    }

    /**
     * Get s_enabled
     *
     * @return integer 
     */
    public function getSEnabled()
    {
        return $this->s_enabled;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return HomeAds
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }
}