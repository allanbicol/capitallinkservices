<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImageSlider
 *
 * @ORM\Table(name="tbl_home_image_slider")
 * @ORM\Entity
 */
class ImageSlider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=1000)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="front_html", type="string")
     */
    private $front_html;

    /**
     * @var integer
     *
     * @ORM\Column(name="s_enabled", type="integer")
     */
    private $s_enabled;


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return ImageSlider
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set front_html
     *
     * @param string $frontHtml
     * @return ImageSlider
     */
    public function setFrontHtml($frontHtml)
    {
        $this->front_html = $frontHtml;
    
        return $this;
    }

    /**
     * Get front_html
     *
     * @return string 
     */
    public function getFrontHtml()
    {
        return $this->front_html;
    }

    /**
     * Set s_enabled
     *
     * @param integer $sEnabled
     * @return ImageSlider
     */
    public function setSEnabled($sEnabled)
    {
        $this->s_enabled = $sEnabled;
    
        return $this;
    }

    /**
     * Get s_enabled
     *
     * @return integer 
     */
    public function getSEnabled()
    {
        return $this->s_enabled;
    }
}