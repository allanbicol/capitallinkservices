<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PublicVisaAdditionalRequirements
 *
 * @ORM\Table(name="tbl_public_visa_additional_requirements")
 * @ORM\Entity
 */
class PublicVisaAdditionalRequirements
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="visa_id", type="integer")
     */
    private $visa_id;

    /**
     * @var string
     *
     * @ORM\Column(name="requirement", type="string", length=225)
     */
    private $requirement;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float")
     */
    private $cost;

    /**
     * @var integer
     *
     * @ORM\Column(name="s_required", type="integer")
     */
    private $s_required;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="item_order", type="integer")
     */
    private $item_order;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visa_id
     *
     * @param integer $visaId
     * @return VisaAdditionalRequirements
     */
    public function setVisaId($visaId)
    {
        $this->visa_id = $visaId;
    
        return $this;
    }

    /**
     * Get visa_id
     *
     * @return integer 
     */
    public function getVisaId()
    {
        return $this->visa_id;
    }

    /**
     * Set requirement
     *
     * @param string $requirement
     * @return VisaAdditionalRequirements
     */
    public function setRequirement($requirement)
    {
        $this->requirement = $requirement;
    
        return $this;
    }

    /**
     * Get requirement
     *
     * @return string 
     */
    public function getRequirement()
    {
        return $this->requirement;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return VisaAdditionalRequirements
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    
        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set s_required
     *
     * @param integer $sRequired
     * @return VisaAdditionalRequirements
     */
    public function setSRequired($sRequired)
    {
        $this->s_required = $sRequired;
    
        return $this;
    }

    /**
     * Get s_required
     *
     * @return integer 
     */
    public function getSRequired()
    {
        return $this->s_required;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return VisaAdditionalRequirements
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set item_order
     *
     * @param integer $itemOrder
     * @return VisaAdditionalRequirements
     */
    public function setItemOrder($itemOrder)
    {
        $this->item_order = $itemOrder;
    
        return $this;
    }

    /**
     * Get item_order
     *
     * @return integer 
     */
    public function getItemOrder()
    {
        return $this->item_order;
    }
}