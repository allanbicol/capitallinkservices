<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderDlQuotes
 *
 * @ORM\Table(name="tbl_order_dl_quotes")
 * @ORM\Entity
 */
class OrderDlQuotes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_no", type="integer")
     */
    private $order_no;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000)
     */
    private $description;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="gst", type="integer")
     */
    private $gst;
    
    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="admin_id", type="integer")
     */
    private $admin_id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="sent_group", type="integer")
     */
    private $sent_group;
    
    /**
     * @var string
     *
     * @ORM\Column(name="sent_date", type="string", length=20)
     */
    private $sent_date;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order_no
     *
     * @param integer $orderNo
     * @return OrderDlQuotes
     */
    public function setOrderNo($orderNo)
    {
        $this->order_no = $orderNo;
    
        return $this;
    }

    /**
     * Get order_no
     *
     * @return integer 
     */
    public function getOrderNo()
    {
        return $this->order_no;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OrderDlQuotes
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return OrderDlQuotes
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set admin_id
     *
     * @param integer $adminId
     * @return OrderDlQuotes
     */
    public function setAdminId($adminId)
    {
        $this->admin_id = $adminId;
    
        return $this;
    }

    /**
     * Get admin_id
     *
     * @return integer 
     */
    public function getAdminId()
    {
        return $this->admin_id;
    }

    /**
     * Set sent_group
     *
     * @param integer $sentGroup
     * @return OrderDlQuotes
     */
    public function setSentGroup($sentGroup)
    {
        $this->sent_group = $sentGroup;
    
        return $this;
    }

    /**
     * Get sent_group
     *
     * @return integer 
     */
    public function getSentGroup()
    {
        return $this->sent_group;
    }

    /**
     * Set sent_date
     *
     * @param string $sentDate
     * @return OrderDlQuotes
     */
    public function setSentDate($sentDate)
    {
        $this->sent_date = $sentDate;
    
        return $this;
    }

    /**
     * Get sent_date
     *
     * @return string 
     */
    public function getSentDate()
    {
        return $this->sent_date;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return OrderDlQuotes
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set gst
     *
     * @param integer $gst
     * @return OrderDlQuotes
     */
    public function setGst($gst)
    {
        $this->gst = $gst;
    
        return $this;
    }

    /**
     * Get gst
     *
     * @return integer 
     */
    public function getGst()
    {
        return $this->gst;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return OrderDlQuotes
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }
}