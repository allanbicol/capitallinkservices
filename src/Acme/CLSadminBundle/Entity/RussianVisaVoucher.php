<?php

namespace Acme\CLSadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RussianVisaVoucher
 *
 * @ORM\Table(name="tbl_russian_visa_voucher_types")
 * @ORM\Entity
 */
class RussianVisaVoucher
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=1000)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="three_days_process_fee", type="float")
     */
    private $three_days_process_fee;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="one_two_days_process_fee", type="float")
     */
    private $one_two_days_process_fee;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="twelve_hrs_process_fee", type="float")
     */
    private $twelve_hrs_process_fee;
    
    /**
     * @var float
     *
     * @ORM\Column(name="thirteen_days", type="float")
     */
    private $thirteen_days;
    
    /**
     * @var float
     *
     * @ORM\Column(name="four_days", type="float")
     */
    private $four_days;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_active", type="integer")
     */
    private $s_active;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="type_order", type="string", length=1000)
     */
    private $type_order;


    

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return RussianVisaVoucher
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RussianVisaVoucher
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set three_days_process_fee
     *
     * @param float $threeDaysProcessFee
     * @return RussianVisaVoucher
     */
    public function setThreeDaysProcessFee($threeDaysProcessFee)
    {
        $this->three_days_process_fee = $threeDaysProcessFee;
    
        return $this;
    }

    /**
     * Get three_days_process_fee
     *
     * @return float 
     */
    public function getThreeDaysProcessFee()
    {
        return $this->three_days_process_fee;
    }

    /**
     * Set one_two_days_process_fee
     *
     * @param float $oneTwoDaysProcessFee
     * @return RussianVisaVoucher
     */
    public function setOneTwoDaysProcessFee($oneTwoDaysProcessFee)
    {
        $this->one_two_days_process_fee = $oneTwoDaysProcessFee;
    
        return $this;
    }

    /**
     * Get one_two_days_process_fee
     *
     * @return float 
     */
    public function getOneTwoDaysProcessFee()
    {
        return $this->one_two_days_process_fee;
    }

    /**
     * Set twelve_hrs_process_fee
     *
     * @param float $twelveHrsProcessFee
     * @return RussianVisaVoucher
     */
    public function setTwelveHrsProcessFee($twelveHrsProcessFee)
    {
        $this->twelve_hrs_process_fee = $twelveHrsProcessFee;
    
        return $this;
    }

    /**
     * Get twelve_hrs_process_fee
     *
     * @return float 
     */
    public function getTwelveHrsProcessFee()
    {
        return $this->twelve_hrs_process_fee;
    }

    /**
     * Set thirteen_days
     *
     * @param float $thirteenDays
     * @return RussianVisaVoucher
     */
    public function setThirteenDays($thirteenDays)
    {
        $this->thirteen_days = $thirteenDays;
    
        return $this;
    }

    /**
     * Get thirteen_days
     *
     * @return float 
     */
    public function getThirteenDays()
    {
        return $this->thirteen_days;
    }

    /**
     * Set four_days
     *
     * @param float $fourDays
     * @return RussianVisaVoucher
     */
    public function setFourDays($fourDays)
    {
        $this->four_days = $fourDays;
    
        return $this;
    }

    /**
     * Get four_days
     *
     * @return float 
     */
    public function getFourDays()
    {
        return $this->four_days;
    }

    /**
     * Set s_active
     *
     * @param integer $sActive
     * @return RussianVisaVoucher
     */
    public function setSActive($sActive)
    {
        $this->s_active = $sActive;
    
        return $this;
    }

    /**
     * Get s_active
     *
     * @return integer 
     */
    public function getSActive()
    {
        return $this->s_active;
    }


    /**
     * Set type_order
     *
     * @param string $typeOrder
     * @return RussianVisaVoucher
     */
    public function setTypeOrder($typeOrder)
    {
        $this->type_order = $typeOrder;
    
        return $this;
    }

    /**
     * Get type_order
     *
     * @return string 
     */
    public function getTypeOrder()
    {
        return $this->type_order;
    }
}