<?php

namespace Acme\CLSembassyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\CLSclientGovBundle\Model;

class ProfileController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'embassy-profile');
        
        
        
        if($session->get('embassy_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login'));
        }
        
        
        if(isset($_POST['hidSubmit'])){
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            
            
            $model = $em->getRepository('AcmeCLSadminBundle:EmbassyUser')->findOneBy(array('id'=>$session->get('embassy_id')));
            $model->setTitle($_POST['title']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setEmail($_POST['email']);
            $model->setPhone($_POST['phone']);
            $model->setMobile($_POST['mobile']);
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            $post = array(
                'title'=>$_POST['title'],
                'fname'=>$_POST['fname'],
                'lname'=>$_POST['lname'],
                'email'=>$_POST['email'],
                'phone'=>$_POST['phone'],
                'mobile'=>$_POST['mobile']
            );
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || $this->sEmailAvailableInDetailUpdate($_POST['email'], $session->get('embassy_id'), 'embassy-user') == false ||
                        (($_POST['password'] != '') && ($_POST['password'] != $_POST['passwordConfirm']))){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }
                
                if($this->sEmailAvailableInDetailUpdate($_POST['email'], $session->get('embassy_id'), 'embassy-user') == false){
                    $errors[] = array('message'=>$_POST['email'].' is already used.');
                    $error_count += 1;
                }
                
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $errors[] = array('message'=>'Password must be repeated exactly.');
                        $error_count += 1;
                    }
                }
                
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Your profile has been updated.'
                );
                
                return $this->redirect($this->generateUrl('acme_cls_embassy_profile'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
                return $this->render('AcmeCLSembassyBundle:Profile:index.html.twig',
                        array('errors'=>$errors,
                            'post'=>$post,
                            'name_titles'=> $this->getNameTitles(),
                            'data'=> $this->getEmbassyClientDetailsById($session->get('embassy_id'))
                        ));
            }
        }else{
            
            $em = $this->getDoctrine()->getManager(); 
            

            $model = $em->getRepository('AcmeCLSadminBundle:EmbassyUser')->findOneBy(array('id'=>$session->get('embassy_id')));
            
            return $this->render('AcmeCLSembassyBundle:Profile:index.html.twig',
                array('post' => $model,
                    'name_titles'=> $this->getNameTitles(),
                    'data'=> $this->getEmbassyClientDetailsById($session->get('embassy_id'))
                    )
                );
        }
    }

}
