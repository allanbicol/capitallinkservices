<?php

namespace Acme\CLSembassyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\CLSclientGovBundle\Entity\Orders;
use Acme\CLSclientGovBundle\Model;
use Acme\CLSclientGovBundle\Json;

class DefaultController extends \Acme\CLSclientGovBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $mod = new Model\GlobalModel;
        $session = $this->getRequest()->getSession();
        $session->set('page_name', 'embassy-home');
        $new_pass =0;
        if ($session->get('pass')!=''){
            if($mod->passwordHardening($session->get('pass')) != '1#1#1#1'){
                $new_pass = 1;
                $session->set('pass', '');
            }
        }
        
        if($session->get('embassy_email') == ''){
            return $this->redirect($this->generateUrl('acme_cls_client_login')); 
        }
        
        return $this->render('AcmeCLSembassyBundle:Default:index.html.twig',
                array(
                        'data'=> $this->getEmbassyClientDetailsById($session->get('embassy_id')),
                        'new_pass'=>$new_pass
                    )
                );
    }
    
    
    public function listAction(){
        $session = $this->getRequest()->getSession();
        
        $json = new Model\Json;
        if($session->get('embassy_email') != ''){
            $em = $this->getDoctrine()->getEntityManager();
            
            $connection = $em->getConnection();
            $statement = $connection->prepare("
                SELECT od.order_no, od.departure_date, od.visa_date_submitted_for_processing, od.visa_date_completed_and_received_at_cls, 
                    (CASE WHEN o.order_type = 6 THEN pvt.type ELSE vt.type END) as type,
                    (Select group_concat(concat(concat(concat(concat(concat(ot.fname,' '), ot.lname), ' ('), ot.passport_number), ')'))  from tbl_order_travellers ot where ot.order_no=o.order_no) as applicants
                FROM tbl_order_destinations od
                LEFT JOIN tbl_orders o ON o.order_no = od.order_no
                LEFT JOIN tbl_visa_types vt ON vt.id = od.selected_visa_type
                LEFT JOIN tbl_public_visa_types pvt ON pvt.id = od.selected_visa_type
                WHERE o.status >= 10 AND (o.order_type = 1 OR o.order_type = 3 OR o.order_type = 6)
                    AND od.country_id = ". $session->get('country'));
            $statement->execute();
            $results = $statement->fetchAll();
            $total_pages= count($results);
            $fieldNames = array("order_no","departure_date","visa_date_submitted_for_processing","visa_date_completed_and_received_at_cls",
                "applicants","type");

            return new Response($json->jsonEncode($results, $total_pages, $fieldNames));
        }else{
            //return $this->redirect($this->generateUrl('login')); // redirect to login page
            return new Response('session expired');
        }
    }
    
    
    public function changePasswordAction(){
        
        if(isset($_POST['password'])){
            
            $mod = new Model\GlobalModel();
            $session = $this->getRequest()->getSession();
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 

            $model = $em->getRepository('AcmeCLSadminBundle:EmbassyUser')->findOneBy(array('id'=>$session->get('embassy_id')));
            if(trim($_POST['password']) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }        
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            $error_code = 0;
            
                
            if($error_count == 0){
                if($_POST['password'] != ''){
                    if($_POST['password'] != $_POST['passwordConfirm']){
                        $error_code =1;
                        $error_count += 1;
                    }
                }
            }
            
            $passCheck = $mod->passwordHardening($_POST['password']);
            if($passCheck != '1#1#1#1'){
                $error_code = 2;//'You must Follow the password required standard.';
                $error_count += 1;
            }
            
            if($error_count == 0){
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
               
                $this->sendEmail($session->get('embassy_email'), "help@capitallinkservices.com.au", "Change Password",
                                        $this->renderView('AcmeCLSadminBundle:Home:changePassword.html.twig',
                                        array('fname'=>$session->get('embassy_fname'),
                                            'password'=> $_POST['password'],
                                            'domain'=>$mod->siteURL()
                                            )
                                        ));
    
            }else{
                $em->getConnection()->rollback();
                $em->close();
                $error_code = 3;
            }
            
            return new Response($error_code);
        }
    }
}
